<?php
class ControllerLivehelpLivehelpStatus extends Controller {
   private $error = array();
   private $file_path = 'livehelp/livehelp_status';
   
   public function index() {
      $this->language->load($this->file_path);
      $this->load->model($this->file_path);
      
      $this->document->setTitle($this->language->get('heading_title'));
      $this->getList();
   }
   
   public function insert() {
      $this->language->load($this->file_path);
      
      $this->document->setTitle($this->language->get('heading_title'));
      
      $this->load->model($this->file_path);
      
      if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
         $this->model_livehelp_livehelp_status->addStatus($this->request->post);
         
         $this->session->data['success'] = $this->language->get('text_success');
         
         $this->redirect($this->url->link($this->file_path, 'token=' . $this->session->data['token'], 'SSL'));
      }
      
      $this->getForm();
   }
   
   public function update() {
      $this->language->load($this->file_path);
      
      $this->document->setTitle($this->language->get('heading_title'));
      
      $this->load->model($this->file_path);
      
      if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
         $this->model_livehelp_livehelp_status->editStatus($this->request->get['status_id'], $this->request->post);
         
         $this->session->data['success'] = $this->language->get('text_success');
         
         $this->redirect($this->url->link($this->file_path, 'token=' . $this->session->data['token'], 'SSL'));
      }
      
      $this->getForm();
   }
   
   public function delete() {
      $this->language->load($this->file_path);
      
      $this->document->setTitle($this->language->get('heading_title'));
      
      $this->load->model($this->file_path);
      
      if (isset($this->request->post['selected']) && $this->validateModify()) {
         foreach ($this->request->post['selected'] as $status_id) {
            $this->model_livehelp_livehelp_status->deleteStatus($status_id);
         }
         
         $this->session->data['success'] = $this->language->get('text_success');
         
         $this->redirect($this->url->link($this->file_path, 'token=' . $this->session->data['token'], 'SSL'));
      }
      
      $this->getList();
   }
   
   protected function getList() {
      if (isset($this->request->get['sort'])) {
         $sort = $this->request->get['sort'];
      } else {
         $sort = 'lst.name';
      }
      
      if (isset($this->request->get['order'])) {
         $order = $this->request->get['order'];
      } else {
         $order = 'ASC';
      }
      
      if (isset($this->request->get['page'])) {
         $page = $this->request->get['page'];
      } else {
         $page = 1;
      }
      
      $url = $this->getUrl(array(
         "sort",
         "order",
         "page"
      ));
      
      $this->data['breadcrumbs'] = array();
      
      $this->data['breadcrumbs'][] = array(
         'text' => $this->language->get('text_home'),
         'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
         'separator' => false
      );
      
      $this->data['breadcrumbs'][] = array(
         'text' => $this->language->get('heading_title'),
         'href' => $this->url->link('livehelp/livehelp_status', 'token=' . $this->session->data['token'] . $url, 'SSL'),
         'separator' => ' :: '
      );
      
      if (isset($this->error['warning'])) {
         $this->data['error_warning'] = $this->error['warning'];
      } else {
         $this->data['error_warning'] = '';
      }
      
      if (isset($this->session->data['success'])) {
         $this->data['success'] = $this->session->data['success'];
         
         unset($this->session->data['success']);
      } else {
         $this->data['success'] = '';
      }
      
      $this->data['heading_title'] = $this->language->get('heading_title');
      
      $this->data['text_no_results'] = $this->language->get('text_no_results');
      
      $this->data['column_name']   = $this->language->get('column_name');
      $this->data['column_color']  = $this->language->get('column_color');
      $this->data['column_action'] = $this->language->get('column_action');
      
      $this->data['button_delete'] = $this->language->get('button_delete');
      $this->data['button_insert'] = $this->language->get('button_insert');
      
      $this->data['insert'] = $this->url->link($this->file_path . "/insert", 'token=' . $this->session->data['token'] . $url);
      $this->data['delete'] = $this->url->link($this->file_path . "/delete", 'token=' . $this->session->data['token'] . $url);
      
      $data = array(
         'sort' => $sort,
         'order' => $order,
         'start' => ($page - 1) * $this->config->get('config_admin_limit'),
         'page' => $page,
         'limit' => $this->config->get('config_admin_limit')
      );
      
      $this->data['statuses'] = array();
      
      $result_total = $this->model_livehelp_livehelp_status->getTotalStatuses($data);
      
      $results = $this->model_livehelp_livehelp_status->getStatuses($data);
      
      foreach ($results as $result) {
         $action = array();
         
         $action[] = array(
            'text' => $this->language->get('text_edit'),
            'href' => $this->url->link('livehelp/livehelp_status/update', 'token=' . $this->session->data['token'] . '&status_id=' . $result['status_id'] . $url, 'SSL')
         );
         
         $this->data['statuses'][] = array(
            'status_id' => $result['status_id'],
            'name' => $result['name'],
            'color' => '<span class="label" style="background-color: #' . $result['color'] . ';">' . $result['color'] . '</span>',
            'selected' => isset($this->request->post['selected']) && in_array($result['status_id'], $this->request->post['selected']),
            'selected' => isset($this->request->post['selected']) && in_array($result['status_id'], $this->request->post['selected']),
            'action' => $action
         );
      }
      
      $url = $this->getUrl(array(
         'sort',
         'order'
      ));
      
      $pagination        = new Pagination();
      $pagination->total = $result_total;
      $pagination->page  = $page;
      $pagination->limit = $this->config->get('config_admin_limit');
      $pagination->text  = $this->language->get('text_pagination');
      $pagination->url   = $this->url->link($this->file_path, 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
      
      $this->data['pagination'] = $pagination->render();
      
      $url = $this->getUrl(array(
         'page'
      ));
      
      if ($order == 'ASC') {
         $url .= '&order=DESC';
      } else {
         $url .= '&order=ASC';
      }
      
      $this->data['sort_name'] = $this->url->link($this->file_path, 'token=' . $this->session->data['token'] . '&sort=lst.name' . $url, 'SSL');
      
      $this->data['sort']  = $sort;
      $this->data['order'] = $order;
      
      $this->template = 'livehelp/livehelp_status_list.tpl';
      $this->children = array(
         'common/header',
         'common/footer'
      );
      
      $this->response->setOutput($this->render());
   }
   
   protected function getForm() {
      $this->document->addStyle('view/javascript/jquery/color_picker/jquery.minicolors.css');
      $this->document->addScript('view/javascript/jquery/color_picker/jquery.minicolors.min.js');
      
      $this->data['heading_title'] = $this->language->get('heading_title');
      
      $this->data['text_enabled']  = $this->language->get('text_enabled');
      $this->data['text_disabled'] = $this->language->get('text_disabled');
      $this->data['text_yes']      = $this->language->get('text_yes');
      $this->data['text_no']       = $this->language->get('text_no');
      $this->data['text_select']   = $this->language->get('text_select');
      
      $this->data['entry_color']       = $this->language->get('entry_color');
      $this->data['entry_name']        = $this->language->get('entry_name');
      $this->data['entry_user']        = $this->language->get('entry_user');
      $this->data['entry_description'] = $this->language->get('entry_description');
      
      $this->data['entry_text'] = $this->language->get('entry_text');
      
      $this->data['title_name']  = $this->language->get('title_name');
      $this->data['title_color'] = $this->language->get('title_color');
      
      $this->data['button_save']   = $this->language->get('button_save');
      $this->data['button_cancel'] = $this->language->get('button_cancel');
      
      $this->data['cancel'] = $this->url->link($this->file_path);
      
      if (isset($this->error['warning'])) {
         $this->data['error_warning'] = $this->error['warning'];
      } else {
         $this->data['error_warning'] = '';
      }
      
      if (isset($this->error['name'])) {
         $this->data['error_name'] = $this->error['name'];
      } else {
         $this->data['error_name'] = array();
      }
      
      if (isset($this->error['text'])) {
         $this->data['error_text'] = $this->error['text'];
      } else {
         $this->data['error_text'] = array();
      }
      
      $url = $this->getUrl(array(
         "sort",
         "order",
         "page"
      ));
      
      $this->data['breadcrumbs'] = array();
      
      $this->data['breadcrumbs'][] = array(
         'text' => $this->language->get('text_home'),
         'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
         'separator' => false
      );
      
      $this->data['breadcrumbs'][] = array(
         'text' => $this->language->get('heading_title'),
         'href' => $this->url->link($this->file_path, 'token=' . $this->session->data['token'] . $url, 'SSL'),
         'separator' => ' :: '
      );
      
      if (!isset($this->request->get['status_id'])) {
         $this->data['action'] = $this->url->link('livehelp/livehelp_status/insert', 'token=' . $this->session->data['token'], 'SSL');
      } else {
         $this->data['action'] = $this->url->link('livehelp/livehelp_status/update', 'token=' . $this->session->data['token'] . '&status_id=' . $this->request->get['status_id'], 'SSL');
      }
      
      if (isset($this->request->get['status_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
         $status_info = $this->model_livehelp_livehelp_status->getStatus($this->request->get['status_id']);
      }
      
      if (isset($this->request->post['status_text'])) {
         $this->data['status_text'] = $this->request->post['status_text'];
      } elseif (isset($this->request->get['status_id'])) {
         $this->data['status_text'] = $this->model_livehelp_livehelp_status->getStatusText($this->request->get['status_id']);
      } else {
         $this->data['status_text'] = array();
      }
      
      if (isset($this->request->post['color'])) {
         $this->data['color'] = $this->request->post['color'];
      } elseif (!empty($status_info)) {
         $this->data['color'] = $status_info['color'];
      } else {
         $this->data['color'] = '000000';
      }
      
      // Languages
      $this->load->model('localisation/language');
      
      $this->data['languages'] = $this->model_localisation_language->getLanguages();
      
      $this->template = 'livehelp/livehelp_status_form.tpl';
      $this->children = array(
         'common/header',
         'common/footer'
      );
      
      $this->response->setOutput($this->render());
   }
   
   /*
   VALIDATION
   */
   protected function validateModify() {
      if (!$this->user->hasPermission('modify', $this->file_path)) {
         $this->error['warning'] = $this->language->get('error_permission');
      }
      
      return !$this->error;
   }
   
   protected function validateForm() {
      $this->validateModify();
      
      foreach ($this->request->post['status_text'] as $language_id => $value) {
         if ((utf8_strlen($value['name']) < 2) || (utf8_strlen($value['name']) > 64)) {
            $this->error['name'][$language_id] = $this->language->get('error_name');
         }
         
         if ((utf8_strlen($value['text']) < 3) || (utf8_strlen($value['text']) > 450)) {
            $this->error['text'][$language_id] = $this->language->get('error_text');
         }
      }
      
      return !$this->error;
   }
   
   protected function getUrl($var) {
      $url = '';
      
      if (is_string($var)) {
         if (isset($this->request->get[$var]))
            $url .= "&" . $var . "=" . $this->request->get[$var];
      } else if (is_array($var)) {
         foreach ($var as $value) {
            if (isset($this->request->get[$value]))
               $url .= "&" . $value . "=" . $this->request->get[$value];
         }
      }
      return $url;
   }
}
?>