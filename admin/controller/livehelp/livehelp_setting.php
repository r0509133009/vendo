<?php
class ControllerLivehelpLivehelpSetting extends Controller {
   private $sound_folder = "sounds/";
   private $error = array();
   private $file_path = 'livehelp/livehelp_setting';
   
   public function index() {
      $this->language->load($this->file_path);
      $this->load->model($this->file_path);
      
      if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
         $this->model_livehelp_livehelp_setting->setSetting($this->request->post);
         
         $this->session->data['success'] = $this->language->get('text_success');
         
         $this->redirect($this->url->link('livehelp/livehelp_setting', 'token=' . $this->session->data['token'], 'SSL'));
      }
      
      $this->initClass();
      $this->document->setTitle($this->language->get('heading_title'));
      
      $this->data['breadcrumbs'] = array();
      
      $this->data['breadcrumbs'][] = array(
         'text' => $this->language->get('text_home'),
         'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
         'separator' => false
      );
      
      $this->data['breadcrumbs'][] = array(
         'text' => $this->language->get('heading_title'),
         'href' => $this->url->link('livehelp/livehelp_setting', 'token=' . $this->session->data['token'], 'SSL'),
         'separator' => ' :: '
      );
      
      if (isset($this->session->data['success'])) {
         $this->data['success'] = $this->session->data['success'];
         
         unset($this->session->data['success']);
      } else {
         $this->data['success'] = '';
      }
      
      $this->data['heading_title'] = $this->language->get('heading_title');
      
      $this->data['text_enabled']     = $this->language->get('text_enabled');
      $this->data['text_disabled']    = $this->language->get('text_disabled');
      $this->data['text_yes']         = $this->language->get('text_yes');
      $this->data['text_no']          = $this->language->get('text_no');
      $this->data['text_select']      = $this->language->get('text_select');
      $this->data['text_plugin']      = $this->language->get('text_plugin');
      $this->data['text_offline']     = $this->language->get('text_offline');
      $this->data['text_count']       = $this->language->get('text_count');
      $this->data['text_seconds']     = $this->language->get('text_seconds');
      $this->data['text_miliseconds'] = $this->language->get('text_miliseconds');
      $this->data['text_play']        = $this->language->get('text_play');
      
      $this->data['text_spam_filter']          = $this->language->get('text_spam_filter');
      $this->data['text_entry_email']          = $this->language->get('text_entry_email');
      $this->data['text_iteration']            = $this->language->get('text_iteration');
      $this->data['text_off']                  = $this->language->get('text_off');
      $this->data['text_logout']               = $this->language->get('text_logout');
      $this->data['text_toggle_status']        = $this->language->get('text_toggle_status');
      $this->data['text_sample_image_display'] = $this->language->get('text_sample_image_display');
      $this->data['text_sample_image']         = $this->language->get('text_sample_image');
      $this->data['text_sounds']               = $this->language->get('text_sounds');
      $this->data['text_accepting_html']       = $this->language->get('text_accepting_html');
      $this->data['tab_general']               = $this->language->get('tab_general');
      $this->data['tab_admin']                 = $this->language->get('tab_admin');
      $this->data['tab_store']                 = $this->language->get('tab_store');
      
      $this->data['column_filename']         = $this->language->get('column_filename');
      $this->data['column_new_message']      = $this->language->get('column_new_message');
      $this->data['column_message_sent']     = $this->language->get('column_message_sent');
      $this->data['column_user_logged']      = $this->language->get('column_user_logged');
      $this->data['column_user_logout']      = $this->language->get('column_user_logout');
      $this->data['column_test_sound']       = $this->language->get('column_test_sound');
      $this->data['column_special_function'] = $this->language->get('column_special_function');
      $this->data['column_operator_logged']  = $this->language->get('column_operator_logged');
      $this->data['column_operator_logout']  = $this->language->get('column_operator_logout');
      
      $this->data['entry_cart_update_interval']    = $this->language->get('entry_cart_update_interval');
      $this->data['entry_absolute_path']           = $this->language->get('entry_absolute_path');
      $this->data['entry_text_max_length']         = $this->language->get('entry_text_max_length');
      $this->data['entry_data_warehouse_status']   = $this->language->get('entry_data_warehouse_status');
      $this->data['entry_store_inactive_timeout']  = $this->language->get('entry_store_inactive_timeout');
      $this->data['entry_admin_inactive_timeout']  = $this->language->get('entry_admin_inactive_timeout');
      $this->data['entry_refresh_rate']            = $this->language->get('entry_refresh_rate');
      $this->data['entry_admin_inactive_action']   = $this->language->get('entry_admin_inactive_action');
      $this->data['entry_expired']                 = $this->language->get('entry_expired');
      $this->data['entry_store_heading_title']     = $this->language->get('entry_store_heading_title');
      $this->data['entry_smiley_status']           = $this->language->get('entry_smiley_status');
      $this->data['entry_bbcode_status']           = $this->language->get('entry_bbcode_status');
      $this->data['entry_offline_form']            = $this->language->get('entry_offline_form');
      $this->data['entry_offline_form_emails']     = $this->language->get('entry_offline_form_emails');
      $this->data['entry_spam_filter_status']      = $this->language->get('entry_spam_filter_status');
      $this->data['entry_spam_filter_score_limit'] = $this->language->get('entry_spam_filter_score_limit');
      $this->data['entry_spam_filter_penalty']     = $this->language->get('entry_spam_filter_penalty');
      $this->data['entry_spam_filter_range']       = $this->language->get('entry_spam_filter_range');
      
      $this->data['title_expired']                = $this->language->get('title_expired');
      $this->data['title_absolute_path']          = $this->language->get('title_absolute_path');
      $this->data['title_refresh_rate']           = $this->language->get('title_refresh_rate');
      $this->data['title_admin_inactive_timeout'] = $this->language->get('title_admin_inactive_timeout');
      $this->data['title_admin_inactive_action']  = $this->language->get('title_admin_inactive_action');
      $this->data['title_store_inactive_timeout'] = $this->language->get('title_store_inactive_timeout');
      $this->data['title_cart_update_interval']   = $this->language->get('title_cart_update_interval');
      $this->data['title_spam_filter_penalty']    = $this->language->get('title_spam_filter_penalty');
      $this->data['title_spam_filter_range']      = $this->language->get('title_spam_filter_range');
      $this->data['title_offline_form_emails']    = $this->language->get('title_offline_form_emails');
      
      
      $this->data['button_save'] = $this->language->get('button_save');
      
      if (isset($this->error['warning'])) {
         $this->data['error_warning'] = $this->error['warning'];
      } else {
         $this->data['error_warning'] = '';
      }
      
      foreach ($this->error as $key => $value) {
         $this->data['error_' . $key] = $value;
      }
      
      $this->data['action'] = $this->url->link('livehelp/livehelp_setting', 'token=' . $this->session->data['token'], 'SSL');
      
      // GENERAL VARIABLES
      if (isset($this->request->post['admin_refresh_rate'])) {
         $this->data['admin_refresh_rate'] = $this->request->post['admin_refresh_rate'];
      } else {
         $this->data['admin_refresh_rate'] = $this->livehelp->getSetting('admin_refresh_rate');
      }
      
      if (isset($this->request->post['store_refresh_rate'])) {
         $this->data['store_refresh_rate'] = $this->request->post['store_refresh_rate'];
      } else {
         $this->data['store_refresh_rate'] = $this->livehelp->getSetting('store_refresh_rate');
      }
      
      if (isset($this->request->post['absolute_path'])) {
         $this->data['absolute_path'] = $this->request->post['absolute_path'];
      } else {
         $this->data['absolute_path'] = $this->livehelp->getSetting('absolute_path');
      }
      
      if (isset($this->request->post['text_max_length'])) {
         $this->data['text_max_length'] = $this->request->post['text_max_length'];
      } else {
         $this->data['text_max_length'] = $this->livehelp->getSetting('text_max_length');
      }
      
      if (isset($this->request->post['smiley_status'])) {
         $this->data['smiley_status'] = $this->request->post['smiley_status'];
      } else {
         $this->data['smiley_status'] = $this->livehelp->getSetting('smiley_status');
      }
      
      if (isset($this->request->post['bbcode_status'])) {
         $this->data['bbcode_status'] = $this->request->post['bbcode_status'];
      } else {
         $this->data['bbcode_status'] = $this->livehelp->getSetting('bbcode_status');
      }
      
      if (isset($this->request->post['data_warehouse_status'])) {
         $this->data['data_warehouse_status'] = $this->request->post['data_warehouse_status'];
      } else {
         $this->data['data_warehouse_status'] = $this->livehelp->getSetting('data_warehouse_status');
      }
      
      if (isset($this->request->post['admin_inactive_timeout'])) {
         $this->data['admin_inactive_timeout'] = $this->request->post['admin_inactive_timeout'];
      } else {
         $this->data['admin_inactive_timeout'] = $this->livehelp->getSetting('admin_inactive_timeout');
      }
      
      if (isset($this->request->post['store_inactive_timeout'])) {
         $this->data['store_inactive_timeout'] = $this->request->post['store_inactive_timeout'];
      } else {
         $this->data['store_inactive_timeout'] = $this->livehelp->getSetting('store_inactive_timeout');
      }
      
      if (isset($this->request->post['cart_update_interval'])) {
         $this->data['cart_update_interval'] = (int) $this->request->post['cart_update_interval'];
      } else {
         $this->data['cart_update_interval'] = (int) $this->livehelp->getSetting('cart_update_interval');
      }
      
      $emoticons                     = $this->livehelp->getEmoticons();
      $this->data['sample_emoticon'] = (isset($emoticons['smile']) ? "image/emoticons/smile.png" : false);
      
      if (isset($this->request->post['offline_form'])) {
         $this->data['offline_form'] = $this->request->post['offline_form'];
      } else {
         $this->data['offline_form'] = $this->livehelp->getSetting('offline_form');
      }
      
      if (isset($this->request->post['offline_form_emails'])) {
         $this->data['offline_form_emails'] = $this->request->post['offline_form_emails'];
      } else {
         $this->data['offline_form_emails'] = $this->livehelp->getSetting('offline_form_emails');
      }
      
      // SPAM FILTER
      if (isset($this->request->post['spam_filter_status'])) {
         $this->data['spam_filter_status'] = $this->request->post['spam_filter_status'];
      } else {
         $this->data['spam_filter_status'] = $this->livehelp->getSetting('spam_filter_status');
      }
      
      if (isset($this->request->post['spam_filter_score_limit'])) {
         $this->data['spam_filter_score_limit'] = $this->request->post['spam_filter_score_limit'];
      } else {
         $this->data['spam_filter_score_limit'] = $this->livehelp->getSetting('spam_filter_score_limit');
      }
      
      if (isset($this->request->post['spam_filter_penalty'])) {
         $this->data['spam_filter_penalty'] = $this->request->post['spam_filter_penalty'];
      } else {
         $this->data['spam_filter_penalty'] = $this->livehelp->getSetting('spam_filter_penalty');
      }
      
      if (isset($this->request->post['spam_filter_range'])) {
         $this->data['spam_filter_range'] = $this->request->post['spam_filter_range'];
      } else {
         $this->data['spam_filter_range'] = $this->livehelp->getSetting('spam_filter_range');
      }
      
      // SOUNDS
      // admin
      if (isset($this->request->post['admin_sound_new_message'])) {
         $this->data['admin_sound_new_message'] = $this->request->post['admin_sound_new_message'];
      } else {
         $this->data['admin_sound_new_message'] = $this->livehelp->getSetting('admin_sound_new_message');
      }
      
      if (isset($this->request->post['admin_sound_message_sent'])) {
         $this->data['admin_sound_message_sent'] = $this->request->post['admin_sound_message_sent'];
      } else {
         $this->data['admin_sound_message_sent'] = $this->livehelp->getSetting('admin_sound_message_sent');
      }
      
      if (isset($this->request->post['admin_sound_user_logged'])) {
         $this->data['admin_sound_user_logged'] = $this->request->post['admin_sound_user_logged'];
      } else {
         $this->data['admin_sound_user_logged'] = $this->livehelp->getSetting('admin_sound_user_logged');
      }
      
      if (isset($this->request->post['admin_sound_user_logout'])) {
         $this->data['admin_sound_user_logout'] = $this->request->post['admin_sound_user_logout'];
      } else {
         $this->data['admin_sound_user_logout'] = $this->livehelp->getSetting('admin_sound_user_logout');
      }
      
      if (isset($this->request->post['admin_sound_special'])) {
         $this->data['admin_sound_special'] = $this->request->post['admin_sound_special'];
      } else {
         $this->data['admin_sound_special'] = $this->livehelp->getSetting('admin_sound_special');
      }
      
      // store
      if (isset($this->request->post['store_heading_title'])) {
         $this->data['store_heading_title'] = $this->request->post['store_heading_title'];
      } else {
         $this->data['store_heading_title'] = ($this->livehelp->getSetting('store_heading_title') ? $this->livehelp->getSetting('store_heading_title') : array());
      }
      
      if (isset($this->request->post['store_sound_new_message'])) {
         $this->data['store_sound_new_message'] = $this->request->post['store_sound_new_message'];
      } else {
         $this->data['store_sound_new_message'] = $this->livehelp->getSetting('store_sound_new_message');
      }
      
      if (isset($this->request->post['store_sound_message_sent'])) {
         $this->data['store_sound_message_sent'] = $this->request->post['store_sound_message_sent'];
      } else {
         $this->data['store_sound_message_sent'] = $this->livehelp->getSetting('store_sound_message_sent');
      }
      
      if (isset($this->request->post['store_sound_operator_logged'])) {
         $this->data['store_sound_operator_logged'] = $this->request->post['store_sound_operator_logged'];
      } else {
         $this->data['store_sound_operator_logged'] = $this->livehelp->getSetting('store_sound_operator_logged');
      }
      
      if (isset($this->request->post['store_sound_operator_logout'])) {
         $this->data['store_sound_operator_logout'] = $this->request->post['store_sound_operator_logout'];
      } else {
         $this->data['store_sound_operator_logout'] = $this->livehelp->getSetting('store_sound_operator_logout');
      }
      
      if (isset($this->request->post['store_sound_special'])) {
         $this->data['store_sound_special'] = $this->request->post['store_sound_special'];
      } else {
         $this->data['store_sound_special'] = $this->livehelp->getSetting('store_sound_special');
      }
      
      // LANGUAGES
      $this->load->model('localisation/language');
      $this->data['languages'] = $this->model_localisation_language->getLanguages();
      
      // STATUS TEXT
      $this->load->model('livehelp/livehelp_status');
      $this->data['statuses'] = $this->model_livehelp_livehelp_status->getStatuses();
      
      // SOUNDS
      $sounds = scandir(DIR_IMAGE . $this->sound_folder);
      unset($sounds[0]);
      unset($sounds[1]);
      
      $this->data['sounds']       = array_values($sounds);
      $this->data['sound_folder'] = DIR_IMAGE . $this->sound_folder;
      
      $this->template = 'livehelp/livehelp_setting.tpl';
      $this->children = array(
         'common/header',
         'common/footer'
      );
      
      $this->response->setOutput($this->render());
   }
   
   protected function validate() {
      if (!$this->user->hasPermission('modify', $this->file_path)) {
         $this->error['warning'] = $this->language->get('error_permission');
      }
      
      // ms unit 1000 = 1 second
      if (!$this->request->post['admin_refresh_rate'] || (int) $this->request->post['admin_refresh_rate'] < 2000) {
         $this->error['admin_refresh_rate'] = $this->language->get('error_admin_refresh_rate');
      }
      
      if (!$this->request->post['store_refresh_rate'] || (int) $this->request->post['store_refresh_rate'] < 2000) {
         $this->error['store_refresh_rate'] = $this->language->get('error_store_refresh_rate');
      }
      
      if ((int) $this->request->post['text_max_length'] < 10) {
         $this->error['text_max_length'] = $this->language->get('error_text_max_length');
      }
      
      // s unit: 60 = 1 minute
      if (!$this->request->post['admin_inactive_timeout'] || $this->request->post['admin_inactive_timeout'] < 60) {
         $this->error['admin_inactive_timeout'] = $this->language->get('error_admin_inactive_timeout');
      }
      
      if (!$this->request->post['store_inactive_timeout'] || $this->request->post['store_inactive_timeout'] < 60) {
         $this->error['store_inactive_timeout'] = $this->language->get('error_store_inactive_timeout');
      }
      
      return !$this->error;
   }
   
   protected function initClass() {
      $this->load->library('livehelp/livehelp_operator');
      $livehelp = new Livehelp($this->registry);
      $this->registry->set('livehelp', $livehelp);
   }
}
?>