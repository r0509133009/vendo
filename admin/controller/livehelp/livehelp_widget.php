<?php
class ControllerLivehelpLivehelpWidget extends Controller {
   public function index() {
      if(!$this->config->get('livehelp_status')) return;

      // Global livehelp status
      $this->initClass();
      if (!$this->livehelp->hasAccess()) return;
            
      $this->language->load('livehelp/livehelp_widget');
      
      $this->document->addStyle('view/stylesheet/livehelp.css');
      $this->document->addScript('view/javascript/livehelp/livehelp_interface.min.js');
      
      // AS LOGGED
      $this->data['is_logged'] = false;
      
      if ($this->livehelp->isLogged()) {
         $this->document->addScript('view/javascript/livehelp/livehelp_chat.min.js');
        
         $this->data['is_logged'] = true;
         $this->data['status_id'] = $this->livehelp->getStatus();
        
         $this->data['operator_id'] = $this->livehelp->getId();

         $this->data['threads'] = $this->livehelp->getCurrentThreads();
        
         // Thread actions
         $this->data['text_kick']            = $this->language->get('text_kick');
         $this->data['text_ban']             = $this->language->get('text_ban');
         $this->data['text_remove']          = $this->language->get('text_remove');
         $this->data['text_toggle_operator'] = $this->language->get('text_toggle_operator');
        
         $this->data['text_thread_manage'] = $this->language->get('text_thread_manage');
        
         $this->data['operators'] = $this->livehelp->getOperators(array(
            'online' => 1,
            'except_self' => true
         ));
        
         // SETTINGS  
         $setting = array();
        
         $setting['admin_refresh_rate'] = (int) $this->livehelp->getSetting('admin_refresh_rate');
         $setting['text_max_length']    = (int) $this->livehelp->getSetting('text_max_length');
        
         $setting['smiley_status'] = $this->livehelp->getSetting('smiley_status');
         $setting['bbcode_status'] = $this->livehelp->getSetting('bbcode_status');
        
         $setting['admin_sound_new_message']  = $this->livehelp->getSetting('admin_sound_new_message');
         $setting['admin_sound_message_sent'] = $this->livehelp->getSetting('admin_sound_message_sent');
         $setting['admin_sound_user_logged']  = $this->livehelp->getSetting('admin_sound_user_logged');
         $setting['admin_sound_user_logout']  = $this->livehelp->getSetting('admin_sound_user_logout');
         $setting['admin_sound_special']      = $this->livehelp->getSetting('admin_sound_special');
        
         $setting['sound'] = ($setting['admin_sound_new_message'] || $setting['admin_sound_message_sent'] || $setting['admin_sound_user_logged'] || $setting['admin_sound_user_logout'] || $setting['admin_sound_special']);
        
         // Load smiles
         $this->data['emoticons']  = $this->livehelp->getEmoticons();
         $setting['smiley_status'] = ($setting['smiley_status'] && !empty($this->data['emoticons'])); // merge to one
        
         $this->data['language_id']   = $this->config->get('config_language_id');
         $this->data['language_code'] = $this->config->get('config_language');
        
         $this->data['setting'] = $setting;
        
         // Get status message
         $this->data['statuses'] = array();
        
         if ($this->livehelp->isLogged()) {
            $this->load->model('livehelp/livehelp_status');
            
            $this->data['statuses'] = $this->model_livehelp_livehelp_status->getStatuses();
         }

         $this->data['js_language'] = array();
         $this->data['js_language'][$this->config->get('config_language')] = array(
            'text_operator_signed'     => $this->language->get('text_js_operator_signed'),
            'text_username'            => $this->language->get('text_js_username'),
            'text_ip'                  => $this->language->get('text_js_ip'),
            'text_user_agent'          => $this->language->get('text_js_user_agent'),
            'text_referer'             => $this->language->get('text_js_referer'),
            'text_customer'            => $this->language->get('text_js_customer'),
            'text_registred'           => $this->language->get('text_js_registred'),
            'text_not_registred'       => $this->language->get('text_js_not_registred'),
            'text_date_added'          => $this->language->get('text_js_date_added'),
            'text_closed'              => $this->language->get('text_js_closed'),
            'text_ban_success'         => $this->language->get('text_js_ban_success'),
            'text_last_activity'       => $this->language->get('text_js_last_activity'),
            'text_before'              => $this->language->get('text_js_before'),
            'text_user_away'           => $this->language->get('text_js_user_away'),
            'text_user_back_online'    => $this->language->get('text_js_user_back_online'),
            'text_right_now'           => $this->language->get('text_js_right_now'),
            'text_user_cart'           => $this->language->get('text_js_user_cart'),
            'text_cart_items'          => $this->language->get('text_js_cart_items'),
            'text_cart_total'          => $this->language->get('text_js_cart_total'),
            'text_in_stock'            => $this->language->get('text_js_in_stock'),
            'text_out_stock'           => $this->language->get('text_js_out_stock'),
            'text_product_id'          => $this->language->get('text_js_product_id'),
            'text_product_name'        => $this->language->get('text_js_product_name'),
            'text_product_qty'         => $this->language->get('text_js_product_qty'),
            'text_empty_cart'          => $this->language->get('text_js_empty_cart'),
         );
      }
     
      // Language
      $this->data['heading_title'] = $this->language->get('heading_title');
      
      $this->data['text_nothing']         = $this->language->get('text_nothing');
      $this->data['text_status']          = $this->language->get('text_status');
      $this->data['text_online']          = $this->language->get('text_online');
      $this->data['text_offline']         = $this->language->get('text_offline');
      $this->data['text_signout_confirm'] = $this->language->get('text_signout_confirm');
      $this->data['text_current_state']   = $this->language->get('text_current_state');
      
      $this->data['button_sign_in']  = $this->language->get('button_sign_in');
      $this->data['button_sign_out'] = $this->language->get('button_sign_out');
      
      // BBCode title
      $this->data['title_bold']      = $this->language->get('title_bold');
      $this->data['title_italic']    = $this->language->get('title_italic');
      $this->data['title_underline'] = $this->language->get('title_underline');
      $this->data['title_link']      = $this->language->get('title_link');
      $this->data['title_image']     = $this->language->get('title_image');
      $this->data['title_youtube']   = $this->language->get('title_youtube');
      
      $this->data['title_tack_toggle']  = $this->language->get('title_tack_toggle');
      $this->data['title_sound_toggle'] = $this->language->get('title_sound_toggle');

      $this->data['token'] = $this->session->data['token'];
            
      $this->template = 'livehelp/livehelp_widget.tpl';
      
      $this->response->setOutput($this->render());
   }
  
   public function operation() {
      $this->initClass();
      $this->language->load('livehelp/livehelp_widget');
      $json = array();
      
      if ($this->request->server['REQUEST_METHOD'] == 'POST') {
         if (isset($this->request->post['operation'])) {
            $operation = strtolower($this->request->post['operation']);
         } else {
            $operation = null;
         }
        
         try {
            switch ($operation) {
               case 'login':
                  $this->livehelp->login();
                  break;
               case 'logout':
                  $this->livehelp->logout();
                  break;
               case 'changestatus':
                  if (!isset($this->request->post['status_id']))
                     throw new Exception("Error requested data", 1);
                  $this->livehelp->setStatus($this->request->post['status_id']);
                  break;
               default:   // All operation taken from livehelp_chat.js  
                  $json = $this->livehelp->autoUpdate($this->request->post);
                  break;
            }
         }
         catch (Exception $e) {
            $json['fatal'] = $e;
         }
      }
      
      $this->response->setOutput(json_encode($json));
   }

   protected function initClass() {
      $this->load->library('livehelp/livehelp_operator');
      $livehelp = new Livehelp($this->registry);
      $this->registry->set('livehelp', $livehelp);
   }
}
?> 