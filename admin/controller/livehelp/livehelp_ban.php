<?php
class ControllerLivehelpLivehelpBan extends Controller {
   private $error = array();
   private $file_path = 'livehelp/livehelp_ban';
   
   public function index() {
      $this->language->load($this->file_path);
      $this->load->model($this->file_path);
      
      $this->document->setTitle($this->language->get('heading_title'));
      $this->getList();
   }
   
   public function insert() {
      $this->language->load($this->file_path);
      
      $this->document->setTitle($this->language->get('heading_title'));
      
      $this->load->model($this->file_path);
      
      if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
         $this->model_livehelp_livehelp_ban->addBan($this->request->post);
         
         $this->session->data['success'] = $this->language->get('text_success');
         
         $url = $this->getUrl(array(
            "filter_date_added",
            "filter_date_expired",
            "sort",
            "order",
            "page"
         ));
         
         $this->redirect($this->url->link($this->file_path, 'token=' . $this->session->data['token'] . $url, 'SSL'));
      }
      
      $this->getForm();
   }
   
   public function update() {
      $this->language->load($this->file_path);
      
      $this->document->setTitle($this->language->get('heading_title'));
      
      $this->load->model($this->file_path);
      
      if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
         $this->model_livehelp_livehelp_ban->editBan($this->request->get['ban_id'], $this->request->post);
         
         $this->session->data['success'] = $this->language->get('text_success');
         
         $url = $this->getUrl(array(
            "filter_date_added",
            "filter_date_expired",
            "sort",
            "order",
            "page"
         ));
         
         $this->redirect($this->url->link($this->file_path, 'token=' . $this->session->data['token'] . $url, 'SSL'));
      }
      
      $this->getForm();
   }
   
   public function delete() {
      $this->language->load($this->file_path);
      
      $this->document->setTitle($this->language->get('heading_title'));
      
      $this->load->model($this->file_path);
      
      if (isset($this->request->post['selected']) && $this->validateModify()) {
         
         foreach ($this->request->post['selected'] as $ban_id) {
            $this->model_livehelp_livehelp_ban->deleteBan($ban_id);
         }
         
         $this->session->data['success'] = $this->language->get('text_success');
         
         $url = $this->getUrl(array(
            "filter_date_added",
            "filter_date_expired",
            "sort",
            "order",
            "page"
         ));
         
         $this->redirect($this->url->link($this->file_path, 'token=' . $this->session->data['token'] . $url, 'SSL'));
      }
      
      $this->getList();
   }
   
   protected function getList() {
      if (isset($this->request->get['filter_date_expired'])) {
         $filter_date_expired = $this->request->get['filter_date_expired'];
      } else {
         $filter_date_expired = null;
      }
      
      if (isset($this->request->get['filter_date_added'])) {
         $filter_date_added = $this->request->get['filter_date_added'];
      } else {
         $filter_date_added = null;
      }
      
      if (isset($this->request->get['sort'])) {
         $sort = $this->request->get['sort'];
      } else {
         $sort = 'user_name';
      }
      
      if (isset($this->request->get['order'])) {
         $order = $this->request->get['order'];
      } else {
         $order = 'ASC';
      }
      
      if (isset($this->request->get['page'])) {
         $page = $this->request->get['page'];
      } else {
         $page = 1;
      }
      
      $url = $this->getUrl(array(
         "filter_date_added",
         "filter_date_expired",
         "sort",
         "order",
         "page"
      ));
      
      $this->data['breadcrumbs'] = array();
      
      $this->data['breadcrumbs'][] = array(
         'text' => $this->language->get('text_home'),
         'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
         'separator' => false
      );
      
      $this->data['breadcrumbs'][] = array(
         'text' => $this->language->get('heading_title'),
         'href' => $this->url->link($this->file_path, 'token=' . $this->session->data['token'] . $url, 'SSL'),
         'separator' => ' :: '
      );
      
      $this->data['heading_title'] = $this->language->get('heading_title');
      
      $this->data['column_user_name']    = $this->language->get('column_user_name');
      $this->data['column_date_added']   = $this->language->get('column_date_added');
      $this->data['column_date_expired'] = $this->language->get('column_date_expired');
      $this->data['column_ip']           = $this->language->get('column_ip');
      $this->data['column_action']       = $this->language->get('column_action');
      
      $this->data['text_forever']    = $this->language->get('text_forever');
      $this->data['text_no_results'] = $this->language->get('text_no_results');
      
      $this->data['button_insert'] = $this->language->get('button_insert');
      $this->data['button_delete'] = $this->language->get('button_delete');
      $this->data['button_filter'] = $this->language->get('button_filter');
      
      $this->data['insert'] = $this->url->link($this->file_path . '/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
      $this->data['delete'] = $this->url->link($this->file_path . '/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
      
      if (isset($this->error['warning'])) {
         $this->data['error_warning'] = $this->error['warning'];
      } else {
         $this->data['error_warning'] = '';
      }
      
      if (isset($this->session->data['success'])) {
         $this->data['success'] = $this->session->data['success'];
         
         unset($this->session->data['success']);
      } else {
         $this->data['success'] = '';
      }
      
      $data = array(
         'filter_date_added' => $filter_date_added,
         'filter_date_expired' => $filter_date_expired,
         'sort' => $sort,
         'order' => $order,
         'start' => ($page - 1) * $this->config->get('config_admin_limit'),
         'limit' => $this->config->get('config_admin_limit')
      );
      
      $this->data['banned_users'] = array();
      
      $result_total = $this->model_livehelp_livehelp_ban->getTotalBans($data);
      
      $results = $this->model_livehelp_livehelp_ban->getBans($data);
      
      foreach ($results as $result) {
         $action = array();
         
         $action[] = array(
            'text' => $this->language->get('text_edit'),
            'href' => $this->url->link($this->file_path . '/update', 'token=' . $this->session->data['token'] . '&ban_id=' . $result['ban_id'] . $url, 'SSL')
         );
         
         $this->data['banned_users'][] = array(
            'ban_id' => $result['ban_id'],
            'user_name' => $result['user_name'],
            'date_added' => date($this->language->get('date_format_short'), $result['date_added']),
            'date_expired' => ($result['date_expired'] > 0 ? date($this->language->get('date_format_short'), $result['date_expired']) : $this->language->get('text_forever')),
            'ip' => $result['ip'],
            'selected' => isset($this->request->post['selected']) && in_array($result['ban_id'], $this->request->post['selected']),
            'action' => $action
         );
      }
      
      $url = $this->getUrl(array(
         "filter_date_added",
         "filter_date_expired",
         "page"
      ));
      
      if ($order == 'ASC') {
         $url .= '&order=DESC';
      } else {
         $url .= '&order=ASC';
      }
      
      $this->data['sort_user_name']    = $this->url->link($this->file_path, 'token=' . $this->session->data['token'] . '&sort=user_name' . $url, 'SSL');
      $this->data['sort_date_added']   = $this->url->link($this->file_path, 'token=' . $this->session->data['token'] . '&sort=date_added' . $url, 'SSL');
      $this->data['sort_date_expired'] = $this->url->link($this->file_path, 'token=' . $this->session->data['token'] . '&sort=date_expired' . $url, 'SSL');
      $this->data['sort_ip']           = $this->url->link($this->file_path, 'token=' . $this->session->data['token'] . '&sort=ip' . $url, 'SSL');
      
      $url = $this->getUrl(array(
         "filter_date_added",
         "filter_date_expired",
         "sort",
         "order"
      ));
      
      $pagination        = new Pagination();
      $pagination->total = $result_total;
      $pagination->page  = $page;
      $pagination->limit = $this->config->get('config_admin_limit');
      $pagination->text  = $this->language->get('text_pagination');
      $pagination->url   = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
      
      $this->data['pagination'] = $pagination->render();
      
      $this->data['filter_date_added']   = $filter_date_added;
      $this->data['filter_date_expired'] = $filter_date_expired;
      
      $this->data['sort']  = $sort;
      $this->data['order'] = $order;
      
      $this->data['token'] = $this->session->data['token'];
      
      $this->template = 'livehelp/livehelp_ban_list.tpl';
      $this->children = array(
         'common/header',
         'common/footer'
      );
      
      $this->response->setOutput($this->render());
   }
   
   protected function getForm() {
      $this->data['heading_title'] = $this->language->get('heading_title');
      
      $this->data['text_enabled']  = $this->language->get('text_enabled');
      $this->data['text_disabled'] = $this->language->get('text_disabled');
      $this->data['text_yes']      = $this->language->get('text_yes');
      $this->data['text_no']       = $this->language->get('text_no');
      $this->data['text_select']   = $this->language->get('text_select');
      
      $this->data['entry_user_name'] = $this->language->get('entry_user_name');
      $this->data['entry_comment']   = $this->language->get('entry_comment');
      $this->data['entry_expired']   = $this->language->get('entry_expired');
      $this->data['entry_ip']        = $this->language->get('entry_ip');
      
      $this->data['title_expired'] = $this->language->get('title_expired');
      
      $this->data['button_save']   = $this->language->get('button_save');
      $this->data['button_cancel'] = $this->language->get('button_cancel');
      
      if (isset($this->error['warning'])) {
         $this->data['error_warning'] = $this->error['warning'];
      } else {
         $this->data['error_warning'] = '';
      }
      
      if (isset($this->error['user_name'])) {
         $this->data['error_user_name'] = $this->error['user_name'];
      } else {
         $this->data['error_user_name'] = '';
      }
      
      if (isset($this->error['ip'])) {
         $this->data['error_ip'] = $this->error['ip'];
      } else {
         $this->data['error_ip'] = '';
      }
      
      $url = $this->getUrl(array(
         "filter_date_added",
         "filter_date_expired",
         "sort",
         "order",
         "page"
      ));
      
      $this->data['breadcrumbs'] = array();
      
      $this->data['breadcrumbs'][] = array(
         'text' => $this->language->get('text_home'),
         'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
         'separator' => false
      );
      
      $this->data['breadcrumbs'][] = array(
         'text' => $this->language->get('heading_title'),
         'href' => $this->url->link($this->file_path, 'token=' . $this->session->data['token'] . $url, 'SSL'),
         'separator' => ' :: '
      );
      
      if (!isset($this->request->get['ban_id'])) {
         $this->data['action'] = $this->url->link('livehelp/livehelp_ban/insert', 'token=' . $this->session->data['token'], 'SSL');
      } else {
         $this->data['action'] = $this->url->link('livehelp/livehelp_ban/update', 'token=' . $this->session->data['token'] . '&ban_id=' . $this->request->get['ban_id'], 'SSL');
      }
      
      $this->data['cancel'] = $this->url->link($this->file_path, 'token=' . $this->session->data['token'] . $url, 'SSL');
      
      if (isset($this->request->get['ban_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
         $ban_info = $this->model_livehelp_livehelp_ban->getBan($this->request->get['ban_id']);
      }
      
      if (isset($this->request->post['user_name'])) {
         $this->data['user_name'] = $this->request->post['user_name'];
      } elseif (!empty($ban_info)) {
         $this->data['user_name'] = $ban_info['user_name'];
      } else {
         $this->data['user_name'] = '';
      }
      
      if (isset($this->request->post['comment'])) {
         $this->data['comment'] = $this->request->post['comment'];
      } elseif (!empty($ban_info)) {
         $this->data['comment'] = $ban_info['comment'];
      } else {
         $this->data['comment'] = '';
      }
      
      if (isset($this->request->post['date_expired'])) {
         $this->data['date_expired'] = $this->request->post['date_expired'];
      } elseif (!empty($ban_info)) {
         $this->data['date_expired'] = ($ban_info['date_expired'] != -1 ? date("Y-m-d H:i:s", $ban_info['date_expired']) : "");
      } else {
         $this->data['date_expired'] = '';
      }
      
      if (isset($this->request->post['ip'])) {
         $this->data['ip'] = $this->request->post['ip'];
      } elseif (!empty($ban_info)) {
         $this->data['ip'] = $ban_info['ip'];
      } else {
         $this->data['ip'] = '';
      }
      
      $this->template = 'livehelp/livehelp_ban_form.tpl';
      $this->children = array(
         'common/header',
         'common/footer'
      );
      
      $this->response->setOutput($this->render());
   }
   
   /*
   VALIDATION
   */
   protected function validateModify() {
      if (!$this->user->hasPermission('modify', $this->file_path)) {
         $this->error['warning'] = $this->language->get('error_permission');
      }
      
      return !$this->error;
   }
   
   protected function validateForm() {
      $this->validateModify();
      
      if ((utf8_strlen($this->request->post['user_name']) < 3) || (utf8_strlen($this->request->post['user_name']) > 40)) {
         $this->error['user_name'] = $this->language->get('error_user_name');
      }
      
      if ((utf8_strlen($this->request->post['ip']) < 3) || (utf8_strlen($this->request->post['ip']) > 128)) {
         $this->error['ip'] = $this->language->get('error_ip');
      }
      
      return !$this->error;
   }
   
   protected function getUrl($var) {
      $url = '';
      
      if (is_string($var)) {
         if (isset($this->request->get[$var]))
            $url .= "&" . $var . "=" . $this->request->get[$var];
      } else if (is_array($var)) {
         foreach ($var as $value) {
            if (isset($this->request->get[$value]))
               $url .= "&" . $value . "=" . $this->request->get[$value];
         }
      }
      return $url;
   }
}
?>