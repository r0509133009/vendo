<?php
class ControllerLivehelpLivehelpUser extends Controller {
   private $error = array();
   private $file_path = 'livehelp/livehelp_user';
   
   public function index() {
      $this->language->load($this->file_path);
      $this->load->model($this->file_path);
      
      $this->document->setTitle($this->language->get('heading_title'));
      $this->getList();
   }
   
   public function insert() {
      $this->language->load($this->file_path);
      
      $this->document->setTitle($this->language->get('heading_title'));
      
      $this->load->model($this->file_path);
      
      if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
         $this->model_livehelp_livehelp_user->addOperator($this->request->post);
         
         $this->session->data['success'] = $this->language->get('text_success');
         
         $this->redirect($this->url->link($this->file_path, 'token=' . $this->session->data['token'], 'SSL'));
      }
      
      $this->getForm();
   }
   
   public function update() {
      $this->language->load($this->file_path);
      
      $this->document->setTitle($this->language->get('heading_title'));
      
      $this->load->model($this->file_path);
      
      if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
         $this->model_livehelp_livehelp_user->editOperator($this->request->get['operator_id'], $this->request->post);
         
         $this->session->data['success'] = $this->language->get('text_success');
         
         $this->redirect($this->url->link($this->file_path, 'token=' . $this->session->data['token'], 'SSL'));
      }
      
      $this->getForm();
   }
   
   public function delete() {
      $this->language->load($this->file_path);
      
      $this->document->setTitle($this->language->get('heading_title'));
      
      $this->load->model($this->file_path);
      
      if (isset($this->request->post['selected']) && $this->validateModify()) {
         
         foreach ($this->request->post['selected'] as $operator_id) {
            $this->model_livehelp_livehelp_user->deleteOperator($operator_id);
         }
         
         $this->session->data['success'] = $this->language->get('text_success');
         
         $url = $this->getUrl(array(
            "sort",
            "order",
            "page"
         ));
         
         $this->redirect($this->url->link($this->file_path, 'token=' . $this->session->data['token'] . $url, 'SSL'));
      }
      
      $this->getList();
   }
     
   protected function getList() {
      if (isset($this->request->get['sort'])) {
         $sort = $this->request->get['sort'];
      } else {
         $sort = 'u.username';
      }
      
      if (isset($this->request->get['order'])) {
         $order = $this->request->get['order'];
      } else {
         $order = 'ASC';
      }
      
      if (isset($this->request->get['page'])) {
         $page = $this->request->get['page'];
      } else {
         $page = 1;
      }
      
      $url = $this->getUrl(array(
         "filter_date_added",
         "filter_date_expired",
         "sort",
         "order",
         "page"
      ));
      
      $this->data['breadcrumbs'] = array();
      
      $this->data['breadcrumbs'][] = array(
         'text' => $this->language->get('text_home'),
         'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
         'separator' => false
      );
      
      $this->data['breadcrumbs'][] = array(
         'text' => $this->language->get('heading_title'),
         'href' => $this->url->link($this->file_path, 'token=' . $this->session->data['token'] . $url, 'SSL'),
         'separator' => ' :: '
      );
      
      $this->data['heading_title'] = $this->language->get('heading_title');
      
      $this->data['text_no_results'] = $this->language->get('text_no_results');
      
      $this->data['column_username']   = $this->language->get('column_username');
      $this->data['column_action']     = $this->language->get('column_action');
      $this->data['column_date_added'] = $this->language->get('column_date_added');
      $this->data['column_status']     = $this->language->get('column_status');
      $this->data['column_name']       = $this->language->get('column_name');
      
      $this->data['button_insert'] = $this->language->get('button_insert');
      $this->data['button_delete'] = $this->language->get('button_delete');
      
      $this->data['insert'] = $this->url->link($this->file_path . '/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
      $this->data['delete'] = $this->url->link($this->file_path . '/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
      
      if (isset($this->error['warning'])) {
         $this->data['error_warning'] = $this->error['warning'];
      } else {
         $this->data['error_warning'] = '';
      }
      
      if (isset($this->session->data['success'])) {
         $this->data['success'] = $this->session->data['success'];
         
         unset($this->session->data['success']);
      } else {
         $this->data['success'] = '';
      }
      
      $data = array(
         'sort' => $sort,
         'order' => $order,
         'start' => ($page - 1) * $this->config->get('config_admin_limit'),
         'limit' => $this->config->get('config_admin_limit')
      );
      
      $this->data['users'] = array();
      
      $result_total = $this->model_livehelp_livehelp_user->getTotalOperators($data);
      
      $results = $this->model_livehelp_livehelp_user->getOperators($data);
      
      foreach ($results as $result) {
         $action = array();
         
         $action[] = array(
            'text' => $this->language->get('text_edit'),
            'href' => $this->url->link($this->file_path . '/update', 'token=' . $this->session->data['token'] . '&operator_id=' . $result['operator_id'] . $url, 'SSL')
         );
         
         $this->data['users'][] = array(
            'operator_id' => $result['operator_id'],
            'name' => $result['name'],
            'username' => '<a href="' . $this->url->link('user/user/update', 'token=' . $this->session->data['token'] . '&user_id=' . $result['user_id']) . '">' . $result['username'] . '</a>',
            'description' => $result['description'],
            'date_added' => $result['date_added'],
            'status' => $result['status'],
            'selected' => isset($this->request->post['selected']) && in_array($result['ban_id'], $this->request->post['selected']),
            'action' => $action
         );
      }
      
      $url = $this->getUrl(array(
         "page"
      ));
      
      if ($order == 'ASC') {
         $url .= '&order=DESC';
      } else {
         $url .= '&order=ASC';
      }
      
      $this->data['sort_name']       = $this->url->link($this->file_path, 'token=' . $this->session->data['token'] . '&sort=lo.name' . $url, 'SSL');
      $this->data['sort_username']   = $this->url->link($this->file_path, 'token=' . $this->session->data['token'] . '&sort=u.username' . $url, 'SSL');
      $this->data['sort_date_added'] = $this->url->link($this->file_path, 'token=' . $this->session->data['token'] . '&sort=lo.date_added' . $url, 'SSL');
      $this->data['sort_status']     = $this->url->link($this->file_path, 'token=' . $this->session->data['token'] . '&sort=lo.status' . $url, 'SSL');
      
      $url = $this->getUrl(array(
         "sort",
         "order"
      ));
      
      $pagination        = new Pagination();
      $pagination->total = $result_total;
      $pagination->page  = $page;
      $pagination->limit = $this->config->get('config_admin_limit');
      $pagination->text  = $this->language->get('text_pagination');
      $pagination->url   = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
      
      $this->data['pagination'] = $pagination->render();
      
      $this->data['sort']  = $sort;
      $this->data['order'] = $order;
      
      $this->template = 'livehelp/livehelp_user_list.tpl';
      $this->children = array(
         'common/header',
         'common/footer'
      );
      
      $this->response->setOutput($this->render());
   }
   
   protected function getForm() {
      $this->data['heading_title'] = $this->language->get('heading_title');
      
      $this->data['text_enabled']              = $this->language->get('text_enabled');
      $this->data['text_disabled']             = $this->language->get('text_disabled');
      $this->data['text_yes']                  = $this->language->get('text_yes');
      $this->data['text_no']                   = $this->language->get('text_no');
      $this->data['text_select']               = $this->language->get('text_select');
      $this->data['text_accepting_html']       = $this->language->get('text_accepting_html');
      $this->data['entry_status']              = $this->language->get('entry_status');
      $this->data['entry_name']                = $this->language->get('entry_name');
      $this->data['entry_user']                = $this->language->get('entry_user');
      $this->data['entry_description']         = $this->language->get('entry_description');
      $this->data['text_operator_description'] = $this->language->get('text_operator_description');
      
      $this->data['button_save']   = $this->language->get('button_save');
      $this->data['button_cancel'] = $this->language->get('button_cancel');
      
      if (isset($this->error['warning'])) {
         $this->data['error_warning'] = $this->error['warning'];
      } else {
         $this->data['error_warning'] = '';
      }
      
      if (isset($this->error['user'])) {
         $this->data['error_user'] = $this->error['user'];
      } else {
         $this->data['error_user'] = '';
      }
      
      if (isset($this->error['name'])) {
         $this->data['error_name'] = $this->error['name'];
      } else {
         $this->data['error_name'] = '';
      }
      
      $url = $this->getUrl(array(
         "sort",
         "order",
         "page"
      ));
      
      $this->data['breadcrumbs'] = array();
      
      $this->data['breadcrumbs'][] = array(
         'text' => $this->language->get('text_home'),
         'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
         'separator' => false
      );
      
      $this->data['breadcrumbs'][] = array(
         'text' => $this->language->get('heading_title'),
         'href' => $this->url->link($this->file_path, 'token=' . $this->session->data['token'] . $url, 'SSL'),
         'separator' => ' :: '
      );
      
      if (!isset($this->request->get['operator_id'])) {
         $this->data['action'] = $this->url->link('livehelp/livehelp_user/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
      } else {
         $this->data['action'] = $this->url->link('livehelp/livehelp_user/update', 'token=' . $this->session->data['token'] . '&operator_id=' . $this->request->get['operator_id'] . $url, 'SSL');
      }
      
      $this->data['cancel'] = $this->url->link($this->file_path, 'token=' . $this->session->data['token'] . $url, 'SSL');
      
      if (isset($this->request->get['operator_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
         $operator_info = $this->model_livehelp_livehelp_user->getOperator($this->request->get['operator_id']);
      }
      
      if (isset($this->request->post['user_id'])) {
         $this->data['user_id'] = $this->request->post['user_id'];
      } elseif (!empty($operator_info)) {
         $this->data['user_id'] = $operator_info['user_id'];
      } else {
         $this->data['user_id'] = '';
      }
      
      if (isset($this->request->post['name'])) {
         $this->data['name'] = $this->request->post['name'];
      } elseif (!empty($operator_info)) {
         $this->data['name'] = $operator_info['name'];
      } else {
         $this->data['name'] = '';
      }
      
      if (isset($this->request->post['status'])) {
         $this->data['status'] = $this->request->post['status'];
      } elseif (!empty($operator_info)) {
         $this->data['status'] = $operator_info['status'];
      } else {
         $this->data['status'] = '';
      }
      
      if (isset($this->request->post['description'])) {
         $this->data['description'] = $this->request->post['description'];
      } elseif (!empty($operator_info)) {
         $this->data['description'] = $operator_info['description'];
      } else {
         $this->data['description'] = '';
      }
      
      // Users
      $this->load->model('user/user');
      $this->data['users'] = $this->model_user_user->getUsers();
      
      $this->template = 'livehelp/livehelp_user_form.tpl';
      $this->children = array(
         'common/header',
         'common/footer'
      );
      
      $this->response->setOutput($this->render());
   }
   
   /*
   VALIDATION
   */
   protected function validateModify() {
      if (!$this->user->hasPermission('modify', $this->file_path)) {
         $this->error['warning'] = $this->language->get('error_permission');
      }
      
      return !$this->error;
   }
   
   protected function validateForm() {
      $this->validateModify();
      
      if (isset($this->request->get['operator_id'])) {
         $operator_id = $this->request->get['operator_id'];
      } else {
         $operator_id = 0;
      }
      
      if (empty($this->request->post['user_id'])) {
         $this->error['user'] = $this->language->get('error_user');
      } else {
         $user_match = $this->model_livehelp_livehelp_user->getOperatorByUserId($this->request->post['user_id'], $operator_id);
         
         if ($user_match) {
            $this->error['user'] = $this->language->get('error_user_exists');
         }
      }
      
      if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 64)) {
         $this->error['name'] = $this->language->get('error_name');
      }
      
      return !$this->error;
   }
   
   protected function getUrl($var) {
      $url = '';
      
      if (is_string($var)) {
         if (isset($this->request->get[$var]))
            $url .= "&" . $var . "=" . $this->request->get[$var];
      } else if (is_array($var)) {
         foreach ($var as $value) {
            if (isset($this->request->get[$value]))
               $url .= "&" . $value . "=" . $this->request->get[$value];
         }
      }
      return $url;
   }
}
?> 