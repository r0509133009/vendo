<?php 
class ControllerFeedGoogleMerchantCenter extends Controller {
	private $error = array(); 

	//public function uninstall() {
		//$this->load->model('feed/google_merchant_center');
		//$this->model_feed_google_merchant_center->uninstall();

	//}
	public function install() {
		$this->load->model('feed/google_merchant_center');
		$this->model_feed_google_merchant_center->install();
	}

	public function index() {

		$this->load->model('feed/google_merchant_center');

		//$this->model_feed_google_merchant_center->install();

		$this->language->load('feed/google_merchant_center');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('google_merchant_center', $this->request->post);

			$this->model_feed_google_merchant_center->saveSetting($this->request->post);		

			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect($this->url->link('extension/feed', 'token=' . $this->session->data['token'], 'SSL'));
		}

		

		$this->data['google_merchant_base_category'] = array();

		$google_base = $this->model_feed_google_merchant_center->getBaseCategory();

		foreach ($google_base as $base) {
			$this->data['google_merchant_base_category'][] = array (
				'taxonomy_id' => $base['taxonomy_id'],				
				'status' => $base['status'],
				'name' => $base['name']
			);
		}


		$this->data['merchant_center_apparel_id'] = array();

		$apparel_id = $this->model_feed_google_merchant_center->getOptionID();

		foreach ($apparel_id as $apparel) {
			$this->data['merchant_center_option'][] = array (
				'option_id' => $apparel['option_id'],
				'name' => $apparel['name']
			);
		}


		if (isset($this->request->post['google_merchant_center_attribute'])) {
			$this->data['google_merchant_center_attribute'] = $this->request->post['google_merchant_center_attribute'];
		} else {
			$this->data['google_merchant_center_attribute'] = $this->config->get('google_merchant_center_attribute');
		}

		if (isset($this->request->post['google_merchant_center_attribute_type'])) {
			$this->data['google_merchant_center_attribute_type'] = $this->request->post['google_merchant_center_attribute_type'];
		} else {
			$this->data['google_merchant_center_attribute_type'] = $this->config->get('google_merchant_center_attribute_type');
		}

		$attribute_id=$this->model_feed_google_merchant_center->getAttributes();
		$this->data['merchant_center_attributes'][] = array (
			'attribute_id' => '-1',
			'name' => $this->language->get('entry_google_merchant_center_attribute_product')
		);
		$this->data['merchant_center_attributes_type'][] = array (
			'attribute_id' => '-1',
			'name' => $this->language->get('entry_google_merchant_center_attribute_product_type')
		);
		foreach ($attribute_id as $attribute) {
			$this->data['merchant_center_attributes'][] = array (
				'attribute_id' => $attribute['attribute_id'],
				'name' => $attribute['name']
			);
			$this->data['merchant_center_attributes_type'][] = array (
				'attribute_id' => $attribute['attribute_id'],
				'name' => $attribute['name']
			);
		}

		$this->data['entry_google_merchant_base'] = $this->language->get('entry_google_merchant_base');
		$this->data['entry_google_merchant_center_attribute'] = $this->language->get('entry_google_merchant_center_attribute');
		$this->data['entry_google_merchant_center_attribute_type'] = $this->language->get('entry_google_merchant_center_attribute_type');
		$this->data['entry_google_merchant_center_option'] = $this->language->get('entry_google_merchant_center_option');
		$this->data['entry_google_merchant_center_availability'] = $this->language->get('entry_google_merchant_center_availability');
		$this->data['entry_google_merchant_center_shipping_flat'] = $this->language->get('entry_google_merchant_center_shipping_flat');
		$this->data['entry_google_merchant_center_description'] = $this->language->get('entry_google_merchant_center_description');
		$this->data['entry_google_merchant_center_description_html'] = $this->language->get('entry_google_merchant_center_description_html');
		$this->data['entry_feed_merchant_center_id1'] = $this->language->get('entry_feed_merchant_center_id1');

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');

		$this->data['entry_status'] = $this->language->get('entry_status');

		$this->data['entry_file'] = $this->language->get('entry_file');

		$this->data['entry_data_feed'] = $this->language->get('entry_data_feed');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_feed'),
			'href'      => $this->url->link('extension/feed', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('feed/google_merchant_center', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->data['action'] = $this->url->link('feed/google_merchant_center', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['cancel'] = $this->url->link('extension/feed', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->post['google_merchant_center_status'])) {
			$this->data['google_merchant_center_status'] = $this->request->post['google_merchant_center_status'];
		} else {
			$this->data['google_merchant_center_status'] = $this->config->get('google_merchant_center_status');
		}

		if (isset($this->request->post['google_merchant_center_description'])) {
			$this->data['google_merchant_center_description'] = $this->request->post['google_merchant_center_description'];
		} else {
			$this->data['google_merchant_center_description'] = $this->config->get('google_merchant_center_description');
		}

		if (isset($this->request->post['google_merchant_center_description_html'])) {
			$this->data['google_merchant_center_description_html'] = $this->request->post['google_merchant_center_description_html'];
		} else {
			$this->data['google_merchant_center_description_html'] = $this->config->get('google_merchant_center_description_html');
		}

		if (isset($this->request->post['google_merchant_center_file'])) {
			$this->data['google_merchant_center_file'] = $this->request->post['google_merchant_center_file'];
		} else {
			$this->data['google_merchant_center_file'] = $this->config->get('google_merchant_center_file');
		}

		if (isset($this->request->post['google_merchant_center_availability'])) {
			$this->data['google_merchant_center_availability'] = $this->request->post['google_merchant_center_availability'];
		} else {
			$this->data['google_merchant_center_availability'] = $this->config->get('google_merchant_center_availability');
		}

		if (isset($this->request->post['google_merchant_center_option'])) {
			$this->data['google_merchant_center_option'] = $this->request->post['google_merchant_center_option'];
		} else {
			$this->data['google_merchant_center_option'] = $this->config->get('google_merchant_center_option');
		}
		if (isset($this->request->post['google_merchant_center_shipping_flat'])) {
			$this->data['google_merchant_center_shipping_flat'] = $this->request->post['google_merchant_center_shipping_flat'];
		} else {
			$this->data['google_merchant_center_shipping_flat'] = $this->config->get('google_merchant_center_shipping_flat');
		}

		if (isset($this->request->post['feed_merchant_center_id1'])) {
			$this->data['feed_merchant_center_id1'] = $this->request->post['feed_merchant_center_id1'];
		} elseif ($this->config->get('feed_merchant_center_id1')!='') {
			$this->data['feed_merchant_center_id1'] = $this->config->get('feed_merchant_center_id1');
		} else {
			$this->data['feed_merchant_center_id1'] = 'product_id';
		}

		$this->data['data_feed'] = HTTP_CATALOG . 'index.php?route=feed/google_merchant_center&lang='.$this->config->get('config_language').'&curr='.$this->config->get('config_currency').'&store='.(int)$this->config->get('config_store_id');

		$this->template = 'feed/google_merchant_center.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	} 

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'feed/google_merchant_center')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}	
}
?>
