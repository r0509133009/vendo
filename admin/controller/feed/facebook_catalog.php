<?php 
class ControllerFeedFacebookCatalog extends Controller {
	private $error = array(); 

	public function index() {
		$this->language->load('feed/facebook_catalog');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		$this->load->model('feed/google_merchant_center');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('facebook_catalog', $this->request->post);				

			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect($this->url->link('extension/feed', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');

		$this->data['entry_status'] = $this->language->get('entry_status');

		$this->data['entry_file'] = $this->language->get('entry_file');

		$this->data['entry_data_feed'] = $this->language->get('entry_data_feed');

		$this->data['entry_facebook_catalog_availability'] = $this->language->get('entry_facebook_catalog_availability');
		$this->data['entry_facebook_catalog_description'] = $this->language->get('entry_facebook_catalog_description');
		$this->data['entry_facebook_catalog_description_html'] = $this->language->get('entry_facebook_catalog_description_html');
		$this->data['entry_feed_facebook_id1'] = $this->language->get('entry_feed_facebook_id1');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['tab_general'] = $this->language->get('tab_general');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_feed'),
			'href'      => $this->url->link('extension/feed', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('feed/facebook_catalog', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->data['action'] = $this->url->link('feed/facebook_catalog', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['cancel'] = $this->url->link('extension/feed', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->post['facebook_catalog_status'])) {
			$this->data['facebook_catalog_status'] = $this->request->post['facebook_catalog_status'];
		} else {
			$this->data['facebook_catalog_status'] = $this->config->get('facebook_catalog_status');
		}

		if (isset($this->request->post['facebook_catalog_file'])) {
			$this->data['facebook_catalog_file'] = $this->request->post['facebook_catalog_file'];
		} else {
			$this->data['facebook_catalog_file'] = $this->config->get('facebook_catalog_file');
		}

		if (isset($this->request->post['facebook_catalog_availability'])) {
			$this->data['facebook_catalog_availability'] = $this->request->post['facebook_catalog_availability'];
		} else {
			$this->data['facebook_catalog_availability'] = $this->config->get('facebook_catalog_availability');
		}

		if (isset($this->request->post['facebook_catalog_description'])) {
			$this->data['facebook_catalog_description'] = $this->request->post['facebook_catalog_description'];
		} else {
			$this->data['facebook_catalog_description'] = $this->config->get('facebook_catalog_description');
		}

		if (isset($this->request->post['facebook_catalog_description_html'])) {
			$this->data['facebook_catalog_description_html'] = $this->request->post['facebook_catalog_description_html'];
		} else {
			$this->data['facebook_catalog_description_html'] = $this->config->get('facebook_catalog_description_html');
		}

		if (isset($this->request->post['facebook_attribute_type'])) {
			$this->data['facebook_attribute_type'] = $this->request->post['facebook_attribute_type'];
		} else {
			$this->data['facebook_attribute_type'] = $this->config->get('facebook_attribute_type');
		}
		
		if (isset($this->request->post['feed_facebook_id1'])) {
			$this->data['feed_facebook_id1'] = $this->request->post['feed_facebook_id1'];
		} elseif ($this->config->get('feed_facebook_id1')!='') {
			$this->data['feed_facebook_id1'] = $this->config->get('feed_facebook_id1');
		} else {
			$this->data['feed_facebook_id1'] = 'product_id';
		}

		$attribute_id=$this->model_feed_google_merchant_center->getAttributes();
		$this->data['facebook_attributes_type'][] = array (
			'attribute_id' => '-1',
			'name' => $this->language->get('entry_facebook_attribute_product_type')
		);

		foreach ($attribute_id as $attribute) {
			$this->data['facebook_attributes_type'][] = array (
				'attribute_id' => $attribute['attribute_id'],
				'name' => $attribute['name']
			);
		}
		$this->data['entry_facebook_attribute_type'] = $this->language->get('entry_facebook_attribute_type');

		$this->data['data_feed'] = HTTP_CATALOG . 'index.php?route=feed/facebook_catalog&lang='.$this->config->get('config_language').'&curr='.$this->config->get('config_currency').'&store='.(int)$this->config->get('config_store_id');

		$this->template = 'feed/facebook_catalog.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	} 

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'feed/facebook_catalog')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}	
}
?>
