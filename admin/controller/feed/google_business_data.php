<?php 
class ControllerFeedGoogleBusinessData extends Controller {
	private $error = array(); 

	public function index() {
		$this->language->load('feed/google_business_data');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('google_business_data', $this->request->post);				

			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect($this->url->link('extension/feed', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');

		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_file'] = $this->language->get('entry_file');
		$this->data['entry_data_feed'] = $this->language->get('entry_data_feed');
		$this->data['entry_google_business_data_description'] = $this->language->get('entry_google_business_data_description');
		$this->data['entry_google_business_data_description_html'] = $this->language->get('entry_google_business_data_description_html');
		$this->data['entry_feed_business_data_id1'] = $this->language->get('entry_feed_business_data_id1');
		$this->data['entry_feed_business_data_id2'] = $this->language->get('entry_feed_business_data_id2');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['tab_general'] = $this->language->get('tab_general');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_feed'),
			'href'      => $this->url->link('extension/feed', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('feed/google_business_data', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->data['action'] = $this->url->link('feed/google_business_data', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['cancel'] = $this->url->link('extension/feed', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->post['google_business_data_status'])) {
			$this->data['google_business_data_status'] = $this->request->post['google_business_data_status'];
		} else {
			$this->data['google_business_data_status'] = $this->config->get('google_business_data_status');
		}

		if (isset($this->request->post['google_business_data_file'])) {
			$this->data['google_business_data_file'] = $this->request->post['google_business_data_file'];
		} else {
			$this->data['google_business_data_file'] = $this->config->get('google_business_data_file');
		}
		if (isset($this->request->post['google_business_data_description'])) {
			$this->data['google_business_data_description'] = $this->request->post['google_business_data_description'];
		} else {
			$this->data['google_business_data_description'] = $this->config->get('google_business_data_description');
		}

		if (isset($this->request->post['google_business_data_description_html'])) {
			$this->data['google_business_data_description_html'] = $this->request->post['google_business_data_description_html'];
		} else {
			$this->data['google_business_data_description_html'] = $this->config->get('google_business_data_description_html');
		}
		
		if (isset($this->request->post['feed_business_data_id1'])) {
			$this->data['feed_business_data_id1'] = $this->request->post['feed_business_data_id1'];
		} elseif ($this->config->get('feed_business_data_id1')!='') {
			$this->data['feed_business_data_id1'] = $this->config->get('feed_business_data_id1');
		} else {
			$this->data['feed_business_data_id1'] = 'model';
		}
		if (isset($this->request->post['feed_business_data_id2'])) {
			$this->data['feed_business_data_id2'] = $this->request->post['feed_business_data_id2'];
		} elseif ($this->config->get('feed_business_data_id2')!='') {
			$this->data['feed_business_data_id2'] = $this->config->get('feed_business_data_id2');
		} else {
			$this->data['feed_business_data_id2'] = '';
		}

		$this->data['data_feed'] = HTTP_CATALOG . 'index.php?route=feed/google_business_data&lang='.$this->config->get('config_language').'&curr='.$this->config->get('config_currency').'&store='.(int)$this->config->get('config_store_id');

		$this->template = 'feed/google_business_data.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	} 

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'feed/google_business_data')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}	
}
?>
