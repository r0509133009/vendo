<?php  
class ControllerCatalogGrsnippet extends Controller {  
	private $error = array();
   
  	public function index() {
    	$this->load->language('catalog/grsnippet');       
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		 
    	$this->document->setTitle($this->language->get('heading_title'));
    	$this->data['heading_title'] = $this->language->get('heading_title');
    	$this->getForm();
  	}
  	
    public function insert() {    
		$this->load->language('catalog/grsnippet');
		$this->load->model('setting/setting');
		$this->document->setTitle($this->language->get('heading_title'));
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$this->model_setting_setting->editSetting('grsnippet', $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$this->redirect($this->url->link('catalog/grsnippet', 'token=' . $this->session->data['token'], 'SSL'));
		}
		$this->getForm();
	}
	
	private function getForm() {
		$this->load->language('catalog/grsnippet');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled'); 
		$this->data['text_about'] = $this->language->get('text_about'); 
		$this->data['business_type'] = $this->language->get('business_type');
		$this->data['payment_method'] = $this->language->get('payment_method');
		$this->data['tab_achieve'] = $this->language->get('tab_achieve');
		$this->data['tab_verify'] = $this->language->get('tab_verify');
		$this->data['tab_company'] = $this->language->get('tab_company');
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel'); 
		$this->data['entry_google_status'] = $this->language->get('entry_google_status'); 
		$this->data['entry_facebook_status'] = $this->language->get('entry_facebook_status'); 
		$this->data['entry_twitter_status'] = $this->language->get('entry_twitter_status'); 
		$this->data['text_status'] = $this->language->get('text_status');
		$this->data['grsnippet'] = $this->language->get('grsnippet');
		$this->data['store_help'] = $this->language->get('store_help');
		$this->data['verify_help'] = $this->language->get('verify_help');
		$this->data['achieve_help'] = $this->language->get('achieve_help');
		$this->data['text_twitterusername'] = $this->language->get('text_twitterusername');
		$this->data['text_googlepageid'] = $this->language->get('text_googlepageid');
		$this->data['text_facebookadminid'] = $this->language->get('text_facebookadminid');
		$this->data['text_googlepageid_help'] = $this->language->get('text_googlepageid_help');
		$this->data['text_facebookadminid_help'] = $this->language->get('text_facebookadminid_help');
		$this->data['text_twitterusername_help'] = $this->language->get('text_twitterusername_help');
		$this->data['check_impact'] = $this->language->get('check_impact');
		$this->data['company_name'] = $this->language->get('company_name');
		$this->data['country_name'] = $this->language->get('country_name');
		$this->data['postal_code'] = $this->language->get('postal_code'); 
		$this->data['locality_name'] = $this->language->get('locality_name'); 
		$this->data['street_address'] = $this->language->get('street_address');
		$this->data['telephone_number'] = $this->language->get('telephone_number');

		$this->data['token'] = $this->session->data['token'];
		$this->data['types'] = array('Sell','LeaseOut','Repair','Maintain','ConstructionInstallation','ProvideService','Dispose');
		$this->data['payments'] = array('ByBankTransferInAdvance','ByInvoice','Cash','CheckInAdvance','COD','DirectDebit','GoogleCheckOut','PayPal','PaySwarm','AmericanExpress','DinersClub','Discover','JCB','MasterCard','VISA');
		$this->data['deliveries'] = array('DeliveryModeDirectDownload','DeliveryModeFreight','DeliveryModeMail','DeliveryModeOwnFleet','DeliveryModePickUp','DHL','FederalExpress','UPS');

		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'text'      => $this->language->get('text_home'),
			'separator' => FALSE
		);

		$this->data['breadcrumbs'][] = array(
			'href'      => $this->url->link('catalog/grsnippet', 'token=' . $this->session->data['token'], 'SSL'),
			'text'      => $this->language->get('heading_title'),
			'separator' => ' :: '
		);

      	$this->data['action'] = $this->url->link('catalog/grsnippet/insert', 'token=' . $this->session->data['token'], 'SSL');
		
	 	if (isset($this->request->post['config_payment_methods'])) {
			$this->data['payment_methods'] = $this->request->post['config_payment_methods'];
		} elseif ($this->config->get('config_payment_methods')) {
			$this->data['payment_methods'] = $this->config->get('config_payment_methods');
		} else {
			$this->data['payment_methods'] = array();
		}
		
	 	if (isset($this->request->post['config_delivery_methods'])) {
			$this->data['delivery_methods'] = $this->request->post['config_delivery_methods'];
		} elseif ($this->config->get('config_delivery_methods')) {
			$this->data['delivery_methods'] = $this->config->get('config_delivery_methods');
		} else {
			$this->data['delivery_methods'] = array();
		}

	    if (isset($this->request->post['config_gr_status'])) {
			$this->data['config_gr_status'] = $this->request->post['config_gr_status'];
		} elseif ($this->config->get('config_gr_status')) {
			$this->data['config_gr_status'] = $this->config->get('config_gr_status');
		} else {
			$this->data['config_gr_status'] = '';
		}

		if (isset($this->request->post['config_twitter_status'])) {
			$this->data['config_twitter_status'] = $this->request->post['config_twitter_status'];
		} elseif ($this->config->get('config_twitter_status')) {
			$this->data['config_twitter_status'] = $this->config->get('config_twitter_status');
		} else {
			$this->data['config_twitter_status'] = '';
		}

		if (isset($this->request->post['config_facebook_status'])) {
			$this->data['config_facebook_status'] = $this->request->post['config_facebook_status'];
		} elseif ($this->config->get('config_facebook_status')) {
			$this->data['config_facebook_status'] = $this->config->get('config_facebook_status');
		} else {
			$this->data['config_facebook_status'] = '';
		}


		if (isset($this->request->post['config_gr_companyname'])) {
			$this->data['config_gr_companyname'] = $this->request->post['config_gr_companyname'];
		} elseif ($this->config->get('config_gr_companyname')) {
			$this->data['config_gr_companyname'] = $this->config->get('config_gr_companyname');
		} else {
			$this->data['config_gr_companyname'] = '';
		}

		if (isset($this->request->post['config_gr_countryname'])) {
			$this->data['config_gr_countryname'] = $this->request->post['config_gr_countryname'];
		} elseif ($this->config->get('config_gr_countryname')) {
			$this->data['config_gr_countryname'] = $this->config->get('config_gr_countryname');
		} else {
			$this->data['config_gr_countryname'] = '';
		}

		if (isset($this->request->post['config_gr_localityname'])) {
			$this->data['config_gr_localityname'] = $this->request->post['config_gr_localityname'];
		} elseif ($this->config->get('config_gr_localityname')) {
			$this->data['config_gr_localityname'] = $this->config->get('config_gr_localityname');
		} else {
			$this->data['config_gr_localityname'] = '';
		}

		if (isset($this->request->post['config_gr_postalcode'])) {
			$this->data['config_gr_postalcode'] = $this->request->post['config_gr_postalcode'];
		} elseif ($this->config->get('config_gr_postalcode')) {
			$this->data['config_gr_postalcode'] = $this->config->get('config_gr_postalcode');
		} else {
			$this->data['config_gr_postalcode'] = '';
		}

		if (isset($this->request->post['config_gr_streetaddress'])) {
			$this->data['config_gr_streetaddress'] = $this->request->post['config_gr_streetaddress'];
		} elseif ($this->config->get('config_gr_streetaddress')) {
			$this->data['config_gr_streetaddress'] = $this->config->get('config_gr_streetaddress');
		} else {
			$this->data['config_gr_streetaddress'] = '';
		}
		
		if (isset($this->request->post['config_gr_telephonenumber'])) {
			$this->data['config_gr_telephonenumber'] = $this->request->post['config_gr_telephonenumber'];
		} elseif ($this->config->get('config_gr_telephonenumber')) {
			$this->data['config_gr_telephonenumber'] = $this->config->get('config_gr_telephonenumber');
		} else {
			$this->data['config_gr_telephonenumber'] = '';
		}

		if (isset($this->request->post['config_google_pageid'])) {
			$this->data['config_google_pageid'] = $this->request->post['config_google_pageid'];
		} elseif ($this->config->get('config_google_pageid')) {
			$this->data['config_google_pageid'] = $this->config->get('config_google_pageid');
		} else {
			$this->data['config_google_pageid'] = '';
		}

		if (isset($this->request->post['config_facebook_adminid'])) {
			$this->data['config_facebook_adminid'] = $this->request->post['config_facebook_adminid'];
		} elseif ($this->config->get('config_facebook_adminid')) {
			$this->data['config_facebook_adminid'] = $this->config->get('config_facebook_adminid');
		} else {
			$this->data['config_facebook_adminid'] = '';
		}

		if (isset($this->request->post['config_twitter_uername'])) {
			$this->data['config_twitter_uername'] = $this->request->post['config_twitter_uername'];
		} elseif ($this->config->get('config_twitter_uername')) {
			$this->data['config_twitter_uername'] = $this->config->get('config_twitter_uername');
		} else {
			$this->data['config_twitter_uername'] = '';
		}
		 
		$this->data['cancel'] = $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'); 
		$this->load->model('localisation/language');
		$this->data['languages'] = $this->model_localisation_language->getLanguages();

		$this->data['links'][] = array(
        		'text'      => "Auto Generate Tool",
            	'href'      => $this->url->link('catalog/seo/autogenerate', 'token=' . $this->session->data['token'], 'SSL'),
            	'image'     => 'view/image/product.png'
        );

         $this->data['links'][] = array(
        		'text'      => "Seo Advanced Editor",
            	'href'      => $this->url->link('catalog/seo/customize', 'token=' . $this->session->data['token'], 'SSL'),
            	'image'     => 'view/image/log.png'
        );

        $this->data['links'][] = array(
        		'text'      => "Dynamic Seo Report",
            	'href'      => $this->url->link('catalog/seoReport', 'token=' . $this->session->data['token'], 'SSL'),
            	'image'     => 'view/image/report.png'
        );

        $this->data['links'][] = array(
        		'text'      => "Complete Rich Snippet",
            	'href'      => $this->url->link('catalog/grsnippet', 'token=' . $this->session->data['token'], 'SSL'),
            	'image'     => 'view/image/feed.png'
        );

        $this->data['links'][] = array(
        		'text'      => "Sitemap Generator Pro",
            	'href'      => $this->url->link('catalog/sitemap', 'token=' . $this->session->data['token'], 'SSL'),
            	'image'     => 'view/image/review.png'
        );

        $this->data['links'][] = array(
        		'text'      => "Clear Seo Tool",
            	'href'      => $this->url->link('catalog/clearseo', 'token=' . $this->session->data['token'], 'SSL'),
            	'image'     => 'view/image/error.png'
        );

        $this->data['links'][] = array(
                    'text'      => "Seo Redirect Manager",
                    'href'      => $this->url->link('catalog/seomanager', 'token=' . $this->session->data['token'], 'SSL'),
                    'image'     => 'view/image/error.png'
        );

        $this->data['links'][] = array(
                    'text'      => "Setting",
                    'href'      => $this->url->link('catalog/setting', 'token=' . $this->session->data['token'], 'SSL'),
                    'image'     => 'view/image/setting.png'
        );
 
		$this->template = 'catalog/grsnippet.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
}
?>