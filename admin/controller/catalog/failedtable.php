<?php 
class ControllerCatalogFailedTable extends Controller { 
	private $error = array();
	
	public function index() {		

		$this->load->model('catalog/seomanager');
		$this->model_catalog_seomanager->createTablesInDatabse();
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'index';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		$url = '';
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->load->language('catalog/failedtable');
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->data['heading_title'] = $this->language->get('heading_title');
		 
		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/failedtable', 'token=' . $this->session->data['token'] . $url, 'SSL'),       		
      		'separator' => ' :: '
   		);

   		$this->data['failed_url'] = $this->language->get('failed_url');
   		$this->data['date'] = $this->language->get('date');
   		$this->data['count'] = $this->language->get('count');
   		$this->data['create_redirect'] = $this->language->get('create_redirect');
   		$this->data['insert_redirect'] = $this->language->get('insert_redirect');
   		$this->data['text_no_results'] = $this->language->get('text_no_results');
   		$this->data['button_cancel'] = $this->language->get('button_cancel');
   		$this->data['cancel'] = $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL');
   		$this->data['clear_all_urls'] = $this->language->get('clear_all_urls');
   		$this->data['clear_all'] = $this->url->link('catalog/failedtable/clear', 'token=' . $this->session->data['token'], 'SSL'); 

    	$this->data['token'] = $this->session->data['token'];
    	$this->data['redirectstatus'] = $this->config->get('config_redirect_status');
    	
    	if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$data = array(
			'sort'            => $sort,
			'order'           => $order,
			'start'           => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'           => $this->config->get('config_admin_limit')
		);

		$this->load->model('catalog/seomanager');
		$this->model_catalog_seomanager->createTablesInDatabse();
		$this->data['redirectlist'] = $this->model_catalog_seomanager->getListt($data);
		foreach ($this->data['redirectlist'] as $key => $value) {
			$this->data['redirectlist'][$key]['action'] = $this->url->link('catalog/seomanager', 'token=' . $this->session->data['token'], 'SSL');
		}
		
		$this->data['redirecttotal'] = $this->model_catalog_seomanager->getTotalListt($data);

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['sort_date'] = $this->url->link('catalog/failedtable', 'token=' . $this->session->data['token'] . '&sort=date' . $url, 'SSL');
		$this->data['sort_failed_url'] = $this->url->link('catalog/failedtable', 'token=' . $this->session->data['token'] . '&sort=failed_Url' . $url, 'SSL');
		$this->data['sort_count'] = $this->url->link('catalog/failedtable', 'token=' . $this->session->data['token'] . '&sort=count' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
												
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$this->data['links'][] = array(
                'text'      => "Auto Generate Tool",
                'href'      => $this->url->link('catalog/seo/autogenerate', 'token=' . $this->session->data['token'], 'SSL'),
                'image'     => 'view/image/product.png'
        );

         $this->data['links'][] = array(
                'text'      => "Seo Advanced Editor",
                'href'      => $this->url->link('catalog/seo/customize', 'token=' . $this->session->data['token'], 'SSL'),
                'image'     => 'view/image/log.png'
        );

        $this->data['links'][] = array(
                'text'      => "Dynamic Seo Report",
                'href'      => $this->url->link('catalog/seoReport', 'token=' . $this->session->data['token'], 'SSL'),
                'image'     => 'view/image/report.png'
        );

        $this->data['links'][] = array(
                'text'      => "Complete Rich Snippet",
                'href'      => $this->url->link('catalog/grsnippet', 'token=' . $this->session->data['token'], 'SSL'),
                'image'     => 'view/image/feed.png'
        );

        $this->data['links'][] = array(
                'text'      => "Sitemap Generator Pro",
                'href'      => $this->url->link('catalog/sitemap', 'token=' . $this->session->data['token'], 'SSL'),
                'image'     => 'view/image/review.png'
        );

        $this->data['links'][] = array(
                'text'      => "Clear Seo Tool",
                'href'      => $this->url->link('catalog/clearseo', 'token=' . $this->session->data['token'], 'SSL'),
                'image'     => 'view/image/error.png'
        );

        $this->data['links'][] = array(
                    'text'      => "Seo Redirect Manager",
                    'href'      => $this->url->link('catalog/seomanager', 'token=' . $this->session->data['token'], 'SSL'),
                    'image'     => 'view/image/error.png'
        );

        $this->data['links'][] = array(
                    'text'      => "Setting",
                    'href'      => $this->url->link('catalog/setting', 'token=' . $this->session->data['token'], 'SSL'),
                    'image'     => 'view/image/setting.png'
        );

		$pagination = new Pagination();
		$pagination->total =$this->data['redirecttotal'];
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/failedtable', 'token=' . $this->session->data['token'] . $url .'&page={page}', 'SSL');
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['pagination'] = $pagination->render();
		
		$this->template = 'catalog/failedtable.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	public function clear(){
		$this->load->model('catalog/seomanager');
		$this->load->language('catalog/failedtable');
		$this->model_catalog_seomanager->clear();
		$this->session->data['success'] = $this->language->get('text_success');
		$this->redirect($this->url->link('catalog/failedtable', 'token=' . $this->session->data['token'], 'SSL'));
	}
	
}
?>