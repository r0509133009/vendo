<?php 
class ControllerCatalogSitemap extends Controller { 
    private $error = array();
    
    public function index() {
        $this->load->language('catalog/sitemap');

        $this->document->setTitle($this->language->get('heading_title'));
        
        $this->load->model('catalog/sitemap');

        $this->data['heading_title'] = $this->language->get('heading_title');
         
        $this->data['button_generate'] = $this->language->get('button_generate');
        
        $this->data['seordata'] = $this->language->get('seordata');
        $this->data['help'] = $this->language->get('help');
        $this->data['button_cancel'] = $this->language->get('button_cancel');
        $this->data['cancel'] = $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL');
        
        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }
        
        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];
        
            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        if (isset($this->session->data['output'])) {
            $this->data['output'] = $this->session->data['output'];
        
            unset($this->session->data['output']);
        } else {
            $this->data['output'] = '';
        }

        $this->data['links'][] = array(
                'text'      => "Auto Generate Tool",
                'href'      => $this->url->link('catalog/seo/autogenerate', 'token=' . $this->session->data['token'], 'SSL'),
                'image'     => 'view/image/product.png'
        );

         $this->data['links'][] = array(
                'text'      => "Seo Advanced Editor",
                'href'      => $this->url->link('catalog/seo/customize', 'token=' . $this->session->data['token'], 'SSL'),
                'image'     => 'view/image/log.png'
        );

        $this->data['links'][] = array(
                'text'      => "Dynamic Seo Report",
                'href'      => $this->url->link('catalog/seoReport', 'token=' . $this->session->data['token'], 'SSL'),
                'image'     => 'view/image/report.png'
        );

        $this->data['links'][] = array(
                'text'      => "Complete Rich Snippet",
                'href'      => $this->url->link('catalog/grsnippet', 'token=' . $this->session->data['token'], 'SSL'),
                'image'     => 'view/image/feed.png'
        );

        $this->data['links'][] = array(
                'text'      => "Sitemap Generator Pro",
                'href'      => $this->url->link('catalog/sitemap', 'token=' . $this->session->data['token'], 'SSL'),
                'image'     => 'view/image/review.png'
        );

        $this->data['links'][] = array(
                'text'      => "Clear Seo Tool",
                'href'      => $this->url->link('catalog/clearseo', 'token=' . $this->session->data['token'], 'SSL'),
                'image'     => 'view/image/error.png'
        );

        $this->data['links'][] = array(
                    'text'      => "Seo Redirect Manager",
                    'href'      => $this->url->link('catalog/seomanager', 'token=' . $this->session->data['token'], 'SSL'),
                    'image'     => 'view/image/error.png'
        );

        $this->data['links'][] = array(
                    'text'      => "Setting",
                    'href'      => $this->url->link('catalog/setting', 'token=' . $this->session->data['token'], 'SSL'),
                    'image'     => 'view/image/setting.png'
        );

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),             
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('catalog/sitemap', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $this->data['sitemapg'] = $this->url->link('catalog/sitemap/generate', 'token=' . $this->session->data['token'], 'SSL');

        $this->data['generate'] = $this->url->link('catalog/sitemap/generate', 'token=' . $this->session->data['token'], 'SSL');
        
        $this->load->model('catalog/sitemap');
            
        $this->template = 'catalog/sitemap.tpl';
        $this->children = array(
            'common/header',    
            'common/footer' 
        );
        if(file_exists(DIR_SYSTEM."../sitemap.xml")) {
            $this->data['sitemapexists'] = HTTP_CATALOG."sitemap.xml";
        }
        $this->response->setOutput($this->render());
    }
    
    public function generate() {
        $this->load->language('catalog/sitemap');

        $this->document->setTitle($this->language->get('heading_title'));
        
        $this->load->model('catalog/sitemap');
        
        if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate()) {
            $this->session->data['output'] = $this->model_catalog_sitemap->generate();          
            $this->session->data['success'] = $this->language->get('text_success');
            $this->redirect($this->url->link('catalog/sitemap', 'token=' . $this->session->data['token'], 'SSL'));
        } else {
            return $this->forward('error/permission');
        }
    }
    
    private function validate() {
        if (!$this->user->hasPermission('modify', 'catalog/sitemap')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        
        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }       
    }
}
?>