<?php  
class ControllerCatalogsetting extends Controller {  
    private $error = array();
   
    public function index() {
        $this->load->language('catalog/setting');       
        
        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }
         
        $this->document->setTitle($this->language->get('heading_title'));
        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->getForm();
    }
    
    public function insert() {    
        $this->load->language('catalog/setting');
        $this->load->model('setting/setting');
        $this->document->setTitle($this->language->get('heading_title'));
        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            
            if(!isset($this->request->post['config_direct_links'])){
                $this->request->post['config_direct_links'] = 0;
            } else {
                 $this->request->post['config_direct_links'] = 1;
            }
            if(!isset($this->request->post['config_breadcrumblink'])){
                $this->request->post['config_breadcrumblink'] = 0;
            } else {
                 $this->request->post['config_breadcrumblink'] = 1;
            }
            if(!isset($this->request->post['config_display_social_widgets'])){
                $this->request->post['config_display_social_widgets'] = 0;
            } else {
                 $this->request->post['config_display_social_widgets'] = 1;
            }
           
            $this->request->post['config_multilang_lang'] = $this->config->get("config_language");
            $this->model_setting_setting->editSetting('seosetting', $this->request->post);
            $this->session->data['success'] = $this->language->get('text_success');
            $this->redirect($this->url->link('catalog/setting', 'token=' . $this->session->data['token'], 'SSL'));
        }
        $this->getForm();
    }
    
    private function getForm() {
       
        $this->load->language('catalog/setting');
        $this->data['text_enabled'] = $this->language->get('text_enabled');
        $this->data['text_disabled'] = $this->language->get('text_disabled'); 
        $this->data['text_about'] = $this->language->get('text_about'); 
        $this->data['tab_company'] = $this->language->get('tab_company');
        $this->data['button_save'] = $this->language->get('button_save');
        $this->data['button_cancel'] = $this->language->get('button_cancel'); 
        $this->data['text_multi_lang'] = $this->language->get('text_multi_lang'); 
        $this->data['text_direct_links'] = $this->language->get('text_direct_links'); 
        $this->data['text_social_widgets'] = $this->language->get('text_social_widgets'); 
        $this->data['token'] = $this->session->data['token'];

        $this->data['breadcrumbs'] = array();
        $this->data['breadcrumbs'][] = array(
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'text'      => $this->language->get('text_home'),
            'separator' => FALSE
        );

        $this->data['breadcrumbs'][] = array(
            'href'      => $this->url->link('catalog/setting', 'token=' . $this->session->data['token'], 'SSL'),
            'text'      => $this->language->get('heading_title'),
            'separator' => ' :: '
        );

        $this->data['action'] = $this->url->link('catalog/setting/insert', 'token=' . $this->session->data['token'], 'SSL');
       
        
        if (isset($this->request->post['config_breadcrumblink'])) {
            $this->data['config_breadcrumblink'] = $this->request->post['config_breadcrumblink'];
        } elseif ($this->config->get('config_breadcrumblink')) {
            $this->data['config_breadcrumblink'] = $this->config->get('config_breadcrumblink');
        } else {
            $this->data['config_breadcrumblink'] = '';
        }
       
        if (isset($this->request->post['config_direct_links'])) {
            $this->data['config_direct_links'] = $this->request->post['config_direct_links'];
        } elseif ($this->config->get('config_direct_links')) {
            $this->data['config_direct_links'] = $this->config->get('config_direct_links');
        } else {
            $this->data['config_direct_links'] = '';
        }
       
        if (isset($this->request->post['config_display_social_widgets'])) {
            $this->data['config_display_social_widgets'] = $this->request->post['config_display_social_widgets'];
        } elseif ($this->config->get('config_display_social_widgets')) {
            $this->data['config_display_social_widgets'] = $this->config->get('config_display_social_widgets');
        } else {
            $this->data['config_display_social_widgets'] = '';
        }
         
        $this->data['cancel'] = $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'); 
        $this->load->model('localisation/language');
        $this->data['languages'] = $this->model_localisation_language->getLanguages();

        $this->data['links'][] = array(
                'text'      => "Auto Generate Tool",
                'href'      => $this->url->link('catalog/seo/autogenerate', 'token=' . $this->session->data['token'], 'SSL'),
                'image'     => 'view/image/product.png'
        );

         $this->data['links'][] = array(
                'text'      => "Seo Advanced Editor",
                'href'      => $this->url->link('catalog/seo/customize', 'token=' . $this->session->data['token'], 'SSL'),
                'image'     => 'view/image/log.png'
        );

        $this->data['links'][] = array(
                'text'      => "Dynamic Seo Report",
                'href'      => $this->url->link('catalog/seoReport', 'token=' . $this->session->data['token'], 'SSL'),
                'image'     => 'view/image/report.png'
        );

        $this->data['links'][] = array(
                'text'      => "Complete Rich Snippet",
                'href'      => $this->url->link('catalog/grsnippet', 'token=' . $this->session->data['token'], 'SSL'),
                'image'     => 'view/image/feed.png'
        );

        $this->data['links'][] = array(
                'text'      => "Sitemap Generator Pro",
                'href'      => $this->url->link('catalog/sitemap', 'token=' . $this->session->data['token'], 'SSL'),
                'image'     => 'view/image/review.png'
        );

        $this->data['links'][] = array(
                'text'      => "Clear Seo Tool",
                'href'      => $this->url->link('catalog/clearseo', 'token=' . $this->session->data['token'], 'SSL'),
                'image'     => 'view/image/error.png'
        );

        $this->data['links'][] = array(
                    'text'      => "Seo Redirect Manager",
                    'href'      => $this->url->link('catalog/seomanager', 'token=' . $this->session->data['token'], 'SSL'),
                    'image'     => 'view/image/error.png'
        );

         $this->data['links'][] = array(
                    'text'      => "Setting",
                    'href'      => $this->url->link('catalog/setting', 'token=' . $this->session->data['token'], 'SSL'),
                    'image'     => 'view/image/setting.png'
        );
 
        $this->template = 'catalog/setting.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );
        $this->response->setOutput($this->render());
    }
}
?>