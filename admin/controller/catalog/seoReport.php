<?php
class ControllerCatalogSeoReport extends Controller {

    public function index() {
        $this->language->load('catalog/seoReport');
    
        $this->document->setTitle($this->language->get('heading_title'));

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('catalog/seoReport', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }
        
        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];
        
            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        $this->data['links'][] = array(
                'text'      => "Auto Generate Tool",
                'href'      => $this->url->link('catalog/seo/autogenerate', 'token=' . $this->session->data['token'], 'SSL'),
                'image'     => 'view/image/product.png'
        );

         $this->data['links'][] = array(
                'text'      => "Seo Advanced Editor",
                'href'      => $this->url->link('catalog/seo/customize', 'token=' . $this->session->data['token'], 'SSL'),
                'image'     => 'view/image/log.png'
        );

        $this->data['links'][] = array(
                'text'      => "Dynamic Seo Report",
                'href'      => $this->url->link('catalog/seoReport', 'token=' . $this->session->data['token'], 'SSL'),
                'image'     => 'view/image/report.png'
        );

        $this->data['links'][] = array(
                'text'      => "Complete Rich Snippet",
                'href'      => $this->url->link('catalog/grsnippet', 'token=' . $this->session->data['token'], 'SSL'),
                'image'     => 'view/image/feed.png'
        );

        $this->data['links'][] = array(
                'text'      => "Sitemap Generator Pro",
                'href'      => $this->url->link('catalog/sitemap', 'token=' . $this->session->data['token'], 'SSL'),
                'image'     => 'view/image/review.png'
        );

        $this->data['links'][] = array(
                'text'      => "Clear Seo Tool",
                'href'      => $this->url->link('catalog/clearseo', 'token=' . $this->session->data['token'], 'SSL'),
                'image'     => 'view/image/error.png'
        );

        $this->data['links'][] = array(
                    'text'      => "Seo Redirect Manager",
                    'href'      => $this->url->link('catalog/seomanager', 'token=' . $this->session->data['token'], 'SSL'),
                    'image'     => 'view/image/error.png'
        );

        $this->data['links'][] = array(
                    'text'      => "Setting",
                    'href'      => $this->url->link('catalog/setting', 'token=' . $this->session->data['token'], 'SSL'),
                    'image'     => 'view/image/setting.png'
        );
        
        $this->data['cancel'] =  $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['button_cancel'] = $this->language->get('button_cancel');
        $this->data['seordata'] = $this->language->get('seordata');
        $this->data['token'] = $this->session->data['token'];
        $this->data['create_report'] = $this->language->get('create_report');
        $this->data['total'] = $this->language->get('total');
        $this->data['mk'] = $this->language->get('mk');
        $this->data['md'] = $this->language->get('md');
        $this->data['sd'] = $this->language->get('sd');
        $this->data['sitemap'] = $this->language->get('sitemap');
        $this->data['sitemapt'] = sprintf($this->language->get('sitemapt'),HTTP_CATALOG."sitemap.xml");
        $this->data['sitemapso'] = sprintf($this->language->get('sitemapso'),$this->url->link('catalog/sitemap', 'token=' . $this->session->data['token'], 'SSL'));
        $this->template = 'catalog/seoReport.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );
                
        $this->response->setOutput($this->render());
    }

    public function getreport() {
        $this->load->model('catalog/seoReport');
        
        $results1 = $this->model_catalog_seoReport->getreport1();
        $results2 = $this->model_catalog_seoReport->getreport2();
        $results3 = $this->model_catalog_seoReport->getreport3();
        $results4 = $this->model_catalog_seoReport->getreport4();
        $sitemap  =  file_exists('../sitemap.xml');
        $results = array('products' => $results1, 'categories' => $results2 ,'information' => $results3,'manufacturer' => $results4,'sitemap' => $sitemap);
        $this->response->setOutput(json_encode($results));
    }

}
?>