<?php
class ControllerCatalogVariantproducts extends Controller {
	private $error = array();

	public function index() {
		$this->language->load('catalog/variantproducts');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/variantproducts');

		$this->getList();
	}

	public function insert() {
		$this->language->load('catalog/variantproducts');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/variantproducts');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_variantproducts->addVariantproduct($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success_insert');

			$this->redirect($this->url->link('catalog/variantproducts', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->getForm();
	}

	public function update() {
		$this->language->load('catalog/variantproducts');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/variantproducts');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_variantproducts->editVariantproduct($this->request->get['variantproduct_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success_update');

			$this->redirect($this->url->link('catalog/variantproducts', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->getForm();
	}

	public function delete() {
		$this->language->load('catalog/variantproducts');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/variantproducts');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $variantproduct_id) {
				$this->model_catalog_variantproducts->deleteVariantproduct($variantproduct_id);
			}

			$this->session->data['success'] = $this->language->get('text_success_delete');

			$this->redirect($this->url->link('catalog/variantproducts', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->getList();
	}

	private function getList() {
        $this->database();

   		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/variantproducts', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);

		$this->data['insert'] = $this->url->link('catalog/variantproducts/insert', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['delete'] = $this->url->link('catalog/variantproducts/delete', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['variantproducts'] = array();

		$results = $this->model_catalog_variantproducts->getVariantproducts();

		foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('catalog/variantproducts/update', 'token=' . $this->session->data['token'] . '&variantproduct_id=' . $result['variantproduct_id'], 'SSL')
			);

			$this->data['variantproducts'][] = array(
				'variantproduct_id'      => $result['variantproduct_id'],
				'title'       => $result['title'],
				'label'       => $result['label'],				
				'sort_order'  => $result['sort_order'],
                'status'      => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
				'selected'    => isset($this->request->post['selected']) && in_array($result['variantproduct_id'], $this->request->post['selected']),
				'action'      => $action
			);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
        $this->data['text_label'] = $this->language->get('text_label');
		$this->data['column_title'] = $this->language->get('column_title');
		$this->data['column_sort_order'] = $this->language->get('column_sort_order');
        $this->data['column_status'] = $this->language->get('column_status');
		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->template = 'catalog/variantproducts_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	private function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_yes'] = $this->language->get('text_yes');
        $this->data['text_no'] = $this->language->get('text_no');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
    	$this->data['text_disabled'] = $this->language->get('text_disabled');
        $this->data['text_select_all'] = $this->language->get('text_select_all');
		$this->data['text_unselect_all'] = $this->language->get('text_unselect_all');
		$this->data['entry_title'] = $this->language->get('entry_title');
		$this->data['entry_label'] = $this->language->get('entry_label');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_status'] = $this->language->get('entry_status');
        $this->data['entry_product'] = $this->language->get('entry_product');
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

 		if (isset($this->error['title'])) {
			$this->data['error_title'] = $this->error['title'];
		} else {
			$this->data['error_title'] = array();
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/variantproducts', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);

		if (!isset($this->request->get['variantproduct_id'])) {
			$this->data['action'] = $this->url->link('catalog/variantproducts/insert', 'token=' . $this->session->data['token'], 'SSL');
		} else {
			$this->data['action'] = $this->url->link('catalog/variantproducts/update', 'token=' . $this->session->data['token'] . '&variantproduct_id=' . $this->request->get['variantproduct_id'], 'SSL');
		}

		$this->data['cancel'] = $this->url->link('catalog/variantproducts', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->get['variantproduct_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
      		$variantproduct_info = $this->model_catalog_variantproducts->getVariantproduct($this->request->get['variantproduct_id']);
    	}

		$this->data['token'] = $this->session->data['token'];

		$this->load->model('localisation/language');

		$this->data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->post['variantproduct_description'])) {
			$this->data['variantproduct_description'] = $this->request->post['variantproduct_description'];
		} elseif (isset($this->request->get['variantproduct_id'])) {
			$this->data['variantproduct_description'] = $this->model_catalog_variantproducts->getVariantproductDescriptions($this->request->get['variantproduct_id']);
		} else {
			$this->data['variantproduct_description'] = array();
		}
		if (isset($this->request->post['sort_order'])) {
			$this->data['sort_order'] = $this->request->post['sort_order'];
		} elseif (!empty($variantproduct_info)) {
			$this->data['sort_order'] = $variantproduct_info['sort_order'];
		} else {
			$this->data['sort_order'] = 0;
		}

		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif (!empty($variantproduct_info)) {
			$this->data['status'] = $variantproduct_info['status'];
		} else {
			$this->data['status'] = 1;
		}

		$this->load->model('catalog/product');

		$this->data['products'] = $this->model_catalog_product->getProducts(array('sort' => 'pd.name'));

        if (isset($this->request->post['product'])) {
			$this->data['product_variantproduct'] = $this->request->post['product'];
		} elseif (isset($this->request->get['variantproduct_id'])) {
			$this->data['product_variantproduct'] = $this->model_catalog_variantproducts->getVariantproductProducts($this->request->get['variantproduct_id']);
		} else {
			$this->data['product_variantproduct'] = array();
		}

		$this->template = 'catalog/variantproducts_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	private function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/variantproducts')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		foreach ($this->request->post['variantproduct_description'] as $language_id => $value) {
			if ((utf8_strlen($value['title']) < 2) || (utf8_strlen($value['title']) > 64)) {
				$this->error['title'][$language_id] = $this->language->get('error_title');
			}
		}

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	private function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/variantproducts')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

    private function database() {
        $query = $this->db->query("SHOW TABLES LIKE '" . DB_PREFIX . "product_additional_variantproduct'");
        $query2 = $this->db->query("SHOW TABLES LIKE '" . DB_PREFIX . "variantproducts'");

        if ((!$query->num_rows) && (!$query2->num_rows)) {
            $this->db->query("CREATE TABLE `" . DB_PREFIX . "variantproducts` (`variantproduct_id` int(11) NOT NULL AUTO_INCREMENT, `sort_order` int(3) NOT NULL DEFAULT '0', `status` tinyint(1) NOT NULL, PRIMARY KEY (`variantproduct_id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;");
        }

        $query_desc = $this->db->query("SHOW TABLES LIKE '" . DB_PREFIX . "product_additional_variantproduct_description'");
        $query_desc2 = $this->db->query("SHOW TABLES LIKE '" . DB_PREFIX . "variantproducts_description'");

        if ((!$query_desc->num_rows) && (!$query_desc2->num_rows)) {
            $this->db->query("CREATE TABLE `" . DB_PREFIX . "variantproducts_description` (`variantproduct_id` int(11) NOT NULL, `language_id` int(11) NOT NULL, `title` varchar(64) COLLATE utf8_bin NOT NULL, `label` varchar(180) COLLATE utf8_bin NOT NULL, PRIMARY KEY (`variantproduct_id`,`language_id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;");
        }

        $query_product = $this->db->query("SHOW TABLES LIKE '" . DB_PREFIX . "variantproducts_to_product'");

        if (!$query_product->num_rows) {
            $this->db->query("CREATE TABLE `" . DB_PREFIX . "variantproducts_to_product` (`variantproduct_id` int(11) NOT NULL, `product_id` int(11) NOT NULL, PRIMARY KEY (`variantproduct_id`, `product_id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;");
        }
        if ($query->num_rows) {
            $query_id = $this->db->query("SHOW COLUMNS FROM `" . DB_PREFIX . "product_additional_variantproduct` LIKE 'product_additional_variantproduct_id'");
            $query_sort = $this->db->query("SHOW COLUMNS FROM `" . DB_PREFIX . "product_additional_variantproduct` LIKE 'sort_order'");

            if (!$query_id->num_rows) {
                $this->db->query("ALTER TABLE `" . DB_PREFIX . "product_additional_variantproduct` DROP PRIMARY KEY, ADD `product_additional_variantproduct_id` int(11) NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`product_additional_variantproduct_id`);");
            }

            if (!$query_sort->num_rows) {
                $this->db->query("ALTER TABLE `" . DB_PREFIX . "product_additional_variantproduct` ADD `sort_order` int(3) NOT NULL DEFAULT '0';");
            }
            $query_title = $this->db->query("DESC " . DB_PREFIX . "product_additional_variantproduct title");
            $query_label = $this->db->query("DESC " . DB_PREFIX . "product_additional_variantproduct label");			

            if ($query_title->num_rows || $query_label->num_rows) {
                $prev_data = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_additional_variantproduct");

                foreach ($prev_data->rows as $prev_data) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "variantproducts_description SET variantproduct_id = '" . (int)$prev_data['product_additional_variantproduct_id'] . "', language_id = '" . (int)$this->config->get('config_language_id') . "', title = '" . $this->db->escape($prev_data['title']) . "', label = '" . $this->db->escape($prev_data['label']) . "'");
                }

                $this->db->query("ALTER TABLE `" . DB_PREFIX . "product_additional_variantproduct` DROP title, DROP label;");
            }
            $query_productid = $this->db->query("DESC " . DB_PREFIX . "product_additional_variantproduct product_id");
            
			if ($query_productid->num_rows) {
                $prev_data = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_additional_variantproduct");

                foreach ($prev_data->rows as $prev_data) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "variantproducts_to_product SET variantproduct_id = '" . (int)$prev_data['product_additional_variantproduct_id'] . "', product_id = '" . (int)$prev_data['product_id'] . "'");
                }

                $this->db->query("ALTER TABLE `" . DB_PREFIX . "product_additional_variantproduct` DROP product_id;");
            }
            if ($query->num_rows) {
                $this->db->query("ALTER TABLE `" . DB_PREFIX . "product_additional_variantproduct` CHANGE COLUMN `product_additional_variantproduct_id` `variantproduct_id` int(11) NOT NULL AUTO_INCREMENT;");
                $this->db->query("RENAME TABLE `" . DB_PREFIX . "product_additional_variantproduct` TO `" . DB_PREFIX . "variantproducts`;");
            }

            if ($query_desc->num_rows) {
                $this->db->query("ALTER TABLE `" . DB_PREFIX . "product_additional_variantproduct_description` DROP INDEX `name`;");
                $this->db->query("ALTER TABLE `" . DB_PREFIX . "product_additional_variantproduct_description` CHANGE COLUMN `product_additional_variantproduct_id` `variantproduct_id` int(11) NOT NULL;");
                $this->db->query("RENAME TABLE `" . DB_PREFIX . "product_additional_variantproduct_description` TO `" . DB_PREFIX . "variantproducts_description`;");
            }
            $query_status = $this->db->query("SHOW COLUMNS FROM `" . DB_PREFIX . "variantproducts` LIKE 'status'");
            if (!$query_status->num_rows) {
                $this->db->query("ALTER TABLE `" . DB_PREFIX . "variantproducts` ADD `status` tinyint(1) NOT NULL DEFAULT '1' AFTER `sort_order`;");
            }
        }
    }
}
?>