<?php 
class ControllerCatalogseomanager extends Controller { 
    private $error = array();
    
    public function index() {       
        
        $this->load->model('catalog/seomanager');
        $this->model_catalog_seomanager->createTablesInDatabse();

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'index';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'DESC';
        }
        $url = '';
        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $this->load->language('catalog/seomanager');
        $this->document->setTitle($this->language->get('heading_title'));
        
        $this->data['heading_title'] = $this->language->get('heading_title');
         
        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );
        
        if (isset($this->request->post['fromTable'])) {
            $this->data['fromTable'] = $this->request->post['fromTable'];
        } else {
            $this->data['fromTable'] = '';
        }
        
        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('catalog/seomanager', 'token=' . $this->session->data['token'] . $url, 'SSL'),            
            'separator' => ' :: '
        );
        $this->data['status'] = $this->language->get('status');
        $this->data['mstatus'] = $this->language->get('mstatus');
        $this->data['from_url'] = $this->language->get('from_url'); 
        $this->data['button_insert'] = $this->language->get('button_insert');
        $this->data['button_delete'] = $this->language->get('button_delete');
        $this->data['text_enabled'] = $this->language->get('text_enabled');
        $this->data['text_disabled'] = $this->language->get('text_disabled');
        $this->data['text_redirect_table'] = $this->language->get('text_redirect_table');
        $this->data['failed'] = $this->language->get('failed');
        $this->data['button_save'] = $this->language->get('button_save');
        $this->data['url_test'] = $this->language->get('url_test');
        $this->data['url_test_redirect'] = $this->language->get('url_test_redirect');
        $this->data['button_cancel'] = $this->language->get('button_cancel');
        $this->data['cancel'] = $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['delete'] = $this->language->get('delete');
        $this->data['times_used'] = $this->language->get('times_used');
        $this->data['help'] = $this->language->get('help');
        $this->data['text_no_results'] = $this->language->get('text_no_results');
        $this->data['action'] = $this->url->link('catalog/seomanager/insert', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['to_url'] = $this->language->get('to_url');
        $this->data['redirect_table'] = $this->url->link('catalog/failedtable', 'token=' . $this->session->data['token'] . $url, 'SSL');

        $this->data['token'] = $this->session->data['token'];
        $this->data['redirectstatus'] = $this->config->get('config_redirect_status');

        $this->data['links'][] = array(
                'text'      => "Auto Generate Tool",
                'href'      => $this->url->link('catalog/seo/autogenerate', 'token=' . $this->session->data['token'], 'SSL'),
                'image'     => 'view/image/product.png'
        );

         $this->data['links'][] = array(
                'text'      => "Seo Advanced Editor",
                'href'      => $this->url->link('catalog/seo/customize', 'token=' . $this->session->data['token'], 'SSL'),
                'image'     => 'view/image/log.png'
        );

        $this->data['links'][] = array(
                'text'      => "Dynamic Seo Report",
                'href'      => $this->url->link('catalog/seoReport', 'token=' . $this->session->data['token'], 'SSL'),
                'image'     => 'view/image/report.png'
        );

        $this->data['links'][] = array(
                'text'      => "Complete Rich Snippet",
                'href'      => $this->url->link('catalog/grsnippet', 'token=' . $this->session->data['token'], 'SSL'),
                'image'     => 'view/image/feed.png'
        );

        $this->data['links'][] = array(
                'text'      => "Sitemap Generator Pro",
                'href'      => $this->url->link('catalog/sitemap', 'token=' . $this->session->data['token'], 'SSL'),
                'image'     => 'view/image/review.png'
        );

        $this->data['links'][] = array(
                'text'      => "Clear Seo Tool",
                'href'      => $this->url->link('catalog/clearseo', 'token=' . $this->session->data['token'], 'SSL'),
                'image'     => 'view/image/error.png'
        );

        $this->data['links'][] = array(
                    'text'      => "Seo Redirect Manager",
                    'href'      => $this->url->link('catalog/seomanager', 'token=' . $this->session->data['token'], 'SSL'),
                    'image'     => 'view/image/error.png'
        );

        $this->data['links'][] = array(
                    'text'      => "Setting",
                    'href'      => $this->url->link('catalog/setting', 'token=' . $this->session->data['token'], 'SSL'),
                    'image'     => 'view/image/setting.png'
        );
        
        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];
        
            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        $data = array(
            'sort'            => $sort,
            'order'           => $order,
            'start'           => ($page - 1) * $this->config->get('config_admin_limit'),
            'limit'           => $this->config->get('config_admin_limit')
        );

        $this->data['redirectlist'] = $this->model_catalog_seomanager->getList($data);
        $this->data['redirecttotal'] = $this->model_catalog_seomanager->getTotalList($data);

        $url = '';

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $this->data['sort_status'] = $this->url->link('catalog/seomanager', 'token=' . $this->session->data['token'] . '&sort=status' . $url, 'SSL');
        $this->data['sort_fromUrl'] = $this->url->link('catalog/seomanager', 'token=' . $this->session->data['token'] . '&sort=fromUrl' . $url, 'SSL');
        $this->data['sort_toUrl'] = $this->url->link('catalog/seomanager', 'token=' . $this->session->data['token'] . '&sort=toUrl' . $url, 'SSL');
        $this->data['sort_times_used'] = $this->url->link('catalog/seomanager', 'token=' . $this->session->data['token'] . '&sort=times_used' . $url, 'SSL');

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }
                                                
        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        $pagination = new Pagination();
        $pagination->total =$this->data['redirecttotal'];
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_admin_limit');
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = $this->url->link('catalog/seomanager', 'token=' . $this->session->data['token'] . $url .'&page={page}', 'SSL');
        $this->data['sort'] = $sort;
        $this->data['order'] = $order;
        $this->data['pagination'] = $pagination->render();
        
        $this->template = 'catalog/seomanager.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );
                
        $this->response->setOutput($this->render());
    }

    public function insert() {
        $status = array();
        $status['config_redirect_status'] = $this->request->get['value'];
        $this->load->model('setting/setting');
        $this->model_setting_setting->editSetting('config_redirect_status', $status);
    }

    public function update() {
        $this->load->model('catalog/seomanager');
        $this->model_catalog_seomanager->updateUrl($this->request->post);
    }

    public function delete() {
        $this->load->model('catalog/seomanager');
        $this->model_catalog_seomanager->deleteUrl($this->request->get);
    }
    public function reset() {
        $this->load->model('catalog/seomanager');
        $this->model_catalog_seomanager->reset($this->request->get);
    }
    public function inserting() {
        $this->load->model('catalog/seomanager');
        $this->model_catalog_seomanager->addUrl1($this->request->post);
    }

}
?>