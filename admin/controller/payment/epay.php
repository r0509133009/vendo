<?php

/**
 * Epay.bg payment gateway for Opencart by Extensa Web Development
 *
 * Copyright � 2010-2015 Extensa Web Development Ltd. All Rights Reserved.
 * This file may not be redistributed in whole or significant part.
 * This copyright notice MUST APPEAR in all copies of the script!
 *
 * @author 		Extensa Web Development Ltd. (www.extensadev.com)
 * @copyright	Copyright (c) 2010-2015, Extensa Web Development Ltd.
 * @package 	Epay.bg payment gateway
 * @link		http://www.opencart.com/index.php?route=extension/extension/info&extension_id=2999
 */

class ControllerPaymentEpay extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('payment/epay');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validate())) {
			$this->model_setting_setting->editSetting('epay', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_all_zones'] = $this->language->get('text_all_zones');
		$this->data['text_yes'] = $this->language->get('text_yes');
		$this->data['text_no'] = $this->language->get('text_no');

		$this->data['entry_test'] = $this->language->get('entry_test');
		$this->data['entry_email'] = $this->language->get('entry_email');
		$this->data['entry_client_number'] = $this->language->get('entry_client_number');
		$this->data['entry_secret_key'] = $this->language->get('entry_secret_key');
		$this->data['entry_description'] = $this->language->get('entry_description');
		$this->data['entry_expired_days'] = $this->language->get('entry_expired_days');
		$this->data['entry_order_status'] = $this->language->get('entry_order_status');
		$this->data['entry_order_status_denied'] = $this->language->get('entry_order_status_denied');
		$this->data['entry_order_status_expired'] = $this->language->get('entry_order_status_expired');
		$this->data['entry_total'] = $this->language->get('entry_total');
		$this->data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['email_client_number'])) {
			$this->data['error_email_client_number'] = $this->error['email_client_number'];
		} else {
			$this->data['error_email_client_number'] = '';
		}

		if (isset($this->error['secret_key'])) {
			$this->data['error_secret_key'] = $this->error['secret_key'];
		} else {
			$this->data['error_secret_key'] = '';
		}

		if (isset($this->error['description'])) {
			$this->data['error_description'] = $this->error['description'];
		} else {
			$this->data['error_description'] = '';
		}

		if (isset($this->error['expired_days'])) {
			$this->data['error_expired_days'] = $this->error['expired_days'];
		} else {
			$this->data['error_expired_days'] = '';
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_payment'),
			'href'      => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('payment/epay', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->data['action'] = $this->url->link('payment/epay', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->post['epay_test'])) {
			$this->data['epay_test'] = $this->request->post['epay_test'];
		} else {
			$this->data['epay_test'] = $this->config->get('epay_test');
		}

		if (isset($this->request->post['epay_email'])) {
			$this->data['epay_email'] = $this->request->post['epay_email'];
		} else {
			$this->data['epay_email'] = $this->config->get('epay_email');
		}

		if (isset($this->request->post['epay_client_number'])) {
			$this->data['epay_client_number'] = $this->request->post['epay_client_number'];
		} else {
			$this->data['epay_client_number'] = $this->config->get('epay_client_number');
		}

		if (isset($this->request->post['epay_secret_key'])) {
			$this->data['epay_secret_key'] = $this->request->post['epay_secret_key'];
		} else {
			$this->data['epay_secret_key'] = $this->config->get('epay_secret_key');
		}

		if (isset($this->request->post['epay_description'])) {
			$this->data['epay_description'] = $this->request->post['epay_description'];
		} else {
			$this->data['epay_description'] = $this->config->get('epay_description');
		}

		if (isset($this->request->post['epay_expired_days'])) {
			$this->data['epay_expired_days'] = $this->request->post['epay_expired_days'];
		} else {
			$this->data['epay_expired_days'] = $this->config->get('epay_expired_days');
		}

		if (isset($this->request->post['epay_total'])) {
			$this->data['epay_total'] = $this->request->post['epay_total'];
		} else {
			$this->data['epay_total'] = $this->config->get('epay_total'); 
		}

		if (isset($this->request->post['epay_order_status_id'])) {
			$this->data['epay_order_status_id'] = $this->request->post['epay_order_status_id'];
		} else {
			$this->data['epay_order_status_id'] = $this->config->get('epay_order_status_id');
		}

		if (isset($this->request->post['epay_order_status_denied_id'])) {
			$this->data['epay_order_status_denied_id'] = $this->request->post['epay_order_status_denied_id'];
		} else {
			$this->data['epay_order_status_denied_id'] = $this->config->get('epay_order_status_denied_id');
		}

		if (isset($this->request->post['epay_order_status_expired_id'])) {
			$this->data['epay_order_status_expired_id'] = $this->request->post['epay_order_status_expired_id'];
		} else {
			$this->data['epay_order_status_expired_id'] = $this->config->get('epay_order_status_expired_id');
		}

		$this->load->model('localisation/order_status');

		$this->data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		if (isset($this->request->post['epay_geo_zone_id'])) {
			$this->data['epay_geo_zone_id'] = $this->request->post['epay_geo_zone_id'];
		} else {
			$this->data['epay_geo_zone_id'] = $this->config->get('epay_geo_zone_id');
		}

		$this->load->model('localisation/geo_zone');

		$this->data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

		if (isset($this->request->post['epay_status'])) {
			$this->data['epay_status'] = $this->request->post['epay_status'];
		} else {
			$this->data['epay_status'] = $this->config->get('epay_status');
		}

		if (isset($this->request->post['epay_sort_order'])) {
			$this->data['epay_sort_order'] = $this->request->post['epay_sort_order'];
		} else {
			$this->data['epay_sort_order'] = $this->config->get('epay_sort_order');
		}

		$this->template = 'payment/epay.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	private function validate() {
		if (!$this->user->hasPermission('modify', 'payment/epay')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->request->post['epay_email'] && !$this->request->post['epay_client_number']) {
			$this->error['email_client_number'] = $this->language->get('error_email_client_number');
		}

		if (!$this->request->post['epay_secret_key']) {
			$this->error['secret_key'] = $this->language->get('error_secret_key');
		}

		if (strlen(utf8_decode($this->request->post['epay_description'])) > 100) {
			$this->error['description'] = $this->language->get('error_description');
		}

		$pattern = '/^[1-9]+[0-9]*$/';

		if (!preg_match($pattern, $this->request->post['epay_expired_days'])) {
			$this->error['expired_days'] = $this->language->get('error_expired_days');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	public function install() {
		@mail('support@extensadev.com', 'ePay Payment Module installed (150123)', HTTP_CATALOG . ' - ' . $this->config->get('config_name') . "\r\n" . 'version - ' . VERSION . "\r\n" . 'IP - ' . $this->request->server['REMOTE_ADDR'], 'MIME-Version: 1.0' . "\r\n" . 'Content-type: text/plain; charset=UTF-8' . "\r\n" . 'From: ' . $this->config->get('config_owner') . ' <' . $this->config->get('config_email') . '>' . "\r\n");
	}
}
?>