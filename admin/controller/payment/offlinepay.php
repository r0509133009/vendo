<?php 
class ControllerPaymentOfflinepay extends Controller {
	private $error = array(); 

	public function index() { 
		$this->language->load('payment/offlinepay');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('offlinepay', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_all_zones'] = $this->language->get('text_all_zones');

		$this->data['entry_order_status'] = $this->language->get('entry_order_status');	
		$this->data['entry_order_paid_status'] = $this->language->get('entry_order_paid_status');		
		$this->data['entry_total'] = $this->language->get('entry_total');	
		$this->data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_header'] = $this->language->get('entry_header');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_payment'),
			'href'      => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('payment/offlinepay', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->data['action'] = $this->url->link('payment/offlinepay', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL');	

		if (isset($this->request->post['offlinepay_total'])) {
			$this->data['offlinepay_total'] = $this->request->post['offlinepay_total'];
		} else {
			$this->data['offlinepay_total'] = $this->config->get('offlinepay_total'); 
		}

		if (isset($this->request->post['offlinepay_order_status_id'])) {
			$this->data['offlinepay_order_status_id'] = $this->request->post['offlinepay_order_status_id'];
		} else {
			$this->data['offlinepay_order_status_id'] = $this->config->get('offlinepay_order_status_id'); 
		}

		if (isset($this->request->post['offlinepay_order_paid_status_id'])) {
			$this->data['offlinepay_order_paid_status_id'] = $this->request->post['offlinepay_order_paid_status_id'];
		} else {
			$this->data['offlinepay_order_paid_status_id'] = $this->config->get('offlinepay_order_paid_status_id'); 
		} 

		$this->load->model('localisation/order_status');

		$this->data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		if (isset($this->request->post['offlinepay_geo_zone_id'])) {
			$this->data['offlinepay_geo_zone_id'] = $this->request->post['offlinepay_geo_zone_id'];
		} else {
			$this->data['offlinepay_geo_zone_id'] = $this->config->get('offlinepay_geo_zone_id'); 
		} 

		$this->load->model('localisation/geo_zone');						

		$this->data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

		if (isset($this->request->post['offlinepay_status'])) {
			$this->data['offlinepay_status'] = $this->request->post['offlinepay_status'];
		} else {
			$this->data['offlinepay_status'] = $this->config->get('offlinepay_status');
		}

		if (isset($this->request->post['offlinepay_sort_order'])) {
			$this->data['offlinepay_sort_order'] = $this->request->post['offlinepay_sort_order'];
		} else {
			$this->data['offlinepay_sort_order'] = $this->config->get('offlinepay_sort_order');
		}
		
		if (isset($this->request->post['offlinepay_logo'])) {
			$this->data['offlinepay_logo'] = $this->request->post['offlinepay_logo'];
		} else {
			$this->data['offlinepay_logo'] = $this->config->get('offlinepay_logo');
		}
		
		if (isset($this->request->post['offlinepay_message'])) {
			$this->data['offlinepay_message'] = $this->request->post['offlinepay_message'];
		} else {
			$this->data['offlinepay_message'] = $this->config->get('offlinepay_message');
		}
		
		if (isset($this->request->post['offlinepay_condition'])) {
			$this->data['offlinepay_condition'] = $this->request->post['offlinepay_condition'];
		} else {
			$this->data['offlinepay_condition'] = $this->config->get('offlinepay_condition');
		}

		$this->template = 'payment/offlinepay.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'payment/offlinepay')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}

	public function install() {
		$this->load->model('payment/offlinepay');
		$this->model_payment_offlinepay->install();
	}
}
?>