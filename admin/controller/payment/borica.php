<?php

/**
 * eBORICA payment gateway for Opencart by Extensa Web Development
 *
 * Copyright © 2011-2015 Extensa Web Development Ltd. All Rights Reserved.
 * This file may not be redistributed in whole or significant part.
 * This copyright notice MUST APPEAR in all copies of the script!
 *
 * @author 		Extensa Web Development Ltd. (www.extensadev.com)
 * @copyright	Copyright (c) 2011-2015, Extensa Web Development Ltd.
 * @package 	eBORICA payment gateway
 * @link		http://www.opencart.com/index.php?route=extension/extension/info&extension_id=3004
 */

class ControllerPaymentBorica extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('payment/borica');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validate())) {
			if (!$this->request->post['borica_state']) {
				$this->request->post['borica_state'] = $this->request->post['borica_locality'];
			}

			if (!$this->request->post['borica_name']) {
				$this->request->post['borica_name'] = $_SERVER['HTTP_HOST'];
			}

			$this->request->post['borica_organization_unit'] = 'Extensa Web Development';

			$this->request->post['borica_test_private_key'] = $this->config->get('borica_test_private_key');
			$this->request->post['borica_test_csr'] = $this->config->get('borica_test_csr');
			$this->request->post['borica_test_certificate'] = $this->config->get('borica_test_certificate');
			$this->request->post['borica_real_private_key'] = $this->config->get('borica_real_private_key');
			$this->request->post['borica_real_csr'] = $this->config->get('borica_real_csr');
			$this->request->post['borica_real_certificate'] = $this->config->get('borica_real_certificate');
			$this->request->post['borica_etlog_private_key'] = $this->config->get('borica_etlog_private_key');
			$this->request->post['borica_etlog_csr'] = $this->config->get('borica_etlog_csr');
			$this->request->post['borica_etlog_certificate'] = $this->config->get('borica_etlog_certificate');

			$this->model_setting_setting->editSetting('borica', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_all_zones'] = $this->language->get('text_all_zones');
		$this->data['text_yes'] = $this->language->get('text_yes');
		$this->data['text_no'] = $this->language->get('text_no');
		$this->data['text_select'] = $this->language->get('text_select');

		$this->data['text_save_before_generate'] = $this->language->get('text_save_before_generate');
		$this->data['text_generate'] = $this->language->get('text_generate');
		$this->data['text_download'] = $this->language->get('text_download');
		$this->data['text_download_ppk'] = $this->language->get('text_download_ppk');
		$this->data['text_send_to_bank'] = $this->language->get('text_send_to_bank');
		$this->data['text_copy_send_to_bank'] = $this->language->get('text_copy_send_to_bank');
		$this->data['text_copy_etlog_certificate'] = $this->language->get('text_copy_etlog_certificate');
		$this->data['text_copy_certificates'] = $this->language->get('text_copy_certificates');
		$this->data['text_import_into_browser'] = $this->language->get('text_import_into_browser');

		$this->data['button_get_p12'] = $this->language->get('button_get_p12');
		$this->data['button_save_certificates'] = $this->language->get('button_save_certificates');

		$this->data['entry_test'] = $this->language->get('entry_test');
		$this->data['entry_terminal'] = $this->language->get('entry_terminal');
		$this->data['entry_description'] = $this->language->get('entry_description');
		$this->data['entry_country'] = $this->language->get('entry_country');
		$this->data['entry_state'] = $this->language->get('entry_state');
		$this->data['entry_locality'] = $this->language->get('entry_locality');
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_email'] = $this->language->get('entry_email');
		$this->data['entry_organization'] = $this->language->get('entry_organization');
		$this->data['entry_organization_unit'] = $this->language->get('entry_organization_unit');
		$this->data['entry_total'] = $this->language->get('entry_total');
		$this->data['entry_order_status'] = $this->language->get('entry_order_status');
		$this->data['entry_order_status_canceled'] = $this->language->get('entry_order_status_canceled');
		$this->data['entry_order_status_canceled_reversal'] = $this->language->get('entry_order_status_canceled_reversal');
		$this->data['entry_order_status_denied'] = $this->language->get('entry_order_status_denied');
		$this->data['entry_order_status_failed'] = $this->language->get('entry_order_status_failed');
		$this->data['entry_currency'] = $this->language->get('entry_currency');
		$this->data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_test_key_csr'] = $this->language->get('entry_test_key_csr');
		$this->data['entry_real_key_csr'] = $this->language->get('entry_real_key_csr');
		$this->data['entry_etlog_key_csr'] = $this->language->get('entry_etlog_key_csr');
		$this->data['entry_test_certificate'] = $this->language->get('entry_test_certificate');
		$this->data['entry_real_certificate'] = $this->language->get('entry_real_certificate');
		$this->data['entry_etlog_certificate'] = $this->language->get('entry_etlog_certificate');
		$this->data['entry_return_url'] = $this->language->get('entry_return_url');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		if ($this->config->get('borica_test_private_key') && $this->config->get('borica_test_csr')) {
			$this->data['text_generate_test_confirm'] = $this->language->get('text_confirm_generate');
		} else {
			$this->data['text_generate_test_confirm'] = '';
		}

		if ($this->config->get('borica_real_private_key') && $this->config->get('borica_real_csr')) {
			$this->data['text_generate_real_confirm'] = $this->language->get('text_confirm_generate');
		} else {
			$this->data['text_generate_real_confirm'] = '';
		}

		if ($this->config->get('borica_etlog_private_key') && $this->config->get('borica_etlog_csr')) {
			$this->data['text_generate_etlog_confirm'] = $this->language->get('text_confirm_generate');
		} else {
			$this->data['text_generate_etlog_confirm'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		if (isset($this->session->data['error'])) {
			$this->data['error'] = $this->session->data['error'];

			unset($this->session->data['error']);
		} else {
			$this->data['error'] = '';
		}

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['terminal'])) {
			$this->data['error_terminal'] = $this->error['terminal'];
		} else {
			$this->data['error_terminal'] = '';
		}

		if (isset($this->error['description'])) {
			$this->data['error_description'] = $this->error['description'];
		} else {
			$this->data['error_description'] = '';
		}

		if (isset($this->error['country'])) {
			$this->data['error_country'] = $this->error['country'];
		} else {
			$this->data['error_country'] = '';
		}

		if (isset($this->error['state'])) {
			$this->data['error_state'] = $this->error['state'];
		} else {
			$this->data['error_state'] = '';
		}

		if (isset($this->error['locality'])) {
			$this->data['error_locality'] = $this->error['locality'];
		} else {
			$this->data['error_locality'] = '';
		}

		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}

		if (isset($this->error['email'])) {
			$this->data['error_email'] = $this->error['email'];
		} else {
			$this->data['error_email'] = '';
		}

		if (isset($this->error['organization'])) {
			$this->data['error_organization'] = $this->error['organization'];
		} else {
			$this->data['error_organization'] = '';
		}

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_payment'),
			'href'      => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('payment/borica', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->data['action'] = $this->url->link('payment/borica', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['generate_test'] = $this->url->link('payment/borica/generate', 'token=' . $this->session->data['token'] . '&type=test', 'SSL');
		$this->data['generate_real'] = $this->url->link('payment/borica/generate', 'token=' . $this->session->data['token'] . '&type=real', 'SSL');
		$this->data['generate_etlog'] = $this->url->link('payment/borica/generate', 'token=' . $this->session->data['token'] . '&type=etlog', 'SSL');
		$this->data['download_test'] = $this->url->link('payment/borica/download', 'token=' . $this->session->data['token'] . '&type=test', 'SSL');
		$this->data['download_real'] = $this->url->link('payment/borica/download', 'token=' . $this->session->data['token'] . '&type=real', 'SSL');
		$this->data['download_etlog'] = $this->url->link('payment/borica/download', 'token=' . $this->session->data['token'] . '&type=etlog', 'SSL');
		$this->data['download_test_ppk'] = $this->url->link('payment/borica/download_ppk', 'token=' . $this->session->data['token'] . '&type=test&support=extensa', 'SSL');
		$this->data['download_real_ppk'] = $this->url->link('payment/borica/download_ppk', 'token=' . $this->session->data['token'] . '&type=real&support=extensa', 'SSL');
		$this->data['download_etlog_ppk'] = $this->url->link('payment/borica/download_ppk', 'token=' . $this->session->data['token'] . '&type=etlog&support=extensa', 'SSL');
		$this->data['return_url'] = HTTP_CATALOG . 'catalog/controller/payment/borica_callback.php';
		$this->data['action_p12'] = $this->url->link('payment/borica/generate_p12', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['action_certificates'] = $this->url->link('payment/borica/save_certificates', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->get['support']) && ($this->request->get['support'] == 'extensa')) {
			$this->data['support'] = true;
		} else {
			$this->data['support'] = false;
		}

		if (isset($this->request->post['borica_test'])) {
			$this->data['borica_test'] = $this->request->post['borica_test'];
		} else {
			$this->data['borica_test'] = $this->config->get('borica_test');
		}

		if (isset($this->request->post['borica_terminal'])) {
			$this->data['borica_terminal'] = $this->request->post['borica_terminal'];
		} else {
			$this->data['borica_terminal'] = $this->config->get('borica_terminal');
		}

		if (isset($this->request->post['borica_description'])) {
			$this->data['borica_description'] = $this->request->post['borica_description'];
		} else {
			$this->data['borica_description'] = $this->config->get('borica_description');
		}

		if (isset($this->request->post['borica_country_id'])) {
			$this->data['borica_country_id'] = $this->request->post['borica_country_id'];
		} else {
			$this->data['borica_country_id'] = $this->config->get('borica_country_id');
		}

		if (isset($this->request->post['borica_state'])) {
			$this->data['borica_state'] = $this->request->post['borica_state'];
		} else {
			$this->data['borica_state'] = $this->config->get('borica_state');
		}

		if (isset($this->request->post['borica_locality'])) {
			$this->data['borica_locality'] = $this->request->post['borica_locality'];
		} else {
			$this->data['borica_locality'] = $this->config->get('borica_locality');
		}

		if (isset($this->request->post['borica_name'])) {
			$this->data['borica_name'] = $this->request->post['borica_name'];
		} else {
			$this->data['borica_name'] = $this->config->get('borica_name');
		}
		if (!$this->data['borica_name']) {
			$this->data['borica_name'] = $_SERVER['HTTP_HOST'];
		}

		if (isset($this->request->post['borica_email'])) {
			$this->data['borica_email'] = $this->request->post['borica_email'];
		} else {
			$this->data['borica_email'] = $this->config->get('borica_email');
		}

		if (isset($this->request->post['borica_organization'])) {
			$this->data['borica_organization'] = $this->request->post['borica_organization'];
		} else {
			$this->data['borica_organization'] = $this->config->get('borica_organization');
		}

		if (isset($this->request->post['borica_organization_unit'])) {
			$this->data['borica_organization_unit'] = $this->request->post['borica_organization_unit'];
		} else {
			$this->data['borica_organization_unit'] = $this->config->get('borica_organization_unit');
		}
		if (!$this->data['borica_organization_unit']) {
			$this->data['borica_organization_unit'] = 'Extensa Web Development';
		}

		if (isset($this->request->post['borica_total'])) {
			$this->data['borica_total'] = $this->request->post['borica_total'];
		} else {
			$this->data['borica_total'] = $this->config->get('borica_total');
		}

		if (isset($this->request->post['borica_order_status_id'])) {
			$this->data['borica_order_status_id'] = $this->request->post['borica_order_status_id'];
		} else {
			$this->data['borica_order_status_id'] = $this->config->get('borica_order_status_id');
		}

		if (isset($this->request->post['borica_order_status_canceled_id'])) {
			$this->data['borica_order_status_canceled_id'] = $this->request->post['borica_order_status_canceled_id'];
		} else {
			$this->data['borica_order_status_canceled_id'] = $this->config->get('borica_order_status_canceled_id');
		}

		if (isset($this->request->post['borica_order_status_canceled_reversal_id'])) {
			$this->data['borica_order_status_canceled_reversal_id'] = $this->request->post['borica_order_status_canceled_reversal_id'];
		} else {
			$this->data['borica_order_status_canceled_reversal_id'] = $this->config->get('borica_order_status_canceled_reversal_id');
		}

		if (isset($this->request->post['borica_order_status_denied_id'])) {
			$this->data['borica_order_status_denied_id'] = $this->request->post['borica_order_status_denied_id'];
		} else {
			$this->data['borica_order_status_denied_id'] = $this->config->get('borica_order_status_denied_id');
		}

		if (isset($this->request->post['borica_order_status_failed_id'])) {
			$this->data['borica_order_status_failed_id'] = $this->request->post['borica_order_status_failed_id'];
		} else {
			$this->data['borica_order_status_failed_id'] = $this->config->get('borica_order_status_failed_id');
		}

		if (isset($this->request->post['borica_currency'])) {
			$this->data['borica_currency'] = $this->request->post['borica_currency'];
		} else {
			$this->data['borica_currency'] = $this->config->get('borica_currency');
		}

		if (isset($this->request->post['borica_geo_zone_id'])) {
			$this->data['borica_geo_zone_id'] = $this->request->post['borica_geo_zone_id'];
		} else {
			$this->data['borica_geo_zone_id'] = $this->config->get('borica_geo_zone_id');
		}

		if (isset($this->request->post['borica_status'])) {
			$this->data['borica_status'] = $this->request->post['borica_status'];
		} else {
			$this->data['borica_status'] = $this->config->get('borica_status');
		}

		if (isset($this->request->post['borica_sort_order'])) {
			$this->data['borica_sort_order'] = $this->request->post['borica_sort_order'];
		} else {
			$this->data['borica_sort_order'] = $this->config->get('borica_sort_order');
		}

		if (isset($this->request->post['borica_test_certificate'])) {
			$this->data['borica_test_certificate'] = $this->request->post['borica_test_certificate'];
		} else {
			$this->data['borica_test_certificate'] = $this->config->get('borica_test_certificate');
		}

		if (isset($this->request->post['borica_real_certificate'])) {
			$this->data['borica_real_certificate'] = $this->request->post['borica_real_certificate'];
		} else {
			$this->data['borica_real_certificate'] = $this->config->get('borica_real_certificate');
		}

		if (isset($this->request->post['borica_etlog_certificate'])) {
			$this->data['borica_etlog_certificate'] = $this->request->post['borica_etlog_certificate'];
		} else {
			$this->data['borica_etlog_certificate'] = $this->config->get('borica_etlog_certificate');
		}

		$this->load->model('localisation/order_status');

		$this->data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		$this->load->model('localisation/currency');

		$this->data['currencies'] = $this->model_localisation_currency->getCurrencies();

		$this->load->model('localisation/geo_zone');

		$this->data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

		$this->load->model('localisation/country');

		$this->data['countries'] = $this->model_localisation_country->getCountries();

		$this->template = 'payment/borica.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	private function validate() {
		if (!$this->user->hasPermission('modify', 'payment/borica')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$terminalIdPattern = '/^[0-9]{8}$/';

		if (!preg_match($terminalIdPattern, $this->request->post['borica_terminal'])) {
			$this->error['terminal'] = $this->language->get('error_terminal');
		}

		if (!$this->request->post['borica_description']) {
			$this->error['description'] = $this->language->get('error_description');
		}

		if ($this->request->post['borica_country_id'] == '') {
			$this->error['country'] = $this->language->get('error_country');
		}

		$latinPattern = '/^[0-9A-Za-z-_\. ]+$/';

		if ($this->request->post['borica_state'] && !preg_match($latinPattern, $this->request->post['borica_state'])) {
			$this->error['state'] = $this->language->get('error_state');
		}

		if (!preg_match($latinPattern, $this->request->post['borica_locality'])) {
			$this->error['locality'] = $this->language->get('error_locality');
		}

		if ($this->request->post['borica_name'] && !preg_match($latinPattern, $this->request->post['borica_name'])) {
			$this->error['name'] = $this->language->get('error_name');
		}

		$emailPattern = '/^[A-Z0-9._%-+]+@[A-Z0-9][A-Z0-9.-]{0,61}[A-Z0-9]\.[A-Z]{2,6}$/i';

		if ((strlen(utf8_decode($this->request->post['borica_email'])) > 96) || (!preg_match($emailPattern, $this->request->post['borica_email']))) {
			$this->error['email'] = $this->language->get('error_email');
		}

		if (!preg_match($latinPattern, $this->request->post['borica_organization'])) {
			$this->error['organization'] = $this->language->get('error_organization');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	public function generate() {
		$this->load->language('payment/borica');

		if ($this->config->get('borica_country_id') &&
			$this->config->get('borica_state') &&
			$this->config->get('borica_locality') &&
			$this->config->get('borica_name') &&
			$this->config->get('borica_email') &&
			$this->config->get('borica_organization') &&
			$this->config->get('borica_organization_unit')) {

			$privateKey = openssl_pkey_new(array('private_key_type' => OPENSSL_KEYTYPE_RSA, 'private_key_bits' => 1024));

			if ($privateKey) {
				openssl_pkey_export($privateKey, $privateKeyString);
			}

			$this->load->model('localisation/country');

			$country_info = $this->model_localisation_country->getCountry($this->config->get('borica_country_id'));

			$dn = array(
				'countryName'            => $country_info['iso_code_2'],
				'stateOrProvinceName'    => $this->config->get('borica_state'),
				'localityName'           => $this->config->get('borica_locality'),
				'commonName'             => $this->config->get('borica_name'),
				'emailAddress'           => $this->config->get('borica_email'),
				'organizationName'       => $this->config->get('borica_organization'),
				'organizationalUnitName' => $this->config->get('borica_organization_unit')
			);

			$csr = openssl_csr_new($dn, $privateKey);

			if ($csr) {
				openssl_csr_export($csr, $csrString);
			}

			if (!empty($privateKeyString) && !empty($csrString)) {
				$this->load->model('setting/setting');

				$data = $this->model_setting_setting->getSetting('borica');

				if ($this->request->get['type'] == 'test') {
					$data['borica_test_private_key'] = $privateKeyString;
					$data['borica_test_csr'] = $csrString;
				} elseif ($this->request->get['type'] == 'real') {
					$data['borica_real_private_key'] = $privateKeyString;
					$data['borica_real_csr'] = $csrString;
				} else if ($this->request->get['type'] == 'etlog') {
					$data['borica_etlog_private_key'] = $privateKeyString;
					$data['borica_etlog_csr'] = $csrString;
				} else {
					$this->session->data['error'] = $this->language->get('error_generate');
				}

				$this->model_setting_setting->editSetting('borica', $data);

				$this->session->data['success'] = $this->language->get('text_success_generate');
			} else {
				$this->session->data['error'] = sprintf($this->language->get('error_openssl'), openssl_error_string());
			}
		} else {
			$this->session->data['error'] = $this->language->get('error_generate');
		}

		$this->redirect($this->url->link('payment/borica', 'token=' . $this->session->data['token'], 'SSL'));
	}

	public function download() {
		$this->load->language('payment/borica');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if ($this->request->get['type'] == 'test') {
			$csrString = $this->config->get('borica_test_csr');
			$fileName = 'apgwtest.csr';
		} elseif ($this->request->get['type'] == 'real') {
			$csrString = $this->config->get('borica_real_csr');
			$fileName = 'apgw.csr';
		} else if ($this->request->get['type'] == 'etlog') {
			$csrString = $this->config->get('borica_etlog_csr');
			$fileName = 'etlog.csr';
		} else {
			$this->session->data['error'] = $this->language->get('error_download');
		}

		if (!empty($fileName) && !empty($csrString)) {
			$this->downloadContents($fileName, $csrString);
		} else {
			$this->session->data['error'] = $this->language->get('error_download');
		}

		$this->redirect($this->url->link('payment/borica', 'token=' . $this->session->data['token'], 'SSL'));
	}

	public function download_ppk() {
		if (isset($this->request->get['support']) && ($this->request->get['support'] == 'extensa')) {
			$this->load->language('payment/borica');

			$this->document->setTitle($this->language->get('heading_title'));

			$this->load->model('setting/setting');

			if ($this->request->get['type'] == 'test') {
				$csrString = $this->config->get('borica_test_private_key');
				$fileName = 'apgwtest.ppk';
			} elseif ($this->request->get['type'] == 'real') {
				$csrString = $this->config->get('borica_real_private_key');
				$fileName = 'apgw.ppk';
			} else if ($this->request->get['type'] == 'etlog') {
				$csrString = $this->config->get('borica_etlog_private_key');
				$fileName = 'etlog.ppk';
			} else {
				$this->session->data['error'] = $this->language->get('error_download');
			}

			if (!empty($fileName) && !empty($csrString)) {
				$this->downloadContents($fileName, $csrString);
			} else {
				$this->session->data['error'] = $this->language->get('error_download');
			}

			$this->redirect($this->url->link('payment/borica', 'token=' . $this->session->data['token'], 'SSL'));
		}
	}

	public function generate_p12() {
		$this->load->language('payment/borica');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (isset($this->request->post['borica_etlog_certificate'])) {
			$this->data['borica_etlog_certificate'] = $this->request->post['borica_etlog_certificate'];
		} else {
			$this->data['borica_etlog_certificate'] = $this->config->get('borica_etlog_certificate');
		}

		if (strpos($this->data['borica_etlog_certificate'], 'BEGIN CERTIFICATE')) {
			$data = $this->model_setting_setting->getSetting('borica');
			$data['borica_etlog_certificate'] = $this->data['borica_etlog_certificate'];
			$this->model_setting_setting->editSetting('borica', $data);

			$privateKeyString = $this->config->get('borica_etlog_private_key');
			if (strpos($privateKeyString, 'BEGIN RSA PRIVATE KEY') || strpos($privateKeyString, 'BEGIN PRIVATE KEY')) {
				$cert = openssl_x509_read($this->data['borica_etlog_certificate']);
				openssl_pkcs12_export($cert, $out, $privateKeyString, null, array('friendly_name' => 'Borica eTLog Certificate'));
				$this->downloadContents('etlog.p12', $out);
			} else {
				$this->session->data['error'] = $this->language->get('error_generate_p12');
			}
		} else {
			$this->session->data['error'] = $this->language->get('error_certificate');
		}

		$this->redirect($this->url->link('payment/borica', 'token=' . $this->session->data['token'], 'SSL'));
	}

	public function save_certificates() {
		$this->load->language('payment/borica');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (isset($this->request->post['borica_test_certificate'])) {
			$this->data['borica_test_certificate'] = $this->request->post['borica_test_certificate'];
		} else {
			$this->data['borica_test_certificate'] = $this->config->get('borica_test_certificate');
		}

		if (isset($this->request->post['borica_real_certificate'])) {
			$this->data['borica_real_certificate'] = $this->request->post['borica_real_certificate'];
		} else {
			$this->data['borica_real_certificate'] = $this->config->get('borica_real_certificate');
		}

		if (strpos($this->data['borica_test_certificate'], 'BEGIN CERTIFICATE') &&
			strpos($this->data['borica_real_certificate'], 'BEGIN CERTIFICATE')) {
			$data = $this->model_setting_setting->getSetting('borica');
			$data['borica_test_certificate'] = $this->data['borica_test_certificate'];
			$data['borica_real_certificate'] = $this->data['borica_real_certificate'];
			$this->model_setting_setting->editSetting('borica', $data);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));
		} else {
			$this->session->data['error'] = $this->language->get('error_certificate');

			$this->redirect($this->url->link('payment/borica', 'token=' . $this->session->data['token'], 'SSL'));
		}
	}

	private function downloadContents($filePath, $contents) {
		header('Pragma: public');
		header('Cache-control: private, must-revalidate, max-age=0');
		header('Expires: 0');
		header('Content-Type: application/octet-stream');
		header('Content-length: ' . strlen($contents));
		header('Content-disposition: attachment; filename="' . basename($filePath) . '"');
		header('Connection: close');

		echo $contents;
		exit;
	}

	public function install() {
		@mail('support@extensadev.com', 'eBORICA Payment Module installed (150123)', HTTP_CATALOG . ' - ' . $this->config->get('config_name') . "\r\n" . 'version - ' . VERSION . "\r\n" . 'IP - ' . $this->request->server['REMOTE_ADDR'], 'MIME-Version: 1.0' . "\r\n" . 'Content-type: text/plain; charset=UTF-8' . "\r\n" . 'From: ' . $this->config->get('config_owner') . ' <' . $this->config->get('config_email') . '>' . "\r\n");
	}
}
?>