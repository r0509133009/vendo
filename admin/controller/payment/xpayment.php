<?php
class ControllerPaymentXpayment extends Controller {
	private $error = array(); 
	
	public function index() {   
		$this->load->language('payment/xpayment');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			
			$save=array();
			$save['xpayment_status']=$this->request->post['xpayment_status'];
			$save['xpayment_sort_order']=$this->request->post['xpayment_sort_order'];
			$save['xpayment_debug']=$this->request->post['xpayment_debug'];
			if(isset($this->request->post['xpayment']))
			$save['xpayment']=serialize($this->request->post['xpayment']);
			$this->model_setting_setting->editSetting('xpayment', $save);		
			$this->session->data['success'] = $this->language->get('text_success');	
			$this->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));
		}
			
				
		$this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['tab_rate'] = $this->language->get('tab_rate');
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_order_total'] = $this->language->get('entry_order_total');
		$this->data['entry_order_weight'] = $this->language->get('entry_order_weight');
		$this->data['entry_to'] = $this->language->get('entry_to');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_all_zones'] = $this->language->get('text_all_zones');
		$this->data['text_none'] = $this->language->get('text_none');
		
		$this->data['entry_manufacturer'] = $this->language->get('entry_manufacturer');
		$this->data['store_default'] = $this->language->get('store_default');
		$this->data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$this->data['entry_store'] = $this->language->get('entry_store');
		$this->data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$this->data['text_all'] = $this->language->get('text_all');
		$this->data['text_category'] = $this->language->get('text_category');
		$this->data['text_category_exact'] = $this->language->get('text_category_exact');
		$this->data['text_category_except'] = $this->language->get('text_category_except');
		$this->data['text_category_all'] = $this->language->get('text_category_all');
		$this->data['text_category_least'] = $this->language->get('text_category_least');
		$this->data['entry_category'] = $this->language->get('entry_category');
		$this->data['text_category_any'] = $this->language->get('text_category_any');
		$this->data['entry_weight_include'] = $this->language->get('entry_weight_include');
		$this->data['entry_desc'] = $this->language->get('entry_desc');
		$this->data['text_select_all'] = $this->language->get('text_select_all');
		$this->data['text_unselect_all'] = $this->language->get('text_unselect_all');
		$this->data['text_any'] = $this->language->get('text_any');
		$this->data['entry_instruction'] = $this->language->get('entry_instruction');
		$this->data['entry_order_hints'] = $this->language->get('entry_order_hints');
		$this->data['keywords_hints'] = $this->language->get('keywords_hints');
		$this->data['entry_order_status'] = $this->language->get('entry_order_status');
		$this->data['entry_callback'] = $this->language->get('entry_callback');
		
		$this->data['text_zip_postal'] = $this->language->get('text_zip_postal');
		$this->data['text_enter_zip'] = $this->language->get('text_enter_zip');
		$this->data['text_zip_rule'] = $this->language->get('text_zip_rule');
		$this->data['text_zip_rule_inclusive'] = $this->language->get('text_zip_rule_inclusive');
		$this->data['text_zip_rule_exclusive'] = $this->language->get('text_zip_rule_exclusive');
		
		
		
		
        $this->data['tip_sorting_own'] = $this->language->get('tip_sorting_own');
        $this->data['tip_status_own'] = $this->language->get('tip_status_own');
        $this->data['tip_store'] = $this->language->get('tip_store');
        $this->data['tip_geo'] = $this->language->get('tip_geo');
        $this->data['tip_manufacturer'] = $this->language->get('tip_manufacturer');
        $this->data['tip_customer_group'] = $this->language->get('tip_customer_group');
        $this->data['tip_zip'] = $this->language->get('tip_zip');
        $this->data['tip_coupon'] = $this->language->get('tip_coupon');
        $this->data['tip_category'] = $this->language->get('tip_category');
        $this->data['tip_product'] = $this->language->get('tip_product');
        $this->data['tip_weight'] = $this->language->get('tip_weight');
        $this->data['tip_total'] = $this->language->get('tip_total');
        $this->data['tip_desc'] = $this->language->get('tip_desc');
        $this->data['tip_desc'] = $this->language->get('tip_desc');
        $this->data['tip_order_status'] = $this->language->get('tip_order_status'); 
        $this->data['tip_callback'] = $this->language->get('tip_callback'); 
        $this->data['tip_instruction'] = $this->language->get('tip_instruction');
        $this->data['tip_status_global'] = $this->language->get('tip_status_global');
        $this->data['tip_sorting_global'] = $this->language->get('tip_sorting_global');
        $this->data['text_debug'] = $this->language->get('text_debug');
        $this->data['tip_debug'] = $this->language->get('tip_debug');
        $this->data['text_inc_email'] = $this->language->get('text_inc_email');
        $this->data['text_instruction_email'] = $this->language->get('text_instruction_email');
        $this->data['tip_email_instruction'] = $this->language->get('tip_email_instruction');
        
        $this->data['tip_redirect'] = $this->language->get('tip_redirect');
        $this->data['tip_redirect_data'] = $this->language->get('tip_redirect_data');
        $this->data['entry_redirect'] = $this->language->get('entry_redirect');
        $this->data['entry_redirect_type'] = $this->language->get('entry_redirect_type');
        $this->data['entry_redirect_post'] = $this->language->get('entry_redirect_post');
        $this->data['entry_redirect_get'] = $this->language->get('entry_redirect_get');
        $this->data['entry_success'] = $this->language->get('entry_success');
        $this->data['tip_success'] = $this->language->get('tip_success');
        
        
        $this->data['text_inc_order'] = $this->language->get('text_inc_order');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['tab_general'] = $this->language->get('tab_general');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_payment'),
			'href'      => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('payment/xpayment', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('payment/xpayment', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL');
		
		$xpayment=$this->config->get('xpayment');
		if($xpayment)
		$xpayment=unserialize($xpayment);
		
		if(!is_array($xpayment))$xpayment=array();
		$this->data['xpayment'] = $xpayment;
		
		$this->data['token']=$this->session->data['token'];
		
		
		 
		 if (isset($this->request->post['xpayment_status'])) {
					$this->data['xpayment_status'] = $this->request->post['xpayment_status'];
				} else {
					$this->data['xpayment_status'] = $this->config->get('xpayment_status');
				}
		if (isset($this->request->post['xpayment_sort_order'])) {
					$this->data['xpayment_sort_order'] = $this->request->post['xpayment_sort_order'];
				} else {
					$this->data['xpayment_sort_order'] = $this->config->get('xpayment_sort_order');
				}						
        if (isset($this->request->post['xpayment_debug'])) {
			$this->data['xpayment_debug'] = $this->request->post['xpayment_debug'];
          } else {
            $this->data['xpayment_debug'] = $this->config->get('xpayment_debug');
         }
		
		
		$this->load->model('localisation/tax_class');
		$this->data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();
		
		$this->load->model('localisation/geo_zone');
		$this->data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();
                
        $this->load->model('setting/store');
		$this->data['stores'] = $this->model_setting_store->getStores();
        $this->data['stores']=  array_merge(array(array('store_id'=>0,'name'=>$this->language->get('store_default'))),$this->data['stores']);
                
        $this->load->model('sale/customer_group');
		$this->data['customer_groups'] = $this->model_sale_customer_group->getCustomerGroups();
                
        $this->load->model('catalog/manufacturer');
		$this->data['manufacturers'] = $this->model_catalog_manufacturer->getManufacturers();
                
		$this->load->model('localisation/language');
		$this->data['languages'] = $this->model_localisation_language->getLanguages();
		$this->data['language_id']=$this->config->get('config_language_id');
		
		$this->load->model('localisation/order_status');
		$this->data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
		
		
		$this->data['form_data']=$this->getFormData();
								
		$this->template = 'payment/xpayment.tpl';
		$this->children = array(
			'common/header',
			'common/footer',
		);
		
				
		$this->response->setOutput($this->render());
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'payment/xpayment')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
	
	public function get_categories() {
		$json = array();
		
		if (isset($this->request->get['filter_name'])) {
			
			
		
			$results = $this->getCategories($this->request->get['filter_name']);
				
			foreach ($results as $result) {
				$json[] = array(
					'category_id' => $result['category_id'], 
					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();
	  
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}
	
	private function getCategories($filter='') {
		
	    
		
			$category_data = array();
		        $where=($filter)?" and lower(cd.name) like '" . $this->db->escape(strtolower($filter)) . "%'":'';
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) WHERE cd.language_id = '" . (int)$this->config->get('config_language_id') . "' $where ORDER BY c.sort_order, cd.name ASC limit 0, 20");
			
			foreach ($query->rows as $result) {
				$category_data[] = array(
					'category_id' => $result['category_id'],
					'name'        => $this->getPath($result['category_id']),
					'status'  	  => $result['status'],
					'sort_order'  => $result['sort_order']
				);
			
			
		}
		
		return $category_data;
	}
	
	private function getPath($category_id) {
	     
		$query = $this->db->query("SELECT name, parent_id FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) WHERE c.category_id = '" . (int)$category_id . "'  AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY c.sort_order, cd.name ASC limit 0, 10");
		
		if ($query->row['parent_id']) {
			return $this->getPath($query->row['parent_id'], $this->config->get('config_language_id')) . $this->language->get('text_separator') . $query->row['name'];
		} else {
			return $query->row['name'];
		}
	} 
	
	private function getFormData()
	{
	   $this->load->model('catalog/category');
	   
	   $return='';
	   if(!isset($this->data['xpayment']['name']))$this->data['xpayment']['name']=array();
	   if(!is_array($this->data['xpayment']['name']))$this->data['xpayment']['name']=array();
	   foreach($this->data['xpayment']['name'] as $no_of_tab=>$names){
	   	 
		 if(!isset($this->data['xpayment']['customer_group'][$no_of_tab]))$this->data['xpayment']['customer_group'][$no_of_tab]=array();
		 if(!isset($this->data['xpayment']['geo_zone_id'][$no_of_tab]))$this->data['xpayment']['geo_zone_id'][$no_of_tab]=array();
		 if(!isset($this->data['xpayment']['product_category'][$no_of_tab]))$this->data['xpayment']['product_category'][$no_of_tab]=array();
		 if(!isset($this->data['xpayment']['store'][$no_of_tab]))$this->data['xpayment']['store'][$no_of_tab]=array();
		 if(!isset($this->data['xpayment']['manufacturer'][$no_of_tab]))$this->data['xpayment']['manufacturer'][$no_of_tab]=array();

		 
		 if(!is_array($this->data['xpayment']['customer_group'][$no_of_tab]))$this->data['xpayment']['customer_group'][$no_of_tab]=array();
		 if(!is_array($this->data['xpayment']['geo_zone_id'][$no_of_tab]))$this->data['xpayment']['geo_zone_id'][$no_of_tab]=array();
		 if(!is_array($this->data['xpayment']['product_category'][$no_of_tab]))$this->data['xpayment']['product_category'][$no_of_tab]=array();
		 if(!is_array($this->data['xpayment']['store'][$no_of_tab]))$this->data['xpayment']['store'][$no_of_tab]=array();
		 if(!is_array($this->data['xpayment']['manufacturer'][$no_of_tab]))$this->data['xpayment']['manufacturer'][$no_of_tab]=array();
		
		 if(!is_array($names))$names=array();
		 
		 if(!isset($this->data['xpayment']['desc'][$no_of_tab]))$this->data['xpayment']['desc'][$no_of_tab]=array();
		 if(!is_array($this->data['xpayment']['desc'][$no_of_tab]))$this->data['xpayment']['desc'][$no_of_tab]=array();
		 
		 if(!isset($this->data['xpayment']['instruction'][$no_of_tab]))$this->data['xpayment']['instruction'][$no_of_tab]=array();
		 if(!is_array($this->data['xpayment']['instruction'][$no_of_tab]))$this->data['xpayment']['instruction'][$no_of_tab]=array();
		 
		 if(!isset($this->data['xpayment']['customer_group_all'][$no_of_tab]))$this->data['xpayment']['customer_group_all'][$no_of_tab]='';
		 if(!isset($this->data['xpayment']['geo_zone_all'][$no_of_tab]))$this->data['xpayment']['geo_zone_all'][$no_of_tab]='';
		 if(!isset($this->data['xpayment']['store_all'][$no_of_tab]))$this->data['xpayment']['store_all'][$no_of_tab]='';
		 if(!isset($this->data['xpayment']['manufacturer_all'][$no_of_tab]))$this->data['xpayment']['manufacturer_all'][$no_of_tab]='';
		 if(!isset($this->data['xpayment']['postal_all'][$no_of_tab]))$this->data['xpayment']['postal_all'][$no_of_tab]='';

		 if(!isset($this->data['xpayment']['inc_email'][$no_of_tab]))$this->data['xpayment']['inc_email'][$no_of_tab]='';
		 if(!isset($this->data['xpayment']['inc_order'][$no_of_tab]))$this->data['xpayment']['inc_order'][$no_of_tab]='';
		 
		 if(!isset($this->data['xpayment']['redirect'][$no_of_tab]))$this->data['xpayment']['redirect'][$no_of_tab]='';
		 if(!isset($this->data['xpayment']['success'][$no_of_tab]))$this->data['xpayment']['success'][$no_of_tab]='';
		 if(!isset($this->data['xpayment']['redirect_type'][$no_of_tab]))$this->data['xpayment']['redirect_type'][$no_of_tab]='post';
		
		 
		  $return.='<div id="payment-'.$no_of_tab.'" class="vtabs-content payment">';
		 
		  $return.='<div class="htabs">';
          foreach ($this->data['languages'] as $language) {
             $return.='<a href="#language'.$language['language_id'].'_'.$no_of_tab.'"><img src="view/image/flags/'.$language['image'].'" title="'.$language['name'].'" /> '.$language['name'].'</a>';
          } 
           $return.='</div>';
		   
		   $inc=0;
		   foreach ($this->data['languages'] as $language) {
		        $lang_cls=($inc==0)?'':'-lang'; $inc++;
			    if(!isset($names[$language['language_id']]) || !$names[$language['language_id']])$names[$language['language_id']]='Untitled Payment '.$no_of_tab;     
				if(!isset($this->data['xpayment']['desc'][$no_of_tab][$language['language_id']]) || !$this->data['xpayment']['desc'][$no_of_tab][$language['language_id']])$this->data['xpayment']['desc'][$no_of_tab][$language['language_id']]='';
				if(!isset($this->data['xpayment']['email_instruction'][$no_of_tab][$language['language_id']]) || !$this->data['xpayment']['email_instruction'][$no_of_tab][$language['language_id']])$this->data['xpayment']['email_instruction'][$no_of_tab][$language['language_id']]='';
				
			    $return.='<div id="language'.$language['language_id'].'_'.$no_of_tab.'">'
		            .'<table class="form">'
				  .'<tr>'
					.'<td>'.$this->data['entry_name'].'</td>'
					.'<td><input type="text" class="method-name'.$lang_cls.'" size="45" name="xpayment[name]['.$no_of_tab.']['.$language['language_id'].']" value="'.$names[$language['language_id']].'" /></td>'
				  .'</tr>'
				  .'<tr>'
					.'<td>'.$this->data['entry_desc'].' <a class="help" tips="'.$this->data['tip_desc'].'">?</a></td>'
					.'<td><input type="text" size="45" name="xpayment[desc]['.$no_of_tab.']['.$language['language_id'].']" value="'.$this->data['xpayment']['desc'][$no_of_tab][$language['language_id']].'" /></td>'
				  .'</tr>'
				  .'<tr>'
					.'<td>'.$this->data['entry_instruction'].' <a class="help" tips="'.$this->data['tip_instruction'].'">?</a></td>'
					.'<td><textarea rows="10" cols="100" name="xpayment[instruction]['.$no_of_tab.']['.$language['language_id'].']">'.$this->data['xpayment']['instruction'][$no_of_tab][$language['language_id']].'</textarea></td>'
				  .'</tr>'
				   .'<tr>'
					.'<td>'.$this->data['text_instruction_email'].' <a class="help" tips="'.$this->data['tip_email_instruction'].'">?</a></td>'
					.'<td><textarea rows="10" cols="100" name="xpayment[email_instruction]['.$no_of_tab.']['.$language['language_id'].']">'.$this->data['xpayment']['email_instruction'][$no_of_tab][$language['language_id']].'</textarea></td>'
				  .'</tr>'
				   .'</table>'
		          .'</div>';
			  }	  
				  
			$return.='<table class="form">'
			        .'<tr>'
                     .'<td>'.$this->data['text_inc_email'].'</td>'
                     .'<td><input '.(($this->data['xpayment']['inc_email'][$no_of_tab]=='1')?'checked="checked"':'').' type="checkbox" name="xpayment[inc_email]['.$no_of_tab.']" value="1" /></td>'
                   .'</tr>'
                   .'<tr>'
                     .'<td>'.$this->data['text_inc_order'].'</td>'
                     .'<td><input '.(($this->data['xpayment']['inc_order'][$no_of_tab]=='1')?'checked="checked"':'').' type="checkbox" name="xpayment[inc_order]['.$no_of_tab.']" value="1" /></td>'
                   .'</tr>'
			       .'<tr>'
					.'<td>'.$this->data['entry_order_status'].'  <a class="help" tips="'.$this->data['tip_order_status'].'">?</a></td>'
					.'<td><select name="xpayment[order_status_id]['.$no_of_tab.']">';
				      foreach ($this->data['order_statuses'] as $order_status) { 
						$return.='<option '.(($this->data['xpayment']['order_status_id'][$no_of_tab]==$order_status['order_status_id'])?'selected':'').' value="'.$order_status['order_status_id'].'">'.$order_status['name'].'</option>';
					   } 
					 $return.='</select></td>'
				  .'</tr>'
				  .'<tr>'
                    .'<td>'.$this->data['entry_callback'].' <a class="help" tips="'.$this->data['tip_callback'].'">?</a></td>'
                    .'<td><input size="45"  type="text" name="xpayment[callback]['.$no_of_tab.']" value="'.$this->data['xpayment']['callback'][$no_of_tab].'" />'
                    .'</td>'
                  .'</tr>'
                 .'<tr>' 
                   .'<td>'.$this->data['entry_redirect'].' <a class="help" tips="'.$this->data['tip_redirect'].'">?</a></td>'
                .'<td><input size="45"  type="text" name="xpayment[redirect]['.$no_of_tab.']" value="'.$this->data['xpayment']['redirect'][$no_of_tab].'" />'
                .'</td>'
               .'</tr>'  
                .'<tr>'
                .'<td>'.$this->data['entry_redirect_type'].' <a class="help" tips="'.$this->data['tip_redirect_data'].'">?</a></td>'
                .'<td>'
                 .'<label><input type="radio" '.(($this->data['xpayment']['redirect_type'][$no_of_tab]=='post')?'checked="checked"':'').' name="xpayment[redirect_type]['.$no_of_tab.']" value="post" />&nbsp;'.$this->data['entry_redirect_post'].'</label>'
                 .'<label><input type="radio" '.(($this->data['xpayment']['redirect_type'][$no_of_tab]=='get')?'checked="checked"':'').'  name="xpayment[redirect_type]['.$no_of_tab.']" value="get" />&nbsp;'.$this->data['entry_redirect_get'].'</label>'
                .'</td>'
               .'</tr>'  
                .'<tr>' 
                   .'<td>'.$this->data['entry_success'].' <a class="help" tips="'.$this->data['tip_success'].'">?</a></td>'
                   .'<td><input size="45" type="text" name="xpayment[success]['.$no_of_tab.']" value="'.$this->data['xpayment']['success'][$no_of_tab].'" />'
                .'</td>'
               .'</tr>'  
			      .'<tr>'
                .'<td>'.$this->data['entry_store'].' <a class="help" tips="'.$this->data['tip_store'].'">?</a></td>' 
                 .'<td>'
		 .'<label class="any-class"><input '.(($this->data['xpayment']['store_all'][$no_of_tab]=='1')?'checked="checked"':'').' type="checkbox" name="xpayment[store_all]['.$no_of_tab.']" class="choose-any" value="1" />&nbsp;'.$this->data['text_any'].'</label>'
		 .'<div class="scrollbox-wrapper" '.(($this->data['xpayment']['store_all'][$no_of_tab]!='1')?'style="display:block"':'').'>'
		 .'<div class="scrollbox">';
                
                    $class = 'even';
                    foreach ($this->data['stores'] as $store) {
                        $class = ($class == 'even' ? 'odd' : 'even');
                    
		          $return.='<div class="'.$class.'">'
                   .'<input '.((in_array($store['store_id'],$this->data['xpayment']['store'][$no_of_tab]))?'checked':'').' type="checkbox" name="xpayment[store]['.$no_of_tab.'][]" value="'.$store['store_id'].'" />&nbsp;'.$store['name']
		        .'</div>';
                  } 
                $return.='</div>'
		       .'<a onclick="$(this).parent().find(\':checkbox\').attr(\'checked\', true);">'.$this->data['text_select_all'].'</a>'
	          .'/ <a onclick="$(this).parent().find(\':checkbox\').attr(\'checked\', false);">'.$this->data['text_unselect_all'].'</a>'
	          .'</div></td>'
                .'</tr>'
                .'<tr>'
                 .'<td>'.$this->data['entry_geo_zone'].' <a class="help" tips="'.$this->data['tip_geo'].'">?</a></td>' 
                 .'<td>'
		 .'<label class="any-class"><input '.(($this->data['xpayment']['geo_zone_all'][$no_of_tab]=='1')?'checked="checked"':'').' type="checkbox" name="xpayment[geo_zone_all]['.$no_of_tab.']" class="choose-any" value="1" />&nbsp;'.$this->data['text_any'].'</label>'
		 .'<div class="scrollbox-wrapper" '.(($this->data['xpayment']['geo_zone_all'][$no_of_tab]!='1')?'style="display:block"':'').'>'
		 .'<div class="scrollbox">';
                  
                    $class = 'even';
                    foreach ($this->data['geo_zones'] as $geo_zone) {
                    $class = ($class == 'even' ? 'odd' : 'even');
		  
		 $return.='<div class="'.$class.'">'
                  .'<input '.((in_array($geo_zone['geo_zone_id'],$this->data['xpayment']['geo_zone_id'][$no_of_tab]))?'checked':'').' type="checkbox" name="xpayment[geo_zone_id]['.$no_of_tab.'][]" value="'.$geo_zone['geo_zone_id'].'" />&nbsp;'.$geo_zone['name']
		  .'</div>';
                  } 
         $return.='</div>'
	         .'<a onclick="$(this).parent().find(\':checkbox\').attr(\'checked\', true);">'.$this->data['text_select_all'].'</a>'
		 .'/ <a onclick="$(this).parent().find(\':checkbox\').attr(\'checked\', false);">'.$this->data['text_unselect_all'].'</a>'
		 .'</div></td>'
                .'</tr>'
                .'<tr>'
                 .'<td>'.$this->data['entry_manufacturer'].' <a class="help" tips="'.$this->data['tip_manufacturer'].'">?</a></td>' 
                 .'<td>'
                 .'<label class="any-class"><input '.(($this->data['xpayment']['manufacturer_all'][$no_of_tab]=='1')?'checked="checked"':'').' type="checkbox" name="xpayment[manufacturer_all]['.$no_of_tab.']" class="choose-any" value="1" />&nbsp;'.$this->data['text_any'].'</label>'
                 .'<div class="scrollbox-wrapper" '.(($this->data['xpayment']['manufacturer_all'][$no_of_tab]!='1')?'style="display:block"':'').'>'
                 .'<div class="scrollbox">';
                 
                    $class = 'even';
                    foreach ($this->data['manufacturers'] as $manufacturer) {
                     $class = ($class == 'even' ? 'odd' : 'even');
                     
		        $return.='<div class="'.$class.'">'
                 .'<input '.((in_array($manufacturer['manufacturer_id'],$this->data['xpayment']['manufacturer'][$no_of_tab]))?'checked':'').' type="checkbox" name="xpayment[manufacturer]['.$no_of_tab.'][]" value="'.$manufacturer['manufacturer_id'].'" />&nbsp;'.$manufacturer['name']
		 .'</div>';
                 } 
          $return.='</div>'
	        .'<a onclick="$(this).parent().find(\':checkbox\').attr(\'checked\', true);">'.$this->data['text_select_all'].'</a>'
	        .'/ <a onclick="$(this).parent().find(\':checkbox\').attr(\'checked\', false);">'.$this->data['text_unselect_all'].'</a>'
		.'</div></td>'
               .'</tr>'
               .'<tr>'
                 .'<td>'.$this->data['entry_customer_group'].' <a class="help" tips="'.$this->data['tip_customer_group'].'">?</a></td>'
                 .'<td>'
                 .'<label class="any-class"><input '.(($this->data['xpayment']['customer_group_all'][$no_of_tab]=='1')?'checked="checked"':'').' type="checkbox" name="xpayment[customer_group_all]['.$no_of_tab.']" class="choose-any" value="1" />&nbsp;'.$this->data['text_any'].'</label>'
                 .'<div class="scrollbox-wrapper" '.(($this->data['xpayment']['customer_group_all'][$no_of_tab]!='1')?'style="display:block"':'').'>'
                 .'<div class="scrollbox">';
                
                    $class = 'even';
                    foreach ($this->data['customer_groups'] as $customer_group) {
                      $class = ($class == 'even' ? 'odd' : 'even');
		
		$return.='<div class="'.$class.'">'
                 .'<input '.((in_array($customer_group['customer_group_id'],$this->data['xpayment']['customer_group'][$no_of_tab]))?'checked':'').' type="checkbox" name="xpayment[customer_group]['.$no_of_tab.'][]" value="'.$customer_group['customer_group_id'].'" />&nbsp;'.$customer_group['name']        
                 .'</div>';
                  } 
          $return.='</div>'
		 .'<a onclick="$(this).parent().find(\':checkbox\').attr(\'checked\', true);">'.$this->data['text_select_all'].'</a>'
	         .'/ <a onclick="$(this).parent().find(\':checkbox\').attr(\'checked\', false);">'.$this->data['text_unselect_all'].'</a>'
	         .'</div></td>'
               .'</tr>'
                .'<tr>'
                 .'<td>'.$this->data['text_zip_postal'].' <a class="help" tips="'.$this->data['tip_zip'].'">?</a></td>'
                 .'<td>'
	             .'<label class="any-class"><input '.(($this->data['xpayment']['postal_all'][$no_of_tab]=='1')?'checked="checked"':'').' type="checkbox" name="xpayment[postal_all]['.$no_of_tab.']" class="choose-zip" value="1" />&nbsp;'.$this->data['text_any'].'</label>'
                  .'</td>'
               .'</tr>'
               .'<tr class="postal-option" '.(($this->data['xpayment']['postal_all'][$no_of_tab]!='1')?'style="display:table-row"':'').'>'
                .'<td>'.$this->data['text_enter_zip'].'</td>'
                .'<td><textarea name="xpayment[postal]['.$no_of_tab.']" rows="8" cols="70" />'.$this->data['xpayment']['postal'][$no_of_tab].'</textarea></td>'
               .'</tr>'
               .'<tr class="postal-option" '.(($this->data['xpayment']['postal_all'][$no_of_tab]!='1')?'style="display:table-row"':'').'>'
                .'<td>'.$this->data['text_zip_rule'].'</td>'
                .'<td><select name="xpayment[postal_rule]['.$no_of_tab.']">'
                    .'<option value="inclusive" '.(($this->data['xpayment']['postal_rule'][$no_of_tab]=='inclusive')?'selected':'').'>'.$this->data['text_zip_rule_inclusive'].'</option>'
                    .'<option value="exclusive" '.(($this->data['xpayment']['postal_rule'][$no_of_tab]=='exclusive')?'selected':'').'>'.$this->data['text_zip_rule_exclusive'].'</option>'
                  .'</select></td>'
               .'</tr>'
				.'<tr>'
				  .'<td>'.$this->data['text_category'].' <a width="500" class="help" tips="'.$this->data['tip_category'].'">?</a></td>'
				  .'<td><select class="category-selection category" name="xpayment[category]['.$no_of_tab.']">'
					  .'<option value="1" '.(($this->data['xpayment']['category'][$no_of_tab]==1)?'selected':'').'>'.$this->data['text_category_any'].'</option>'
					  .'<option value="2" '.(($this->data['xpayment']['category'][$no_of_tab]==2)?'selected':'').'>'.$this->data['text_category_all'].'</option>'
					  .'<option value="3" '.(($this->data['xpayment']['category'][$no_of_tab]==3)?'selected':'').'>'.$this->data['text_category_least'].'</option>'
					  .'<option value="4" '.(($this->data['xpayment']['category'][$no_of_tab]==4)?'selected':'').'>'.$this->data['text_category_exact'].'</option>'
					  .'<option value="5" '.(($this->data['xpayment']['category'][$no_of_tab]==5)?'selected':'').'>'.$this->data['text_category_except'].'</option>'
					.'</select></td>'
				.'</tr>'
				.'<tr class="category" '.(($this->data['xpayment']['category'][$no_of_tab]!=1)?'style="display:table-row"':'').'>'
				  .'<td>'.$this->data['entry_category'].'</td>'
				  .'<td><input type="text" name="category" value="" /></td>'
				.'</tr>'
				.'<tr class="category" '.(($this->data['xpayment']['category'][$no_of_tab]!=1)?'style="display:table-row"':'').'>'
				  .'<td>&nbsp;</td>'
				  .'<td><div class="scrollbox product-category">';
				  foreach ($this->data['xpayment']['product_category'][$no_of_tab] as $category_id) {
						   $category_name=$this->getPath($category_id);
						   $return.='<div class="product-category'.$category_id. '">'.$category_name.'<img src="view/image/delete.png" alt="" />'
						     .'<input type="hidden" class="category" name="xpayment[product_category]['.$no_of_tab.'][]" value="'.$category_id.'" /></div>';
						}
			      $return.='</div></td>'
				.'</tr>'
				   .'<tr>'
					.'<td>'.$this->data['entry_order_total'].'  <a class="help" tips="'.$this->data['tip_total'].'">?</a></td>'
					.'<td><input size="15" type="text" name="xpayment[order_total_start]['.$no_of_tab.']" value="'.$this->data['xpayment']['order_total_start'][$no_of_tab].'" /> &nbsp;'.$this->data['entry_to'].'&nbsp; <input size="15" type="text" name="xpayment[order_total_end]['.$no_of_tab.']" value="'.$this->data['xpayment']['order_total_end'][$no_of_tab].'" />&nbsp;&nbsp;['.$this->data['entry_order_hints'].']</td>'
				  .'</tr>'
				  .'<tr>'
				  .'<td>'.$this->data['entry_order_weight'].'  <a class="help" tips="'.$this->data['tip_weight'].'">?</a></td>'
					.'<td><input size="15" type="text" name="xpayment[weight_start]['.$no_of_tab.']" value="'.$this->data['xpayment']['weight_start'][$no_of_tab].'" /> &nbsp;'.$this->data['entry_to'].'&nbsp; <input size="15" type="text" name="xpayment[weight_end]['.$no_of_tab.']" value="'.$this->data['xpayment']['weight_end'][$no_of_tab].'" />&nbsp;&nbsp;['.$this->data['entry_order_hints'].']</td>'
				  .'</tr>'
				  .'<tr>'
					.'<td>'.$this->data['entry_sort_order'].'  <a class="help" tips="'.$this->data['tip_status_own'].'">?</a></td>'
					.'<td><input type="text" name="xpayment[sort_order]['.$no_of_tab.']" value="'.$this->data['xpayment']['sort_order'][$no_of_tab].'" size="1" /></td>'
				  .'</tr>'
				  .'<tr>'
					  .'<td>'.$this->data['entry_status'].'  <a class="help" tips="'.$this->data['tip_status_own'].'">?</a></td>'
					  .'<td><select name="xpayment[status]['.$no_of_tab.']">'
						  .'<option value="1" '.(($this->data['xpayment']['status'][$no_of_tab]==1 || $this->data['xpayment']['status'][$no_of_tab]=='')?'selected':'').'>'.$this->data['text_enabled'].'</option>'
						  .'<option value="0" '.(($this->data['xpayment']['status'][$no_of_tab]==0)?'selected':'').'>'.$this->data['text_disabled'].'</option>'
						.'</select></td>'
					.'</tr>'
				.'</table>'
				.'</div>';
		}
		
		return $return;		
	}
}
?>