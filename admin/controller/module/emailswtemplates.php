<?php
class ControllerModuleEmailswtemplates extends Controller {	
private $error = array();



	 	public function index() {
			$this->load->model('module/emailswtemplates'); 
		
		if(isset($_GET['action'])){
			switch( $_GET['action'] )
			{   
				case "upload_file":
				if($_FILES){
				
						$uploaddir = $_SERVER['DOCUMENT_ROOT'].'/download/tmp/'; 
						$file = $uploaddir . basename($_FILES['uploadfile']['name']); 
						if (!file_exists($uploaddir)) {
							mkdir($_SERVER['DOCUMENT_ROOT'].'/download/tmp', 0777, true);
						}
						if (move_uploaded_file($_FILES['uploadfile']['tmp_name'], $file)) { 
						
						
							if (file_exists($file )){            
							  $file_array = file($file);
					 		  $status = basename($file);
							  $count = 0;
							  foreach($file_array as $this->data){
								  $field = explode(',', $this->data);
								//  echo '<pre>';
								//  print_r($field );
								//  echo '</pre>';
									if(!empty($field['2']) && filter_var(trim($field['2']), FILTER_VALIDATE_EMAIL)){
																			
										if(  $this->model_module_emailswtemplates->UploadSubscriberFromFile($field) ){
											$count ++;
										}
									}else{}
								}
								$status = $count;
								unlink ($file);
							} 
						} else {  
							$status = "error";
						}
							  
						echo $status;
						exit;
					break;
				}

			}
		}
		
		$this->load->language('module/emailswtemplates');	
		$this->document->setTitle($this->language->get('heading_title'));	
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_newsletter'] = $this->language->get('text_newsletter');	
		$this->data['text_newsletter_c'] = $this->model_module_emailswtemplates->newsletter_count();	
		$this->data['text_nynewsletter_c'] = $this->model_module_emailswtemplates->ny_newsletter_count();	
		$this->data['text_module_newsletter'] = $this->language->get('text_module_newsletter');	
		$this->data['text_test_email'] = $this->language->get('text_test_email');	
	    $this->data['text_customer_all'] = $this->language->get('text_customer_all');	
	    $this->data['text_customer'] = $this->language->get('text_customer');	
	    $this->data['column_left'] = '';	

		$this->data['column_id'] = $this->language->get('column_id');
		$this->data['column_to'] = $this->language->get('column_to');
		$this->data['column_subject'] = $this->language->get('column_subject');
		
		$this->data['help_customer'] = $this->language->get('help_customer');
		$this->data['help_affiliate'] = $this->language->get('help_affiliate');
		$this->data['help_product'] = $this->language->get('help_product');
		
		$this->data['delete_history'] = $this->url->link('module/emailswtemplates/delete', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['view_all_history'] = $this->language->get('view_all_history');
		$this->data['text_customer_group'] = $this->language->get('text_customer_group');
		$this->data['text_affiliate_all'] = $this->language->get('text_affiliate_all');	
		$this->data['text_affiliate'] = $this->language->get('text_affiliate');		
		$this->data['text_edit'] = $this->language->get('text_edit');		
		$this->data['text_product'] = $this->language->get('text_product');		
		$this->data['entry_example_text'] = $this->language->get('entry_example_text');	
		$this->data['column_date'] = $this->language->get('column_date');	
		$this->data['entry_alowed_format'] = $this->language->get('entry_alowed_format');	
		$this->data['entry_test_email'] = $this->language->get('entry_test_email');	
		$this->data['entry_store'] = $this->language->get('entry_store');	
		$this->data['entry_template'] = $this->language->get('entry_template');		
		$this->data['entry_to'] = $this->language->get('entry_to');	
		$this->data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$this->data['entry_customer'] = $this->language->get('entry_customer');		
		$this->data['entry_affiliate'] = $this->language->get('entry_affiliate');	
		$this->data['entry_product'] = $this->language->get('entry_product');	
		$this->data['entry_subject'] = $this->language->get('entry_subject');		
		$this->data['entry_message'] = $this->language->get('entry_message');	
		$this->data['text_no_results'] = $this->language->get('text_no_results');	
		$this->data['entry_edit'] = $this->language->get('entry_edit');	
		$this->data['entry_view'] = $this->language->get('entry_view');	
		$this->data['entry_action'] = $this->language->get('entry_action');	
		$this->data['edit_template'] = $this->language->get('edit_template');	
		$this->data['edit_subscribers'] = $this->language->get('edit_subscribers');	
		$this->data['button_send'] = $this->language->get('button_send');	
		$this->data['button_delete'] = $this->language->get('button_delete');	
		$this->data['button_delete_history'] = $this->language->get('button_delete_history');	
		$this->data['text_export'] = $this->language->get('text_export');	
		$this->data['button_cancel'] = $this->language->get('button_cancel');	
		$this->data['token'] = $this->session->data['token'];  	
		
		$this->load->model('localisation/language');
		$this->data['languages'] = $this->model_localisation_language->getLanguages();
			
	    
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
    	$this->data['breadcrumbs'] = array();   
		$this->data['breadcrumbs'][] = array(       
		'text'      => $this->language->get('text_home'),	
		'href'      => $this->url->link('common/home',		'token=' . $this->session->data['token'], 'SSL'), 
		'separator' => false   	
		);	
	    $this->data['breadcrumbs'][] = array(       	
		'text'      => $this->language->get('text_module'),	'href'   => $this->url->link('extension/module',	'token=' . $this->session->data['token'], 'SSL'),  'separator' => ' :: '  
 		);  
 		$this->data['breadcrumbs'][] = array( 
		'text'      => $this->language->get('heading_title'),	'href'  => $this->url->link('module/emailswtemplates', 	'token=' . $this->session->data['token'], 'SSL'),      		'separator' => ' :: '
   		); 

		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		$this->load->model('setting/store');	
		$this->data['stores'] = $this->model_setting_store->getStores();	
		$this->data['templates'] = $this->model_module_emailswtemplates-> getsettingTemplates();
	
		
		if(isset($this->request->get['page'])) {
	            $page = $this->request->get['page'];
		}else{
		        $page = 1; 
		}
		
		
		
		
		$this->data['history'] = $this->model_module_emailswtemplates->getshortHistory();
 
		
		$this->load->model('sale/customer_group');		 
		$this->data['customer_groups'] = $this->model_sale_customer_group->getCustomerGroups(0);	
				

		
		
		
		$this->load->model('design/layout');
		$this->data['layouts'] = $this->model_design_layout->getLayouts();
		$this->template = 'module/emailswtemplates.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
		
	}

	
	
		public function send() {
		//$this->load->model('module/emailtemplbuilder'); 
		$this->load->model('module/emailswtemplates'); 
        $this->model_module_emailswtemplates->createTableHistory();
		
		$this->load->language('module/emailswtemplates');	
		$json = array();	
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {	
		if (!$this->user->hasPermission('modify', 'module/emailswtemplates')) {
		$json['error']['warning'] = $this->language->get('error_permission');	
		}	
		if (!$this->request->post['subject']) {		
		$json['error']['subject'] = $this->language->get('error_subject');	
		}	
		if (!$this->request->post['message']) {		
		$json['error']['message'] = $this->language->get('error_message');		
		}	

		if (!$json) {		
			$this->load->model('setting/store');	
			
			$this->load->model('sale/customer');		
			$this->load->model('sale/customer_group');	
			$this->load->model('sale/affiliate');	
			$this->load->model('sale/order');
			
			if (isset($this->request->get['page'])) {
				$page = $this->request->get['page'];		
			} else {			
				$page = 1;		
			} 
			
			$email_total = 0;	
			$emails = array();	
			switch ($this->request->post['to']) {
			
		
			case 'test_email':
				$emails[] = $this->request->post['test_email'];					
			break;

			case 'newsletter':
						$customer_data = array(
							'filter_newsletter' => 1,
							'start'             => ($page - 1) * 10,
							'limit'             => 10
						);

						$email_total = $this->model_sale_customer->getTotalCustomers($customer_data);

						$results = $this->model_sale_customer->getCustomers($customer_data);

						foreach ($results as $result) {
							$emails[] = $result['email'];
						}
			break;
			
			case 'module_newsletter':
			$emails[] =$this->model_module_emailswtemplates-> getny_newsletter_subscribers();
					
			break;

	
			case 'customer_all':		
			$customer_data = array(
					'start'  => ($page - 1) * 10,
					'limit'  => 10				
					);				
			$email_total = $this->model_sale_customer->getTotalCustomers($customer_data);		
			$results = $this->model_sale_customer->getCustomers($customer_data);	
			foreach ($results as $result) {
									$emails[] = $result['email'];	
				}			
			
			break;

	
			case 'customer_group':	
				$customer_data = array(
						'filter_customer_group_id' => $this->request->post['customer_group_id'],	
						'start'                    => ($page - 1) * 10,		
						'limit'         => 10			
						);	
			$email_total = $this->model_sale_customer->getTotalCustomers($customer_data);	
			$results = $this->model_sale_customer->getCustomers($customer_data);	
			foreach ($results as $result) {			
			$emails[$result['customer_id']] = $result['email'];		
			}
				break;

		
			case 'customer':	
			if (!empty($this->request->post['customer'])) {		
			foreach ($this->request->post['customer'] as $customer_id) {	
			$customer_info = $this->model_sale_customer->getCustomer($customer_id);		
			if ($customer_info) {			
			$emails[] = $customer_info['email'];	
			}		
			}		
			}		
			break;
		
			case 'affiliate_all':	
			$affiliate_data = array(
			'start'  => ($page - 1) * 10,	
			'limit'  => 10				
			);
			$email_total = $this->model_sale_affiliate->getTotalAffiliates($affiliate_data);
			$results = $this->model_sale_affiliate->getAffiliates($affiliate_data);		
			foreach ($results as $result) {		
			$emails[] = $result['email'];		
			}
			break;

		
			case 'affiliate':	
			if (!empty($this->request->post['affiliate'])) {	
			foreach (
				$this->request->post['affiliate'] as $affiliate_id) {	
				$affiliate_info = $this->model_sale_affiliate->getAffiliate($affiliate_id);																
				if ($affiliate_info) {		
					$emails[] = $affiliate_info['email'];		
					}					
					}				
					}			
			break;
		
			case 'product':			
			if (isset($this->request->post['product'])) {	
			$email_total = $this->model_sale_order->getTotalEmailsByProductsOrdered($this->request->post['product']);		
			$results = $this->model_sale_order->getEmailsByProductsOrdered($this->request->post['product'], ($page - 1) * 10, 10);																				
			foreach ($results as $result) {				
			$emails[] = $result['email'];	
			}
			}		
			break;
		
		}	


			/*echo '<pre>';
		print_r($emails);
		echo '</pre>';*/
		
		
		if ($emails) {	
		$start = ($page - 1) * 10;
		$end = $start + 10;		
		
		if ($end < $email_total) {	
		$json['success'] = sprintf($this->language->get('text_sent'), $start, $email_total);
		} else { 
		$json['success'] = $this->language->get('text_success');	
		}	
		
		
		if ($end < $email_total) {
				$json['success'] = sprintf($this->language->get('text_sent'), $start, $email_total);
		} else {
				$json['success'] = $this->language->get('text_success');
		}	
		
		if ($end < $email_total) {
			$json['next'] = str_replace('&amp;', '&', $this->url->link('marketing/contact/send', 'token=' . $this->session->data['token'] . '&page=' . ($page + 1), 'SSL'));
		} else {
			$json['next'] = '';
		}
			$this->model_module_emailswtemplates->savetoHistory($this->request->post['subject'],$this->request->post['message'], $this->request->post['to'] );
			
			
			
			$message  = '<html dir="ltr" lang="en">' . "\n";	
			$message .= '  <head>' . "\n";
			$message .= '    <title>' . $this->request->post['subject'] . '</title>' . "\n";		
			$message .= '    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">' . "\n";
			$message .= '  </head>' . "\n";				
			$message .= '  <body>' . html_entity_decode($this->request->post['message'], ENT_QUOTES, 'UTF-8') . '</body>' . "\n";	
			$message .= '</html>' . "\n";
			
			foreach ($emails as $email) {			
						if (preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $email)) {
							$mail = new Mail($this->config->get('config_mail'));
							$mail->setTo($email);
							$mail->setFrom(  $this->request->post['from_email']  );
							$mail->setSender( $this->config->get('config_name') );
							$mail->setSubject($this->request->post['subject']);
							$mail->setHtml($message);
							$mail->send();
						}
			}	
			
			}		
			}		
			}	
			$this->response->addHeader('Content-Type: application/json');
		   $this->response->setOutput(json_encode($json));
			}
			
			
			
public function delete() { 
    		$this->load->language('module/emailswtemplates');
			$this->load->model('module/emailswtemplates'); 
	    	$this->document->setTitle($this->language->get('heading_title'));		
			
			if (isset($this->request->post['selected'])) {
			//print_r($this->request->post);
			

      		foreach ($this->request->post['selected'] as $id) {
				$this->model_module_emailswtemplates->deleteEmailHistory($id);	
			}
			$this->session->data['success'] = $this->language->get('text_success');
			$url = '';
			
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'].'';
			}


				$this->redirect($this->url->link('module/emailswtemplates', 'token=' . $this->session->data['token'].$url.'&tab=history', 'SSL'));
			
		}
		
	}
	

	
public function templates() {
			$this->load->model('module/emailswtemplates');
			$this->model_module_emailswtemplates->checkSettingTableType();
		
			$this->load->language('module/emailswtemplates');
			$this->load->model('setting/setting');
					
			if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
				$this->model_setting_setting->editSetting('emailswtemplates', $this->request->post);		
				$this->session->data['success'] = $this->language->get('text_success');
			}
			$this->data['action'] = $this->url->link('module/emailswtemplates/templates', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['token'] = $this->session->data['token'];

			$this->data['tab_module'] = $this->language->get('tab_module_templ');
			$this->data['button_module_add'] = $this->language->get('button_module_add');
			$this->data['heading_title'] = $this->language->get('heading_title');
			$this->data['button_back_to'] = $this->language->get('button_back_to');
			$this->data['entry_subject'] = $this->language->get('entry_subject');
			$this->data['entry_heading'] = $this->language->get('entry_heading');
			$this->data['entry_status'] = $this->language->get('entry_status');
			$this->data['entry_message'] = $this->language->get('entry_message');
			$this->data['entry_description'] = $this->language->get('entry_description');
			$this->data['text_edit'] = $this->language->get('text_edit');
			if (isset($this->error['warning'])) {
				$this->data['error_warning'] = $this->error['warning'];
			} else {
				$this->data['error_warning'] = '';
			}
			$this->data['button_save'] = $this->language->get('button_save');
			$this->data['button_cancel'] = $this->language->get('button_cancel');
			$this->data['cancel'] = $this->url->link('module/emailswtemplates', 'token=' . $this->session->data['token'], 'SSL');
			$this->document->setTitle($this->language->get('heading_title'));
			
			$this->data['breadcrumbs'] = array();   
			$this->data['breadcrumbs'][] = array(       
			'text'      => $this->language->get('text_home'),	
			'href'      => $this->url->link('common/home',		'token=' . $this->session->data['token'], 'SSL'), 
			'separator' => false   	
			);	

			$this->data['breadcrumbs'][] = array(       	
			'text'      => $this->language->get('text_module'),	'href'   => $this->url->link('extension/module',	'token=' . $this->session->data['token'], 'SSL'),  'separator' => ' :: '  
			);  

			$this->data['breadcrumbs'][] = array( 
			'text'      => $this->language->get('heading_title'),	'href'  => $this->url->link('module/emailswtemplates', 	'token=' . $this->session->data['token'], 'SSL'),      		'separator' => ' :: '
			);
			$this->load->model('setting/store');	
			$this->data['stores'] = $this->model_setting_store->getStores();
			
			$this->data['modules'] = array();
			if (isset($this->request->post['emailswtemplates_module'])) {
				$modules = $this->request->post['emailswtemplates_module'];
			} elseif ($this->config->get('emailswtemplates_module')) { 
				$modules = $this->config->get('emailswtemplates_module');
			} else {
				$modules = array();
			}
			
			$this->data['emailswtemplates_modules'] = array();
		
	
		foreach ($modules as $key => $module) {
			$this->data['emailswtemplates_modules'][$key] = array(
				'key'         => $key,
				'description' => $module['description']
			);
		}
		
			$this->load->model('localisation/language');
		    $this->data['languages'] = $this->model_localisation_language->getLanguages();
		
			
			
			
			
			
			
		$this->load->model('design/layout');
		$this->data['layouts'] = $this->model_design_layout->getLayouts();
		$this->template = 'module/emailswtemplates_templates.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	
	}
	
	public function view_all_history() {
		$this->load->model('module/emailswtemplates');
		$this->load->language('module/emailswtemplates');
		$this->document->setTitle($this->language->get('heading_title_view_history'));
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['view_all_history'] = $this->language->get('view_all_history');	
		$this->data['button_cancel'] =  $this->language->get('button_cancel');
		
		$this->data['cancel'] = $this->url->link('module/emailswtemplates', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['breadcrumbs'] = array(); 
 
  		$this->data['breadcrumbs'][] = array(
       		'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('text_home'),
      		'separator' => FALSE
   		);

  		$this->data['breadcrumbs'][] = array(
       		'href'      => $this->url->link('module/emailswtemplates', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('heading_title'),
      		'separator' => ' :: '
   		);
		
		
							
	
		$this->data['delete'] = $this->url->link('module/emailswtemplates/delete_history', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['text_export'] = $this->language->get('text_export');
		if(isset($this->request->get['page'])) {
	            $page = $this->request->get['page'];
		}else{
		        $page = 1;
		}
	
		$this->data['emails'] = array();
		$email_total = $this->model_module_emailswtemplates->getTotalHistory($this->data);
		$results = $this->model_module_emailswtemplates->getHistoryrecords($this->data,($page - 1) * $this->config->get('config_admin_limit'),$this->config->get('config_admin_limit'));
        
		if($email_total>0){
		foreach ($results as $result) {
			$action = array();
			
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('module/emailswtemplates/view_history', 'token=' . $this->session->data['token'] . '&id=' . $result['id'].' &back=view_all_history', 'SSL')
			);		
		
			$this->data['emails'][] = array(
				'id' 		=> $result['id'],
				'subject' 	=> $result['subject'],
				'date' 	=> $result['date'],
				'content' 	=> $result['content'],
				'to' 	=> $result['to'],
				'action'    => $action
			);
		}	
	   }
			
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['column_date'] = $this->language->get('column_date');

		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_email'] = $this->language->get('column_email');
		
		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
 
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		$url = "";	
		$pagination = new Pagination();
		$pagination->total = $email_total;
		$pagination->page  = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text  = $this->language->get('text_pagination');
		$pagination->url   = $this->url->link('module/emailswtemplates/view_all_history', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();
		$this->data['sort_name'] = $this->url->link('module/emailswtemplates/', 'token=' . $this->session->data['token']);
	   

        	
	
		$this->load->model('design/layout');
		$this->data['layouts'] = $this->model_design_layout->getLayouts();
		$this->template = 'module/emailswtemplates_allhistory.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}
	
	
		public function delete_history() { 
		$this->load->language('module/emailswtemplates');

		$this->document->setTitle($this->language->get('heading_title'));		
		
		$this->load->model('module/emailswtemplates');
		
		if (isset($this->request->post['selected'])) {
      		foreach ($this->request->post['selected'] as $id) {
				$this->model_module_emailswtemplates->deleteEmailHistory($id);	
			}
						
			$this->session->data['success'] = $this->language->get('text_success');
			
			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
				$this->redirect($this->url->link('module/emailswtemplates/view_all_history', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}
	
	    //single history record
public function view_history() { 
			$this->load->model('module/emailswtemplates');
			$this->load->language('module/emailswtemplates');
			$this->data['button_cancel'] = $this->language->get('button_cancel');	
			$this->data['heading_title'] = $this->language->get('heading_title_view_history'); 
			$this->data['button_back_to'] = $this->language->get('button_back_to');
			$this->data['entry_subject'] = $this->language->get('entry_subject');
			$this->data['entry_message'] = $this->language->get('entry_message');
			$this->data['view_history_item'] = $this->language->get('view_history_item');
			$this->document->setTitle($this->language->get('heading_title'));
			
			$this->data['info'] = $this->model_module_emailswtemplates->getHistorybyid($this->request->get['id']);	
			$this->data['cancel'] = $this->url->link('module/emailswtemplates/view_all_history', 'token=' . $this->session->data['token'], 'SSL');
			

			
			if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
			} else {
				$this->data['error_warning'] = '';
			}

			$this->data['breadcrumbs'] = array();   
			$this->data['breadcrumbs'][] = array(       
			'text'      => $this->language->get('text_home'),	
			'href'      => $this->url->link('common/home',		'token=' . $this->session->data['token'], 'SSL'), 
			'separator' => false   	
			);	

		   $this->data['breadcrumbs'][] = array(       	
			'text'      => $this->language->get('text_module'),	'href'   => $this->url->link('extension/module',	'token=' . $this->session->data['token'], 'SSL'),  'separator' => ' :: '  
			);  

			$this->data['breadcrumbs'][] = array( 
			'text'      => $this->language->get('heading_title'),	'href'  => $this->url->link('module/emailswtemplates', 	'token=' . $this->session->data['token'], 'SSL'),      		'separator' => ' :: '
			);
			$this->load->model('setting/store');	
			$this->data['stores'] = $this->model_setting_store->getStores();
			
		

			
			
			
		$this->load->model('design/layout');
		$this->data['layouts'] = $this->model_design_layout->getLayouts();
		$this->template = 'module/emailswtemplates_history.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
		
	}
	
	public function subscribers() { 
		$this->load->language('module/emailswtemplates');
		$this->document->setTitle($this->language->get('heading_title_subscribers'));		
		$this->load->model('module/emailswtemplates');
		$this->getListSubscribers();	

	
	
		}
		
		
public function insert_subscribers() {
		$this->load->language('module/emailswtemplates');
		$this->document->setTitle($this->language->get('heading_title_insert_subscribers'));
		$this->load->model('module/emailswtemplates');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateSubscribersForm()) {
			$this->model_module_emailswtemplates->addSubscriber($this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
     		$url = '';
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
			$this->redirect($this->url->link('module/emailswtemplates/subscribers', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
		$this->getFormSubscribers();
	}
	
		
public function update_subscribers() {
		$this->load->language('module/emailswtemplates');

		$this->document->setTitle($this->language->get('heading_title'));		
		
		$this->load->model('module/emailswtemplates');
		
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateSubscribersForm()) {
			$this->model_module_emailswtemplates->editSubscribers($this->request->get['id'], $this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');
			
			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
				$this->redirect($this->url->link('module/emailswtemplates/subscribers', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}

		$this->getFormSubscribers();
}
	
public function export_subscribers_xls() {
		$this->load->model('module/emailswtemplates');
		$contents="First Name \t Last Name \t Email \n";
		$results = $this->model_module_emailswtemplates->exportSubscribers();
		$filename ="Newsletter_subscribers_".date("Y-m-d").".xls";
		if($results) {
			foreach($results as $results){
				$contents .= implode("\t",$results)."\n";	
			}
		}else{
			$contents = $this->language->get('text_no_results');
		}
		header('Content-type: application/ms-excel');
		header('Content-Disposition: attachment; filename='.$filename);
		echo $contents;
		
	}
	
public function export_subscribers_csv() {
		
		$this->load->model('module/emailswtemplates');
		
		$contents="First Name , Last Name , Email \n";
		
		$results = $this->model_module_emailswtemplates->exportSubscribers();
		
		$filename ="Newsletter_subscribers_".date("Y-m-d").".csv";
		if($results) {
			foreach($results as $results){
				$contents .= implode(",",$results)."\n";	
			}
		}else{
			$contents = $this->language->get('text_no_results');
		}
		header('Content-type: application/ms-excel');
		header('Content-Disposition: attachment; filename='.$filename);
		echo $contents;
		
	}
	
	
public function delete_subscribers() { 
		$this->load->language('module/emailswtemplates');

		$this->document->setTitle($this->language->get('heading_title'));		
		
		$this->load->model('module/emailswtemplates');
		
		if (isset($this->request->post['selected'])) {
      		foreach ($this->request->post['selected'] as $id) {
				$this->model_module_emailswtemplates->deleteSubscribers($id);	
			}
						
			$this->session->data['success'] = $this->language->get('text_success');
			
			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
				$this->redirect($this->url->link('module/emailswtemplates/subscribers', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}

		$this->getListSubscribers();
	}
	
private function getListSubscribers() {
			if (isset($this->error['warning'])) {
				$this->data['error_warning'] = $this->error['warning'];
			} else {
				$this->data['error_warning'] = '';
			}
		
			$this->data['breadcrumbs'][] = array(       	
			'text'      => $this->language->get('text_module'),	'href'   => $this->url->link('extension/module',	'token=' . $this->session->data['token'], 'SSL'),  'separator' => ' :: '  
			);  

			$this->data['breadcrumbs'][] = array( 
			'text'      => $this->language->get('heading_title'),	'href'  => $this->url->link('module/emailswtemplates', 	'token=' . $this->session->data['token'], 'SSL'),      		'separator' => ' :: '
			);
			$this->load->model('setting/store');	
			$this->data['stores'] = $this->model_setting_store->getStores();
			$this->data['button_cancel'] = $this->language->get('button_cancel');	
			$this->data['cancel'] = $this->url->link('module/emailswtemplates', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['insert'] = $this->url->link('module/emailswtemplates/insert_subscribers', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['delete'] = $this->url->link('module/emailswtemplates/delete_subscribers', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['export_xls'] = $this->url->link('module/emailswtemplates/export_subscribers_xls', 'token=' . $this->session->data['token'], 'SSL');	
			$this->data['export_csv'] = $this->url->link('module/emailswtemplates/export_subscribers_csv', 'token=' . $this->session->data['token'], 'SSL');	
					
			$this->data['text_export_xls'] = $this->language->get('text_export_xls');
			$this->data['text_export_csv'] = $this->language->get('text_export_csv');
			$this->data['subscr_panel_title'] = $this->language->get('subscr_panel_title');
			
		if(isset($this->request->get['page'])) {
	            $page = $this->request->get['page'];
		}else{
		        $page = 1;
		}

		$this->data['emails'] = array();
		$email_total = $this->model_module_emailswtemplates->getTotalSubscribers($this->data);
		$results = $this->model_module_emailswtemplates->getSubscribers($this->data,($page - 1) * $this->config->get('config_admin_limit'),$this->config->get('config_admin_limit'));

		foreach ($results as $result) {
			$action = array();
			
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('module/emailswtemplates/update_subscribers', 'token=' . $this->session->data['token'] . '&id=' . $result['id'], 'SSL')
			);		
		
			$this->data['emails'][] = array(
				'id' 		=> $result['id'],
				'name' 	=> $result['name'],
				'email' 	=> $result['email_id'],
				'action'    => $action
			);
		}	
	
		$this->data['heading_title'] = $this->language->get('heading_title_subscribers');
		
		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_email'] = $this->language->get('column_email');
		
		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
 
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		$url = "";		$pagination = new Pagination();
		$pagination->total = $email_total;
		$pagination->page  = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text  = $this->language->get('text_pagination');
		$pagination->url   = $this->url->link('module/emailswtemplates/subscribers', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['sort_name'] = $this->url->link('module/emailswtemplates/subscribers', 'token=' . $this->session->data['token']);
		
		
	
		
		
		
		
		$this->load->model('design/layout');
		$this->data['layouts'] = $this->model_design_layout->getLayouts();
		$this->template = 'module/emailswtemplates_subscribers.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
		
 	}
	
private function getFormSubscribers() {
		$this->data['heading_title'] = $this->language->get('heading_title_insert_subscribers');
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_code'] = $this->language->get('entry_code');
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['tab_general'] = $this->language->get('tab_general');
		$this->data['entry_last_name'] = $this->language->get('entry_last_name');
		$this->data['entry_first_name'] = $this->language->get('entry_first_name');
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

 		if (isset($this->error['error_email_name'])) {
			$this->data['error_email_name'] = $this->error['error_email_name'];
		} else {
			$this->data['error_email_name'] = '';
		}
		
 		if (isset($this->error['error_email_exist'])) {
			$this->data['error_email_exist'] = $this->error['error_email_exist'];
		} else {
			$this->data['error_email_exist'] = '';
		}
		
		

  		$this->data['breadcrumbs'] = array();

  		$this->data['breadcrumbs'][] = array(
       		'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('text_home'),
      		'separator' => FALSE
   		);

  		$this->data['breadcrumbs'][] = array(
       		'href'      => $this->url->link('module/emailswtemplates', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('heading_title'),
      		'separator' => ' :: '
   		);
			
		if (!isset($this->request->get['id'])) {
			$this->data['action'] = $this->url->link('module/emailswtemplates/insert_subscribers', 'token=' . $this->session->data['token'], 'SSL');
		} else {
			$this->data['action'] = $this->url->link('module/emailswtemplates/update_subscribers', 'token=' . $this->session->data['token'] . '&id=' . $this->request->get['id'], 'SSL');
		}
		
		$this->data['token'] = $this->session->data['token'];
		  
    	$this->data['cancel'] = $this->url->link('module/emailswtemplates/subscribers', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$subscriber_info = $this->model_module_emailswtemplates->getSubscriber($this->request->get['id']);
		}
 
		if (isset($this->request->post['email_name'])) {
			$this->data['email_name'] = $this->request->post['email_name'];
		} elseif (isset($subscriber_info)) {
			$this->data['email_name'] = $subscriber_info['name'];
		} else {
			$this->data['email_name'] = '';
		}
		
		if (isset($this->request->post['last_name'])) {
			$this->data['last_name'] = $this->request->post['last_name'];
		} elseif (isset($subscriber_info)) {
			$this->data['last_name'] = $subscriber_info['last_name'];
		} else {
			$this->data['last_name'] = '';
		}
		
		if (isset($this->request->post['email_id'])) {
			$this->data['email_id'] = $this->request->post['email_id'];
		} elseif (isset($subscriber_info)) {
			$this->data['email_id'] = $subscriber_info['email_id'];
		} else {
			$this->data['email_id'] = '';
		}
			
		

		
		
		$this->load->model('design/layout');
		$this->data['layouts'] = $this->model_design_layout->getLayouts();
		$this->template = 'module/emailswtemplates_subscribers_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
		$this->response->setOutput($this->render());
		
	}
		
	
private function validate() {
		if (!$this->user->hasPermission('modify', 'module/emailswtemplates')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
	
private function validateSubscribersForm() {
		
	$this->load->model('module/emailswtemplates');
		
		if (!$this->user->hasPermission('modify', 'module/emailswtemplates')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

	 if(!filter_var($this->request->post['email_id'],FILTER_VALIDATE_EMAIL)){
			$this->error['error_email_name'] = $this->language->get('error_email_name');
		}
		
	 	if(isset($this->request->get['id']) and $this->request->get['id']!=""){
		    
			if($this->model_module_emailswtemplates->checkmailSubscribers($this->request->post['email_id'],$this->request->get['id']))
			 $this->error['error_email_exist'] = $this->language->get('error_email_exist');
			 
		}else{
			
		   if($this->model_module_emailswtemplates->checkmailSubscribers($this->request->post['email_id']))
		   $this->error['error_email_exist'] = $this->language->get('error_email_exist');
		 
		}

		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	
}
?>