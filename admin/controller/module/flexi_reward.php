<?php

class ControllerModuleFlexiReward extends Controller { 
	public $flexi_config = array();
	private $error = array(); 
	
	public function index() {  
	
		$this->load->language('module/flexi_reward');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
		
		$flexi_name = array_key_exists( 'flexi_group', $this->request->get ) ? 'flexi_reward_'.$this->request->get['flexi_group'] : 'flexi_reward';
		//var_dump( $flexi_name, $this->request->post, $this->config->get($flexi_name) ); 
		
		$this->flexi_config = $this->config->get($flexi_name);
		
		//var_dump($this->config);
		//var_dump( $flexi_name, $this->flexi_config ); 	
	
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {	
			
			$this->model_setting_setting->editSetting('flexi_rewards_customer_group_'.$this->request->get['flexi_group'], array("$flexi_name"=> $this->request->post));	
		
			$this->session->data['success'] = $this->language->get('text_success');
									
			$this->redirect(HTTPS_SERVER . 'index.php?route=extension/module&token=' . ($this->session->data['token']));
				
		}
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_none'] = $this->language->get('text_none');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		
		$this->data['entry_rate'] = $this->language->get('entry_rate');
		$this->data['entry_flexi_rewardclass'] = $this->language->get('entry_flexi_rewardclass');
		$this->data['entry_purchase_text'] = $this->language->get('entry_purchase_text');
		
		$this->data['entry_status'] = $this->language->get('entry_status');
        $this->data['entry_order_status'] = $this->language->get('entry_order_status');
        $this->data['entry_exp_days_init_text'] = $this->language->get('entry_exp_days_init_text');
        $this->data['entry_exp_days_text'] = $this->language->get('entry_exp_days_text');
		$this->data['entry_exp_days_price_multiplier'] = $this->language->get('entry_exp_days_price_multiplier');
       
	    $this->data['entry_redeem_min_order_amount'] = $this->language->get('entry_redeem_min_order_amount');
	    $this->data['entry_redeem_max'] = $this->language->get('entry_redeem_max');

	    $this->data['entry_ignore_product_points_value'] = $this->language->get('entry_ignore_product_points_value');
	    $this->data['entry_ignore_product_rewards_value'] = $this->language->get('entry_ignore_product_rewards_value');

	    $this->data['text_head_ignorepoints'] = $this->language->get('text_head_ignorepoints');
	    $this->data['text_head_ignore_description'] = $this->language->get('text_head_ignore_description');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add_rule'] = $this->language->get('button_add_rule');
		$this->data['button_remove'] = $this->language->get('button_remove');
		
		$this->data['tab_general'] = $this->language->get('tab_general');
        $this->data['tab_setting'] = $this->language->get('tab_setting');
        $this->data['text_head_useractions'] = $this->language->get('text_head_useractions');

		$this->data['entry_registration'] = $this->language->get('entry_registration');
		$this->data['entry_newsletter_signup'] = $this->language->get('entry_newsletter_signup');
		$this->data['entry_productreview'] = $this->language->get('entry_productreview');
		$this->data['entry_firstorder'] = $this->language->get('entry_firstorder');
		$this->data['entry_firstorder_operator'] = $this->language->get('entry_firstorder_operator');

		$this->data['entry_total_subtotal_operator'] = $this->language->get('entry_total_subtotal_operator');


        $this->load->model('localisation/order_status');
		$this->data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		$this->load->model('sale/customer_group');
		$this->data['customer_groups'] = $this->model_sale_customer_group->getCustomerGroups();

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'href'      => HTTPS_SERVER . 'index.php?route=common/home',
       		'text'      => $this->language->get('text_home'),
      		'separator' => FALSE
   		);

   		$this->data['breadcrumbs'][] = array(
       		'href'      => HTTPS_SERVER . 'index.php?route=extension/module',
       		'text'      => $this->language->get('text_module'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'href'      => HTTPS_SERVER . 'index.php?route=module/flexi_reward',
       		'text'      => $this->language->get('heading_title'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = HTTPS_SERVER . 'index.php?route=module/flexi_reward&token=' . ($this->session->data['token']);
		
		$this->data['cancel'] = HTTPS_SERVER . 'index.php?route=extension/module&token=' . ($this->session->data['token']);

                if (isset($this->request->post['flexi_reward_rate'])) {
				$this->data['flexi_reward_rate'] = $this->request->post['flexi_reward_rate'];
			} else {
				$this->data['flexi_reward_rate'] = $this->flexi_config['flexi_reward_rate'];
			}
			
            if (isset($this->request->post['flexi_purchase_rate'])) {
				$this->data['flexi_purchase_rate'] = $this->request->post['flexi_purchase_rate'];
			} else {
				$this->data['flexi_purchase_rate'] = $this->flexi_config['flexi_purchase_rate'];
			}
			
		$this->data['flexi_group'] = 'no';
		$this->data['flexi_group_name'] = 'no';
		
		if (isset($this->request->get['flexi_group'])) {
			foreach($this->data['customer_groups'] as $group) {
				if($group['customer_group_id'] == $this->request->get['flexi_group']) {
					$this->data['flexi_group_name'] = $group['name'];
					$this->data['flexi_group'] = $this->request->get['flexi_group'];
				} 
			} 
		} 
			
                
		if (isset($this->request->post['flexi_reward_status'])) {
			$this->data['flexi_reward_status'] = $this->request->post['flexi_reward_status'];
		} else {
			$this->data['flexi_reward_status'] = $this->flexi_config['flexi_reward_status'];
		}
                
        if (isset($this->request->post['flexi_reward_order_status'])) {
			$this->data['flexi_reward_order_status_id'] = $this->request->post['flexi_reward_order_status'];
		} else {
			$this->data['flexi_reward_order_status_id'] = $this->flexi_config['flexi_reward_order_status'];
		}

		if (isset($this->request->post['flexi_redeem_min_order_amount'])) {
			$this->data['flexi_redeem_min_order_amount'] = $this->request->post['flexi_redeem_min_order_amount'];
		}elseif(!isset($this->flexi_config['flexi_redeem_min_order_amount'])){
			$this->data['flexi_redeem_min_order_amount'] = '0';
		} else {
			$this->data['flexi_redeem_min_order_amount'] = $this->flexi_config['flexi_redeem_min_order_amount'];
		}
		// flexi_redeem_max
		if (isset($this->request->post['flexi_redeem_max'])) {
			$this->data['flexi_redeem_max'] = $this->request->post['flexi_redeem_max'];
		}elseif(!isset($this->flexi_config['flexi_redeem_max'])){
			$this->data['flexi_redeem_max'] = '1';
		} else {
			$this->data['flexi_redeem_max'] = $this->flexi_config['flexi_redeem_max'];
		}
		// flexi_total_subtotal_operator
		if (isset($this->request->post['flexi_total_subtotal_operator'])) {
			$this->data['flexi_total_subtotal_operator'] = $this->request->post['flexi_total_subtotal_operator'];
		}elseif(!isset($this->flexi_config['flexi_total_subtotal_operator'])){
			$this->data['flexi_total_subtotal_operator'] = '0';
		} else {
			$this->data['flexi_total_subtotal_operator'] = $this->flexi_config['flexi_total_subtotal_operator'];
		}

		
		// flexi_ignore_product_points_value
		if (isset($this->request->post['flexi_ignore_product_points_value'])) {
			$this->data['flexi_ignore_product_points_value'] = $this->request->post['flexi_ignore_product_points_value'];
		}elseif(!isset($this->flexi_config['flexi_ignore_product_points_value'])){
			$this->data['flexi_ignore_product_points_value'] = '1';
		} else {
			$this->data['flexi_ignore_product_points_value'] = $this->flexi_config['flexi_ignore_product_points_value'];
		}
		
		// flexi_ignore_product_rewards_value
		if (isset($this->request->post['flexi_ignore_product_rewards_value'])) {
			$this->data['flexi_ignore_product_rewards_value'] = $this->request->post['flexi_ignore_product_rewards_value'];
		}elseif(!isset($this->flexi_config['flexi_ignore_product_rewards_value'])){
			$this->data['flexi_ignore_product_rewards_value'] = '1';
		} else {
			$this->data['flexi_ignore_product_rewards_value'] = $this->flexi_config['flexi_ignore_product_rewards_value'];
		}
		
		// flexi_registration_points
		if (isset($this->request->post['flexi_registration_points'])) {
			$this->data['flexi_registration_points'] = $this->request->post['flexi_registration_points'];
		}elseif(!isset($this->flexi_config['flexi_registration_points'])){
			$this->data['flexi_registration_points'] = '0';
		} else {
			$this->data['flexi_registration_points'] = $this->flexi_config['flexi_registration_points'];
		}
		// flexi_newsletter_sign_up_points
		if (isset($this->request->post['flexi_newsletter_sign_up_points'])) {
			$this->data['flexi_newsletter_sign_up_points'] = $this->request->post['flexi_newsletter_sign_up_points'];
		}elseif(!isset($this->flexi_config['flexi_newsletter_sign_up_points'])){
			$this->data['flexi_newsletter_sign_up_points'] = '0';
		} else {
			$this->data['flexi_newsletter_sign_up_points'] = $this->flexi_config['flexi_newsletter_sign_up_points'];
		}
		// flexi_productreview_points
		if (isset($this->request->post['flexi_productreview_points'])) {
			$this->data['flexi_productreview_points'] = $this->request->post['flexi_productreview_points'];
		}elseif(!isset($this->flexi_config['flexi_productreview_points'])){
			$this->data['flexi_productreview_points'] = '0';
		} else {
			$this->data['flexi_productreview_points'] = $this->flexi_config['flexi_productreview_points'];
		}
		// flexi_firstorder_points
		if (isset($this->request->post['flexi_firstorder_points'])) {
			$this->data['flexi_firstorder_points'] = $this->request->post['flexi_firstorder_points'];
		}elseif(!isset($this->flexi_config['flexi_firstorder_points'])){
			$this->data['flexi_firstorder_points'] = '0';
		} else {
			$this->data['flexi_firstorder_points'] = $this->flexi_config['flexi_firstorder_points'];
		}
		//flexi_firstorder_operator
		if (isset($this->request->post['flexi_firstorder_operator'])) {
			$this->data['flexi_firstorder_operator'] = $this->request->post['flexi_firstorder_operator'];
		}elseif(!isset($this->flexi_config['flexi_firstorder_operator'])){
			$this->data['flexi_firstorder_operator'] = '1';
		} else {
			$this->data['flexi_firstorder_operator'] = $this->flexi_config['flexi_firstorder_operator'];
		}		
		
		
		// Expiration
 /*       if (isset($this->request->post['flexi_exp_days_init'])) {
			$this->data['flexi_exp_days_init'] = $this->request->post['flexi_exp_days_init'];
		}elseif(!$this->flexi_config['flexi_exp_days_init')){
			$this->data['flexi_exp_days_init'] = '365';
		} else {
			$this->data['flexi_exp_days_init'] = $this->flexi_config['flexi_exp_days_init');
		}
*/
        if (isset($this->request->post['flexi_exp_days'])) {
			$this->data['flexi_exp_days'] = $this->request->post['flexi_exp_days'];
		}elseif(!$this->flexi_config['flexi_exp_days']){
			$this->data['flexi_exp_days'] = array(array('days'=>'10','rate'=>'0.8'));
		} else {
			$this->data['flexi_exp_days'] = $this->flexi_config['flexi_exp_days'];
			$this->data['flexi_exp_days'] = $this->flexi_config['flexi_exp_days'];			
			
		}
		
		// Manufacturer
	//	$flexi_manufacturers[0]['ids'] = array('29','15','154','711');
	//	$flexi_manufacturers[0]['multiplier'] = '0.4';
		$data = array();
		$this->load->model('catalog/manufacturer');
		$manufacturer_total = $this->model_catalog_manufacturer->getTotalManufacturers();

		$this->data['flexi_all_manufacturers'] = $this->model_catalog_manufacturer->getManufacturers($data);

		/*
				'manufacturer_id' => $result['manufacturer_id'],
				'name'            => $result['name'],
				'sort_order'      => $result['sort_order'],
		*/	
		
		if (isset($this->request->post['flexi_manufacturers'])) {
			$this->data['flexi_manufacturers'] = $this->request->post['flexi_manufacturers'];
		}elseif(!isset($this->flexi_config['flexi_manufacturers'])){
			$this->data['flexi_manufacturers'] = array();
		} else {
			$this->data['flexi_manufacturers'] = $this->flexi_config['flexi_manufacturers'];
		}


		// Category
	//	$flexi_categories[0]['ids'] = array('29','15','154','711');
	//	$flexi_categories[0]['multiplier'] = '0.4';
		
		$this->load->model('catalog/category');
		$category_total = $this->model_catalog_category->getTotalCategories();

		$this->data['flexi_all_categories'] = $this->model_catalog_category->getCategories($data); //$data
		
		
	/*			'category_id' => $result['category_id'],
				'name'        => $result['name'],
				'sort_order'  => $result['sort_order'],
			);
			*/
		
		if (isset($this->request->post['flexi_categories'])) {
			$this->data['flexi_categories'] = $this->request->post['flexi_categories'];
		}elseif(!isset($this->flexi_config['flexi_categories'])){
			$this->data['flexi_categories'] = array();
		} else {
			$this->data['flexi_categories'] = $this->flexi_config['flexi_categories'];
		}
		
		$this->template = 'module/flexi_reward.tpl';
		$this->children = array(
			'common/header',	
			'common/footer'
		);
		
		$this->response->setOutput($this->render());
	}
		
	protected function validate() {
//		var_dump(!$this->user->hasPermission('modify', 'module/flexi_reward'));
		if (!$this->user->hasPermission('modify', 'module/flexi_reward')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if(!array_key_exists( 'flexi_group', $this->request->get )) {
			$this->error['warning'] = 'Failed to identify target customer group! - flexi_group is missing';
		}
		
		$this->load->model('sale/customer_group');

		if(!$this->model_sale_customer_group->getCustomerGroup($this->request->get['flexi_group'])) {
			$this->error['warning'] = $this->language->get('error_permission').'  Bad customer group!';
		}

		
		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}	
	}
}
?>