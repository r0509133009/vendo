<?php
class ControllerModuleLivehelp extends Controller {
   private $error = array();
   private $file_path = 'module/livehelp';
   
   // Information about module
   public static function getInfo() {
      return array(
         'version' => '1.0.0',
         'author' => 'MalGanis',
         'icon' => '<i class="fa fa-comments fa-2x"></i>'
      );
   }
   
   public function index() {
      $this->language->load($this->file_path);
      
      $this->document->setTitle($this->language->get('heading_title'));
      
      $this->load->model('setting/setting');
      
      if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
         $this->model_setting_setting->editSetting('livehelp', $this->request->post);
         
         $this->session->data['success'] = $this->language->get('text_success');
         
         $this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
      }
      
      $this->data['heading_title'] = $this->language->get('heading_title');
      
      $this->data['text_yes']      = $this->language->get('text_yes');
      $this->data['text_no']       = $this->language->get('text_no');
      $this->data['text_enabled']  = $this->language->get('text_enabled');
      $this->data['text_disabled'] = $this->language->get('text_disabled');
      
      $this->data['text_info']                = $this->language->get('text_info');
      $this->data['text_module_info']         = $this->language->get('text_module_info');
      $this->data['title_livehelp_status']    = $this->language->get('title_livehelp_status');
      $this->data['text_content_bottom']      = $this->language->get('text_content_bottom');
      $this->data['text_livehelp_appearance'] = $this->language->get('text_livehelp_appearance');
      
      $this->data['entry_layout']        = $this->language->get('entry_layout');
      $this->data['entry_heading_title'] = $this->language->get('entry_heading_title');
      $this->data['entry_sort_order']    = $this->language->get('entry_sort_order');
      
      $this->data['entry_status'] = $this->language->get('entry_status');
      $this->data['entry_admin']  = $this->language->get('entry_admin');
      
      $this->data['button_save']       = $this->language->get('button_save');
      $this->data['button_cancel']     = $this->language->get('button_cancel');
      $this->data['button_add_module'] = $this->language->get('button_add_module');
      $this->data['button_remove']     = $this->language->get('button_remove');
      
      if (isset($this->error['warning'])) {
         $this->data['error_warning'] = $this->error['warning'];
      } else {
         $this->data['error_warning'] = '';
      }
      
      $this->data['breadcrumbs'] = array();
      
      $this->data['breadcrumbs'][] = array(
         'text' => $this->language->get('text_home'),
         'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
         'separator' => false
      );
      
      $this->data['breadcrumbs'][] = array(
         'text' => $this->language->get('text_module'),
         'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
         'separator' => ' :: '
      );
      
      $this->data['breadcrumbs'][] = array(
         'text' => $this->language->get('heading_title'),
         'href' => $this->url->link('module/livehelp', 'token=' . $this->session->data['token'], 'SSL'),
         'separator' => ' :: '
      );
            
      $this->data['action'] = $this->url->link($this->file_path, 'token=' . $this->session->data['token'], 'SSL');
      
      $this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
      
      // Livehelp status
      if (isset($this->request->post['livehelp_status'])) {
         $this->data['livehelp_status'] = $this->request->post['livehelp_status'];
      } else {
         $this->data['livehelp_status'] = $this->config->get('livehelp_status');
      }
      
      $this->data['modules'] = array();
      
      if (isset($this->request->post['livehelp_module'])) {
         $this->data['modules'] = $this->request->post['livehelp_module'];
      } elseif ($this->config->get('livehelp_module')) {
         $this->data['modules'] = $this->config->get('livehelp_module');
      }
      
      $this->load->model('design/layout');
      
      $this->data['layouts'] = $this->model_design_layout->getLayouts();
      
      // LANGUAGES
      $this->load->model('localisation/language');
      
      $this->data['languages'] = $this->model_localisation_language->getLanguages();
      
      $this->template = 'module/livehelp.tpl';
      $this->children = array(
         'common/header',
         'common/footer'
      );
      
      $this->response->setOutput($this->render());
   }
   
   public function install() {
      $this->language->load($this->file_path);
      $this->load->model('livehelp/livehelp');
      
      $status = $this->model_livehelp_livehelp->install();
      if ($status) {
         $this->session->data['success'] = $this->language->get('success_sql_install');
      } else {
         $this->session->data['error'] = $this->language->get('error_sql_install');
      }
   }
   
   public function uninstall() {
      $this->language->load($this->file_path);
      $this->load->model('livehelp/livehelp');
      $status = $this->model_livehelp_livehelp->uninstall();
      if ($status) {
         $this->session->data['success'] = $this->language->get('success_sql_uninstall');
      } else {
         $this->session->data['error'] = $this->language->get('error_sql_uninstall');
      }
   }
   
   protected function validate() {
      if (!$this->user->hasPermission('modify', $this->file_path)) {
         $this->error['warning'] = $this->language->get('error_permission');
      }
      
      return !$this->error;
   }
}
?>