<?php
class ControllerModuleAccountPlus extends Controller {
	private $error = array();
    private $_name = 'accountplus';
	
	public function index() {   
		$this->language->load('module/accountplus');

		$this->document->setTitle($this->language->get('heading_title'));
		 
		$this->load->model('setting/setting');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$mode = $this->request->post['action'];
			unset($this->request->post['action']);

			$this->model_setting_setting->editSetting('accountplus', $this->request->post);		
					
			$this->session->data['success'] = $this->language->get('text_success');

			if ($mode == "save-stay") {
				$this->redirect($this->url->link('module/accountplus', 'token=' . $this->session->data['token'], 'SSL'));
			} else { 
				$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
			}
		}


		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$this->data['server'] = HTTPS_CATALOG;
		} else {
			$this->data['server'] = HTTP_CATALOG;
		}


		$this->data['heading_title'] = $this->language->get('heading_title');
        
		$this->data['text_general_display'] = $this->language->get('text_general_display');
        $this->data['text_links_display'] = $this->language->get('text_links_display');
        $this->data['text_extra_display'] = $this->language->get('text_extra_display');
        $this->data['text_links_display_desc'] = $this->language->get('text_links_display_desc');
        $this->data['text_extra_display_desc'] = $this->language->get('text_extra_display_desc');
        $this->data['text_tab_account'] = $this->language->get('text_tab_account');
        $this->data['text_tab_affiliate'] = $this->language->get('text_tab_affiliate');
        $this->data['text_account_link'] = $this->language->get('text_account_link');
        $this->data['text_account_link1'] = $this->language->get('text_account_link1');
		$this->data['text_order_link'] = $this->language->get('text_order_link');
		$this->data['text_newsletter_link'] = $this->language->get('text_newsletter_link');
        $this->data['text_account_link_a'] = $this->language->get('text_account_link_a');
		$this->data['text_tracking_link_a'] = $this->language->get('text_tracking_link_a');
		$this->data['text_transaction_link_a'] = $this->language->get('text_transaction_link_a');
        $this->data['text_custom_title'] = $this->language->get('text_custom_title');
        $this->data['text_styles'] = $this->language->get('text_styles');
        $this->data['text_display'] = $this->language->get('text_display');
        $this->data['text_modules'] = $this->language->get('text_modules');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
    	$this->data['text_browse'] = $this->language->get('text_browse');
		$this->data['text_clear'] = $this->language->get('text_clear');
        $this->data['text_image_manager'] = $this->language->get('text_image_manager');

        $this->data['entry_links_display'] = $this->language->get('entry_links_display');
        $this->data['entry_logout_link'] = $this->language->get('entry_logout_link');
        $this->data['entry_edit_link'] = $this->language->get('entry_edit_link');
        $this->data['entry_password_link'] = $this->language->get('entry_password_link');
        $this->data['entry_address_link'] = $this->language->get('entry_address_link');
        $this->data['entry_wishlist_link'] = $this->language->get('entry_wishlist_link');
        $this->data['entry_order_link'] = $this->language->get('entry_order_link');
        $this->data['entry_download_link'] = $this->language->get('entry_download_link');
        $this->data['entry_return_link'] = $this->language->get('entry_return_link');
        $this->data['entry_transaction_link'] = $this->language->get('entry_transaction_link');
        $this->data['entry_recurring_link'] = $this->language->get('entry_recurring_link');
        $this->data['entry_reward_link'] = $this->language->get('entry_reward_link');
        $this->data['entry_newsletter_link'] = $this->language->get('entry_newsletter_link');
        $this->data['entry_edit_link_a'] = $this->language->get('entry_edit_link_a');
        $this->data['entry_password_link_a'] = $this->language->get('entry_password_link_a');
        $this->data['entry_payment_link_a'] = $this->language->get('entry_payment_link_a');
        $this->data['entry_tracking_link_a'] = $this->language->get('entry_tracking_link_a');
        $this->data['entry_transaction_link_a'] = $this->language->get('entry_transaction_link_a');
        $this->data['entry_logout_link_a'] = $this->language->get('entry_logout_link_a');
        $this->data['entry_tabs'] = $this->language->get('entry_tabs');
        $this->data['entry_secs'] = $this->language->get('entry_secs');
        $this->data['entry_secs_af'] = $this->language->get('entry_secs_af');  
        $this->data['entry_tabs_a'] = $this->language->get('entry_tabs_a');
        $this->data['entry_secs_af'] = $this->language->get('entry_secs_af');
		$this->data['entry_title'] = $this->language->get('entry_title');
        $this->data['entry_title_account'] = $this->language->get('entry_title_account');
        $this->data['entry_title_orders'] = $this->language->get('entry_title_orders');
        $this->data['entry_title_newsletter'] = $this->language->get('entry_title_newsletter');
		$this->data['entry_title_a'] = $this->language->get('entry_title_a');
        $this->data['entry_title_account_a'] = $this->language->get('entry_title_account_a');
        $this->data['entry_title_tracking_a'] = $this->language->get('entry_title_tracking_a');
        $this->data['entry_title_transaction_a'] = $this->language->get('entry_title_transaction_a');
		$this->data['entry_icon'] = $this->language->get('entry_icon');
        $this->data['entry_icons'] = $this->language->get('entry_icons');
        $this->data['entry_welcome_a'] = $this->language->get('entry_welcome_a');
        $this->data['entry_balance_a'] = $this->language->get('entry_balance_a');
        $this->data['entry_reward_a'] = $this->language->get('entry_reward_a');
        $this->data['entry_register_a'] = $this->language->get('entry_register_a');
        $this->data['entry_welcome_af'] = $this->language->get('entry_welcome_af');
        $this->data['entry_balance_af'] = $this->language->get('entry_balance_af');
        $this->data['entry_commi_af'] = $this->language->get('entry_commi_af');
        $this->data['entry_order_af'] = $this->language->get('entry_order_af');
        $this->data['entry_register_af'] = $this->language->get('entry_register_af');
 		$this->data['entry_yes'] = $this->language->get('entry_yes');
        $this->data['entry_no'] = $this->language->get('entry_no');
        $this->data['entry_style'] = $this->language->get('entry_style');
        $this->data['entry_icotype'] = $this->language->get('entry_icotype');
		$this->data['entry_title'] = $this->language->get('entry_title');
        $this->data['entry_description'] = $this->language->get('entry_description');
		$this->data['entry_link'] = $this->language->get('entry_link');
		$this->data['entry_image'] = $this->language->get('entry_image');		
		$this->data['entry_status'] = $this->language->get('entry_status');

        $this->data['entry_acc_icon_a'] = $this->language->get('entry_acc_icon_a');
        $this->data['entry_acc_icon_af'] = $this->language->get('entry_acc_icon_af');
        
        $this->data['tab_account'] = $this->language->get('tab_account');
        $this->data['tab_affiliate'] = $this->language->get('tab_affiliate');
        
		$this->data['button_save'] = $this->language->get('button_save');
        $this->data['button_save_stay'] = $this->language->get('button_save_stay');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

        $this->load->model('localisation/language');
		
		$languages = $this->model_localisation_language->getLanguages();

  		if (isset($this->error[$this->_name . '_extra_link_url1'])) {
			$this->data['error_' . $this->_name . '_extra_link_url1'] = $this->error[$this->_name . '_extra_link_url1'];
		} else {
			$this->data['error_' . $this->_name . '_extra_link_url1'] = '';
		}

  		if (isset($this->error[$this->_name . '_extra_link_url2'])) {
			$this->data['error_' . $this->_name . '_extra_link_url2'] = $this->error[$this->_name . '_extra_link_url2'];
		} else {
			$this->data['error_' . $this->_name . '_extra_link_url2'] = '';
		}

  		if (isset($this->error[$this->_name . '_extra_link_url3'])) {
			$this->data['error_' . $this->_name . '_extra_link_url3'] = $this->error[$this->_name . '_extra_link_url3'];
		} else {
			$this->data['error_' . $this->_name . '_extra_link_url3'] = '';
		}

  		if (isset($this->error[$this->_name . '_extra_link_url4'])) {
			$this->data['error_' . $this->_name . '_extra_link_url4'] = $this->error[$this->_name . '_extra_link_url4'];
		} else {
			$this->data['error_' . $this->_name . '_extra_link_url4'] = '';
		}

  		if (isset($this->error[$this->_name . '_extra_link_url5'])) {
			$this->data['error_' . $this->_name . '_extra_link_url5'] = $this->error[$this->_name . '_extra_link_url5'];
		} else {
			$this->data['error_' . $this->_name . '_extra_link_url5'] = '';
		}

  		if (isset($this->error[$this->_name . '_extra_link_url6'])) {
			$this->data['error_' . $this->_name . '_extra_link_url6'] = $this->error[$this->_name . '_extra_link_url6'];
		} else {
			$this->data['error_' . $this->_name . '_extra_link_url6'] = '';
		}

  		if (isset($this->error[$this->_name . '_extra_link_afurl1'])) {
			$this->data['error_' . $this->_name . '_extra_link_afurl1'] = $this->error[$this->_name . '_extra_link_afurl1'];
		} else {
			$this->data['error_' . $this->_name . '_extra_link_afurl1'] = '';
		}

  		if (isset($this->error[$this->_name . '_extra_link_afurl2'])) {
			$this->data['error_' . $this->_name . '_extra_link_afurl2'] = $this->error[$this->_name . '_extra_link_afurl2'];
		} else {
			$this->data['error_' . $this->_name . '_extra_link_afurl2'] = '';
		}

  		if (isset($this->error[$this->_name . '_extra_link_afurl3'])) {
			$this->data['error_' . $this->_name . '_extra_link_afurl3'] = $this->error[$this->_name . '_extra_link_afurl3'];
		} else {
			$this->data['error_' . $this->_name . '_extra_link_afurl3'] = '';
		}

  		if (isset($this->error[$this->_name . '_extra_link_afurl4'])) {
			$this->data['error_' . $this->_name . '_extra_link_afurl4'] = $this->error[$this->_name . '_extra_link_afurl4'];
		} else {
			$this->data['error_' . $this->_name . '_extra_link_afurl4'] = '';
		}

  		if (isset($this->error[$this->_name . '_extra_link_afurl5'])) {
			$this->data['error_' . $this->_name . '_extra_link_afurl5'] = $this->error[$this->_name . '_extra_link_afurl5'];
		} else {
			$this->data['error_' . $this->_name . '_extra_link_afurl5'] = '';
		}

  		if (isset($this->error[$this->_name . '_extra_link_afurl6'])) {
			$this->data['error_' . $this->_name . '_extra_link_afurl6'] = $this->error[$this->_name . '_extra_link_afurl6'];
		} else {
			$this->data['error_' . $this->_name . '_extra_link_afurl6'] = '';
		}
               
        foreach ($languages as $language) {
      		if (isset($this->error[$this->_name . '_extra_link_atitle1' . $language['language_id']])) {
    			$this->data['error_' . $this->_name . '_extra_link_atitle1' . $language['language_id']] = $this->error[$this->_name . '_extra_link_atitle1' . $language['language_id']];
    		} else {
    			$this->data['error_' . $this->_name . '_extra_link_atitle1' . $language['language_id']] = '';
    		}

      		if (isset($this->error[$this->_name . '_extra_link_atitle2' . $language['language_id']])) {
    			$this->data['error_' . $this->_name . '_extra_link_atitle2' . $language['language_id']] = $this->error[$this->_name . '_extra_link_atitle2' . $language['language_id']];
    		} else {
    			$this->data['error_' . $this->_name . '_extra_link_atitle2' . $language['language_id']] = '';
    		}

      		if (isset($this->error[$this->_name . '_extra_link_atitle3' . $language['language_id']])) {
    			$this->data['error_' . $this->_name . '_extra_link_atitle3' . $language['language_id']] = $this->error[$this->_name . '_extra_link_atitle3' . $language['language_id']];
    		} else {
    			$this->data['error_' . $this->_name . '_extra_link_atitle3' . $language['language_id']] = '';
    		}

      		if (isset($this->error[$this->_name . '_extra_link_atitle4' . $language['language_id']])) {
    			$this->data['error_' . $this->_name . '_extra_link_atitle4' . $language['language_id']] = $this->error[$this->_name . '_extra_link_atitle4' . $language['language_id']];
    		} else {
    			$this->data['error_' . $this->_name . '_extra_link_atitle4' . $language['language_id']] = '';
    		}

      		if (isset($this->error[$this->_name . '_extra_link_atitle5' . $language['language_id']])) {
    			$this->data['error_' . $this->_name . '_extra_link_atitle5' . $language['language_id']] = $this->error[$this->_name . '_extra_link_atitle5' . $language['language_id']];
    		} else {
    			$this->data['error_' . $this->_name . '_extra_link_atitle5' . $language['language_id']] = '';
    		}

      		if (isset($this->error[$this->_name . '_extra_link_atitle6' . $language['language_id']])) {
    			$this->data['error_' . $this->_name . '_extra_link_atitle6' . $language['language_id']] = $this->error[$this->_name . '_extra_link_atitle6' . $language['language_id']];
    		} else {
    			$this->data['error_' . $this->_name . '_extra_link_atitle6' . $language['language_id']] = '';
    		}

      		if (isset($this->error[$this->_name . '_extra_link_afatitle1' . $language['language_id']])) {
    			$this->data['error_' . $this->_name . '_extra_link_afatitle1' . $language['language_id']] = $this->error[$this->_name . '_extra_link_afatitle1' . $language['language_id']];
    		} else {
    			$this->data['error_' . $this->_name . '_extra_link_afatitle1' . $language['language_id']] = '';
    		}

      		if (isset($this->error[$this->_name . '_extra_link_afatitle2' . $language['language_id']])) {
    			$this->data['error_' . $this->_name . '_extra_link_afatitle2' . $language['language_id']] = $this->error[$this->_name . '_extra_link_afatitle2' . $language['language_id']];
    		} else {
    			$this->data['error_' . $this->_name . '_extra_link_afatitle2' . $language['language_id']] = '';
    		}

      		if (isset($this->error[$this->_name . '_extra_link_afatitle3' . $language['language_id']])) {
    			$this->data['error_' . $this->_name . '_extra_link_afatitle3' . $language['language_id']] = $this->error[$this->_name . '_extra_link_afatitle3' . $language['language_id']];
    		} else {
    			$this->data['error_' . $this->_name . '_extra_link_afatitle3' . $language['language_id']] = '';
    		}

      		if (isset($this->error[$this->_name . '_extra_link_afatitle4' . $language['language_id']])) {
    			$this->data['error_' . $this->_name . '_extra_link_afatitle4' . $language['language_id']] = $this->error[$this->_name . '_extra_link_afatitle4' . $language['language_id']];
    		} else {
    			$this->data['error_' . $this->_name . '_extra_link_afatitle4' . $language['language_id']] = '';
    		}

      		if (isset($this->error[$this->_name . '_extra_link_afatitle5' . $language['language_id']])) {
    			$this->data['error_' . $this->_name . '_extra_link_afatitle5' . $language['language_id']] = $this->error[$this->_name . '_extra_link_afatitle5' . $language['language_id']];
    		} else {
    			$this->data['error_' . $this->_name . '_extra_link_afatitle5' . $language['language_id']] = '';
    		}

      		if (isset($this->error[$this->_name . '_extra_link_afatitle6' . $language['language_id']])) {
    			$this->data['error_' . $this->_name . '_extra_link_afatitle6' . $language['language_id']] = $this->error[$this->_name . '_extra_link_afatitle6' . $language['language_id']];
    		} else {
    			$this->data['error_' . $this->_name . '_extra_link_afatitle6' . $language['language_id']] = '';
    		}
		}
        
		foreach ($languages as $language) {
		    if (isset($this->request->post[$this->_name . '_title' . $language['language_id']])) {
			$this->data[$this->_name . '_title' . $language['language_id']] = $this->request->post[$this->_name . '_title' . $language['language_id']];
            } else {
			$this->data[$this->_name . '_title' . $language['language_id']] = $this->config->get($this->_name . '_title' . $language['language_id']);
            }
            
            if (isset($this->request->post[$this->_name . '_title_account' . $language['language_id']])) {
			$this->data[$this->_name . '_title_account' . $language['language_id']] = $this->request->post[$this->_name . '_title_account' . $language['language_id']];
            } else {
			$this->data[$this->_name . '_title_account' . $language['language_id']] = $this->config->get($this->_name . '_title_account' . $language['language_id']);
            }
            
            if (isset($this->request->post[$this->_name . '_title_orders' . $language['language_id']])) {
			$this->data[$this->_name . '_title_orders' . $language['language_id']] = $this->request->post[$this->_name . '_title_orders' . $language['language_id']];
            } else {
			$this->data[$this->_name . '_title_orders' . $language['language_id']] = $this->config->get($this->_name . '_title_orders' . $language['language_id']);
            }
            
            if (isset($this->request->post[$this->_name . '_title_newsletter' . $language['language_id']])) {
			$this->data[$this->_name . '_title_newsletter' . $language['language_id']] = $this->request->post[$this->_name . '_title_newsletter' . $language['language_id']];
            } else {
			$this->data[$this->_name . '_title_newsletter' . $language['language_id']] = $this->config->get($this->_name . '_title_newsletter' . $language['language_id']);
            }
               
		        
		    if (isset($this->request->post[$this->_name . '_title_a' . $language['language_id']])) {
			$this->data[$this->_name . '_title_a' . $language['language_id']] = $this->request->post[$this->_name . '_title_a' . $language['language_id']];
            } else {
			$this->data[$this->_name . '_title_a' . $language['language_id']] = $this->config->get($this->_name . '_title_a' . $language['language_id']);
            }
            
            if (isset($this->request->post[$this->_name . '_title_account_a' . $language['language_id']])) {
			$this->data[$this->_name . '_title_account_a' . $language['language_id']] = $this->request->post[$this->_name . '_title_account_a' . $language['language_id']];
            } else {
			$this->data[$this->_name . '_title_account_a' . $language['language_id']] = $this->config->get($this->_name . '_title_account_a' . $language['language_id']);
            }
            
            if (isset($this->request->post[$this->_name . '_title_tracking_a' . $language['language_id']])) {
			$this->data[$this->_name . '_title_tracking_a' . $language['language_id']] = $this->request->post[$this->_name . '_title_tracking_a' . $language['language_id']];
            } else {
			$this->data[$this->_name . '_title_tracking_a' . $language['language_id']] = $this->config->get($this->_name . '_title_tracking_a' . $language['language_id']);
            }
            
            if (isset($this->request->post[$this->_name . '_title_transaction_a' . $language['language_id']])) {
			$this->data[$this->_name . '_title_transaction_a' . $language['language_id']] = $this->request->post[$this->_name . '_title_transaction_a' . $language['language_id']];
            } else {
			$this->data[$this->_name . '_title_transaction_a' . $language['language_id']] = $this->config->get($this->_name . '_title_transaction_a' . $language['language_id']);
            }
        
            if (isset($this->request->post[$this->_name . '_extra_link_atitle1' . $language['language_id']])) {
			$this->data[$this->_name . '_extra_link_atitle1' . $language['language_id']] = $this->request->post[$this->_name . '_extra_link_atitle1' . $language['language_id']];
            } else {
			$this->data[$this->_name . '_extra_link_atitle1' . $language['language_id']] = $this->config->get($this->_name . '_extra_link_atitle1' . $language['language_id']);
            }

            if (isset($this->request->post[$this->_name . '_extra_link_atitle2' . $language['language_id']])) {
			$this->data[$this->_name . '_extra_link_atitle2' . $language['language_id']] = $this->request->post[$this->_name . '_extra_link_atitle2' . $language['language_id']];
            } else {
			$this->data[$this->_name . '_extra_link_atitle2' . $language['language_id']] = $this->config->get($this->_name . '_extra_link_atitle2' . $language['language_id']);
            }

            if (isset($this->request->post[$this->_name . '_extra_link_atitle3' . $language['language_id']])) {
			$this->data[$this->_name . '_extra_link_atitle3' . $language['language_id']] = $this->request->post[$this->_name . '_extra_link_atitle3' . $language['language_id']];
            } else {
			$this->data[$this->_name . '_extra_link_atitle3' . $language['language_id']] = $this->config->get($this->_name . '_extra_link_atitle3' . $language['language_id']);
            }

            if (isset($this->request->post[$this->_name . '_extra_link_atitle4' . $language['language_id']])) {
			$this->data[$this->_name . '_extra_link_atitle4' . $language['language_id']] = $this->request->post[$this->_name . '_extra_link_atitle4' . $language['language_id']];
            } else {
			$this->data[$this->_name . '_extra_link_atitle4' . $language['language_id']] = $this->config->get($this->_name . '_extra_link_atitle4' . $language['language_id']);
            }

            if (isset($this->request->post[$this->_name . '_extra_link_atitle5' . $language['language_id']])) {
			$this->data[$this->_name . '_extra_link_atitle5' . $language['language_id']] = $this->request->post[$this->_name . '_extra_link_atitle5' . $language['language_id']];
            } else {
			$this->data[$this->_name . '_extra_link_atitle5' . $language['language_id']] = $this->config->get($this->_name . '_extra_link_atitle5' . $language['language_id']);
            }

            if (isset($this->request->post[$this->_name . '_extra_link_atitle6' . $language['language_id']])) {
			$this->data[$this->_name . '_extra_link_atitle6' . $language['language_id']] = $this->request->post[$this->_name . '_extra_link_atitle6' . $language['language_id']];
            } else {
			$this->data[$this->_name . '_extra_link_atitle6' . $language['language_id']] = $this->config->get($this->_name . '_extra_link_atitle6' . $language['language_id']);
            }

            if (isset($this->request->post[$this->_name . '_extra_link_afatitle1' . $language['language_id']])) {
			$this->data[$this->_name . '_extra_link_afatitle1' . $language['language_id']] = $this->request->post[$this->_name . '_extra_link_afatitle1' . $language['language_id']];
            } else {
			$this->data[$this->_name . '_extra_link_afatitle1' . $language['language_id']] = $this->config->get($this->_name . '_extra_link_afatitle1' . $language['language_id']);
            }

            if (isset($this->request->post[$this->_name . '_extra_link_afatitle2' . $language['language_id']])) {
			$this->data[$this->_name . '_extra_link_afatitle2' . $language['language_id']] = $this->request->post[$this->_name . '_extra_link_afatitle2' . $language['language_id']];
            } else {
			$this->data[$this->_name . '_extra_link_afatitle2' . $language['language_id']] = $this->config->get($this->_name . '_extra_link_afatitle2' . $language['language_id']);
            }

            if (isset($this->request->post[$this->_name . '_extra_link_afatitle3' . $language['language_id']])) {
			$this->data[$this->_name . '_extra_link_afatitle3' . $language['language_id']] = $this->request->post[$this->_name . '_extra_link_afatitle3' . $language['language_id']];
            } else {
			$this->data[$this->_name . '_extra_link_afatitle3' . $language['language_id']] = $this->config->get($this->_name . '_extra_link_afatitle3' . $language['language_id']);
            }

            if (isset($this->request->post[$this->_name . '_extra_link_afatitle4' . $language['language_id']])) {
			$this->data[$this->_name . '_extra_link_afatitle4' . $language['language_id']] = $this->request->post[$this->_name . '_extra_link_afatitle4' . $language['language_id']];
            } else {
			$this->data[$this->_name . '_extra_link_afatitle4' . $language['language_id']] = $this->config->get($this->_name . '_extra_link_afatitle4' . $language['language_id']);
            }

            if (isset($this->request->post[$this->_name . '_extra_link_afatitle5' . $language['language_id']])) {
			$this->data[$this->_name . '_extra_link_afatitle5' . $language['language_id']] = $this->request->post[$this->_name . '_extra_link_afatitle5' . $language['language_id']];
            } else {
			$this->data[$this->_name . '_extra_link_afatitle5' . $language['language_id']] = $this->config->get($this->_name . '_extra_link_afatitle5' . $language['language_id']);
            }

            if (isset($this->request->post[$this->_name . '_extra_link_afatitle6' . $language['language_id']])) {
			$this->data[$this->_name . '_extra_link_afatitle6' . $language['language_id']] = $this->request->post[$this->_name . '_extra_link_afatitle6' . $language['language_id']];
            } else {
			$this->data[$this->_name . '_extra_link_afatitle6' . $language['language_id']] = $this->config->get($this->_name . '_extra_link_afatitle6' . $language['language_id']);
            }

            if (isset($this->request->post[$this->_name . '_extra_link_adesc1' . $language['language_id']])) {
			$this->data[$this->_name . '_extra_link_adesc1' . $language['language_id']] = $this->request->post[$this->_name . '_extra_link_adesc1' . $language['language_id']];
            } else {
			$this->data[$this->_name . '_extra_link_adesc1' . $language['language_id']] = $this->config->get($this->_name . '_extra_link_adesc1' . $language['language_id']);
            }

            if (isset($this->request->post[$this->_name . '_extra_link_adesc2' . $language['language_id']])) {
			$this->data[$this->_name . '_extra_link_adesc2' . $language['language_id']] = $this->request->post[$this->_name . '_extra_link_adesc2' . $language['language_id']];
            } else {
			$this->data[$this->_name . '_extra_link_adesc2' . $language['language_id']] = $this->config->get($this->_name . '_extra_link_adesc2' . $language['language_id']);
            }

            if (isset($this->request->post[$this->_name . '_extra_link_adesc3' . $language['language_id']])) {
			$this->data[$this->_name . '_extra_link_adesc3' . $language['language_id']] = $this->request->post[$this->_name . '_extra_link_adesc3' . $language['language_id']];
            } else {
			$this->data[$this->_name . '_extra_link_adesc3' . $language['language_id']] = $this->config->get($this->_name . '_extra_link_adesc3' . $language['language_id']);
            }

            if (isset($this->request->post[$this->_name . '_extra_link_adesc4' . $language['language_id']])) {
			$this->data[$this->_name . '_extra_link_adesc4' . $language['language_id']] = $this->request->post[$this->_name . '_extra_link_adesc4' . $language['language_id']];
            } else {
			$this->data[$this->_name . '_extra_link_adesc4' . $language['language_id']] = $this->config->get($this->_name . '_extra_link_adesc4' . $language['language_id']);
            }

            if (isset($this->request->post[$this->_name . '_extra_link_adesc5' . $language['language_id']])) {
			$this->data[$this->_name . '_extra_link_adesc5' . $language['language_id']] = $this->request->post[$this->_name . '_extra_link_adesc5' . $language['language_id']];
            } else {
			$this->data[$this->_name . '_extra_link_adesc5' . $language['language_id']] = $this->config->get($this->_name . '_extra_link_adesc5' . $language['language_id']);
            }

            if (isset($this->request->post[$this->_name . '_extra_link_adesc6' . $language['language_id']])) {
			$this->data[$this->_name . '_extra_link_adesc6' . $language['language_id']] = $this->request->post[$this->_name . '_extra_link_adesc6' . $language['language_id']];
            } else {
			$this->data[$this->_name . '_extra_link_adesc6' . $language['language_id']] = $this->config->get($this->_name . '_extra_link_adesc6' . $language['language_id']);
            }

            if (isset($this->request->post[$this->_name . '_extra_link_afadesc1' . $language['language_id']])) {
			$this->data[$this->_name . '_extra_link_afadesc1' . $language['language_id']] = $this->request->post[$this->_name . '_extra_link_afadesc1' . $language['language_id']];
            } else {
			$this->data[$this->_name . '_extra_link_afadesc1' . $language['language_id']] = $this->config->get($this->_name . '_extra_link_afadesc1' . $language['language_id']);
            }

            if (isset($this->request->post[$this->_name . '_extra_link_afadesc2' . $language['language_id']])) {
			$this->data[$this->_name . '_extra_link_afadesc2' . $language['language_id']] = $this->request->post[$this->_name . '_extra_link_afadesc2' . $language['language_id']];
            } else {
			$this->data[$this->_name . '_extra_link_afadesc2' . $language['language_id']] = $this->config->get($this->_name . '_extra_link_afadesc2' . $language['language_id']);
            }

            if (isset($this->request->post[$this->_name . '_extra_link_afadesc3' . $language['language_id']])) {
			$this->data[$this->_name . '_extra_link_afadesc3' . $language['language_id']] = $this->request->post[$this->_name . '_extra_link_afadesc3' . $language['language_id']];
            } else {
			$this->data[$this->_name . '_extra_link_afadesc3' . $language['language_id']] = $this->config->get($this->_name . '_extra_link_afadesc3' . $language['language_id']);
            }

            if (isset($this->request->post[$this->_name . '_extra_link_afadesc4' . $language['language_id']])) {
			$this->data[$this->_name . '_extra_link_afadesc4' . $language['language_id']] = $this->request->post[$this->_name . '_extra_link_afadesc4' . $language['language_id']];
            } else {
			$this->data[$this->_name . '_extra_link_afadesc4' . $language['language_id']] = $this->config->get($this->_name . '_extra_link_afadesc4' . $language['language_id']);
            }

            if (isset($this->request->post[$this->_name . '_extra_link_afadesc5' . $language['language_id']])) {
			$this->data[$this->_name . '_extra_link_afadesc5' . $language['language_id']] = $this->request->post[$this->_name . '_extra_link_afadesc5' . $language['language_id']];
            } else {
			$this->data[$this->_name . '_extra_link_afadesc5' . $language['language_id']] = $this->config->get($this->_name . '_extra_link_afadesc5' . $language['language_id']);
            }

            if (isset($this->request->post[$this->_name . '_extra_link_afadesc6' . $language['language_id']])) {
			$this->data[$this->_name . '_extra_link_afadesc6' . $language['language_id']] = $this->request->post[$this->_name . '_extra_link_afadesc6' . $language['language_id']];
            } else {
			$this->data[$this->_name . '_extra_link_afadesc6' . $language['language_id']] = $this->config->get($this->_name . '_extra_link_afadesc6' . $language['language_id']);
            }

        }
		
		$this->data['languages'] = $languages;
	
        if (isset($this->request->post[$this->_name . '_tabs'])) { 
			$this->data[$this->_name . '_tabs'] = $this->request->post[$this->_name . '_tabs']; 
		} else { 
			$this->data[$this->_name . '_tabs'] = $this->config->get($this->_name . '_tabs' ); 
		} 

        if (isset($this->request->post[$this->_name . '_secs'])) { 
			$this->data[$this->_name . '_secs'] = $this->request->post[$this->_name . '_secs']; 
		} else { 
			$this->data[$this->_name . '_secs'] = $this->config->get($this->_name . '_secs' ); 
		} 

        if (isset($this->request->post[$this->_name . '_secs_af'])) { 
			$this->data[$this->_name . '_secs_af'] = $this->request->post[$this->_name . '_secs_af']; 
		} else { 
			$this->data[$this->_name . '_secs_af'] = $this->config->get($this->_name . '_secs_af' ); 
		} 

        if (isset($this->request->post[$this->_name . '_tabs_a'])) { 
			$this->data[$this->_name . '_tabs_a'] = $this->request->post[$this->_name . '_tabs_a']; 
		} else { 
			$this->data[$this->_name . '_tabs_a'] = $this->config->get($this->_name . '_tabs_a' ); 
		} 

        if (isset($this->request->post[$this->_name . '_secs_a'])) { 
			$this->data[$this->_name . '_secs_a'] = $this->request->post[$this->_name . '_secs_a']; 
		} else { 
			$this->data[$this->_name . '_secs_a'] = $this->config->get($this->_name . '_secs_a' ); 
		} 

        if (isset($this->request->post[$this->_name . '_welcome_a'])) { 
			$this->data[$this->_name . '_welcome_a'] = $this->request->post[$this->_name . '_welcome_a']; 
		} else { 
			$this->data[$this->_name . '_welcome_a'] = $this->config->get($this->_name . '_welcome_a' ); 
		} 

        if (isset($this->request->post[$this->_name . '_acc_icon_a'])) { 
			$this->data[$this->_name . '_acc_icon_a'] = $this->request->post[$this->_name . '_acc_icon_a']; 
		} else { 
			$this->data[$this->_name . '_acc_icon_a'] = $this->config->get($this->_name . '_acc_icon_a' ); 
		} 

        if (isset($this->request->post[$this->_name . '_acc_icon_af'])) { 
			$this->data[$this->_name . '_acc_icon_af'] = $this->request->post[$this->_name . '_acc_icon_af']; 
		} else { 
			$this->data[$this->_name . '_acc_icon_af'] = $this->config->get($this->_name . '_acc_icon_af' ); 
		} 

        if (isset($this->request->post[$this->_name . '_balance_a'])) { 
			$this->data[$this->_name . '_balance_a'] = $this->request->post[$this->_name . '_balance_a']; 
		} else { 
			$this->data[$this->_name . '_balance_a'] = $this->config->get($this->_name . '_balance_a' ); 
		} 

        if (isset($this->request->post[$this->_name . '_reward_a'])) { 
			$this->data[$this->_name . '_reward_a'] = $this->request->post[$this->_name . '_reward_a']; 
		} else { 
			$this->data[$this->_name . '_reward_a'] = $this->config->get($this->_name . '_reward_a' ); 
		} 

        if (isset($this->request->post[$this->_name . '_register_a'])) { 
			$this->data[$this->_name . '_register_a'] = $this->request->post[$this->_name . '_register_a']; 
		} else { 
			$this->data[$this->_name . '_register_a'] = $this->config->get($this->_name . '_register_a' ); 
		} 

        if (isset($this->request->post[$this->_name . '_welcome_af'])) { 
			$this->data[$this->_name . '_welcome_af'] = $this->request->post[$this->_name . '_welcome_af']; 
		} else { 
			$this->data[$this->_name . '_welcome_af'] = $this->config->get($this->_name . '_welcome_af' ); 
		} 

        if (isset($this->request->post[$this->_name . '_balance_af'])) { 
			$this->data[$this->_name . '_balance_af'] = $this->request->post[$this->_name . '_balance_af']; 
		} else { 
			$this->data[$this->_name . '_balance_af'] = $this->config->get($this->_name . '_balance_af' ); 
		} 

        if (isset($this->request->post[$this->_name . '_commi_af'])) { 
			$this->data[$this->_name . '_commi_af'] = $this->request->post[$this->_name . '_commi_af']; 
		} else { 
			$this->data[$this->_name . '_commi_af'] = $this->config->get($this->_name . '_commi_af' ); 
		} 

        if (isset($this->request->post[$this->_name . '_order_af'])) { 
			$this->data[$this->_name . '_order_af'] = $this->request->post[$this->_name . '_order_af']; 
		} else { 
			$this->data[$this->_name . '_order_af'] = $this->config->get($this->_name . '_order_af' ); 
		} 

        if (isset($this->request->post[$this->_name . '_register_af'])) { 
			$this->data[$this->_name . '_register_af'] = $this->request->post[$this->_name . '_register_af']; 
		} else { 
			$this->data[$this->_name . '_register_af'] = $this->config->get($this->_name . '_register_af' ); 
		} 

        if (isset($this->request->post[$this->_name . '_logout_link'])) { 
			$this->data[$this->_name . '_logout_link'] = $this->request->post[$this->_name . '_logout_link']; 
		} else { 
			$this->data[$this->_name . '_logout_link'] = $this->config->get($this->_name . '_logout_link' ); 
		} 
        
        if (isset($this->request->post[$this->_name . '_logout_link_a'])) { 
			$this->data[$this->_name . '_logout_link_a'] = $this->request->post[$this->_name . '_logout_link_a']; 
		} else { 
			$this->data[$this->_name . '_logout_link_a'] = $this->config->get($this->_name . '_logout_link_a' ); 
		} 
        
        if (isset($this->request->post[$this->_name . '_password_link'])) { 
			$this->data[$this->_name . '_password_link'] = $this->request->post[$this->_name . '_password_link']; 
		} else { 
			$this->data[$this->_name . '_password_link'] = $this->config->get($this->_name . '_password_link' ); 
		}  
        
        if (isset($this->request->post[$this->_name . '_password_link_a'])) { 
			$this->data[$this->_name . '_password_link_a'] = $this->request->post[$this->_name . '_password_link_a']; 
		} else { 
			$this->data[$this->_name . '_password_link_a'] = $this->config->get($this->_name . '_password_link_a' ); 
		}  
        
        if (isset($this->request->post[$this->_name . '_edit_link'])) { 
			$this->data[$this->_name . '_edit_link'] = $this->request->post[$this->_name . '_edit_link']; 
		} else { 
			$this->data[$this->_name . '_edit_link'] = $this->config->get($this->_name . '_edit_link' ); 
		} 

        if (isset($this->request->post[$this->_name . '_edit_link_a'])) { 
			$this->data[$this->_name . '_edit_link_a'] = $this->request->post[$this->_name . '_edit_link_a']; 
		} else { 
			$this->data[$this->_name . '_edit_link_a'] = $this->config->get($this->_name . '_edit_link_a' ); 
		} 

        if (isset($this->request->post[$this->_name . '_address_link'])) { 
			$this->data[$this->_name . '_address_link'] = $this->request->post[$this->_name . '_address_link']; 
		} else { 
			$this->data[$this->_name . '_address_link'] = $this->config->get($this->_name . '_address_link' ); 
		} 
        
        if (isset($this->request->post[$this->_name . '_payment_link_a'])) { 
			$this->data[$this->_name . '_payment_link_a'] = $this->request->post[$this->_name . '_payment_link_a']; 
		} else { 
			$this->data[$this->_name . '_payment_link_a'] = $this->config->get($this->_name . '_payment_link_a' ); 
		} 

        if (isset($this->request->post[$this->_name . '_wishlist_link'])) { 
			$this->data[$this->_name . '_wishlist_link'] = $this->request->post[$this->_name . '_wishlist_link']; 
		} else { 
			$this->data[$this->_name . '_wishlist_link'] = $this->config->get($this->_name . '_wishlist_link' ); 
		} 
        
        if (isset($this->request->post[$this->_name . '_tracking_link_a'])) { 
			$this->data[$this->_name . '_tracking_link_a'] = $this->request->post[$this->_name . '_tracking_link_a']; 
		} else { 
			$this->data[$this->_name . '_tracking_link_a'] = $this->config->get($this->_name . '_tracking_link_a' ); 
		} 

        if (isset($this->request->post[$this->_name . '_transaction_link_a'])) { 
			$this->data[$this->_name . '_transaction_link_a'] = $this->request->post[$this->_name . '_transaction_link_a']; 
		} else { 
			$this->data[$this->_name . '_transaction_link_a'] = $this->config->get($this->_name . '_transaction_link_a' ); 
		} 
        
        if (isset($this->request->post[$this->_name . '_order_link'])) { 
			$this->data[$this->_name . '_order_link'] = $this->request->post[$this->_name . '_order_link']; 
		} else { 
			$this->data[$this->_name . '_order_link'] = $this->config->get($this->_name . '_order_link' ); 
		} 
        
        
        if (isset($this->request->post[$this->_name . '_download_link'])) { 
			$this->data[$this->_name . '_download_link'] = $this->request->post[$this->_name . '_download_link']; 
		} else { 
			$this->data[$this->_name . '_download_link'] = $this->config->get($this->_name . '_download_link' ); 
		} 
        
        
        if (isset($this->request->post[$this->_name . '_return_link'])) { 
			$this->data[$this->_name . '_return_link'] = $this->request->post[$this->_name . '_return_link']; 
		} else { 
			$this->data[$this->_name . '_return_link'] = $this->config->get($this->_name . '_return_link' ); 
		} 
        
        
        if (isset($this->request->post[$this->_name . '_transaction_link'])) { 
			$this->data[$this->_name . '_transaction_link'] = $this->request->post[$this->_name . '_transaction_link']; 
		} else { 
			$this->data[$this->_name . '_transaction_link'] = $this->config->get($this->_name . '_transaction_link' ); 
		} 

        if (isset($this->request->post[$this->_name . '_recurring_link'])) { 
			$this->data[$this->_name . '_recurring_link'] = $this->request->post[$this->_name . '_recurring_link']; 
		} else { 
			$this->data[$this->_name . '_recurring_link'] = $this->config->get($this->_name . '_recurring_link' ); 
		} 
        
        if (isset($this->request->post[$this->_name . '_newsletter_link'])) { 
			$this->data[$this->_name . '_newsletter_link'] = $this->request->post[$this->_name . '_newsletter_link']; 
		} else { 
			$this->data[$this->_name . '_newsletter_link'] = $this->config->get($this->_name . '_newsletter_link' ); 
		} 
        
        
        if (isset($this->request->post[$this->_name . '_reward_link'])) { 
			$this->data[$this->_name . '_reward_link'] = $this->request->post[$this->_name . '_reward_link']; 
		} else { 
			$this->data[$this->_name . '_reward_link'] = $this->config->get($this->_name . '_reward_link' ); 
		} 

        if (isset($this->request->post[$this->_name . '_extra_link_url1'])) { 
			$this->data[$this->_name . '_extra_link_url1'] = $this->request->post[$this->_name . '_extra_link_url1']; 
		} else { 
			$this->data[$this->_name . '_extra_link_url1'] = $this->config->get($this->_name . '_extra_link_url1' ); 
		} 

        if (isset($this->request->post[$this->_name . '_extra_link_status1'])) { 
			$this->data[$this->_name . '_extra_link_status1'] = $this->request->post[$this->_name . '_extra_link_status1']; 
		} else { 
			$this->data[$this->_name . '_extra_link_status1'] = $this->config->get($this->_name . '_extra_link_status1' ); 
		} 

        if (isset($this->request->post[$this->_name . '_extra_link_url2'])) { 
			$this->data[$this->_name . '_extra_link_url2'] = $this->request->post[$this->_name . '_extra_link_url2']; 
		} else { 
			$this->data[$this->_name . '_extra_link_url2'] = $this->config->get($this->_name . '_extra_link_url2' ); 
		} 

        if (isset($this->request->post[$this->_name . '_extra_link_status2'])) { 
			$this->data[$this->_name . '_extra_link_status2'] = $this->request->post[$this->_name . '_extra_link_status2']; 
		} else { 
			$this->data[$this->_name . '_extra_link_status2'] = $this->config->get($this->_name . '_extra_link_status2' ); 
		} 

        if (isset($this->request->post[$this->_name . '_extra_link_url3'])) { 
			$this->data[$this->_name . '_extra_link_url3'] = $this->request->post[$this->_name . '_extra_link_url3']; 
		} else { 
			$this->data[$this->_name . '_extra_link_url3'] = $this->config->get($this->_name . '_extra_link_url3' ); 
		} 

        if (isset($this->request->post[$this->_name . '_extra_link_status3'])) { 
			$this->data[$this->_name . '_extra_link_status3'] = $this->request->post[$this->_name . '_extra_link_status3']; 
		} else { 
			$this->data[$this->_name . '_extra_link_status3'] = $this->config->get($this->_name . '_extra_link_status3' ); 
		} 

        if (isset($this->request->post[$this->_name . '_extra_link_url4'])) { 
			$this->data[$this->_name . '_extra_link_url4'] = $this->request->post[$this->_name . '_extra_link_url4']; 
		} else { 
			$this->data[$this->_name . '_extra_link_url4'] = $this->config->get($this->_name . '_extra_link_url4' ); 
		} 

        if (isset($this->request->post[$this->_name . '_extra_link_status4'])) { 
			$this->data[$this->_name . '_extra_link_status4'] = $this->request->post[$this->_name . '_extra_link_status4']; 
		} else { 
			$this->data[$this->_name . '_extra_link_status4'] = $this->config->get($this->_name . '_extra_link_status4' ); 
		} 

        if (isset($this->request->post[$this->_name . '_extra_link_url5'])) { 
			$this->data[$this->_name . '_extra_link_url5'] = $this->request->post[$this->_name . '_extra_link_url5']; 
		} else { 
			$this->data[$this->_name . '_extra_link_url5'] = $this->config->get($this->_name . '_extra_link_url5' ); 
		} 

        if (isset($this->request->post[$this->_name . '_extra_link_status5'])) { 
			$this->data[$this->_name . '_extra_link_status5'] = $this->request->post[$this->_name . '_extra_link_status5']; 
		} else { 
			$this->data[$this->_name . '_extra_link_status5'] = $this->config->get($this->_name . '_extra_link_status5' ); 
		} 

        if (isset($this->request->post[$this->_name . '_extra_link_url6'])) { 
			$this->data[$this->_name . '_extra_link_url6'] = $this->request->post[$this->_name . '_extra_link_url6']; 
		} else { 
			$this->data[$this->_name . '_extra_link_url6'] = $this->config->get($this->_name . '_extra_link_url6' ); 
		} 

        if (isset($this->request->post[$this->_name . '_extra_link_status6'])) { 
			$this->data[$this->_name . '_extra_link_status6'] = $this->request->post[$this->_name . '_extra_link_status6']; 
		} else { 
			$this->data[$this->_name . '_extra_link_status6'] = $this->config->get($this->_name . '_extra_link_status6' ); 
		} 
        
        $this->load->model('tool/image');
        $this->data['token'] = $this->session->data['token'];
        
        if (isset($this->request->post[$this->_name . '_extra_link_image1'])) {
			$this->data[$this->_name . '_extra_link_image1'] = $this->request->post[$this->_name . '_extra_link_image1'];
		} else {
			$this->data[$this->_name . '_extra_link_image1'] = $this->config->get($this->_name . '_extra_link_image1');			
		}
		
        if ($this->config->get($this->_name . '_extra_link_image1') && file_exists(DIR_IMAGE . $this->config->get($this->_name . '_extra_link_image1')) && is_file(DIR_IMAGE . $this->config->get($this->_name . '_extra_link_image1'))) {
			$this->data[$this->_name . '_extra_link_pic1'] = $this->model_tool_image->resize($this->config->get($this->_name . '_extra_link_image1'), 100, 100);		
		} else {
			$this->data[$this->_name . '_extra_link_pic1'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
		}

        if (isset($this->request->post[$this->_name . '_extra_link_image2'])) {
			$this->data[$this->_name . '_extra_link_image2'] = $this->request->post[$this->_name . '_extra_link_image2'];
		} else {
			$this->data[$this->_name . '_extra_link_image2'] = $this->config->get($this->_name . '_extra_link_image2');			
		}
		
        if ($this->config->get($this->_name . '_extra_link_image2') && file_exists(DIR_IMAGE . $this->config->get($this->_name . '_extra_link_image2')) && is_file(DIR_IMAGE . $this->config->get($this->_name . '_extra_link_image2'))) {
			$this->data[$this->_name . '_extra_link_pic2'] = $this->model_tool_image->resize($this->config->get($this->_name . '_extra_link_image2'), 100, 100);		
		} else {
			$this->data[$this->_name . '_extra_link_pic2'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
		}

        if (isset($this->request->post[$this->_name . '_extra_link_image3'])) {
			$this->data[$this->_name . '_extra_link_image3'] = $this->request->post[$this->_name . '_extra_link_image3'];
		} else {
			$this->data[$this->_name . '_extra_link_image3'] = $this->config->get($this->_name . '_extra_link_image3');			
		}
		
        if ($this->config->get($this->_name . '_extra_link_image3') && file_exists(DIR_IMAGE . $this->config->get($this->_name . '_extra_link_image3')) && is_file(DIR_IMAGE . $this->config->get($this->_name . '_extra_link_image3'))) {
			$this->data[$this->_name . '_extra_link_pic3'] = $this->model_tool_image->resize($this->config->get($this->_name . '_extra_link_image3'), 100, 100);		
		} else {
			$this->data[$this->_name . '_extra_link_pic3'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
		}

        if (isset($this->request->post[$this->_name . '_extra_link_image4'])) {
			$this->data[$this->_name . '_extra_link_image4'] = $this->request->post[$this->_name . '_extra_link_image4'];
		} else {
			$this->data[$this->_name . '_extra_link_image4'] = $this->config->get($this->_name . '_extra_link_image4');			
		}
		
        if ($this->config->get($this->_name . '_extra_link_image4') && file_exists(DIR_IMAGE . $this->config->get($this->_name . '_extra_link_image4')) && is_file(DIR_IMAGE . $this->config->get($this->_name . '_extra_link_image4'))) {
			$this->data[$this->_name . '_extra_link_pic4'] = $this->model_tool_image->resize($this->config->get($this->_name . '_extra_link_image4'), 100, 100);		
		} else {
			$this->data[$this->_name . '_extra_link_pic4'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
		}

        if (isset($this->request->post[$this->_name . '_extra_link_image5'])) {
			$this->data[$this->_name . '_extra_link_image5'] = $this->request->post[$this->_name . '_extra_link_image5'];
		} else {
			$this->data[$this->_name . '_extra_link_image5'] = $this->config->get($this->_name . '_extra_link_image5');			
		}
		
        if ($this->config->get($this->_name . '_extra_link_image5') && file_exists(DIR_IMAGE . $this->config->get($this->_name . '_extra_link_image5')) && is_file(DIR_IMAGE . $this->config->get($this->_name . '_extra_link_image5'))) {
			$this->data[$this->_name . '_extra_link_pic5'] = $this->model_tool_image->resize($this->config->get($this->_name . '_extra_link_image5'), 100, 100);		
		} else {
			$this->data[$this->_name . '_extra_link_pic5'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
		}

        if (isset($this->request->post[$this->_name . '_extra_link_image6'])) {
			$this->data[$this->_name . '_extra_link_image6'] = $this->request->post[$this->_name . '_extra_link_image6'];
		} else {
			$this->data[$this->_name . '_extra_link_image6'] = $this->config->get($this->_name . '_extra_link_image6');			
		}
		
        if ($this->config->get($this->_name . '_extra_link_image6') && file_exists(DIR_IMAGE . $this->config->get($this->_name . '_extra_link_image6')) && is_file(DIR_IMAGE . $this->config->get($this->_name . '_extra_link_image6'))) {
			$this->data[$this->_name . '_extra_link_pic6'] = $this->model_tool_image->resize($this->config->get($this->_name . '_extra_link_image6'), 100, 100);		
		} else {
			$this->data[$this->_name . '_extra_link_pic6'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
		}
        
        if (isset($this->request->post[$this->_name . '_extra_link_afurl1'])) { 
			$this->data[$this->_name . '_extra_link_afurl1'] = $this->request->post[$this->_name . '_extra_link_afurl1']; 
		} else { 
			$this->data[$this->_name . '_extra_link_afurl1'] = $this->config->get($this->_name . '_extra_link_afurl1' ); 
		} 

        if (isset($this->request->post[$this->_name . '_extra_link_afstatus1'])) { 
			$this->data[$this->_name . '_extra_link_afstatus1'] = $this->request->post[$this->_name . '_extra_link_afstatus1']; 
		} else { 
			$this->data[$this->_name . '_extra_link_afstatus1'] = $this->config->get($this->_name . '_extra_link_afstatus1' ); 
		} 

        if (isset($this->request->post[$this->_name . '_extra_link_afurl2'])) { 
			$this->data[$this->_name . '_extra_link_afurl2'] = $this->request->post[$this->_name . '_extra_link_afurl2']; 
		} else { 
			$this->data[$this->_name . '_extra_link_afurl2'] = $this->config->get($this->_name . '_extra_link_afurl2' ); 
		} 

        if (isset($this->request->post[$this->_name . '_extra_link_afstatus2'])) { 
			$this->data[$this->_name . '_extra_link_afstatus2'] = $this->request->post[$this->_name . '_extra_link_afstatus2']; 
		} else { 
			$this->data[$this->_name . '_extra_link_afstatus2'] = $this->config->get($this->_name . '_extra_link_afstatus2' ); 
		} 

        if (isset($this->request->post[$this->_name . '_extra_link_afurl3'])) { 
			$this->data[$this->_name . '_extra_link_afurl3'] = $this->request->post[$this->_name . '_extra_link_afurl3']; 
		} else { 
			$this->data[$this->_name . '_extra_link_afurl3'] = $this->config->get($this->_name . '_extra_link_afurl3' ); 
		} 

        if (isset($this->request->post[$this->_name . '_extra_link_afstatus3'])) { 
			$this->data[$this->_name . '_extra_link_afstatus3'] = $this->request->post[$this->_name . '_extra_link_afstatus3']; 
		} else { 
			$this->data[$this->_name . '_extra_link_afstatus3'] = $this->config->get($this->_name . '_extra_link_afstatus3' ); 
		} 

        if (isset($this->request->post[$this->_name . '_extra_link_afurl4'])) { 
			$this->data[$this->_name . '_extra_link_afurl4'] = $this->request->post[$this->_name . '_extra_link_afurl4']; 
		} else { 
			$this->data[$this->_name . '_extra_link_afurl4'] = $this->config->get($this->_name . '_extra_link_afurl4' ); 
		} 

        if (isset($this->request->post[$this->_name . '_extra_link_afstatus4'])) { 
			$this->data[$this->_name . '_extra_link_afstatus4'] = $this->request->post[$this->_name . '_extra_link_afstatus4']; 
		} else { 
			$this->data[$this->_name . '_extra_link_afstatus4'] = $this->config->get($this->_name . '_extra_link_afstatus4' ); 
		} 

        if (isset($this->request->post[$this->_name . '_extra_link_afurl5'])) { 
			$this->data[$this->_name . '_extra_link_afurl5'] = $this->request->post[$this->_name . '_extra_link_afurl5']; 
		} else { 
			$this->data[$this->_name . '_extra_link_afurl5'] = $this->config->get($this->_name . '_extra_link_afurl5' ); 
		} 

        if (isset($this->request->post[$this->_name . '_extra_link_afstatus5'])) { 
			$this->data[$this->_name . '_extra_link_afstatus5'] = $this->request->post[$this->_name . '_extra_link_afstatus5']; 
		} else { 
			$this->data[$this->_name . '_extra_link_afstatus5'] = $this->config->get($this->_name . '_extra_link_afstatus5' ); 
		} 

        if (isset($this->request->post[$this->_name . '_extra_link_afurl6'])) { 
			$this->data[$this->_name . '_extra_link_afurl6'] = $this->request->post[$this->_name . '_extra_link_afurl6']; 
		} else { 
			$this->data[$this->_name . '_extra_link_afurl6'] = $this->config->get($this->_name . '_extra_link_afurl6' ); 
		} 

        if (isset($this->request->post[$this->_name . '_extra_link_afstatus6'])) { 
			$this->data[$this->_name . '_extra_link_afstatus6'] = $this->request->post[$this->_name . '_extra_link_afstatus6']; 
		} else { 
			$this->data[$this->_name . '_extra_link_afstatus6'] = $this->config->get($this->_name . '_extra_link_afstatus6' ); 
		} 
        
        $this->load->model('tool/image');
        $this->data['token'] = $this->session->data['token'];
        
        if (isset($this->request->post[$this->_name . '_extra_link_afimage1'])) {
			$this->data[$this->_name . '_extra_link_afimage1'] = $this->request->post[$this->_name . '_extra_link_afimage1'];
		} else {
			$this->data[$this->_name . '_extra_link_afimage1'] = $this->config->get($this->_name . '_extra_link_afimage1');			
		}
		
        if ($this->config->get($this->_name . '_extra_link_afimage1') && file_exists(DIR_IMAGE . $this->config->get($this->_name . '_extra_link_afimage1')) && is_file(DIR_IMAGE . $this->config->get($this->_name . '_extra_link_afimage1'))) {
			$this->data[$this->_name . '_extra_link_afpic1'] = $this->model_tool_image->resize($this->config->get($this->_name . '_extra_link_afimage1'), 100, 100);		
		} else {
			$this->data[$this->_name . '_extra_link_afpic1'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
		}

        if (isset($this->request->post[$this->_name . '_extra_link_afimage2'])) {
			$this->data[$this->_name . '_extra_link_afimage2'] = $this->request->post[$this->_name . '_extra_link_afimage2'];
		} else {
			$this->data[$this->_name . '_extra_link_afimage2'] = $this->config->get($this->_name . '_extra_link_afimage2');			
		}
		
        if ($this->config->get($this->_name . '_extra_link_afimage2') && file_exists(DIR_IMAGE . $this->config->get($this->_name . '_extra_link_afimage2')) && is_file(DIR_IMAGE . $this->config->get($this->_name . '_extra_link_afimage2'))) {
			$this->data[$this->_name . '_extra_link_afpic2'] = $this->model_tool_image->resize($this->config->get($this->_name . '_extra_link_afimage2'), 100, 100);		
		} else {
			$this->data[$this->_name . '_extra_link_afpic2'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
		}

        if (isset($this->request->post[$this->_name . '_extra_link_afimage3'])) {
			$this->data[$this->_name . '_extra_link_afimage3'] = $this->request->post[$this->_name . '_extra_link_afimage3'];
		} else {
			$this->data[$this->_name . '_extra_link_afimage3'] = $this->config->get($this->_name . '_extra_link_afimage3');			
		}
		
        if ($this->config->get($this->_name . '_extra_link_afimage3') && file_exists(DIR_IMAGE . $this->config->get($this->_name . '_extra_link_afimage3')) && is_file(DIR_IMAGE . $this->config->get($this->_name . '_extra_link_afimage3'))) {
			$this->data[$this->_name . '_extra_link_afpic3'] = $this->model_tool_image->resize($this->config->get($this->_name . '_extra_link_afimage3'), 100, 100);		
		} else {
			$this->data[$this->_name . '_extra_link_afpic3'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
		}

        if (isset($this->request->post[$this->_name . '_extra_link_afimage4'])) {
			$this->data[$this->_name . '_extra_link_afimage4'] = $this->request->post[$this->_name . '_extra_link_afimage4'];
		} else {
			$this->data[$this->_name . '_extra_link_afimage4'] = $this->config->get($this->_name . '_extra_link_afimage4');			
		}
		
        if ($this->config->get($this->_name . '_extra_link_afimage4') && file_exists(DIR_IMAGE . $this->config->get($this->_name . '_extra_link_afimage4')) && is_file(DIR_IMAGE . $this->config->get($this->_name . '_extra_link_afimage4'))) {
			$this->data[$this->_name . '_extra_link_afpic4'] = $this->model_tool_image->resize($this->config->get($this->_name . '_extra_link_afimage4'), 100, 100);		
		} else {
			$this->data[$this->_name . '_extra_link_afpic4'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
		}

        if (isset($this->request->post[$this->_name . '_extra_link_afimage5'])) {
			$this->data[$this->_name . '_extra_link_afimage5'] = $this->request->post[$this->_name . '_extra_link_afimage5'];
		} else {
			$this->data[$this->_name . '_extra_link_afimage5'] = $this->config->get($this->_name . '_extra_link_afimage5');			
		}
		
        if ($this->config->get($this->_name . '_extra_link_afimage5') && file_exists(DIR_IMAGE . $this->config->get($this->_name . '_extra_link_afimage5')) && is_file(DIR_IMAGE . $this->config->get($this->_name . '_extra_link_afimage5'))) {
			$this->data[$this->_name . '_extra_link_afpic5'] = $this->model_tool_image->resize($this->config->get($this->_name . '_extra_link_afimage5'), 100, 100);		
		} else {
			$this->data[$this->_name . '_extra_link_afpic5'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
		}

        if (isset($this->request->post[$this->_name . '_extra_link_afimage6'])) {
			$this->data[$this->_name . '_extra_link_afimage6'] = $this->request->post[$this->_name . '_extra_link_afimage6'];
		} else {
			$this->data[$this->_name . '_extra_link_afimage6'] = $this->config->get($this->_name . '_extra_link_afimage6');			
		}
		
        if ($this->config->get($this->_name . '_extra_link_afimage6') && file_exists(DIR_IMAGE . $this->config->get($this->_name . '_extra_link_afimage6')) && is_file(DIR_IMAGE . $this->config->get($this->_name . '_extra_link_afimage6'))) {
			$this->data[$this->_name . '_extra_link_afpic6'] = $this->model_tool_image->resize($this->config->get($this->_name . '_extra_link_afimage6'), 100, 100);		
		} else {
			$this->data[$this->_name . '_extra_link_afpic6'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
		}

        $this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);

        
        $this->data['style_a'] = $this->config->get($this->_name . '_style_a');
        if (isset($this->request->post['style_a'])) {
			$this->data['style_a'] = $this->request->post['style_a'];
		} else {
			$this->data['style_a'] = $this->config->get('style_a');
		}   
        
        $this->data['icotype_a'] = $this->config->get($this->_name . '_icotype_a');
        if (isset($this->request->post['icotype_a'])) {
			$this->data['icotype_a'] = $this->request->post['icotype_a'];
		} else {
			$this->data['icotype_a'] = $this->config->get('icotype_a');
		}   
        
        $this->data['style_af'] = $this->config->get($this->_name . '_style_af');
        if (isset($this->request->post['style_af'])) {
			$this->data['style_af'] = $this->request->post['style_af'];
		} else {
			$this->data['style_af'] = $this->config->get('style_af');
		}   
        
        $this->data['icotype_af'] = $this->config->get($this->_name . '_icotype_af');
        if (isset($this->request->post['icotype_af'])) {
			$this->data['icotype_af'] = $this->request->post['icotype_af'];
		} else {
			$this->data['icotype_af'] = $this->config->get('icotype_af');
		}   

        
        if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

        $this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/accountplus', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('module/accountplus', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['modules'] = array();
		
		if (isset($this->request->post['accountplus_module'])) {
			$this->data['modules'] = $this->request->post['accountplus_module'];
		} elseif ($this->config->get('accountplus_module')) { 
			$this->data['modules'] = $this->config->get('accountplus_module');
		}	
		
		$this->load->model('design/layout');
		
		$this->data['layouts'] = $this->model_design_layout->getLayouts();
						
		$this->template = 'module/accountplus.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/accountplus')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

        if (($this->request->post[$this->_name . '_extra_link_status1'] == '1') && ((utf8_strlen($this->request->post[$this->_name . '_extra_link_url1']) < 3) || (utf8_strlen($this->request->post[$this->_name . '_extra_link_url1']) > 64))) {
      		$this->error[$this->_name . '_extra_link_url1'] = $this->language->get('error_extra_link_url');
    	}

        if (($this->request->post[$this->_name . '_extra_link_status2'] == '1') && ((utf8_strlen($this->request->post[$this->_name . '_extra_link_url2']) < 3) || (utf8_strlen($this->request->post[$this->_name . '_extra_link_url2']) > 64))) {
      		$this->error[$this->_name . '_extra_link_url2'] = $this->language->get('error_extra_link_url');
    	}

        if (($this->request->post[$this->_name . '_extra_link_status3'] == '1') && ((utf8_strlen($this->request->post[$this->_name . '_extra_link_url3']) < 3) || (utf8_strlen($this->request->post[$this->_name . '_extra_link_url3']) > 64))) {
      		$this->error[$this->_name . '_extra_link_url3'] = $this->language->get('error_extra_link_url');
    	}

        if (($this->request->post[$this->_name . '_extra_link_status4'] == '1') && ((utf8_strlen($this->request->post[$this->_name . '_extra_link_url4']) < 3) || (utf8_strlen($this->request->post[$this->_name . '_extra_link_url4']) > 64))) {
      		$this->error[$this->_name . '_extra_link_url4'] = $this->language->get('error_extra_link_url');
    	}

        if (($this->request->post[$this->_name . '_extra_link_status5'] == '1') && ((utf8_strlen($this->request->post[$this->_name . '_extra_link_url5']) < 3) || (utf8_strlen($this->request->post[$this->_name . '_extra_link_url5']) > 64))) {
      		$this->error[$this->_name . '_extra_link_url5'] = $this->language->get('error_extra_link_url');
    	}

        if (($this->request->post[$this->_name . '_extra_link_status6'] == '1') && ((utf8_strlen($this->request->post[$this->_name . '_extra_link_url6']) < 3) || (utf8_strlen($this->request->post[$this->_name . '_extra_link_url6']) > 64))) {
      		$this->error[$this->_name . '_extra_link_url6'] = $this->language->get('error_extra_link_url');
    	}

        if (($this->request->post[$this->_name . '_extra_link_afstatus1'] == '1') && ((utf8_strlen($this->request->post[$this->_name . '_extra_link_afurl1']) < 3) || (utf8_strlen($this->request->post[$this->_name . '_extra_link_afurl1']) > 64))) {
      		$this->error[$this->_name . '_extra_link_afurl1'] = $this->language->get('error_extra_link_url');
    	}

        if (($this->request->post[$this->_name . '_extra_link_afstatus2'] == '1') && ((utf8_strlen($this->request->post[$this->_name . '_extra_link_afurl2']) < 3) || (utf8_strlen($this->request->post[$this->_name . '_extra_link_afurl2']) > 64))) {
      		$this->error[$this->_name . '_extra_link_afurl2'] = $this->language->get('error_extra_link_url');
    	}

        if (($this->request->post[$this->_name . '_extra_link_afstatus3'] == '1') && ((utf8_strlen($this->request->post[$this->_name . '_extra_link_afurl3']) < 3) || (utf8_strlen($this->request->post[$this->_name . '_extra_link_afurl3']) > 64))) {
      		$this->error[$this->_name . '_extra_link_afurl3'] = $this->language->get('error_extra_link_url');
    	}

        if (($this->request->post[$this->_name . '_extra_link_afstatus4'] == '1') && ((utf8_strlen($this->request->post[$this->_name . '_extra_link_afurl4']) < 3) || (utf8_strlen($this->request->post[$this->_name . '_extra_link_afurl4']) > 64))) {
      		$this->error[$this->_name . '_extra_link_afurl4'] = $this->language->get('error_extra_link_url');
    	}

        if (($this->request->post[$this->_name . '_extra_link_afstatus5'] == '1') && ((utf8_strlen($this->request->post[$this->_name . '_extra_link_afurl5']) < 3) || (utf8_strlen($this->request->post[$this->_name . '_extra_link_afurl5']) > 64))) {
      		$this->error[$this->_name . '_extra_link_afurl5'] = $this->language->get('error_extra_link_url');
    	}

        if (($this->request->post[$this->_name . '_extra_link_afstatus6'] == '1') && ((utf8_strlen($this->request->post[$this->_name . '_extra_link_afurl6']) < 3) || (utf8_strlen($this->request->post[$this->_name . '_extra_link_afurl6']) > 64))) {
      		$this->error[$this->_name . '_extra_link_afurl6'] = $this->language->get('error_extra_link_url');
    	}

        $this->load->model('localisation/language');
		
		$languages = $this->model_localisation_language->getLanguages();
		
		foreach ($languages as $language) {

    		if (($this->request->post[$this->_name . '_extra_link_status1'] == '1') && ((utf8_strlen($this->request->post[$this->_name . '_extra_link_atitle1' . $language['language_id']]) < 3) || (utf8_strlen($this->request->post[$this->_name . '_extra_link_atitle1' . $language['language_id']]) > 64))) {
    			$this->error[$this->_name . '_extra_link_atitle1' . $language['language_id']] = $this->language->get('error_extra_link_title');
    		}

    		if (($this->request->post[$this->_name . '_extra_link_status2'] == '1') && ((utf8_strlen($this->request->post[$this->_name . '_extra_link_atitle2' . $language['language_id']]) < 3) || (utf8_strlen($this->request->post[$this->_name . '_extra_link_atitle2' . $language['language_id']]) > 64))) {
    			$this->error[$this->_name . '_extra_link_atitle2' . $language['language_id']] = $this->language->get('error_extra_link_title');
    		}

    		if (($this->request->post[$this->_name . '_extra_link_status3'] == '1') && ((utf8_strlen($this->request->post[$this->_name . '_extra_link_atitle3' . $language['language_id']]) < 3) || (utf8_strlen($this->request->post[$this->_name . '_extra_link_atitle3' . $language['language_id']]) > 64))) {
    			$this->error[$this->_name . '_extra_link_atitle3' . $language['language_id']] = $this->language->get('error_extra_link_title');
    		}

    		if (($this->request->post[$this->_name . '_extra_link_status4'] == '1') && ((utf8_strlen($this->request->post[$this->_name . '_extra_link_atitle4' . $language['language_id']]) < 3) || (utf8_strlen($this->request->post[$this->_name . '_extra_link_atitle4' . $language['language_id']]) > 64))) {
    			$this->error[$this->_name . '_extra_link_atitle4' . $language['language_id']] = $this->language->get('error_extra_link_title');
    		}

    		if (($this->request->post[$this->_name . '_extra_link_status5'] == '1') && ((utf8_strlen($this->request->post[$this->_name . '_extra_link_atitle5' . $language['language_id']]) < 3) || (utf8_strlen($this->request->post[$this->_name . '_extra_link_atitle5' . $language['language_id']]) > 64))) {
    			$this->error[$this->_name . '_extra_link_atitle5' . $language['language_id']] = $this->language->get('error_extra_link_title');
    		}

    		if (($this->request->post[$this->_name . '_extra_link_status6'] == '1') && ((utf8_strlen($this->request->post[$this->_name . '_extra_link_atitle6' . $language['language_id']]) < 3) || (utf8_strlen($this->request->post[$this->_name . '_extra_link_atitle6' . $language['language_id']]) > 64))) {
    			$this->error[$this->_name . '_extra_link_atitle6' . $language['language_id']] = $this->language->get('error_extra_link_title');
    		}

    		if (($this->request->post[$this->_name . '_extra_link_afstatus1'] == '1') && ((utf8_strlen($this->request->post[$this->_name . '_extra_link_afatitle1' . $language['language_id']]) < 3) || (utf8_strlen($this->request->post[$this->_name . '_extra_link_afatitle1' . $language['language_id']]) > 64))) {
    			$this->error[$this->_name . '_extra_link_afatitle1' . $language['language_id']] = $this->language->get('error_extra_link_title');
    		}

    		if (($this->request->post[$this->_name . '_extra_link_afstatus2'] == '1') && ((utf8_strlen($this->request->post[$this->_name . '_extra_link_afatitle2' . $language['language_id']]) < 3) || (utf8_strlen($this->request->post[$this->_name . '_extra_link_afatitle2' . $language['language_id']]) > 64))) {
    			$this->error[$this->_name . '_extra_link_afatitle2' . $language['language_id']] = $this->language->get('error_extra_link_title');
    		}

    		if (($this->request->post[$this->_name . '_extra_link_afstatus3'] == '1') && ((utf8_strlen($this->request->post[$this->_name . '_extra_link_afatitle3' . $language['language_id']]) < 3) || (utf8_strlen($this->request->post[$this->_name . '_extra_link_afatitle3' . $language['language_id']]) > 64))) {
    			$this->error[$this->_name . '_extra_link_afatitle3' . $language['language_id']] = $this->language->get('error_extra_link_title');
    		}

    		if (($this->request->post[$this->_name . '_extra_link_afstatus4'] == '1') && ((utf8_strlen($this->request->post[$this->_name . '_extra_link_afatitle4' . $language['language_id']]) < 3) || (utf8_strlen($this->request->post[$this->_name . '_extra_link_afatitle4' . $language['language_id']]) > 64))) {
    			$this->error[$this->_name . '_extra_link_afatitle4' . $language['language_id']] = $this->language->get('error_extra_link_title');
    		}

    		if (($this->request->post[$this->_name . '_extra_link_afstatus5'] == '1') && ((utf8_strlen($this->request->post[$this->_name . '_extra_link_afatitle5' . $language['language_id']]) < 3) || (utf8_strlen($this->request->post[$this->_name . '_extra_link_afatitle5' . $language['language_id']]) > 64))) {
    			$this->error[$this->_name . '_extra_link_afatitle5' . $language['language_id']] = $this->language->get('error_extra_link_title');
    		}

    		if (($this->request->post[$this->_name . '_extra_link_afstatus6'] == '1') && ((utf8_strlen($this->request->post[$this->_name . '_extra_link_afatitle6' . $language['language_id']]) < 3) || (utf8_strlen($this->request->post[$this->_name . '_extra_link_afatitle6' . $language['language_id']]) > 64))) {
    			$this->error[$this->_name . '_extra_link_afatitle6' . $language['language_id']] = $this->language->get('error_extra_link_title');
    		}

        }

        if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>