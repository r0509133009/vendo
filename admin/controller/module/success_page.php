<?php
include DIR_APPLICATION . '/view/javascript/SuccessPage/compatibility.php';

class ControllerModuleSuccessPage extends CompatibilityAdmin {
	const MODULE_VERSION = "v2.0";

	private $error = array();
	private $sections = array(
		'text' => 'Text/Html',
		'social' => 'Social share',
		'address' => 'Address payment/shipping',
		'comment' => 'Comment',
		'bank_transfer' => 'Bank transfer instructions',
		'payment' => 'Instructions for Payment',
		'cart' => 'Purchased products',
		'order' => 'Order information',
		'product' => 'Product box',
		'facebook' => 'Facebook',
		'youtube' => 'Youtube'
	);
	private $fields = array(
		'status' => array('default' => '0', 'decode' => false, 'required' => true),
		'coupon_id' => array('default' => '0', 'decode' => false, 'required' => false),
		'invoice_status' => array('default' => '0', 'decode' => false, 'required' => false),
		'button_status' => array('default' => '1', 'decode' => false, 'required' => false),
		'adwords_status' => array('default' => '0', 'decode' => false, 'required' => false),
		'adwords_conversion_id' => array('default' => '', 'decode' => false, 'required' => false),
		'adwords_conversion_label' => array('default' => '', 'decode' => false, 'required' => false),
		'section' => array('default' => array(), 'decode' => false, 'required' => false),
		'css' => array('default' => '.success_page { }', 'decode' => false, 'required' => false),
		'javascript' => array('default' => '', 'decode' => false, 'required' => false),
		'license_key' => array('default' => '', 'decode' => false, 'required' => false),
		'license' => array('default' => '', 'decode' => false, 'required' => false)
	);

	public function index() {
		$data = array_merge(array(), $this->language->load('module/success_page'));

		$this->loadStyles('SuccessPage');

		$this->document->setTitle($this->language->get('heading_title'));
		$data['heading_title'] = $this->language->get('heading_title') . ' ' . self::MODULE_VERSION;

		if (isset($this->request->get['filter_store_id'])) {
			$filter_store_id = (int)$this->request->get['filter_store_id'];
		} else {
			$filter_store_id = 0;
		}

		$url = '&filter_store_id=' . (int)$filter_store_id;
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateSetting()) {
			$section = $this->request->post['success_page']['section'];
			ksort($section);
			$this->request->post['success_page']['section'] = $section;

			$this->editSetting('success_page', $this->request->post['success_page'], $filter_store_id);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirectTo('module/success_page', $url);
		}

 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['error'])) {
			$data['error'] = $this->error['error'];
		} else {
			$data['error'] = array();
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['action'] = $this->url->link('module/success_page', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		$config_store = $this->getSetting('success_page', $filter_store_id);

		foreach ($this->fields as $key => $value) {
			if (isset($this->request->post['success_page'][$key])) {
				$data[$key] = $this->request->post['success_page'][$key];
			} elseif (isset($config_store[$key])) {
				$data[$key] = $config_store[$key];
			} else {
				if ($value['decode']) {
					$value['default'] = base64_decode($value['default']);
				}

				$data[$key] = $value['default'];
			}
		}

		if ($data['section']) {
			$this->load->model('catalog/product');

			foreach ($data['section'] as $key => $module) {
				if (key($module) == 'product') {
					if (isset($module['product']['product']) && $module['product']['product']) {
						$product = array();

						foreach ($module['product']['product'] as $product_id) {
							$product_info = $this->model_catalog_product->getProduct($product_id);

							if ($product_info) {
								$product[] = array(
									'product_id' => $product_info['product_id'],
									'name'       => $product_info['name']
								);
							}
						}

						$data['section'][$key]['product']['product'] = $product;
					}
				}
			}
		}

		$data['stores'] = $this->getStores('module/success_page');

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		$data['lines'] = array('Solid', 'Dashed', 'Dotted', 'Double', 'Groove');
		$data['aligns'] = array('left' => $this->language->get('text_left'), 'center' => $this->language->get('text_center'), 'right' => $this->language->get('text_right'));

		$data['coupons'] = $this->getCoupons(0, 9999);

		$this->load->model('setting/extension');

		$extensions = $this->model_setting_extension->getInstalled('payment');

		foreach ($extensions as $key => $value) {
			if (!file_exists(DIR_APPLICATION . 'controller/payment/' . $value . '.php')) {
				$this->model_extension_extension->uninstall('payment', $value);

				unset($extensions[$key]);
			}
		}

		$data['payments'] = array();

		$files = glob(DIR_APPLICATION . 'controller/payment/*.php');

		if ($files) {
			foreach ($files as $file) {
				$extension = basename($file, '.php');

				if (!in_array($extension, $extensions)) {
					continue;
				}

				$this->load->language('payment/' . $extension);

				$data['payments'][$extension] = array(
					'name'      => $this->language->get('heading_title'),
					'extension' => $extension
				);
			}
		}

		$data['sections'] = $this->sections;

		$data['token'] = $this->session->data['token'];

		$data['filter_store_id'] = $filter_store_id;

		$this->toOutput('module/success_page.tpl', $data);
	}

	private function validateSetting() {
		if (!$this->user->hasPermission('modify', 'module/success_page')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if ($this->request->post['success_page']) {
			foreach ($this->fields as $key => $value) {
				if ($value['required']) {
					if (is_array($this->request->post['success_page'][$key])) {
						foreach ($this->request->post['success_page'][$key] as $key2 => $setting) {
							if (is_array($setting)) {
								foreach ($setting as $key3 => $value2) {
									if (is_array($value2)) {
										foreach ($value2 as $value3) {
											if (!$value3) {
												$this->error['error'][$key] = $this->language->get('error_' . $key);
											}
										}
									} elseif (!isset($value2) || empty($value2)) {
										$this->error['error'][$key] = $this->language->get('error_' . $key);
									}
								}
							} elseif (!isset($setting) || $setting === null || $setting == '') {
								$this->error['error'][$key] = $this->language->get('error_' . $key);
							}
						}
					} elseif (!isset($this->request->post['success_page'][$key]) || $this->request->post['success_page'][$key] === null || $this->request->post['success_page'][$key] == '') {
						$this->error['error'][$key] = $this->language->get('error_' . $key);
					}
				}
			}

			if (isset($this->error['error'])) {
				$this->error['warning'] = $this->language->get('error_required');
			}
		} else {
			$this->error['warning'] = $this->language->get('error_module');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>