<?php
/**
 * iScroller 1.2.1, May 7, 2016
 * Copyright 2014-2016 Igor Bukin / egozza88@gmail.com
 * License: Commercial. Reselling of this software or its derivatives is not allowed. You may use this software for one website ONLY including all its subdomains if the top level domain belongs to you and all subdomains are parts of the same OpenCart store.
 * Support: oc.iscroller@gmail.com
 */
class ControllerModuleIscroller extends Controller 
{
    private $_data = array('error_warning' => false, 'success' => false);

    private $_defaultSettings = array(
        'infScroll' => array(
            'enabled' => 1,
            'showSeparator' => 1,
            'minDistToBottom' => 100,
            'containerSelector' => '.product-list, .product-grid',
            'paginatorSelector' => '.pagination',
            'itemSelector' => '.product-list > div, .product-grid > div',
            'debugMode' => 0,
            'loadingMode' => 'auto',
            'animation' => 0,
            'statefulScroll' => 1,
            'show_btn_after' => 3,
            'customJsCode' => '',
            'customCssCode' => '',
            'domObserverEnabled' => 1,
        ),
        'scrollTop' => array(
            'enabled' => 0,
            'position' => 'left',
            'fitTo' => '',
            'barColor' => 'rgba(198, 198, 198, 0.25)',
            'arrowColor' => 'rgba(49, 49, 49, 0.8)',
            'speed' => 500,
            'minWidthToShow' => 0,
            'minWidthToShowAsBar' => 0,
        ),
        'module_numb' => 1,
        'name' => '',
    );
    
    public function index()
    {
        $modules = $this->config->get('iscroller') 
                ? $this->config->get('iscroller')
                : array();
        $moduleId = isset($this->request->get['module_id']) ? $this->request->get['module_id'] : count($modules) + 1;
        
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->request->post = $this->specialCharsFix($this->request->post);
            $modules[$moduleId] = $this->request->post['iscroller_module'];
            $this->save($modules);

			$this->session->data['success'] = $this->language->get('text_success');

			if ($this->request->post['action'] == 'apply') {
                $this->redirect($this->url->link('module/iscroller', 'token=' . $this->session->data['token'] . '&module_id=' . $moduleId, 'SSL'));
            } else {
                $this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
            }
		}
        if (isset($this->session->data['success'])) {
			$this->_data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		}
        
        $isMijoShop = class_exists('MijoShop') && defined('JPATH_MIJOSHOP_OC');
        if ($isMijoShop) {
            MijoShop::get('base')->addHeader(JPATH_MIJOSHOP_OC . 'view/javascript/bootstrap.tooltip-popover.js', false);
            MijoShop::get('base')->addHeader(JPATH_MIJOSHOP_OC . 'view/javascript/tinycolor.js', false);
            MijoShop::get('base')->addHeader(JPATH_MIJOSHOP_OC . 'view/javascript/bootstrap.colorpickersliders.min.js', false);
        } else {
            $this->document->addScript('view/javascript/bootstrap.tooltip-popover.js');
            $this->document->addScript('view/javascript/tinycolor.js');
            $this->document->addScript('view/javascript/bootstrap.colorpickersliders.min.js');
        }
        $this->document->addStyle('view/stylesheet/bootstrap.colorpickersliders.min.css');
        
        $this->_defaultSettings = $this->adjustSettingsForTheme($this->_defaultSettings);
        
        $this->_data['modules'] = $modules;
        if (isset($this->request->post['iscroller_module'])) {
            $this->_data['settings'] = $this->request->post['iscroller_module'];
        } elseif ($this->config->get('iscroller') && isset($this->request->get['module_id'])) {
            $this->_data['settings'] = $modules[$this->request->get['module_id']];
        } else {
            $this->_data['settings'] = $this->_defaultSettings;
        }
        $this->_data['settings'] = $this->mergeSettings($this->_data['settings'], $this->_defaultSettings);
        $this->_data['module_numb'] = (int)$this->_data['settings']['module_numb'];
        
        $lang = $this->load->language('module/iscroller');
        $this->document->setTitle($this->language->get('heading_title_adv'));
        if (count($lang)) {
            foreach ($lang as $var => $val) {
                $this->_data['lang_' . $var] = $val;
            }
        }
        $this->_data['text_yes'] = $this->language->get('text_yes');
        $this->_data['text_no']  = $this->language->get('text_no');
        $this->_data['button_cancel'] = $this->language->get('button_cancel');
        
        
        $this->_data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
		);

		$this->_data['breadcrumbs'][] = array(
			'text' => $this->language->get('modules'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
		);

		$this->_data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title_adv'),
			'href' => $this->url->link('module/iscroller', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
		);
        $this->_data['moduleId'] = isset($this->request->get['module_id']) ? $this->request->get['module_id'] : 0;

		$this->_data['action'] = $this->url->link('module/iscroller', 'token=' . $this->session->data['token'], 'SSL');

		$this->_data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
        
		$this->_data['removeModuleAction'] = $this->url->link('module/iscroller/removeModule', 'token=' . $this->session->data['token'], 'SSL');
        
		$this->_data['filemanager_route'] = $this->url->link('common/filemanager', 'token=' . $this->session->data['token'] . '&target=loader-img&thumb=thumb-image', 'SSL');
        $this->_data['catalog'] = $this->config->get('config_secure') ? HTTPS_CATALOG : HTTP_CATALOG;
        
        $this->load->model('localisation/language');
		
		$this->_data['languages'] = $this->model_localisation_language->getLanguages();
        
        $this->load->model('design/layout');
		
		$this->_data['layouts'] = $this->model_design_layout->getLayouts();

        $this->template = 'module/iscroller.tpl';
        $this->children = array(
			'common/header',
			'common/footer'
		);
        
        $this->data = $this->_data;
		$this->response->setOutput($this->render());
    }
    
    public function removeModule()
    {
        if (isset($this->request->get['module_id'])) {
            $moduleId = $this->request->get['module_id'];
            $config = $this->config->get('iscroller');
            if (isset($config[$moduleId])) {
                unset($config[$moduleId]);
                $config = array_values($config);
                $len = count($config);
                for ($i = $len - 1; $i >= 0; $i --) {
                    $config[$i + 1] = $config[$i];
                }
                unset($config[0]);
                $this->save($config);
            }
        }
        $this->redirect($this->url->link('module/iscroller', 'token=' . $this->session->data['token'], 'SSL'));
    }
    
    protected function save($settings)
    {
        if (empty($settings)) {
            return;
        }
        $layouts = array();
        foreach ($settings as $id => $module) {
            if (count($module['layout'])) {
                foreach ($module['layout'] as $layout) {
                    if (!isset($layouts[$layout['layout_id']])) {
                        $layouts[$layout['layout_id']] = array(
                            'layout_id' => $layout['layout_id'],
                            'position' => $layout['position'],
                            'status' => 1,
                            'sort_order' => 0,
                            'iscModules' => array(),
                        );
                    }
                    $layouts[$layout['layout_id']]['iscModules'][] = $id;
                }
            }
        }
        
        $data = array(
            'iscroller_status' => '1',
            'iscroller_module' => array_values($layouts),
            'iscroller'        => $settings,
        );
        
        $this->load->model('setting/setting');
        $this->model_setting_setting->editSetting('iscroller', $data);
    }


    protected function validate() {
        $this->load->language('module/iscroller');
        $hasAccess = $this->user->hasPermission('modify', 'module/iscroller');
		if (!$hasAccess) {
			$this->_data['error_warning'] = $this->language->get('error_permission');
		}
		return $hasAccess;
	}
    
    protected function mergeSettings($set, $defSet) {
        if (!isset($set['infScroll'])) {
            $set['infScroll'] = array();
        }
        if (!isset($set['scrollTop'])) {
            $set['scrollTop'] = array();
        }
        foreach ($defSet['infScroll'] as $k => $v) {
            if (!isset($set['infScroll'][$k])) {
                $set['infScroll'][$k] = $v;
            }
        }
        foreach ($defSet['scrollTop'] as $k => $v) {
            if (!isset($set['scrollTop'][$k])) {
                $set['scrollTop'][$k] = $v;
            }
        }
        return $set;
    }
    
    protected function specialCharsFix($param) {
        if (is_array($param)) {
            foreach ($param as $k => $v) {
                $param[$k] = $this->specialCharsFix($v);
            }
            return $param;
        } else {
            return htmlspecialchars_decode($param);
        }
    }
    
    protected function adjustSettingsForTheme($settings) {
        if ( preg_match('/^journal/i', $this->config->get('config_template')) ) {
            $settings['infScroll']['containerSelector'] = '.main-products.product-list, .main-products.product-grid';
            $settings['infScroll']['paginatorSelector'] = '.pagination';
            $settings['infScroll']['itemSelector'] = '.main-products.product-list>div, .main-products.product-grid>div';
        } elseif ( preg_match('/^megashop/i', $this->config->get('config_template')) ) {
            $settings['infScroll']['containerSelector'] = '.products-block ';
            $settings['infScroll']['paginatorSelector'] = '.paginations';
            $settings['infScroll']['itemSelector'] = '.products-block > .row';
        }
        
        return $settings;
    }
}