<?php
require_once(substr_replace(DIR_SYSTEM, '', -7) . 'vendor/equotix/related_pro/equotix.php');
class ControllerModuleRelatedPro extends Equotix {
	protected $version = '2.2.3';
	protected $code = 'related_pro';
	protected $extension = 'Related Pro';
	protected $extension_id = '65';
	protected $purchase_url = 'related-pro';
	protected $purchase_id = '17144';
	protected $error = array();

	public function index() {   
		$this->language->load('module/related_pro');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('related_pro', $this->request->post);		

			$this->cache->delete('product');

			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['tab_general'] = $this->language->get('tab_general');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_content_top'] = $this->language->get('text_content_top');
		$this->data['text_content_bottom'] = $this->language->get('text_content_bottom');		
		$this->data['text_column_left'] = $this->language->get('text_column_left');
		$this->data['text_column_right'] = $this->language->get('text_column_right');
		$this->data['text_category'] = $this->language->get('text_category');
		$this->data['text_name'] = $this->language->get('text_name');
		$this->data['text_related'] = $this->language->get('text_related');

		$this->data['entry_display_tab'] = $this->language->get('entry_display_tab');
		$this->data['entry_limit_tab'] = $this->language->get('entry_limit_tab');
		$this->data['entry_related'] = $this->language->get('entry_related');
		$this->data['entry_random'] = $this->language->get('entry_random');
		$this->data['entry_join'] = $this->language->get('entry_join');
		$this->data['entry_exclude'] = $this->language->get('entry_exclude');
		$this->data['entry_limit'] = $this->language->get('entry_limit');
		$this->data['entry_image'] = $this->language->get('entry_image');
		$this->data['entry_layout'] = $this->language->get('entry_layout');
		$this->data['entry_position'] = $this->language->get('entry_position');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add_module'] = $this->language->get('button_add_module');
		$this->data['button_remove'] = $this->language->get('button_remove');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['image'])) {
			$this->data['error_image'] = $this->error['image'];
		} else {
			$this->data['error_image'] = array();
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/related_pro', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->data['action'] = $this->url->link('module/related_pro', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['modules'] = array();

		if (isset($this->request->post['related_pro_module'])) {
			$this->data['modules'] = $this->request->post['related_pro_module'];
		} elseif ($this->config->get('related_pro_module')) { 
			$this->data['modules'] = $this->config->get('related_pro_module');
		}

		if (isset($this->request->post['related_pro_display_tab'])) {
			$this->data['related_pro_display_tab'] = $this->request->post['related_pro_display_tab'];
		} else { 
			$this->data['related_pro_display_tab'] = $this->config->get('related_pro_display_tab');
		}
		
		if (isset($this->request->post['related_pro_limit_tab'])) {
			$this->data['related_pro_limit_tab'] = $this->request->post['related_pro_limit_tab'];
		} else { 
			$this->data['related_pro_limit_tab'] = $this->config->get('related_pro_limit_tab');
		}
		
		if (isset($this->request->post['related_pro_category'])) {
			$this->data['related_pro_category'] = $this->request->post['related_pro_category'];
		} else { 
			$this->data['related_pro_category'] = $this->config->get('related_pro_category');
		}
		
		if (isset($this->request->post['related_pro_name'])) {
			$this->data['related_pro_name'] = $this->request->post['related_pro_name'];
		} else { 
			$this->data['related_pro_name'] = $this->config->get('related_pro_name');
		}
		
		if (isset($this->request->post['related_pro_related'])) {
			$this->data['related_pro_related'] = $this->request->post['related_pro_related'];
		} else { 
			$this->data['related_pro_related'] = $this->config->get('related_pro_related');
		}
		
		if (isset($this->request->post['related_pro_random'])) {
			$this->data['related_pro_random'] = $this->request->post['related_pro_random'];
		} else { 
			$this->data['related_pro_random'] = $this->config->get('related_pro_random');
		}
		
		if (isset($this->request->post['related_pro_join'])) {
			$this->data['related_pro_join'] = $this->request->post['related_pro_join'];
		} else { 
			$this->data['related_pro_join'] = $this->config->get('related_pro_join');
		}
		
		if (isset($this->request->post['related_pro_exclude'])) {
			$this->data['related_pro_exclude'] = $this->request->post['related_pro_exclude'];
		} else { 
			$this->data['related_pro_exclude'] = $this->config->get('related_pro_exclude');
		}

		$this->load->model('design/layout');

		$this->data['layouts'] = $this->model_design_layout->getLayouts();

		$this->generateOutput('module/related_pro.tpl');
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/related_pro') || !$this->validated()) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (isset($this->request->post['related_pro_module'])) {
			foreach ($this->request->post['related_pro_module'] as $key => $value) {
				if (!$value['image_width'] || !$value['image_height']) {
					$this->error['image'][$key] = $this->language->get('error_image');
				}
			}
		}		

		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>