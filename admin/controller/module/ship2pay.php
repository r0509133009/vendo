<?php
class ControllerModuleShip2Pay extends Controller {
	private $error = array();

	public function index() {
		$this->language->load('module/ship2pay');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/ship2pay', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['action'] = $this->url->link('module/ship2pay', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['entry_shipping_method'] = $this->language->get('entry_shipping_method');
		$this->data['entry_allowed_payment'] = $this->language->get('entry_allowed_payment');
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
		
		$results = $this->request->post;

            foreach($results as $shipping=>$payment) {
            
            $shipping = str_replace('_', '.', $shipping);

            $arrlength=count($payment);
            $input_payment = '';
            for($i=0;$i<$arrlength;$i++) {
              if (strlen($payment[$i])>0) {
              $input_payment = $input_payment . $payment[$i].';';
              }
              }
              
           $query = $this->db->query("REPLACE INTO " . DB_PREFIX . "ship2pay SET shipment  = '" . $shipping . "', payments_allowed  = '" . $input_payment . "'");
           
            }
	
			
		$this->session->data['success'] = $this->language->get('text_success');
			
		$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}
				
            //get results from ship2pay table

            $query = $this->db->query("SELECT shipment, payments_allowed FROM " . DB_PREFIX . "ship2pay");
            
            $modules = array();
		if ($query->num_rows > 0) {
		foreach ($query->rows as $mods) {
		$modules[$mods['shipment']] = $mods['payments_allowed'];
		}
		}

            $this->data['modules'] = $modules;
            
            
            //get payment options
            
            $this->load->model('setting/extension');
            
            $extensions = $this->model_setting_extension->getInstalled('payment');

		$this->data['extensions'] = array();
		
		$files = glob(DIR_APPLICATION . 'controller/payment/*.php');

		if ($files) {
			foreach ($files as $file) {
				$extension = basename($file, '.php');

				$this->language->load('payment/' . $extension);

				$this->data['extensions'][] = array(
					'name'       => $this->language->get('heading_title'),
					'status'     => $this->config->get($extension . '_status'),
					'code'       => $extension
				);
			}
		}
		
		//get shipping costs
		
		
		$shipping_methods = $this->model_setting_extension->getInstalled('shipping');

		$this->data['shipping_methods'] = array();

		$files = glob(DIR_APPLICATION . 'controller/shipping/*.php');

		if ($files) {
			foreach ($files as $file) {
				$shiping_method = basename($file, '.php');

				$this->language->load('shipping/' . $shiping_method);

				$this->data['shipping_methods'][] = array(
					'name'       => $this->language->get('heading_title'),
					'status'     => $this->config->get($shiping_method . '_status'),
					'code'       => $shiping_method

				);
			}
		}
		
	
            $this->template = 'module/ship2pay.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/ship2pay')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	public function install() {
	
	$this->db->query("
	CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ship2pay` (
      `shipment` varchar(100) NOT NULL,
      `payments_allowed` varchar(250) NOT NULL,
      PRIMARY KEY  (`shipment`)
	) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci;");
	
	$this->db->query("INSERT INTO `" . DB_PREFIX . "setting` (`store_id`, `group`, `key`, `value`) VALUES (0, 'ship2pay', 'ship2pay_status', '1')");
			
	}

	public function uninstall() {
      $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "ship2pay`;");
      $this->db->query("DELETE FROM `" . DB_PREFIX . "setting` WHERE 'key'='ship2pay_status'");
	}
}
?>
