<?php
class ControllerModuleCustomerphotos extends Controller {
	private $error = array(); 
	
	public function index() {   
		$this->language->load('module/customerphotos');

		$this->load->model('catalog/product');
		$this->load->model('setting/setting');
		$this->load->model('setting/store');
		$this->load->model('module/customerphotos');
		$this->load->model('localisation/language');
		$this->load->model('design/layout');

		$this->document->setTitle($this->language->get('heading_title'));
				
		$this->document->addScript('view/javascript/customerphotos/bootstrap/js/bootstrap.min.js');
		$this->document->addStyle('view/javascript/customerphotos/bootstrap/css/bootstrap.min.css');
		$this->document->addStyle('view/javascript/customerphotos/font-awesome/css/font-awesome.min.css');
		$this->document->addStyle('view/stylesheet/customerphotos.css');		
		
		if(!isset($this->request->get['store_id'])) {
           $this->request->get['store_id'] = 0;
        }
		$store = $this->getCurrentStore($this->request->get['store_id']);
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			if (!$this->user->hasPermission('modify', 'module/customerphotos')) {
				$this->validate();
				$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
			}
			if (!empty($_POST['OaXRyb1BhY2sgLSBDb21'])) {
				$this->request->post['CustomerPhotos']['LicensedOn']   = $_POST['OaXRyb1BhY2sgLSBDb21'];
			}
			if (!empty($_POST['cHRpbWl6YXRpb24ef4fe'])) {
				$this->request->post['CustomerPhotos']['License']	  = json_decode(base64_decode($_POST['cHRpbWl6YXRpb24ef4fe']),true);
			}
			$store = $this->getCurrentStore($this->request->post['store_id']);
		
			if(!isset($this->request->post['customerphotos_module'])) {
				$this->request->post['customerphotos_module']		  = array();
			}
			if(!isset($this->request->post['customerphotos_customtags'])) {
				$this->request->post['customerphotos_customtags']	  = array();
			}
			$this->model_module_customerphotos->editSetting('customerphotos', $this->request->post, $this->request->post['store_id']);
			$this->model_module_customerphotos->editSetting('customerphotos_module', $this->request->post['customerphotos_module'], $this->request->post['store_id']);
			$this->model_module_customerphotos->editSetting('customerphotos_customtags', $this->request->post['customerphotos_customtags'], $this->request->post['store_id']);
			$this->session->data['success']							= $this->language->get('text_success');
			if (!empty($_GET['activate'])) {
				$this->session->data['success']						= $this->language->get('text_success_activation');
			}
			$selectedTab											   = (empty($this->request->post['selectedTab'])) ? 0 : $this->request->post['selectedTab'];
			
			$this->redirect($this->url->link('module/customerphotos', 'token=' . $this->session->data['token'] . '&tab='.$selectedTab .'&store_id='.$store['store_id'], 'SSL'));
		}

		$languageVariables = array(
			'heading_title',
			'text_enabled',
			'text_disabled',
			'text_content_top',
			'text_content_bottom',
			'text_column_left',
			'text_column_right',
			'entry_code',
			'button_save',
			'button_cancel',
			'button_add_module',
			'button_remove',
			'entry_layouts_active',
			'entry_layout',
			'entry_position',
			'entry_status',
			'entry_sort_order',
			'entry_layout_options',
			'entry_position_options'
        );
       
        foreach ($languageVariables as $languageVariable) {
            $this->data[$languageVariable]   = $this->language->get($languageVariable);
        }
		
 		if (isset($this->error['code'])) {
			$this->data['error_code']		= $this->error['code'];
		} else {
			$this->data['error_code']		= '';
		}
		$this->data['breadcrumbs']		   = array();
   		$this->data['breadcrumbs'][]		 = array(
				'text'      => $this->language->get('text_home'),
				'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
				'separator' => false
   		);
   		$this->data['breadcrumbs'][]		 = array(
				'text'      => $this->language->get('text_module'),
				'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
				'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][]		 = array(
				'text'      => $this->language->get('heading_title'),
				'href'      => $this->url->link('module/customerphotos', 'token=' . $this->session->data['token'], 'SSL'),
				'separator' => ' :: '
   		);
		$this->data['error_warning']		 = '';
		$this->data['currenttemplate']	   = $this->config->get('config_template');
		$this->data['stores']				= array_merge(array(0 => array('store_id' => '0', 'name' => $this->config->get('config_name') . ' (Default)', 'url' => HTTP_SERVER, 'ssl' => HTTPS_SERVER)), $this->model_setting_store->getStores());
		$languages						   = $this->model_localisation_language->getLanguages();;
		$this->data['languages']			 = $languages;
		$firstLanguage					   = array_shift($languages);
		$this->data['firstLanguageCode']	 = $firstLanguage['code'];
		$this->data['store']				 = $store;
		$this->data['action']				= $this->url->link('module/customerphotos', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['cancel']				= $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['layouts']			   = $this->model_design_layout->getLayouts();
		$this->template			       	  = 'module/customerphotos.tpl';
		$this->children					  = array(
				'common/header',
				'common/footer'
		);
		$this->data['data']				  = array();
		$this->data['CustomTags']			= array();
		$this->data['modules']			   = array();
		$this->data['data']				  = $this->model_module_customerphotos->getSetting('customerphotos', $store['store_id']);
		$this->data['CustomTags']			= $this->model_module_customerphotos->getSetting('customerphotos_customtags', $store['store_id']);
		$this->data['modules']			   = $this->model_module_customerphotos->getSetting('customerphotos_module', $store['store_id']);

		$this->response->setOutput($this->render());
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/customerphotos')) {
			$this->error = true;
		}
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
	
	private function getCurrentStore($store_id) {    
        if($store_id && $store_id != 0) {
            $store = $this->model_setting_store->getStore($store_id);
        } else {
            $store['store_id'] = 0;
            $store['name'] = $this->config->get('config_name');
            $store['url'] = $this->getCatalogURL();
        }
        return $store;
    }
	
	private function getCatalogURL(){
        if (isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1'))) {
            $storeURL = HTTPS_CATALOG;
        } else {
            $storeURL = HTTP_CATALOG;
        } 
        return $storeURL;
    }
	
	public function uninstall() {
		$this->load->model('setting/setting');
		$this->load->model('setting/store');
		$this->model_setting_setting->deleteSetting('customerphotos',0);
		$this->model_setting_setting->deleteSetting('customerphotos_customtags',0);
		$this->model_setting_setting->deleteSetting('customerphotos_module',0);
		$stores = $this->model_setting_store->getStores();
		foreach ($stores as $store) {
			$this->model_setting_setting->deleteSetting('customerphotos', $store['store_id']);
			$this->model_setting_setting->deleteSetting('customerphotos_customtags', $store['store_id']);
			$this->model_setting_setting->deleteSetting('customerphotos_module', $store['store_id']);
		}
	}
}
?>