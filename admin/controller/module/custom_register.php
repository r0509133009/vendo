<?php
// ***************************************************
//             Custom Registration Fields   
//       
// Author : Francesco Pisanò - francesco1279@gmail.com
//              
//                   www.leverod.com		
//               © All rights reserved	  
// ***************************************************


// Admin Controller


class ControllerModuleCustomRegister extends Controller {

	private $error = array(); 
	
	
	public function __construct($registry) {
		parent::__construct($registry);

		// Paths and Files
		$this->vqmod_script = array('custom_register', 'cs_shoppica2.2.4_patch');
		
		$this->vqmod_dir = substr_replace(DIR_SYSTEM, '/vqmod/', -8); // -8 = /system/
		$this->vqmod_script_dir = substr_replace(DIR_SYSTEM, '/vqmod/xml/', -8);	
		$this->vqcache_files = substr_replace(DIR_SYSTEM, '/vqmod/vqcache/vq*', -8);
		$this->vqmod_modcache = substr_replace(DIR_SYSTEM, '/vqmod/mods.cache', -8);
		clearstatcache();
	}
	
	public function install() {
	
		// cleanup the system from old files (Custom Registration < 2.3)
		$basepath = substr_replace(DIR_APPLICATION, '/catalog/', -7);	//   /admin/ = 7
		
		$files = array(
			$basepath.'controller/module/custom_register.php',
			$basepath.'language/english/module/custom_register.php',
			$basepath.'language/italian/module/custom_register.php',
			$basepath.'view/theme/default/template/module/custom_register.tpl'
		);
		
		foreach ($files as $file){
		
			if (file_exists($file)) {
				unlink($file);
			} 
		}
		

	
	
		$this->language->load('module/custom_register');

	/*	if (!$this->user->hasPermission('modify', 'module/custom_register')) {
			$this->session->data['error'] = $this->language->get('error_permission');
		} 
		else {
		*/
			// if the extension was previously uninstalled
			
			foreach ( $this->vqmod_script as $vqmod_script){
			
				if (is_file($this->vqmod_script_dir . $vqmod_script . '.xml_')) {
					rename($this->vqmod_script_dir . $vqmod_script . '.xml_', $this->vqmod_script_dir . $vqmod_script . '.xml');

					$this->clear_vqcache();
					$this->session->data['success'] = $this->language->get('success_install');	
				}
				// if there is no need of renaming the file
				elseif (is_file($this->vqmod_script_dir . $vqmod_script . '.xml')) {

					$this->clear_vqcache();
					$this->session->data['success'] = $this->language->get('success_install');	
				}
				else {
					$this->model_setting_extension->uninstall('module', $this->request->get['extension']); // force uninstall the extension if the xml was missing
					$this->session->data['error'] = $this->language->get('error_install');
				}
			}
	//	}
	
		// this code creates empty files that will be filled by vQmod. By creating these empty files
		// we let vQmod to find them in Oc <= 1.5.1.3.1. Otherwise it will not be able to add the 
		// "require_once('file')"
		
		//  Oc <= 1.5.1.3.1 doesn't have the following files, so we create them
		if ( version_compare(VERSION, '1.5.1.3.1', '<=') ) {
			
			$path = substr_replace(DIR_APPLICATION, '/catalog/controller/checkout/', -7);	//   /admin/ = 7
						
			$files = array (
				'payment_address.php',
			//	'payment_method.php',
				'shipping_address.php',
				'shipping_method.php',
				'guest_shipping.php'
			);

			foreach ($files as $file) {

				$this->create_file($path, $file, '<?php require_once(\'' . $path . 'custom_'.$file .'\') ?>');	
			}
			
			
			
			// The file payment_method.php must be copied from the folder crf_repository to /controller/checkout,
			// (on Oc <= 1.5.1.3.1 that file is called payment.php but its content can't be used)
			
			$repository_path = substr_replace(DIR_APPLICATION, '/catalog/controller/crf_repository/', -7);	//   /admin/ = 7
			copy($repository_path.'payment_method.php', $path.'payment_method.php');
		
					
		
			
			$path = substr_replace(DIR_APPLICATION, '/catalog/view/theme/default/template/checkout/', -7);	//   /admin/ = 7
			
			$files = array (
				'payment_address.tpl',
				'payment_method.tpl',
				'shipping_address.tpl',
				'shipping_method.tpl'
			);
			
			
			foreach ($files as $file) {

				$this->create_file($path, $file, '<?php require_once(\'' . $path . 'custom_'.$file .'\') ?>');	
			}

		} // end if version_compare()

	}
	
	private function create_file($path, $filename, $content){

		$file = $path . $filename;
		if (!file_exists($file)) {
			$f = fopen($file, "w");

			file_put_contents($file, $content);

			fclose($f);
		}

	}
	

	
	public function uninstall() {
	
		$this->language->load('module/custom_register');

		if (!$this->user->hasPermission('modify', 'module/custom_register')) {
			$this->session->data['error'] = $this->language->get('error_permission');
		} 
		else {
		
			foreach ( $this->vqmod_script as $vqmod_script){
				if (is_file($this->vqmod_script_dir . $vqmod_script . '.xml')) {
					rename($this->vqmod_script_dir . $vqmod_script . '.xml', $this->vqmod_script_dir . $vqmod_script . '.xml_');

					$this->clear_vqcache();

					$this->session->data['success'] = $this->language->get('success_uninstall');
				} else {
					$this->session->data['error'] = $this->language->get('error_uninstall');
				}
			}
		}
	}
	
	
	public function clear_vqcache() {
	
		$files = glob($this->vqcache_files);
		if ($files) {
			foreach ($files as $file) {
				if (is_file($file)) {
					unlink($file);
				}
			}
		}
		if (is_file($this->vqmod_modcache)) {
			unlink($this->vqmod_modcache);
		}
		return;
	}
	
	
	
	public function reset() {
	
		$this->load->model('setting/setting');

		// Get the current date that will be used to save the last access date
		$now = new DateTime();
		
		$settings = array(
		
			'custom_register_first_boot'						=> null,				

			'custom_register_account_member_enable_firstname'	=> 1,
			'custom_register_account_member_firstname_required'	=> 1,
			'custom_register_account_member_enable_lastname'	=> 1,		
			'custom_register_account_member_lastname_required'	=> 1,
			'custom_register_account_member_enable_telephone'	=> 1,
			'custom_register_account_member_telephone_required'	=> 1,
			'custom_register_account_member_enable_fax'			=> 1,
			'custom_register_account_member_enable_confirm'		=> 1,
			'custom_register_account_member_enable_company'		=> 1,	
			'custom_register_account_member_enable_address_1'	=> 1,
			'custom_register_account_member_address_1_required'	=> 1,			
			'custom_register_account_member_enable_address_2'	=> 1,
			'custom_register_account_member_enable_city'		=> 1,	
			'custom_register_account_member_city_required'		=> 1,
			'custom_register_account_member_enable_postcode'	=> 1,	
			'custom_register_account_member_enable_country'		=> 1,
			'custom_register_account_member_country_required'	=> 1,	
			'custom_register_account_member_enable_zone'		=> 1,
			'custom_register_account_member_zone_required'		=> 1,
			'custom_register_account_member_enable_newsletter'	=> 1,
			
			
			'custom_register_member_enable_firstname'			=> 1,
			'custom_register_member_firstname_required'			=> 1,
			'custom_register_member_enable_lastname'			=> 1,
			'custom_register_member_lastname_required'			=> 1,
			'custom_register_member_enable_telephone'			=> 1,
			'custom_register_member_telephone_required'			=> 1,
			'custom_register_member_enable_fax'					=> 1,
			'custom_register_member_enable_confirm'				=> 1,
			'custom_register_member_enable_company'				=> 1,
			'custom_register_member_enable_address_1'			=> 1,
			'custom_register_member_address_1_required'			=> 1,
			'custom_register_member_enable_address_2'			=> 1,		
			'custom_register_member_enable_city'				=> 1,
			'custom_register_member_city_required'				=> 1,
			'custom_register_member_enable_postcode'			=> 1,
			'custom_register_member_enable_country'				=> 1,
			'custom_register_member_country_required'			=> 1,	
			'custom_register_member_enable_zone'				=> 1,
			'custom_register_member_zone_required'				=> 1,	
			'custom_register_member_enable_newsletter'			=> 1,
			'custom_register_member_enable_shipping_address'	=> 1,
			
			
			'custom_register_guest_enable_firstname'			=> 1,
			'custom_register_guest_firstname_required'			=> 1,			
			'custom_register_guest_enable_lastname'				=> 1,
			'custom_register_guest_lastname_required'			=> 1,
			'custom_register_guest_enable_email'				=> 1,
			'custom_register_guest_email_required'				=> 1,
			'custom_register_guest_enable_telephone'			=> 1,
			'custom_register_guest_telephone_required'			=> 1,
			'custom_register_guest_enable_fax'					=> 1,
			'custom_register_guest_enable_company'				=> 1,
			'custom_register_guest_enable_address_1'			=> 1,
			'custom_register_guest_address_1_required'			=> 1,
			'custom_register_guest_enable_address_2'			=> 1,
			'custom_register_guest_enable_city'					=> 1,
			'custom_register_guest_city_required'				=> 1,
			'custom_register_guest_enable_postcode'				=> 1,
			'custom_register_guest_enable_country'				=> 1,
			'custom_register_guest_country_required'			=> 1,	
			'custom_register_guest_enable_zone'					=> 1,
			'custom_register_guest_zone_required'				=> 1,	
			'custom_register_guest_enable_shipping_address'		=> 1,


			'custom_register_default_country'					=> 13, // Australia
			'custom_register_enable_antispam'					=> 1,
						
			'crf_last_cp_save_date'								=> $now->format('Y-m-d H:i:s')
		);
		
		
		$this->model_setting_setting->editSetting('custom_register', $settings);
		
		foreach ($settings as $setting => $value){
			$this->config->set($setting, $value);		
		}
		


		if (CRF_DEMO) {
			
			// CUSTOMER GROUPS, TAX ID AND COMPANY ID HAVE BEEN ADDED WITH Oc 1.5.3
			if ( version_compare(VERSION, '1.5.3', '>=') ) {
			
				// language settings for the translations
				$this->load->model('localisation/language');
				$languages = $this->model_localisation_language->getLanguages();

					
				$this->load->model('sale/customer_group');
				$customer_groups = $this->model_sale_customer_group->getCustomerGroups();
				
				
				// I didn't use the function editsettingValue to save the keys config_customer_group_id and 
				// config_customer_group_display because it is buggy on Oc > 1.5.5.1. Instead, I get all the settings
				// from the group "config" and change the two keys:
				$config_settings = $this->model_setting_setting->getSetting('config');
				
				$config_customer_group_display = array();
				
				foreach ($customer_groups as $id => $customer_group) {
				
					// set the first customer group to be the default one:
					if ($id == 0){
						$config_settings['config_customer_group_id'] = $customer_group['customer_group_id'];
					}	
						
					foreach ($languages as $language) {
					
						if ( $customer_group['customer_group_id'] == $this->config->get('config_customer_group_id') ) {
						
							$name	= 'Default Group';
							$descr	= 'This is a sample description for the Default Group';
						}
						else {
							$name	= 'Group '.$id;
							$descr	= 'This is a sample description for the Group '.$id;
						}
						
						$data['customer_group_description'][$language['language_id']]['name'] 			= $name;
						$data['customer_group_description'][$language['language_id']]['description']	= $descr;
					}

					
					$data['company_id_display']			= 1;
					$data['company_id_required']		= 0;
					
					$data['tax_id_display']				= 1;
					$data['tax_id_required']			= 0;
					
					$data['approval'] 					= 0;
					$data['sort_order']					= $id;

					$this->model_sale_customer_group->editCustomerGroup($customer_group['customer_group_id'], $data);
									
					// Set which customer groups to display				
					$config_customer_group_display[] = $customer_group['customer_group_id'];	
				}
				

				// Set which customer groups to display
				$config_settings['config_customer_group_display'] = $config_customer_group_display;	
	
				// Enable guest checkout
				$config_settings['config_guest_checkout'] = 1;
				
				// Dropdown "Account Terms"
				$config_settings['config_account_id'] = 3;
				
				// Dropdown "Checkout Terms"
				$config_settings['config_checkout_id'] = 5;
				
				
				$this->model_setting_setting->editSetting('config', $config_settings);

				foreach ($config_settings as $setting => $value){
					$this->config->set($setting, $value);		
				}
				
			}
		}

	}
	
	
	
	
	
	public function index() {   
	
	
		$this->language->load('module/custom_register');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
		
		
		
		// ****************************************************************************************
		// This block sets the last control panel save date
		// (the date is contained in crf_last_cp_save_date)
		
			// If this script is called by a page save
			if ( $this->request->server['REQUEST_METHOD'] == 'POST' ) {
			
				$last_save_date_string = $this->request->post['crf_last_cp_save_date'];
			}
			// if this script is called by the "edit module" button
			else {
				$last_save_date_string = $this->config->get('crf_last_cp_save_date'); 
			}
			

			$last_cp_save_date 				 = new DateTime($last_save_date_string);
			$last_cp_save_date_plus_interval = new DateTime($last_save_date_string);
			
			if ( CRF_DEMO && version_compare(phpversion(), '5.2', '>=') ) { 
				$last_cp_save_date_plus_interval->modify('+'. CRF_DEMO_RESET_TIME.' minutes'); // for php >= 5.2
			//	$last_cp_save_date_plus_interval->add(new DateInterval('PT'.CRF_DEMO_RESET_TIME.'M')); // for php >= 5.3
			}
			
			$current_cp_access_date = new DateTime();

			// ****************************************************************************************
			// This block of code is for the Demo, it resets settings after 45 mins (see constant CRF_DEMO_RESET_TIME)
			// from the last save. It only works for php 5.3 and later
				if ( CRF_DEMO && version_compare(phpversion(), '5.3', '>=') ) { 
				
				// DateTime::diff() and the operators < and > for the dates are available from php 5.3
					if ($current_cp_access_date > $last_cp_save_date_plus_interval) {
						$this->reset();
					}
				}
			// end code for the Demo
			// ****************************************************************************************

			
			// pass the current date to a hidden input field
			$this->data['current_date'] = $current_cp_access_date->format('Y-m-d H:i:s');	
						
		// end code for the Demo
		// ****************************************************************************************
			

		
		

		
		$validate = $this->validate();
		
		// Error messages
		
		if (isset($this->error['customer_group_display'])) {
			$this->data['error_customer_group_display'] = $this->error['customer_group_display'];
		} else {
			$this->data['error_customer_group_display'] = '';
		}
		
		
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		
		if (isset($this->error['customer_groups'])) {
			$this->data['error_customer_groups'] = $this->error['customer_groups'];
		} else {
			$this->data['error_customer_groups'] = array();
		}
		
		// End error messages
		


		$is_ajax = isset($this->request->post['is_ajax_request']) && $this->request->post['is_ajax_request'];

		if ($this->request->server['REQUEST_METHOD'] == 'POST'){
		
			$json = array();
					
			if ( $validate ) {
			
			
				// CUSTOMER GROUPS, TAX ID AND COMPANY ID HAVE BEEN ADDED WITH Oc 1.5.3
				if ( version_compare(VERSION, '1.5.3', '>=') ) {
						
					$this->load->model('sale/customer_group');
					
					foreach ($this->request->post['customer_groups'] as $id => $customer_group) {

						$data['customer_group_description'] = $customer_group['customer_group_description'];
						$data['company_id_display']			= $customer_group['company_id_display'];
						$data['company_id_required']		= $customer_group['company_id_required'];
						$data['tax_id_display']				= $customer_group['tax_id_display'];
						$data['tax_id_required']			= $customer_group['tax_id_required'];
						$data['approval'] 					= $customer_group['approval'];
						$data['sort_order']					= $customer_group['sort_order'];

						$this->model_sale_customer_group->editCustomerGroup($id, $data);
					}
				}
			

				$this->model_setting_setting->editSetting('custom_register', $this->request->post);	


				// I didn't use the function editsettingValue to save the keys config_customer_group_id and 
				// config_customer_group_display because it is buggy on Oc > 1.5.5.1. Instead, I get all the settings
				// from the group "config" and change the two keys:
				
				$config_settings = $this->model_setting_setting->getSetting('config');
				
				if (isset($this->request->post['config_customer_group_id'])){
					$config_settings['config_customer_group_id'] = $this->request->post['config_customer_group_id'];
				}
				
				if (isset($this->request->post['config_customer_group_display'])){
					$config_settings['config_customer_group_display'] = $this->request->post['config_customer_group_display'];
				}
				else {
					unset($config_settings['config_customer_group_display']);
				}

				if (isset($this->request->post['config_guest_checkout'])){
					$config_settings['config_guest_checkout'] = $this->request->post['config_guest_checkout'];
				}

				// Dropdown "Account Terms"
				if (isset($this->request->post['config_account_id'])){
					$config_settings['config_account_id'] = $this->request->post['config_account_id'];
				}
				
				// Dropdown "Checkout Terms"
				if (isset($this->request->post['config_checkout_id'])){
					$config_settings['config_checkout_id'] = $this->request->post['config_checkout_id'];
				}		
				

				$this->model_setting_setting->editSetting('config', $config_settings);

				
				// the module can be saved by an ajax request (for example with the button "Save and Continue") 
				// or by the default Save button:
				if ($is_ajax) {

					$json['saved']='true';
				}
				else {
				
					$this->session->data['success'] = $this->language->get('text_success');
					$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
				}
		
			}
			else {
			
				if ($is_ajax) {
					
					$json['error'] = $this->error;
				}
			}
			

			if ($is_ajax) {
				$this->response->setOutput(json_encode($json));
				return;	
			}
		}
				
		
				
		$text_strings = array(
				
				'heading_title',
				'heading_title_cp',
				
				'text_settings',
				'text_antispam',
				'text_user_guide',
				'text_license',
				
				'text_enabled',
				'text_disabled',
				
				'text_yes',
				'text_no',
				
				'text_entry',
				'text_show',
				'text_enable',
				'text_member',
				'text_guest',
				'text_customer_grp_mgr',
				'text_help',
				'text_select',
				'text_select_all',
				'text_deselect_all',
				'text_none',
				'text_display',
				'text_required',
				'text_checked_by_default',
				'text_approval',
				'text_name',
				'text_description',
				'text_default',
				'text_reset_default',
				'text_sort_order',
				'text_account_registration',
				'text_checkout_page',
				'text_help_member',	
				'text_help_member_firstname',
				'text_help_member_lastname',
				'text_help_member_email',
				'text_help_member_telephone',
				'text_help_member_fax',
				'text_help_member_password',
				'text_help_member_confirm',
				'text_help_member_company',
				'text_help_member_address_1',
				'text_help_member_address_2',
				'text_help_member_city',
				'text_help_member_country',
				'text_help_member_default_country',
				'text_help_member_default_country',
				'text_help_member_postcode',
				'text_help_member_zone',
				'text_help_member_newsletter',
				'text_help_member_shipping_address',	
				'text_help_member_agree',	
				'text_help_guest_checkout',
				'text_help_guest_firstname',
				'text_help_guest_lastname',
				'text_help_guest_email',
				'text_help_guest_telephone',
				'text_help_guest_fax',
				'text_help_guest_password',
				'text_help_guest_confirm',
				'text_help_guest_company',
				'text_help_guest_address_1',
				'text_help_guest_address_2',
				'text_help_guest_city',
				'text_help_guest_country',
				'text_help_guest_default_country',
				'text_help_guest_postcode',
				'text_help_guest_zone',
				'text_help_guest_newsletter',
				'text_help_guest_shipping_address',
				'text_help_guest_agree',
				'text_help_hidden_antispam',
				'text_saving',
				'text_success',
				'entry_status',
				'button_save',
				'button_save_continue',
				'button_cancel',
				'entry_firstname',   
				'entry_lastname',
				'entry_email',
				'entry_telephone',
				'entry_fax',
				'entry_password',
				'entry_confirm',
				'entry_company',
				'entry_address_1',
				'entry_address_2',			
				'entry_city',
				'entry_country',
				'entry_default_country',
				'entry_postcode',
				'entry_zone',
				'entry_newsletter',
				'entry_shipping_address',
				'entry_agree',
				'entry_account',
				'entry_checkout',
				'entry_customer_group',
				'entry_default_customer_group',
				'entry_company_id',
				'entry_tax_id',

				'entry_customer_group_display',
				'entry_customer_group_name'				
		);
		
		foreach ($text_strings as $text) {
			$this->data[$text] = $this->language->get($text);
		}
		
						
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title_cp'),
			'href'      => $this->url->link('module/custom_register', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('module/custom_register', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		
		
		// Select the right protocol for the urls that will be prepended to the autocomplete url.  If the protocol or the website name
		// doesn't match exactly the url of the current page, browsers will not allow the ajax request. For further details see:
		// XMLHttpRequest: Error 0x80070005  Access denied - (visible with IE Development Tools)
		$is_https = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443;

		if ($is_https) {
			$this->data['catalog_url'] = HTTPS_CATALOG;
		}
		else {
			$this->data['catalog_url'] = HTTP_CATALOG;
		}
		
		
		$this->load->model('localisation/country');	
    	$this->data['countries'] = $this->model_localisation_country->getCountries();
		
		$this->load->model('localisation/language');
		$this->data['languages'] = $this->model_localisation_language->getLanguages();
		
		$this->load->model('catalog/information');
		$this->data['informations'] = $this->model_catalog_information->getInformations();


		
		$reg_entries = array(		
			'firstname',
			'lastname',
			'email',
			'telephone',
			'fax',
			'password',
			'confirm',
			'company',
			'address_1',
			'address_2',
			'city',
			'country',
			'postcode',
			'zone',
			'newsletter',
			'shipping_address',
			'agree'
		);
		
		$reg_required_entries = array(		
			'firstname',
			'lastname',
			'email',
			'telephone',
			'password',
			'confirm',
			'address_1',
			'city',
			'country',
			'postcode',
			'zone',
			'agree'
		);

		
		
		// This "if" runs only when the mdule loads for the first time.
		// The flag "first_boot" tells us whether is the first time the module has been launched
		// at the first boot the var $this->config->get('iwsmile_first_boot') is empty
		if ( $this->config->get('custom_register_first_boot') == '' ) {
		
			$this->reset();
			
			// After the first boot, the template sets the flag "custom_register_first_boot" to 0 
		}
			
		
		if (isset($this->request->post['custom_register_first_boot'])) {
			$this->data['custom_register_first_boot'] = $this->request->post['custom_register_first_boot'];
		} else {
			$this->data['custom_register_first_boot'] = $this->config->get('custom_register_first_boot');
		}
		

	
		//  Members:
		foreach ($reg_entries as $reg_entry) {
		
		
		// for the account registration page:
			if (isset($this->request->post['custom_register_account_member_enable_'.$reg_entry])) {
				$this->data['custom_register_account_member_enable_'.$reg_entry] = $this->request->post['custom_register_account_member_enable_'.$reg_entry];
			} else {
				$this->data['custom_register_account_member_enable_'.$reg_entry] = $this->config->get('custom_register_account_member_enable_'.$reg_entry);
			}
			
			if ( $reg_entry != 'newsletter' && $reg_entry != 'shipping_address' ) { // "Newsletter" and "Shipping address" checkboxes don't need the asterisk "required"
			
				if (isset($this->request->post['custom_register_account_member_'.$reg_entry.'_required'])) {
					$this->data['custom_register_account_member_'.$reg_entry.'_required'] = $this->request->post['custom_register_account_member_'.$reg_entry.'_required'];
				} else {
					$this->data['custom_register_account_member_'.$reg_entry.'_required'] = $this->config->get('custom_register_account_member_'.$reg_entry.'_required');
				}
			}
		
		
		// for the checkout	page:
			if (isset($this->request->post['custom_register_member_enable_'.$reg_entry])) {
				$this->data['custom_register_member_enable_'.$reg_entry] = $this->request->post['custom_register_member_enable_'.$reg_entry];
			} else {
				$this->data['custom_register_member_enable_'.$reg_entry] = $this->config->get('custom_register_member_enable_'.$reg_entry);
			}
			
			if ( $reg_entry != 'newsletter' && $reg_entry != 'shipping_address' ) { // "Newsletter" and "Shipping address" checkboxes don't need the asterisk "required"
			
				if (isset($this->request->post['custom_register_member_'.$reg_entry.'_required'])) {
					$this->data['custom_register_member_'.$reg_entry.'_required'] = $this->request->post['custom_register_member_'.$reg_entry.'_required'];
				} else {
					$this->data['custom_register_member_'.$reg_entry.'_required'] = $this->config->get('custom_register_member_'.$reg_entry.'_required');
				}
			}
		}
		
		// Guest users	
		foreach ($reg_entries as $reg_entry) {
		
			if ( $reg_entry != 'password' && $reg_entry != 'confirm' && $reg_entry != 'newsletter' ) { // password and newsletter are disabled for guests 
				if (isset($this->request->post['custom_register_guest_enable_'.$reg_entry])) {
					$this->data['custom_register_guest_enable_'.$reg_entry] = $this->request->post['custom_register_guest_enable_'.$reg_entry];
				} else {
					$this->data['custom_register_guest_enable_'.$reg_entry] = $this->config->get('custom_register_guest_enable_'.$reg_entry);
				}
			}
				
			if ( $reg_entry != 'newsletter' && $reg_entry != 'shipping_address' && $reg_entry != 'password' && $reg_entry != 'confirm' ) { // "Newsletter" and "Shipping address" checkboxes don't need the asterisk "required", password is disabled for guests
			
				if (isset($this->request->post['custom_register_guest_'.$reg_entry.'_required'])) {
					$this->data['custom_register_guest_'.$reg_entry.'_required'] = $this->request->post['custom_register_guest_'.$reg_entry.'_required'];
				} else {
					$this->data['custom_register_guest_'.$reg_entry.'_required'] = $this->config->get('custom_register_guest_'.$reg_entry.'_required');
				}
			}
		}
		
		
		// Default country
		$default_country = $this->config->get('custom_register_default_country');
		
		if (isset($this->request->post['custom_register_default_country'])) {
		
			$this->data['custom_register_default_country'] = $this->request->post['custom_register_default_country'];
			
		} elseif (!empty($default_country)) {
		
			$this->data['custom_register_default_country'] = $default_country;
		}
		else {
			$this->data['custom_register_default_country'] = $this->config->get('config_country_id');
		}
		
		
		
		
		// Antispam
		if (isset($this->request->post['custom_register_enable_antispam'])) {
			$this->data['custom_register_enable_antispam'] = $this->request->post['custom_register_enable_antispam'];
		} else {
			$this->data['custom_register_enable_antispam'] = $this->config->get('custom_register_enable_antispam');
		}
		
		
		
		// Default value for the  checkbox "Newsletter"
		if (isset($this->request->post['custom_register_newsletter_checked'])) {
			$this->data['custom_register_newsletter_checked'] = $this->request->post['custom_register_newsletter_checked'];
		} else {
			$this->data['custom_register_newsletter_checked'] = $this->config->get('custom_register_newsletter_checked');
		}
		
		
		
		// Default value for the  checkbox "Delivery = Shipping"
		if (isset($this->request->post['custom_register_shipping_address_checked'])) {
			$this->data['custom_register_shipping_address_checked'] = $this->request->post['custom_register_shipping_address_checked'];
		} else {
			$this->data['custom_register_shipping_address_checked'] = $this->config->get('custom_register_shipping_address_checked');
		}
		

		
		// Dropdown "Account Terms"
		if (isset($this->request->post['config_account_id'])) {
			$this->data['config_account_id'] = $this->request->post['config_account_id'];
		} else {
			$this->data['config_account_id'] = $this->config->get('config_account_id');			
		}
		
		// Default value for the account checkbox "Agree with"
		if (isset($this->request->post['custom_register_account_agree_checked'])) {
			$this->data['custom_register_account_agree_checked'] = $this->request->post['custom_register_account_agree_checked'];
		} else {
			$this->data['custom_register_account_agree_checked'] = $this->config->get('custom_register_account_agree_checked');
		}
		

		
		// Dropdown "Checkout Terms"
		if (isset($this->request->post['config_checkout_id'])) {
			$this->data['config_checkout_id'] = $this->request->post['config_checkout_id'];
		} else {
			$this->data['config_checkout_id'] = $this->config->get('config_checkout_id');		
		}
		
		// Default value for the checkout checkbox "Agree with"
		if (isset($this->request->post['custom_register_checkout_agree_checked'])) {
			$this->data['custom_register_checkout_agree_checked'] = $this->request->post['custom_register_checkout_agree_checked'];
		} else {
			$this->data['custom_register_checkout_agree_checked'] = $this->config->get('custom_register_checkout_agree_checked');
		}
		
		
		
		
		
		

		// Customer group to display on the registration forms
		if (isset($this->request->post['config_customer_group_id'])) {
			$this->data['config_customer_group_id'] = $this->request->post['config_customer_group_id'];
		} else {
			$this->data['config_customer_group_id'] = $this->config->get('config_customer_group_id');			
		}

		
	
		// CUSTOMER GROUPS, TAX ID AND COMPANY ID HAVE BEEN ADDED WITH Oc 1.5.3
		if ( version_compare(VERSION, '1.5.3', '>=') ) {

		
			// List of customer groups to display
			if (isset($this->request->post['config_customer_group_display'])) {
				$this->data['config_customer_group_display'] = $this->request->post['config_customer_group_display'];
			} elseif ($this->config->get('config_customer_group_display')) {
				$this->data['config_customer_group_display'] = $this->config->get('config_customer_group_display');	
			} else {
				$this->data['config_customer_group_display'] = array();			
			}
			
			
			
			//  Customer group info (name, description, tax id, company id, approval, sort order)
			if (isset($this->request->post['customer_groups'])) {
			
				$this->data['customer_groups'] = $this->request->post['customer_groups'];
			}
			else {
				
				$this->load->model('sale/customer_group');

				$customer_groups = $this->model_sale_customer_group->getCustomerGroups();
				
				foreach ($customer_groups as $customer_group) {
			
					$customer_group_info = $this->model_sale_customer_group->getCustomerGroup($customer_group['customer_group_id']);

					$this->data['customer_groups'][$customer_group['customer_group_id']] = array(
					
					//	'customer_group_id'				=> $customer_group['customer_group_id'],
					//	'default'						=> ($customer_group['customer_group_id'] == $this->config->get('config_customer_group_id')) ? $this->language->get('text_default') : null,
						'customer_group_description'	=> $this->model_sale_customer_group->getCustomerGroupDescriptions($customer_group['customer_group_id']),
						'company_id_display'			=> $customer_group_info['company_id_display'],
						'company_id_required'			=> $customer_group_info['company_id_required'],
						'tax_id_display'				=> $customer_group_info['tax_id_display'],
						'tax_id_required'				=> $customer_group_info['tax_id_required'],
						'approval'						=> $customer_group_info['approval'],
						'sort_order'					=> $customer_group_info['sort_order']
					);	
				}
			}
		}


		
		// Guest checkout
		if (isset($this->request->post['config_guest_checkout'])) {
			$this->data['config_guest_checkout'] = $this->request->post['config_guest_checkout'];
		} else {
			$this->data['config_guest_checkout'] = $this->config->get('config_guest_checkout');		
		}
		
		
		
	
		//This code handles the situation where you have multiple instances of this module, for different layouts.
		$this->data['modules'] = array();
		if (isset($this->request->post['custom_register_module'])) {
			$this->data['modules'] = $this->request->post['custom_register_module'];
		} elseif ($this->config->get('custom_register_module')) { 
			$this->data['modules'] = $this->config->get('custom_register_module');
		}	
		
		
		$this->load->model('design/layout');
		
		$this->data['layouts'] = $this->model_design_layout->getLayouts();
						
		$this->template = 'module/custom_register.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
	
		$this->response->setOutput($this->render());
	}
	
		
	
	private function validate() {
	
		
		if (!$this->user->hasPermission('modify', 'module/custom_register')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
	

		if (!empty($this->request->post['config_customer_group_display']) && !in_array($this->request->post['config_customer_group_id'], $this->request->post['config_customer_group_display'])) {
			$this->error['customer_group_display'] = $this->language->get('error_customer_group_display');
		}

		
		$customer_groups = !empty($this->request->post['customer_groups'])? $this->request->post['customer_groups'] : array();
		
		foreach ($customer_groups as $id => $customer_group) {
		
			foreach ($customer_group['customer_group_description'] as $language_id => $value) {
				if ((utf8_strlen($value['name']) < 3) || (utf8_strlen($value['name']) > 32)) {
					$this->error['customer_groups'][$id]['customer_group_description'][$language_id]['name'] = $this->language->get('error_customer_group_name');
				}
			}
		}

		
	
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
	

}
?>