<?php class ControllerModuleColorboxpopup extends Controller {
        private $error = array();

        public function index() {
                $this->language->load('module/colorboxpopup');
                $this->document->setTitle($this->language->get('heading_title'));

				$this->load->model('setting/setting');
				
				$this->load->model('module/coupon');
				
				$chk_db = $this->model_module_coupon->check_db();
				
				if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
					$this->model_setting_setting->editSetting('colorboxpopup', $this->request->post);
					
                    $this->session->data['success'] = $this->language->get('text_success');

                    $this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
                }
				
				$this->data = array();

                $this->data['heading_title'] = $this->language->get('heading_title');
				$this->data['entry_status'] = $this->language->get('entry_status');
				$this->data['entry_coupon'] = $this->language->get('entry_coupon');
				$this->data['entry_dicount_title_txt'] = $this->language->get('entry_dicount_title_txt');
				$this->data['entry_dicount_title_txt_second'] = $this->language->get('entry_dicount_title_txt_second');
				$this->data['entry_dicount_desc'] = $this->language->get('entry_dicount_desc');
				$this->data['entry_dicount_desc_second'] = $this->language->get('entry_dicount_desc_second');
				$this->data['text_enabled'] = $this->language->get('text_enabled');
				$this->data['text_disabled'] = $this->language->get('text_disabled');
				$this->data['text_tooltipmsg'] = $this->language->get('text_tooltipmsg');
				$this->data['text_edit'] = 'Edit';
				$this->data['button_save'] = $this->language->get('button_save');
                $this->data['button_cancel'] = $this->language->get('button_cancel');
                $this->data['button_add_module'] = $this->language->get('button_add_module');
                $this->data['button_remove'] = $this->language->get('button_remove');
                $this->data['text_no_results'] = $this->language->get('text_no_results');
                
				$this->data['colorboxpopup'] = $this->config->get('colorboxpopup');
				$this->data['colorboxpopup_status'] = $this->config->get('colorboxpopup_status');
				$this->data['colorboxpopup_dicount_title'] = $this->config->get('colorboxpopup_dicount_title');
				$this->data['colorboxpopup_dicount_title_second'] = $this->config->get('colorboxpopup_dicount_title_second');
				$this->data['colorboxpopup_dicount_desc'] = $this->config->get('colorboxpopup_dicount_desc');
				$this->data['colorboxpopup_dicount_desc_second'] = $this->config->get('colorboxpopup_dicount_desc_second');
				
				if (isset($this->request->get['page'])) {
					$page = $this->request->get['page'];
				} else {
					$page = 1;
				}
				
				$url = '';

				if (isset($this->request->get['page'])) {
					$url .= '&page=' . $this->request->get['page'];
				}
				
				$this->data['popupdata'] = array();
				
				$this->load->model('module/coupon');
				
				$popupdata_total = $this->model_module_coupon->getTotalPopupData();
				
				$filter_popupdata = array(
					'start' => ($page - 1) * $this->config->get('config_admin_limit'),
					'limit' => $this->config->get('config_admin_limit')
				);
				
				$popupdata_results = $this->model_module_coupon->getPopupData($filter_popupdata);

				foreach ($popupdata_results as $result) {
					$this->data['popupdata'][] = array(
						'id' 		 		=> $result['id'],
						'name'        		=> $result['name'],
						'email_id' 		  	=> $result['email_id'],
						'date_added'        => $result['date_added']
					);
				}
				
				$this->data['sort_name'] = $this->url->link('module/colorboxpopup', 'token=' . $this->session->data['token'] . $url);
				
				$pagination = new Pagination();
				$pagination->total = $popupdata_total;
				$pagination->page = $page;
				$pagination->limit = $this->config->get('config_admin_limit');
				$pagination->text = $this->language->get('text_pagination');
				$pagination->url = $this->url->link('module/colorboxpopup', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

				$this->data['pagination'] = $pagination->render();

				
				$this->data['breadcrumbs'] = array();
                $this->data['breadcrumbs'][] = array(
						'text' => $this->language->get('text_home'),
                        'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
						'separator' => false
                );
                $this->data['breadcrumbs'][] = array(
						'text' => $this->language->get('text_module'),
                        'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
						'separator' => ' :: '
                );
				
				$this->load->model('module/coupon');

				$this->data['coupons'] = $this->model_module_coupon->getCoupons();
				
				
 				
                $this->data['action'] = $this->url->link('module/colorboxpopup', 'token=' . $this->session->data['token'], 'SSL');
				
                $this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
				
				if (isset($this->request->post['colorboxpopup'])) {
                        $this->data['colorboxpopup'] = $this->request->post['colorboxpopup'];
                }
              
				$this->load->model('design/layout');

				$this->data['layouts'] = $this->model_design_layout->getLayouts();

				$this->template = 'module/colorboxpopup.tpl';
				$this->children = array(
					'common/header',
					'common/footer'
				);

				$this->response->setOutput($this->render());			
			}
        }
?>
