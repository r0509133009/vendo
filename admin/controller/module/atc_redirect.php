<?php
class ControllerModuleatcRedirect extends Controller {
	private $error = array();

   private $button_background_hover ='#777777';
	
	
	public function index() {   
	
		$this->load->language('module/atc_redirect');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
		
		$this->document->addStyle('view/stylesheet/atc_redirect.css');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('atc_redirect', $this->request->post);		
					
			$this->session->data['success'] = $this->language->get('text_success');
						
			$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}
				
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_settings_tab'] = $this->language->get('text_settings_tab');
		$this->data['text_about_tab'] = $this->language->get('text_about_tab');
	
		$this->data['text_cart_page'] = $this->language->get('text_cart_page');
		$this->data['text_checkout_page'] = $this->language->get('text_checkout_page');
		$this->data['text_additional_button'] = $this->language->get('text_additional_button');
		$this->data['text_replace_current_button'] = $this->language->get('text_replace_current_button');	
		
	
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_redirect_to'] = $this->language->get('entry_redirect_to');
		$this->data['entry_appearance'] = $this->language->get('entry_appearance');
		$this->data['entry_button_text'] = $this->language->get('entry_button_text');
		$this->data['entry_button_text_color'] = $this->language->get('entry_button_text_color');
		$this->data['entry_button_text_color_hover'] = $this->language->get('entry_button_text_color_hover');
		$this->data['entry_button_background'] = $this->language->get('entry_button_background');
		$this->data['entry_button_background_hover'] = $this->language->get('entry_button_background_hover');
										
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/atc_redirect', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('module/atc_redirect', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		
		if (isset($this->request->post['atc_redirect_status'])) {
			$this->data['atc_redirect_status'] = $this->request->post['atc_redirect_status'];
		} else {
			$this->data['atc_redirect_status'] = $this->config->get('atc_redirect_status');
		}
		
		
		if (isset($this->request->post['atc_redirect_to'])) {
			$this->data['atc_redirect_to'] = $this->request->post['atc_redirect_to'];
		} else {
			$this->data['atc_redirect_to'] = $this->config->get('atc_redirect_to');
		}
		
		
		if (isset($this->request->post['button_appearance'])) {
			$this->data['button_appearance'] = $this->request->post['button_appearance'];
		} else {
			$this->data['button_appearance'] = $this->config->get('button_appearance');
		}
		if (isset($this->request->post['button_text'])) {
			$this->data['button_text'] = $this->request->post['button_text'];
		} else {
			$this->data['button_text'] = $this->config->get('button_text');
		}
		
		if (isset($this->request->post['button_text_color'])) {
			$this->data['button_text_color'] = $this->request->post['button_text_color'];
		} else {
			$this->data['button_text_color'] = $this->config->get('button_text_color');
		}
		
		if (isset($this->request->post['button_text_color_hover'])) {
			$this->data['button_text_color_hover'] = $this->request->post['button_text_color_hover'];
		} else {
			$this->data['button_text_color_hover'] = $this->config->get('button_text_color_hover');
		}
		
		if (isset($this->request->post['button_background'])) {
			$this->data['button_background'] = $this->request->post['button_background'];
		} else {
			$this->data['button_background'] = $this->config->get('button_background');
		}
		
		if (isset($this->request->post['button_background_hover'])) {
			$this->data['button_background_hover'] = $this->request->post['button_background_hover'];
		} else {
			$this->data['button_background_hover'] = $this->config->get('button_background_hover');		
		}
		
		if (isset($this->request->post['atc_product_page_status'])) {
			$this->data['atc_product_page_status'] = $this->request->post['atc_product_page_status'];
		} else {
			$this->data['atc_product_page_status'] = $this->config->get('atc_product_page_status');		
		}
		
		if (isset($this->request->post['atc_category_page_status'])) {
			$this->data['atc_category_page_status'] = $this->request->post['atc_category_page_status'];
		} else {
			$this->data['atc_category_page_status'] = $this->config->get('atc_category_page_status');		
		}
		
		if (isset($this->request->post['atc_manufacturer_page_status'])) {
			$this->data['atc_manufacturer_page_status'] = $this->request->post['atc_manufacturer_page_status'];
		} else {
			$this->data['atc_manufacturer_page_status'] = $this->config->get('atc_manufacturer_page_status');		
		}
		
		if (isset($this->request->post['atc_search_page_status'])) {
			$this->data['atc_search_page_status'] = $this->request->post['atc_search_page_status'];
		} else {
			$this->data['atc_search_page_status'] = $this->config->get('atc_search_page_status');		
		}
		
		if (isset($this->request->post['atc_special_page_status'])) {
			$this->data['atc_special_page_status'] = $this->request->post['atc_special_page_status'];
		} else {
			$this->data['atc_special_page_status'] = $this->config->get('atc_special_page_status');		
		}
		
		if (isset($this->request->post['atc_bestseller_module_status'])) {
			$this->data['atc_bestseller_module_status'] = $this->request->post['atc_bestseller_module_status'];
		} else {
			$this->data['atc_bestseller_module_status'] = $this->config->get('atc_bestseller_module_status');		
		}
		
		if (isset($this->request->post['atc_featured_module_status'])) {
			$this->data['atc_featured_module_status'] = $this->request->post['atc_featured_module_status'];
		} else {
			$this->data['atc_featured_module_status'] = $this->config->get('atc_featured_module_status');		
		}
		
		if (isset($this->request->post['atc_latest_module_status'])) {
			$this->data['atc_latest_module_status'] = $this->request->post['atc_latest_module_status'];
		} else {
			$this->data['atc_latest_module_status'] = $this->config->get('atc_latest_module_status');		
		}
		
		if (isset($this->request->post['atc_special_module_status'])) {
			$this->data['atc_special_module_status'] = $this->request->post['atc_special_module_status'];
		} else {
			$this->data['atc_special_module_status'] = $this->config->get('atc_special_module_status');		
		}
		

		$this->template = 'module/atc_redirect.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/atc_redirect')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
	
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>