<?php
/**
 * @total-module	Hide Admin Products and Category fields
 * @author-name 	◘ Dotbox Creative
 * @copyright		Copyright (C) 2014 ◘ Dotbox Creative www.dotboxcreative.com
 * @license			GNU/GPL, see http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
 */
class ControllerModuleHampaCat extends Controller {
	private $error = array(); 
		
	public function index() {  
		 
		$this->load->language('module/hampa_cat');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
                
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validate())) {
			
			$this->model_setting_setting->editSetting('hampa_cat', $this->request->post);	
						
			
			$this->session->data['success'] = $this->language->get('text_success');				
			$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}
				
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
	    		
		$this->data['entry_status'] = $this->language->get('entry_status');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
             
		$this->data['tab_product'] = $this->language->get('tab_product');
		$this->data['tab_category'] = $this->language->get('tab_category');
		
		$this->data['hidden'] = $this->language->get('hidden');
		$this->data['visible'] = $this->language->get('visible');
		
		// product tabs
		$this->data['tab_general'] = $this->language->get('tab_general');
		$this->data['tab_meta_tag_des'] = $this->language->get('tab_meta_tag_des');	
		$this->data['tab_meta_tag_key'] = $this->language->get('tab_meta_tag_key');
		$this->data['tab_product_tag'] = $this->language->get('tab_product_tag');	
		$this->data['tab_data'] = $this->language->get('tab_data');	
		$this->data['tab_sku'] = $this->language->get('tab_sku');	
		$this->data['tab_upc'] = $this->language->get('tab_upc');	
		$this->data['tab_ean'] = $this->language->get('tab_ean');	
		$this->data['tab_jan'] = $this->language->get('tab_jan');	
		$this->data['tab_isbn'] = $this->language->get('tab_isbn');		
		$this->data['tab_mnp'] = $this->language->get('tab_mnp');		
		$this->data['tab_location'] = $this->language->get('tab_location');	
		$this->data['tab_price'] = $this->language->get('tab_price');	
		$this->data['tab_price_info'] = $this->language->get('tab_price_info');
		$this->data['tab_tax'] = $this->language->get('tab_tax');	
		$this->data['tab_quantity'] = $this->language->get('tab_quantity');	
		$this->data['tab_quantity_info'] = $this->language->get('tab_quantity_info');	
		$this->data['tab_min_quantity'] = $this->language->get('tab_min_quantity');	
		$this->data['tab_sub_stock'] = $this->language->get('tab_sub_stock');	
		$this->data['tab_ooss'] = $this->language->get('tab_ooss');	
		$this->data['tab_req_shipping'] = $this->language->get('tab_req_shipping');	
		$this->data['tab_seo'] = $this->language->get('tab_seo');	
		$this->data['tab_date_ava'] = $this->language->get('tab_date_ava');	
		$this->data['tab_dimensions'] = $this->language->get('tab_dimensions');	
		$this->data['tab_length'] = $this->language->get('tab_length');	
		$this->data['tab_weight'] = $this->language->get('tab_weight');	
		$this->data['tab_weight_class'] = $this->language->get('tab_weight_class');
		$this->data['tab_sort'] = $this->language->get('tab_sort');
		$this->data['tab_link'] = $this->language->get('tab_link');
		$this->data['tab_manufacturer'] = $this->language->get('tab_manufacturer');
		$this->data['tab_filters'] = $this->language->get('tab_filters');
		$this->data['tab_stores'] = $this->language->get('tab_stores');	
		$this->data['tab_downloads'] = $this->language->get('tab_downloads');		
		$this->data['tab_related'] = $this->language->get('tab_related');	
		$this->data['tab_atribute'] = $this->language->get('tab_atribute');		
		$this->data['tab_atribute_sec'] = $this->language->get('tab_atribute_sec');	
		$this->data['tab_option'] = $this->language->get('tab_option');		
		$this->data['tab_option_sec'] = $this->language->get('tab_option_sec');	
		$this->data['tab_profile'] = $this->language->get('tab_profile');		
		$this->data['tab_profile_sec'] = $this->language->get('tab_profile_sec');		
		$this->data['tab_discount'] = $this->language->get('tab_discount');		
		$this->data['tab_discount_sec'] = $this->language->get('tab_discount_sec');	
		$this->data['tab_special'] = $this->language->get('tab_special');		
		$this->data['tab_special_sec'] = $this->language->get('tab_special_sec');	
		$this->data['tab_image'] = $this->language->get('tab_image');		
		$this->data['tab_image_sec'] = $this->language->get('tab_image_sec');	
		$this->data['tab_reward'] = $this->language->get('tab_reward');		
		$this->data['tab_reward_sec'] = $this->language->get('tab_reward_sec');			
		$this->data['tab_design'] = $this->language->get('tab_design');		
		$this->data['tab_design_sec'] = $this->language->get('tab_design_sec');	
		$this->data['tab_collabse'] = $this->language->get('tab_collabse');	
		$this->data['tab_status'] = $this->language->get('tab_status');	
		$this->data['tab_status_sec'] = $this->language->get('tab_status_sec');	
		$this->data['tab_marketplace'] = $this->language->get('tab_marketplace');	
		$this->data['tab_marketplace_sec'] = $this->language->get('tab_marketplace_sec');			
		
		// category tabs
		$this->data['tab_general_cat'] = $this->language->get('tab_general_cat');
		$this->data['tab_meta_tag_des_cat'] = $this->language->get('tab_meta_tag_des_cat');	
		$this->data['tab_meta_tag_key_cat'] = $this->language->get('tab_meta_tag_key_cat');
		$this->data['tab_data_cat'] = $this->language->get('tab_data_cat');	
		$this->data['tab_filters_cat'] = $this->language->get('tab_filters_cat');	
		$this->data['tab_stores_cat'] = $this->language->get('tab_stores_cat');		
		$this->data['tab_seo_cat'] = $this->language->get('tab_seo_cat');	
		$this->data['tab_image_cat'] = $this->language->get('tab_image_cat');
		$this->data['tab_top_cat'] = $this->language->get('tab_top_cat');		
		$this->data['tab_columns_cat'] = $this->language->get('tab_columns_cat');		
		$this->data['tab_sort_cat'] = $this->language->get('tab_sort_cat');	
		$this->data['tab_design_cat'] = $this->language->get('tab_design_cat');		
		$this->data['tab_design_sec_cat'] = $this->language->get('tab_design_sec_cat');	
		$this->data['tab_collabse_cat'] = $this->language->get('tab_collabse_cat');	
		$this->data['tab_status_cat'] = $this->language->get('tab_status_cat');	
		$this->data['tab_status_sec_cat'] = $this->language->get('tab_status_sec_cat');						
		
		$this->data['token'] = $this->session->data['token'];
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
			
		$this->load->model('localisation/language');
		$this->data['languages'] = $this->model_localisation_language->getLanguages();
				    
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['banner_image'])) {
			$this->data['error_banner_image'] = $this->error['banner_image'];
		} else {
			$this->data['error_banner_image'] = array();
		}	
		
		
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/hampa_cat', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('module/hampa_cat', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

// HIDE PRODUCTS TABS CONFIG		
		if (isset($this->request->post['hampa_hidden_fields_enabled'])) {
			$this->data['hampa_hidden_fields_enabled'] = $this->request->post['hampa_hidden_fields_enabled'];
		} else {
			$this->data['hampa_hidden_fields_enabled'] = $this->config->get('hampa_hidden_fields_enabled');
		}
		
		if (isset($this->request->post['hampa_hidden_meta_description'])) {
			$this->data['hampa_hidden_meta_description'] = $this->request->post['hampa_hidden_meta_description'];
		} else {
			$this->data['hampa_hidden_meta_description'] = $this->config->get('hampa_hidden_meta_description');
		}
		
		if (isset($this->request->post['hampa_hidden_meta_keywords'])) {
			$this->data['hampa_hidden_meta_keywords'] = $this->request->post['hampa_hidden_meta_keywords'];
		} else {
			$this->data['hampa_hidden_meta_keywords'] = $this->config->get('hampa_hidden_meta_keywords');
		}
		
		if (isset($this->request->post['hampa_hidden_product_tags'])) {
			$this->data['hampa_hidden_product_tabs'] = $this->request->post['hampa_hidden_product_tags'];
		} else {
			$this->data['hampa_hidden_product_tabs'] = $this->config->get('hampa_hidden_product_tags');
		}
		
		if (isset($this->request->post['hampa_hidden_sku'])) {
			$this->data['hampa_hidden_sku'] = $this->request->post['hampa_hidden_sku'];
		} else {
			$this->data['hampa_hidden_sku'] = $this->config->get('hampa_hidden_sku');
		}
		
		if (isset($this->request->post['hampa_hidden_upc'])) {
			$this->data['hampa_hidden_upc'] = $this->request->post['hampa_hidden_upc'];
		} else {
			$this->data['hampa_hidden_upc'] = $this->config->get('hampa_hidden_upc');
		}
		
		if (isset($this->request->post['hampa_hidden_ean'])) {
			$this->data['hampa_hidden_ean'] = $this->request->post['hampa_hidden_ean'];
		} else {
			$this->data['hampa_hidden_ean'] = $this->config->get('hampa_hidden_ean');
		}
		
		if (isset($this->request->post['hampa_hidden_jan'])) {
			$this->data['hampa_hidden_jan'] = $this->request->post['hampa_hidden_jan'];
		} else {
			$this->data['hampa_hidden_jan'] = $this->config->get('hampa_hidden_jan');
		}
		
		if (isset($this->request->post['hampa_hidden_isbn'])) {
			$this->data['hampa_hidden_isbn'] = $this->request->post['hampa_hidden_isbn'];
		} else {
			$this->data['hampa_hidden_isbn'] = $this->config->get('hampa_hidden_isbn');
		}
		
		if (isset($this->request->post['hampa_hidden_mpn'])) {
			$this->data['hampa_hidden_mpn'] = $this->request->post['hampa_hidden_mpn'];
		} else {
			$this->data['hampa_hidden_mpn'] = $this->config->get('hampa_hidden_mpn');
		}
		
		if (isset($this->request->post['hampa_hidden_location'])) {
			$this->data['hampa_hidden_location'] = $this->request->post['hampa_hidden_location'];
		} else {
			$this->data['hampa_hidden_location'] = $this->config->get('hampa_hidden_location');
		}
		
		if (isset($this->request->post['hampa_hidden_price'])) {
			$this->data['hampa_hidden_price'] = $this->request->post['hampa_hidden_price'];
		} else {
			$this->data['hampa_hidden_price'] = $this->config->get('hampa_hidden_price');
		}
		
		if (isset($this->request->post['hampa_hidden_price_default'])) {
			if (!empty($this->request->post['hampa_hidden_price_default'])) {
				$this->data['hampa_hidden_price_default'] = $this->request->post['hampa_hidden_price_default'];
			} else {
				$this->data['hampa_hidden_price_default'] = 0;
			}
			$this->data['hampa_hidden_price_default'] = $this->request->post['hampa_hidden_price_default'];
		} else {
			$this->data['hampa_hidden_price_default'] = $this->config->get('hampa_hidden_price_default');
		}
		

		if (isset($this->request->post['hampa_hidden_tax_class'])) {
			$this->data['hampa_hidden_tax_class'] = $this->request->post['hampa_hidden_tax_class'];
		} else {
			$this->data['hampa_hidden_tax_class'] = $this->config->get('hampa_hidden_tax_class');
		}
		
		if (isset($this->request->post['hampa_hidden_quantity'])) {
			$this->data['hampa_hidden_quantity'] = $this->request->post['hampa_hidden_quantity'];
		} else {
			$this->data['hampa_hidden_quantity'] = $this->config->get('hampa_hidden_quantity');
		}
		
		if (isset($this->request->post['hampa_hidden_quantity_default'])) {
			if (!empty($this->request->post['hampa_hidden_quantity_default'])) {
				$this->data['hampa_hidden_quantity_default'] = $this->request->post['hampa_hidden_quantity_default'];
			} else {
				$this->data['hampa_hidden_quantity_default'] = 1;
			}
			
		} else {
			$this->data['hampa_hidden_quantity_default'] = $this->config->get('hampa_hidden_quantity_default');
		}
		
		if (isset($this->request->post['hampa_hidden_entry_minimum'])) {
			$this->data['hampa_hidden_entry_minimum'] = $this->request->post['hampa_hidden_entry_minimum'];
		} else {
			$this->data['hampa_hidden_entry_minimum'] = $this->config->get('hampa_hidden_entry_minimum');
		}
		
		if (isset($this->request->post['hampa_hidden_subtract'])) {
			$this->data['hampa_hidden_subtract'] = $this->request->post['hampa_hidden_subtract'];
		} else {
			$this->data['hampa_hidden_subtract'] = $this->config->get('hampa_hidden_subtract');
		}
		
		if (isset($this->request->post['hampa_hidden_stock_status'])) {
			$this->data['hampa_hidden_stock_status'] = $this->request->post['hampa_hidden_stock_status'];
		} else {
			$this->data['hampa_hidden_stock_status'] = $this->config->get('hampa_hidden_stock_status');
		}
		
		if (isset($this->request->post['hampa_hidden_entry_shipping'])) {
			$this->data['hampa_hidden_entry_shipping'] = $this->request->post['hampa_hidden_entry_shipping'];
		} else {
			$this->data['hampa_hidden_entry_shipping'] = $this->config->get('hampa_hidden_entry_shipping');
		}
		
		if (isset($this->request->post['hampa_hidden_seo_keyword'])) {
			$this->data['hampa_hidden_seo_keyword'] = $this->request->post['hampa_hidden_seo_keyword'];
		} else {
			$this->data['hampa_hidden_seo_keyword'] = $this->config->get('hampa_hidden_seo_keyword');
		}
		
		if (isset($this->request->post['hampa_hidden_date_available'])) {
			$this->data['hampa_hidden_date_available'] = $this->request->post['hampa_hidden_date_available'];
		} else {
			$this->data['hampa_hidden_date_available'] = $this->config->get('hampa_hidden_date_available');
		}
		
		if (isset($this->request->post['hampa_hidden_dimension'])) {
			$this->data['hampa_hidden_dimension'] = $this->request->post['hampa_hidden_dimension'];
		} else {
			$this->data['hampa_hidden_dimension'] = $this->config->get('hampa_hidden_dimension');
		}
		
		if (isset($this->request->post['hampa_hidden_length'])) {
			$this->data['hampa_hidden_length'] = $this->request->post['hampa_hidden_length'];
		} else {
			$this->data['hampa_hidden_length'] = $this->config->get('hampa_hidden_length');
		}
		
		if (isset($this->request->post['hampa_hidden_weight'])) {
			$this->data['hampa_hidden_weight'] = $this->request->post['hampa_hidden_weight'];
		} else {
			$this->data['hampa_hidden_weight'] = $this->config->get('hampa_hidden_weight');
		}
		
		if (isset($this->request->post['hampa_hidden_weight_class'])) {
			$this->data['hampa_hidden_weight_class'] = $this->request->post['hampa_hidden_weight_class'];
		} else {
			$this->data['hampa_hidden_weight_class'] = $this->config->get('hampa_hidden_weight_class');
		}
		
		if (isset($this->request->post['hampa_hidden_sort_order'])) {
			$this->data['hampa_hidden_sort_order'] = $this->request->post['hampa_hidden_sort_order'];
		} else {
			$this->data['hampa_hidden_sort_order'] = $this->config->get('hampa_hidden_sort_order');
		}
		
		if (isset($this->request->post['hampa_hidden_manufacturer'])) {
			$this->data['hampa_hidden_manufacturer'] = $this->request->post['hampa_hidden_manufacturer'];
		} else {
			$this->data['hampa_hidden_manufacturer'] = $this->config->get('hampa_hidden_manufacturer');
		}
		
		if (isset($this->request->post['hampa_hidden_filter'])) {
			$this->data['hampa_hidden_filter'] = $this->request->post['hampa_hidden_filter'];
		} else {
			$this->data['hampa_hidden_filter'] = $this->config->get('hampa_hidden_filter');
		}
		
		if (isset($this->request->post['hampa_hidden_store'])) {
			$this->data['hampa_hidden_store'] = $this->request->post['hampa_hidden_store'];
		} else {
			$this->data['hampa_hidden_store'] = $this->config->get('hampa_hidden_store');
		}
		
		if (isset($this->request->post['hampa_hidden_download'])) {
			$this->data['hampa_hidden_download'] = $this->request->post['hampa_hidden_download'];
		} else {
			$this->data['hampa_hidden_download'] = $this->config->get('hampa_hidden_download');
		}
		
		if (isset($this->request->post['hampa_hidden_related'])) {
			$this->data['hampa_hidden_related'] = $this->request->post['hampa_hidden_related'];
		} else {
			$this->data['hampa_hidden_related'] = $this->config->get('hampa_hidden_related');
		}					
		
		if (isset($this->request->post['hampa_hidden_design_tab'])) {
			$this->data['hampa_hidden_design_tab'] = $this->request->post['hampa_hidden_design_tab'];
		} else {
			$this->data['hampa_hidden_design_tab'] = $this->config->get('hampa_hidden_design_tab');
		}
		
		if (isset($this->request->post['hampa_hidden_reward_tab'])) {
			$this->data['hampa_hidden_reward_tab'] = $this->request->post['hampa_hidden_reward_tab'];
		} else {
			$this->data['hampa_hidden_reward_tab'] = $this->config->get('hampa_hidden_reward_tab');
		}
		
		if (isset($this->request->post['hampa_hidden_special_tab'])) {
			$this->data['hampa_hidden_special_tab'] = $this->request->post['hampa_hidden_special_tab'];
		} else {
			$this->data['hampa_hidden_special_tab'] = $this->config->get('hampa_hidden_special_tab');
		}
		
		if (isset($this->request->post['hampa_hidden_image_tab'])) {
			$this->data['hampa_hidden_image_tab'] = $this->request->post['hampa_hidden_image_tab'];
		} else {
			$this->data['hampa_hidden_image_tab'] = $this->config->get('hampa_hidden_image_tab');
		}
		
		if (isset($this->request->post['hampa_hidden_discount_tab'])) {
			$this->data['hampa_hidden_discount_tab'] = $this->request->post['hampa_hidden_discount_tab'];
		} else {
			$this->data['hampa_hidden_discount_tab'] = $this->config->get('hampa_hidden_discount_tab');
		}
		
		if (isset($this->request->post['hampa_hidden_profile_tab'])) {
			$this->data['hampa_hidden_profile_tab'] = $this->request->post['hampa_hidden_profile_tab'];
		} else {
			$this->data['hampa_hidden_profile_tab'] = $this->config->get('hampa_hidden_profile_tab');
		}
		
		if (isset($this->request->post['hampa_hidden_option_tab'])) {
			$this->data['hampa_hidden_option_tab'] = $this->request->post['hampa_hidden_option_tab'];
		} else {
			$this->data['hampa_hidden_option_tab'] = $this->config->get('hampa_hidden_option_tab');
		}
		
		if (isset($this->request->post['hampa_hidden_attribute_tab'])) {
			$this->data['hampa_hidden_attribute_tab'] = $this->request->post['hampa_hidden_attribute_tab'];
		} else {
			$this->data['hampa_hidden_attribute_tab'] = $this->config->get('hampa_hidden_attribute_tab');
		}
		
		if (isset($this->request->post['hampa_hidden_collabse'])) {
			$this->data['hampa_hidden_collabse'] = $this->request->post['hampa_hidden_collabse'];
		} else {
			$this->data['hampa_hidden_collabse'] = $this->config->get('hampa_hidden_collabse');
		}
		
		if (isset($this->request->post['hampa_hidden_marketplace_tab'])) {
			$this->data['hampa_hidden_marketplace_tab'] = $this->request->post['hampa_hidden_marketplace_tab'];
		} else {
			$this->data['hampa_hidden_marketplace_tab'] = $this->config->get('hampa_hidden_marketplace_tab');
		}
		
// HIDE CATEGORY TABS CONFIG	

		if (isset($this->request->post['hampa_hidden_meta_description_cat'])) {
			$this->data['hampa_hidden_meta_description_cat'] = $this->request->post['hampa_hidden_meta_description_cat'];
		} else {
			$this->data['hampa_hidden_meta_description_cat'] = $this->config->get('hampa_hidden_meta_description_cat');
		}

		if (isset($this->request->post['hampa_hidden_meta_keywords_cat'])) {
			$this->data['hampa_hidden_meta_keywords_cat'] = $this->request->post['hampa_hidden_meta_keywords_cat'];
		} else {
			$this->data['hampa_hidden_meta_keywords_cat'] = $this->config->get('hampa_hidden_meta_keywords_cat');
		}

		if (isset($this->request->post['hampa_hidden_filter_cat'])) {
			$this->data['hampa_hidden_filter_cat'] = $this->request->post['hampa_hidden_filter_cat'];
		} else {
			$this->data['hampa_hidden_filter_cat'] = $this->config->get('hampa_hidden_filter_cat');
		}

		if (isset($this->request->post['hampa_hidden_store_cat'])) {
			$this->data['hampa_hidden_store_cat'] = $this->request->post['hampa_hidden_store_cat'];
		} else {
			$this->data['hampa_hidden_store_cat'] = $this->config->get('hampa_hidden_store_cat');
		}

		if (isset($this->request->post['hampa_hidden_seo_keyword_cat'])) {
			$this->data['hampa_hidden_seo_keyword_cat'] = $this->request->post['hampa_hidden_seo_keyword_cat'];
		} else {
			$this->data['hampa_hidden_seo_keyword_cat'] = $this->config->get('hampa_hidden_seo_keyword_cat');
		}

		if (isset($this->request->post['hampa_hidden_image_cat'])) {
			$this->data['hampa_hidden_image_cat'] = $this->request->post['hampa_hidden_image_cat'];
		} else {
			$this->data['hampa_hidden_image_cat'] = $this->config->get('hampa_hidden_image_cat');
		}

		if (isset($this->request->post['hampa_hidden_top_cat'])) {
			$this->data['hampa_hidden_top_cat'] = $this->request->post['hampa_hidden_top_cat'];
		} else {
			$this->data['hampa_hidden_top_cat'] = $this->config->get('hampa_hidden_top_cat');
		}

		if (isset($this->request->post['hampa_hidden_columns_cat'])) {
			$this->data['hampa_hidden_columns_cat'] = $this->request->post['hampa_hidden_columns_cat'];
		} else {
			$this->data['hampa_hidden_columns_cat'] = $this->config->get('hampa_hidden_columns_cat');
		}

		if (isset($this->request->post['hampa_hidden_sort_order_cat'])) {
			$this->data['hampa_hidden_sort_order_cat'] = $this->request->post['hampa_hidden_sort_order_cat'];
		} else {
			$this->data['hampa_hidden_sort_order_cat'] = $this->config->get('hampa_hidden_sort_order_cat');
		}

		if (isset($this->request->post['hampa_hidden_design_tab_cat'])) {
			$this->data['hampa_hidden_design_tab_cat'] = $this->request->post['hampa_hidden_design_tab_cat'];
		} else {
			$this->data['hampa_hidden_design_tab_cat'] = $this->config->get('hampa_hidden_design_tab_cat');
		}

		if (isset($this->request->post['hampa_hidden_fields_enabled_cat'])) {
			$this->data['hampa_hidden_fields_enabled_cat'] = $this->request->post['hampa_hidden_fields_enabled_cat'];
		} else {
			$this->data['hampa_hidden_fields_enabled_cat'] = $this->config->get('hampa_hidden_fields_enabled_cat');
		}

		if (isset($this->request->post['hampa_hidden_collabse_cat'])) {
			$this->data['hampa_hidden_collabse_cat'] = $this->request->post['hampa_hidden_collabse_cat'];
		} else {
			$this->data['hampa_hidden_collabse_cat'] = $this->config->get('hampa_hidden_collabse_cat');
		}
		
		$this->template = 'module/hampa_cat.tpl';
		$this->children = array(
			'common/header',
			'common/footer',
		);
		
		$this->response->setOutput($this->render());
	}
		
    public function install() { }
	
    public function uninstall()
    { 
		$this->load->model('setting/setting');
		$this->model_setting_setting->editSetting('hampa_cat', array('hampa_hidden_fields_enabled' => '','hampa_hidden_meta_description' => '','hampa_hidden_meta_keywords' => '','hampa_hidden_product_tags' => '','hampa_hidden_sku' => '','hampa_hidden_upc' => '','hampa_hidden_ean' => '','hampa_hidden_jan' => '','hampa_hidden_isbn' => '','hampa_hidden_mpn' => '','hampa_hidden_location' => '','hampa_hidden_price' => '','hampa_hidden_price_default' => '','hampa_hidden_tax_class' => '','hampa_hidden_quantity' => '','hampa_hidden_quantity_default' => '','hampa_hidden_entry_minimum' => '','hampa_hidden_subtract' => '','hampa_hidden_stock_status' => '','hampa_hidden_entry_shipping' => '','hampa_hidden_seo_keyword' => '','hampa_hidden_date_available' => '','hampa_hidden_dimension' => '','hampa_hidden_length' => '','hampa_hidden_weight' => '','hampa_hidden_weight_class' => '','hampa_hidden_sort_order' => '','hampa_hidden_manufacturer' => '','hampa_hidden_filter' => '','hampa_hidden_filter' => '','hampa_hidden_store' => '','hampa_hidden_download' => '','hampa_hidden_download' => '','hampa_hidden_related' => '','hampa_hidden_related' => '','hampa_hidden_attribute_tab' => '','hampa_hidden_option_tab' => '','hampa_hidden_profiles_tab' => '','hampa_hidden_discount_tab' => '','hampa_hidden_special_tab' => '','hampa_hidden_image_tab' => '','hampa_hidden_reward_tab' => '','hampa_hidden_design_tab' => '','hampa_hidden_meta_description_cat' => '','hampa_hidden_meta_keywords_cat' => '','hampa_hidden_filter_cat' => '','hampa_hidden_store_cat' => '','hampa_hidden_seo_keyword_cat' => '','hampa_hidden_image_cat' => '','hampa_hidden_top_cat' => '','hampa_hidden_top_cat' => '','hampa_hidden_sort_order_cat' => '','hampa_hidden_design_tab_cat' => '','hampa_hidden_fields_enabled_cat' => '','hampa_hidden_collabse_cat' => '','hampa_hidden_collabse' => ''));			  
    }
	  
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/hampa_cat')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}	
	}      
}
?>