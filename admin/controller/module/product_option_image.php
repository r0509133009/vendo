<?php
class ControllerModuleProductOptionImage extends Controller {
	private $error = array(); 
	
	public function index() {   
		$this->language->load('module/product_option_image');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
		
			$this->session->data['success'] = $this->language->get('text_success');
						
			$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}
				
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_yes'] = $this->language->get('text_yes');
		$this->data['text_no'] = $this->language->get('text_no');
		
		$this->data['entry_enable'] = $this->language->get('entry_enable');
		$this->data['entry_show_selected_image_in_cart'] = $this->language->get('entry_show_selected_image_in_cart');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/product_option_image', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['enable'] ="";
        if (is_file('../vqmod/xml/product_detail_edit_page.xml')) {
			$path_parts1 = pathinfo('../vqmod/xml/product_detail_edit_page.xml');			
		} else if (is_file('../vqmod/xml/product_detail_edit_page.xml_')){
			$path_parts1 = pathinfo('../vqmod/xml/product_detail_edit_page.xml_');
		}
		if (is_file('../vqmod/xml/show_selected_image_in_cart.xml')) {
			$path_parts2 = pathinfo('../vqmod/xml/show_selected_image_in_cart.xml');			
		} else if (is_file('../vqmod/xml/show_selected_image_in_cart.xml_')){
			$path_parts2 = pathinfo('../vqmod/xml/show_selected_image_in_cart.xml_');
		}
		$extension1 = $path_parts1['extension'];
		$extension2 = $path_parts2['extension'];
        if ($extension1 == 'xml_'){
            $this->data['enable'] = $this->language->get('text_no');
        }
        else{
            $this->data['enable'] = $this->language->get('text_yes');
        }
        if ($extension2 == 'xml_') {
			$this->data['show_image_enable'] = $this->language->get('text_no');
		} else {
			$this->data['show_image_enable'] = $this->language->get('text_yes');
		}			
		$this->data['action'] = $this->url->link('module/product_option_image/product_option_image', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->load->model('design/layout');
		
		$this->data['layouts'] = $this->model_design_layout->getLayouts();

		$this->template = 'module/product_option_image.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
		
		$this->response->setOutput($this->render());
	}
	public function product_option_image(){
		$this->language->load('module/product_option_image');
		if ($this->request->post['poi_module_enable'] == 1) {
			$this->data['enable'] = $this->language->get('text_yes');
			$this->rename2('../vqmod/xml/product_detail_edit_page.xml_', '../vqmod/xml/product_detail_edit_page.xml');
			if($this->request->post['poi_module_show_selected_image_in_cart'] == 1) {
			    $this->data['show_image_enable'] = $this->language->get('text_yes');
				$this->rename2('../vqmod/xml/show_selected_image_in_cart.xml_', '../vqmod/xml/show_selected_image_in_cart.xml');
			} else {
			    $this->data['show_image_enable'] = $this->language->get('text_no');
                $this->rename2('../vqmod/xml/show_selected_image_in_cart.xml', '../vqmod/xml/show_selected_image_in_cart.xml_');                 
			}
		} else {
		    $this->data['enable'] = $this->language->get('text_no');
			$this->rename2('../vqmod/xml/product_detail_edit_page.xml', '../vqmod/xml/product_detail_edit_page.xml_');
			$this->rename2('../vqmod/xml/show_selected_image_in_cart.xml', '../vqmod/xml/show_selected_image_in_cart.xml_');			
		}
        $this->session->data['success'] = $this->language->get('text_success');        
		$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
	}
	
	public function install()
	{
		$this->load->model('tool/misc_util');
		
		if($this->model_tool_misc_util->existColumn('product_option_value', 'image_product_option_value') == false)
		{
			$query = "ALTER TABLE " . DB_PREFIX . "product_option_value ";
			$query .= "ADD COLUMN image_product_option_value VARCHAR(255) NOT NULL DEFAULT ''";
					
			$this->db->query($query);
		}
	}
	
	public function uninstall()
	{
		$this->load->model('tool/misc_util');
		
		if($this->model_tool_misc_util->existColumn('product_option_value', 'image_product_option_value') == false)
		{
			$this->model_tool_misc_util->removeColumn('product_option_value', 'image_product_option_value');
		}
	}
	
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/product_option_image')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
	
	private function rename2($oldFileName, $newFileName)
	{
		if(file_exists($oldFileName))
			rename($oldFileName, $newFileName);
	}
}
?>