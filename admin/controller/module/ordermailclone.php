<?php
class ControllerModuleOrderMailClone extends Controller {
  private $error = array(); 

  public function index() {   
    $this->language->load('module/ordermailclone');
    $this->load->model('ordermailclone/data');

    //////////////////////////////////////////////////////////////////////////////
    // HANDLE AJAX REQUESTS                                                     //       
    //////////////////////////////////////////////////////////////////////////////
    if (isset($this->request->get['action'])) {
      
      // ADD 
      if ($this->request->get['action'] == 'add') {
         if (isset($this->request->get['email']) && isset($this->request->get['description'])) {
          $this->model_ordermailclone_data->add(array('description' => $this->request->get['description'], 'mail' => $this->request->get['email']));
          return $this->response->setOutput(json_encode(array('status' => 'success', 'id' => $this->db->getLastId())));
         } {
          return $this->response->setOutput(json_encode(array('status' => 'failure')));
         }
      }

      // DELETE
      if ($this->request->get['action'] == 'delete') {
         if (isset($this->request->get['order_mail_clone_id'])) {
          $this->model_ordermailclone_data->delete(array('order_mail_clone_id' => $this->request->get['order_mail_clone_id']));
          return $this->response->setOutput(json_encode(array('status' => 'success')));
         } {
          return $this->response->setOutput(json_encode(array('status' => 'failure')));
         }
      }

    }

    // SET TEMPLATE OUTPUT VARIABLES
    $this->document->setTitle($this->language->get('heading_title'));

    $this->data['heading_title'] = $this->language->get('heading_title');

    $this->data['ajax_url'] = array(
      'add' => $this->url->link('module/ordermailclone', 'token=' . $this->session->data['token'] . '&action=add', 'SSL'),
      'delete' => $this->url->link('module/ordermailclone', 'token=' . $this->session->data['token'] . '&action=delete', 'SSL')
    );

    $this->data['breadcrumbs'] = array();

    $this->data['breadcrumbs'][] = array(
      'text'      => $this->language->get('text_home'),
      'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      'separator' => false
    );

    $this->data['breadcrumbs'][] = array(
      'text'      => $this->language->get('text_module'),
      'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      'separator' => ' :: '
    );

    $this->data['breadcrumbs'][] = array(
      'text'      => $this->language->get('heading_title'),
      'href'      => $this->url->link('module/ordermailclone', 'token=' . $this->session->data['token'], 'SSL'),
      'separator' => ' :: '
    );


    // DATA
    $this->data['clone_mails'] = $this->model_ordermailclone_data->select();

    $this->template = 'module/ordermailclone.tpl';
    $this->children = array(
      'common/header',
      'common/footer'
    );

    $this->response->setOutput($this->render());
  }


  public function install() 
  {
    $this->load->model('ordermailclone/data');
    $this->model_ordermailclone_data->createTable(); 
 
    // $this->load->model('setting/setting');
   // $this->model_setting_setting->editSetting('ordermailclone', array('ordermailclone_status'=>1));
  }


  public function uninstall() 
  {
    $this->load->model('ordermailclone/data');
    $this->model_ordermailclone_data->deleteTable();
         
    // $this->load->model('setting/setting');
    // $this->model_setting_setting->editSetting('ordermailclone', array('ordermailclone_status'=>0));
  }

  protected function validate() {
    if (!$this->user->hasPermission('modify', 'module/information')) {
      $this->error['warning'] = $this->language->get('error_permission');
    }

    if (!$this->error) {
      return true;
    } else {
      return false;
    } 
  }
}
?>