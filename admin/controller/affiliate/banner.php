<?php
class ControllerAffiliateBanner extends Controller {
	private $error = array();
	public function index()
	{
		$this->load->language('affiliate/banner');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('affiliate/banner');
		$this->getList();
	}
	
	public function add() {
		$this->load->language('affiliate/banner');

		$this->document->setTitle($this->language->get('heading_title_add'));
		$this->data['heading_title'] = $this->language->get('heading_title_add');

		$this->load->model('affiliate/banner');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_affiliate_banner->addBanner($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_bn_name'])) {
				$url .= '&filter_bn_name=' . urlencode(html_entity_decode($this->request->get['filter_bn_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_bn_status'])) {
				$url .= '&filter_bn_status=' . $this->request->get['filter_bn_status'];
			}

			if (isset($this->request->get['filter_bn_create'])) {
				$url .= '&filter_bn_create=' . $this->request->get['filter_bn_create'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('affiliate/banner&token=' . $this->session->data['token'] . $url));
		}

		$this->getForm();
	}
	
	public function edit() {
		$this->load->language('affiliate/banner');

		$this->document->setTitle($this->language->get('heading_title_edit'));
		$this->data['heading_title'] = $this->language->get('heading_title_edit');

		$this->load->model('affiliate/banner');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_affiliate_banner->editBanner($this->request->get['bn_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_bn_name'])) {
				$url .= '&filter_bn_name=' . urlencode(html_entity_decode($this->request->get['filter_bn_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_bn_create'])) {
				$url .= '&filter_bn_create=' . urlencode(html_entity_decode($this->request->get['filter_bn_create'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_bn_status'])) {
				$url .= '&filter_bn_status=' . $this->request->get['filter_bn_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('affiliate/banner&token=' . $this->session->data['token'] . $url));
		}

		$this->getForm();
	}
	
	public function delete() {
		$this->load->language('affiliate/banner');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('affiliate/banner');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $bn_id) {
				$this->model_affiliate_banner->deleteBanner($bn_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_bn_name'])) {
				$url .= '&filter_bn_name=' . urlencode(html_entity_decode($this->request->get['filter_bn_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_bn_create'])) {
				$url .= '&filter_bn_create=' . urlencode(html_entity_decode($this->request->get['filter_bn_create'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_bn_status'])) {
				$url .= '&filter_bn_status=' . $this->request->get['filter_bn_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('affiliate/banner&token=' . $this->session->data['token'] . $url));
		}

		$this->getList();
	}
	
	protected function getList() {
		if (isset($this->request->get['filter_bn_name'])) {
			$filter_bn_name = $this->request->get['filter_bn_name'];
		} else {
			$filter_bn_name = null;
		}
		
		if (isset($this->request->get['filter_bn_proid'])) {
			$filter_bn_proid = $this->request->get['filter_bn_proid'];
		} else {
			$filter_bn_proid = null;
		}
		
		if (isset($this->request->get['filter_bn_size'])) {
			$filter_bn_size = $this->request->get['filter_bn_size'];
		} else {
			$filter_bn_size = null;
		}
		
		if (isset($this->request->get['filter_bn_create'])) {
			$filter_bn_create = $this->request->get['filter_bn_create'];
		} else {
			$filter_bn_create = null;
		}
		
		if (isset($this->request->get['filter_bn_status'])) {
			$filter_bn_status = $this->request->get['filter_bn_status'];
		} else {
			$filter_bn_status = null;
		}
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'bn_name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';
		
		if(isset($_GET['limit'])){
			if($_GET['limit'] < 10){
				$limit = 10;
			} else {
				$limit = $_GET['limit'];
			}
		} else {
			$limit = 10;
		}
		
		if (isset($this->request->get['filter_bn_name'])) {
			$url .= '&filter_bn_name=' . urlencode(html_entity_decode($this->request->get['filter_bn_name'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_bn_proid'])) {
			$url .= '&filter_bn_proid=' . $this->request->get['filter_bn_proid'];
		}
		
		if (isset($this->request->get['filter_bn_size'])) {
			$url .= '&filter_bn_size=' . $this->request->get['filter_bn_size'];
		}
		
		if (isset($this->request->get['filter_bn_create'])) {
			$url .= '&filter_bn_create=' . $this->request->get['filter_bn_create'];
		}
		
		if (isset($this->request->get['filter_bn_status'])) {
			$url .= '&filter_bn_status=' . $this->request->get['filter_bn_status'];
		}
		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard&token=' . $this->session->data['token']),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('affiliate/banner&token=' . $this->session->data['token'] . $url),
			'separator' => ' :: '
		);
		
		$this->data['add'] = $this->url->link('affiliate/banner/add&token=' . $this->session->data['token'] . $url);
		$this->data['delete'] = $this->url->link('affiliate/banner/delete&token=' . $this->session->data['token'] . $url);
		
		$this->data['banners'] = array();
		$filter_data = array(
			'filter_bn_proid'   	=> $filter_bn_proid,
			'filter_bn_name'      	=> $filter_bn_name,
			'filter_bn_size'     	=> $filter_bn_size,
			'filter_bn_create'   	=> $filter_bn_create,
			'filter_bn_status' 		=> $filter_bn_status,
			'sort'              => $sort,
			'order'             => $order,
			'start'             => ($page - 1) * $limit,
			'limit'             => $limit
		);
		
		$banner_total = $this->model_affiliate_banner->getTotalBanners($filter_data);
		$results = $this->model_affiliate_banner->getBanners($filter_data);
		
		foreach ($results as $result) {
			$this->data['banners'][] = array(
				'bn_id' 		=> $result['bn_id'],
				'bn_name'		=> $result['bn_name'],
				'bn_size'		=> $result['bn_size'],
				'bn_img'		=> $result['bn_img'],
				'bn_camid'		=> $this->data['name'] = $this->model_affiliate_banner->getCampaignsName($result['bn_camid']),
				'bn_views'		=> $result['bn_views'],
				'bn_click'		=> $result['bn_click'],
				'bn_create'   	=> date($this->language->get('date_format_short'), strtotime($result['bn_create'])),
				'bn_status'		=> ($result['bn_status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
				'edit'         => $this->url->link('affiliate/banner/edit&token=' . $this->session->data['token'] . '&bn_id=' . $result['bn_id'] . $url)
			);
		}
		
		//Get_text
		$this->data['heading_title'] 		= $this->language->get('heading_title');
		$this->data['text_list'] = $this->language->get('text_list');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_yes'] = $this->language->get('text_yes');
		$this->data['text_no'] = $this->language->get('text_no');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_confirm'] = $this->language->get('text_confirm');
		$this->data['text_none'] = $this->language->get('text_none');
		
		$this->data['button_approve'] = $this->language->get('button_approve');
		$this->data['button_add'] = $this->language->get('button_add');
		$this->data['button_edit'] = $this->language->get('button_edit');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['text_action'] = $this->language->get('text_action');
		$this->data['text_date'] = $this->language->get('text_date');
		$this->data['text_name'] = $this->language->get('text_name');
		$this->data['text_size'] = $this->language->get('text_size');
		$this->data['text_campaign'] = $this->language->get('text_campaign');
		$this->data['text_image'] = $this->language->get('text_image');
		$this->data['text_impression'] = $this->language->get('text_impression');
		$this->data['text_click'] = $this->language->get('text_click');
		$this->data['text_status'] = $this->language->get('text_status');
		$this->data['text_no_banner'] = $this->language->get('text_no_banner');
		$this->data['text_please_choose_img'] = $this->language->get('text_please_choose_img');
		$this->data['text_preview'] = $this->language->get('text_preview');
		$this->data['text_save'] = $this->language->get('text_save');
		$this->data['text_cancel'] = $this->language->get('text_cancel');
		
		$this->data['token'] = $this->session->data['token'];
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$this->data['selected'] = (array)$this->request->post['selected'];
		} else {
			$this->data['selected'] = array();
		}
		
		$url = '';
		
		if (isset($this->request->get['filter_bn_name'])) {
			$url .= '&filter_bn_name=' . urlencode(html_entity_decode($this->request->get['filter_bn_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_bn_status'])) {
			$url .= '&filter_bn_status=' . $this->request->get['filter_bn_status'];
		}

		if (isset($this->request->get['filter_bn_create'])) {
			$url .= '&filter_bn_create=' . $this->request->get['filter_bn_create'];
		}
		
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$this->data['sort_name'] = $this->url->link('affiliate/banner&token=' . $this->session->data['token'] . '&sort=name' . $url);
		
		$this->data['sort_date_added'] = $this->url->link('affiliate/banner&token=' . $this->session->data['token'] . '&sort=a.bn_create' . $url);

		$url = '';
		
		if (isset($this->request->get['filter_bn_name'])) {
			$url .= '&filter_bn_name=' . urlencode(html_entity_decode($this->request->get['filter_bn_name'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_bn_status'])) {
			$url .= '&filter_bn_status=' . $this->request->get['filter_bn_status'];
		}
		
		if (isset($this->request->get['filter_bn_create'])) {
			$url .= '&filter_bn_create=' . $this->request->get['filter_bn_create'];
		}
		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		$pagination = new Pagination();
		$pagination->total = $banner_total;
		$pagination->page = $page;
		$pagination->limit = $limit;
		$pagination->url = $this->url->link('affiliate/banner&token=' . $this->session->data['token'] . $url . '&page={page}');

		$this->data['pagination'] = $pagination->render();

		$this->data['results'] = sprintf($this->language->get('text_pagination'), ($banner_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($banner_total - $limit)) ? $banner_total : ((($page - 1) * $limit) + $limit), $banner_total, ceil($banner_total / $limit));
		
		$this->data['filter_bn_name'] 	= $filter_bn_name;
		$this->data['filter_bn_proid'] 	= $filter_bn_proid;
		$this->data['filter_bn_size'] 	= $filter_bn_size;
		$this->data['filter_bn_create'] 	= $filter_bn_create;
		$this->data['filter_bn_status'] 	= $filter_bn_status;
		

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		//$this->data['header'] = $this->load->controller('common/header');
		//$this->data['column_left'] = $this->load->controller('common/column_left');
		//$this->data['footer'] = $this->load->controller('common/footer');

		//$this->response->setOutput($this->load->view('affiliate/banner_list.tpl', $data));
		
		$this->template = 'affiliate/banner_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
		$this->response->setOutput($this->render());
		
	}
	
	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_form'] = !isset($this->request->get['bn_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->error['bn_name'])) {
			$this->data['error_bn_name'] = $this->error['bn_name'];
		} else {
			$this->data['error_bn_name'] = '';
		}
		
		if (isset($this->error['bn_des'])) {
			$this->data['error_bn_des'] = $this->error['bn_des'];
		} else {
			$this->data['error_bn_des'] = '';
		}
		
		if (isset($this->error['bn_camid'])) {
			$this->data['error_bn_camid'] = $this->error['bn_camid'];
		} else {
			$this->data['error_bn_camid'] = '';
		}
		if (isset($this->error['bn_img'])) {
			$this->data['error_bn_img'] = $this->error['bn_img'];
		} else {
			$this->data['error_bn_img'] = '';
		}
		
		if (isset($this->error['bn_size'])) {
			$this->data['error_bn_size'] = $this->error['bn_size'];
		} else {
			$this->data['error_bn_size'] = '';
		}
		
		$url = '';
		
		if (isset($this->request->get['filter_bn_name'])) {
			$url .= '&filter_bn_name=' . urlencode(html_entity_decode($this->request->get['filter_bn_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_bn_status'])) {
			$url .= '&filter_bn_status=' . $this->request->get['filter_bn_status'];
		}

		if (isset($this->request->get['filter_bn_create'])) {
			$url .= '&filter_bn_create=' . $this->request->get['filter_bn_create'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard&token=' . $this->session->data['token']),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('affiliate/banner&token=' . $this->session->data['token'] . $url),
			'separator' => ' :: '
		);
		
		$this->data['button_add'] = $this->language->get('button_add');
		$this->data['text_action'] = $this->language->get('text_action');
		$this->data['text_date'] = $this->language->get('text_date');
		$this->data['text_name'] = $this->language->get('text_name');
		$this->data['text_bn_name'] = $this->language->get('text_bn_name');
		$this->data['text_size'] = $this->language->get('text_size');
		$this->data['text_campaign'] = $this->language->get('text_campaign');
		$this->data['text_image'] = $this->language->get('text_image');
		$this->data['text_url_image'] = $this->language->get('text_url_image');
		$this->data['text_impression'] = $this->language->get('text_impression');
		$this->data['text_click'] = $this->language->get('text_click');
		$this->data['text_status'] = $this->language->get('text_status');
		$this->data['text_please_choose_img'] = $this->language->get('text_please_choose_img');
		$this->data['text_preview'] = $this->language->get('text_preview');
		$this->data['text_save'] = $this->language->get('text_save');
		$this->data['text_cancel'] = $this->language->get('text_cancel');
		$this->data['text_no_none'] = $this->language->get('text_no_none');
		
		$this->data['text_option'] = $this->language->get('text_option');
		$this->data['text_product'] = $this->language->get('text_product');
		$this->data['text_select_size'] = $this->language->get('text_select_size');
		$this->data['text_select_campaign'] = $this->language->get('text_select_campaign');
		$this->data['text_description'] = $this->language->get('text_description');
		$this->data['text_bn_description'] = $this->language->get('text_bn_description');
		$this->data['text_choose_img'] = $this->language->get('text_choose_img');
		
		if (!isset($this->request->get['bn_id'])) {
			$this->data['action'] = $this->url->link('affiliate/banner/add&token=' . $this->session->data['token'] . $url);
		} else {
			$this->data['action'] = $this->url->link('affiliate/banner/edit&token=' . $this->session->data['token'] . '&bn_id=' . $this->request->get['bn_id'] . $url);
		}

		$this->data['cancel'] = $this->url->link('affiliate/banner&token=' . $this->session->data['token'] . $url);

		if (isset($this->request->get['bn_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$bn_info = $this->model_affiliate_banner->getBanner($this->request->get['bn_id']);
		}

		$this->data['token'] = $this->session->data['token'];
		
		if (isset($this->request->get['bn_id'])) {
			$this->data['bn_id'] = $this->request->get['bn_id'];
		} else {
			$this->data['bn_id'] = 0;
		}
		
		if (isset($this->request->post['bn_name'])) {
			$this->data['bn_name'] = $this->request->post['bn_name'];
		} elseif (!empty($bn_info)) {
			$this->data['bn_name'] = $bn_info['bn_name'];
		} else {
			$this->data['bn_name'] = '';
		}
		
		if (isset($this->request->post['bn_proid'])) {
			$this->data['bn_proid'] = $this->request->post['bn_proid'];
		} elseif (!empty($bn_info)) {
			$this->data['bn_proid'] = $bn_info['bn_proid'];
		} else {
			$this->data['bn_proid'] = '';
		}
		
		if (isset($this->request->post['bn_camid'])) {
			$this->data['bn_camid'] = $this->request->post['bn_camid'];
		} elseif (!empty($bn_info)) {
			$this->data['bn_camid'] = $bn_info['bn_camid'];
		} else {
			$this->data['bn_camid'] = '';
		}
		
		if (isset($this->request->post['bn_size'])) {
			$this->data['bn_size'] = $this->request->post['bn_size'];
		} elseif (!empty($bn_info)) {
			$this->data['bn_size'] = $bn_info['bn_size'];
		} else {
			$this->data['bn_size'] = '';
		}
		
		if (isset($this->request->post['bn_des'])) {
			$this->data['bn_des'] = $this->request->post['bn_des'];
		} elseif (!empty($bn_info)) {
			$this->data['bn_des'] = $bn_info['bn_des'];
		} else {
			$this->data['bn_des'] = '';
		}
		

		if (isset($this->request->post['bn_img'])) {
			$this->data['bn_img'] = $this->request->post['bn_img'];
		} elseif (!empty($bn_info)) {
			$this->data['bn_img'] = $bn_info['bn_img'];
		} else {
			$this->data['bn_img'] = '';
		}
		
		if (isset($this->request->post['bn_status'])) {
			$this->data['bn_status'] = $this->request->post['bn_status'];
		} elseif (!empty($bn_info)) {
			$this->data['bn_status'] = $bn_info['bn_status'];
		} else {
			$this->data['bn_status'] = true;
		}
		
		if (isset($this->request->post['bn_create'])) {
			$this->data['bn_create'] = $this->request->post['bn_create'];
		} elseif (!empty($bn_info)) {
			$this->data['bn_create'] = $bn_info['bn_create'];
		} else {
			$this->data['bn_create'] = true;
		}
		
		if (isset($this->request->post['confirm'])) {
			$this->data['confirm'] = $this->request->post['confirm'];
		} else {
			$this->data['confirm'] = '';
		}
		
		//Size banner
		$this->load->model('affiliate/banner');
		$this->data['banners'] = $this->model_affiliate_banner->getSizes();
		
		//Products banner
		
		$this->load->model('catalog/product');

		if (isset($this->request->post['product_id'])) {
			$this->data['product_id'] = $this->request->post['product_id'];
		} elseif (!empty($product_info)) {
			$this->data['product_id'] = $product_info['product_id'];
		} else {
			$this->data['product_id'] = 0;
		}
		
		if(isset($_GET['bn_id']))
		{
			$product_info = $this->model_catalog_product->getProduct($bn_info['bn_proid']);
			if ($product_info) {
				$this->data['product_name'] = $product_info['name'];
			} else {
				$this->data['product_name'] = '';
			}
		} else {
			$this->data['product_name'] = '';
		}
		//$this->data['products'] = $this->model_affiliate_banner->getProducts();
		//$this->load->model('catalog/product');
		//$this->data['products'] = array();
		//$results_products = $this->model_catalog_product->getProducts();
		//foreach ($results_products as $resultpro) {
			//$this->data['products'][] = array(
				//'product_id' => $resultpro['product_id'],
				//'name'       => $resultpro['name'],
				//'model'      => $resultpro['model'],
				//'price'      => $resultpro['price'],
			//);
		//}
		//getPorductByCategory
		//$this->data['PorductByCategory'] = $this->model_affiliate_banner->getPorductByCategory($campaign_camid);
		
		//Campaigns for banner
		$this->load->model('affiliate/campaign');
		$this->data['campaigns'] = $this->model_affiliate_campaign->getCampaigns();
		
		// Country
		$this->load->model('localisation/country');

		$this->data['countries'] = $this->model_localisation_country->getCountries();
		
		// Categories
		$datas = '';
		$this->load->model('catalog/category');
		$this->data['categories'] = $this->model_catalog_category->getCategories($datas);
		
		//$this->data['header'] = $this->load->controller('common/header');
		//$this->data['column_left'] = $this->load->controller('common/column_left');
		//$this->data['footer'] = $this->load->controller('common/footer');

		//$this->response->setOutput($this->load->view('affiliate/banner_form.tpl', $data));
		
		$this->template = 'affiliate/banner_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
		$this->response->setOutput($this->render());
		
	}
	
	protected function validateForm() {
		
		if (!$this->user->hasPermission('modify', 'affiliate/banner')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen(trim($this->request->post['bn_name'])) < 2) || (utf8_strlen(trim($this->request->post['bn_name'])) > 255)) {
			$this->error['bn_name'] = $this->language->get('error_bn_name');
		}

		if ((utf8_strlen(trim($this->request->post['bn_des'])) < 10) || (utf8_strlen(trim($this->request->post['bn_des'])) > 255)) {
			$this->error['bn_des'] = $this->language->get('error_bn_des');
		}
		
		if ($this->request->post['bn_camid'] == '') {
			$this->error['bn_camid'] = $this->language->get('error_bn_camid');
		}
		
		if ($this->request->post['bn_img'] == '') {
			$this->error['bn_img'] = $this->language->get('error_bn_img');
		}
		
		if ($this->request->post['bn_size'] == '') {
			$this->error['bn_size'] = $this->language->get('error_bn_size');
		}

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		return !$this->error;
	}
	
	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'affiliate/banner')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}