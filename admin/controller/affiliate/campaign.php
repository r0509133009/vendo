<?php
class ControllerAffiliateCampaign extends Controller {
	private $error = array();
	public function index()
	{
		$this->load->language('affiliate/campaign');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('affiliate/campaign');
		$this->getList();
	}
	
	public function add() {
		$this->load->language('affiliate/campaign');

		$this->load->model('affiliate/campaign');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_affiliate_campaign->addCampaign($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_campaign_name'])) {
				$url .= '&filter_campaign_name=' . urlencode(html_entity_decode($this->request->get['filter_campaign_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_campaign_status'])) {
				$url .= '&filter_campaign_status=' . $this->request->get['filter_campaign_status'];
			}

			if (isset($this->request->get['filter_campaign_create'])) {
				$url .= '&filter_campaign_create=' . $this->request->get['filter_campaign_create'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('affiliate/campaign&token=' . $this->session->data['token'] . $url));
		}

		$this->getForm();
	}
	
	public function edit() {
		$this->load->language('affiliate/campaign');

		$this->load->model('affiliate/campaign');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_affiliate_campaign->editCampaign($this->request->get['campaign_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_campaign_name'])) {
				$url .= '&filter_campaign_name=' . urlencode(html_entity_decode($this->request->get['filter_campaign_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_campaign_create'])) {
				$url .= '&filter_campaign_create=' . urlencode(html_entity_decode($this->request->get['filter_campaign_create'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_campaign_status'])) {
				$url .= '&filter_campaign_status=' . $this->request->get['filter_campaign_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('affiliate/campaign&token=' . $this->session->data['token'] . $url));
		}

		$this->getForm();
	}
	
	public function delete() {
		$this->load->language('affiliate/campaign');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('affiliate/campaign');
		

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $campaign_id) {
				$this->model_affiliate_campaign->deleteCampaign($campaign_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_campaign_name'])) {
				$url .= '&filter_campaign_name=' . urlencode(html_entity_decode($this->request->get['filter_campaign_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_campaign_create'])) {
				$url .= '&filter_campaign_create=' . urlencode(html_entity_decode($this->request->get['filter_campaign_create'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_campaign_status'])) {
				$url .= '&filter_campaign_status=' . $this->request->get['filter_campaign_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('affiliate/campaign&token=' . $this->session->data['token'] . $url));
		}

		$this->getList();
	}
	
	protected function getList() {
		
		if (isset($this->request->get['filter_campaign_name'])) {
			$filter_campaign_name = $this->request->get['filter_campaign_name'];
		} else {
			$filter_campaign_name = null;
		}
		
		if (isset($this->request->get['filter_campaign_cateid'])) {
			$filter_campaign_cateid = $this->request->get['filter_campaign_cateid'];
		} else {
			$filter_campaign_cateid = null;
		}
		
		if (isset($this->request->get['filter_campaign_geo'])) {
			$filter_campaign_geo = $this->request->get['filter_campaign_geo'];
		} else {
			$filter_campaign_geo = null;
		}
		
		if (isset($this->request->get['filter_campaign_create'])) {
			$filter_campaign_create = $this->request->get['filter_campaign_create'];
		} else {
			$filter_campaign_create = null;
		}
		
		if (isset($this->request->get['filter_campaign_status'])) {
			$filter_campaign_status = $this->request->get['filter_campaign_status'];
		} else {
			$filter_campaign_status = null;
		}
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'campaign_name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_campaign_name'])) {
			$url .= '&filter_campaign_name=' . urlencode(html_entity_decode($this->request->get['filter_campaign_name'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_campaign_cateid'])) {
			$url .= '&filter_campaign_cateid=' . urlencode(html_entity_decode($this->request->get['filter_campaign_cateid'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_campaign_geo'])) {
			$url .= '&filter_campaign_geo=' . urlencode(html_entity_decode($this->request->get['filter_campaign_geo'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_campaign_create'])) {
			$url .= '&filter_campaign_create=' . urlencode(html_entity_decode($this->request->get['filter_campaign_create'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_campaign_status'])) {
			$url .= '&filter_campaign_status=' . urlencode(html_entity_decode($this->request->get['filter_campaign_status'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', 'token=' . $this->session->data['token']),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('affiliate/campaign&token=' . $this->session->data['token'] . $url),
			'separator' => ' :: '
		);
		
		if(isset($_GET['limit'])){
			if($_GET['limit'] < 10){
				$limit = 10;
			} else {
				$limit = $_GET['limit'];
			}
		} else {
			$limit = 10;
		}
		
		//$this->data['approve'] = $this->url->link('affiliate/campaign/approve', 'token=' . $this->session->data['token'] . $url);
		$this->data['add'] = $this->url->link('affiliate/campaign/add&token=' . $this->session->data['token'] . $url);
		$this->data['delete'] = $this->url->link('affiliate/campaign/delete&token=' . $this->session->data['token'] . $url);
		
		$this->data['campaigns'] = array();

		$filter_data = array(
			'filter_campaign_cateid'   	=> $filter_campaign_cateid,
			'filter_campaign_name'      => $filter_campaign_name,
			'filter_campaign_geo'     	=> $filter_campaign_geo,
			'filter_campaign_create'   	=> $filter_campaign_create,
			'filter_campaign_status' 	=> $filter_campaign_status,
			'sort'              => $sort,
			'order'             => $order,
			'start'             => ($page - 1) * $limit,
			'limit'             => $limit
		);
		
		$this->data['campaign_total'] = $this->model_affiliate_campaign->getTotalCampaigns($filter_data);
		$this->data['results'] = $this->model_affiliate_campaign->getCampaigns($filter_data);
		//Data campaign

		foreach ($this->data['results'] as $result) {
			$this->data['campaigns'][] = array(
				'campaign_id' 		=> $result['campaign_id'],
				'campaign_name'		=> $result['campaign_name'],
				'campaign_cateid'	=> $result['campaign_cateid'],
				'campaign_des'		=> $result['campaign_des'],
				'campaign_geo'		=> $result['campaign_geo'],
				'campaign_create'   => date($this->language->get('date_format_short'), strtotime($result['campaign_create'])),
				'campaign_status'	=> ($result['campaign_status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
				'cate_name' => $this->data['name'] = $this->model_affiliate_campaign->getCampaignsCategory($result['campaign_cateid']),
				'country_name' => $this->data['name'] = $this->model_affiliate_campaign->getCampaignsCountry($result['campaign_geo']),
				'edit'         => $this->url->link('affiliate/campaign/edit&token=' . $this->session->data['token'] . '&campaign_id=' . $result['campaign_id'] . $url)
			);
		}
		
		//Get_text
		$this->data['heading_title'] 		= $this->language->get('heading_title');
		$this->data['text_list'] = $this->language->get('text_list');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_yes'] = $this->language->get('text_yes');
		$this->data['text_no'] = $this->language->get('text_no');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_confirm'] = $this->language->get('text_confirm');
		$this->data['text_none'] = $this->language->get('text_none');
		
		$this->data['button_approve'] = $this->language->get('button_approve');
		$this->data['button_add'] = $this->language->get('button_add');
		$this->data['button_edit'] = $this->language->get('button_edit');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');
		
		$this->data['text_action'] = $this->language->get('text_action');
		$this->data['text_category'] = $this->language->get('text_category');
		$this->data['text_campaign_name'] = $this->language->get('text_campaign_name');
		$this->data['text_name'] = $this->language->get('text_name');
		$this->data['text_description'] = $this->language->get('text_description');
		$this->data['text_country'] = $this->language->get('text_country');
		$this->data['text_date_create'] = $this->language->get('text_date_create');
		$this->data['text_status'] = $this->language->get('text_status');
		$this->data['text_no_campaign'] = $this->language->get('text_no_campaign');
		
		$this->data['text_error_not_none'] = $this->language->get('text_error_not_none');
		$this->data['text_campaign_des'] = $this->language->get('text_campaign_des');
		$this->data['text_select_category'] = $this->language->get('text_select_category');
		$this->data['text_select_country'] = $this->language->get('text_select_country');
		
		$this->data['token'] = $this->session->data['token'];
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$this->data['selected'] = (array)$this->request->post['selected'];
		} else {
			$this->data['selected'] = array();
		}
		
		$url = '';

		if (isset($this->request->get['filter_campaign_name'])) {
			$url .= '&filter_campaign_name=' . urlencode(html_entity_decode($this->request->get['filter_campaign_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_campaign_status'])) {
			$url .= '&filter_campaign_status=' . $this->request->get['filter_campaign_status'];
		}

		if (isset($this->request->get['filter_campaign_create'])) {
			$url .= '&filter_campaign_create=' . $this->request->get['filter_campaign_create'];
		}
		
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$this->data['sort_name'] = $this->url->link('affiliate/campaign&token=' . $this->session->data['token'] . '&sort=name' . $url);
		
		$this->data['sort_date_added'] = $this->url->link('affiliate/campaign&token=' . $this->session->data['token'] . '&sort=a.campaign_create' . $url);

		$url = '';
		
		if (isset($this->request->get['filter_campaign_name'])) {
			$url .= '&filter_campaign_name=' . urlencode(html_entity_decode($this->request->get['filter_campaign_name'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_campaign_status'])) {
			$url .= '&filter_campaign_status=' . $this->request->get['filter_campaign_status'];
		}
		
		if (isset($this->request->get['filter_campaign_create'])) {
			$url .= '&filter_campaign_create=' . $this->request->get['filter_campaign_create'];
		}
		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		$pagination = new Pagination();
		$pagination->total = $this->data['campaign_total'];
		$pagination->page = $page;
		$pagination->limit = $limit;
		$pagination->url = $this->url->link('affiliate/campaign&token=' . $this->session->data['token'] . $url . '&page={page}');

		$this->data['pagination'] = $pagination->render();

		$this->data['results'] = sprintf($this->language->get('text_pagination'), ($this->data['campaign_total']) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($this->data['campaign_total'] - $limit)) ? $this->data['campaign_total'] : ((($page - 1) * $limit) + $limit), $this->data['campaign_total'], ceil($this->data['campaign_total'] / $limit));
		
		$this->data['filter_campaign_name'] 	= $filter_campaign_name;
		$this->data['filter_campaign_cateid'] = $filter_campaign_cateid;
		$this->data['filter_campaign_geo'] 	= $filter_campaign_geo;
		$this->data['filter_campaign_create'] = $filter_campaign_create;
		$this->data['filter_campaign_status'] = $filter_campaign_status;
		

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		//$this->data['header'] = $this->load->controller('common/header');
		//$this->data['column_left'] = $this->load->controller('common/column_left');
		//$this->data['footer'] = $this->load->controller('common/footer');

		//$this->response->setOutput($this->load->view('affiliate/campaign_list.tpl', $data));
		
		$this->template = 'affiliate/campaign_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	
	protected function getForm() {
		$this->document->setTitle($this->language->get('heading_title_add'));
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_form'] = !isset($this->request->get['campaign_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->error['campaign_name'])) {
			$this->data['error_campaign_name'] = $this->error['campaign_name'];
		} else {
			$this->data['error_campaign_name'] = '';
		}
		
		if (isset($this->error['campaign_des'])) {
			$this->data['error_campaign_des'] = $this->error['campaign_des'];
		} else {
			$this->data['error_campaign_des'] = '';
		}
		
		if (isset($this->error['campaign_geo'])) {
			$this->data['error_campaign_geo'] = $this->error['campaign_geo'];
		} else {
			$this->data['error_campaign_geo'] = '';
		}
		
		$url = '';
		
		if (isset($this->request->get['filter_campaign_name'])) {
			$url .= '&filter_campaign_name=' . urlencode(html_entity_decode($this->request->get['filter_campaign_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_campaign_status'])) {
			$url .= '&filter_campaign_status=' . $this->request->get['filter_campaign_status'];
		}

		if (isset($this->request->get['filter_campaign_create'])) {
			$url .= '&filter_campaign_create=' . $this->request->get['filter_campaign_create'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', 'token=' . $this->session->data['token']),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('affiliate/campaign&token=' . $this->session->data['token'] . $url),
			'separator' => ' :: '
		);
		
		if (!isset($this->request->get['campaign_id'])) {
			$this->data['action'] = $this->url->link('affiliate/campaign/add&token=' . $this->session->data['token'] . $url);
		} else {
			$this->data['action'] = $this->url->link('affiliate/campaign/edit&&token=' . $this->session->data['token'] . '&campaign_id=' . $this->request->get['campaign_id'] . $url);
		}

		$this->data['cancel'] = $this->url->link('affiliate/campaign&token=' . $this->session->data['token'] . $url);

		if (isset($this->request->get['campaign_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$campaign_info = $this->model_affiliate_campaign->getCampaign($this->request->get['campaign_id']);
		}

		//Get_text
		$this->data['heading_title'] 		= $this->language->get('heading_title');
		$this->data['text_list'] = $this->language->get('text_list');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_yes'] = $this->language->get('text_yes');
		$this->data['text_no'] = $this->language->get('text_no');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_confirm'] = $this->language->get('text_confirm');
		$this->data['text_none'] = $this->language->get('text_none');
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		
		$this->data['button_approve'] = $this->language->get('button_approve');
		$this->data['button_add'] = $this->language->get('button_add');
		$this->data['button_edit'] = $this->language->get('button_edit');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');
		
		$this->data['text_action'] = $this->language->get('text_action');
		$this->data['text_category'] = $this->language->get('text_category');
		$this->data['text_campaign_name'] = $this->language->get('text_campaign_name');
		$this->data['text_name'] = $this->language->get('text_name');
		$this->data['text_description'] = $this->language->get('text_description');
		$this->data['text_country'] = $this->language->get('text_country');
		$this->data['text_date_create'] = $this->language->get('text_date_create');
		$this->data['text_status'] = $this->language->get('text_status');
		$this->data['text_no_campaign'] = $this->language->get('text_no_campaign');
		
		$this->data['text_error_not_none'] = $this->language->get('text_error_not_none');
		$this->data['text_campaign_des'] = $this->language->get('text_campaign_des');
		$this->data['text_select_category'] = $this->language->get('text_select_category');
		$this->data['text_select_country'] = $this->language->get('text_select_country');
		
		$this->data['token'] = $this->session->data['token'];
		
		if (isset($this->request->get['campaign_id'])) {
			$this->data['campaign_id'] = $this->request->get['campaign_id'];
		} else {
			$this->data['campaign_id'] = 0;
		}
		
		if (isset($this->request->post['campaign_name'])) {
			$this->data['campaign_name'] = $this->request->post['campaign_name'];
		} elseif (!empty($campaign_info)) {
			$this->data['campaign_name'] = $campaign_info['campaign_name'];
		} else {
			$this->data['campaign_name'] = '';
		}
		
		if (isset($this->request->post['campaign_cateid'])) {
			$this->data['campaign_cateid'] = $this->request->post['campaign_cateid'];
		} elseif (!empty($campaign_info)) {
			$this->data['campaign_cateid'] = $campaign_info['campaign_cateid'];
		} else {
			$this->data['campaign_cateid'] = '';
		}
		
		if (isset($this->request->post['campaign_des'])) {
			$this->data['campaign_des'] = $this->request->post['campaign_des'];
		} elseif (!empty($campaign_info)) {
			$this->data['campaign_des'] = $campaign_info['campaign_des'];
		} else {
			$this->data['campaign_des'] = '';
		}
		
		if (isset($this->request->post['campaign_geo'])) {
			$this->data['campaign_geo'] = $this->request->post['campaign_geo'];
		} elseif (!empty($campaign_info)) {
			$this->data['campaign_geo'] = $campaign_info['campaign_geo'];
		} else {
			$this->data['campaign_geo'] = '';
		}
		
		if (isset($this->request->post['campaign_status'])) {
			$this->data['campaign_status'] = $this->request->post['campaign_status'];
		} elseif (!empty($campaign_info)) {
			$this->data['campaign_status'] = $campaign_info['campaign_status'];
		} else {
			$this->data['campaign_status'] = true;
		}
		
		// Country
		$this->load->model('localisation/country');

		$this->data['countries'] = $this->model_localisation_country->getCountries();
		
		// Categories
		$datas = '';
		$this->load->model('catalog/category');
		$this->data['categories'] = $this->model_catalog_category->getCategories($datas);
		
		if (isset($this->request->post['campaign_status'])) {
			$this->data['campaign_status'] = $this->request->post['campaign_status'];
		} elseif (!empty($campaign_info)) {
			$this->data['campaign_status'] = $campaign_info['campaign_status'];
		} else {
			$this->data['campaign_status'] = true;
		}
		
		if (isset($this->request->post['campaign_create'])) {
			$this->data['campaign_create'] = $this->request->post['campaign_create'];
		} elseif (!empty($campaign_info)) {
			$this->data['campaign_create'] = $campaign_info['campaign_create'];
		} else {
			$this->data['campaign_create'] = true;
		}
		
		if (isset($this->request->post['confirm'])) {
			$this->data['confirm'] = $this->request->post['confirm'];
		} else {
			$this->data['confirm'] = '';
		}
		
		//$this->data['header'] = $this->load->controller('common/header');
		//$this->data['column_left'] = $this->load->controller('common/column_left');
		//$this->data['footer'] = $this->load->controller('common/footer');

		//$this->response->setOutput($this->load->view('affiliate/campaign_form.tpl', $data));
		
		$this->template = 'affiliate/campaign_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
		$this->response->setOutput($this->render());
		
	}
	
	protected function validateForm() {

		if (!$this->user->hasPermission('modify', 'affiliate/campaign')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen(trim($this->request->post['campaign_name'])) < 2) || (utf8_strlen(trim($this->request->post['campaign_name'])) > 255)) {
			$this->error['campaign_name'] = $this->language->get('error_campaign_name');
		}

		if ((utf8_strlen(trim($this->request->post['campaign_des'])) < 2) || (utf8_strlen(trim($this->request->post['campaign_des'])) > 255)) {
			$this->error['campaign_des'] = $this->language->get('error_campaign_des');
		}
		
		if ($this->request->post['campaign_geo'] == '') {
			$this->error['campaign_geo'] = $this->language->get('error_campaign_geo');
		}

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		return !$this->error;
	}
	
	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'affiliate/campaign')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	public function country() {
		$json = array();

		$this->load->model('localisation/country');

		$country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);


		if ($country_info) {
			$this->load->model('localisation/zone');

			$json = array(
				'country_id'        => $country_info['country_id'],
				'name'              => $country_info['name'],
				'iso_code_2'        => $country_info['iso_code_2'],
				'iso_code_3'        => $country_info['iso_code_3'],
				'address_format'    => $country_info['address_format'],
				'postcode_required' => $country_info['postcode_required'],
				'zone'              => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),
				'status'            => $country_info['status']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function autocomplete() {
		$affiliate_data = array();

		if (isset($this->request->get['filter_campaign_name'])) {
			if (isset($this->request->get['filter_campaign_name'])) {
				$filter_campaign_name = $this->request->get['filter_campaign_name'];
			} else {
				$filter_campaign_name = '';
			}

			if (isset($this->request->get['filter_campaign_cateid'])) {
				$filter_campaign_cateid = $this->request->get['filter_campaign_cateid'];
			} else {
				$filter_campaign_cateid = '';
			}

			$this->load->model('affiliate/campaign');

			$filter_data = array(
				'filter_campaign_name'  => $filter_campaign_name,
				'filter_campaign_cateid' => $filter_campaign_cateid,
				'start'        => 0,
				'limit'        => 10
			);

			$results = $this->model_affiliate_campaign->getCampaigns($filter_data);

			foreach ($results as $result) {
				$campaign_data[] = array(
					'campaign_id' 	=> $result['campaign_id'],
					'campaign_name'	=> strip_tags(html_entity_decode($result['campaign_name'], ENT_QUOTES, 'UTF-8')),
					'campaign_cateid'        => $result['campaign_cateid']
				);
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($campaign_data));
	}
}