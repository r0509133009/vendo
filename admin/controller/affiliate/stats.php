<?php
class ControllerAffiliateStats extends Controller {
	public function index()
	{
		$this->load->language('affiliate/stats');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('affiliate/stats');
		
		$url = '';
		
		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('affiliate/stats&token=' . $this->session->data['token'] . $url),
      		'separator' => ' :: '
   		);
		
		//Get_text
		$this->data['heading_title'] 		= $this->language->get('heading_title');
		$this->data['text_list'] = $this->language->get('text_list');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_yes'] = $this->language->get('text_yes');
		$this->data['text_no'] = $this->language->get('text_no');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_confirm'] = $this->language->get('text_confirm');
		$this->data['text_none'] = $this->language->get('text_none');
		
		$this->data['button_approve'] = $this->language->get('button_approve');
		$this->data['button_add'] = $this->language->get('button_add');
		$this->data['button_edit'] = $this->language->get('button_edit');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');
		
		$this->data['text_last_10day'] = $this->language->get('text_last_10day');
		$this->data['text_impression'] = $this->language->get('text_impression');
		$this->data['text_clicks'] = $this->language->get('text_clicks');
		$this->data['text_last_day_month'] = $this->language->get('text_last_day_month');
		
		$this->data['text_date'] = $this->language->get('text_date');
		$this->data['text_click'] = $this->language->get('text_click');
		$this->data['text_sale'] = $this->language->get('text_sale');
		$this->data['text_amount'] = $this->language->get('text_amount');
		$this->data['text_ctr'] = $this->language->get('text_ctr');
		$this->data['text_cr'] = $this->language->get('text_cr');
		$this->data['text_event'] = $this->language->get('text_event');
		$this->data['text_email'] = $this->language->get('text_email');
		$this->data['text_description'] = $this->language->get('text_description');
		$this->data['text_no_data'] = $this->language->get('text_no_data');
		$this->data['text_total'] = $this->language->get('text_total');
		
		$this->data['token'] = $this->session->data['token'];
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$this->data['selected'] = (array)$this->request->post['selected'];
		} else {
			$this->data['selected'] = array();
		}
		
		$url = '';
		
		if(isset($_GET['month'])){
			$this->data['smonth'] = $_GET['month'];
		} else {
			$this->data['smonth'] = date('m/Y');
		}
		
		$this->data['stats_resut'] = $this->model_affiliate_stats->getStats($this->data['smonth']);
		foreach ($this->data['stats_resut'] as $stats) {
			$this->data['stats'][] = array(
				'statsd_id' 	=> $stats['statsd_id'],
				'statsd_views'	=> $stats['statsd_views'],
				'statsd_click'	=> $stats['statsd_click'],
				'total_sale'	=> $this->model_affiliate_stats->getTotalSales(date('Y-m-d', strtotime($stats['statsd_date']))),
				'total_amount'	=> $this->model_affiliate_stats->getTotalSalesAmount(date('Y-m-d', strtotime($stats['statsd_date']))),
				'statsd_date'   => date($this->language->get('date_format_short'), strtotime($stats['statsd_date'])),
			);
		}
		
		$this->data['trans_resut'] = $this->model_affiliate_stats->getTransactions();
		foreach ($this->data['trans_resut'] as $trans) {
			$this->data['trans'][] = array(
				'affiliate_transaction_id' 	=> $trans['affiliate_transaction_id'],
				'affiliate_id'	=> $trans['affiliate_id'],
				'order_id'	=> $trans['order_id'],
				'description'	=> $trans['description'],
				'amount' => $trans['amount'],
				'email' => $data['email'] = $this->model_affiliate_stats->getAffiliateEmail($trans['affiliate_id']),
				'link_account' => $this->url->link('affiliate/member/edit', 'token=' . $this->session->data['token'] . '&affiliate_id=' . $trans['affiliate_id'] . $url, 'SSL'),
				'date_added'   => date($this->language->get('date_format_short'), strtotime($trans['date_added'])),
			);
		}
		
		$this->data['groupmonths'] = $this->model_affiliate_stats->getStatsMonth();
		foreach ($this->data['groupmonths'] as $group_month) {
			$this->data['group_months'][] = array(
				'statsd_month' => $group_month['statsd_month'],
			);
		}
		
		$this->data['ChartStats'] = $this->model_affiliate_stats->getChartStats();
		$category = array();
		$category['name'] = 'Date';

		$seriesStats1 = array();
		$seriesStats1['name'] = 'Click';
		$seriesStats2['name'] = 'Impression';
		
		foreach ($this->data['ChartStats'] as $ChartStat) {
			$aa[] = str_replace(date('/Y'), '', @$ChartStat['statsd_date']);
			$bb[] = @$ChartStat['statsd_click'];
			$cc[] = @$ChartStat['statsd_views'];
		}
		if(isset($aa)){
		$category['data'] 		= @array_reverse(@$aa);
		$seriesStats1['data'] 	= @array_reverse(@$bb);
		$seriesStats2['data'] 	= @array_reverse(@$cc);
		$this->data['category'] = json_encode($category['data'], JSON_NUMERIC_CHECK);
		$this->data['series'] = json_encode($seriesStats1['data'], JSON_NUMERIC_CHECK);
		$this->data['series1'] = json_encode($seriesStats2['data'], JSON_NUMERIC_CHECK);
		}
		
		//$data['header'] = $this->load->controller('common/header');
		//$data['column_left'] = $this->load->controller('common/column_left');
		//$data['footer'] = $this->load->controller('common/footer');

		//$this->response->setOutput($this->load->view('affiliate/stats.tpl', $data));
		$this->template = 'affiliate/stats.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
}