<?php
class ModelCatalogSeoReport extends Model {
	
	public function getreport1() {
		$products = $this->getProducts();
		$countm = count($products);
		$countpd = 0;
		$countpk = 0;
		$countsk = 0;
		foreach ($products as $key => $value) {

			if($this->checkKeyword("product_id",$value['product_id'])) {
				$countsk += 1; 
			}
			if($value['meta_description'] == '') {
				$countpd += 1; 
			}
			if($value['meta_keyword'] == '') {
				$countpk += 1; 
			}
		}

		$array = array('count' => $countm,'metad' => $countpd,'metak' => $countpk,'seok' => $countsk);
		return $array;
	}

	public function getreport2() {
		$categories = $this->getCategories();
		$countm = count($categories);
		$countpd = 0;
		$countpk = 0;
		$countsk = 0;
		foreach ($categories as $key => $value) {

			if($this->checkKeyword("category_id",$value['category_id'])) {
				$countsk += 1; 
			}

			if($value['meta_description'] == '') {
				$countpd += 1; 
			}
			if($value['meta_keyword'] == '') {
				$countpk += 1; 
			}
		}

		$array = array('count' => $countm,'metad' => $countpd,'metak' => $countpk,'seok' => $countsk);
		return $array;
	}

	public function getreport3() {
		$informations = $this->getInformationPages();
		$countm = count($informations);
		$countpd = 0;
		$countpk = 0;
		$countsk = 0;
		foreach ($informations as $key => $value) {

			if($this->checkKeyword("information_id",$value['information_id'])) {
				$countsk += 1; 
			}

			$value1 = $this->getInfo($value['information_id']);

			if($value1->num_rows == 0) {
					$countpd += 1; 
					$countpk += 1; 
			} else {
				if($value1->row['meta_keywords'] == '') {
					$countpk += 1; 
				}
				if($value1->row['meta_description'] == '') {
					$countpd += 1; 
				}
			}
		}

		$array = array('count' => $countm,'metad' => $countpd,'metak' => $countpk,'seok' => $countsk);
		return $array;
	}

	public function getreport4() {
		$manufacturers = $this->getManufacturers();
		$countm = count($manufacturers);
		$countpd = 0;
		$countpk = 0;
		$countsk = 0;
		foreach ($manufacturers as $key => $value) {

			if($this->checkKeywordm("manufacturer_id",$value['manufacturer_id'])) {
				$countsk += 1; 
			}

			$value1 = $this->getManu($value['manufacturer_id']);

			if($value1->num_rows == 0) {
					$countpd += 1; 
					$countpk += 1; 
			} else {
				if($value1->row['meta_keywords'] == '') {
					$countpk += 1; 
				}
				if($value1->row['meta_description'] == '') {
					$countpd += 1; 
				}
			}
		}

		$array = array('count' => $countm,'metad' => $countpd,'metak' => $countpk,'seok' => $countsk);
		return $array;
	}

	private function getProducts() {
		$query = $this->db->query("SELECT p.product_id,pd.meta_description,pd.meta_keyword  FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON(p.product_id=p2s.product_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY pd.name ASC");
		return $query->rows;
	}

	private function getCategories() {
		$query = $this->db->query("SELECT c.category_id,  cd.meta_description, cd.meta_keyword FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id=c2s.category_id) WHERE cd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY c.sort_order, cd.name ASC");
		return $query->rows;
	}

	private function getInfo($id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_data WHERE type = 'information' AND id = '" . (int)$id . "' AND `language_id` = '". (int)$this->config->get('config_language_id') ."' ");    
		return $query;
	}

	private function getManu($id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_data WHERE type = 'manufacturer' AND id = '" . (int)$id . "' AND `language_id` = '". (int)$this->config->get('config_language_id') ."' ");    
		return $query;
	}

	private function checkKeyword($name, $id) {
		$query = $this->db->query("SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = '".$name."=".$id."' AND `lang` = '". (int)$this->config->get('config_language_id') ."' ");    
		if($query->num_rows){
			return 0;
		} else {
			return 1;
		}
	}

	private function checkKeywordm($name, $id) {
		$query = $this->db->query("SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = '".$name."=".$id."'");    
		if($query->num_rows){
			return 0;
		} else {
			return 1;
		}
	}

	private function getInformationPages(){
		$query = $this->db->query("SELECT id.information_id FROM " . DB_PREFIX . "information_description id LEFT JOIN " . DB_PREFIX . "information i ON (id.information_id = i.information_id)  LEFT JOIN " . DB_PREFIX . "information_to_store its ON (id.information_id = its.information_id) WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY id.title ASC");
		return $query->rows;
	}

	private function getManufacturers() {
		$query = $this->db->query("SELECT m.manufacturer_id, m.name FROM " . DB_PREFIX . "manufacturer m LEFT JOIN " . DB_PREFIX . "manufacturer_to_store m2s ON (m.manufacturer_id=m2s.manufacturer_id) ORDER BY m.name ASC");
		return $query->rows;
	}

}
?>