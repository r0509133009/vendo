<?php
class ModelPaymentOfflinepay extends Model {

	public function install()
  	{
  		$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "offlinepay` (
			  `offlinepay_id` int(11) NOT NULL AUTO_INCREMENT,
			  `order_id` int(11) NOT NULL,
			  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
			  `payment_date` TIMESTAMP NOT NULL,
			  `transaction_id` VARCHAR(30) NOT NULL UNIQUE,
			  `total` DECIMAL( 10, 2 ) NOT NULL,
			  PRIMARY KEY (`offlinepay_id`)
			) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci;");
  	}
}