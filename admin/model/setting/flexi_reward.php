<?php
// ADMIN 15
class ModelSettingFlexiReward extends Model {
	public $flexi_config = array();
	public $flexi_cache = array();
	public $reli = array( 'productreview'=> -3000000,
				      'firstorder'=>-4000000
					  	);

	
    public function addReward($order_id) {  
        $this->language->load('sale/order');
        $this->load->model('sale/order');
		
		$order_info = $this->model_sale_order->getOrder($order_id);
	//	$order_products = $this->model_sale_order->getOrderProducts($order_id);

	    $reward_points = 0;
		
        if ($this->flexi_config['flexi_reward_status']) {
            $this->language->load('module/flexi_reward');
			
            //Checking for already added reward points
            if ($this->checkForAlreadyAddedRewards($order_id, $this->flexi_config['flexi_reward_order_status'],$order_info['customer_id'])) {
                return $this->language->get('error_reward_added');
            }
			
			// Add Rewards for first order
			$this->addRewardFirstOrder($order_info['customer_id'], $order_info['customer_group_id'],$order_id) ;
			
			// Points to add
			$reward_points = $this->calcReward($order_id);
        }
		
        if ($reward_points != 0) {
	        $this->load->model('sale/customer');
		    $this->model_sale_customer->addReward($order_info['customer_id'], $this->language->get('text_order_id') . ' #' . $order_id, $reward_points, $order_id);
            return $this->language->get('text_reward_success');
        } else {
            $this->language->load('sale/order');
            return $this->language->get('text_success');
        }
    }

	// Calculate and return
	public function calcReward($order_id) { 
			global $flexi_cache;
		// Cache all current calls	
		// 4 calls we can cache them to 1... we still have to track them down why they are 4??
	/*	if(!isset($flexi_cache['order_rewards'][$order_id]) ) {
			$flexi_cache['order_rewards'][$order_id] = false;
		}elseif($flexi_cache['order_rewards'][$order_id] !== false) {
			return $flexi_cache['order_rewards'][$order_id];
		}*/
		// var_dump('Processing call--------------');
	// 	$order_info = $this->model_sale_order->getOrder($order_id); // Infinite loop
	 	$order_query = $this->db->query("SELECT customer_id, customer_group_id FROM `" . DB_PREFIX . "order` o  WHERE o.order_id = '" . (int)$order_id . "'");
		$customer_id = $order_query->row['customer_id'];
		$customer_group_id = $order_query->row['customer_group_id'];
		$flexi_sett = $this->config->get('flexi_reward_'.$customer_group_id);
      
	    if (!$flexi_sett['flexi_reward_status']) { 
		   return false;
		}
	
	 	$order_products = $this->getOrderProducts($order_id);
	    $rates = $flexi_sett['flexi_reward_rate'];
		// var_dump('$rates',$rates);
	    $reward_points = 0;
   		$reward_points_cur = 0;
		
        if ($flexi_sett['flexi_reward_status']) {
//            $this->language->load('module/flexi_reward');

            $order_total_info = $this->getOrderTotals($order_id);
            $order_sub_total = 0;
            $order_coupon_amount = 0;
            $order_voucher_amount = 0;
			$order_reward  = 0;
            foreach ($order_total_info as $order_total) {
	//			// var_dump('$order_total',$order_total);
                switch ($order_total['code']) {
                    case 'sub_total':
                        $order_sub_total = (float) $order_total['value'];
                        break;
                    case 'coupon':
                        $order_coupon_amount = (float) $order_total['value'];
                        break;
                    case 'voucher':
                        $order_voucher_amount = (float) $order_total['value'];
                        break;
                    case 'reward':
                        $order_reward = (float) $order_total['value'];
                        break;
                    case 'total':
                        $order_case_total = (float) $order_total['value'];
                        break;
						
                }
            }

		$cart_discounts =  $order_reward + $order_coupon_amount + $order_voucher_amount;
	
			// Manufacturer and category
			$this->load->model('catalog/product');
			
			$mfc_sett = isset($flexi_sett['flexi_manufacturers']) ? $flexi_sett['flexi_manufacturers']:array();
			$cat_sett = isset($flexi_sett['flexi_categories']) ? $flexi_sett['flexi_categories']:array();
			
			// get points for sub-total  and manufacturer / category 
			foreach( $order_products as $key=>$val) {
				$reward_points_cur = 0;
				$tmp_product_info = $this->model_catalog_product->getProduct($val['product_id']);
				$tmp_product_cats = $this->getProductCategories($val['product_id']) 	;
	
				$reward_points_mfc = 0;
				$reward_points_cat = 0;
				
				foreach($mfc_sett as $mkey=>$mvalue) {
					if(in_array($tmp_product_info['manufacturer_id'],$mvalue['ids']))	{ 
						// calc rewards for this manufacturer and add them
						$reward_points_mfc += $val['total'] * $mvalue['rate'];
					}
				}
				foreach($cat_sett as $ckey=>$cvalue) {
					if(array_intersect($cvalue['ids'], $tmp_product_cats)) {
						// calc rewards for this category and add them
						$reward_points_cat += $val['total'] * $cvalue['rate'];
						break; // to avoid multiple categories adding more points I know I can do it better ;)
					}
				}
					// var_dump('$reward_points_mfc',$reward_points_mfc);
					// var_dump('$reward_points_cat',$reward_points_cat);

				if ($reward_points_cat === 0 && $reward_points_mfc === 0 ){
					$reward_points_cur += $val['total'] * $rates;
					// var_dump('$val[total]', $val['total']);					
				}elseif($reward_points_cat === 0 && $reward_points_mfc > 0 ) {
					$reward_points_cur += $reward_points_mfc ;
				}elseif($reward_points_cat > 0 && $reward_points_mfc === 0 ) {
					$reward_points_cur += $reward_points_cat ;
				}elseif($reward_points_cat > 0 && $reward_points_mfc > 0 ) {
					$reward_points_cur += $this->theBigger($reward_points_cat, $reward_points_mfc);
				}

				// Ignore product rewards value
				// IMPORTANT NOTE: Options have points... but points for purchase not rewards?!!
				if(isset($flexi_sett['flexi_ignore_product_rewards_value']) && !$flexi_sett['flexi_ignore_product_rewards_value'] && $val['reward'] == 0 || $flexi_sett['flexi_ignore_product_rewards_value'] == 1) {
					$reward_points	+= $reward_points_cur;
					// var_dump('CASE ignore Adding reward points $reward_points',$reward_points);
					// var_dump('CASE ignore Adding reward points $reward_points_cur',$reward_points_cur);
					
				}elseif(isset($flexi_sett['flexi_ignore_product_rewards_value']) && !$flexi_sett['flexi_ignore_product_rewards_value'] && $val['reward'] > 0) {
					$reward_points	+= $val['reward'];
					// var_dump('CASE obay Adding reward points $reward_points',$reward_points);
					// var_dump('CASE obay Adding reward points $ $val[reward]', $val['reward']);
					
				}

			} //foreach $order_products 
        }

		// Redeem discount case where total is 42 and subtotal 60
		// Set Total if setting says so 
		if( $flexi_sett['flexi_total_subtotal_operator'] == 1){
			// Total
			$reward_points = $reward_points * ($order_case_total / $order_sub_total);
			// var_dump('flexi_total_subtotal_operator == 1 reward_points ', $reward_points);
  			// var_dump('flexi_total_subtotal_operator == 1 order_case_total', $order_case_total);
			// var_dump('flexi_total_subtotal_operator == 1 order_sub_total', $order_sub_total);
			
			
		}	else {
			// Sub-total - finalize reward points by handling discounts
			$reward_points += $cart_discounts * $rates; 
			// var_dump(' ELSE flexi_total_subtotal_operator == 1   $cart_discounts * $rates',  $cart_discounts * $rates);
			// var_dump(' ELSE flexi_total_subtotal_operator == 1   $reward_points',  $reward_points );
			
		}

		  $flexi_cache['order_rewards'][$order_id] = $reward_points;		
        if ($reward_points > 0) {
          return round($reward_points);
        } 
	return 0;
	}
	
	
	// Review
    public function addRewardReview($review_id = '', $product_id = NULL, $customer_id=false, $customer_group_id=false, $product_name ) {
		$flexi_sett = $this->config->get('flexi_reward_'.$customer_group_id);
		if($flexi_sett['flexi_productreview_points'] == '0') {
		   return false;
		}
		
		$this->language->load('module/flexi_reward');
		$customer_id	= (int)$customer_id;
		$order_id       = $this->reli['productreview'] - $product_id;
		
		if( $this->getTotalCustomerRewardsByOrderId($order_id, $customer_id) > 0){
		   return false;
		}
		
		$points 		= isset($flexi_sett['flexi_productreview_points']) ? $flexi_sett['flexi_productreview_points'] : 0 ;
		$description 	= isset($flexi_sett['flexi_productreview_desc']) ? $flexi_sett['flexi_productreview_desc'] : '' ;

		if(!$description)		$description 	= $this->language->get('text_flexi_productreview') ? $this->language->get('text_flexi_productreview') : 'product review' ;

        $this->load->model('sale/customer');
        $this->model_sale_customer->addReward($customer_id, $description . ' - '.$product_name, $flexi_sett['flexi_productreview_points'], $order_id);
		return true;
	}

	// First order
    public function addRewardFirstOrder($customer_id=false, $customer_group_id=false,$order_id=false) {
		$flexi_sett = $this->config->get('flexi_reward_'.$customer_group_id);

		if($flexi_sett['flexi_firstorder_points'] == '0') {
		   return false;
		}
		if( $this->getTotalCustomerRewardsByOrderId( ($this->reli['firstorder']), $customer_id ) > 0){
		   return false;
		}
////////////// Order amount
        $this->load->model('sale/order');
		$order_total_info = $this->model_sale_order->getOrderTotals($order_id);
		$order_sub_total = 0;
		$order_coupon_amount = 0;
		$order_case_total = 0;
		$order_voucher_amount = 0;
		foreach ($order_total_info as $order_total) {
//			// var_dump('$order_total',$order_total);
			switch ($order_total['code']) {
				case 'sub_total':
					$order_sub_total = $order_total['value'];
					break;
				case 'coupon':
					$order_coupon_amount =  $order_total['value'];
					break;
				case 'voucher':
					$order_voucher_amount =  $order_total['value'];
					break;
				case 'reward':
					$order_reward =  $order_total['value'];
					break;
				case 'total':
					$order_case_total =  $order_total['value'];
					break;
					
			}
		}
		//TODO:::  REWARD not included --- why?
		$cart_subtotal = $order_sub_total + $order_coupon_amount + $order_voucher_amount;
		$order_amount = $cart_subtotal;
		
		// Redeem discount case where total is 42 and subtotal 60
		// Set Total if setting says so 
		if($order_case_total < $cart_subtotal || $this->flexi_config['flexi_total_subtotal_operator'] == 1){
			$order_amount = $order_case_total;
		}

////////////// END order amount


		$this->language->load('module/flexi_reward');
		$description 	= isset($flexi_sett['flexi_firstorder_desc']) ? $flexi_sett['flexi_firstorder_desc'] : '' ;

		if(!$description){
			$description 	= $this->language->get('text_flexi_firstorder') ? $this->language->get('text_flexi_firstorder') : 'first order' ;
		}
		if($flexi_sett['flexi_firstorder_operator'] > 0) 	{
			$points = $flexi_sett['flexi_firstorder_points'];
		} else {
			// We assume we get final order_amount wheter total / sub-total from addReward.
			$points = round($order_amount * $flexi_sett['flexi_firstorder_points'] / 100.00);
		}
 		
		if($points < 1) return false;
		
        $this->load->model('sale/customer');
        $this->model_sale_customer->addReward($customer_id, $description . ' #'.$order_id, $points, ($this->reli['firstorder']));

		return true;
	}



  /*  public function checkForAlreadyAddedRewards($order_id, $flexi_reward_order_status,$customer_id) {
        $query = $this->db->query("SELECT COUNT(1) AS total_count FROM " . DB_PREFIX . "customer_reward cr WHERE cr.order_id = '" . (int) $order_id . "' AND cr.customer_id = '". (int) $customer_id . "'");
        if ($query->row['total_count'] > 0) {
            return true;
        } else {
            return false;
        }
    }
	
	public function getDaysSinceLastTransaction($customer_id){
		$query = $this->db->query("SELECT DATEDIFF(NOW(),date_added) AS days FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '". (int) $customer_id . "' ORDER BY date_added DESC LIMIT 0,1");
		return isset($query->row['days']) ? $query->row['days'] :0;
	}
	
	public function getRewardTotal($customer_id){
        $this->load->model('sale/customer');
		$total = $this->model_sale_customer->getRewardTotal($customer_id);
		return $total;
	}
	
	public function calcTotal($customer_id){	
		$total =  $this->getRewardTotal($customer_id);
		$days =	  $this->getDaysSinceLastTransaction($customer_id);
		$exp_sett = $this->flexi_config['flexi_exp_days'];
	
		$end_total = $total ;
		
		// SORT_DESC 
		if(!function_exists('sortByOrder')) {
			function sortByOrder($a, $b) {
				return $b['days'] - $a['days'];
			}
		}
		usort($exp_sett, 'sortByOrder');
 
 		foreach($exp_sett as $exp) {
			if($days > $exp['days'] ) {
				$end_total = round($exp['rate']*$total);
				break;
			}
		}
		
		return $end_total;
	}
	*/
	public function getCustomerGroupByCustomer($customer_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->row['customer_group_id'];
	}
	
	public function getCustomerGroupByOrder($order_id) {
		$order_query = $this->db->query("SELECT *, (SELECT CONCAT(c.firstname, ' ', c.lastname) FROM " . DB_PREFIX . "customer c WHERE c.customer_id = o.customer_id) AS customer FROM `" . DB_PREFIX . "order` o WHERE o.order_id = '" . (int)$order_id . "'");

		return $order_query->row['customer_group_id'];
	}
	
	public function getFlexi($id, $type='order') {
		if($type == 'order') {
			$customer_group_id = $this->getCustomerGroupByOrder($id);
		}elseif($type == 'customer') {
			$customer_group_id = $this->getCustomerGroupByCustomer($id);
		}
		
		$sett = $this->config->get('flexi_reward_'.$customer_group_id);
		$this->flexi_config = $sett;
		
		return $sett['flexi_reward_status'] == '1' ? $sett : $sett['flexi_reward_status'] ;
	}
/*	public function getTotalCustomerRewardsByOrderId($order_id, $customer_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_reward WHERE order_id = '" . (int)$order_id . "' AND customer_id = '" . (int) $customer_id. "'");

		return $query->row['total'];
	}*/
	public function getTotalOrdersByCustomerId($customer_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "order WHERE customer_id = '" . (int) $customer_id. "'");

		return $query->row['total'];
	}
	
	//// FRONT --------------- Copy
		public function getOrderProducts($order_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");
		
		return $query->rows;
	}
	
	public function getProductCategories($product_id) {
		$product_category_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");

		foreach ($query->rows as $result) {
			$product_category_data[] = $result['category_id'];
		}

		return $product_category_data;
	}
	
	public function getOrderTotals($order_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_total WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order");

		return $query->rows;
	}

    public function theBigger($a,$b) {
		if($a > $b) return $a;
		if($b > $a) return $b;
	}
	
	public function getDaysSinceLastTransaction($customer_id){
		$query = $this->db->query("SELECT DATEDIFF(NOW(),date_added) AS days FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '". (int) $customer_id . "' ORDER BY date_added DESC LIMIT 0,1");
		return $query->row['days'] ? $query->row['days'] :0;
	}
	
	public function getRewardTotal($customer_id) {
		$query = $this->db->query("SELECT SUM(points) AS total FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->row['total'];
	}		
	
	public function getTotalCustomerRewardsByOrderId($order_id, $customer_id=0) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_reward WHERE order_id = '" . (int)$order_id . "' AND customer_id = '" . (int) $customer_id . "'");

		return $query->row['total'];
	}
	
	public function calcTotal($customer_id){	
		$total =  $this->getRewardTotal($customer_id);
		// NK edit 29-3-2016   -- added return to prevent points expiration calculation. 
		return $total;
		
		$days =	  $this->getDaysSinceLastTransaction($customer_id);
		$exp_sett = $this->flexi_config['flexi_exp_days'];
		
	
		$end_total = $total ;
		
		// SORT_DESC 
		function sortByOrder($a, $b) {
  			return $b['days'] - $a['days'];
		}
		usort($exp_sett, 'sortByOrder');
 
 		foreach($exp_sett as $exp) {
			//// var_dump($exp);
			if($days > $exp['days'] ) {
				$end_total = round($exp['rate']*$total);
				break;
			}
		}
		
		return $end_total;
	}
	
    public function checkForAlreadyAddedRewards($order_id, $flexi_reward_order_status,$customer_id) {
        $query = $this->db->query("SELECT COUNT(1) AS total_count FROM " . DB_PREFIX . "customer_reward cr WHERE cr.order_id = '" . (int) $order_id . "' AND cr.customer_id = '". (int) $customer_id . "'");
        if ($query->row['total_count'] > 0) {
            return true;
        } else {
            return false;
        }
    }

	
	
}

?>