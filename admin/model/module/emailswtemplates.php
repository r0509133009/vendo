<?php
################################################################################################
#  Module Builder #
################################################################################################
class ModelModuleEmailswtemplates extends Model {

	// This function is how my blog module creates it's tables to store blog entries. You would call this function in your controller in a
	// function called install(). The install() function is called automatically by OC versions 1.4.9.x, and maybe 1.4.8.x when a module is
	// installed in admin.
	
	public function newsletter_count() {
	$query = $this->db->query("SELECT `email` FROM `".DB_PREFIX."customer` WHERE `newsletter` >0");
	//echo '<pre>';	print_r($query->rows );	echo '</pre>';
	return $query->num_rows;
	}
	
		
	public function ny_newsletter_count() {
	$this->check_dbSubscribers();
	$query = $this->db->query("SELECT * FROM `".DB_PREFIX."ny_subscribe` ");
	 // echo '<pre>';	print_r($query->num_rows );	echo '</pre>';
	return $query->num_rows;
	}
	
	public function UploadSubscriberFromFile($field) {
	    $this->check_dbSubscribers();
		$first_name = trim($field['0']);
		$last_name = trim($field['1']);
		$email = trim($field['2']);

		$k = $this->db->query("SELECT * FROM `".DB_PREFIX."ny_subscribe` WHERE `email_id` LIKE '".$email."'  ");
		if(isset( $k->num_rows) && $k->num_rows>0 ){
		   return false;
		}else{
			$query = $this->db->query("INSERT INTO `".DB_PREFIX."ny_subscribe`
					SET `email_id`='".$this->db->escape($email)."',
						`name`='".$this->db->escape( $first_name ) ."' ,
						`last_name`='".$this->db->escape( $last_name ) ."' 
						");
			return true; 
		}		
	}
	
	public function getny_newsletter_subscribers(){
	     $query = $this->db->query('SELECT `email_id` FROM `'.DB_PREFIX.'subscribe` ');	
		 foreach($query->rows as $val){
		   $emails[] = $val['email_id'] ;
		 }
       return $emails;			
	}
	
public function  checkSettingTableType(){
		$table_info = $this->db->query("SELECT COLUMN_TYPE FROM information_schema.COLUMNS WHERE TABLE_NAME = '".DB_PREFIX."setting' AND COLUMN_NAME = 'value' ");
		if( $table_info->row['COLUMN_TYPE']!=='longtext'){
			$this->db->query("ALTER TABLE `".DB_PREFIX."setting` CHANGE `value` `value` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;");
		}
}	
	
	
	public function getHistoryrecords($data,$start,$limit) {	
		$query = $this->db->query( "SELECT * FROM `".DB_PREFIX ."ny_mail_history` ORDER BY `id` DESC LIMIT $start,$limit" );	
	
		if(isset( $query->num_rows) && $query->num_rows>0 ){
		  return $query->rows;
		}else{
		return false;
		}
			
	}
	
	
	public function getTotalHistory($data) {	
	  	$query = $this->db->query( "SELECT * FROM " . DB_PREFIX . "ny_mail_history" );
		return $query->num_rows;
	}
	
	
	public function createTableHistory() {
		$query = $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ny_mail_history` (
									`id` INT(11) AUTO_INCREMENT, 
									`subject` TEXT COLLATE utf8_general_ci NOT NULL,
									`content` TEXT COLLATE utf8_general_ci NOT NULL, 
									`to`      TEXT COLLATE utf8_general_ci NOT NULL,
									`date` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00', 
									 PRIMARY KEY (`id`))");
	}	
	
	public function savetoHistory($subject, $content, $to) {
     	$query = $this->db->query("INSERT INTO `" . DB_PREFIX . "ny_mail_history` SET 
									 `subject` ='".$this->db->escape($subject)."',
									 `content` ='".$this->db->escape($content)."',
				 					 `to` ='".$this->db->escape($to)."',									
									 `date` ='".date("Y-m-d H:m:s")."'  
									 ");
	}
	
	public function getshortHistory() {
	          $this->createTableHistory();
			 $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "ny_mail_history` ORDER BY `id` DESC LIMIT 25  ");
		     return $query->rows;
		}
		
	public function getTotalEmailsHistory() {
		$query = $this->db->query(" SELECT * FROM `" . DB_PREFIX . "ny_mail_history` LIMIT 25 ");
		return $query->num_rows;
	}
	

		
	public function deleteEmailHistory($id) {

		$this->db->query("DELETE FROM " . DB_PREFIX . "ny_mail_history WHERE id = '" . (int)$id . "'");

	}
	
	public function getHistorybyid($id){
			$query = $this->db->query(" SELECT * FROM `" . DB_PREFIX . "ny_mail_history` WHERE `id` = '" . (int)$id . "'  ");
		     return $query->row;
	
	}
	
	public function getTotalSubscribers($data) {
		$this->check_dbSubscribers();
		$sql = "SELECT * FROM " . DB_PREFIX . "ny_subscribe ORDER BY id DESC";
		$query = $this->db->query($sql);
		return $query->num_rows;

	}
	
	public function getSubscribers($data,$start,$limit) {
		$query = $this->db->query( "SELECT * FROM `" . DB_PREFIX . "ny_subscribe`  ORDER BY `id` DESC LIMIT $start,$limit" );
		return $query->rows;
	}
	
	public function getSubscriber($id) {
		$query = $this->db->query("SELECT  * FROM `".DB_PREFIX."ny_subscribe` WHERE `id` = '" . (int)$id . "'");
		return $query->row;
	}
	
	
	public function addSubscriber($data) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "ny_subscribe` SET 
		`email_id` = '".$this->db->escape($data['email_id'])."',
		`name` = '".$this->db->escape($data['email_name'])."',
		`last_name` ='".$this->db->escape($data['last_name'])."'
			");
	}
	public function checkmailSubscribers($data,$id=FALSE) {
	   if($id)
		$sql = "SELECT * FROM " . DB_PREFIX . "ny_subscribe WHERE email_id='".$data."' AND id!='".$id."'";
	   else	
		$sql = "SELECT * FROM " . DB_PREFIX . "ny_subscribe WHERE email_id='".$data."'";
    	$query = $this->db->query($sql);
		return $query->num_rows;
	}
	
	public function deleteSubscribers($id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "ny_subscribe WHERE id = '" . (int)$id . "'");
	}
	
	public function exportSubscribers() {
		$this->check_dbSubscribers();
		$sql = "SELECT name,last_name,email_id FROM " . DB_PREFIX . "ny_subscribe";
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function editSubscribers($id, $data) {

		$this->db->query("UPDATE `" . DB_PREFIX . "ny_subscribe` SET 
		`email_id` ='".$data['email_id']."',
		`name` ='".$data['email_name']."', 
		`last_name` ='".$data['last_name']."' 
		WHERE `id` = '" . (int)$id . "'");

	}
	
		public function getsettingTemplates() {

		$res = $this->db->query("SELECT * FROM `".DB_PREFIX."setting` WHERE `group` LIKE 'emailswtemplates'  ");

			if( $res->num_rows>0 ){
				$templates = unserialize( $res->row['value'] );
				ksort($templates);
				reset($templates);
				return $templates;
			}else{
				return '';
			}
		}
	
	
	private function check_dbSubscribers(){
	 $this->db->query("CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "ny_subscribe (

	  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,

	  `email_id` varchar(225) NOT NULL,

	  `name` varchar(225) NOT NULL,
	  `last_name` varchar(225) NOT NULL,

	  `option1` varchar(225) NOT NULL,

	  `option2` varchar(225) NOT NULL,

	  `option3` varchar(225) NOT NULL,

	  `option4` varchar(225) NOT NULL,

	  `option5` varchar(225) NOT NULL,

	  `option6` varchar(225) NOT NULL,

	  PRIMARY KEY (`id`),

	  UNIQUE KEY `Index_2` (`email_id`)

	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;");

   }
	
}
?>