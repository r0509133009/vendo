<?php
class ModelAffiliateBanner extends Model {
	
	public function getBanner($bn_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "affiliate_bn WHERE bn_id = '" . (int)$bn_id . "'");
		return $query->row;
	}
	
	public function getBanners($data = array()) {
		
		$sql = "SELECT * FROM " . DB_PREFIX . "affiliate_bn";

		$implode = array();
		
		if (!empty($data['filter_bn_proid'])) {
			$implode[] = "LCASE(a.bn_proid) = '" . $this->db->escape(utf8_strtolower($data['filter_bn_proid'])) . "'";
		}
		
		if (!empty($data['filter_bn_name'])) {
			$implode[] = "CONCAT(a.bn_name) LIKE '" . $this->db->escape($data['bn_name']) . "%'";
		}
		
		if (!empty($data['filter_bn_size'])) {
			$implode[] = "LCASE(a.bn_size) = '" . $this->db->escape(utf8_strtolower($data['filter_bn_size'])) . "'";
		}
		
		if (!empty($data['filter_bn_des'])) {
			$implode[] = "LCASE(a.bn_des) = '" . $this->db->escape(utf8_strtolower($data['filter_bn_des'])) . "'";
		}
		
		if (!empty($data['filter_bn_key'])) {
			$implode[] = "LCASE(a.bn_key) = '" . $this->db->escape(utf8_strtolower($data['filter_bn_key'])) . "'";
		}
		
		if (!empty($data['filter_bn_views'])) {
			$implode[] = "LCASE(a.bn_views) = '" . $this->db->escape(utf8_strtolower($data['filter_bn_views'])) . "'";
		}
		
		if (!empty($data['filter_bn_click'])) {
			$implode[] = "LCASE(a.bn_click) = '" . $this->db->escape(utf8_strtolower($data['filter_bn_click'])) . "'";
		}
		
		if (!empty($data['filter_bn_status'])) {
			$implode[] = "LCASE(a.bn_status) = '" . $this->db->escape(utf8_strtolower($data['filter_bn_status'])) . "'";
		}
		
		if ($implode) {
			$sql .= " WHERE " . implode(" AND ", $implode);
		}
		
		$sort_data = array(
			'bn_name',
			'a.bn_proid',
			'a.bn_size',
			'a.bn_views',
			'a.bn_click'
		);
		
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY bn_name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getCampaignsName($campaign_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "affiliate_cp WHERE campaign_id = '" . $this->db->escape($campaign_id) . "'");
		return $query->row['campaign_name'];
	}
	
	public function getTotalBanners($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "affiliate_bn";

		$implode = array();
		
		if (!empty($data['filter_bn_name'])) {
			$implode[] = "bn_name LIKE '%" . $this->db->escape($data['filter_bn_name']) . "%'";
		}

		if (isset($data['filter_bn_size']) && !is_null($data['filter_bn_size'])) {
			$implode[] = "bn_size = '" . $data['filter_bn_size'] . "'";
		}

		if (isset($data['filter_bn_status']) && !is_null($data['filter_bn_status'])) {
			$implode[] = "bn_status = '" . (int)$data['filter_bn_status'] . "'";
		}

		if (!empty($data['filter_bn_create'])) {
			$implode[] = "bn_create = '" . (int)$data['filter_bn_create'] . "'";
		}

		if ($implode) {
			$sql .= " WHERE " . implode(" AND ", $implode);
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}
	
	public function getSizes($data = array()) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "affiliate_size");
		return $query->rows;
	}
	
	public function getProducts($data = array()) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "product");
		return $query->rows;
	}
	
	public function getPorduct($bn_proid) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$bn_proid . "'");
		return $query->row;
	}
	
	public function getPorductByCategory($campaign_camid) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "product_to_category WHERE 	category_id = '" . (int)$campaign_camid . "'");
		return $query->row;
	}
	
	public function addBanner($data) {
		$time_create = date('Y-m-d');
		$this->db->query("INSERT INTO " . DB_PREFIX . "affiliate_bn (bn_name, bn_des, bn_img, bn_camid, bn_proid, bn_create, bn_size, bn_status) VALUE ('".$this->db->escape($data['bn_name'])."', '".$this->db->escape($data['bn_des'])."', '".$this->db->escape($data['bn_img'])."', '".$this->db->escape($data['bn_camid'])."', '".$this->db->escape($data['product_id'])."', '$time_create', '".$this->db->escape($data['bn_size'])."', '".$this->db->escape($data['bn_status'])."')");
	}
	
	public function editBanner($bn_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "affiliate_bn SET bn_name='".$this->db->escape($data['bn_name'])."', bn_des='".$this->db->escape($data['bn_des'])."', bn_img='".$this->db->escape($data['bn_img'])."', bn_camid='".$this->db->escape($data['bn_camid'])."', bn_status='".$this->db->escape($data['bn_status'])."', bn_proid='".$this->db->escape($data['product_id'])."', bn_size='".$this->db->escape($data['bn_size'])."' WHERE bn_id='".(int)$bn_id."'");
	}
	public function deleteBanner($bn_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "affiliate_bn WHERE bn_id = '" . (int)$bn_id . "'");
	}
	
}