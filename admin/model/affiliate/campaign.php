<?php
class ModelAffiliateCampaign extends Model {
	
	public function getCampaign($campaign_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "affiliate_cp WHERE campaign_id = '" . (int)$campaign_id . "'");
		return $query->row;
	}
	
	public function getCampaigns($data = array()) {
		
		$sql = "SELECT * FROM " . DB_PREFIX . "affiliate_cp";

		$implode = array();
		
		if (!empty($data['filter_campaign_cateid'])) {
			$implode[] = "LCASE(a.campaign_cateid) = '" . $this->db->escape(utf8_strtolower($data['filter_campaign_cateid'])) . "'";
		}
		
		if (!empty($data['filter_campaign_name'])) {
			$implode[] = "CONCAT(a.campaign_name) LIKE '" . $this->db->escape($data['campaign_name']) . "%'";
		}
		
		if (!empty($data['filter_campaign_geo'])) {
			$implode[] = "LCASE(a.campaign_geo) = '" . $this->db->escape(utf8_strtolower($data['filter_campaign_geo'])) . "'";
		}
		
		if (!empty($data['filter_campaign_create'])) {
			$implode[] = "LCASE(a.campaign_create) = '" . $this->db->escape(utf8_strtolower($data['filter_campaign_create'])) . "'";
		}
		
		if (!empty($data['filter_campaign_status'])) {
			$implode[] = "LCASE(a.campaign_status) = '" . $this->db->escape(utf8_strtolower($data['filter_campaign_status'])) . "'";
		}
		
		if ($implode) {
			$sql .= " WHERE " . implode(" AND ", $implode);
		}
		
		$sort_data = array(
			'campaign_name',
			'a.campaign_cateid',
			'a.campaign_geo',
			'a.campaign_create',
			'a.campaign_status'
		);
		
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY campaign_name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getCampaignsCategory($campaign_id) {
		$query = @$this->db->query("SELECT * FROM " . DB_PREFIX . "category_description WHERE category_id = '" . $this->db->escape($campaign_id) . "'");
		if(isset($query->row['name'])){
		return @$query->row['name'];
		}
	}
	
	public function getCampaignsCountry($code_2) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "country WHERE iso_code_2 = '" . $this->db->escape($code_2) . "'");
		if(isset($query->row['name'])){
		return @$query->row['name'];
		}
	}
	
	public function getTotalCampaigns($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "affiliate_cp";

		$implode = array();
		
		if (!empty($data['filter_campaign_name'])) {
			$implode[] = "campaign_name LIKE '%" . $this->db->escape($data['filter_campaign_name']) . "%'";
		}

		if (isset($data['filter_campaign_geo']) && !is_null($data['filter_campaign_geo'])) {
			$implode[] = "campaign_geo = '" . $data['filter_campaign_geo'] . "'";
		}

		if (isset($data['filter_campaign_status']) && !is_null($data['filter_campaign_status'])) {
			$implode[] = "campaign_status = '" . (int)$data['filter_campaign_status'] . "'";
		}

		if (!empty($data['filter_campaign_create'])) {
			$implode[] = "DATE(campaign_create) = DATE('" . $this->db->escape($data['filter_campaign_create']) . "')";
		}

		if ($implode) {
			$sql .= " WHERE " . implode(" AND ", $implode);
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}
	
	public function addCampaign($data) {
		$time_create = date('Y-m-d');
		$this->db->query("INSERT INTO " . DB_PREFIX . "affiliate_cp (campaign_name, campaign_des, campaign_cateid, campaign_geo, campaign_status, campaign_create) VALUE ('".$this->db->escape($data['campaign_name'])."', '".$this->db->escape($data['campaign_des'])."', '".$this->db->escape($data['campaign_cateid'])."', '".$this->db->escape($data['campaign_geo'])."', '".$this->db->escape($data['campaign_status'])."', '$time_create')");
	}
	public function editCampaign($campaign_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "affiliate_cp SET campaign_name='".$this->db->escape($data['campaign_name'])."', campaign_des='".$this->db->escape($data['campaign_des'])."', campaign_cateid='".$this->db->escape($data['campaign_cateid'])."', campaign_geo='".$this->db->escape($data['campaign_geo'])."', campaign_status='".$this->db->escape($data['campaign_status'])."' WHERE campaign_id='".(int)$campaign_id."'");
	}
	public function deleteCampaign($campaign_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "affiliate_cp WHERE campaign_id = '" . (int)$campaign_id . "'");
	}
}