<?php
class ModelAffiliateStats extends Model {
	
	public function getStats($month) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "affiliate_statsd WHERE statsd_month='" . $this->db->escape($month) . "' ORDER BY statsd_id DESC");
		return $query->rows;
	}
	
	public function getChartStats($data = array()) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "affiliate_statsd ORDER BY statsd_id DESC LIMIT 10");
		return $query->rows;
	}
	
	public function getStatsMonth($data = array()) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "affiliate_statsd GROUP BY statsd_month DESC");
		return $query->rows;
	}
	
	public function getTotalTransaction($affiliate_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "affiliate_transaction WHERE affiliate_id = '" . $this->db->escape($affiliate_id) . "'");

		return $query->row['total'];
	}
	public function getTransactions($data = array()) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "affiliate_transaction ORDER BY affiliate_transaction_id DESC LIMIT 31");
		return $query->rows;
	}
	
	public function getAffiliateEmail($affiliate_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "affiliate  WHERE affiliate_id = '" . $this->db->escape($affiliate_id) . "'");
		return $query->row['email'];
	}
	
	public function getTotalSales($date) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "affiliate_transaction WHERE order_id != '0' AND (date_added LIKE '%" . $this->db->escape($date) . "%')");

		return $query->row['total'];
	}
	
	public function getTotalSalesAmount($date) {
		$query = $this->db->query("SELECT SUM(amount) AS total FROM " . DB_PREFIX . "affiliate_transaction WHERE order_id != '0' AND (date_added LIKE '%" . $this->db->escape($date) . "%')");

		return $query->row['total'];
	}
	
}