<?php
class ModelOrderMailCloneData extends Model {

  public function select()
  {
    $query = $this->db->query("SELECT * FROM `" . DB_PREFIX ."order_mail_clone` ORDER BY `order_mail_clone_id` ASC");
  
    return $query->rows;
  }

  public function add($data)
  {
    $this->db->query("INSERT INTO `" . DB_PREFIX ."order_mail_clone` (`description`, `email`) VALUES ('" . $this->db->escape($data['description']) . "', '" . $this->db->escape($data['mail']) . "')");
  }

  public function delete($data)
  {
    $this->db->query("DELETE FROM `" . DB_PREFIX . "order_mail_clone` WHERE `order_mail_clone_id` = " . $data['order_mail_clone_id']);
  }

  public function createTable()
  {
    $this->db->query("
      CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "order_mail_clone` (
        `order_mail_clone_id` int(11) NOT NULL AUTO_INCREMENT,
        `description` varchar(255) NOT NULL ,
        `email` varchar(255) NOT NULL ,
        PRIMARY KEY (`order_mail_clone_id`)
    ) DEFAULT COLLATE=utf8_general_ci;");
  }

  public function deleteTable()
  {
    $this->db->query("
      DROP TABLE IF EXISTS `" . DB_PREFIX . "order_mail_clone`");
  }

}
?>