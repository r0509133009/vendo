<?php
/**
 * @total-module	Hide Admin Products and Category fields
 * @author-name 	◘ Dotbox Creative
 * @copyright		Copyright (C) 2014 ◘ Dotbox Creative www.dotboxcreative.com
 * @license			GNU/GPL, see http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
 */
// Heading
$_['heading_title']       = 'Hide Admin Menu Produkty a Kategórie';

$_['text_success']        = 'Zaheslovaný obchod sa podarilo aktualizovať!';
$_['text_module']         = 'Moduly';

//
$_['hidden']              = ' Skyť ';
$_['visible']             = ' Ukázať ';

// Tab Switcher
$_['tab_product']         = 'Produkty';
$_['tab_category']        = 'Kategórie';

// Tab Title Products
$_['tab_general']          = 'Všeobecné Polia';
	$_['tab_meta_tag_des']          = 'Popis (Meta Tag):';
	$_['tab_meta_tag_key']          = 'Kľúčové slová (Meta Tag):';
	$_['tab_product_tag']           = 'Štítky produktu:';
$_['tab_data']             = 'Dáta';
	$_['tab_sku']          			= 'SKU';
	$_['tab_upc']          			= 'UPC';
	$_['tab_ean']          			= 'EAN';
	$_['tab_jan']          			= 'JAN';
	$_['tab_isbn']         			= 'ISBN';
	$_['tab_mnp']          			= 'MNP';
	$_['tab_location']         		= 'Umiestnenie:';
	$_['tab_price']         		= 'Cena:';
	$_['tab_tax']         			= 'Daňová trieda:';
	$_['tab_quantity']         		= 'Množstvo:';
	$_['tab_min_quantity']          = 'Minimálne množstvo:';
	$_['tab_sub_stock']          	= 'Odčítanie skladom:';
	$_['tab_ooss']          		= 'Stav vypredania:';
	$_['tab_req_shipping']         	= 'Vyžaduje doručenie:';
	$_['tab_seo']         			= 'SEO kľúčové slovo:';
	$_['tab_date_ava']         		= 'Dátum dostupnosti:';
	$_['tab_dimensions']          	= 'Rozmery (d x š x v):';
	$_['tab_length']          		= 'Dĺžková trieda:';
	$_['tab_weight']          		= 'Váha:';
	$_['tab_weight_class']          = 'Váhová trieda:';
	$_['tab_sort']          		= 'Radenie:';
$_['tab_link']          = 'Linky';
	$_['tab_manufacturer']         	= 'Výrobca:';	
	$_['tab_filters']          		= 'Filtre:';
	$_['tab_stores']          		= 'Obchod:';
	$_['tab_downloads']          	= 'Stiahnutia:';
	$_['tab_related']          		= 'Súvisiace produkty:';
$_['tab_atribute']        	= 'Atribúty';
	$_['tab_atribute_sec']        	= 'Atribúty';
$_['tab_option']        	= 'Možnosti';
	$_['tab_option_sec']        	= 'Možnosti';
$_['tab_profile']        	= 'Profil';
	$_['tab_profile_sec']        	= 'Profil';					
$_['tab_discount']        	= 'Množstevná zľava';
	$_['tab_discount_sec']        	= 'Množstevná zľava';	
$_['tab_special']        	= 'Akcia';
	$_['tab_special_sec']        	= 'Akcia';	
$_['tab_image']        		= 'Obrázky';
	$_['tab_image_sec']        		= 'Obrázky';	
$_['tab_reward']        	= 'Vernostné Body';
	$_['tab_reward_sec']        	= 'Vernostné Body';		
$_['tab_design']        	= 'Dizajn';
	$_['tab_design_sec']        	= 'Dizajn';	
$_['tab_marketplace']        	= 'Marketplace';
	$_['tab_marketplace_sec']        	= 'Marketplace';			
$_['tab_status']        	= 'Stav';
	$_['tab_collabse']        		= 'Spojiť Tabulky';
	$_['tab_status_sec']        	= 'Skryť Produktové polia status';		
	
// Tab Title Category	
$_['tab_general_cat']          	= 'Všeobecné Polia';
	$_['tab_meta_tag_des_cat']          = 'Popis (Meta tag):';
	$_['tab_meta_tag_key_cat']          = 'Kľúčové slová (Meta tag):';
$_['tab_data_cat']            	= 'Dáta';
	$_['tab_filters_cat']          		= 'Filtre:';
	$_['tab_stores_cat']          		= 'Obchod:';
	$_['tab_seo_cat']         			= 'SEO kľúčové slová:';
	$_['tab_image_cat']          		= 'Obrázok:';
	$_['tab_top_cat']          			= 'Hore:';
	$_['tab_columns_cat']         		= 'Stĺpce:';
	$_['tab_sort_cat']          		= 'Radenie:';
$_['tab_design_cat']        	= 'Dizajn';
	$_['tab_design_sec_cat']        	= 'Dizajn';	
$_['tab_status_cat']        	= 'Stav:';
	$_['tab_collabse_sec']        		= 'Spojiť Tabulky';
	$_['tab_status_sec_cat']        	= 'Skryť Kategórie status';

//info
$_['tab_price_info']         		    = ' Zakladná cena ak je pole skryté';
$_['tab_quantity_info']         		= ' Základné množstvo ak je pole skryté';

// Error
$_['error_permission']    = 'Upozornenie: Nemáte oprávnenie k úprave modulu Hide Admin Menu Produkty a Kategórie!';
?>