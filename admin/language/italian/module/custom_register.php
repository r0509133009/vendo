<?php
// ***************************************************
//             Custom Registration Fields   
//       
// Author : Francesco Pisanò - francesco1279@gmail.com
//              
//                   www.leverod.com		
//               © All rights reserved	  
// ***************************************************


// Admin language file

$crf_version = '2.7.1';

$_['heading_title']    = '<span style="position:relative; padding-left:26px;font-weight:bold;"><img style="width:20px; position:absolute; bottom:-3px; left:-1px;" src="view/image/crf-logo-light.gif" />Custom Registration Fields '.$crf_version .'</span>';
$_['heading_title_cp'] = 'Custom Registration Fields '.$crf_version;

// Text
$_['text_settings'] 		= 'Impostazioni';
$_['text_antispam'] 		= 'AntiSpam Shield';
$_['text_user_guide']		= 'Guida utente';
$_['text_license']			= 'Licenza';
$_['text_module']			= 'Moduli';
$_['text_entry']			= 'Fields';
$_['text_show']				= 'Mostra';
$_['text_enable']			= 'Abilita';
$_['text_account']			= 'Account';
$_['text_register']			= 'Registra';
$_['text_account_already']	= 'Se hai gi&agrave; un account con noi, sei pregato di effettuare il login <a href="%s">qui</a>.';
$_['text_your_details']		= 'I tuoi Dettagli Personali';
$_['text_your_address']		= 'I tuoi Indirizzi';
$_['text_newsletter']		= 'Newsletter';
$_['text_your_password']	= 'La Tua Password';
$_['text_agree']			= 'Ho letto e accetto <a class="fancybox" href="%s" alt="%s"><b>%s</b></a>';

$_['entry_account']			= 'Condizioni di utilizzo dell&#039;account (Pagina di Registrazione e Checkout)';
$_['entry_checkout']		= 'Condizioni di vendita';

$_['text_member']			= 'Utenti con account';
$_['text_guest']			= 'Consenti Utenti ospiti';
$_['text_customer_grp_mgr']	= 'Gestione Gruppi Clienti';
$_['text_help']				= 'Aiuto';
$_['text_select']			= 'Seleziona';
$_['text_select_all']		= 'Tutti';
$_['text_deselect_all']		= 'Nessuno';
$_['text_none']				= 'Nascondi checkbox';
$_['text_display']			= 'Mostra';
$_['text_required']			= 'Obbligatorio';
$_['text_checked_by_default']	= 'Attivato per default';
$_['text_approval']			= 'Approva';
$_['text_name']				= 'Nome';
$_['text_description']		= 'Descrizione';
$_['text_default']			= 'Predefinito';
$_['text_sort_order']		= 'Ordinamento';

$_['text_reset_default']	= 'Ripristina valori<br />predefiniti';

$_['text_account_registration'] = 'Pagina di registrazione nuovo account';
$_['text_checkout_page'] 		= 'Pagina di pagamento';

// Help
$_['text_help_member']						= 'Gli utenti con registrazione necessitano di un account per poter effettuare un ordine.';
$_['text_help_member_firstname']			= '';
$_['text_help_member_lastname']				= '';
$_['text_help_member_email']				= 'L&#039;email non pu&ograve essere disabilitata per gli utenti con registrazione, ne avranno bisogno per accedere ai loro accounts.';
$_['text_help_member_telephone']			= '';
$_['text_help_member_fax']					= '';
$_['text_help_member_password']				= 'La password non pu&ograve essere disabilitata per gli utenti con registrazione, &egrave; necessaria per accedere ai loro accounts.';
$_['text_help_member_confirm']				= 'Quando &egrave; abilitata, la conferma password &egrave; sempre un campo obbligatorio.';
$_['text_help_member_company']				= '';
$_['text_help_member_address_1']			= '';
$_['text_help_member_address_2']			= '';
$_['text_help_member_city']					= '';
$_['text_help_member_country']				= '';
$_['text_help_member_default_country']		= '';
$_['text_help_member_postcode']				= 'Nota: questa impostazione non sovrascrive quella presente in System -> Localization -> Countries -> "Postcode Required". <a href="#postcode-required">Leggi qui</a>.';
$_['text_help_member_zone']					= '';
$_['text_help_member_newsletter']			= '';
$_['text_help_member_shipping_address']		= 'Se nascondi questo checkbox, come impostazione predefinita l&#039;indirizzo di fatturazione sar&agrave; uguale a quello di spedizione ma gli utenti potranno modificarlo cliccando sulla voce &quot;Modifica&quot;.';
$_['text_help_member_agree']				= '';	

$_['text_help_guest_checkout']				= 'Autorizza gli utenti non registrati ad acquistare senza un account. Questa opzione non sar&agrave; disponibile se un prodotto scaricabile &egrave; presente nel carrello.';
$_['text_help_guest_firstname']				= '';
$_['text_help_guest_lastname']				= '';
$_['text_help_guest_email']					= '';
$_['text_help_guest_telephone']				= '';
$_['text_help_guest_fax']					= '';
$_['text_help_guest_password']				= '';
$_['text_help_guest_confirm']				= '';
$_['text_help_guest_company']				= '';
$_['text_help_guest_address_1']				= '';
$_['text_help_guest_address_2']				= '';
$_['text_help_guest_city']					= '';
$_['text_help_guest_country']				= '';
$_['text_help_guest_default_country']		= '';
$_['text_help_guest_postcode']				= '';
$_['text_help_guest_zone']					= '';
$_['text_help_guest_newsletter']			= 'Un utente guest non pu&ograve; ricevere newsletters perch&eacute; Opencart non memorizza il suo account ma solo il suo ordine.';
$_['text_help_guest_shipping_address']		= 'Se nascondi questo checkbox, gli utenti non saranno in grado di inserire un indirizzo di spedizione diverso da quello di fatturazione.';
$_['text_help_guest_agree']					= '';

$_['text_help_hidden_antispam'] 			= 'Attiva l&#039;AntiSpam Shield per <b>bloccare i tentativi di registrazione sul tuo sito da parte degli spam bots</b>. Questa funzionalit&agrave; rimane <b>invisibile</b> ai tuoi clienti.';


// buttons

$_['button_save']			= 'Salva';
$_['button_save_continue']	= 'Salva e continua';


// Entry
$_['entry_firstname']		= 'Nome';
$_['entry_lastname']		= 'Cognome';
$_['entry_email']			= 'E-Mail';
$_['entry_telephone']		= 'Telefono';
$_['entry_fax']				= 'Fax';
$_['entry_password']		= 'Password';
$_['entry_confirm']			= 'Conferma Password';
$_['entry_company']			= 'Azienda';
$_['entry_address_1']		= 'Indirizzo 1';
$_['entry_address_2']		= 'Indirizzo 2';
$_['entry_city']			= 'Citt&agrave;';
$_['entry_country'] 		= 'Nazione';
$_['entry_default_country']	= 'Nazione Predefinita';
$_['entry_postcode'] 		= 'CAP';
$_['entry_zone']			= 'Provincia';
$_['entry_newsletter']		= 'Mostra checkbox &quot;Sottoscrivi Newsletter&quot;';
$_['entry_shipping_address']= 'Mostra checkbox &quot;Il mio indirizzo di fatturazione &egrave; uguale a quello di consegna&quot;';
$_['entry_agree']			= 'Mostra checkbox &quot;Accetto i termini e le condizioni&quot;';

$_['entry_customer_group']			= 'Gruppo clienti';
$_['entry_customer_group_name']		= 'Nome gruppo clienti';
$_['entry_default_customer_group']	= 'Gruppo clienti predefinito';
$_['entry_company_id']				= 'Company id (codice fiscale)';
$_['entry_tax_id']					= 'Tax id (Partita Iva)';


// Errors

$_['error_warning']					= 'Attenzione: Correggi gli errori presenti sul form';
$_['error_customer_group_name'] 	= 'La lunghezza del nome gruppo clienti deve essere compresa tra 3 e 32 caratteri!';
$_['error_customer_group_display']	= 'Devi includere almeno il gruppo di default se vuoi usare questa funzione!';
$_['error_install']					= 'Errore durante l&#039;installazione di <b>Custom Registration Fields</b>. XML mancante?';
$_['error_uninstall']				= 'Errore durante la disinstallazione di <b>Custom Registration Fields</b>. XML mancante?';


// install / uninstall / Save

$_['success_install']		= 'Per completare l&#039;installazione entra nel pannello dell&#039;estensione e clicca su <b>Salva</b>.';
$_['success_uninstall']		= '<b>Custom Registration Fields</b> &egrave; stato disinstallato con successo.';
$_['text_saving']			= 'Sto salvando';
$_['text_success']			= 'L&#039;estensione <b>Custom Registration Fields</b> &egrave; stata salvata con successo';
?>