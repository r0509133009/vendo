<?php
// Heading
$_['heading_title']       = 'Мультизагрузка изображений';

// Text
$_['text_module']         = 'Модули';
$_['text_success']        = 'Success: You have modified module Multi Image Uploader!';


// Entry
$_['entry_folder']          = 'Папка для сохранения изображений:<br /><span class="help">Если папка не существует, то она будет создана автоматически. Файлы сохранятся в этой папке после нажатия кнопки "Сохранить".</span>';
$_['entry_segmet']        = 'Segment images:<br /><span class="help">The segmentation is done, when you hit save button.</span>';
$_['entry_segmet_by_none']        = 'No segment';
$_['entry_segmet_by_date']        = 'По дате';
$_['entry_delete_def_image']      = 'Delete Default image as additinal image<br /><span class="help">If set to "NO", the default image will be keeped as additional image.</span>';
$_['text_yes'] = "Да, удалить";
$_['text_no'] = "Нет, оставить";

$_['entry_status']        = 'Status:';

// Errors
$_['error_folder'] = 'Please specify folder';
?>