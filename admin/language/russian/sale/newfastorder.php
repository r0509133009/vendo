<?php  
// Heading
$_['heading_title']        						= 'Быстрый заказ';
$_['heading_title_setting']     				= 'Быстрый заказ настройка';
// Text
$_['text_success']        						= 'Список заказов обновлен';
$_['text_id']      	 	 						= '№';
$_['text_name']      	  						= 'Имя';
$_['text_manager']      	  					= 'Менеджер';
$_['text_manager_form']      					= 'Выберите Менеджера';
$_['text_telephone']   	  						= 'Телефон';
$_['text_comment']     			 				= 'Комментарий Менеджера';
$_['text_comment_buyer']   	 					= 'Комментарий Покупателя';
$_['text_email_buyer']   	 					= 'E-mail Покупателя';
$_['text_product_name']   	 					= 'Товар';
$_['text_product_price']   	 					= 'Цена';
$_['text_product_image']   	 					= 'Фото товара';
$_['text_newfastorder_url']   					= 'URL';
$_['text_status']      	 						= 'Статус';
$_['text_added']      	 						= 'Дата заказа';
$_['text_modified']       						= 'Дата изменения';
$_['text_action']         						= 'Действие';
$_['text_edit']       	  						= 'Редактировать';
$_['text_total_all']       	  					= 'ИТОГО';
$_['product_name_fast']        					= 'Название товара';
$_['number_order_id']        					= 'Заказ №';
$_['text_number_order_id_']     				= 'Заказ № - ';
$_['button_fastorder_setting']  			 	= 'Настройка';
$_['text_link']  			 					= 'Ссылка';
$_['text_link_description']     				= 'Страница с которой заказали товар';
$_['status_wait']         		= 'Ожидание';
$_['status_done']         		= 'Обработан';

$_['column_name']         		= 'Имя';
$_['column_telephone']    		= 'Телефон';
$_['column_date_added']   		= 'Дата заказа';

$_['button_delete']         	= 'Удалить';
$_['button_save']         		= 'Сохранить';
$_['button_cancel']         	= 'Отменить';



/*SETTING*/
$_['text_setting_popup_and_button_on_off'] 						= 'Настройки Popup окна и Область показа Кнопки';
$_['text_general_image_product_popup'] 							= 'Отключить изображение товара в Popup Окне';
$_['text_on_off_qo_fm'] 										= 'В модуле Рекомендуемые';
$_['text_on_off_qo_sm'] 										= 'В модуле Акции';
$_['text_on_off_qo_bm'] 										= 'В модуле Хит Продаж';
$_['text_on_off_qo_lm'] 										= 'В модуле Последние поступления';
$_['text_on_off_qo_cpage'] 										= 'В категориях';
$_['text_on_off_qo_special_page'] 								= 'На странице специальных предложений (акций)';
$_['text_on_off_qo_search_page'] 								= 'На странице результатов поиска';
$_['text_on_off_qo_manufacturer_page'] 							= 'На странице производителя';
$_['text_on_off_qo_product_page'] 								= 'На странице товара';
$_['text_on_off_qo_shopping_cart'] 								= 'В корзине';
$_['text_success_save_setting'] 								= 'Настройки успешно сохранены !';

$_['register_site_fastorder'] 									= 'Пройдите регистрацию на сайте:';
$_['entry_instruction_icon_change_fastorder'] 					= '<span style="color:#727272;">Для того, чтобы поменять иконку на кнопке, откройте сайт <a href="http://fortawesome.github.io/Font-Awesome/icons/"  target="_blank">http://fortawesome.github.io/Font-Awesome/icons/</a> выберите иконку, откройте ее и скопируйте class</br> будет это вот так ->       fa fa-phone-square      <-:</span>';
$_['entry_phone_number_send_sms_fastorder'] 					= 'Введите номер для получения смс:';
$_['entry_login_send_sms_fastorder'] 							= 'Логин который регистрировали на сайте:';
$_['entry_pass_send_sms_fastorder'] 							= 'Пароль который регистрировали на сайте:';
$_['text_yes_fastorder'] 										= 'Да:';
$_['text_no_fastorder'] 										= 'Нет:';
$_['on_off_sms_fastorder'] 										= 'Вкл выкл отправку смс:';
$_['entry_icon_send_fastorder'] 								= 'Изменить иконку на кнопке [ОФОРМИТЬ ЗАКАЗ]:';
$_['entry_background_button_send_fastorder'] 					= 'Изменить Цвет кнопки [ОФОРМИТЬ ЗАКАЗ]';
$_['entry_background_button_send_fastorder_hover'] 				= 'Изменить Цвет кнопки [ОФОРМИТЬ ЗАКАЗ] при наведении:';
$_['entry_background_button_open_form_send_order'] 				= 'Изменить Цвет кнопки [БЫСТРЫЙ ЗАКАЗ]:';
$_['entry_icon_open_form_send_order'] 							= 'Изменить Иконку кнопки [БЫСТРЫЙ ЗАКАЗ]:';
$_['entry_icon_open_form_send_order_size'] 						= 'Изменить размер иконки [БЫСТРЫЙ ЗАКАЗ]:';
$_['entry_color_button_open_form_send_order'] 					= 'Изменить Изменить цвет иконки и текста:';
$_['entry_text_open_form_send_order'] 							= 'Изменить название кнопки [БЫСТРЫЙ ЗАКАЗ]:';
$_['entry_any_text_at_the_bottom'] 								= 'Добавить любой текст после кнопки [ОФОРМИТЬ ЗАКАЗ]:';
$_['entry_img_fastorder'] 										= 'Добавить Картинку в [БЫСТРЫЙ ЗАКАЗ]:';
$_['entry_any_text_at_the_bottom_color'] 						= 'Изменить цвет текста после кнопки [ОФОРМИТЬ ЗАКАЗ]:';
$_['entry_background_button_open_form_send_order_hover'] 		= 'Изменить Цвет кнопки [БЫСТРЫЙ ЗАКАЗ] при наведении:';
$_['entry_mask_phone_number'] 									= 'Изменить Маску телефона (+3(999) 999-99-99) или вот так 79(999)99-99-999:';
$_['text_image_manager'] 										= 'Редактор Изображений';
$_['text_browse'] 												= 'Изменить';
$_['text_clear'] 												= 'Удалить';

$_['text_status_fields'] 										= 'Статус поля';
$_['text_requared_fields'] 										= 'Обязательно для заполнения';
$_['text_placeholder_fields'] 									= 'Заполнитель';
$_['text_on_off_fields_firstname'] 								= 'Поле - Имя -';
$_['text_on_off_fields_phone'] 									= 'Поле - Телефон -';
$_['text_on_off_fields_comment'] 								= 'Поле - Комментарий -';
$_['text_on_off_fields_email'] 									= 'Поле - Email -';

$_['tab_fields_setting'] 										= 'Поля Формы';
$_['tab_general_setting'] 										= 'Основные настройки';
$_['tab_design_setting'] 										= 'Дизайн';
$_['tab_sms_setting'] 											= 'Настройки Смс';
$_['tab_email_setting'] 										= 'Настройки Шаблона Письма';

$_['entry_title_popup_quickorder'] 								= 'Заголовок всплывающее окно';
$_['text_on_off_shipping_method'] 								= 'Способ Доставки';
$_['text_on_off_payment_method'] 								= 'Способ Оплаты';

$_['form_latter_from_buyer'] 									= 'Шаблон письма для Покупателя!';
$_['form_latter_from_me'] 										= 'Шаблон письма для Вас [Админ Магазина]!';
$_['text_on_off_send_buyer_mail'] 								= 'Отправлять письмо на почту покупателю';
$_['text_on_off_send_me_mail'] 									= 'Отправлять письмо на почту Вам [Админ]';
$_['select_design_fast_order'] 									= 'Выберите дизайн быстрого заказа';
$_['text_theme1'] 												= '№ - 1';
$_['text_theme2'] 												= '№ - 2';
$_['text_theme3'] 												= '№ - 3 Справа';
$_['text_theme4'] 												= '№ - 3 Слева';

$_['quickorder_subject_buyer'] 									= 'Тема письма';
$_['subject_text_variables'] 									= 'Поддерживает переменные';
$_['quickorder_description_buyer'] 								= 'Шаблон письма';
$_['text_form_latter_products'] 								= 'Прикрепить Шаблон письма - товара который заказали';
$_['text_complete_quickorder'] 									= 'Текст после успешного оформления заказа';
$_['heading_title_activation'] 									= 'Активация Модуля';
$_['add_activation_key'] 										= 'Введите лицензионный ключ:';
$_['btn_activation'] 											= 'Активировать';
$_['the_module_is_activated'] 									= 'Модуль Активирован!';
$_['key_success_deactivation'] 									= 'Модуль Деактивирован!';
$_['this_key_is_not_present_enter_the_correct_key'] 			= '<span style="color:red;">Ошибка:</span> такого ключа нет, введите правильный ключ!';
$_['key_error_deactivation'] 									= '<span style="color:red;">Ошибка:</span> Невозможно Деактивировать ключ';
$_['enter_key_deactivation'] 									= '<span style="color:red;">Ошибка:</span> ключ Деактивации не указан.!';
$_['enter_deactivation_key'] 									= 'Введите ключ для деактивации';
$_['btn_deactivation'] 											= 'Деактивировать ключ';

$_['list_of_variables_entry'] 						= '
<table><tr><td>
<br/><b>~name_fastorder~</b><i style="font-weight:400"> - Имя покупателя</i>
<br/><b>~phone~</b><i style="font-weight:400"> - телефон</i>
<br/><b>~comment_buyer~</b><i style="font-weight:400"> - комментарий</i>
<br/><b>~email_buyer~</b><i style="font-weight:400"> - email</i>      
<br/><b>~url_site~</b><i style="font-weight:400"> - url старницы с которой заказали товара</i> 
<br/><b>~price_shipping_text~</b><i style="font-weight:400"> - стоимость доставки </i> 
<br/><b>~shipping_title~</b><i style="font-weight:400"> - способ доставки</i>          
<br/><b>~payment_title~</b><i style="font-weight:400"> - способ оплаты </i>        			
<br/><b>~ip_store~</b><i style="font-weight:400"> - ip </i>
<br/><b>~currency_code~</b><i style="font-weight:400"> - Валюта</i>
<br/><b>~store_name~</b><i style="font-weight:400"> - Название магазина</i>             
<br/><b>~store_url~</b><i style="font-weight:400"> - url сайта  </i> 
</td></tr></table><br/>
';

?>
