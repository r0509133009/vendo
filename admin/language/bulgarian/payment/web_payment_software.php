<?php
// Heading
$_['heading_title']             = 'Web Payment Software';

// Text 
$_['text_payment']              = 'Плащане';
$_['text_success']              = 'Готово: промяната на данните за използване на &quot;Web Payment Software&quot; бе успешна!';
$_['text_web_payment_software'] = '<a href="http://www.web-payment-software.com/" target="_blank"><img src="view/image/payment/wps-logo.jpg" alt="Web Payment Software" title="Web Payment Software" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_test']                 = 'Тестов режим';
$_['text_live']                 = 'На живо';
$_['text_authorization']        = 'Авторизация';
$_['text_capture']              = '';

// Entry
$_['entry_login']               = 'Номер на търговец (ID):';
$_['entry_key']                 = 'Ключ за достъп:';
$_['entry_mode']                = 'Трансакционен режим:';
$_['entry_method']              = 'Трансакционен метод:';
$_['entry_total']               = 'Ценови праг:<br /><span class="help">Сумата за плащане, която трябва да се събере в кошницата, преди този метод за плащане да стане активен.</span>';
$_['entry_order_status']        = 'Статус на поръчката:';
$_['entry_geo_zone']            = 'Гео-зона:';
$_['entry_status']              = 'Статус:'; 
$_['entry_sort_order']          = 'Поредност:';

// Error 
$_['error_permission']          = 'Внимание: Нямате права за промяна в секцията!';
$_['error_login']               = 'Внимание: Посочването на номера на търговеца (ID) е задължително!';
$_['error_key']                 = 'Внимание: Посочването на ключа за достъп на търговеца е задължително!';
?>