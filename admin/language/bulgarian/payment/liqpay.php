<?php //!
// Heading
$_['heading_title']      = 'LIQPAY';

// Текст 
$_['text_payment']       = 'Плащане';
$_['text_success']       = 'Готово, промените са запазени!';   
$_['text_pay']           = 'LIQPAY';
$_['text_card']          = 'Кредитна карта';

// Entry
$_['entry_merchant']     = 'Номер на търговец (ID):';
$_['entry_signature']    = 'Подпис:';
$_['entry_type']         = 'Тип:';
$_['entry_total']        = 'Ценови праг:<br /><span class="help">Сумата за плащане, която трябва да се събере в кошницата, преди този метод за плащане да стане активен.</span>';
$_['entry_order_status'] = 'Статус на поръчката:';
$_['entry_geo_zone']     = 'Гео-зона:';
$_['entry_status']       = 'Статус:';
$_['entry_sort_order']   = 'Поредност:';

// Error
$_['error_permission']   = 'Внимание: Нямате права за промяна в секцията';
$_['error_merchant']     = 'Внимание: Посочването на номера на търговеца (ID) е задължително!';
$_['error_signature']    = 'Внимание: Посочването на подписа е задължително!';
?>