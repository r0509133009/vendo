<?php

/**
 * eBORICA payment gateway for Opencart by Extensa Web Development
 *
 * Copyright © 2011-2015 Extensa Web Development Ltd. All Rights Reserved.
 * This file may not be redistributed in whole or significant part.
 * This copyright notice MUST APPEAR in all copies of the script!
 *
 * @author 		Extensa Web Development Ltd. (www.extensadev.com)
 * @copyright	Copyright (c) 2011-2015, Extensa Web Development Ltd.
 * @package 	eBORICA payment gateway
 * @link		http://www.opencart.com/index.php?route=extension/extension/info&extension_id=3004
 */

// Heading
$_['heading_title']                        = 'Директно плащане с кредитна/дебитна карта (eBORICA)';

// Text
$_['text_payment']                         = 'Плащане';
$_['text_success']                         = 'Успешно променихте параметрите на eBORICA!';
$_['text_borica']                          = '<a onclick="window.open(\'http://borica.bg/\');"><img src="view/image/payment/borica.gif" alt="eBORICA" title="eBORICA" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_save_before_generate']            = 'Запазете настройките преди да генерирате Частен ключ и Заявка за сертификат!';
$_['text_generate']                        = 'Генериране';
$_['text_download']                        = 'Сваляне';
$_['text_download_ppk']                    = 'Сваляне на PPK';
$_['text_send_to_bank']                    = '(и го изпратете на банката ви)';
$_['text_copy_send_to_bank']               = '(копирайте линка и го изпратете на банката ви)';
$_['text_copy_etlog_certificate']          = 'Копирайте и поставете eTLog сертификатa от банката';
$_['text_copy_certificates']               = 'Копирайте и поставете сертификатите от Административния интерфейс (eTLog)';
$_['text_import_into_browser']             = 'Импортирайте .p12 файлът в браузера си';
$_['text_success_generate']                = 'Успешно генерирахте Частен ключ и Заявка за сертификат!';
$_['text_confirm_generate']                = 'ВНИМАНИЕ! Ако отново генерирате Частен ключ и Заявка за сертификат трябва отново да ги изпратите на банката ви, да ви върнат нови Сертификати и да ги поставите в параметрите. Плащането няма да работи дотогава!';

// Button
$_['button_get_p12']                       = 'Запазване и сваляне p12 файл';
$_['button_save_certificates']             = 'Запазване на сертификатите';

// Entry
$_['entry_test']                           = 'Тестов модел:';
$_['entry_terminal']                       = 'Индентификатор на терминала:<br/><span class="help">8 цифрен код, предоставя се от обслужващата ви банка</span>';
$_['entry_description']                    = 'Кратко описание на поръчката';
$_['entry_country']                        = 'Държава:';
$_['entry_state']                          = 'Област:<br/><span class="help">на латиница</span>';
$_['entry_locality']                       = 'Местоположение (град):<br/><span class="help">на латиница</span>';
$_['entry_name']                           = 'Име (напр. домейна):<br/><span class="help">на латиница</span>';
$_['entry_email']                          = 'E-Mail:';
$_['entry_organization']                   = 'Име на фирмата:<br/><span class="help">на латиница</span>';
$_['entry_organization_unit']              = 'Звено в организацията:';
$_['entry_total']                          = 'Общо:<br /><span class="help">Общата сума на поръчката, която трябва да се достигне, за да стане този метод за плащане активен.</span>';
$_['entry_order_status']                   = 'Статус на поръчката при завършване на плащането:';
$_['entry_order_status_canceled']          = 'Статус на поръчката при отказано плащане:';
$_['entry_order_status_canceled_reversal'] = 'Статус на поръчката при обръщане на отказа:<br /><span class="help">Това означава, че връщането на пари е било отменено; например, Вие, търговецът, печелите спор с клиента и парите, върнати от вас при рекламация, отново отиват във Вашата сметка.</span>';
$_['entry_order_status_denied']            = 'Статус на поръчката при отхвърлено плащане:';
$_['entry_order_status_failed']            = 'Статус на поръчката при неуспешно плащане:';
$_['entry_currency']                       = 'Валута:';
$_['entry_geo_zone']                       = 'Гео зона:';
$_['entry_status']                         = 'Статус:';
$_['entry_sort_order']                     = 'Поредна позиция в колоната:';
$_['entry_test_private_key']               = 'Тестов Частен ключ:';
$_['entry_real_private_key']               = 'Реален Частен ключ:';
$_['entry_etlog_private_key']              = 'eTLog Частен ключ:';
$_['entry_test_key_csr']                   = 'Тестов Частен ключ и Заявка за сертификат (CSR):';
$_['entry_real_key_csr']                   = 'Реален Частен ключ и Заявка за сертификат (CSR):';
$_['entry_etlog_key_csr']                  = 'eTLog Частен ключ и Заявка за сертификат (CSR):<br/><span class="help">за системата за наблюдение на транзакции</span>';
$_['entry_test_certificate']               = 'Тестов сертификат:';
$_['entry_real_certificate']               = 'Реален сертификат:';
$_['entry_etlog_certificate']              = 'eTLog сертификат:';
$_['entry_return_url']                     = 'Return URL:';

// Error
$_['error_permission']                     = 'Внимание: Нямате права да променяте параметрите на eBORICA!';
$_['error_terminal']                       = 'Индентификатор на терминала е невалиден (трябва да е 8 цифри)!';
$_['error_description']                    = 'Необходимо е да въведете Описание!';
$_['error_country']                        = 'Необходимо е да изберете Държава!';
$_['error_state']                          = 'Областта е невалидна!';
$_['error_locality']                       = 'Местоположението е невалидно!';
$_['error_name']                           = 'Името е невалидно!';
$_['error_email']                          = 'E-Mail адресът е невалиден!';
$_['error_organization']                   = 'Име на фирмата е невалидно!';
$_['error_generate']                       = 'За да генерирате Заявка за сертификат, първо трябва да попълните данните!';
$_['error_openssl']                        = 'Възникна грешка при генерирането на Заявка за сертификат!<br />%s';
$_['error_download']                       = 'За да свалите Заявка за сертификат, първо трябва да го генерирате!';
$_['error_certificate']                    = 'Сертификатът е невалиден!';
$_['error_generate_p12']                   = 'За да генерирате .p12 файл, първо трябва да генерирате eTLog Частен ключ!';
?>