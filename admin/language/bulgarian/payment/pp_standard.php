<?php //!
// Heading
$_['heading_title']					 = 'PayPal Standard';

// Текст
$_['text_payment']					 = 'Плащане';
$_['text_success']					 = 'Готово, промените са запазени!';
$_['text_pp_standard']			 = '<a href="https://www.paypal.com/bg/mrb/pal=45ZYRMEVQJCD6" target="_blank"><img src="view/image/payment/paypal.png" alt="PayPal Payment Standard" title="PayPal Payments Standard" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization']		 = 'Авторизация';
$_['text_sale']						   = 'Продажба';

// Entry
$_['entry_email']					   = 'Е-поща:';
$_['entry_test']					   = 'Тестов режим:';
$_['entry_transaction']			 = 'Трансакционен метод:';
$_['entry_pdt_token']				 = 'PDT Token:<br/><span class="help">Payment Data Transfer Token се използва за осигуряване на допълнителна сигурност и надеждност. Как да включите използването на PDT <a href="https://cms.paypal.com/us/cgi-bin/?&cmd=_render-content&content_ID=developer/howto_html_paymentdatatransfer" alt="">вижте тук</a>!</span>';
$_['entry_debug']					   = 'Записване на системни съобщения:<br/><span class="help">Включване записването на информация за трансакциите в системната история.</span>';
$_['entry_total']            = 'Ценови праг:<br /><span class="help">Сумата за плащане, която трябва да се събере в кошницата, преди този метод за плащане да стане активен.</span>';
$_['entry_canceled_reversal_status'] = 'Статус при спиране на връщане:';
$_['entry_completed_status'] = 'Статус при приключване:';
$_['entry_denied_status']		 = 'Статус при отхвърляне:';
$_['entry_expired_status']	 = 'Статус при изтичане:';
$_['entry_failed_status']		 = 'Статус при неуспех:';
$_['entry_pending_status']	 = 'Статус при приемане:';
$_['entry_processed_status'] = 'Статус след обработка:';
$_['entry_refunded_status']	 = 'Статус при възстановяване на сумата:';
$_['entry_reversed_status']	 = 'Статус при връщане:';
$_['entry_voided_status']		 = 'Статус при отмяна:';
$_['entry_geo_zone']				 = 'Гео-зона:';
$_['entry_status']					 = 'Статус:';
$_['entry_sort_order']			 = 'Ред:';

// Error
$_['error_permission']			 = 'Внимание: Нямате права за промяна в секцията!';
$_['error_email']					   = 'Внимание: Посочването на е-поща е задължително!';
?>