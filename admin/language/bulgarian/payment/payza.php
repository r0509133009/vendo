<?php
// Heading
$_['heading_title']      = 'Payza';

// Text 
$_['text_payment']       = 'Плащане';
$_['text_success']       = 'Готово, промените са запазени!';
      
// Entry
$_['entry_merchant']     = 'Номер на търговец (ID):';
$_['entry_security']     = 'Парола:';
$_['entry_callback']     = 'Alert URL:<br /><span class="help">Задава се в контролния ви панел в Payza. Следва да поставите отметка и на "IPN Status".</span>';
$_['entry_total']        = 'Ценови праг:<br /><span class="help">Сумата за плащане, която трябва да се събере в кошницата, преди този метод за плащане да стане активен.</span>';
$_['entry_order_status'] = 'Статус на поръчката:';
$_['entry_geo_zone']     = 'Гео-зона:';
$_['entry_status']       = 'Статус:'; 
$_['entry_sort_order']   = 'Поредност:';

// Error
$_['error_permission']   = 'Внимание: Нямате права за промяна в секцията';
$_['error_merchant']     = 'Внимание: Посочването на номера на търговеца (ID) е задължително!';
$_['error_security']     = 'Внимание: Посочването на парола за достъп е задължително!';
?>