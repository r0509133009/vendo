<?php //!
// Heading
$_['heading_title']      = 'WorldPay';

// Text 
$_['text_payment']       = 'Плащане';
$_['text_success']       = 'Готово, успешно променихте данните за профила си в WordldPay!';
$_['text_successful']    = 'On - Always Successful';
$_['text_declined']      = 'On - Always Declined';
$_['text_off']           = 'Off';
      
// Entry
$_['entry_merchant']     = 'Номер на търговец (ID):';
$_['entry_password']     = 'Payment Response password:<br /><span class="help">Задава се от администртивния панел на профила ви в WordPay.</span>';
$_['entry_callback']     = 'Relay Response URL:<br /><span class="help">Задава се от администртивния панел на профила ви в WordPay. Ще е нужно да отметнете "Enable the Shopper Response".</span>';
$_['entry_test']         = 'Тестов режим:';
$_['entry_total']        = 'Ценови праг:<br /><span class="help">Сумата за плащане, която трябва да се събере в кошницата, преди този метод за плащане да стане активен.</span>';
$_['entry_order_status'] = 'Статус на поръчката:';
$_['entry_geo_zone']     = 'Гео зона:';
$_['entry_status']       = 'Статус:';
$_['entry_sort_order']   = 'Поредност:';

// Error
$_['error_permission']   = 'Внимание: Нямате права за промяна в секцията!';
$_['error_merchant']     = 'Внимание: Посочването на номера на търговеца (ID) е задължително!';
$_['error_password']     = 'Внимание: Посочването на парола е задължително!';
?>