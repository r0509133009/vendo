<?php //!
//Heading
$_['heading_title']             = 'Връщане на суми';

//Text
$_['text_pp_express']           = 'PayPal Express Checkout';
$_['text_current_refunds']      = 'По тази трансакция вече има възстановена сума. Максималната сума за възстановяване е';

//Button
$_['btn_cancel']                = 'Отмени';
$_['btn_refund']                = 'Възстанови сума';

//Form entry
$_['entry_transaction_id']      = 'Номер на трансакция (ID)';
$_['entry_full_refund']         = 'Пълно възстановяване';
$_['entry_amount']              = 'Сума';
$_['entry_message']             = 'Съобщение';

//Error
$_['error_partial_amt']         = 'Внимание: Трябва да въведете сума, която връщате.';
$_['error_data']                = 'Внимание: Липсват данни.';
?>