<?php //!
// Heading
$_['heading_title']      = 'SagePay Direct';

// Текст 
$_['text_payment']       = 'Плащане'; 
$_['text_success']       = 'Готово, промените са запазени!';
$_['text_sagepay']       = '<a href="https://support.sagepay.com/apply/default.aspx?PartnerID=E511AF91-E4A0-42DE-80B0-09C981A3FB61" target="_blank"><img src="view/image/payment/sagepay.png" alt="SagePay" title="SagePay" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_sim']           = 'Симулатор';
$_['text_test']          = 'Тест';
$_['text_live']          = 'На живо';
$_['text_defered']       = 'Отложено плащане';
$_['text_authenticate']  = 'Авторизация';

// Entry
$_['entry_vendor']       = 'Номер на търговец (ID):';
$_['entry_test']         = 'Тестов режим:';
$_['entry_transaction']  = 'Трансакционен метод:';
$_['entry_total']        = 'Ценови праг:<br /><span class="help">Сумата за плащане, която трябва да се събере в кошницата, преди този метод за плащане да стане активен.</span>';
$_['entry_order_status'] = 'Статус на поръчката:';
$_['entry_geo_zone']     = 'Гео-зона:';
$_['entry_status']       = 'Статус:';
$_['entry_sort_order']   = 'Поредност:';

// Error
$_['error_permission']   = 'Внимание: Нямате права за промяна в секцията!';
$_['error_vendor']       = 'Внимание: Посочването на номера на търговеца (ID) е задължително!';
?>