<?php //!
// Heading
$_['heading_title']      = 'PayPal Website Payment Pro (UK)';

// Текст 
$_['text_payment']       = 'Плащане';
$_['text_success']       = 'Готово, промените са запазени!';
$_['text_pp_pro_uk']     = '<a href="https://www.paypal.com/bg/mrb/pal=45ZYRMEVQJCD6" target="_blank"><img src="view/image/payment/paypal.png" alt="PayPal Website Payment Pro (UK)" title="PayPal Website Payment Pro (UK)" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization'] = 'Авторизация';
$_['text_sale']          = 'Продажба';

// Entry
$_['entry_username']     = 'API потребител:';
$_['entry_password']     = 'API парола:';
$_['entry_signature']    = 'API подпис:';
$_['entry_test']         = 'Тестов режим:<br /><span class="help">Кой портал за трансакции ще ползвате - за тестове или за работа в реални условия?</span>';
$_['entry_transaction']  = 'Трансакционен метод:';
$_['entry_total']        = 'Ценови праг:<br /><span class="help">Сумата за плащане, която трябва да се събере в кошницата, преди този метод за плащане да стане активен.</span>';
$_['entry_order_status'] = 'Статус на поръчката:';
$_['entry_geo_zone']     = 'Гео-зона:';
$_['entry_status']       = 'Статус:';
$_['entry_sort_order']   = 'Поредност:';

// Error
$_['error_permission']   = 'Внимание: Нямате права за промяна в секцията!';
$_['error_username']     = 'Внимание: Посочването на API потребител е задължително!'; 
$_['error_password']     = 'Внимание: Посочването на API парола е задължително!'; 
$_['error_signature']    = 'Внимание: Посочването API подпис е задължително!'; 
?>