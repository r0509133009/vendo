<?php //!
// Heading
$_['heading_title']         = 'Klarna Фактура';

// Text
$_['text_payment']          = 'Плащане';
$_['text_success']          = 'Готово, промените са запазени!';
$_['text_klarna_invoice']   = '<a onclick="window.open(\'https://merchants.klarna.com/signup?locale=en&partner_id=d5c87110cebc383a826364769047042e777da5e8&utm_campaign=Platform&utm_medium=Partners&utm_source=Opencart\');"><img src="view/image/payment/klarna.png" alt="Klarna" title="Klarna" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_live']             = 'На живо';
$_['text_beta']             = 'Тестов режим';
$_['text_sweden']           = 'Швеция';
$_['text_norway']           = 'Норвегия';
$_['text_finland']          = 'Финландия';
$_['text_denmark']          = 'Дания';
$_['text_germany']          = 'Германия';
$_['text_netherlands']      = 'Холандия';

// Entry
$_['entry_merchant']        = 'Номер на търговец (ID):<br /><span class="help">(estore id) за ползване услугите на Klarna (дава се от Klarna).</span>';
$_['entry_secret']          = 'Код:<br /><span class="help">Код (парола) за ползване услугите на Klarna (дава се от Klarna).</span>';
$_['entry_server']          = 'Сървър:';
$_['entry_total']           = 'Ценови праг:<br /><span class="help">Сумата за плащане, която трябва да се събере в кошницата, преди този метод за плащане да стане активен.</span>';		
$_['entry_pending_status']  = 'Статус в очакване:';
$_['entry_accepted_status'] = 'Статус при потвърджение:';
$_['entry_order_status']    = 'Статус на поръчка:';
$_['entry_geo_zone']        = 'Гео-зона:';
$_['entry_status']          = 'Статус:';
$_['entry_sort_order']      = 'Поредност:';

// Error
$_['error_permission']      = 'Внимание: Нямате права за промяна в секцията!';
$_['error_pclass']          = 'Внимание: Не може да извлече pClass за %s. Код на грешката: %s; Текст на грешката: %s';
$_['error_curl']            = 'Внимание: Curl грешка - Код: %d; Текст: %s';
$_['error_log']             = 'Внимание: При обновяването на модула възникна грешка. Моля, проверете файла със системни съобщения.';
$_['error_xmlrpc']          = 'Внимание: Нужно е инсталирането на XML-RPC PHP разширение!';
$_['error_merchant']        = 'Внимание: Посочването на номера на търговеца (ID) е задължително!';
$_['error_secret']          = 'Внимание: Посочването на кода е задължително!';
?>