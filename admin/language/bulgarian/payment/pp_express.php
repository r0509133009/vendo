<?php //!
// Heading
$_['heading_title']      = 'PayPal Express';

// Текст 
$_['text_payment']       = 'Плащане';
$_['text_success']       = 'Готово, промените са запазени!';
$_['text_pp_express']    = '<a href="https://www.paypal.com/bg/mrb/pal=45ZYRMEVQJCD6" taget="_blank"><img src="view/image/payment/paypal.png" alt="PayPal" title="PayPal" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization'] = 'Авторизация';
$_['text_sale']          = 'Продажба';
$_['text_clear']                        = 'Изчисти';
$_['text_browse']                       = 'Посочи';
$_['text_image_manager']                = 'Управление на изображения';
$_['text_ipn']                          = 'IPN url<span class="help">Задължително за абонаменти</span>';

// Entry
$_['entry_username']     = 'API Потребител:';
$_['entry_password']     = 'API Парола:';
$_['entry_signature']    = 'API Подпис:';
$_['entry_test']         = 'Тестов режим:';
$_['entry_method']       = 'Трансакционен метод:';
$_['entry_total']        = 'Ценови праг:<br /><span class="help">Сумата за плащане, която трябва да се събере в кошницата, преди този метод за плащане да стане активен.</span>';
$_['entry_order_status'] = 'Статус на поръчката:';
$_['entry_geo_zone']     = 'Гео-зона:';
$_['entry_status']       = 'Статус:';
$_['entry_sort_order']   = 'Поредност:';
$_['entry_icon_sort_order']             = 'Ред на икноките:';
$_['entry_debug']                       = 'Запис на системна информация:';
$_['entry_currency']                    = 'Основна валута<span class="help">За търсене на трансакции</span>';
$_['entry_profile_cancellation']        = 'Позволи на клиентите да прекратяват профилите си.';

// Order Status
$_['entry_canceled_reversal_status']    = 'Canceled Reversal Status:';
$_['entry_completed_status']            = 'Completed Status:';
$_['entry_denied_status']			    = 'Denied Status:';
$_['entry_expired_status']			    = 'Expired Status:';
$_['entry_failed_status']			    = 'Failed Status:';
$_['entry_pending_status']			    = 'Pending Status:';
$_['entry_processed_status']		    = 'Processed Status:';
$_['entry_refunded_status']			    = 'Refunded Status:';
$_['entry_reversed_status']			    = 'Reversed Status:';
$_['entry_voided_status']		        = 'Voided Status:';

// Customise area
$_['entry_display_checkout']            = 'Display quick checkout icon:';
$_['entry_allow_notes']                 = 'Allow notes:';
$_['entry_logo']                        = 'Logo<span class="help">Max 750px(w) x 90px(h)<br />You should only use a logo if you have SSL set up.</span>';
$_['entry_border_colour']               = 'Header border colour:<span class="help">6 character HTML colour code</span>';
$_['entry_header_colour']               = 'Header background colour:<span class="help">6 character HTML colour code</span>';
$_['entry_page_colour']                 = 'Page background colour:<span class="help">6 character HTML colour code</span>';


// Error
$_['error_permission']   = 'Внимание: Нямате права за промяна в секцията!';
$_['error_username']     = 'Внимание: Посочването на API на потребител е задължително!'; 
$_['error_password']     = 'Внимание: Посочването на API за парола е задължително!'; 
$_['error_signature']    = 'Внимание: Посочването на API за подпис е задължително!'; 
$_['error_data']         = 'Внимание: В заявката липсва информация.';
$_['error_timeout']      = 'Внимание: Заявката просрочи времето за обработка.';
?>