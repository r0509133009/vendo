<?php //!
// Heading
$_['heading_title']      = 'SagePay (US)';

// Текст 
$_['text_payment']       = 'Плащане'; 
$_['text_success']       = 'Готово, промените са запазени!';

// Entry
$_['entry_merchant_id']  = 'Номер на търговец (ID):';
$_['entry_merchant_key'] = 'Код на търговец (key):';
$_['entry_total']        = 'Ценови праг:<br /><span class="help">Сумата за плащане, която трябва да се събере в кошницата, преди този метод за плащане да стане активен.</span>';
$_['entry_order_status'] = 'Статус на поръчката:';
$_['entry_geo_zone']     = 'Гео-зона:';
$_['entry_status']       = 'Статус:';
$_['entry_sort_order']   = 'Поредност:';

// Error
$_['error_permission']   = 'Внимание: Нямате права за промяна в секцията!';
$_['error_merchant_id']  = 'Внимание: Посочването на номера на търговеца (ID) е задължително!';
$_['error_merchant_key'] = 'Внимание: Посочването на кода на търговеца (key) е задължително!';
?>