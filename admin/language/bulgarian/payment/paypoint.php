<?php //!
// Heading
$_['heading_title']      = 'PayPoint'; 

// Текст 
$_['text_payment']       = 'Плащане';
$_['text_success']       = 'Готово, промените са запазени!';
$_['text_paypoint']      = '<a href="https://www.paypoint.net/partners/opencart" target="_blank"><img src="view/image/payment/paypoint.png" alt="PayPoint" title="PayPoint" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_live']          = 'На живо';
$_['text_successful']    = 'Винаги успешно';
$_['text_fail']          = 'Винаги неуспешно';

// Entry
$_['entry_merchant']     = 'Номер на търговец (ID):';
$_['entry_password']     = 'Парола за достъп:<br /><span class="help">Не попълвайте, ако нямате активен "Digest Key Authentication" към профила си.</span>';
$_['entry_test']         = 'Тестов режим:';
$_['entry_total']        = 'Ценови праг:<br /><span class="help">Сумата за плащане, която трябва да се събере в кошницата, преди този метод за плащане да стане активен.</span>';
$_['entry_order_status'] = 'Статус на поръчката:';
$_['entry_geo_zone']     = 'Гео-зона:';
$_['entry_status']       = 'Статус:'; 
$_['entry_sort_order']   = 'Поредност:';

// Error
$_['error_permission']   = 'Внимание: Нямате права за промяна в секцията';
$_['error_merchant']     = 'Внимание: Посочването на номера на търговеца (ID) е задължително!';
?>