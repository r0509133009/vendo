<?php //!
//Текст
$_['text_payment_info']         = 'Информация за плащане';
$_['text_capture_status']       = 'Capture status';
$_['text_amount_auth']          = 'Amount authorised';
$_['text_amount_captured']      = 'Amount captured';
$_['text_amount_refunded']      = 'Amount refunded';
$_['text_capture_amount']       = 'Capture amount';
$_['text_complete_capture']     = 'Complete capture';
$_['text_transactions']         = 'Трансакции';
$_['text_complete']             = 'Изпълни';
$_['text_confirm_void']         = 'If you void you cannot capture any further funds';
$_['text_view']                 = 'Виж';
$_['text_refund']               = 'Възстанови';
$_['text_resend']               = 'Изпрати отново';

//Table columns
$_['column_trans_id']           = 'Номер на трансакция (ID)';
$_['column_amount']             = 'Сума';
$_['column_type']               = 'Вид плащане';
$_['column_status']             = 'Статус';
$_['column_pend_reason']        = 'Причина за изчакване';
$_['column_created']            = 'Дата на създаване';
$_['column_action']             = 'Действие';

//Buttons
$_['btn_void']                  = 'Void';
$_['btn_capture']               = 'Capture';

//Messages
$_['success_transaction_resent']= 'Трансакцията беше успешно изпратена повторно.';

//Errors
$_['error_capture_amt']         = 'Enter an amount to capture';
$_['error_timeout']             = 'Request timed out';
$_['error_transaction_missing'] = 'Transaction could not be found';
?>