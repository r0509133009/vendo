<?php //!
// Heading
$_['heading_title']       = 'Klarna Плащане';

// Text
$_['text_payment']        = 'Плащане';
$_['text_success']        = 'Готово, промените са запазени!';
$_['text_klarna_pp']      = '<a onclick="window.open(\'https://merchants.klarna.com/signup?locale=en&partner_id=d5c87110cebc383a826364769047042e777da5e8&utm_campaign=Platform&utm_medium=Partners&utm_source=Opencart\');"><img src="view/image/payment/klarna.png" alt="Klarna" title="Klarna" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_live']           = 'На живо';
$_['text_beta']           = 'Тестов режим';

// Entry
$_['entry_merchant']      = 'Номер на търговец (ID):<br /><span class="help">(estore id) за ползване услугите на Klarna (дава се от Klarna).</span>';
$_['entry_secret']        = 'Код:<br /><span class="help">Код (парола) за ползване услугите на Klarna (дава се от Klarna).</span>';
$_['entry_server']        = 'Сървър:';
$_['entry_total']         = 'Ценови праг:<br /><span class="help">Сумата за плащане, която трябва да се събере в кошницата, преди този метод за плащане да стане активен.</span>';		
$_['entry_order_status']  = 'Статус на поръчка:';
$_['entry_geo_zone']      = 'Гео-зона:';
$_['entry_status']        = 'Статус:';
$_['entry_sort_order']    = 'Поредност:';

// Error
$_['error_permission']    = 'Внимание: Нямате права за промяна в секцията!';
$_['error_xmlrpc']        = 'Внимание: Нужно е инсталирането на XML-RPC PHP разширение!';
$_['error_merchant']      = 'Внимание: Посочването на номера на търговеца (ID) е задължително!';
$_['error_secret']        = 'Внимание: Посочването на кода е задължително!';
?>