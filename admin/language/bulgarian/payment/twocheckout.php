<?php //!
// Heading
$_['heading_title']      = '2Checkout';

// Текст 
$_['text_payment']       = 'Плащане';
$_['text_success']       = 'Готово, промените са запазени!';
$_['text_twocheckout']	 = '<a href="https://www.2checkout.com/2co/affiliate?affiliate=1596408" target="_blank"><img src="view/image/payment/2checkout.png" alt="2Checkout" title="2Checkout" style="border: 1px solid #EEEEEE;" /></a>';

// Entry
$_['entry_account']      = 'Номер на търговец (ID):';
$_['entry_secret']       = 'Тайна дума:<br /><span class="help">Тайната дума за потвърждаване на трансакциите (трябва да е същата, посочена в страницата за настройка на профила на търговеца).</span>';
$_['entry_test']         = 'Тестов режим:';
$_['entry_total']        = 'Ценови праг:<br /><span class="help">Сумата за плащане, която трябва да се събере в кошницата, преди този метод за плащане да стане активен.</span>';
$_['entry_order_status'] = 'Статус на поръчката:';
$_['entry_geo_zone']     = 'Гео-зона:';
$_['entry_status']       = 'Статус:';
$_['entry_sort_order']   = 'Поредност:';

// Error
$_['error_permission']   = 'Внимание: Нямате права за промяна в секцията!';
$_['error_account']      = 'Внимание: Посочването на номера на търговеца (ID) е задължително!';
$_['error_secret']       = 'Внимание: Посочването на тайната дума е задължително!';
?>