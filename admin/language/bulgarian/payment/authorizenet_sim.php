<?php //!
// Heading
$_['heading_title']      = 'Authorize.Net (AIM)';

// Текст 
$_['text_payment']       = 'Плащане';
$_['text_success']       = 'Готово, промените са запазени!';

// Entry
$_['entry_merchant']     = 'Номер на търговец (ID):';
$_['entry_key']          = 'Трансакционен код:';
$_['entry_callback']     = 'Relay Response URL:<br /><span class="help">Моля, влезте в профила си, за да зададете този адрес (<a href="https://secure.authorize.net" target="_blank" class="txtlink">Вход</a>.</span>)';
$_['entry_test']         = 'Тестов режим:';
$_['entry_total']        = 'Ценови праг:<br /><span class="help">Сумата за плащане, която трябва да се събере в кошницата, преди този метод за плащане да стане активен.</span>';
$_['entry_order_status'] = 'Статус на поръчката:';
$_['entry_geo_zone']     = 'Гео-зона:';
$_['entry_status']       = 'Статус:'; 
$_['entry_sort_order']   = 'Поредност:';

// Error 
$_['error_permission']   = 'Внимание: Нямате права за промяна в секцията!';
$_['error_merchant']     = 'Внимание: Посочването на номера на търговеца (ID) е задължително!';
$_['error_key']          = 'Внимание: Посочването на трансакционния код е задължително!';
?>