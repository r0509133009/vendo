<?php //!
// Heading
$_['heading_title']      = 'Свободно от плащане (при нулева стойност на покупките)';

// Текст
$_['text_payment']       = 'Плащане';
$_['text_success']       = 'Готово, промените са запазени!';

// Entry
$_['entry_order_status'] = 'Статус на поръчката:';
$_['entry_status']       = 'Статус:';
$_['entry_sort_order']   = 'Поредност:';

// Error
$_['error_permission']   = 'Внимание: Нямате права за промяна в секцията!';
?>