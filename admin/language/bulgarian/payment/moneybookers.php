<?php //!
// Heading					
$_['heading_title']		        = 'Moneybookers';

// Текст 					
$_['text_payment']		        = 'Плащане';
$_['text_success']		        = 'Готово, промените са запазени!';
$_['text_moneybookers']	      = '<a href="https://www.moneybookers.com/partners/?p=OpenCart" target="_blank"><img src="view/image/payment/moneybookers.png" alt="Moneybookers" title="Moneybookers" style="border: 1px solid #EEEEEE;" /></a>';
	
// Entry					
$_['entry_email']		          = 'е-поща:';
$_['entry_secret']		        = 'Секретен код:';
$_['entry_total']             = 'Ценови праг:<br /><span class="help">Сумата за плащане, която трябва да се събере в кошницата, преди този метод за плащане да стане активен.</span>';
$_['entry_order_status']      = 'Статус на поръчката:';
$_['entry_pending_status']    = 'Статус при приемане:';
$_['entry_canceled_status']   = 'Статус при отказване:';
$_['entry_failed_status']     = 'Статус при неуспех:';
$_['entry_chargeback_status'] = 'Статус при възстановяване:';
$_['entry_geo_zone']          = 'Гео-зона:';
$_['entry_status']            = 'Статус:';
$_['entry_sort_order']        = 'Ред:';

// Error					
$_['error_permission']	      = 'Внимание: Нямате права за промяна в секцията!'; 
$_['error_email']		          = 'Внимание: Посочването на е-поща е задължително!';
$_['error_secret']		        = 'Внимание: Посочването на секретния код е задължително!';
?>