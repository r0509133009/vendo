<?php //!
// Heading
$_['heading_title']     = 'Модули за доставка';

// Текст
$_['text_install']      = 'Инсталирай';
$_['text_uninstall']    = 'Деинсталирай';

// Column
$_['column_name']       = 'Име на модула';
$_['column_status']     = 'Статус';
$_['column_sort_order'] = 'Ред';
$_['column_action']     = 'Действие';

// Error
$_['error_permission']  = 'Внимание: Нямате права за промяна в секцията!';
?>