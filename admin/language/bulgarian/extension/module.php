<?php //!
// Heading
$_['heading_title']    = 'Допълнителни модули';

// Текст
$_['text_install']     = 'Инсталирай';
$_['text_uninstall']   = 'Деинсталирай';

// Column
$_['column_name']      = 'Име на модула';
$_['column_action']    = 'Действие';

// Error
$_['error_permission'] = 'Внимание: Нямате права за промяна на модулите!';
?>