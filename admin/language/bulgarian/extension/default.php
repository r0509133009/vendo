<?php //!
$_['lang_openbay_new']              = 'Създай обява';
$_['lang_openbay_edit']             = 'Преглед/Редакция';
$_['lang_openbay_fix']              = 'Премахване грешки';
$_['lang_openbay_processing']       = 'Обработка';

$_['lang_amazonus_saved']           = 'Запазен (неизпратен)';
$_['lang_amazon_saved']             = 'Запазен (неизпратен)';

$_['lang_markets']                  = 'Пазари';
$_['lang_bulk_btn']                 = 'eBay групово изпращане';
$_['lang_bulk_amazon_btn']          = 'Amazon EU групово изпращане';

$_['lang_marketplace']              = 'Пазар';
$_['lang_status']                   = 'Статус';
$_['lang_option']                   = 'Опция';
?>