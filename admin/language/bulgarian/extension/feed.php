<?php //!
// Heading 
$_['heading_title']    = 'Продуктови емисии';

// Текст
$_['text_install']     = 'Инсталирай';
$_['text_uninstall']   = 'Деинсталирай';

// Column
$_['column_name']      = 'Име на продуктовата емисия';
$_['column_status']    = 'Статус';
$_['column_action']    = 'Действие';

// Error
$_['error_permission'] = 'Внимание: Нямате права за промяна на продуктови емисии!';
?>