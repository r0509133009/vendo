<?php //!
// Heading
$_['heading_title']    = 'Статуси за наличност';

// Текст
$_['text_success']     = 'Готово, промените са запазени!';

// Column
$_['column_name']      = 'Статуси';
$_['column_action']    = 'Действие';

// Entry
$_['entry_name']       = 'Стойност на статуса:';

// Error
$_['error_permission'] = 'Внимание: Нямате права за промяна на статуси за наличност!';
$_['error_name']       = 'Внимание: Дължината на статуса трябва да бъде между 3 и 32 символа!';
$_['error_product']    = 'Внимание: Този статус не може да бъде изтрит, тъй като е асоцииран с %s продукта!';
?>
