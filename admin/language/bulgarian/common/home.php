<?php //!
// Heading
$_['heading_title']                 = 'Табло';

// Текст
$_['text_overview']                 = 'Общ преглед';
$_['text_statistics']               = 'Статистика';
$_['text_latest_10_orders']         = 'Последните 10 поръчки';
$_['text_total_sale']               = 'Общо продажби:';
$_['text_total_sale_year']          = 'Общо продажби тази година:';
$_['text_total_order']              = 'Общо поръчки:';
$_['text_total_customer']           = 'Брой клиенти:';
$_['text_total_customer_approval']  = 'Клиенти, очакващи одобрение:';
$_['text_total_review_approval']    = 'Отзиви, очакващи одобрение:';
$_['text_total_affiliate']          = 'Брой партньори:';
$_['text_total_affiliate_approval'] = 'Партньори, очакващи одобрение:';
$_['text_day']                      = 'Днес';
$_['text_week']                     = 'Тази седмица';
$_['text_month']                    = 'Този месец';
$_['text_year']                     = 'Тази година';
$_['text_order']                    = 'Общо поръчки';
$_['text_customer']                 = 'Общо клиенти';

// Column 
$_['column_order']                  = 'Поръчка №';
$_['column_customer']               = 'Клиент';
$_['column_status']                 = 'Статус';
$_['column_date_added']             = 'Дата на добавяне';
$_['column_total']                  = 'Всичко';
$_['column_firstname']              = 'Име';
$_['column_lastname']               = 'Фамилия';
$_['column_action']                 = 'Действие';

// Entry
$_['entry_range']                   = 'Изберете интервал:';

// Error
$_['error_install']                 = 'Внимание: Инсталационната папка не е изтрита!';
$_['error_image']                   = 'Внимание: Папката за изображения %s не позволява записване!';
$_['error_image_cache']             = 'Внимание: Папката за кеширане на изображения %s не позволява записване!';
$_['error_cache']                   = 'Внимание: Папката за кеширане %s не позволява записване!';
$_['error_download']                = 'Внимание: Папката за изтеглени файлове %s не позволява записване!';
$_['error_logs']                    = 'Внимание: Папката за системни съобщения %s не позволява записване!';
?>