<?php //!
// Heading
$_['heading_title']             = 'Партньори-комисионери';

// Текст
$_['text_success']              = 'Готово, промените са запазени!';
$_['text_approved']             = 'Профилът на %s е одобрен!';
$_['text_wait']                 = 'Моля, почакайте!';
$_['text_balance']              = 'Баланс:';
$_['text_cheque']               = 'Чек';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'Банков превод';

// Column
$_['column_name']               = 'Име на партньора';
$_['column_email']              = 'Е-поща';
$_['column_code']               = 'Проследяващ код';
$_['column_balance']            = 'Баланс';
$_['column_status']             = 'Статус';
$_['column_approved']           = 'Одобрен';
$_['column_date_added']         = 'Дата на добавяне';
$_['column_description']        = 'Описание';
$_['column_amount']             = 'Сума';
$_['column_action']             = 'Действие';

// Entry
$_['entry_firstname']           = 'Име:';
$_['entry_lastname']            = 'Фамилия:';
$_['entry_email']               = 'е-поща:';
$_['entry_telephone']           = 'Телефон:';
$_['entry_fax']                 = 'Факс:';
$_['entry_status']              = 'Статус:';
$_['entry_password']            = 'Парола';
$_['entry_confirm']             = 'Потвърди:';
$_['entry_company']             = 'Фирма:';
$_['entry_address_1']           = 'Адрес:';
$_['entry_address_2']           = 'Допълнение към адреса:';
$_['entry_city']                = 'Населено място:';
$_['entry_postcode']            = 'Пощ. код:';
$_['entry_country']             = 'Страна:';
$_['entry_zone']                = 'Област:';
$_['entry_code']                = 'Проследяващ код:<span class="help">Кодът, служещ за проследяване резултатите от дейността на партньора.</span>';
$_['entry_commission']          = 'Комисиони (%):<span class="help">Процентът, който партньорите получават за всяка покупка от привлечените от тях клиенти.</span>';
$_['entry_tax']                 = 'Данъчен номер:';
$_['entry_payment']             = 'Начин на плащане:';
$_['entry_cheque']              = 'Трите имена на лицето, което ще осребрява чека:';
$_['entry_paypal']              = 'Адрес на е-поща към профил в PayPal:';
$_['entry_bank_name']           = 'Име на банка:';
$_['entry_bank_branch_number']  = 'ABA/BSB номер (номер на клон):';
$_['entry_bank_swift_code']     = 'SWIFT код:';
$_['entry_bank_account_name']   = 'Имена на собственика на сметката:';
$_['entry_bank_account_number'] = 'Номер на сметката:';
$_['entry_amount']              = 'Сума:';
$_['entry_description']         = 'Описание:';

// Error
$_['error_permission']          = 'Внимание: Нямате права за промяна в секцията!';
$_['error_exists']              = 'Внимание: Вече има регистриран профил с тази е-поща!';
$_['error_firstname']           = 'Внимание: Името трябва да е между 1 и 32 символа!';
$_['error_lastname']            = 'Внимание: Името трябва да е между 1 и 32 символа!';
$_['error_email']               = 'Внимание: Невалиден адрес на е-поща!';
$_['error_telephone']           = 'Внимание: Телефонът трябва да е между 3 и 32 символа!';
$_['error_password']            = 'Внимание: Паролата трябва да е между 4 и 20 символа!';
$_['error_confirm']             = 'Внимание: Внимание: Паролата в двете полета не е еднаква!';
$_['error_address_1']           = 'Внимание: Адресът трябва да е между 3 и 128 символа!';
$_['error_city']                = 'Внимание: Населеното място трябва да е между 2 и 128 символа!';
$_['error_postcode']            = 'Внимание: Пощенският код трябва да бъде между 2 и 10 символа for this country!';
$_['error_country']             = 'Внимание: Моля, посочете страна!';
$_['error_zone']                = 'Внимание: Моля, посочете област!';
$_['error_code']                = 'Внимание: Посочването на проследяващия код е задължително!';
?>