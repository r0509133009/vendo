<?php //!
// Heading
$_['heading_title']       = 'Връщания на продукти';

// Текст
$_['text_opened']         = 'Отварян';
$_['text_unopened']       = 'Неотварян';
$_['text_success']        = 'Готово, промените са запазени!';
$_['text_wait']           = 'Моля, почакайте!';

// Текст
$_['text_return_id']      = 'Входящ №:';
$_['text_order_id']       = 'Поръчка №:';
$_['text_date_ordered']   = 'Дата на поръчката:';
$_['text_customer']       = 'Клиент:';
$_['text_email']          = 'е-поща:';
$_['text_telephone']      = 'Телефон:';
$_['text_return_status']  = 'Статус на връщане:';
$_['text_date_added']     = 'Дата на създаване:';
$_['text_date_modified']  = 'Дата на промяна:';
$_['text_product']        = 'Продукт:';
$_['text_model']          = 'Модел:';
$_['text_quantity']       = 'Брой:';
$_['text_return_reason']  = 'Причина за връшане:';
$_['text_return_action']  = 'Предприето действие:';
$_['text_comment']        = 'Пояснение:';

// Column
$_['column_return_id']     = 'Входящ №';
$_['column_order_id']      = 'Поръчка №';
$_['column_customer']      = 'Клиент';
$_['column_product']       = 'Продукт';
$_['column_model']         = 'Модел';
$_['column_status']        = 'Статус';
$_['column_date_added']    = 'Дата на добавяне';
$_['column_date_modified'] = 'Дата на промяна';
$_['column_comment']       = 'Отзив';
$_['column_notify']        = 'Клиентът е уведомен';
$_['column_action']        = 'Действие';

// Entry
$_['entry_customer']      = 'Клиент:';
$_['entry_order_id']      = 'Поръчка №:';
$_['entry_date_ordered']  = 'Дата на поръчката:';
$_['entry_firstname']     = 'Име:';
$_['entry_lastname']      = 'Фамилия:';
$_['entry_email']         = 'е-поща:';
$_['entry_telephone']     = 'Телефон:';
$_['entry_product']       = 'Продукт:<br /><span class="help">(автоматично довършване)</span>';
$_['entry_model']         = 'Модел:';
$_['entry_quantity']      = 'Количество:';
$_['entry_reason']        = 'Причина за връщане:';
$_['entry_opened']        = 'Отварян:';
$_['entry_comment']       = 'Пояснение:';
$_['entry_return_status'] = 'Статус на връщане:';
$_['entry_notify']        = 'Уведоми клиента:';
$_['entry_action']        = 'Действие по връщане:';

// Error
$_['error_warning']       = 'Внимание: Моля, проверете внимателно формата за грешки!';
$_['error_permission']    = 'Внимание: Нямате права за промяна в секцията!';
$_['error_order_id']      = 'Внимание: Посочването на номер на поръчката е задължително!';
$_['error_firstname']     = 'Внимание: Името трябва да е между 1 и 32 символа!';
$_['error_lastname']      = 'Внимание: Фамилията трябва да е между 1 и 32 символа!';
$_['error_email']         = 'Внимание: Невалиден адрес на е-поща!';
$_['error_telephone']     = 'Внимание: Телефонът трябва да е между 3 и 32 символа!';
$_['error_product']       = 'Внимание: Името на продукта трябва да е между 3 и 255 символа!';
$_['error_model']         = 'Внимание: Името на модела трябва да е между 3 и 64 символа!';
?>