<?php //!
// Heading  
$_['heading_title']     = 'Ваучери';

// Текст
$_['text_send']         = 'Изпрати';
$_['text_success']      = 'Готово, промените са запазени!';
$_['text_sent']         = 'Готово, е-писмото бе изпратено!';
$_['text_wait']         = 'Моля, почакайте!';

// Column
$_['column_name']       = 'Име на ваучера';
$_['column_code']       = 'код';
$_['column_from']       = 'От';
$_['column_to']         = 'До';
$_['column_theme']      = 'Визия';
$_['column_amount']     = 'Сума';
$_['column_status']     = 'Статус';
$_['column_order_id']   = 'Поръчка №';
$_['column_customer']   = 'Клиент';
$_['column_date_added'] = 'Дата на добавяне';
$_['column_action']     = 'Действие';

// Entry
$_['entry_code']        = 'код:<br /><span class="help">Текстът, който клиентът трябва да въведе, за да активира ваучера.</span>';
$_['entry_from_name']   = 'Изпращач:';
$_['entry_from_email']  = 'Е-поща на изпращача:';
$_['entry_to_name']     = 'Получател:';
$_['entry_to_email']    = 'Е-поща на получателя:';
$_['entry_theme']       = 'Визия:';
$_['entry_message']     = 'Съобщение:';
$_['entry_amount']      = 'Сума:';
$_['entry_status']      = 'Статус:';

// Error
$_['error_permission']  = 'Внимание: Нямате права за промяна в секцията!';
$_['error_exists']      = 'Внимание: Този код вече се използва!';
$_['error_code']        = 'Внимание: Кодът трябва да бъде между 3 и 10 символа!';
$_['error_to_name']     = 'Внимание: Името на получателя трябва да бъде между 1 и 64 символа!';
$_['error_from_name']   = 'Внимание: Вашето име трябва да бъде между 1 и 64 символа!';
$_['error_email']       = 'Внимание: Невалиден адрес на е-поща!';
$_['error_amount']      = 'Внимание: Сумата трябва да е по-голяма или равна на 1!';
$_['error_order']       = 'Внимание: Този ваучер не може да бъде изтрит, тъй като е част от <a href="%s">поръчка</a>!';
?>
