<?php //!  
// Heading
$_['heading_title']        = 'Е-поща';

// Текст
$_['text_success']         = 'Съобщението бе изпратено успешно!';
$_['text_sent']            = 'Съобщението бе изпратено успешно до %s от %s адресата!';
$_['text_default']         = 'Основен';
$_['text_newsletter']      = 'Всички абонирали се за известия';
$_['text_customer_all']    = 'Всички клиенти';
$_['text_customer_group']  = 'Клиентска група';
$_['text_customer']        = 'Клиенти';
$_['text_affiliate_all']   = 'Всички партньори';
$_['text_affiliate']       = 'Партньорска програма';
$_['text_product']         = 'Артикули';

// Entry
$_['entry_store']          = 'От:';
$_['entry_to']             = 'До:';
$_['entry_customer_group'] = 'Клиентска група:';
$_['entry_customer']       = 'Клиент:<span class="help">(автоматично довършване)</span>';
$_['entry_affiliate']      = 'Партньор:<span class="help">(автоматично довършване)</span>';
$_['entry_product']        = 'Артикули:<br /><span class="help">Изпращане само до клиенти, закупили продукти от списъка.</span>';
$_['entry_subject']        = 'Тема:';
$_['entry_message']        = 'Съобщение:';

// Error
$_['error_permission']     = 'Внимание: Нямате права за промяна на send Е-поща\'s!';
$_['error_subject']        = 'Внимание: Посочването на тема е задължително!';
$_['error_message']        = 'Внимание: Въвеждането на съобщение е задължително!';
?>