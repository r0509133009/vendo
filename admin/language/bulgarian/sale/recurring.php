<?php //!

$_['heading_title'] = 'Платежни профили';

$_['text_success'] = 'Готово, промените са запазени!';
$_['text_payment_profiles'] = 'Платежни профили';
$_['text_status_active'] = 'Включен';
$_['text_status_inactive'] = 'Изключен';
$_['text_status_cancelled'] = 'Прекратен';
$_['text_status_suspended'] = 'Спрян';
$_['text_status_expired'] = 'Изтекъл';
$_['text_status_pending'] = 'В изчакване';
$_['text_transactions'] = 'Трансакции';
$_['text_return'] = 'Връщане';
$_['text_cancel'] = 'Отказ';
$_['text_filter'] = 'Филтър';

$_['text_cancel_confirm'] = "Прекратяване на профил не може да бъде отменено. Желаете ли да продължите?";

$_['text_transaction_created'] = 'Създадено';
$_['text_transaction_payment'] = 'Плащане';
$_['text_transaction_outstanding_payment'] = 'Чакащо плащане';
$_['text_transaction_skipped'] = 'Пропуснато плащане';
$_['text_transaction_failed'] = 'Плащането е неуспешно';
$_['text_transaction_cancelled'] = 'Прекратено';
$_['text_transaction_suspended'] = 'Спряно';
$_['text_transaction_suspended_failed'] = 'Спряно след неуспешно плащане';
$_['text_transaction_outstanding_failed'] = 'Чакащото плащане е неупешно.';
$_['text_transaction_expired'] = 'Изтекло';

$_['entry_cancel_payment'] = 'Отмени плащане:';
$_['entry_order_recurring'] = 'ID:';
$_['entry_order_id'] = 'Номер на поръчка (ID):';
$_['entry_payment_reference'] = 'Номер на плащането';
$_['entry_customer'] = 'Клиент';
$_['entry_date_created'] = 'Дата на създаване';
$_['entry_status'] = 'Статус';
$_['entry_type'] = 'Тип';
$_['entry_action'] = 'Действие';
$_['entry_email'] = 'Е-поща';
$_['entry_profile_description'] = "Описание на профила";
$_['entry_product'] = "Артикул:";
$_['entry_quantity'] = "Брой:";
$_['entry_amount'] = "Сума:";
$_['entry_profile'] = "Профил:";
$_['entry_payment_type'] = "Начин на плащане:";

$_['error_not_cancelled'] = 'Грешка: %s';
$_['error_not_found'] = 'Профилът не може да бъде прекратен.';
$_['success_cancelled'] = 'Абонаметното плащане бе прекратено';
?>