<?php //!
// Heading
$_['heading_title']    = 'Черен списък (по IP на клиент)';

// Text
$_['text_success']     = 'Готово, Успешно променихте вашия клиентски черен списък!';

// Column
$_['column_ip']        = 'IP адрес';
$_['column_customer']  = 'Клиенти';
$_['column_action']    = 'Действие';

// Entry
$_['entry_ip']         = 'IP:';

// Error
$_['error_permission'] = 'Внимание: Нямате права за промяна в секцията!';
$_['error_ip']         = 'Внимание: IP адресът трябва да е между 1 и 15 символа!';
?>