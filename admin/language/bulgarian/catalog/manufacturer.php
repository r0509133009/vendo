<?php //!
// Heading
$_['heading_title']      = 'Производител';

// Текст
$_['text_success']       = 'Готово, промените са запазени!';
$_['text_default']       = 'Основен';
$_['text_image_manager'] = 'Управление на изображения';
$_['text_browse']        = 'Посочи';
$_['text_clear']         = 'Премахни';
$_['text_percent']       = 'Процент';
$_['text_amount']        = 'Фиксирана сума';

// Column
$_['column_name']        = 'Име на производителя';
$_['column_sort_order']  = 'Ред';
$_['column_action']      = 'Действие';

// Entry
$_['entry_name']         = 'Име на производителя:';
$_['entry_store']        = 'Магазини:';
$_['entry_keyword']      = 'Оптимизиран адрес (SEO URL):<br/><span class="help">трябва да е написан с тирета между отделните думи, без интервали и за предпочитане на латиница, като не трябва да се дублира с друг оптимизиран адрес.</span>';
$_['entry_image']        = 'Изображение:';
$_['entry_sort_order']   = 'Поредност:';
$_['entry_type']         = 'Тип:';

// Error
$_['error_permission']   = 'Внимание: Нямате права за промяна на производители!';
$_['error_name']         = 'Внимание: Името на производителя трябва да бъде между 3 и 64 символа!';
$_['error_product']      = 'Внимание: Този производител не може да бъде изтрит, тъй като е асоцииран към %s продукт(а)!';
?>
