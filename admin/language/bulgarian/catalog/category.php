<?php //!
// Heading
$_['heading_title']          = 'Категория';

// Текст
$_['text_success']           = 'Готово, промените са запазени!';
$_['text_default']           = 'Основен';
$_['text_image_manager']     = 'Управление на изображения';
$_['text_browse']            = 'Посочи';
$_['text_clear']             = 'Премахни';

// Column
$_['column_name']            = 'Име на категория';
$_['column_sort_order']      = 'Ред';
$_['column_action']          = 'Действие';

// Entry
$_['entry_name']             = 'Име на категория:';
$_['entry_meta_keyword'] 	   = 'Мета ключови думи:';
$_['entry_meta_description'] = 'Мета описание:';
$_['entry_description']      = 'Описание:';
$_['entry_parent']           = 'Категория-родител:';
$_['entry_filter']           = 'Филтри:<br /><span class="help">(автоматично довършване)</span>';
$_['entry_store']            = 'Магазини:';
$_['entry_keyword']          = 'Оптимизиран адрес (SEO URL):<br/><span class="help">трябва да е написан с тирета между отделните думи, без интервали и за предпочитане на латиница, като не трябва да се дублира с друг оптимизиран адрес.</span>';
$_['entry_image']            = 'Изображение:';
$_['entry_top']              = 'Горно меню:<br/><span class="help">Включване в горното меню. Действа само за най-горните в йерархията категории.</span>';
$_['entry_column']           = 'Колони в подменюто:<br/><span class="help">Брой на колоните за списъка от елементи в подменюто на последните 3 родителски категории в горното меню.</span>';
$_['entry_sort_order']       = 'Поредност:';
$_['entry_status']           = 'Статус:';
$_['entry_layout']           = 'Промяна на изгледа:';

// Error 
$_['error_warning']          = 'Внимание: Моля, проверете внимателно формата за грешки!';
$_['error_permission']       = 'Внимание: Нямате права за промяна на категории!';
$_['error_name']             = 'Внимание: Името на категорията трябва да бъде между 2 и 32 символа!';
?>