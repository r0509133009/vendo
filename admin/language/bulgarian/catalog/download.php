<?php //!
// Heading
$_['heading_title']    = 'Е-продукти';

// Текст
$_['text_success']     = 'Готово, промените са запазени!';
$_['text_upload']      = 'Готово, файлът беше изпратен успешно!';

// Column
$_['column_name']      = 'Име';
$_['column_remaining'] = 'Общо разрешени изтегляния';
$_['column_action']    = 'Действие';

// Entry
$_['entry_name']       = 'Име на електронния продукт:';
$_['entry_filename']   = 'Име на файла:';
$_['entry_mask']       = 'Разширение:';
$_['entry_remaining']  = 'Общо разрешени изтегляния:';
$_['entry_update']     = 'Изпрати на предишни купувачи:<br /><span class="help">Обновяване и за вече закупените версии.</span>';

// Error
$_['error_permission'] = 'Внимание: Нямате права за промени по електронните продукти!';
$_['error_name']       = 'Внимание: Името трябва да бъде между 3 и 64 символа!';
$_['error_upload']     = 'Внимание: Изпращането е задължително!';
$_['error_filename']   = 'Внимание: Името на файла трябва да бъде между 3 и 128 символа!';
$_['error_exists']     = 'Внимание: Файлът не съществува!';
$_['error_mask']       = 'Внимание: Разширението следва да е между 3 и 128 символа!';
$_['error_filetype']   = 'Внимание: Невалиден тип файл!';
$_['error_product']    = 'Внимание: Този е-продукт не може да бъде изтрит, тъй като е асоцииран към %s продукт(а)!';
?>