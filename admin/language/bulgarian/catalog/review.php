<?php //!
// Heading
$_['heading_title']     = 'Отзиви';

// Текст
$_['text_success']      = 'Готово, промените са запазени!';

// Column
$_['column_product']    = 'Продукт';
$_['column_author']     = 'Автор';
$_['column_rating']     = 'Оценка';
$_['column_status']     = 'Статус';
$_['column_date_added'] = 'Дата на добавяне';
$_['column_action']     = 'Действие';

// Entry
$_['entry_product']     = 'Продукт:<br /><span class="help">(автоматично довършване)</span>';
$_['entry_author']      = 'Автор:';
$_['entry_rating']      = 'Оценка:';
$_['entry_status']      = 'Статус:';
$_['entry_text']        = 'Текст:';
$_['entry_good']        = 'Добър';
$_['entry_bad']         = 'Лош';

// Error
$_['error_permission']  = 'Внимание: Нямате права за промяна на отзиви!';
$_['error_product']     = 'Внимание: Посочването на продукт е задължително!';
$_['error_author']      = 'Внимание: Името на автора трябва да бъде между 3 и 64 символа!';
$_['error_text']        = 'Внимание: Текстът на отзива трябва да е поне 1 символ!';
$_['error_rating']      = 'Внимание: Изборът на рейтинг е задължително!';
?>
