<?php //!
// Heading
$_['heading_title'] = 'Абонаменти';

// Button
$_['button_insert'] = 'Вмъкни';
$_['button_copy'] = 'Копирай';
$_['button_delete'] = 'Изтрий';
$_['button_remove'] = 'Премахни';

// Текст
$_['text_no_results'] = 'Няма резултати';
$_['text_remove'] = 'Премахни';
$_['text_edit'] = 'Редактирай';
$_['text_enabled'] = 'Включено';
$_['text_disabled'] = 'Изключено';
$_['text_success'] = 'Профилът бе успешно добавен.';
$_['text_day'] = 'Ден';
$_['text_week'] = 'Седмица';
$_['text_semi_month'] = 'Половин месец';
$_['text_month'] = 'Месец';
$_['text_year'] = 'Година';

// Entry
$_['entry_name'] = 'Име:';
$_['entry_sort_order'] = 'Поредност:';
$_['entry_price'] = 'Цена:';
$_['entry_duration'] = 'Продължителност:';
$_['entry_status'] = 'Статус:';
$_['entry_cycle'] = 'Цикъл:';
$_['entry_frequency'] = 'Период:';
$_['entry_trial_price'] = 'Пробна цена:';
$_['entry_trial_duration'] = 'Пробна продължителност:';
$_['entry_trial_status'] = 'Пробен статус:';
$_['entry_trial_cycle'] = 'Пробен цикъл:';
$_['entry_trial_frequency'] = 'Пробен период:';

// Column
$_['column_name'] = 'Име';
$_['column_sort_order'] = 'Ред';
$_['column_action'] = 'Действие';

// Error
$_['error_warning'] = 'Внимание: Моля, проверете внимателно формата за грешки!';
$_['error_permission'] = 'Внимание: Нямате права за промяна на профили!';
$_['error_name'] = 'Внимание: името на профила трябва да е между 3 и 255 символа!';

// Help
$_['text_recurring_help']    = 'Повтарящите се плащания (абонаменти) са на циклична база. <br />Например, ако периодът е "седмица", а цикълът е "2", клиентът ще бъде таксуван на всеки 2 седмици.<br />Продължителността указва колко пъти ще се повтори цикълът.';
?>