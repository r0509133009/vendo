<?php //!
// Heading
$_['heading_title']          = 'Артикули'; 

// Текст  
$_['text_success']           = 'Готово, промените са запазени!';
$_['text_plus']              = '+';
$_['text_minus']             = '-';
$_['text_default']           = 'Основен';
$_['text_image_manager']     = 'Управление на изображения';
$_['text_browse']            = 'Посочи';
$_['text_clear']             = 'Премахни';
$_['text_option']            = 'Вариант';
$_['text_option_value']      = 'Признак';
$_['text_percent']           = 'Процент';
$_['text_amount']            = 'Фиксирана сума';

// Column
$_['column_name']            = 'Име на продукта';
$_['column_model']           = 'Модел';
$_['column_image']           = 'Изображение';
$_['column_price']           = 'Цена';
$_['column_quantity']        = 'Количество';
$_['column_status']          = 'Статус';
$_['column_action']          = 'Действие';

// Entry
$_['entry_name']             = 'Име на продукта:';
$_['entry_meta_keyword'] 	 = 'Мета ключови думи:';
$_['entry_meta_description'] = 'Мета описание:';
$_['entry_description']      = 'Описание:';
$_['entry_store']            = 'Магазини:';
$_['entry_keyword']          = 'Оптимизиран адрес (SEO URL):<br/><span class="help">трябва да е написан с тирета между отделните думи, без интервали и за предпочитане на латиница, като не трябва да се дублира с друг оптимизиран адрес.</span>';
$_['entry_model']            = 'Модел:';
$_['entry_sku']              = 'Артикулен номер:<br/><span class="help">Номер или друг идентификационен код, който е уникален за всеки продукт и се използва от фирмата за каталогизиране и управление на продуктите, с които търгува.</span>';
$_['entry_upc']              = 'Универсален продуктов код:<br/><span class="help">Международен баркод (ако продуктът има).</span>';
$_['entry_ean']              = 'ЕАН:<br/><span class="help">Европейски артикулен номер</span>';
$_['entry_jan']              = 'ЯАН:<br/><span class="help">Японски артикулен номер</span>';
$_['entry_isbn']             = 'ISBN:<br/><span class="help">International Standard Book Number</span>';
$_['entry_mpn']              = 'MPN:<br/><span class="help">Manufacturer Part Number</span>';
$_['entry_location']         = 'Местоположение:';
$_['entry_manufacturer']     = 'Производител:<br /><span class="help">(автоматично довършване)</span>';
$_['entry_shipping']         = 'Изисква доставка:'; 
$_['entry_date_available']   = 'Наличен от:';
$_['entry_quantity']         = 'Количество:';
$_['entry_minimum']          = 'Минимално Брой:<br/><span class="help">задавате минималното количество за една поръчка</span>';
$_['entry_stock_status']     = 'Съобщение при изчерпване:<br/><span class="help">показва се, когато артикулът се изчерпи</span>';
$_['entry_tax_class']        = 'Данъчна група:';
$_['entry_price']            = 'Цена:';
$_['entry_points']           = 'Точки:<br/><span class="help">Наградни точки, с които продуктът може да бъде купен. Оставете "0", ако не искате купуването на продукта с точки да е възможно.</span>';
$_['entry_option_points']    = 'Точки:';
$_['entry_subtract']         = 'Следи наличност:';
$_['entry_weight_class']     = 'Мярка за тегло:';
$_['entry_weight']           = 'Тегло:';
$_['entry_length']           = 'Мярка за дължина:';
$_['entry_dimension']        = 'Размери (Д х Ш х В):';
$_['entry_image']            = 'Изображение:';
$_['entry_customer_group']   = 'Клиентска група:';
$_['entry_date_start']       = 'Начална дата:';
$_['entry_date_end']         = 'Край:';
$_['entry_priority']         = 'Приоритет:';
$_['entry_attribute']        = 'Атрибути:';
$_['entry_attribute_group']  = 'Група на атрибута:';
$_['entry_text']             = 'Текст:';
$_['entry_option']           = 'Вариант:';
$_['entry_option_value']     = 'Признак:';
$_['entry_required']         = 'Задължително:';
$_['entry_status']           = 'Статус:';
$_['entry_sort_order']       = 'Ред:';
$_['entry_category']         = 'Категории:<br /><span class="help">(автоматично довършване)</span>';
$_['entry_filter']           = 'Филтри:<br /><span class="help">(автоматично довършване)</span>';
$_['entry_download']         = 'Е-продукти:<br /><span class="help">(автоматично довършване)</span>';
$_['entry_related']          = 'Сходни артикули:<br /><span class="help">(автоматично довършване)</span>';
$_['entry_tag']          	 = 'Етикети за продукта:<br /><span class="help">разделени със запетая</span>';
$_['entry_reward']           = 'Наградни точки:';
$_['entry_layout']           = 'Промяна на изгледа:';
$_['entry_profile']          = 'Профил:';

$_['text_recurring_help']    = 'Повтарящите се плащания (абонаменти) са на циклична база. <br />Например, ако периодът е "седмица", а цикълът е "2", клиентът ще бъде таксуван на всеки 2 седмици.<br />Продължителността указва колко пъти ще се повтори цикълът.';
$_['text_recurring_title']   = 'Абонаменти';
$_['text_recurring_trial']   = 'Пробен период';
$_['entry_recurring']        = 'Плащане:';
$_['entry_recurring_price']  = 'Сума:';
$_['entry_recurring_freq']   = 'Период:';
$_['entry_recurring_cycle']  = 'Цикъл:<span class="help">Стойността трябва да е "1" или повече</span>';
$_['entry_recurring_length'] = 'Продължителност:<span class="help">0 = до отмяна</span>';
$_['entry_trial']            = 'Пробен период:';
$_['entry_trial_price']      = 'Пробна абонаментна сума:';
$_['entry_trial_freq']       = 'Пробен абонаментен период:';
$_['entry_trial_cycle']      = 'Пробен цикъл:<span class="help">Стойността трябва да е "1" или повече</span>';
$_['entry_trial_length']     = 'Пробна продължителност:';

$_['text_length_day']        = 'Ден';
$_['text_length_week']       = 'Седмица';
$_['text_length_month']      = 'Месец';
$_['text_length_month_semi'] = 'Половин месец';
$_['text_length_year']       = 'Година';

// Error
$_['error_warning']          = 'Внимание: Моля, проверете внимателно формата за грешки!';
$_['error_permission']       = 'Внимание: Нямате права за промяна на продукти!';
$_['error_name']             = 'Внимание: Името на продукта трябва да е между 3 и 255 символа!';
$_['error_model']            = 'Внимание: Името на модела на продукта трябва да е между 3 и 64 символа!';
?>