<?php //!
// Heading
$_['heading_title']    = 'Фиксирана цена';

// Текст
$_['text_shipping']    = 'Модули за доставка';
$_['text_success']     = 'Готово, промените са запазени!';

// Entry
$_['entry_cost']       = 'Цена:';
$_['entry_tax_class']  = 'Данъчна група:';
$_['entry_geo_zone']   = 'Гео-зона:';
$_['entry_status']     = 'Статус:';
$_['entry_sort_order'] = 'Ред:';

// Error
$_['error_permission'] = 'Внимание: Нямате права за промяна в секцията!';
?>