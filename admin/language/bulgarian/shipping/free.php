<?php //!
// Heading
$_['heading_title']    = 'Безплатна доставка';

// Текст 
$_['text_shipping']    = 'Модули за доставка';
$_['text_success']     = 'Готово, промените са запазени!';

// Entry
$_['entry_total']      = 'Минимална сума:<br /><span class="help">Сума на поръчката, над която ще важи възможността за безплатна доставка.</span>';
$_['entry_geo_zone']   = 'Гео-зона:';
$_['entry_status']     = 'Статус:';
$_['entry_sort_order'] = 'Ред:';

// Error
$_['error_permission'] = 'Внимание: Нямате права за промяна в секцията!';
?>