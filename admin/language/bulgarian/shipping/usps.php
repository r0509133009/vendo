<?php //!
// Heading
$_['heading_title']         = 'United States Postal Service';

// Текст
$_['text_shipping']         = 'Модули за доставка';
$_['text_success']          = 'Готово, промените са запазени!';
$_['text_domestic_00']      = 'First-Class Mail Parcel';
$_['text_domestic_01']      = 'First-Class Mail Large Envelope';
$_['text_domestic_02']      = 'First-Class Mail Letter';
$_['text_domestic_03']      = 'First-Class Mail Postcards';
$_['text_domestic_1']       = 'Priority Mail';
$_['text_domestic_2']       = 'Express Mail Hold for Pickup';
$_['text_domestic_3']       = 'Express Mail';
$_['text_domestic_4']       = 'Parcel Post';
$_['text_domestic_5']       = 'Bound Printed Matter';
$_['text_domestic_6']       = 'Media Mail';
$_['text_domestic_7']       = 'Library';
$_['text_domestic_12']      = 'First-Class Postcard Stamped';
$_['text_domestic_13']      = 'Express Mail Flat-Rate Envelope';
$_['text_domestic_16']      = 'Priority Mail Flat-Rate Envelope';
$_['text_domestic_17']      = 'Priority Mail Regular Flat-Rate Box';
$_['text_domestic_18']      = 'Priority Mail Keys and IDs';
$_['text_domestic_19']      = 'First-Class Keys and IDs';
$_['text_domestic_22']      = 'Priority Mail Flat-Rate Large Box';
$_['text_domestic_23']      = 'Express Mail Sunday/Holiday';
$_['text_domestic_25']      = 'Express Mail Flat-Rate Envelope Sunday/Holiday';
$_['text_domestic_27']      = 'Express Mail Flat-Rate Envelope Hold For Pickup';
$_['text_domestic_28']      = 'Priority Mail Small Flat-Rate Box';
$_['text_international_1']  = 'Express Mail International';
$_['text_international_2']  = 'Priority Mail International';
$_['text_international_4']  = 'Global Express Guaranteed (Document and Non-document)';
$_['text_international_5']  = 'Global Express Guaranteed Document used';
$_['text_international_6']  = 'Global Express Guaranteed Non-Document Rectangular shape';
$_['text_international_7']  = 'Global Express Guaranteed Non-Document Non-Rectangular';
$_['text_international_8']  = 'Priority Mail Flat Rate Envelope';
$_['text_international_9']  = 'Priority Mail Flat Rate Box';
$_['text_international_10'] = 'Express Mail International Flat Rate Envelope';
$_['text_international_11'] = 'Priority Mail Flat Rate Large Box';
$_['text_international_12'] = 'Global Express Guaranteed Envelope';
$_['text_international_13'] = 'First Class Mail International Letters';
$_['text_international_14'] = 'First Class Mail International Flats';
$_['text_international_15'] = 'First Class Mail International Parcels';
$_['text_international_16'] = 'Priority Mail Flat Rate Small Box';
$_['text_international_21'] = 'Пощенски картички';
$_['text_regular']          = 'Обикновен';
$_['text_large']            = 'Голям';
$_['text_rectangular']      = 'Правоъгълен';
$_['text_non_rectangular']  = 'Неправоъгълен';
$_['text_variable']         = 'Друга форма';

// Entry
$_['entry_user_id']         = 'Потребителско име (ID):';
$_['entry_postcode']        = 'Пощ. код:';
$_['entry_domestic']        = 'Местни услуги:';
$_['entry_international']   = 'Международни услуги:';
$_['entry_size']            = 'Размер:';
$_['entry_container']       = 'Контейнер:';
$_['entry_machinable']      = 'Машинно обработваем:';
$_['entry_dimension']       = 'Размери (Д х Ш х В):<br/><span class="help">Приблизителни размери на пакета. Тук не се посочват размерите на самия продукт.</span>';
$_['entry_display_time']    = 'Покажи времето за доставка:<br /><span class="help">Показване времето, за което пратката ще бъде доставена (напр.: Време за доставка от 3 до 5 дни)</span>';
$_['entry_display_weight']  = 'Покажи теглото:<br /><span class="help">Показване теглото на пратката (напр.: 2,7674 кг.)</span>';
$_['entry_weight_class']    = 'Мяркa за тегло:<br /><span class="help">Трябва да е зададена в паунди.</span>';
$_['entry_tax']             = 'Данък:';
$_['entry_geo_zone']        = 'Гео-зона:';
$_['entry_status']          = 'Статус:';
$_['entry_sort_order']      = 'Ред:';
$_['entry_debug']      	  	= 'Записване на системни съобщения:<br/><span class="help">Включване записването на информация в системната история.</span>';

// Error
$_['error_permission']      = 'Внимание: Нямате права за промяна в секцията!';
$_['error_user_id']         = 'Внимание: Посочването на потребителското име (ID) е задължително!';
$_['error_postcode']        = 'Внимание: Посочването на пощ. код е задължително!';
$_['error_width']         	= 'Внимание: Посочването на ширината е задължително!';
$_['error_length']        	= 'Внимание: Посочването на дължината е задължително!';
$_['error_height']        	= 'Внимание: Посочването на височината е задължително!';
$_['error_girth']         	= 'Внимание: Посочването на обиколката е задължително!';
?>