<?php //!
// Heading
$_['heading_title']                = 'UPS';

// Текст
$_['text_shipping']                = 'Модули за доставка';
$_['text_success']                 = 'Готово, промените са запазени!';
$_['text_regular_daily_pickup']    = 'Regular Daily Pickup';
$_['text_daily_pickup']            = 'Daily Pickup';
$_['text_customer_counter']        = 'Customer Counter';
$_['text_one_time_pickup']         = 'One Time Pickup';
$_['text_on_call_air_pickup']      = 'On Call Air Pickup';
$_['text_letter_center']           = 'Letter Center';
$_['text_air_service_center']      = 'Air Service Center';
$_['text_suggested_retail_rates']  = 'Suggested Retail Rates (UPS Store)';
$_['text_package']                 = 'Package';
$_['text_ups_letter']              = 'UPS Letter';
$_['text_ups_tube']                = 'UPS Tube';
$_['text_ups_pak']                 = 'UPS Pak';
$_['text_ups_express_box']         = 'UPS Express Box';
$_['text_ups_25kg_box']            = 'UPS 25kg box';
$_['text_ups_10kg_box']            = 'UPS 10kg box';
$_['text_us']                      = 'US Origin';
$_['text_ca']                      = 'Canada Origin';
$_['text_eu']                      = 'European Union Origin';
$_['text_pr']                      = 'Puerto Rico Origin';
$_['text_mx']                      = 'Mexico Origin';
$_['text_other']                   = 'All Other Origins';
$_['text_test']                    = 'Тест';
$_['text_production']              = 'На живо';	
$_['text_residential']             = 'Residential';
$_['text_commercial']              = 'Commercial';
$_['text_next_day_air']            = 'UPS Next Day Air';
$_['text_2nd_day_air']             = 'UPS Second Day Air';
$_['text_ground']                  = 'UPS Ground';
$_['text_3_day_select']            = 'UPS Three-Day Select';
$_['text_next_day_air_saver']      = 'UPS Next Day Air Saver';
$_['text_next_day_air_early_am']   = 'UPS Next Day Air Early A.M.';
$_['text_2nd_day_air_am']          = 'UPS Second Day Air A.M.';
$_['text_saver']                   = 'UPS Saver';
$_['text_worldwide_express']       = 'UPS Worldwide Express';
$_['text_worldwide_expedited']     = 'UPS Worldwide Expedited';
$_['text_standard']                = 'UPS Standard';
$_['text_worldwide_express_plus']  = 'UPS Worldwide Express Plus';
$_['text_express']                 = 'UPS Express';
$_['text_expedited']               = 'UPS Expedited';
$_['text_express_early_am']        = 'UPS Express Early A.M.';
$_['text_express_plus']            = 'UPS Express Plus';
$_['text_today_standard']          = 'UPS Today Standard';
$_['text_today_dedicated_courier'] = 'UPS Today Dedicated Courier';
$_['text_today_intercity']         = 'UPS Today Intercity';
$_['text_today_express']           = 'UPS Today Express';
$_['text_today_express_saver']     = 'UPS Today Express Saver';

// Entry
$_['entry_key']                    = 'Ключ за достъп:<span class="help">Въведете дадения ви XML ключ за достъп до тарифата на UPS.</span>';
$_['entry_username']               = 'Потребителско име:<span class="help">Въведете името на профила, с който ползвате услугите на UPS.</span>';
$_['entry_password']               = 'Парола:<span class="help">Въведете паролата за профила, с който ползвате услугите на UPS.</span>';
$_['entry_pickup']                 = 'Метод на предаване:<span class="help">Как предавате пратките на UPS (валидно само при изпращане от САЩ)?</span>';
$_['entry_packaging']              = 'Тип опаковка:<span class="help">Какъв вид опаковка ползвате?</span>';
$_['entry_classification']         = 'Клиентски класификационен код:<span class="help">01 - ако се таксувате с UPS профил и имате ежедневно вдигане на пратки от UPS, 03 - ако нямате UPS профил или ако се таксувате с UPS профил но нямате ежедневно вдигане, 04 - ако изпращате от магазин за търговия на дребно (валидно само при изпращане от САЩ)</span>';
$_['entry_origin']                 = 'Код за произход:<span class="help">Коя начална точка да се приложи (опцията засяга само това кои имена на продукти на UPS се показват на потребителя)</span>';
$_['entry_city']                   = 'Отправно населено място:<span class="help">Въведете името на населеното място, от където тръгва пратката.</span>';
$_['entry_state']                  = 'Отправна област:<span class="help">Въведете двубуквения код на областта, от която тръгва пратката.</span>';
$_['entry_country']                = 'Отправна държава:<span class="help">Въведете двубуквения код на страната, от която тръгва пратката.</span>';
$_['entry_postcode']               = 'Пощ. код:<span class="help">Въведете пощенския код на мястото, от където тръгва пратката.</span>';
$_['entry_test']                   = 'Тестов режим:<span class="help">Използване на модула в  тестов режим (Да) или жива среда (Не)?</span>';
$_['entry_quote_type']             = 'Вид тарифа:<span class="help">Тарифа за граждани или фирми.</span>';
$_['entry_service']                = 'Услуги:<span class="help">Изберете услугите на UPS, които да бъдат предложени.</span>';
$_['entry_insurance']              = 'Разреши застраховка:<span class="help">Разрешавате застраховане на стойност сумата на поръчката.</span>';
$_['entry_display_weight']         = 'Покажи теглото:<br /><span class="help">Показване теглото на пратката (напр.: 2,7674 кг.)</span>';
$_['entry_weight_class']           = 'Мярка за тегло:<span class="help">Задайте килограми или паунди.</span>';
$_['entry_length_class']           = 'Мярка за дължина:<span class="help">Задайте сантиметри или инчове.</span>';
$_['entry_dimension']			   = 'Размери (Д х Ш х В):';
$_['entry_tax_class']              = 'Данъчна група:';
$_['entry_geo_zone']               = 'Гео-зона:';
$_['entry_status']                 = 'Статус:';
$_['entry_sort_order']             = 'Ред:';
$_['entry_debug']      			   = 'Режим на изследване:<br /><span class="help">Запазвате изпратената/получената информация в системната история</span>';

// Error
$_['error_permission']             = 'Внимание: Нямате права за промяна в секцията!';
$_['error_key']                    = 'Внимание: Посочването на ключа за достъп е задължително!';
$_['error_username']               = 'Внимание: Посочването на потребителското име е задължително!';
$_['error_password']               = 'Внимание: Посочването на паролата е задължително!';
$_['error_city']                   = 'Внимание: Посочването на отправното населено място е задължително!';
$_['error_state']                  = 'Внимание: Посочването на отправната област е задължително!';
$_['error_country']                = 'Внимание: Посочването на отправната държава е задължително!';
$_['error_dimension']              = 'Внимание: Посочването на размери е задължително!';
?>