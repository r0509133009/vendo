<?php //!
// Heading
$_['heading_title']      = 'Australia Post';

// Text
$_['text_shipping']      = 'Модули за доставка';
$_['text_success']       = 'Готово, промените са запазени!';

// Entry
$_['entry_postcode']     = 'Пощ. код:';
$_['entry_express']      = 'Експресна доставка:';
$_['entry_standard']     = 'Стандартна доставка:';
$_['entry_display_time'] = 'Покажи времето за доставка:<br /><span class="help">Показвате на клиента времето за доставка (напр. Доставка в рамките на 3 до 5  дни).</span>';
$_['entry_weight_class'] = 'Мяркa за тегло:<br /><span class="help">Трябва да е зададена в паунди.</span>';
$_['entry_tax_class']    = 'Данък:';
$_['entry_geo_zone']     = 'Гео-зона:';
$_['entry_status']       = 'Статус:';
$_['entry_sort_order']   = 'Ред:';

// Error
$_['error_permission']   = 'Внимание: Нямате права за промяна в секцията!';
$_['error_postcode']     = 'Внимание: пощенският код трябва да е 4-цифрен!';
?>