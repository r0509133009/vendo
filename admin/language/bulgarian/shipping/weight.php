<?php //!
// Heading
$_['heading_title']    = 'Модули за доставка, базирана на тегло ';

// Текст
$_['text_shipping']    = 'Модули за доставка';
$_['text_success']     = 'Готово, промените са запазени!';

// Entry
$_['entry_rate']       = 'Ставки:<br /><span class="help">Пример: 5:10.00,7:12.00 - Тегло:Цена,Тегло:Цена и т.н.</span>';
$_['entry_tax_class']  = 'Данъчна група:';
$_['entry_geo_zone']   = 'Гео-зона:';
$_['entry_status']     = 'Статус:';
$_['entry_sort_order'] = 'Ред:';

// Error
$_['error_permission'] = 'Внимание: Нямате права за промяна в секцията!';
?>