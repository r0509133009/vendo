<?php //!
// Heading
$_['heading_title']       = 'Банери';

// Текст
$_['text_module']         = 'Допълнителни модули';
$_['text_success']        = 'Готово, промените са запазени!';
$_['text_content_top']    = 'Горе';
$_['text_content_bottom'] = 'Долу';
$_['text_column_left']    = 'Лява колона';
$_['text_column_right']   = 'Дясна колона';

// Entry
$_['entry_banner']        = 'Банери:';
$_['entry_dimension']     = 'Размер (Ш х В) и вид преоразмеряване:';
$_['entry_layout']        = 'Показване в страница:<br /><span class="help">Изберете типа страница, при отварянето на който от посетител на магазина, този модул да се показва.</span>';
$_['entry_position']      = 'Позиция:';
$_['entry_status']        = 'Статус:';
$_['entry_sort_order']    = 'Ред:';

// Error
$_['error_permission']    = 'Внимание: Нямате права за промяна на банери!';
$_['error_dimension']     = 'Внимание: Посочването на ширина и височина е задължително!';
?>