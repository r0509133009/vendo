<?php //!
// Heading
$_['heading_title']       = 'Google Talk';

// Текст
$_['text_module']         = 'Допълнителни модули';
$_['text_success']        = 'Готово, промените са запазени!';
$_['text_content_top']    = 'Горе';
$_['text_content_bottom'] = 'Долу';
$_['text_column_left']    = 'Лява колона';
$_['text_column_right']   = 'Дясна колона';

// Entry
$_['entry_code']          = 'Код за Google Talk:<br /><span class="help">Отворете <a href="http://www.google.com/talk/service/badge/New" target="_blank"><u>Създайте Google Talk chatback badge</u></a> и поставете генерирания код в полето.</span>';
$_['entry_layout']        = 'Показване в страница:<br /><span class="help">Изберете типа страница, при отварянето на който от посетител на магазина, този модул да се показва.</span>';
$_['entry_position']      = 'Позиция:';
$_['entry_status']        = 'Статус:';
$_['entry_sort_order']    = 'Ред:';

// Error
$_['error_permission']    = 'Внимание: Нямате права за промяна на modify module Google Talk!';
$_['error_code']          = 'Внимание: Въвеждането на код за Google Talk е задължително';
?>