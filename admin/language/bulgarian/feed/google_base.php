<?php //!
// Heading
$_['heading_title']    = 'Google Base';

// Текст   
$_['text_feed']        = 'Продуктови емисии';
$_['text_success']     = 'Готово, промените са запазени!';

// Entry
$_['entry_status']     = 'Статус:';
$_['entry_data_feed']  = 'Адрес на емисията:';

// Error
$_['error_permission'] = 'Внимание: Нямате права за промяна в секцията!';
?>