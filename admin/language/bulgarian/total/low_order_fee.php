<?php //!
// Heading
$_['heading_title']    = 'Такса за ниска стойност на поръчката';

// Текст
$_['text_total']       = 'Сума на поръчката';
$_['text_success']     = 'Готово, промените са запазени!';

// Entry
$_['entry_total']      = 'Сума:';
$_['entry_fee']        = 'Такса:';
$_['entry_tax_class']  = 'Данъчна група:';
$_['entry_status']     = 'Статус:';
$_['entry_sort_order'] = 'Ред:';

// Error
$_['error_permission'] = 'Внимание: Нямате права за промяна в секцията!';
?>