<?php //!
// Heading
$_['heading_title']    = 'Начин на доставка';

// Текст
$_['text_total']       = 'Сума на поръчката';
$_['text_success']     = 'Готово, промените са запазени!';

// Entry
$_['entry_estimator']  = 'Цена на доставката:';
$_['entry_status']     = 'Статус:';
$_['entry_sort_order'] = 'Ред:';

// Error
$_['error_permission'] = 'Внимание: Нямате права за промяна в секцията!';
?>