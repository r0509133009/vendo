<?php //!
// Heading
$_['heading_title']    = 'Klarna Такси';

// Text
$_['text_total']       = 'Сума на поръчката';
$_['text_success']     = 'Готово, промените са запазени!';
$_['text_sweden']      = 'Швеция';
$_['text_norway']      = 'Норвегия';
$_['text_finland']     = 'Финландия';
$_['text_denmark']     = 'Дания';
$_['text_germany']     = 'Германия';
$_['text_netherlands'] = 'Холандия';

// Entry
$_['entry_total']      = 'Ценови праг:<br /><span class="help">Сумата за плащане, която трябва да се събере в кошницата, преди този метод за плащане да стане активен.</span>:';
$_['entry_fee']        = 'Такса по фактурата:';
$_['entry_tax_class']  = 'Данъчен клас:';
$_['entry_status']     = 'Статус:';
$_['entry_sort_order'] = 'Поредност:';

// Error
$_['error_permission'] = 'Внимание: нямате права за промяна в секцията!';
?>