<?php
// Heading
$_['heading_title']	= 'Affiliate Campaign';
$_['heading_title_edit']	= 'Campaign Edit';
$_['heading_title_add']		= 'Campaign Create';
// Text
$_['text_enabled']	= 'Enable';
$_['text_disable']	= 'Disable';
$_['text_list']  	= 'Campaign List';
$_['text_action']	= 'Action';
$_['button_add'] = 'Insert';
$_['button_cancel'] = 'Cancel';
$_['button_save'] = 'Save';

$_['text_campaign_name'] = 'Campaign name';
$_['text_category']	= 'Category';
$_['text_description'] = 'Description';
$_['text_country'] = 'Country';
$_['text_date_create'] = 'Date Create';
$_['text_status'] = 'Status';
$_['text_no_campaign'] = 'No campaign!';
$_['text_confirm'] = 'Do you really want to delete?';

$_['text_name'] = 'Name';
$_['text_error_not_none'] = 'Error not none!';
$_['text_campaign_des'] = 'Campaign description';
$_['text_select_category'] = '--Select Category--';
$_['text_select_country']	= '--Select country--';