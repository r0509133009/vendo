<?php
// Heading
$_['heading_title']	= 'Affiliate Website';
$_['heading_title_edit']	= 'Banner Edit';
$_['heading_title_add']		= 'Banner Create';
// Text
$_['text_enabled']	= 'Enable';
$_['text_disable']	= 'Disable';
$_['text_list']  	= 'Banner List';
$_['text_action']	= 'Action';