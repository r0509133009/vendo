<?php
// Heading
$_['heading_title']	= 'Affiliate Stats';
$_['heading_title_edit']	= 'Banner Edit';
$_['heading_title_add']		= 'Banner Create';
// Text
$_['text_enabled']	= 'Enable';
$_['text_disable']	= 'Disable';
$_['text_list']  	= 'Banner List';
$_['text_action']	= 'Action';

$_['text_last_10day'] 	= 'Last 10 Days';
$_['text_impression']	= 'Impression';
$_['text_clicks']	= 'Clicks';
$_['text_last_day_month']	= 'Stats the day of month';
$_['text_date']	= 'Date';
$_['text_click'] = 'Click';
$_['text_sale']	= 'Sale';
$_['text_amount'] = 'Amount';
$_['text_ctr']	= 'CTR';
$_['text_cr']	= 'CR';
$_['text_event'] = 'Event';
$_['text_email'] = 'Email';
$_['text_description'] = 'Description';
$_['text_no_data'] = 'Nodata!';
$_['text_total']	= 'Total';
?>