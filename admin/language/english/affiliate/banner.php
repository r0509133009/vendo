<?php
// Heading
$_['heading_title']	= 'Affiliate Banner';
$_['heading_title_edit']	= 'Banner Edit';
$_['heading_title_add']		= 'Banner Create';
// Text
$_['text_enabled']	= 'Enable';
$_['text_disable']	= 'Disable';
$_['text_list']  	= 'Banner List';
$_['text_action']	= 'Action';
$_['button_add'] = 'Insert';
$_['text_date']	= 'Date';
$_['text_name'] = 'Name';
$_['text_bn_name'] = 'Banner name';
$_['text_size'] = 'Size';
$_['text_campaign'] = 'Campaign';
$_['text_image'] = 'Image';
$_['text_url_image'] = 'URL Image';
$_['text_impression'] = 'Impression';
$_['text_click'] = 'Click';
$_['text_status'] = 'Status';
$_['text_no_banner'] = 'No banner!';
$_['text_please_choose_img'] = 'Please choose url image!';
$_['text_preview'] = 'Preview';
$_['text_save'] = 'Save';
$_['text_cancel'] = 'Cancel';
$_['text_no_none'] = 'No empty!';
$_['text_option']	= 'Option';
$_['text_product']	= 'Product';
$_['text_select_size'] = '--Select Size--';
$_['text_description'] = 'Description';
$_['text_bn_description'] = 'Banner description';
$_['text_select_campaign'] = '--Select Campaign--';
$_['text_choose_img']	= 'Choose Img';
$_['button_add'] = 'Insert';
$_['button_cancel'] = 'Cancel';
$_['button_save'] = 'Save';