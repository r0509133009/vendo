<?php 
/*
  Opencart Delivery Status Extension for Opencart
  Version: 1.0, Feb 2014
  Author: Danny Kessels opencart@parkstadmedia.nl
*/

?>
<?php
// Heading
$_['heading_title']    = 'Delivery Status';

// Text
$_['text_success']     = 'Success: You have modified delivery statuses!';
$_['text_current_version']     = '1.0';

// Column
$_['column_name']      = 'Delivery Status Name';
$_['column_action']    = 'Action';

// Tabs
$_['tab_statuses']      = 'Statuses';
$_['tab_info']    = 'Info';
$_['tab_contact']    = 'Contact';

// Description
$_['description']	= 'Thank you for using The Delivery Status Extension!<br /><br /><br />Before you can use this Opencart extension, you first have to create some statuses here like: shipped in 2 days, next day delivery, 1 day, etc.<br />
After you have done this go to \'System --> Settings --> Your Store --> Edit --> TAB Option --> \'Delivery Status\' to set the Default Delivery Status for new products, show the Delivery Status in the Admin Product List and set the currently unassigned products to the Default Delivery Status.<br /><br />
<strong>In System --> Settings --> Your Store --> Edit --> TAB Option --> Delivery Status :</strong><br /><br />
- <strong>Delivery Status:</strong> Delivery Status that new (created) products should have by default. This setting is also important for the setting \'Update Delivery Status\'.<br />
- <strong>Show Delivery Status:</strong> Set this to \'Yes\' to show the selected Status in the Admin Product List and set to \'No\' to hide the status from the Admin Product list<br />
- <strong>Update Delivery Status:</strong> Set this to \'Yes\' if you want all your current products, with no Delivery Status assigned, to the Default Delivery Status. In most cases you will use this only one time when (you just have installed this extension) no products are assigned to a Delivery Status.<br /><br />
Hope you enjoy this extension and if you need a custom solution, please use the TAB Contact for contacting details.<br /><br />
Note:<br />
<strong><em>Some feeds have a mandatory field \'Delivery Status\'. In these cases you need to have this extension to make the feed work properly!</em></strong>
';

// Entry
$_['entry_name']       = 'Delivery Status Name (3-100 char):';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify delivery statuses!';
$_['error_name']       = 'Delivery Status Name must be between 3 and 32 characters!';
$_['error_product']    = 'Warning: This delivery status cannot be deleted as it is currently assigned to %s products!';
?>
