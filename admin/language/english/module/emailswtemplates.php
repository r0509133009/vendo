<?php
// Heading
$_['heading_title']    = 'Newsletter + Templates';
$_['heading_title_history']    = 'Newsletter history';
$_['heading_title_subscribers']    = 'Newsletter subscribers';
$_['heading_title_view_history']    = 'Newsletter history';
$_['heading_title_insert_subscribers']    = 'Insert subscribers';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: !';
$_['text_edit']        = 'Edit';
$_['text_left']           = 'Left';
$_['text_right']          = 'Right';
$_['text_info']           = '<span class="help">Developed by</span>';  
$_['text_module']         = 'Modules';
$_['text_list']            = 'Layout List';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';
$_['text_sent']            = 'Your message has been successfully sent to %s of %s recipients!';
$_['text_default']         = 'Default';
$_['text_newsletter']      = 'Site Newsletter Subscribers';
$_['text_module_newsletter']      = 'Newsletter Subscribers';
$_['text_test_email']      = 'Test email';
$_['text_customer_all']    = 'All Customers';
$_['text_customer_group']  = 'Customer Group';
$_['text_customer']        = 'Customers';
$_['text_affiliate_all']   = 'All Affiliates';
$_['text_affiliate']       = 'Affiliates';
$_['text_product']         = 'Products';
$_['button_delete']          = 'Delete';
$_['button_save']          = 'Save';
$_['button_delete_history']          = 'Delete record';
$_['text_export_xls']          = 'Export xls';
$_['text_export_csv']          = 'Export csv';
$_['button_back_to']          = 'Back';
$_['button_module_add']        = 'Add template';
$_['tab_module_templ']   = 'Template';
$_['text_no_results']   = 'no results';
$_['view_history_item']   = 'History record id ';
$_['subscr_panel_title']   = 'title ';

// Help
$_['help_customer']       = 'Autocomplete';
$_['help_affiliate']      = 'Autocomplete';
$_['help_product']        = 'Send only to customers who have ordered products in the list. (Autocomplete)';

// Column
$_['column_name']      = 'Name';$_['column_date']      = 'Date';
$_['column_email']     = 'Email';
$_['column_action']    = 'Action';
$_['entry_action']         = 'Action';
$_['column_subject']         = 'Subject';
$_['column_to']         = 'To';
$_['column_id']          = 'ID';
$_['column_date']  = 'Date';

//Edit
$_['edit_template']       = 'Edit Templates';
$_['edit_subscribers']       = 'Edit Subscribers';
$_['entry_edit']       = 'Edit';
$_['entry_view']       = 'View';

// Entry
$_['entry_heading']     = 'Heading Title';
$_['view_all_history']  = 'Newsletter history';
$_['entry_subject']  = 'Subject';
$_['entry_example_text']  = 'Example: first name, last name, email';
$_['entry_alowed_format']  = 'Allowed format :  txt, csv';
$_['entry_test_email']  = 'Test email:';
$_['entry_unsubscribe']  = 'Unsubscribe:';
$_['entry_position']     = 'Position:';
$_['entry_status']       = 'Status:';
$_['entry_sort_order']   = 'Sort Order:';
$_['entry_options']      = 'Options:';
$_['entry_mail']   		 = 'Send Email';
$_['entry_thickbox']   	 = 'Thickbox';
$_['entry_registered']   = 'Registered Users:';
$_['entry_layout']       = 'Layout:';
$_['entry_store']          = 'From:';
$_['entry_template']          = 'Template';
$_['entry_to']             = 'To:';
$_['entry_customer_group'] = 'Customer Group';
$_['entry_customer']       = 'Customer';
$_['entry_affiliate']      = 'Affiliate';
$_['entry_product']        = 'Products';
$_['entry_subject']        = 'Subject';
$_['entry_message']        = 'Message:';
$_['entry_description']   = 'Content';
$_['entry_layout']        = 'Layout:';
$_['entry_position']      = 'Position:';
$_['entry_sort_order']    = 'Sort Order:';
$_['entry_name']       = 'Name';
$_['entry_first_name']       = 'First Name';
$_['entry_last_name']       = 'Last name';
$_['entry_code']       = 'Email';

// Error

$_['error_permission']     = 'Warning: You do not have permission to send E-Mail\'s!';
$_['error_subject']        = 'E-Mail Subject required!';
$_['error_message']        = 'E-Mail Message required!';
$_['error_warning']        = 'Error!';
$_['error_email_exist']        = 'Email address is not correct!';
$_['error_email_name']        = 'email is not correct ';
?>