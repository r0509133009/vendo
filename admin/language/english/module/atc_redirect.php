<?php
// Heading
$_['heading_title']       = 'Add to Cart - Redirect';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified Add to Cart - Redirect  module!';


$_['text_cart_page']   = 'Cart';
$_['text_checkout_page']   = 'Checkout';
$_['text_additional_button']        = 'Add additional button';
$_['text_replace_current_button']        = 'Replace current button "Add to Cart"';
$_['text_settings_tab']        = 'Settings';
$_['text_about_tab']        = 'About';

// Entry
$_['entry_status']        = 'Status:';
$_['entry_redirect_to']        = 'Redirect to:';
$_['entry_appearance']        = 'Appearance:';
$_['entry_button_text']        = 'Text:';
$_['entry_button_text_color']        = 'Text color:';
$_['entry_button_text_color_hover']        = 'Text hover color:';
$_['entry_button_background']        = 'Background color:';
$_['entry_button_background_hover']        = 'Background hover color:';


// Error
$_['error_permission']    = 'Warning: You do not have permission to modify Add to Cart - Redirect module!';
?>