<?php
// Heading
$_['heading_title']       = 'Account Plus';

// Text
$_['text_module']               = 'Modules';
$_['text_success']              = 'Success: You have modified module Account Plus!';
$_['text_column_left']          = 'Column Left';
$_['text_column_right']         = 'Column Right';
$_['text_general_display']      = 'General Dispaly';
$_['text_links_display']        = 'Links Dispaly';
$_['text_extra_display']        = 'Extra Links';
$_['text_links_display_desc']   = '<span class="help">Turn the display of certain links <b>OFF</b>, by choosing <b>YES</b>.</span>';
$_['text_extra_display_desc']   = '<span class="help"><b>Add other links to account page.</b></span>';
$_['text_account_link']         = 'My Account Links';
$_['text_order_link']           = 'My Orders Links';
$_['text_newsletter_link']      = 'Newsletter Links';
$_['text_custom_title']         = 'Custom Titles';
$_['text_styles']               = 'Styles';
$_['text_display']              = 'Display';
$_['text_modules']              = 'Modules';
$_['text_browse']               = 'Browse Files';
$_['text_clear']                = 'Clear Image';
$_['text_image_manager']        = 'Image Manager';
$_['text_tab_account']          = 'Customer Account';
$_['text_tab_affiliate']        = 'Affiliate Account';
$_['text_account_link_a']       = 'My Affiliate Account Links';
$_['text_tracking_link_a']      = 'My Tracking Information Links';
$_['text_transaction_link_a']   = 'My Transactions Links';

// Entry
$_['entry_title']               = 'My Account Page Custom Title :<br /><span class="help">Change My Account page title.</span>'; 
$_['entry_title_account']       = 'My Account Custom Title :<br /><span class="help">Change My Account title.</span>'; 
$_['entry_title_orders']        = 'My Orders Custom Title :<br /><span class="help">Change My Orders title.</span>';
$_['entry_title_newsletter']    = 'Newsletter Custom Title :<br /><span class="help">Change Newsletter title.</span>';
$_['entry_title_a']             = 'My Affiliate Page Custom Title :<br /><span class="help">Change My Affiliate page title.</span>'; 
$_['entry_title_account_a']     = 'My Affiliate Custom Title :<br /><span class="help">Change My Affiliate title.</span>'; 
$_['entry_title_tracking_a']    = 'My Tracking Information Custom Title :<br /><span class="help">Change My Tracking Information title.</span>';
$_['entry_title_transaction_a'] = 'My Transactions Custom Title :<br /><span class="help">Change My Transactions title.</span>';



$_['entry_bt_logout']           = 'Show Logout Button :<br /><span class="help">If "NO", it will set to default format as text.</span>';
$_['entry_ico_logout']          = 'Show Icon befor Logout:<br /><span class="help">Display icon before the text of Logoutt under the welcome message.</span>';
$_['entry_yes']   		        = 'Yes';
$_['entry_no']   		        = 'No';
$_['entry_style']   	        = 'Style of module:<br /><span class="help">Choose the style of the Account Plus module that you prefer.</span>';
$_['entry_icotype']   	        = 'Type of icons:<br /><span class="help">Choose the type of icons, if the style is default the icons are hidden.</span>';
$_['entry_account_link']        = 'My Account';
$_['entry_edit_link']           = 'Edit Account';
$_['entry_password_link']       = 'Password';
$_['entry_address_link']        = 'Address book';
$_['entry_wishlist_link']       = 'Wish List';
$_['entry_order_link']          = 'Order History';
$_['entry_download_link']       = 'Downloads';
$_['entry_return_link']         = 'Returns';
$_['entry_transaction_link']    = 'Transactions';
$_['entry_recurring_link']      = 'Recurring';
$_['entry_newsletter_link']     = 'Newsletter';
$_['entry_reward_link']         = 'Reward Points';
$_['entry_logout_link']         = 'Logout';
$_['entry_tabs']                = 'Display in tabs<br /><span class="help">Show the account categories in tabs.</span>';
$_['entry_secs']                = 'Display in sections<br /><span class="help">Show the account links in sections. Disable if enable tabs display.</span>';
$_['entry_secs_af']             = 'Display in sections<br /><span class="help">Show the account links in sections.</span>';
$_['entry_welcome_a']	        = 'Welcome Customer:<br /><span class="help">Display the Welcome Message with customer name.</span>';
$_['entry_balance_a']	        = 'Customer Balance:<br /><span class="help">Display the total customer balance.</span>';
$_['entry_reward_a']	        = 'Customer Rewards:<br /><span class="help">Display the total customer rewards.</span>';
$_['entry_order_a']	            = 'Total Customer Orders:<br /><span class="help">Display the total orders of customer.</span>';
$_['entry_register_a']	        = 'Customer Register Date:<br /><span class="help">Display the date of registration of customer.</span>';
$_['entry_welcome_af']	        = 'Welcome Affiliate!<br /><span class="help">Display the Welcome Message with affiliate name.</span>';
$_['entry_balance_af']	        = 'Affiliate Balance:<br /><span class="help">Display the total affiliate customer balance.</span>';
$_['entry_commi_af']	        = 'Affiliate Commision:<br /><span class="help">Display the affikiate commision.</span>';
$_['entry_order_af']	        = 'Total Affiliate Orders:<br /><span class="help">Display the total of affiliate orders.</span>';
$_['entry_customer_af']	        = 'Total Affiliate Customers:<br /><span class="help">Display the total customers of affiliate.</span>';
$_['entry_register_af']	        = 'Affiliate Register Date:<br /><span class="help">Display the date of registration of affiliate.</span>';
$_['entry_title']               = 'Title:';
$_['entry_link']                = 'Url:';
$_['entry_image']               = 'Image:';
$_['entry_status']              = 'Status:';
$_['entry_description']         = 'Description:';
$_['entry_acc_icon_a']	        = 'Account Icon:<br /><span class="help">Display an account image near welcome message. It disable if welcome message is disabled</span>';
$_['entry_acc_icon_af']	        = 'Account Icon:<br /><span class="help">Display an account image near welcome message. It disable if welcome message is disabled</span>';


$_['entry_edit_link_a']         = 'Edit Account';
$_['entry_password_link_a']     = 'Password';
$_['entry_payment_link_a']      = 'Payment Preferences';
$_['entry_tracking_link_a']     = 'Tracking Code';
$_['entry_transaction_link_a']  = 'Transaction History';
$_['entry_download_link_a']     = 'Downloads';
$_['entry_return_link_a']       = 'Returns';
$_['entry_transaction_link_a']  = 'Transactions';
$_['entry_newsletter_link_a']   = 'Newsletter';
$_['entry_reward_link_a']       = 'Reward Points';
$_['entry_logout_link_a']       = 'Logout';
$_['entry_tabs_a']              = 'Display in tabs<br /><span class="help">Show the affiliate account categories in tabs.</span>';

//Tab
$_['tab_account']   		    = 'Account';
$_['tab_affiliate']   		    = 'Affiliatte';

//Button
$_['button_save_stay']          = 'Save & Stay';


// Error
$_['error_permission']          = 'Warning: You do not have permission to modify module Account Plus!';
$_['error_warning']             = 'Warning: Please check the form carefully for errors!';
$_['error_extra_link_url']      = 'Please, enter the link!';
$_['error_extra_link_title']    = 'Title must be between 3 and 64 characters!';

?>