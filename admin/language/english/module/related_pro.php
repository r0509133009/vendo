<?php
// Heading
$_['heading_title']       = 'Related Pro';

// Tab
$_['tab_general'] 		  = 'General';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module Related Pro!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';
$_['text_category']       = 'Same Category';
$_['text_name']   	  	  = 'Similar Name';
$_['text_related']   	  = 'Self selected related products';

// Entry
$_['entry_display_tab']   = 'Display Related Tab:'; 
$_['entry_limit_tab']     = 'Limit Products in Tab:'; 
$_['entry_related']    	  = 'Choose how the system select related products:'; 
$_['entry_random']    	  = 'Randomise Related Products:'; 
$_['entry_join']    	  = 'Choose how the system joins the conditions:'; 
$_['entry_exclude']    	  = 'Excluded words (comma separated):'; 
$_['entry_limit']		  = 'Limit:';
$_['entry_image']         = 'Image (W x H) and Resize Type:';
$_['entry_layout']        = 'Layout:';
$_['entry_position']      = 'Position:';
$_['entry_status']        = 'Status:';
$_['entry_sort_order']    = 'Sort Order:';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify module Related Pro!';
$_['error_image']         = 'Image width &amp; height dimensions required!';
?>