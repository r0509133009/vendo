<?php
// Heading
$_['heading_title']       = 'CustomerPhotos 2.2';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module CustomerPhotos!';
$_['text_success_activation']        = 'ACTIVATED: You have successfully activated CustomerPhotos!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';

// Entry
$_['entry_code']          = 'CustomerPhotos status:<br /><span class="help">Enable or disable CustomerPhotos</span>';
$_['entry_layouts_active']          = 'Activated on:<br /><span class="help">Choose which pages CustomerPhotos to be active</span>';
// Entry
$_['entry_layout']        		= 'Layout:';
$_['entry_position']      		= 'Position:';
$_['entry_status']        		= 'Status:';
$_['entry_sort_order']    		= 'Sort Order:';
$_['entry_layout_options']      = 'Layout Options:';
$_['entry_position_options']    = 'Position Options:';
$_['entry_enable_disable']		= 'CustomerPhotos status:<span class="help">Enable or disable RelatedProductsPro for this store</span>';
$_['entry_yes']					= 'Yes';
$_['entry_no']					= 'No';
?>