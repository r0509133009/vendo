<?php
/* This module is copywrite to ozxmod
 * Author: ozxmod(ozxmod@gmail.com)
 * It is illegal to remove this comment without prior notice to ozxmod(ozxmod@gmail.com)
*/ 
// Heading
$_['heading_title']       = 'Awesome Facebook Google Login (<a target="_blank" href="http://www.ozxmod.com">OZXMOD</a> <a href="mailto:ozxmod@gmail.com">ozxmod@gmail.com</a>)';

// Text
$_['text_module']         = 'Modules';
$_['text_edit']        = 'Edit';
$_['text_success']        = 'Success: You have modified module ajax facebook google!';

// Entry
$_['text_modulesetting'] = 'Module Setting Details';
$_['entry_display_at_login']	= 'Display Social Login option at opencart default Login Page:';
$_['entry_display_at_checkout']	= 'Display Social Login option at Checkout:';
$_['entry_display']	= 'Social Login Option:';
$_['entry_fb']		= 'Facebook';
$_['entry_google']	= 'Google';
$_['entry_twitter']	= 'Twitter';
$_['entry_linkedin']	= 'LinkedIn';
$_['entry_enable']	= 'Enable';
$_['entry_disable'] = 'Disable';
$_['entry_status'] = 'Status';
$_['entry_yes'] = 'Yes';
$_['entry_no'] = 'No';

$_['text_apisetting']	 = 'API Setting Details (<a target="_blank" href="http://youtu.be/VLo1XPoA_aU?list=PL2u0U6obV8KxvOfV-aK7IlG_V8YUWpnEL">Click Here</a> to Watch Video for Setup)';
$_['entry_apikey']        = 'Facebook API Key:';
$_['entry_apisecret']     = 'Facebook API Secret:';
$_['entry_googleapikey']        = 'Google API Key:';
$_['entry_googleapisecret']     = 'Google API Secret:';
$_['entry_googleredirect'] = 'Google Redirect URI:';
$_['entry_twitterapikey']        = 'Twitter Consumer Key:';
$_['entry_twitterapisecret']     = 'Twitter Consumer Secret:';
$_['entry_linkedinapikey']        = 'LinkedIn API Key:';
$_['entry_linkedinapisecret']     = 'LindedIn API Secret:';
$_['text_newfbapp']		= '<a href="https://developers.facebook.com/" target="_blank">Click Here</a> to create new facebook app.';
$_['text_newgoogleapp']		= '<a href="https://developers.google.com/+/quickstart/php" target="_blank">Click Here</a> to create APIKEY and SECRET KEY for Google.';
$_['text_newtwitterapp']		= '<a href="https://apps.twitter.com/" target="_blank">Click Here</a> to create CONSUMER KEY and CONSUMER SECRET for Twitter.';
$_['text_newlinkedinapp']		= '<a href="https://www.linkedin.com/secure/developer?newapp" target="_blank">Click Here</a> to create API KEY and SECRET KEY for LinkedIn.';
$_['text_googlenote']	 = '<b>Note:</b>&nbsp;Redirect URI added while creating google api and secret key should be following. If this differ then you will get error.<br/>
<b>http://[DOMAIN NAME]/index.php?route=account/ajax_login_register/glogin</b><br/>Here <b>DOMAIN NAME</b> is name of your domain. Suppose your website domain is test.com then that redirect url will be <b>http://test.com/index.php?route=account/ajax_login_register/glogin</b>';



// Error
$_['error_permission']    = 'Warning: You do not have permission to modify module most viewed!';
$_['error_code']         = 'Error: Facebook Google APIs are required!';

/* This module is copywrite to ozxmod
 * Author: ozxmod(ozxmod@gmail.com)
 * It is illegal to remove this comment without prior notice to ozxmod(ozxmod@gmail.com)
*/ 
?>