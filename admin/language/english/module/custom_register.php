<?php
// ***************************************************
//             Custom Registration Fields   
//       
// Author : Francesco Pisanò - francesco1279@gmail.com
//              
//                   www.leverod.com		
//               © All rights reserved	  
// ***************************************************


// Admin language file

$crf_version = '2.7.1';

$_['heading_title']    = '<span style="position:relative; padding-left:26px;font-weight:bold;"><img style="width:20px; position:absolute; bottom:-3px; left:-1px;" src="view/image/crf-logo-light.gif" />Custom Registration Fields '.$crf_version .'</span>';
$_['heading_title_cp'] = 'Custom Registration Fields '.$crf_version;

// Text
$_['text_settings'] 		= 'Settings';
$_['text_antispam'] 		= 'AntiSpam Shield';
$_['text_user_guide']		= 'User Guide';
$_['text_license']			= 'License';
$_['text_module']			= 'Modules';
$_['text_entry']			= 'Fields';
$_['text_show']				= 'Show';
$_['text_enable']			= 'Enable';
$_['text_account']			= 'Account';
$_['text_register']			= 'Register';
$_['text_account_already']	= 'If you already have an account with us, please login at the <a href="%s">login page</a>.';
$_['text_your_details']		= 'Your Personal Details';
$_['text_your_address']		= 'Your Address';
$_['text_newsletter']		= 'Newsletter';
$_['text_your_password']	= 'Your Password';
$_['text_agree']			= 'I have read and agree to the <a class="fancybox" href="%s" alt="%s"><b>%s</b></a>';

$_['text_member']			= 'Members';
$_['text_guest']			= 'Enable Guests';
$_['text_customer_grp_mgr']	= 'Customer Group Manager';
$_['text_help']				= 'Help';
$_['text_select']			= 'Select';
$_['text_select_all']		= 'All';
$_['text_deselect_all']		= 'None';
$_['text_none']				= 'Hide checkbox';
$_['text_display']			= 'Display';
$_['text_required']			= 'Required';
$_['text_checked_by_default']	= 'Checked by default';

$_['text_approval']			= 'Approval';
$_['text_name']				= 'Name';
$_['text_description']		= 'Description';
$_['text_default']			= 'Default';
$_['text_sort_order']		= 'Sort Order';
$_['text_reset_default']	= 'Reset to default <br /> values';

$_['text_account_registration'] = 'Account Registration Form';
$_['text_checkout_page'] 		= 'Checkout Page';

// Help
$_['text_help_member']						= 'Members require an account to place an order.';
$_['text_help_member_firstname']			= '';
$_['text_help_member_lastname']				= '';
$_['text_help_member_email']				= 'Email cannot be disabled for members, they will need it to log into their accounts.';
$_['text_help_member_telephone']			= '';
$_['text_help_member_fax']					= '';
$_['text_help_member_password']				= 'Password cannot be disabled for members, they need it to log into their accounts.';
$_['text_help_member_confirm']				= 'When is enabled, password confirmation is always a required field.';
$_['text_help_member_company']				= '';
$_['text_help_member_address_1']			= '';
$_['text_help_member_address_2']			= '';
$_['text_help_member_city']					= '';
$_['text_help_member_country']				= '';
$_['text_help_member_default_country']		= '';
$_['text_help_member_postcode']				= 'Note: this setting doesn&#039;t overwrite the option in System -> Localization -> Countries -> "Postcode Required". <a href="#postcode-required">Read more</a>.';

$_['text_help_member_zone']					= '';
$_['text_help_member_newsletter']			= '';
$_['text_help_member_shipping_address']		= 'If you hide this checkbox, by default billing and shipping addresses will be the same but users will be able to modify the delivery info by clicking on &quot;Modify&quot;.';
$_['text_help_member_agree']				= '';

$_['text_help_guest_checkout']				= 'Allow customers to checkout without creating an account. This will not be available when a downloadable product is in the shopping cart.';
$_['text_help_guest_firstname']				= '';
$_['text_help_guest_lastname']				= '';
$_['text_help_guest_email']					= '';
$_['text_help_guest_telephone']				= '';
$_['text_help_guest_fax']					= '';
$_['text_help_guest_password']				= '';
$_['text_help_guest_confirm']				= '';
$_['text_help_guest_company']				= '';
$_['text_help_guest_address_1']				= '';
$_['text_help_guest_address_2']				= '';
$_['text_help_guest_city']					= '';
$_['text_help_guest_country']				= '';
$_['text_help_guest_default_country']		= '';
$_['text_help_guest_postcode']				= '';
$_['text_help_guest_zone']					= '';
$_['text_help_guest_newsletter']			= 'Guests cannot receive newsletters because Opencart doesn&#039;t save their accounts but only their orders.';
$_['text_help_guest_shipping_address']		= 'If you hide this checkbox, users will not be able to choose a delivery address other than the billing address.';
$_['text_help_guest_agree']					= '';

$_['text_help_hidden_antispam'] 			= 'Enable the AntiSpam Shield <b>to prevent spam bots from registering on your website</b>. This feature is <b>invisible</b> to your customers.';


// buttons

$_['button_save']			= 'Save';
$_['button_save_continue']	= 'Save & Continue';


// Entry
$_['entry_firstname']		= 'First Name';
$_['entry_lastname']		= 'Last Name';
$_['entry_email']			= 'E-Mail';
$_['entry_telephone']		= 'Telephone';
$_['entry_fax']				= 'Fax';
$_['entry_password']		= 'Password';
$_['entry_confirm']			= 'Password Confirm';
$_['entry_company']			= 'Company';
$_['entry_address_1']		= 'Address 1';
$_['entry_address_2']		= 'Address 2';
$_['entry_city']			= 'City';
$_['entry_country'] 		= 'Country';
$_['entry_default_country']	= 'Default Country';
$_['entry_postcode'] 		= 'Post Code';
$_['entry_zone']			= 'Region / State';
$_['entry_newsletter']		= 'Show checkbox &quot;Subscribe Newsletter&quot;';
$_['entry_shipping_address']= 'Show checkbox &quot;My delivery and billing addresses are the same&quot;';
$_['entry_agree']			= 'Show checkbox &quot;Agree with the Account/Checkout terms&quot;';

$_['entry_account']			= 'Account Terms (Registration and Checkout pages)';
$_['entry_checkout']		= 'Checkout Terms';

$_['entry_customer_group']			= 'Customer group';
$_['entry_customer_group_name']		= 'Customer Group Name';
$_['entry_default_customer_group']	= 'Default Customer group';
$_['entry_company_id']				= 'Company id';
$_['entry_tax_id']					= 'Tax id';



// Errors

$_['error_warning']					= 'Warning: Please check the form carefully for errors';
$_['error_customer_group_name'] 	= 'Customer Group Name must be between 3 and 32 characters!';
$_['error_customer_group_display']	= 'You must include the default customer group if you are going to use this feature!';
$_['error_install']					= 'Warning: Unable to install <b>Custom Registration Fields</b>. XML missing?';
$_['error_uninstall']				= 'Warning: An error occurred while uninstalling <b>Custom Registration Fields</b>. XML missing?';


// install / uninstall / Save

$_['success_install']		= 'To complete the installation click <b>Edit</b>, then click <b>Save</b>.';
$_['success_uninstall']		= '<b>Custom Registration Fields</b> uninstalled successfully.';
$_['text_saving']			= 'Saving';
$_['text_success']			= 'The extension <b>Custom Registration Fields</b> has been saved successfully';
?>