<?php
// Heading
$_['heading_title']       = 'Custom Success Page';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module Custom Success Page!';
$_['text_left']           = 'Left';
$_['text_center']         = 'Center';
$_['text_right']          = 'Right';
$_['text_in_table']       = 'In table';
$_['text_without_table']  = 'Without table';

// Entry
$_['entry_module_status']              = 'Module status';
$_['entry_coupon']                     = 'Coupon';
$_['entry_invoice_status']             = 'Auto generate invoice number';
$_['entry_button_status']              = 'Hide button \'Continue\'';
$_['entry_adwords_status']             = 'Google Adwords status';
$_['entry_adwords_conversion_id']      = 'Adwords Conversion ID';
$_['entry_adwords_conversion_label']   = 'Adwords Conversion Label';
$_['entry_css']                        = 'Custom CSS';
$_['entry_javascript']                 = 'Custom Javascript';
$_['entry_status']                     = 'Status';
$_['entry_wrapper']                    = 'Wrapper width';
$_['entry_background_color']           = 'Background color';
$_['entry_padding']                    = 'Padding';
$_['entry_margin']                     = 'Margin';
$_['entry_top']                        = 'Top';
$_['entry_right']                      = 'Right';
$_['entry_bottom']                     = 'Bottom';
$_['entry_left']                       = 'Left';
$_['entry_border']                     = 'Border';
$_['entry_size']                       = 'Size';
$_['entry_color']                      = 'Color';
$_['entry_border_radius']              = 'Border radius';
$_['entry_title']                      = 'Title header';
$_['entry_table']                      = 'Show content';
$_['entry_logged']                     = 'Visible only for logged customer';
$_['entry_description']                = 'Description';
$_['entry_text_align']                 = 'Text align';
$_['entry_social_facebook']            = 'Facebook';
$_['entry_social_twitter']             = 'Twitter';
$_['entry_social_pinterest']           = 'Pinterest';
$_['entry_social_googleplus']          = 'Google Plus';
$_['entry_social_linkedin']            = 'LinkedIn';
$_['entry_payment_address']            = 'Payment address:';
$_['entry_shipping_address']           = 'Shipping address';
$_['entry_payment_title']              = 'Payment title header';
$_['entry_shipping_title']             = 'Shipping title header';
$_['entry_comment']                    = 'Comment';
$_['entry_bank_transfer']              = 'Instructions';
$_['entry_image']                      = 'Show the Image column';
$_['entry_image_dimension']            = 'Product image dimension';
$_['entry_name']                       = 'Show the Product name column';
$_['entry_model']                      = 'Show the Model column';
$_['entry_sku']                        = 'Show the SKU column';
$_['entry_quantity']                   = 'Show the Quantity column';
$_['entry_price']                      = 'Show the Price column';
$_['entry_total']                      = 'Show the Total column';
$_['entry_totals']                     = 'Order totals section';
$_['entry_column_image']               = 'Image';
$_['entry_column_product']             = 'Product name';
$_['entry_column_model']               = 'Model';
$_['entry_column_sku']                 = 'SKU';
$_['entry_column_quantity']            = 'Quantity';
$_['entry_column_price']               = 'Price';
$_['entry_column_total']               = 'Total';
$_['entry_order_id']                   = 'Order ID';
$_['entry_date_added']                 = 'Date added';
$_['entry_payment_method']             = 'Payment method';
$_['entry_shipping_method']            = 'Shipping method';
$_['entry_email']                      = 'Email';
$_['entry_telephone']                  = 'Telephone';
$_['entry_invoice']                    = 'Invoice number';
$_['entry_product']                    = 'Product';
$_['entry_title_font']                 = 'Title font';
$_['entry_button_cart']                = 'Button text';
$_['entry_facebook_url']               = 'Facebook page url';
$_['entry_facebook_dimension']         = 'The dimensions of the box';
$_['entry_youtube_url']                = 'YouTube video ID';
$_['entry_youtube_dimension']          = 'The dimensions of the video';
$_['entry_width']                      = 'Width';
$_['entry_height']                     = 'Height';
$_['entry_weight']                     = 'Weight';
$_['entry_section']                    = 'Add section';

// Help
$_['help_coupon']                      = 'Coupon for the next purchase. Set in the \'Text\' section using the shortcode {coupon}.';
$_['help_adwords_conversion_label']    = 'Ad the label of the conversion code you want to track. Example: fOadr1EviUcrlaDRiU5';
$_['help_adwords_conversion_id']       = 'Example: 910113634';
$_['help_bank_transfer']               = 'Show instructions only when the customer has chosen Bank Transfer payment method.';
$_['help_youtube_url']                 = 'Video ID is a unique code at the end of the URL, e.g. i9oNpR4YXws';

// Tab
$_['tab_general']            = 'Setting';

// Error
$_['error_permission']       = 'Warning: You do not have permission to modify module Custom Success Page!';
$_['error_required']         = 'Warning: Please check the form carefully for errors!';
?>