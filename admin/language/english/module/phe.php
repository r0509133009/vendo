<?php
// Heading
$_['heading_title']       		  = 'Pretty HTML Email | By <a href="http://www.marketinsg.com">MarketInSG</a> |';
$_['text_heading']		  		  = 'Pretty HTML Email';

// Tab
$_['tab_general']		  		  = 'General';
$_['tab_style']			  		  = 'CSS Style';
$_['tab_preview']		  		  = 'Preview';
$_['tab_list']	  		  		  = 'List';
$_['tab_about']	  		  		  = 'About';

// Column
$_['column_name']		  		  = 'Name';
$_['column_type']		  		  = 'Type';
$_['column_store']		  		  = 'Store';
$_['column_priority']		  	  = 'Priority';
$_['column_date_start']   		  = 'Date Start';
$_['column_date_end']	  		  = 'Date End';
$_['column_action']		  		  = 'Action';

// Text
$_['text_module']         		  = 'Modules';
$_['text_success']        		  = 'Success: You have modified module Pretty HTML Email!';
$_['text_register']		  		  = 'Account Register Mail';
$_['text_affiliate']	  		  = 'Affiliate Register Mail';
$_['text_order']		  		  = 'Order Confirmation Mail';
$_['text_contact']		  		  = 'Contact Mail';
$_['text_forgotten']	  		  = 'Forgotten Password Mail';
$_['text_reward']		  		  = 'Add Reward Mail';
$_['text_account_approve']		  = 'Account Approval Mail';
$_['text_account_transaction']	  = 'Account Add Transaction Mail';
$_['text_affiliate_approve']	  = 'Affiliate Approval Mail';
$_['text_affiliate_transaction']  = 'Affiliate Add Commission Mail';
$_['text_gift_voucher']		  	  = 'Gift Voucher Mail';
$_['text_return']  				  = 'Product Return Status';
$_['text_status']		  		  = 'Order Status';
$_['text_code']			  		  = 'Template Codes';
$_['text_default']		  		  = 'Default';
$_['text_select_image']	  		  = 'Select Image';
$_['text_purchase']		    	  = 'Purchase Pretty HTML Email';
$_['text_review']		  		  = 'Rate Pretty HTML Email';
$_['text_support']		  		  = 'Need support from <a href="http://www.marketinsg.com">MarketInSG</a>? Fill up the form below to email us (English only).';

// Entry
$_['entry_name']		  		  = 'Name:';
$_['entry_type'] 		  		  = 'Type:';
$_['entry_priority'] 	 		  = 'Priority:';
$_['entry_date_start']	  		  = 'Date Start:';
$_['entry_date_end']	  		  = 'Date End:';
$_['entry_subject']		 		  = 'Subject:';
$_['entry_message']				  = 'Message:';
$_['entry_background']			  = 'Background Colour:';
$_['entry_body']	  			  = 'Body Colour:';
$_['entry_heading']				  = 'Heading Colour:';
$_['entry_store']				  = 'Store ';
$_['entry_image']				  = 'Banner: <span class="help">Replaces logo and use this image instead.</span>';

$_['entry_mail_name']			  = 'Your Name:<br /><span class="help">How do we address you?</span>';
$_['entry_mail_email']	 		  = 'Email Address:<br /><span class="help">Which email do you want us to reply to?</span>';
$_['entry_mail_order_id']		  = 'Order ID:<br /><span class="help">Your Order ID when you purchased this extension.</span>';
$_['entry_mail_message']		  = 'Message:<br /><span class="help">What would you like to tell us?<br />Please attached store URL, store admin login, and FTP login if you require technical support.</span>';

// Button
$_['button_mail']				  = 'Contact Support';
$_['button_email']				  = 'Email Me';

// Error
$_['error_permission']   		  = 'Warning: You do not have permission to modify module Pretty HTML Email!';
$_['error_name']				  = 'Warning: Name required!';

// Mail
$_['mail_success']				  = 'You have successfully contacted MarketInSG\'s Support';
$_['mail_error_name']			  = 'Name must be between 5 and 32 characters';
$_['mail_error_email']			  = 'Email address must be valid!';
$_['mail_error_order_id']		  = 'Order ID must be valid!';
$_['mail_error_message']		  = 'Message must be between 20 and 2400 characters!';
?>