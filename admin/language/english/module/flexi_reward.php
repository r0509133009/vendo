<?php
// Heading
$_['heading_title']      = 'GGW Flexi Rewards';

// Text
$_['text_module']      = 'Modules';
$_['tab_setting'] = 'Setting';

$_['text_flexi_productreview']	= ' Product review ';
$_['text_flexi_firstorder']	= ' First order ';

$_['text_head_useractions'] = 'Points for customer actions';
$_['entry_registration'] = 'Registration points';
$_['entry_newsletter_signup'] = 'Newsletter sign up points';
$_['entry_productreview'] = 'Product review points';
$_['entry_firstorder'] = 'First order points';
$_['entry_firstorder_operator'] = 'First order calculation: <br /><span class="help">If you want 10% to be rewarded use: 10 as points above and set this as Percentage.</span>';

$_['entry_redeem_min_order_amount'] = 'Minimum order amount above which customers will be able to use reward points:<br /><span class="help">If you want orders above $50 to be able to use reward points during check out set 50. <br> Set 0 for no minimum limit.</span>';
$_['entry_redeem_max'] = ' Max % of redeemable points:<br /><span class="help">If order allows max 200 points to be redeemed you can reduce them to any % you like and force customer to pay the rest. <br>Set 0.5 for 50% so 100 / 200 can be redeemed; Set 0.2 for 20% so 40 / 200 points can be redeemed etc.<br>  Set 1 for no max limit - 200/200.</span>';
//$_['entry_redeem_max'] = 'Max % of redeemable points:<br /><span class="help">Example: If order = $150, $150 = 150 points, customer has 400 points in their account, customer will not pay anything just by redeeming 150 points. if you set here 0.5 then only 75 points will be redeemable and customer still has to pay $75 even if they have 400 points in their account. <br> Set 1 for no max limit. <br> <br> If order allows max 200 points to be redeemed you can reduce them to any % you like.</span>';
$_['entry_ignore_product_points_value'] = ' Ignore Purchase points value:<br /><span class="help">If Yes, regardless of points value - product price will be used as base for calculation.</span>';
$_['entry_ignore_product_rewards_value'] = ' Ignore Rewards points value:<br /><span class="help">If Yes, regardless of rewards value - order sub-total / total will be used as base for calculation.</span>';
$_['entry_total_subtotal_operator'] = ' Rewards for total / Sub-total? <br /><span class="help"> Order total / sub-totalwill be used as base for calculation for rewards.</span>';


$_['text_head_ignorepoints'] = 'IGNORE product Points / Rewards value';
$_['text_head_ignore_description'] = '<span class="help"><br> If NO:<br>if product points >0 (ex. 10 points ) customer can buy for 10 points; <br>if product points = 0 auto calculation rules apply; 
<br> if product points < 0 customer can\'t buy with points; 
<br><b>WARNING!!!</b> If you set Ignore points to NO and you have Product A ($100, points=0) and Product B ($100, points=25) and set $1=1 point and the user redeems 10 points, the system will calculate like this: 100 * 10 / 125 = 8 (x2) = $-16 discount and not the expected $-10. Be aware that this option overrides the purchase points multiplier.
<br><br> If Rewards is NO:<br>if product rewards >0 (ex. 10 rewards ) customer will get 10 rewards; <br>if product rewards = 0 auto calculation rules apply; <br> if product rewards < 0 customer won\'t get rewards; </span>
</span>';


//$_['entry_ignore_product_points_value'] = 'Ignore product Purchase points value:<br /><span class="help">If Yes, regardless of points value - product price will be used as base for calculation. <br><br> If NO:<br>if product points >0 (ex. 10 points ) customer can buy for 10 points; <br>if product points = 0 auto calculation rules apply; <br> if product points < 0 customer can\'t buy for with points; </span>';
//$_['entry_ignore_product_rewards_value'] = 'Ignore product Rewards points value:<br /><span class="help">If Yes, regardless of rewards value - order sub-total / total will be used as base for calculation. <br><br> If NO:<br>if product rewards >0 (ex. 10 rewards ) customer will get 10 rewards; <br>if product rewards = 0 auto calculation rules apply; <br> if product rewards < 0 customer won\'t get rewards; </span>';

// Entry
$_['entry_rate']         = 'Price multiplier:<br /><span class="help">If you want 10% to be rewarded use: 0.10</span>';
$_['entry_purchase_text']= 'Purchase price multiplier: <br /><span class="help">Use 1 if vyou want $1 = 1 Purchase point. Will use the default currency. <br><br>Example Case: To buy a $10 Product with 20 points you enter 2 as multiplier here.';

$_['entry_status']       = 'Status:';
$_['entry_order_status']       = 'Order Status:<br /><span class="help">Select order status on which reward points needd to be awarded.</span>';

$_['entry_exp_days_init_text']       = 'Order Points Expiration:<br /><span class="help">Fill in the number of days after customer\'s points will multiply by a percentage of your choice. 
<br> If you want to reduce reward points to 80% from original use 0.8. 
<br> 
<br> 
Example: Starting with 10 reward points
<br> After 10 days reduce to 80% (enter 0.8 as multiplier) = Customer will see 8 points
<br> After 20 days reduce to 50% (enter 0.5 as multiplier) = Customer will see 5 points 
<br> After 30 days reduce to 20% (enter 0.2 as multiplier) = Customer will see 2 points 
<br> After 90 days reduce to 0% (enter 0 as multiplier)  = Customer will see 0 points 
</span>';
$_['entry_exp_days_text']       = 'Expiration in (DAYS):<br /><span class="help">After how many days points will reduce / expire. </span>';
$_['entry_exp_days_price_multiplier']       = 'Reduction multiplier:<br /><span class="help">To reduce points in half use 0.5 To fully expire them use 0.</span>';


// Error
$_['error_permission']   = 'Warning: You do not have permission to modify GGW Flexi Rewards!';
$_['error_reward_added'] = 'Success: You have modified order! and <b>Already Reward Points have been added to this order</b>';

// Success
$_['text_success']       = 'Success: You have modified module GGW Flexi Rewards!';
$_['text_reward_success'] = 'Success: You have modified order! and <b>Reward points added to customer</bss>';
$_['text_bonus_days'] = 'First %s days bonus. ';
$_['text_bonus_manufacturer'] = 'Vendor bonus. ';

?>