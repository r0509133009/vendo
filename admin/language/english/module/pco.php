<?php
// Heading
$_['heading_title'] = 'Product Color Option';

// Text
$_['text_module'] = 'Module';
$_['text_success'] = 'Success: You have modified module Product Color Option!';
$_['text_yes'] = 'Yes';
$_['text_no'] = 'No';
$_['text_style_basic_rectangle'] = 'Basic Rectangle';
$_['text_style_oval'] = 'Oval';
$_['text_style_double_rectangle'] = 'Double Rectangle';
$_['text_style_double_oval'] = 'Double Oval';

// Column

// Entry
$_['entry_category'] = 'Category';
$_['entry_show_on_category'] = 'Show color options on category page';
$_['entry_list_color_selector_size'] = 'Dimension of color options on category page';
$_['entry_product'] = 'Product';
$_['entry_product_color_selector_size'] = 'Dimension of color options on product page';
$_['entry_style'] = 'Style';
$_['entry_color_selector_style'] = 'Color options\' style';
$_['entry_preview'] = 'Preview';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify module Product Color Option!';
?>