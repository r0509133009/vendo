<?php //!
// Heading
$_['heading_title']       = 'Bulgarian Language Pack';

// Text
$_['text_success']        = 'Success: You have modified module Bulgarian Language Pack!';
$_['text_module']         = 'Module';
$_['text_license']        = 'You have the Bulgarian language pack and Bulgarian currency installed.<br /><br />This language pack is licensed per installation. If you need to use it with more installations you should buy more licenses. You are advised to email your domain name to info@napravisisait.com for authentication.<br /><br />View more extensions by <a href="http://j.mp/OCxseon" target="_blank">xseon</a>.';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify module Bulgarian Language!';
?>
