<?php
// Heading
$_['heading_title']       = 'Colorbox popup Coupon';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have installed Colorbox popup Coupon!';

// Entry
$_['stats_counter_code']  = 'Coupon Pop Code:';
$_['entry_status']        = 'Status';
$_['entry_coupon']        = 'Select Coupon Pop code';
$_['entry_dicount_title_txt']  = 'Title (First Popup)';
$_['entry_dicount_title_txt_second']  = 'Title (Second Popup)';
$_['entry_dicount_desc']  = 'Description (First Popup)';
$_['entry_dicount_desc_second']  = 'Description (Second Popup)';
$_['text_enabled']		  = 'Enabled';
$_['text_disabled']	   	  = 'Disabled';
$_['text_tooltipmsg']	  = "enable coupon from 'Marketing > Coupons'";

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify module Colorbox popup Coupon!';
$_['error_code']          = 'Code Required';
?>
