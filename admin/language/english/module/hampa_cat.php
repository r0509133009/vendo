<?php
/**
 * @total-module	Hide Admin Products and Category fields
 * @author-name 	◘ Dotbox Creative
 * @copyright		Copyright (C) 2014 ◘ Dotbox Creative www.dotboxcreative.com
 * @license			GNU/GPL, see http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
 */
// Heading
$_['heading_title']       = 'Hide Admin Menu Products and Categories fields';

$_['text_success']        = 'Hide Admin Menu Products and Categories fields module updated!';
$_['text_module']         = 'Modules';

//
$_['hidden']              = ' Hide ';
$_['visible']             = ' Show ';

// Tab Switcher
$_['tab_product']           = 'Products';
$_['tab_category']          = 'Categories';

// Tab Title Products
$_['tab_general']          = 'General Tab';
	$_['tab_meta_tag_des']          = 'Meta Tag Description';
	$_['tab_meta_tag_key']          = 'Meta Tag Keywords';
	$_['tab_product_tag']           = 'Product Tags';
$_['tab_data']             = 'Data Tab';
	$_['tab_sku']          			= 'SKU';
	$_['tab_upc']          			= 'UPC';
	$_['tab_ean']          			= 'EAN';
	$_['tab_jan']          			= 'JAN';
	$_['tab_isbn']         			= 'ISBN';
	$_['tab_mnp']          			= 'MNP';
	$_['tab_location']         		= 'Location';
	$_['tab_price']         		= 'Price';
	$_['tab_tax']         			= 'Tax Class';
	$_['tab_quantity']         		= 'Quantity';
	$_['tab_min_quantity']          = 'Minimum Quantity';
	$_['tab_sub_stock']          	= 'Subtract Stock';
	$_['tab_ooss']          		= 'Out Of Stock Status';
	$_['tab_req_shipping']         	= 'Requires Shipping';
	$_['tab_seo']         			= 'SEO Keyword';
	$_['tab_date_ava']         		= 'Date Available';
	$_['tab_dimensions']          	= 'Dimensions (L x W x H)';
	$_['tab_length']          		= 'Length Class';
	$_['tab_weight']          		= 'Weight';
	$_['tab_weight_class']          = 'Weight Class';
	$_['tab_sort']          		= 'Sort Order';
$_['tab_link']          = 'Links Tab';
	$_['tab_manufacturer']         	= 'Manufacturer';	
	$_['tab_filters']          		= 'Filters';
	$_['tab_stores']          		= 'Stores';
	$_['tab_downloads']          	= 'Downloads';
	$_['tab_related']          		= 'Related Products';
$_['tab_atribute']        	= 'Attribute tab';
	$_['tab_atribute_sec']        	= 'Attribute tab';
$_['tab_option']        	= 'Option tab';
	$_['tab_option_sec']        	= 'Option tab';	
$_['tab_profile']        	= 'Profiles tab';
	$_['tab_profile_sec']        	= 'Profiles tab';		
$_['tab_discount']        	= 'Discount tab';
	$_['tab_discount_sec']        	= 'Discount tab';	
$_['tab_special']        	= 'Special tab';
	$_['tab_special_sec']        	= 'Special tab';	
$_['tab_image']        		= 'Image tab';
	$_['tab_image_sec']        		= 'Image tab';	
$_['tab_reward']        	= 'Reward points tab';
	$_['tab_reward_sec']        	= 'Reward points tab';		
$_['tab_design']        	= 'Design tab';
	$_['tab_design_sec']        	= 'Design tab';	
$_['tab_marketplace']        	= 'Marketplace tab';
	$_['tab_marketplace_sec']        	= 'Marketplace tab';		
	
	
	
$_['tab_status']        	= 'STATUS';
	$_['tab_collabse']        		= 'Collabse Product tabs';
	$_['tab_status_sec']        	= 'Hide Products Fields status';		
	

// Tab Title Category	
$_['tab_general_cat']          	= 'General Tab';
	$_['tab_meta_tag_des_cat']          = 'Meta Tag Description';
	$_['tab_meta_tag_key_cat']          = 'Meta Tag Keywords';
$_['tab_data_cat']            	= 'Data Tab';
	$_['tab_filters_cat']          		= 'Filters';
	$_['tab_stores_cat']          		= 'Stores';
	$_['tab_seo_cat']         			= 'SEO Keyword';
	$_['tab_image_cat']          		= 'Image:';
	$_['tab_top_cat']          			= 'Top:';
	$_['tab_columns_cat']         		= 'Columns:';
	$_['tab_sort_cat']          		= 'Sort Order';
$_['tab_design_cat']        	= 'Design tab';
	$_['tab_design_sec_cat']        	= 'Design tab';	
$_['tab_status_cat']        	= 'STATUS';
	$_['tab_collabse_cat']        			= 'Collabse Category tabs';
	$_['tab_status_sec_cat']        	= 'Hide Category Fields status';		

//info
$_['tab_price_info']         		    = ' Set the default price of the field if hidden';
$_['tab_quantity_info']         		= ' Set the default quantity of the field if hidden';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify module Hide Admin Menu Products and Categories fields!';
?>