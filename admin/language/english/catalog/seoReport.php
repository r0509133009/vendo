<?php
$_['heading_title'] = 'Seo Data Check Up';
$_['seordata']   = '1) Seo Report is designed to provide to give you an idea about your current seo data.<br>
          2) Many times it happens that we forget about seo while workinng on other things of store.<br>
         3) So this report lets you instantly know that something you have missed in seo.<br>
         4) It will give you report for all products, categories, information, manufacturers.<br>
         5) Click button to generate a report to know about current seo data.<br>';
         
$_['create_report'] = 'Check Seo Health';
$_['total'] = 'Total';
$_['md']  = 'Meta Description';
$_['mk']  = 'Meta Keywords';
$_['sd']  = 'Seo Keyword';
$_['sitemapt']  = 'Just check <a href="%s" target="_blank">Sitemap</a> of your store to check if contains all links you want search engines to index.';
$_['sitemapso'] = 'Create sitemap.xml for your store using our <a href="%s">Sitemap Generator Tool</a>';
$_['sitemap'] = 'A Sitemap is a way to tell search engine about the pages on your site.	<br>Having a sitemap makes your pages get indexed properly.';

?>