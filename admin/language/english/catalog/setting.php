<?php
// Heading
$_['heading_title']          = 'Seo Setting';
$_['text_success']           = 'Success: You have modified Seo Settings !!'; 
$_['text_multi_lang']    	 = 'Show complete breaccrumb links: <div class="help">If enabled products will show complete breaccrumb link when seen directly</div>';
$_['text_direct_links']    	 = 'Enable Direct Links: <div class="help">Will make all links direct. Keep it either enabled or disable, But once you do never change.</div>';
$_['text_about']   = "1) Rich Snipppet tools  are used search engines and social sites.<br>
          2) The above form must be filled and status must be enabled, that's it.<br>
         3) Rich snippet is inserted automatically on product page and home page.<br>
4)You can also verify if rich snippet is working in verification tab.";
$_['text_social_widgets']    	 = 'Show Social widgets on product page:<div class="help">Many themes uses add this widget for displaying social widgets. If this is enabled it will show widgets provided by social networking sites.</div>';

?>