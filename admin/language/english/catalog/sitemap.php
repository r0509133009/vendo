<?php
$_['heading_title'] = 'Seo Sitemap Generator';
$_['seordata']   = 'Generate a updated sitemap for store with a single click<br>This sitemap helps search engines to index url<br>Keep it updated with a click.';
$_['help']   = "1) It is necessary that your sitemap must be updated with latest links or new pages added.<br>
          2) If new pages or product links are not added in sitemap there can be issue.<br>
         3) The issue will be that those links will never get indexed or get indexed very late.<br>
<h4>How to use?</h4>
When you add new products or categories you just need to click this button.<br>
You can also do it in intervals like every 3 days or 5 days or as convienient.<br><br>If sitemap exist, a link will appear where you can view that sitemap.<br><br><b>Please note this sitemap is used by search engines and sitemap that comes with opencart is for humans.</b>";
$_['button_generate']  = 'Generate Sitemap';
$_['text_success']           = 'Success: You have Generated Sitemap Successfully!!'; 
?>