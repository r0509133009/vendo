<?php
// Heading
$_['heading_title']          = 'Social Rich Snippets For Good Seo';
$_['text_success']           = 'Success: You have modified Rich Snippets!'; 
$_['grsnippet']              = 'Google Rich Snippets For Seo';
$_['business_type']          = 'Buisness Type:<div class="help">Select your business type from following options </div>';
$_['payment_method']         = 'Select Payment Method<div class="help">Select payment method which will be submitted to google. You can select multiple methods.</div>';
$_['delivery_method']        = 'Select Delivery Method<div class="help">Select delivery method which will be submitted to google. You can select miltiple methods.</div>';
$_['entry_google_status']    = 'Google Rich Snippet Status<div class="help">Enable google rich snippets, to submit data to search engines</div>';
$_['entry_facebook_status']    = 'Facebook Rich Snippet Status<div class="help">Enable facebook rich snippets, submit attractive seo data to facebook when people like or share on facebook.</div>';
$_['entry_twitter_status']    = 'Twitter Rich Snippet Status<div class="help">Enable twitter rich snippets, to submit attractive seo data to twitter when people tweet about product page.</div>';
$_['button_save']            = 'Save';
$_['tab_achieve']            = 'What you achieve?';
$_['tab_verify']          	 = 'Let us verify!!';
$_['tab_company']            = 'Form Details Page';
$_['text_googlepageid_help'] = "If this is provided to us, we will link your store with Google plus page.<br>This authorship will be displayed in google search results.";
$_['text_facebookadminid_help'] = "If this is provided to us, we will link your store with give your Facebook page.<br>It gives you access to analytics about how your store content is shared on Facebook.";
$_['text_twitterusername_help'] = "If this is provided to us, we will link your store with give your Twiiter username.<br>When someone tweets about your products, then your twitter username will also be tweeted.";
$_['text_googlepageid']      = 'Google Plus Page Id<div class="help">If you have google plus page for your store. Enter the page id. You can know your google plus page id from here: <a href="http://www.twelveskip.com/tutorials/google-plus/1134/how-to-find-your-google-plus-id-number" target="_blank">Google Page Id</a></div>';
$_['text_twitterusername']     = 'Twitter User Name<div class="help">If you have twitter account for your store. Enter the twitter username. You can know twitter username from here : <a href="http://help.wisestamp.com/email-apps/username/how-do-i-find-my-twitter-username" target="_blank">Twitter Username</a> </div>';
$_['text_facebookadminid']     = 'Facebook Admin Id<div class="help">If you have facebook page for your store and you are owner of it. Enter your id. You can know your id from here: <a href="http://developers.facebook.com/tools/explorer/" target="_blank">Facebook Admin Id</a></div>';
$_['company_name'] 		    = 'Store Name:<div class="help">The Name of the store that needs to be displayed in search results of Search Engine</div>';
$_['country_name']  		= 'Country Name:<div class="help">Country where store is located. The place of origin</div>';
$_['locality_name'] 		= 'Locality Name:<div class="help">Any Famous Locality near by store location</div>';
$_['postal_code'] 		    = 'Postal Code:<div class="help">Postal Code where store is located</div>';
$_['street_address'] 		= 'Street Address:<div class="help">Street Address of store</div>';
$_['telephone_number'] 		= 'Telephone Number:<div class="help">Store Contact Number</div>';
$_['check_impact']         = 'Go to next tab above to know how this field is most important';
$_['store_help'] 		=  'Fill the form below to make best use of all the snippets.<br>Details below will be used by snippets designed for Facebook, Google, Twitter.';
$_['verify_help'] 		= 'Would you like to verify rich snippets your self, check below';
$_['achieve_help'] 		= 'What you will achieve is important, have a look below:';
$_['twitter_help'] 		= 'Following details below will be inserted on product page in rdfa format.<br>This rdfa format details are understood by Search Engine Google.';
$_['text_about']   = "1) Rich Snipppet tools  are used search engines and social sites.<br>
          2) The above form must be filled and status must be enabled, that's it.<br>
         3) Rich snippet is inserted automatically on product page and home page.<br>
4)You can also verify if rich snippet is working in verification tab.";
?>