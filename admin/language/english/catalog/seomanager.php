<?php
// Heading
$_['heading_title'] = 'Seo Manager For Redirection';

// Text
$_['text_success']  = 'Success: You have successfully modified redirection url!';
$_['from_url']   = 'Broken Link';
$_['to_url']   = 'New Link';
$_['status']    = 'Status';
$_['mstatus']    = 'Master Status:<div class="help">This will make all redirection off</div>';
$_['times_used']    = 'Redirection Done';
$_['delete']    = 'Delete';
$_['button_insert'] = 'Insert New Redirect';
$_['button_save']  = 'Save All';
$_['url_test']  = 'Redirection Test';
$_['url_test_redirect']  = 'Test Redirection';
$_['failed'] = "Check Failed Url";
$_['text_redirect_table']  = 'Check Failed Url Table <div class ="help">Click to view failed url\'s that occured on your site. And assign them a correct redirect</div>';
$_['help']   = "1)Seo Manager is tool to add redirects to failed / old / not existing url's<br>
          2) You just need to enter the full url which need to be redirected.<br>
         3) You can example below:<br><br>
<h4>Suppose I need to redirect link</h4>
http://store.com/old-product-link<br><br>to<br><br>
http://store.com/new-product-link<br><br><b>So Broken link will contain Link 1 and New link will contain Link 2</b><br><b>And you can also test by clicking Test Redirection</b>";
?>