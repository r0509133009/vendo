<?php
// Heading
$_['heading_title']    	= 'SEO Package Sitemap';

// Text 
$_['text_feed']        	= 'Product Feeds';
$_['text_success']		= 'Success: You have modified Google Sitemap feed!';
$_['text_info']			= 'Give either the full feed or each language based feeds links to google and search engines.';

// Entry
$_['entry_status']     	= 'Status:';
$_['entry_data_feed']	= 'Full Feed:<span class="help">Feed containing all languages using hreflang tag (hreflang must be enabled in complete seo options)</span>';
$_['entry_lang_feed']	= 'Lang based feeds:<span class="help">If you prefer separate language version, use these</span>';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify SEO Package Sitemap feed!';
?>