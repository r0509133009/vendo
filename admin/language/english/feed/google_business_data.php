<?php
// Heading
$_['heading_title']    = 'Google Business Data (for remarketing via analytics)';

// Text   
$_['text_feed']        = 'Product Feeds';
$_['text_success']     = 'Success: You have modified Google Bussines Data feed!';

// Entry
$_['entry_status']     = 'Status:';
$_['entry_file']     = 'Save to file only<br /><span class="help">The feed is not displayed directly, but is saved in ../download/feeds/.</span>';
$_['entry_data_feed']  = 'Data Feed Url:';
$_['entry_google_business_data_description']  = 'Use meta description:';
$_['entry_google_business_data_description_html']  = 'Remove html tags from the description:';
$_['entry_feed_business_data_id1']  = 'Main product ID:<br /><span class="help">Main product ID used on the remarketing tag (dynx_itemid).</span>';
$_['entry_feed_business_data_id2']  = 'Second product ID:<br /><span class="help">Optional second product ID used on the remarketing tag (dynx_itemid2). Select empty if you are not using a second product ID.</span>';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Google Base feed!';
?>
