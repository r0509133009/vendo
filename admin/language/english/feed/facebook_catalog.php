<?php
// Heading
$_['heading_title']    = 'Facebook Product Catalog';

// Text   
$_['text_feed']        = 'Product Feeds';
$_['text_success']     = 'Success: You have modified Facebook Product Catalog feed!';

// Entry
$_['entry_status']     = 'Status:';
$_['entry_file']     = 'Save to file only<br /><span class="help">The feed is not displayed directly, but is saved in ../download/feeds/.</span>';
$_['entry_data_feed']  = 'Data Feed Url:';
$_['entry_facebook_catalog_availability']  = 'Products with 0 quantity set as "available for order":';
$_['entry_facebook_catalog_description']  = 'Use meta description (recommend if available):';
$_['entry_facebook_catalog_description_html']  = 'Remove html tags from the description:';
$_['entry_facebook_attribute_type']  = 'Source of the product type value<br /><span class="help">Category based product type or an attribute. If selected attribute is not set, category is used.</span>';
$_['entry_facebook_attribute_product_type']  = 'Generated from categories (default)';
$_['entry_feed_facebook_id1']  = 'Product ID:';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Facebook Product Catalog feed!';
?>
