<?php
// Heading
$_['heading_title']    = 'Google Merchant Center';

// Text   
$_['text_feed']        = 'Product Feeds';
$_['text_success']     = 'Success: You have modified Google Merchant Center feed!';

// Entry
$_['entry_status']     = 'Status:';
$_['entry_file']     = 'Save to file only<br /><span class="help">The feed is not displayed directly, but is saved in ../download/feeds/.</span>';
$_['entry_data_feed']  = 'Data Feed Url:';

$_['entry_google_merchant_category']     = 'Google merchant center category';
$_['entry_google_merchant_center_attribute']  = 'Source of the color value<br /><span class="help">Set a color, or select an attribute in Extensions->Product Feeds->[Google Merchant Center].</span>';
$_['entry_google_merchant_center_attribute_type']  = 'Source of the product type value<br /><span class="help">Category based product type or an attribute. If selected attribute is not set, category is used.</span>';
$_['entry_google_merchant_center_attribute_product_type']  = 'Generated from categories (default)';
$_['entry_google_merchant_center_option']  = "Source of the size value - options (Apparel & Shoes only)<br /><span class='help'>Select an attribute, or set the color in Catalog->Products[edit](Google merchant center).</span>";
$_['entry_google_merchant_center_attribute_product']  = 'Set colors in products (merchant tab)';
$_['entry_google_merchant_center_availability']     = 'Sold out items<br /><span class="help">Defines how to mark zero stock products in the feed.</span>';
$_['entry_google_merchant_center_shipping_flat']  = 'Shipping rate<br /><span class="help">Shipping flat rate for the product feed (optional - only if not set in your google merchant account).</span>';
$_['entry_google_merchant_base']  = 'Main categories<br /><span class="help">Selecting your category will reduce the amount of options available in the category setup Catalog->Categories[Edit](Data).</span>';
$_['tab_taxonomy']  = 'Google merchant center';
$_['entry_google_merchant_gender']     = 'Gender <br /><span class="help">Apparel & Shoes only</span>';
$_['entry_google_merchant_age_group']     = 'Age group <br /><span class="help">Apparel & Shoes only</span>';
$_['entry_google_merchant_color']     = 'Color (if not set via attributes)<br /><span class="help">Set the color of the item, if you use an atribute for color, select the atribute in the Extensions->Product Feeds[Google Merchant Center].</span>';
$_['entry_google_merchant_center_description']  = 'Use meta description:';
$_['entry_google_merchant_center_description_html']  = 'Remove html tags from the description:';
$_['entry_feed_merchant_center_id1']  = 'Product ID:<br /><span class="help">Product ID used on the remarketing tag (ecomm_prodid).</span>';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Google Merchant Center feed!';
?>
