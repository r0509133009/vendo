<?php
  $_['heading_title']                = 'Banned address';
  $_['column_action']                = 'Action';
  $_['column_user_name']                = 'Username';
  $_['column_date_added']                = 'Date Added';
  $_['column_date_expired']                = 'Expiration Date';
  $_['column_ip']                = 'IP address';
  $_['text_forever']                = 'Forever';
  $_['entry_user_name']                = 'Username:';
  $_['entry_comment']                = 'Comment:';
  $_['entry_expired']                = 'Expiration:';
  $_['entry_ip']                = 'IP address:';
  $_['title_expired']                = 'Leave empty for never ending ban';
  $_['text_success']                = 'Banned address was successfuly deleted!';
?>