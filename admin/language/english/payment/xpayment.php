<?php
// Heading
$_['heading_title']    = 'X-Payment';
$_['tab_general']    = 'General Setting';
$_['tab_rate']    = 'Method Setting';

// Text 
$_['text_payment']    = 'payment';
$_['text_success']     = 'Success: You have modified X-Payment!';

$_['text_select_all']    = 'Select All';
$_['text_unselect_all']    = 'Unselect All';
$_['text_any']    = 'For Any';

$_['text_zip_postal']    = 'Zip/Postal';
$_['text_enter_zip']    = 'Enter Zip/Postal Code';
$_['text_zip_rule']    = 'Zip/Postal Rule';
$_['text_zip_rule_inclusive']    = 'Only for entered zip/postal codes';
$_['text_zip_rule_exclusive']    = 'For all except entered zip/postal codes';

// Entry 
$_['entry_customer_group'] = 'Customer Group:';
$_['entry_store'] = 'Store:';
$_['entry_manufacturer'] = 'Manufacturer:';
$_['store_default'] = 'Default';
$_['entry_name']       = 'Payment Name:';
$_['entry_desc']       = 'Description:';
$_['entry_order_total']       = 'Order Total Range:';
$_['entry_order_weight']       = 'Weight Range:';
$_['entry_to']       = 'to';
$_['entry_order_hints']       = 'Please enter 0 (zero) if not applicable';
$_['entry_tax']        = 'Tax Class:';
$_['entry_geo_zone']   = 'Geo Zone:';
$_['entry_status']     = 'Status:';
$_['entry_sort_order'] = 'Sort Order:';
$_['entry_customer_group'] = 'Customer Group:';
$_['text_all'] = 'Any';
$_['text_category'] = 'Category Rule';
$_['text_category_any'] = 'For any categories';
$_['text_category_all'] = 'Must have selected categories';
$_['text_category_least'] = 'Any of the selected categories';
$_['text_category_except'] = 'Except for the selected categories';
$_['text_category_exact'] = 'Only for the selected categories';
$_['entry_category']         = 'Categories:<br /><span class="help">(Autocomplete)</span>';
$_['entry_instruction']         = 'Payment Instruction:<br /><span class="help">(HTML allowed)</span>';
$_['keywords_hints']        = 'You can use these keywords in the description';
$_['entry_order_status'] = 'Order Status:';
$_['entry_callback'] = 'Callback URL:';
$_['entry_redirect'] = 'Redirct URL:';
$_['entry_redirect_type'] = 'Redirect Data Method:';
$_['entry_redirect_post'] = 'POST';
$_['entry_redirect_get'] = 'GET';
$_['entry_success'] = 'Success URL:';


$_['text_debug']    = 'Debugging:';
$_['text_inc_email']    = 'Send Instruction in Order Email:';
$_['text_inc_order']    = 'Show Instruction in Order Detail:';
$_['text_instruction_email']    = 'Instruction for Email:';
$_['tip_email_instruction']        = 'Optional. If you want to send different instruction in order email, you can use it.';


// Error
$_['error_permission'] = 'Warning: You do not have permission to modify X-Payment!';

$_['tip_callback']       = 'If you want to call/execute any url after check out process done. It will be called silently. Please enter here.';
$_['tip_redirect']       = 'If you want to redirect to a url after clicking on confirm button, Please enter here.';
$_['tip_redirect_data']       = 'It will send order data (Order ID and Order Amount Only) either post or get method to your provided redirect URL';
$_['tip_order_status']       = 'Please select the order status what you want to apply after checking out using this method';
$_['tip_instruction']       = 'Please enter the payment instruction for the customer. It will show in the confirmation step in the checkout process. You can add placeholder in the instruction. Available placeholders are - {orderId}, {orderTotal}';
$_['tip_sorting_own']       = 'Sorting order with respective to x-payment methods';
$_['tip_status_own']       = 'Enable/Disable this particular method only';
$_['tip_store']       = 'Please Select Stores for which this payment method will work';
$_['tip_geo']       = 'Please Select Geo Zones for which this payment method will work';
$_['tip_manufacturer']       = 'Please Select Manufacturer for which this payment method will work';
$_['tip_customer_group']       = 'Please Select customer groups for which this payment method will work';
$_['tip_zip']       = 'Please enter zip/postal for which this payment method will work';
$_['tip_category']       = '<b>For any categories</b>: Valid for any categories.<br /><b>Must have selected categories</b>: Selected categories must have in the payment cart with  other categories.<br /><b>Any of the selected categories</b>: At least one of the selected category must have in the payment cart with  other categories.<br /><b>Only for the selected categories</b>: All cart categories should be in the selected categories. Other categories are not allowed.<br /><b>Except for the selected categories</b>: Shopping cart should not have any of the selected categories. Only non-selected categories are allowed .';
$_['tip_weight']       = 'Please enter weight range for which it will work';
$_['tip_total']       = 'Please enter order total range for which it will work';
$_['tip_desc']       = 'Optional field. It will show a supportive small description under payment method name in the site.';
$_['tip_status_global']       = 'Global Status of the module';
$_['tip_sorting_global']       = 'Global Sorting Order with respective to other payment modules';
$_['tip_debug']       = 'You can debug for which conditions a payment has failed. It is very useful for debugging.';
$_['tip_success']       = 'If you want to overwrite success page URL, Please enter here. Placeholder allowed: {orderId}, {orderTotal}';
?>