<?php

/**
 * eBORICA payment gateway for Opencart by Extensa Web Development
 *
 * Copyright © 2011-2015 Extensa Web Development Ltd. All Rights Reserved.
 * This file may not be redistributed in whole or significant part.
 * This copyright notice MUST APPEAR in all copies of the script!
 *
 * @author 		Extensa Web Development Ltd. (www.extensadev.com)
 * @copyright	Copyright (c) 2011-2015, Extensa Web Development Ltd.
 * @package 	eBORICA payment gateway
 * @link		http://www.opencart.com/index.php?route=extension/extension/info&extension_id=3004
 */

// Heading
$_['heading_title']                        = 'Direct payment with credit/debit card (eBORICA)';

// Text
$_['text_payment']                         = 'Payment';
$_['text_success']                         = 'Success: You have modified the eBORICA details!';
$_['text_borica']                          = '<a onclick="window.open(\'http://borica.bg/\');"><img src="view/image/payment/borica.gif" alt="eBORICA" title="eBORICA" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_save_before_generate']            = 'Save the settings before generate Private key and Certificate signing request!';
$_['text_generate']                        = 'Generate';
$_['text_download']                        = 'Download';
$_['text_download_ppk']                    = 'Download PPK';
$_['text_send_to_bank']                    = '(and send it to your bank)';
$_['text_copy_send_to_bank']               = '(copy the link and send it to your bank)';
$_['text_copy_etlog_certificate']          = 'Copy and paste the eTLog certificate from the bank';
$_['text_copy_certificates']               = 'Copy and paste the certificates from the Administrative interface (eTLog)';
$_['text_import_into_browser']             = 'Import the .p12 file into your browser';
$_['text_success_generate']                = 'Success: You have generated Private key and Certificate signing request!';
$_['text_confirm_generate']                = 'ATTENTION! If you again generate Private key and Certificate signing request you have to send it to the bank, it has to send you back the new Certificates and you have to paste them in the details. The Payment will be not working until then!';

// Button
$_['button_get_p12']                       = 'Save and download .p12 file';
$_['button_save_certificates']             = 'Save the certificates';

// Entry
$_['entry_test']                           = 'Test Mode:';
$_['entry_terminal']                       = 'Terminal ID:<br/><span class="help">8 digit code provided by your bank</span>';
$_['entry_description']                    = 'Short Order Description:';
$_['entry_country']                        = 'Country:';
$_['entry_state']                          = 'State/Province:<br/><span class="help">in latin</span>';
$_['entry_locality']                       = 'Locality (city):<br/><span class="help">in latin</span>';
$_['entry_name']                           = 'Name (e.g. site domain):<br/><span class="help">in latin</span>';
$_['entry_email']                          = 'E-Mail:';
$_['entry_organization']                   = 'Organization (company):<br/><span class="help">in latin</span>';
$_['entry_organization_unit']              = 'Organization Unit:';
$_['entry_total']                          = 'Total:<br /><span class="help">The checkout total the order must reach before this payment method becomes active.</span>';
$_['entry_order_status']                   = 'Order Status Completed:';
$_['entry_order_status_canceled']          = 'Order Status Canceled:';
$_['entry_order_status_canceled_reversal'] = 'Order Status Canceled Reversal:';
$_['entry_order_status_denied']            = 'Order Status Denied:';
$_['entry_order_status_failed']            = 'Order Status Failed:';
$_['entry_currency']                       = 'Currency:';
$_['entry_geo_zone']                       = 'Geo Zone:';
$_['entry_status']                         = 'Status:';
$_['entry_sort_order']                     = 'Sort Order:';
$_['entry_test_private_key']               = 'Test Private key:';
$_['entry_real_private_key']               = 'Real Private key:';
$_['entry_etlog_private_key']              = 'eTLog Private key:';
$_['entry_test_key_csr']                   = 'Test Private key and Certificate signing request (CSR):';
$_['entry_real_key_csr']                   = 'Real Private key and Certificate signing request (CSR):';
$_['entry_etlog_key_csr']                  = 'eTLog Private key and Certificate signing request (CSR):<br/><span class="help">for transactions monitoring system</span>';
$_['entry_test_certificate']               = 'Test certificate:';
$_['entry_real_certificate']               = 'Real certificate:';
$_['entry_etlog_certificate']              = 'eTLog certificate:';
$_['entry_return_url']                     = 'Return URL:';

// Error
$_['error_permission']                     = 'Warning: You do not have permission to modify eBORICA!';
$_['error_terminal']                       = 'Invalid Terminal ID (it has to be 8 digit code)!';
$_['error_description']                    = 'Description Required!';
$_['error_country']                        = 'Country Required!';
$_['error_state']                          = 'Invalid State!';
$_['error_locality']                       = 'Invalid Locality!';
$_['error_name']                           = 'Invalid Name!';
$_['error_email']                          = 'Invalid E-Mail!';
$_['error_organization']                   = 'Invalid Organization!';
$_['error_generate']                       = 'To generate the Certificate signing request, first you have to fill in the data!';
$_['error_openssl']                        = 'There was an error while generating Certificate signing request!<br />%s';
$_['error_download']                       = 'To download the Certificate signing request, first you have to generate it!';
$_['error_certificate']                    = 'Invalid certificate!';
$_['error_generate_p12']                   = 'To generate the .p12 file, first you have to generate eTLog Private key!';
?>