<?php

/**
 * Epay.bg payment gateway for Opencart by Extensa Web Development
 *
 * Copyright � 2010-2015 Extensa Web Development Ltd. All Rights Reserved.
 * This file may not be redistributed in whole or significant part.
 * This copyright notice MUST APPEAR in all copies of the script!
 *
 * @author 		Extensa Web Development Ltd. (www.extensadev.com)
 * @copyright	Copyright (c) 2010-2015, Extensa Web Development Ltd.
 * @package 	Epay.bg payment gateway
 * @link		http://www.opencart.com/index.php?route=extension/extension/info&extension_id=2999
 */

// Heading
$_['heading_title']              = 'ePay.bg';

// Text
$_['text_payment']               = 'Payment';
$_['text_success']               = 'Success: You have modified the ePay details!';
$_['text_epay']                  = '<a onclick="window.open(\'https://www.epay.bg/\');"><img src="view/image/payment/epay.png" alt="ePay.bg" title="ePay.bg" style="border: 1px solid #EEEEEE;" /></a>';

// Entry
$_['entry_test']                 = 'Test Mode:';
$_['entry_email']                = 'E-Mail:';
$_['entry_client_number']        = 'Client Number (MIN):';
$_['entry_secret_key']           = 'Secret Key (SECRET):';
$_['entry_description']          = 'Description:<br/><span class="help">up to 100 symbols</span>';
$_['entry_expired_days']         = 'Payment Expired Days:';
$_['entry_total']                = 'Total:<br /><span class="help">The checkout total the order must reach before this payment method becomes active.</span>';
$_['entry_order_status']         = 'Order Status Completed:';
$_['entry_order_status_denied']  = 'Order Status Denied:';
$_['entry_order_status_expired'] = 'Order Status Expired:';
$_['entry_geo_zone']             = 'Geo Zone:';
$_['entry_status']               = 'Status:';
$_['entry_sort_order']           = 'Sort Order:';

// Error
$_['error_permission']           = 'Warning: You do not have permission to modify ePay!';
$_['error_email']                = 'E-Mail Required!';
$_['error_client_number']        = 'Client Number Required!';
$_['error_email_client_number']  = 'E-Mail OR Client Number Required!';
$_['error_secret_key']           = 'Secret Key Required!';
$_['error_description']          = 'Description Required!';
$_['error_expired_days']         = 'Expired Days must be number greater than 0!';
?>