<?php
// Heading
$_['heading_title']      = 'Offline payments';

// Text
$_['text_payment']       = 'Payment';
$_['text_success']       = 'Success: You have modified Offline payments module!';

// Entry
$_['entry_total']        = 'Total:<br /><span class="help">The checkout total the order must reach before this payment method becomes active.</span>';
$_['entry_order_status'] = 'Order Status:';
$_['entry_order_paid_status'] = 'Order Paid Status:';
$_['entry_geo_zone']     = 'Geo Zone:';
$_['entry_status']       = 'Status:';
$_['entry_sort_order']   = 'Sort Order:';
$_['entry_header']   	 	 = 'Offline Payments:';
// Error
$_['error_permission']   = 'Warning: You do not have permission to modify Offline payments!';
?>