<?php
// Heading
$_['tab_remarketing']                		= 'Remarketing Manager';

// Entry
$_['entry_google_remarketing_analytics']                = 'Use google analytics for remarketing (all countries):<br /><span class="help">If Google Merchant center is not available in your country select Yes. This allows dynamic remarketing for all countries via Google Analytics. When you are creating your dynamic remarketing campaign, select as your Business Type "Other (custom option)" instead of "Retail"!<br>Google Analytics code must include ga(\'require\', \'displayfeatures\'); and remarketing must be enabled in Google Analytics.</span>';
$_['entry_remarketing_pagetype_index']                  = 'Dimension index (dynx_pagetype):<br /><span class="help">This setting is required.</span>';
$_['entry_remarketing_id1_index']                = 'Dimension index (dynx_itemid):<br /><span class="help">All dimension indexes can be found/created in Google Analytics, under Admin->Custom Definitions->Custom Dimensions. This setting is required.</span>';
$_['entry_remarketing_id2_index']                  = 'Dimension index (dynx_itemid2):<br /><span class="help">This setting is optional and should be set only, if you are using a second product ID in the product feed.</span>';
$_['entry_remarketing_totalvalue_index']              = 'Dimension index (dynx_totalvalue):<br /><span class="help">This setting is required.</span>';
$_['entry_google_remarketing_code']                    = 'Google remarketing script:<br /><span class="help">Login to your <a href="https://adwords.google.com/cm/CampaignMgmt?#uls.uls&app=cm" target="_blank"><u>Google Adwords</u></a> account and get your remarketing script from Tag Details->Setup.</span>';
$_['entry_google_remarketing_id1']                  = 'Product ID used in the product feed (Merchant & Analytics):';
$_['entry_google_remarketing_id2']       = 'Second product ID used in your product feed (Analytics only):<br /><span class="help">Select empty if you are not using a second product ID. If you are not sure, leave it empty.</span>';
$_['entry_google_remarketing_random']                 = 'Use random products on category/search pages:<br /><span class="help">Will increase successful product hits, but decrease precision.</span>';
$_['entry_google_multi_analytics']               = 'Multistore Google Analytics Code:<br /><span class="help">Login to your <a href="http://www.google.com/analytics/" target="_blank"><u>Google Analytics</u></a> account and after creating your web site profile copy and paste the analytics code into this field.</span>';
$_['entry_facebook_remarketing_code']                = 'Facebook audience pixel code<br /><span class="help">Login to your <a href="https://www.facebook.com" target="_blank"><u>Facebook</u></a> account and after creating your custom audience pixel, copy and paste the code into this field.<br />The audience pixel code is also used for conversion tracking.</span>';
$_['entry_facebook_remarketing_id']                = 'Product ID used in your product feed (Facebook):';
?>

