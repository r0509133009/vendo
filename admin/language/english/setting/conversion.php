<?php
// Heading
$_['tab_conversion']                		= 'Conversion Manager';

// Entry
$_['entry_google_conversion_code']                = 'Google conversion tracking code<br /><span class="help">Login to your <a href="https://adwords.google.com/ct/ConversionTracking/Manager" target="_blank"><u>Google Adwords</u></a> account and after creating your conversion action, copy and paste the tracking code into this field. In the value section select "The value of this conversion action may vary", so you can use the dynamic value feature.</span>';
$_['entry_google_conversion_value']                = 'Dynamic google conversion value<br /><span class="help">If enabled the conversion code will contain the value of the conversion and its currency (in google adwords the "value" setting must be set to may vary).</span>';
$_['entry_universal_conversion_code']                = 'Universal conversion tracking code<br /><span class="help">Copy and paste into this field all other conversion codes, which you want to use on your checkout success page.<br />Available dynamic codes:<br />#order_id<br />#total<br />#currency<br />Example: conversion_value=\'#total\';</span>';
$_['entry_google_ecommerce_value']                = 'Google e-commerce conversion tracking<br /><span class="help">If enabled, the google e-commerce script will be added to the google analytics code of the success page.</span>';

$_['text_google_conversion_code']                = 'Google conversion';
$_['text_universal_conversion_code']                = 'Universal conversion';

?>
