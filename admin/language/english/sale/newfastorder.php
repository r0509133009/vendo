<?php  
// Heading
$_['heading_title']        		= 'Quick order';
$_['heading_title_setting']     = 'Settings';
// Text
$_['text_success']        		= 'The order list is updated';
$_['text_id']      	 	 		= '№';
$_['text_name']      	  		= 'Name';
$_['text_manager']      	  	= 'Manager';
$_['text_manager_form']      	= 'Select manager';
$_['text_telephone']   	  		= 'Phone';
$_['text_comment']     			 = 'comment Manager';
$_['text_comment_buyer']   	 	= 'Note Buyer';
$_['text_email_buyer']   	 	= 'E-mail Buyer';
$_['text_product_name']   	 	= 'Product';
$_['text_product_price']   	 	= 'Price';
$_['text_product_image']   	 	= 'Product photo';
$_['text_newfastorder_url']   	= 'URL from which the ordered goods';
$_['text_status']      	 		= 'Status';
$_['text_added']      	 		= 'Order date';
$_['text_modified']       		= 'Date of change';
$_['text_action']         		= 'Action';
$_['text_edit']       	  		= 'Edit';
$_['text_total_all']       	  	= 'TOTAL';
$_['product_name_fast']        	= 'Product name';
$_['number_order_id']        	= 'ORDER NUMBER';
$_['text_number_order_id_']     = '№ ORDER - ';
$_['button_fastorder_setting']  = 'Setting';
$_['text_link']  			 	= 'Link';
$_['status_wait']         		= 'Expectation';
$_['status_done']         		= 'Processed';
$_['text_link_description']     = 'Page from which the ordered products';
$_['column_name']         		= 'Name';
$_['column_telephone']    		= 'Phone';
$_['column_date_added']   		= 'Order date';

$_['button_delete']         	= 'Delete';
$_['button_save']         		= 'Save';
$_['button_cancel']         	= 'Cancel';



/*SETTING*/

$_['register_site_fastorder'] 									= 'Please register:';
$_['entry_instruction_icon_change_fastorder'] 					= '<span style="color:#727272;">To change the icon on the button, open the site<a href="http://fortawesome.github.io/Font-Awesome/icons/"  target="_blank">http://fortawesome.github.io/Font-Awesome/icons/</a> select an icon, open it and copy class</br> it is like this ->       fa fa-phone-square      <-:</span>';
$_['entry_phone_number_send_sms_fastorder'] 					= 'Enter the number for sms:';
$_['entry_login_send_sms_fastorder'] 							= 'Username that was registered on the site:';
$_['entry_pass_send_sms_fastorder'] 							= 'Password that is registered on the site:';
$_['text_yes_fastorder'] 										= 'Yes:';
$_['text_no_fastorder'] 										= 'No:';
$_['on_off_sms_fastorder'] 										= 'On Off sending SMS:';
$_['entry_icon_send_fastorder'] 								= 'Change the icon on the [ORDER]:';
$_['entry_background_button_send_fastorder'] 					= 'Change the color of the [ORDER]';
$_['entry_background_button_send_fastorder_hover'] 				= 'Change the color of the [ORDER] when you move:';
$_['entry_background_button_open_form_send_order'] 				= 'Change the color of the [QUICK ORDER]:';
$_['entry_icon_open_form_send_order'] 							= 'Change the icon of the [QUICK ORDER]:';
$_['entry_icon_open_form_send_order_size'] 						= 'Change the size of icons [QUICK ORDER]:';
$_['entry_color_button_open_form_send_order'] 					= 'Change Change the color of icons and text:';
$_['entry_text_open_form_send_order'] 							= 'Change the name of the [QUICK ORDER]:';
$_['entry_any_text_at_the_bottom'] 								= 'Add any text after the [ORDER]:';
$_['entry_img_fastorder'] 										= 'Adding images to the [QUICK ORDER]:';
$_['entry_any_text_at_the_bottom_color'] 						= 'Change the color of the text after the [ORDER]:';
$_['entry_background_button_open_form_send_order_hover'] 		= 'Change the color of the [QUICK ORDER] hover:';
$_['entry_mask_phone_number'] 									= 'Edit Mask phone (+3(999) 999-99-99) or like this 79(999)99-99-999:';
$_['text_image_manager'] 										= 'image editor';
$_['text_browse'] 												= 'change';
$_['text_clear'] 												= 'Delete';

$_['text_status_fields'] 										= 'Status field';
$_['text_requared_fields'] 										= 'Required';
$_['text_placeholder_fields'] 									= 'Placeholder';
$_['text_on_off_fields_firstname'] 								= 'Fields - Name -';
$_['text_on_off_fields_phone'] 									= 'Fields - Phone -';
$_['text_on_off_fields_comment'] 								= 'Fields - Comment -';
$_['text_on_off_fields_email'] 									= 'Fields - Email -';

$_['tab_fields_setting'] 										= 'Form fields';
$_['tab_general_setting'] 										= 'General Setting';
$_['tab_design_setting'] 										= 'Design';
$_['tab_sms_setting'] 											= 'SMS Settings';
$_['tab_email_setting'] 										= 'Customize Template Letters';

$_['text_on_off_shipping_method'] 								= 'Shipping method';
$_['text_on_off_payment_method'] 								= 'Payment method';
$_['entry_title_popup_quickorder'] 								= 'Header Title Popup';

$_['form_latter_from_buyer'] 									= 'A template letter for the buyer!';
$_['form_latter_from_me'] 										= 'A template letter for you [Admin Store]!';
$_['text_on_off_send_buyer_mail'] 								= 'Send an email to the buyer';
$_['text_on_off_send_me_mail'] 									= 'Send an e-mail to you [Admin]';
$_['text_on_off_send_me_mail'] 									= 'Send an e-mail to you [Admin]';
$_['select_design_fast_order'] 									= 'Select Design Fast Order';
$_['text_theme1'] 												= '№ - 1';
$_['text_theme2'] 												= '№ - 2';
$_['text_theme3'] 												= '№ - 3 Right';
$_['text_theme4'] 												= '№ - 3 Left';
$_['quickorder_subject_buyer'] 									= 'Letter subject';
$_['subject_text_variables'] 									= 'It supports variable';
$_['quickorder_description_buyer'] 								= 'Email Template';
$_['text_form_latter_products'] 								= 'Attach Template letter - the goods which ordered';
$_['text_complete_quickorder'] 									= 'Text after a successful checkout';
$_['heading_title_activation'] 									= 'Activation Module';
$_['add_activation_key'] 										= 'Enter the license key:';
$_['btn_activation'] 											= 'Activate';
$_['the_module_is_activated'] 									= 'The module is activated!';
$_['key_success_deactivation'] 									= 'Module deactivation!';
$_['this_key_is_not_present_enter_the_correct_key'] 			= '<span style="color:red;">Error:</span> this key is not present, enter the correct key';
$_['key_error_deactivation'] 									= '<span style="color:red;">Error:</span> Unable to deactivation key';
$_['enter_key_deactivation'] 									= '<span style="color:red;">Error:</span> deactivation key is not specified.!';
$_['enter_deactivation_key'] 									= 'Enter key Deactivation';
$_['btn_deactivation'] 											= 'Deactivation key';

$_['list_of_variables_entry'] 						= '
<table><tr><td>
<br/><b>~name_fastorder~</b><i style="font-weight:400"> - Customer Name</i>
<br/><b>~phone~</b><i style="font-weight:400"> - Phone</i>
<br/><b>~comment_buyer~</b><i style="font-weight:400"> - comment</i>
<br/><b>~email_buyer~</b><i style="font-weight:400"> - email</i>      
<br/><b>~url_site~</b><i style="font-weight:400"> - url page there order products</i> 
<br/><b>~price_shipping_text~</b><i style="font-weight:400"> - shhiping price </i> 
<br/><b>~shipping_title~</b><i style="font-weight:400"> - shhiping method</i>          
<br/><b>~payment_title~</b><i style="font-weight:400"> - payment method </i>        			
<br/><b>~ip_store~</b><i style="font-weight:400"> - ip </i>
<br/><b>~currency_code~</b><i style="font-weight:400"> - Currency</i>
<br/><b>~store_name~</b><i style="font-weight:400"> - Name of shop</i>             
<br/><b>~store_url~</b><i style="font-weight:400"> - url page  </i> 
</td></tr></table><br/>
';
/*SETTING*/
$_['text_setting_popup_and_button_on_off'] 						= 'Настройки Popup окна и Область показа Кнопки';
$_['text_general_image_product_popup'] 							= 'Отключить изображение товара в Popup Окне';
$_['text_on_off_qo_fm'] 										= 'В модуле Рекомендуемые';
$_['text_on_off_qo_sm'] 										= 'В модуле Акции';
$_['text_on_off_qo_bm'] 										= 'В модуле Хит Продаж';
$_['text_on_off_qo_lm'] 										= 'В модуле Последние поступления';
$_['text_on_off_qo_cpage'] 										= 'В категориях';
$_['text_on_off_qo_special_page'] 								= 'На странице специальных предложений (акций)';
$_['text_on_off_qo_search_page'] 								= 'На странице результатов поиска';
$_['text_on_off_qo_manufacturer_page'] 							= 'На странице производителя';
$_['text_on_off_qo_product_page'] 								= 'На странице товара';
$_['text_on_off_qo_shopping_cart'] 								= 'В корзине';
$_['text_success_save_setting'] 								= 'Настройки успешно сохранены !';
?>
