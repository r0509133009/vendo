<?php 
/*
  Opencart Verzend Status Extension for Opencart
  Version: 1.0, Feb 2014
  Author: Danny Kessels opencart@parkstadmedia.nl
*/

?>
<?php
// Heading
$_['heading_title']    = 'Verzend Status';

// Text
$_['text_success']     = 'Succes: U hebt de wijzigingen van de Verzend Status succesvol opgeslagen!';
$_['text_current_version']     = '1.0';

// Column
$_['column_name']      = 'Verzend Status Name';
$_['column_action']    = 'Aktie';

// Tabs
$_['tab_statuses']      = 'Statussen';
$_['tab_info']    = 'Info';
$_['tab_contact']    = 'Contact';

// Description
$_['description']	= 'Bedankt voor het gebruik van de The Verzend Status Extensie!<br /><br /><br />Voodat u deze Opencart extensie kunt gebruiken, zult u eerst een aantal Verzend Statussen moeten gaan aanmaken zoals:1 dag, 2 dagen, 3 dagen, vandaag besteld morgen in huis etc.<br />
Nadat u dit hebt gedaan gaat u naar \'System --> Settings --> Your Store --> Edit --> TAB Option --> \'Verzend Status\' om de Standaard Verzend Status voor nieuwe producten in te stellen, om de Verzend Status te tonen in de Admin producten lijst en om de (huidige) producten die nog geen Verzend status hebben, de Standaard Verzend Status toe te wijzen.<br /><br />
<strong>In System --> Settings --> Your Store --> Edit --> TAB Option --> Verzend Status :</strong><br /><br />
- <strong>Verzend Status:</strong> Standaard Verzend Status die producten krijgen wanneer u nieuwe producten gaat toevoegen. Deze instelling is ook van belang voor de instelling \'Update Verzend Status\'.<br />
- <strong>Toon Verzend Status:</strong> Zet deze op \'Ja\' om de Verzend Statussen te tonen in de Admin producten lijst. <br />
- <strong>Update Verzend Status:</strong> Zet deze op \'JA\' wanneer u wilt dat de huidige producten in uw systeem de Standaard Verzend Status krijgen toegewezen. In de meeste gevallen zult u deze optie slechts éénmalig nodig hebben (bijvoorbeeld wanneer u deze extensie net heeft geïnstalleerd) wanneer er producten zijn die nog geen Verzend Status hebben.<br /><br />
Wij hopen dat u veel plezier en profijt hebt van deze extensie en mocht u een \'custom\' oplossing nodig hebben kijk dan in de TAB Contact voor onze contactgegevens.<br /><br />
Opmerking:<br />
<strong><em>Er zijn feeds die de \'Verzend Status\' als een verplicht veld hebben. Opencart voorziet standaard niet in dit veld en met deze extensie kunt u dan wel dit veld gebruiken!</em></strong>
';

// Entry
$_['entry_name']       = 'Verzend Status Naam:';

// Error
$_['error_permission'] = 'Waarschuwing: U heeft niet de bevoegdheid om de Verzend Statussen aan te passen!';
$_['error_name']       = 'Verzend Status naam moet tussen 3 en 100 karakters bevatten!';
$_['error_product']    = 'Waarschuwing: Deze verzend Status kan niet verwijderd worden omdat het is toegewezen aan %s producten!';
?>
