<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <?php if ($error) { ?>
  <div class="warning"><?php echo $error; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/payment.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
          <tr>
            <td><?php echo $entry_test; ?></td>
            <td><?php if ($borica_test) { ?>
              <input type="radio" name="borica_test" value="1" checked="checked" />
              <?php echo $text_yes; ?>
              <input type="radio" name="borica_test" value="0" />
              <?php echo $text_no; ?>
              <?php } else { ?>
              <input type="radio" name="borica_test" value="1" />
              <?php echo $text_yes; ?>
              <input type="radio" name="borica_test" value="0" checked="checked" />
              <?php echo $text_no; ?>
              <?php } ?></td>
          </tr>
          <tr>
            <td><?php echo $entry_terminal; ?></td>
            <td><input type="text" name="borica_terminal" value="<?php echo $borica_terminal; ?>" />
              <?php if ($error_terminal) { ?>
              <span class="error"><?php echo $error_terminal; ?></span>
              <?php } ?></td>
          </tr>
          <tr>
            <td><?php echo $entry_description; ?></td>
            <td><input type="text" name="borica_description" value="<?php echo $borica_description; ?>" />
              <?php if ($error_description) { ?>
              <span class="error"><?php echo $error_description; ?></span>
              <?php } ?></td>
          </tr>
          <tr>
            <td><?php echo $entry_country; ?></td>
            <td><select name="borica_country_id" id="country_id">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($countries as $country) { ?>
                <?php if ($country['country_id'] == $borica_country_id) { ?>
                <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
              <?php if ($error_country) { ?>
              <span class="error"><?php echo $error_country; ?></span>
              <?php } ?></td>
          </tr>
          <tr>
            <td><?php echo $entry_state; ?></td>
            <td><input type="text" name="borica_state" value="<?php echo $borica_state; ?>" />
              <?php if ($error_state) { ?>
              <span class="error"><?php echo $error_state; ?></span>
              <?php } ?></td>
          </tr>
          <tr>
            <td><?php echo $entry_locality; ?></td>
            <td><input type="text" name="borica_locality" value="<?php echo $borica_locality; ?>" />
              <?php if ($error_locality) { ?>
              <span class="error"><?php echo $error_locality; ?></span>
              <?php } ?></td>
          </tr>
          <tr>
            <td><?php echo $entry_name; ?></td>
            <td><input type="text" name="borica_name" value="<?php echo $borica_name; ?>" />
              <?php if ($error_name) { ?>
              <span class="error"><?php echo $error_name; ?></span>
              <?php } ?></td>
          </tr>
          <tr>
            <td><?php echo $entry_email; ?></td>
            <td><input type="text" name="borica_email" value="<?php echo $borica_email; ?>" />
              <?php if ($error_email) { ?>
              <span class="error"><?php echo $error_email; ?></span>
              <?php } ?></td>
          </tr>
          <tr>
            <td><?php echo $entry_organization; ?></td>
            <td><input type="text" name="borica_organization" value="<?php echo $borica_organization; ?>" />
              <?php if ($error_organization) { ?>
              <span class="error"><?php echo $error_organization; ?></span>
              <?php } ?></td>
          </tr>
          <tr>
            <td><?php echo $entry_organization_unit; ?></td>
            <td><?php echo $borica_organization_unit; ?></td>
          </tr>
          <tr>
            <td><?php echo $entry_total; ?></td>
            <td><input type="text" name="borica_total" value="<?php echo $borica_total; ?>" /></td>
          </tr>
          <tr>
            <td><?php echo $entry_order_status; ?></td>
            <td><select name="borica_order_status_id">
                <?php foreach ($order_statuses as $order_status) { ?>
                <?php if ($order_status['order_status_id'] == $borica_order_status_id) { ?>
                <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select></td>
          </tr>
          <tr>
            <td><?php echo $entry_order_status_canceled; ?></td>
            <td><select name="borica_order_status_canceled_id">
                <?php foreach ($order_statuses as $order_status) { ?>
                <?php if ($order_status['order_status_id'] == $borica_order_status_canceled_id) { ?>
                <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select></td>
          </tr>
          <tr>
            <td><?php echo $entry_order_status_canceled_reversal; ?></td>
            <td><select name="borica_order_status_canceled_reversal_id">
                <?php foreach ($order_statuses as $order_status) { ?>
                <?php if ($order_status['order_status_id'] == $borica_order_status_canceled_reversal_id) { ?>
                <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select></td>
          </tr>
          <tr>
            <td><?php echo $entry_order_status_denied; ?></td>
            <td><select name="borica_order_status_denied_id">
                <?php foreach ($order_statuses as $order_status) { ?>
                <?php if ($order_status['order_status_id'] == $borica_order_status_denied_id) { ?>
                <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select></td>
          </tr>
          <tr>
            <td><?php echo $entry_order_status_failed; ?></td>
            <td><select name="borica_order_status_failed_id">
                <?php foreach ($order_statuses as $order_status) { ?>
                <?php if ($order_status['order_status_id'] == $borica_order_status_failed_id) { ?>
                <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select></td>
          </tr>
          <tr>
            <td><label for="borica_currency"><?php echo $entry_currency; ?></label></td>
            <td><select id="borica_currency" name="borica_currency">
                <?php foreach ($currencies as $currency) { ?>
                <?php if ($currency['code'] == $borica_currency) { ?>
                <option value="<?php echo $currency['code']; ?>" selected="selected"><?php echo $currency['title']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $currency['code']; ?>"><?php echo $currency['title']; ?></option>
                <?php } ?>
                <?php } ?>
              </select></td>
          </tr>
          <tr>
            <td><?php echo $entry_geo_zone; ?></td>
            <td><select name="borica_geo_zone_id">
                <option value="0"><?php echo $text_all_zones; ?></option>
                <?php foreach ($geo_zones as $geo_zone) { ?>
                <?php if ($geo_zone['geo_zone_id'] == $borica_geo_zone_id) { ?>
                <option value="<?php echo $geo_zone['geo_zone_id']; ?>" selected="selected"><?php echo $geo_zone['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select></td>
          </tr>
          <tr>
            <td><?php echo $entry_status; ?></td>
            <td><select name="borica_status">
                <?php if ($borica_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
          </tr>
          <tr>
            <td><?php echo $entry_sort_order; ?></td>
            <td><input type="text" name="borica_sort_order" value="<?php echo $borica_sort_order; ?>" size="3" /></td>
          </tr>
        </table>
      </form>
      <div><b><?php echo $text_save_before_generate; ?></b></div>
      <table class="form">
        <tr>
          <td><?php echo $entry_test_key_csr; ?></td>
          <td><a href="<?php echo $generate_test; ?>"<?php if ($text_generate_test_confirm) { ?> onclick="return confirm('<?php echo $text_generate_test_confirm; ?>');"<?php } ?>><?php echo $text_generate; ?></a>
            <?php if ($text_generate_test_confirm) { ?>
            <a href="<?php echo $download_test; ?>"><?php echo $text_download; ?></a>
            <?php echo $text_send_to_bank; ?>
              <?php if ($support) { ?>
                <a href="<?php echo $download_test_ppk; ?>"><?php echo $text_download_ppk; ?></a>
              <?php } ?>
            <?php } ?>
          </td>
        </tr>
        <tr>
          <td><?php echo $entry_real_key_csr; ?></td>
          <td><a href="<?php echo $generate_real; ?>"<?php if ($text_generate_real_confirm) { ?> onclick="return confirm('<?php echo $text_generate_real_confirm; ?>');"<?php } ?>><?php echo $text_generate; ?></a>
            <?php if ($text_generate_real_confirm) { ?>
            <a href="<?php echo $download_real; ?>"><?php echo $text_download; ?></a>
            <?php echo $text_send_to_bank; ?>
              <?php if ($support) { ?>
                <a href="<?php echo $download_real_ppk; ?>"><?php echo $text_download_ppk; ?></a>
              <?php } ?>
            <?php } ?>
          </td>
        </tr>
        <tr>
          <td><?php echo $entry_etlog_key_csr; ?></td>
          <td><a href="<?php echo $generate_etlog; ?>"<?php if ($text_generate_etlog_confirm) { ?> onclick="return confirm('<?php echo $text_generate_etlog_confirm; ?>');"<?php } ?>><?php echo $text_generate; ?></a>
            <?php if ($text_generate_etlog_confirm) { ?>
            <a href="<?php echo $download_etlog; ?>"><?php echo $text_download; ?></a>
            <?php echo $text_send_to_bank; ?>
              <?php if ($support) { ?>
                <a href="<?php echo $download_etlog_ppk; ?>"><?php echo $text_download_ppk; ?></a>
              <?php } ?>
            <?php } ?>
          </td>
        </tr>
        <tr>
          <td><?php echo $entry_return_url; ?></td>
          <td><b><?php echo $return_url; ?></b>
            <?php echo $text_copy_send_to_bank; ?></td>
        </tr>
      </table>
      <form action="<?php echo $action_p12; ?>" method="post" enctype="multipart/form-data" id="form_p12">
        <div><b><?php echo $text_copy_etlog_certificate; ?></b></div>
        <table class="form">
          <tr>
            <td><?php echo $entry_etlog_certificate; ?></td>
            <td><textarea name="borica_etlog_certificate" cols="40" rows="5"><?php echo $borica_etlog_certificate; ?></textarea></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><a onclick="$('#form_p12').submit();" class="button"><span><?php echo $button_get_p12; ?></span></a>
              <br />
              <?php echo $text_import_into_browser; ?></td>
          </tr>
        </table>
      </form>
      <form action="<?php echo $action_certificates; ?>" method="post" enctype="multipart/form-data" id="form_certificates">
        <div><b><?php echo $text_copy_certificates; ?></b></div>
        <table class="form">
          <tr>
            <td><?php echo $entry_test_certificate; ?></td>
            <td><textarea name="borica_test_certificate" cols="40" rows="5"><?php echo $borica_test_certificate; ?></textarea></td>
          </tr>
          <tr>
            <td><?php echo $entry_real_certificate; ?></td>
            <td><textarea name="borica_real_certificate" cols="40" rows="5"><?php echo $borica_real_certificate; ?></textarea></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><a onclick="$('#form_certificates').submit();" class="button"><span><?php echo $button_save_certificates; ?></span></a></td>
          </tr>
        </table>
      </form>
    </div>
  </div>
</div>
<?php echo $footer; ?>
<!-- eBORICA payment gateway by Extensa Web Development - www.extensadev.com -->