<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/payment.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a></div>
    </div>
    <div class="content">
	 <div class="vtabs">
	             <a href="#tab-general" class="tab"><?php echo $tab_general; ?></a>	
				 <?php
				    foreach($xpayment['name'] as $no_of_tab=>$names){
					  if(!is_array($names))$names=array();
					  if(!isset($names[$language_id]) || !$names[$language_id])$names[$language_id]='Untitled Method '.$no_of_tab;
					  echo '<a class="tab tab'.$no_of_tab.'" href="#payment-'.$no_of_tab.'"><span class="delete">x</span><strong>'.html_entity_decode($names[$language_id]).'</strong></a>';
					}
				 ?>	
				 <a href="#" class="add-new"><span>+</span>&nbsp;Add New Payment</a>			
      </div>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
	  
	  <div id="tab-general" class="vtabs-content">
          <table class="form">
            <tr>
              <td><?php echo $tab_general; ?> <a class="help" tips="<?php echo $tip_status_global;?>">?</a></td>
              <td><select name="xpayment_status">
                  <?php if ($xpayment_status) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
            </tr>
		   <tr>
            <td><?php echo $entry_sort_order; ?> <a class="help" tips="<?php echo $tip_sorting_global;?>">?</a></td>
            <td><input type="text" name="xpayment_sort_order" value="<?php echo $xpayment_sort_order; ?>" size="1" /></td>
          </tr>
          <tr>
              <td><?php echo $text_debug; ?> <a class="help" tips="<?php echo $tip_debug;?>">?</a></td>
              <td><select name="xpayment_debug">
                  <?php if ($xpayment_debug) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
            </tr>
          </table>
        </div>
        <?php echo $form_data;?>
	   
      </form>
    </div>
  </div>
</div>
<style type="text/css">
  .add-new span{ border: medium none;
    color: #000000;
    display: inline;
    font-size: 24px;
    height: 6px;
    line-height: 5px;
    margin-right: 0;
    padding-bottom: 0;
    padding-right: 0;
    width: 25px;}

.tab span.delete {
    border: medium none;
    color: #000000;
    cursor: pointer;
    font-size: 17px;
    height: 6px;
    line-height: 5px;
    margin-right: 0;
    padding-bottom: 0;
    padding-left: 1px;
    padding-right: 0;
    width: 10px;
	display:none;
}
.tab strong{font-weight:bold;}
.tab:hover span{ display:inline;}
.tab span.delete:hover {
    color: #FF0000;
}
.vtabs{ min-height:530px;}	
tr.category, tr.postal-option{ display:none;}
.any-class {
    display: block;
    font-weight: bold;
    margin-bottom: 10px;
}
.scrollbox-wrapper{ display:none;}
a.help{position:relative; cursor: help; display:inline;}
div.quick-toopltip{ position:absolute; left:10px; top:-10px; background-color: #F5F5B5;border: 1px solid #DECA7E;color: #303030;font-size: 12px;line-height: 18px;padding: 10px 13px;text-align: left;z-index: 2; min-width:180px;font-weight: normal;}
.method-htabs > a {
    background: none repeat scroll 0 0 #F7F2E3;
}
.tab img{ max-width:100%;}
</style>
<script type="text/javascript"><!--
var tmp='<div id="__ID__" class="vtabs-content payment">'
          +'<div class="htabs">'
            <?php foreach ($languages as $language) { ?>
            +'<a href="#language<?php echo $language['language_id']; ?>___INDEX__"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a>'
            <?php } ?>
          +'</div>'
		  <?php $inc=0;foreach ($languages as $language) { $lang_cls=($inc==0)?'':'-lang'; $inc++;  ?>
          +'<div id="language<?php echo $language['language_id']; ?>___INDEX__">'
          +'<table class="form">'
		  +'<tr>'
            +'<td><?php echo $entry_name; ?></td>'
            +'<td><input type="text" class="method-name<?php echo $lang_cls?>" size="45" name="xpayment[name][__INDEX__][<?php echo $language['language_id']; ?>]" value="" /></td>'
          +'</tr>'
		   +'<tr>'
            +'<td><?php echo $entry_desc; ?> <a class="help" tips="<?php echo addslashes($tip_desc)?>">?</a></td>'
            +'<td><input type="text" size="45" name="xpayment[desc][__INDEX__][<?php echo $language['language_id']; ?>]" value="" /></td>'
          +'</tr>'
          +'<tr>'
            +'<td><?php echo $entry_instruction; ?> <a class="help" tips="<?php echo addslashes($tip_instruction)?>">?</a></td>'
            +'<td><textarea rows="12" cols="90" name="xpayment[instruction][__INDEX__][<?php echo $language['language_id']; ?>]"></textarea></td>'
          +'</tr>'
          +'<tr>'
            +'<td><?php echo $text_instruction_email; ?> <a class="help" tips="<?php echo addslashes($tip_email_instruction)?>">?</a></td>'
            +'<td><textarea rows="12" cols="90" name="xpayment[email_instruction][__INDEX__][<?php echo $language['language_id']; ?>]"></textarea></td>'
          +'</tr>'
		  +'</table>'
		  +'</div>'
		  <?php } ?>
		  +'<table class="form">'
		     +'<tr>'
                   +'<td><?php echo $text_inc_email; ?></td>'
                   +'<td><input type="checkbox" name="xpayment[inc_email][__INDEX__]" value="1" /></td>'
                +'</tr>'
               +'<tr>'
                   +'<td><?php echo $text_inc_order; ?></td>'
                   +'<td><input type="checkbox" name="xpayment[inc_order][__INDEX__]" value="1" /></td>'
                +'</tr>'  
		     +'<tr>'
                  +'<td><?php echo $entry_order_status; ?> <a class="help" tips="<?php echo addslashes($tip_order_status);?>">?</a></td>'
                  +'<td><select name="xpayment[order_status_id][__INDEX__]">'
                  <?php foreach ($order_statuses as $order_status) { ?>
                  +'<option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>'
                  <?php } ?>
                  +'</select></td>'
                +'</tr>'
              +'<tr>'
                +'<td><?php echo $entry_callback?> <a class="help" tips="<?php echo addslashes($tip_callback);?>">?</a></td>'
                +'<td><input size="45"  type="text" name="xpayment[callback][__INDEX__]" value="" />'
                +'</td>'
               +'</tr>'  
                +'<tr>'
                +'<td><?php echo $entry_redirect?> <a class="help" tips="<?php echo addslashes($tip_redirect);?>">?</a></td>'
                +'<td><input size="45"  type="text" name="xpayment[redirect][__INDEX__]" value="" />'
                +'</td>'
               +'</tr>'  
                +'<tr>'
                +'<td><?php echo $entry_redirect_type?> <a class="help" tips="<?php echo addslashes($tip_redirect_data);?>">?</a></td>'
                +'<td>'
                 +'<label><input type="radio" checked name="xpayment[redirect_type][__INDEX__]" value="post" />&nbsp;<?php echo $entry_redirect_post;?></label>'
                 +'<label><input type="radio" name="xpayment[redirect_type][__INDEX__]" value="get" />&nbsp;<?php echo $entry_redirect_get;?></label>'
                +'</td>'
               +'</tr>'  
                +'<tr>'
                +'<td><?php echo $entry_success?> <a class="help" tips="<?php echo addslashes($tip_success);?>">?</a></td>'
                +'<td><input size="45"  type="text" name="xpayment[success][__INDEX__]" value="" />'
                +'</td>'
               +'</tr>'    
		    +'<tr>'
                +'<td><?php echo $entry_store; ?> <a class="help" tips="<?php echo addslashes($tip_store);?>">?</a></td>' 
                 +'<td>'
		 +'<label class="any-class"><input checked type="checkbox" name="xpayment[store_all][__INDEX__]" class="choose-any" value="1" />&nbsp;<?php echo $text_any; ?></label>'
		 +'<div class="scrollbox-wrapper">'
		 +'<div class="scrollbox">'
                 <?php 
                    $class = 'even';
                    foreach ($stores as $store) {
                        $class = ($class == 'even' ? 'odd' : 'even');
                     ?>
		   +'<div class="<?php echo $class; ?>">'
                   +'<input type="checkbox" name="xpayment[store][__INDEX__][]" value="<?php echo $store['store_id']; ?>" />&nbsp;<?php echo $store['name']; ?>'
		   +'</div>'
                 <?php } ?>
                  +'</div>'
		  +'<a onclick="$(this).parent().find(\':checkbox\').attr(\'checked\', true);"><?php echo $text_select_all; ?></a>'
	          +'/ <a onclick="$(this).parent().find(\':checkbox\').attr(\'checked\', false);"><?php echo $text_unselect_all; ?></a>'
	          +'</div></td>'
                +'</tr>'
                +'<tr>'
                 +'<td><?php echo $entry_geo_zone; ?> <a class="help" tips="<?php echo addslashes($tip_geo);?>">?</a></td>' 
                 +'<td>'
		 +'<label class="any-class"><input checked type="checkbox" name="xpayment[geo_zone_all][__INDEX__]" class="choose-any" value="1" />&nbsp;<?php echo $text_any; ?></label>'
		 +'<div class="scrollbox-wrapper">'
		 +'<div class="scrollbox">'
                  <?php 
                    $class = 'even';
                    foreach ($geo_zones as $geo_zone) {
                    $class = ($class == 'even' ? 'odd' : 'even');
		   ?>
		  +'<div class="<?php echo $class; ?>">'
                  +'<input type="checkbox" name="xpayment[geo_zone_id][__INDEX__][]" value="<?php echo $geo_zone['geo_zone_id']; ?>" />&nbsp;<?php echo $geo_zone['name']; ?>'
		  +'</div>'
                 <?php } ?>
                 +'</div>'
	         +'<a onclick="$(this).parent().find(\':checkbox\').attr(\'checked\', true);"><?php echo $text_select_all; ?></a>'
		 +'/ <a onclick="$(this).parent().find(\':checkbox\').attr(\'checked\', false);"><?php echo $text_unselect_all; ?></a>'
		 +'</div></td>'
                +'</tr>'
                +'<tr>'
                 +'<td><?php echo $entry_manufacturer; ?> <a class="help" tips="<?php echo addslashes($tip_manufacturer);?>">?</a></td>' 
                 +'<td>'
                 +'<label class="any-class"><input checked type="checkbox" name="xpayment[manufacturer_all][__INDEX__]" class="choose-any" value="1" />&nbsp;<?php echo $text_any; ?></label>'
                 +'<div class="scrollbox-wrapper">'
                 +'<div class="scrollbox">'
                 <?php 
                    $class = 'even';
                    foreach ($manufacturers as $manufacturer) {
                     $class = ($class == 'even' ? 'odd' : 'even');
                     ?>
		 +'<div class="<?php echo $class; ?>">'
                 +'<input type="checkbox" name="xpayment[manufacturer][__INDEX__][]" value="<?php echo $manufacturer['manufacturer_id']; ?>" />&nbsp;<?php echo addslashes($manufacturer['name']); ?>'
		 +'</div>'
                <?php } ?>
                +'</div>'
	        +'<a onclick="$(this).parent().find(\':checkbox\').attr(\'checked\', true);"><?php echo $text_select_all; ?></a>'
	        +'/ <a onclick="$(this).parent().find(\':checkbox\').attr(\'checked\', false);"><?php echo $text_unselect_all; ?></a>'
		+'</div></td>'
               +'</tr>'
               +'<tr>'
                 +'<td><?php echo $entry_customer_group; ?> <a class="help" tips="<?php echo addslashes($tip_customer_group);?>">?</a></td>'
                 +'<td>'
                 +'<label class="any-class"><input checked type="checkbox" name="xpayment[customer_group_all][__INDEX__]" class="choose-any" value="1" />&nbsp;<?php echo $text_any; ?></label>'
                 +'<div class="scrollbox-wrapper">'
                 +'<div class="scrollbox">'
                 <?php 
                    $class = 'even';
                    foreach ($customer_groups as $customer_group) {
                      $class = ($class == 'even' ? 'odd' : 'even');
		 ?>
		 +'<div class="<?php echo $class; ?>">'
                 +'<input type="checkbox" name="xpayment[customer_group][__INDEX__][]" value="<?php echo $customer_group['customer_group_id']?>" />&nbsp;<?php echo $customer_group['name']?>'        
                 +'</div>'
                 <?php } ?>
                 +'</div>'
		 +'<a onclick="$(this).parent().find(\':checkbox\').attr(\'checked\', true);"><?php echo $text_select_all; ?></a>'
	         +'/ <a onclick="$(this).parent().find(\':checkbox\').attr(\'checked\', false);"><?php echo $text_unselect_all; ?></a>'
	         +'</div></td>'
               +'</tr>'
                +'<tr>'
                 +'<td><?php echo $text_zip_postal?> <a class="help" tips="<?php echo addslashes($tip_zip);?>">?</a></td>'
                 +'<td>'
	             +'<label class="any-class"><input checked type="checkbox" name="xpayment[postal_all][__INDEX__]" class="choose-zip" value="1" />&nbsp;<?php echo $text_any; ?></label>'
                  +'</td>'
               +'</tr>'
               +'<tr class="postal-option">'
                +'<td><?php echo $text_enter_zip?></td>'
                +'<td><textarea name="xpayment[postal][__INDEX__]" rows="8" cols="70" /></textarea></td>'
               +'</tr>'
               +'<tr class="postal-option">'
                +'<td><?php echo $text_zip_rule?></td>'
                +'<td><select name="xpayment[postal_rule][__INDEX__]">'
                    +'<option value="inclusive"><?php echo $text_zip_rule_inclusive?></option>'
                    +'<option value="exclusive"><?php echo $text_zip_rule_exclusive?></option>'
                  +'</select></td>'
               +'</tr>'
		    +'<tr>'
              +'<td><?php echo $text_category; ?> <a width="500" class="help" tips="<?php echo addslashes($tip_category);?>">?</a></td>'
              +'<td><select class="category-selection category" name="xpayment[category][__INDEX__]">'
                  +'<option value="1"><?php echo $text_category_any; ?></option>'
                  +'<option value="2"><?php echo $text_category_all; ?></option>'
		          +'<option value="3"><?php echo $text_category_least; ?></option>'
		          +'<option value="4"><?php echo $text_category_exact; ?></option>'
		          +'<option value="5"><?php echo $text_category_except; ?></option>'
                +'</select></td>'
            +'</tr>'
			+'<tr class="category">'
              +'<td><?php echo $entry_category; ?></td>'
              +'<td><input type="text" name="category" value="" /></td>'
            +'</tr>'
            +'<tr class="category">'
              +'<td>&nbsp;</td>'
              +'<td><div class="scrollbox product-category">'
                +'</div></td>'
            +'</tr>'
		   +'<tr>'
            +'<td><?php echo $entry_order_total; ?>  <a class="help" tips="<?php echo addslashes($tip_total);?>">?</a></td>'
            +'<td><input size="15" type="text" name="xpayment[order_total_start][__INDEX__]" value="" /> &nbsp;<?php echo $entry_to?>&nbsp; <input size="15" type="text" name="xpayment[order_total_end][__INDEX__]" value="" />&nbsp;&nbsp;[<?php echo $entry_order_hints?>]</td>'
          +'</tr>'
		  +'<tr>'
          +'<td><?php echo $entry_order_weight; ?>  <a class="help" tips="<?php echo addslashes($tip_weight);?>">?</a></td>'
            +'<td><input size="15" type="text" name="xpayment[weight_start][__INDEX__]" value="" /> &nbsp;<?php echo $entry_to?>&nbsp; <input size="15" type="text" name="xpayment[weight_end][__INDEX__]" value="" />&nbsp;&nbsp;[<?php echo $entry_order_hints?>]</td>'
          +'</tr>'
          +'<tr>'
            +'<td><?php echo $entry_sort_order; ?> <a class="help" tips="<?php echo addslashes($tip_sorting_own);?>">?</a></td>'
            +'<td><input type="text" name="xpayment[sort_order][__INDEX__]" value="" size="1" /></td>'
          +'</tr>'
		  +'<tr>'
              +'<td><?php echo $entry_status; ?> <a class="help" tips="<?php echo addslashes($tip_status_own);?>">?</a></td>'
              +'<td><select name="xpayment[status][__INDEX__]">'
                  +'<option value="1" selected="selected"><?php echo $text_enabled; ?></option>'
                  +'<option value="0"><?php echo $text_disabled; ?></option>'
                +'</select></td>'
            +'</tr>'
        +'</table>'
        +'</div>';


$('.add-new').live('click',function(e) {

		  e.preventDefault();
		  $this=$(this);

		  var no_of_tab=$('#form').find('div.payment').length;
		  no_of_tab=parseInt(no_of_tab)+1;
		
		  //finding qnique id
		  while($('#payment-'+no_of_tab).length!=0)
		   {
		     no_of_tab++;
		   }

		  var tab_html=tmp;
		  tab_html=tab_html.replace('__ID__','payment-'+no_of_tab);
		  tab_html=tab_html.replace(/__INDEX__/g, no_of_tab);
		  $('#form').append(tab_html);
		  
		  $('<a class="tab tab'+no_of_tab+'" href="#payment-'+no_of_tab+'"><span class="delete">x</span><strong>Untitled Payment '+no_of_tab+'</strong></a>').insertBefore($this);
		  
		  $('#form #payment-'+no_of_tab+' input.method-name').keyup(function(){
		      var tabId=$(this).closest('div.payment').attr('id');
			  tabId=tabId.replace('payment-','');
			  tabId=parseInt(tabId);
			  var method_name=$(this).val();
			  if(method_name=='')method_name='Untitled Method '+tabId;
			  $('a.tab'+tabId+' strong').html(method_name);
		   });
		   
		    $('a.tab'+no_of_tab+' span.delete').click(function(){
			  if(confirm('Are you sure to delete this method?')){
				  var tabId=$(this).parent().attr('href');
				  tabId=tabId.replace('#payment-','');
				  tabId=parseInt(tabId);
				  $('a.tab'+tabId).remove();
				  $('#payment-'+tabId).remove();
				  $('.vtabs a.tab').first().click();
			  }
			  return false;
		   });
		   
		   $('#form #payment-'+no_of_tab+' input[name=\'category\']').autocomplete({
			delay: 500,
			source: function(request, response) {
				$.ajax({
					url: 'index.php?route=payment/xpayment/get_categories&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
					dataType: 'json',
					success: function(json) {		
						response($.map(json, function(item) {
							return {
								label: item.name,
								value: item.category_id
							}
						}));
					}
				});
			}, 
			select: function(event, ui) {
				$('#form #payment-'+no_of_tab+' .product-category' + ui.item.value).remove();
				
				$('#form #payment-'+no_of_tab+' .product-category').append('<div class="product-category' + ui.item.value + '">' + ui.item.label + '<img src="view/image/delete.png" alt="" /><input type="hidden" name="xpayment[product_category]['+no_of_tab+'][]" value="' + ui.item.value + '" /></div>');
						
				return false;
			},
			focus: function(event, ui) {
			  return false;
		   }
		  });
		  
		  
		  $('.vtabs a.tab').each(function(){
		     $(this).unbind('click');
		  });
		  
		  $('.vtabs a.tab').tabs();
		  $('#form #payment-'+no_of_tab+' .htabs a').tabs();

    });

$(document).ready(function () {		
	 $('.vtabs a.tab').tabs(); 
	 $('div.payment').each(function(){
	   $(this).find('.htabs a').tabs();
	 });
	 
	 $('a.tab span.delete').click(function(){
				  if(confirm('Are you sure to delete this method?')){
					  var tabId=$(this).parent().attr('href');
					  tabId=tabId.replace('#payment-','');
					  tabId=parseInt(tabId);
					  $('a.tab'+tabId).remove();
					  $('#payment-'+tabId).remove();
					  $('.vtabs a.tab').first().click();
				}
		   return false;			  
	   });
	   
     $('#form input.method-name').keyup(function(){
		  var tabId=$(this).closest('div.payment').attr('id');
		  tabId=tabId.replace('payment-','');
		  tabId=parseInt(tabId);
		  var method_name=$(this).val();
		  if(method_name=='')method_name='Untitled Payment '+tabId;
		  $('a.tab'+tabId+' strong').html(method_name);
	   });
	   
	   
	   $('input[name=\'category\']').autocomplete({
			delay: 500,
			source: function(request, response) {
				$.ajax({
					url: 'index.php?route=payment/xpayment/get_categories&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
					dataType: 'json',
					success: function(json) {		
						response($.map(json, function(item) {
							return {
								label: item.name,
								value: item.category_id
							}
						}));
					}
				});
			}, 
			select: function(event, ui) {
			    var no_of_tab=$(this).closest('div.payment').attr('id');
				no_of_tab=no_of_tab.replace('payment-','');
				no_of_tab=parseInt(no_of_tab);
				$('#form #payment-'+no_of_tab+' .product-category' + ui.item.value).remove();
				
				$('#form #payment-'+no_of_tab+' .product-category').append('<div class="product-category' + ui.item.value + '">' + ui.item.label + '<img src="view/image/delete.png" alt="" /><input type="hidden" name="xpayment[product_category]['+no_of_tab+'][]" value="' + ui.item.value + '" /></div>');		
				return false;
			},
			focus: function(event, ui) {
			  return false;
		   }
		});
		
		$('.product-category div img').live('click', function() {
			var no_of_tab=$(this).closest('div.payment').attr('id');
				no_of_tab=no_of_tab.replace('payment-','');
				no_of_tab=parseInt(no_of_tab);
			$(this).parent().remove();	
		});
		
		$('select.category-selection').live('change', function() {
		    var no_of_tab=$(this).closest('div.payment').attr('id');
				no_of_tab=no_of_tab.replace('payment-','');
				no_of_tab=parseInt(no_of_tab);
			 if($(this).val()=='1'){
			    $('#form #payment-'+no_of_tab+' tr.category').css('display', 'none');
			 }else{
			   $('#form #payment-'+no_of_tab+' tr.category').css('display', 'table-row');
			 }
		});
		
		$('.choose-any').live('click', function() {
		
		     if($(this).prop('checked')){
			     $(this).parent().parent().find('div.scrollbox-wrapper').slideUp();  
			 }else{
				$(this).parent().parent().find('div.scrollbox-wrapper').slideDown();
			}
		});
		
		$('.choose-zip').live('click', function() {
		          var no_of_tab=$(this).closest('div.payment').attr('id');
				  no_of_tab=no_of_tab.replace('payment-','');
				  no_of_tab=parseInt(no_of_tab);
		        if($(this).prop('checked')){
			        $('#form #payment-'+no_of_tab+' tr.postal-option').css('display', 'none');
			     }else{
			        $('#form #payment-'+no_of_tab+' tr.postal-option').css('display', 'table-row');
			    }
		      });
		
		
		$('a.help').live('click',function(e){
	      e.preventDefault();
        });
        
        $('a.help').live('hover',function(e){
            var width=($(this).attr('width'))?'style="width:'+$(this).attr('width')+'px"':'';
            $(this).append('<div '+width+' class="quick-toopltip">'+$(this).attr('tips')+'</div>');
         }).live('mouseleave',function(){
            $(this).find('div.quick-toopltip').remove();
         });
	    
 });
 
 
 	 
//--></script> 
<?php echo $footer; ?> 