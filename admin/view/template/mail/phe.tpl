<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><?php echo $subject; ?></title>
</head>
<body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;">
<table style="width:700px;margin-left:auto;margin-right:auto;text-align:left;border-collapse:collapse;" width="700px;">
<tr>
  <td id="t-header" style="text-align:center;verticle-align:middle;height:100px;background:<?php echo $background; ?>" height="100px;">
	  <a href="<?php echo $url; ?>"><img src="<?php echo $logo; ?>" alt="<?php echo $store_name; ?>" title="<?php echo $store_name; ?>" /></a>
  </td>
</tr>
<tr>
  <td id="t-body" style="padding:20px;background:<?php echo $body; ?>">
	  <div id="t-heading" style="font-size:16px;font-weight:bold;border-bottom:1px solid #aaaaaa;color:<?php echo $heading; ?>"><?php echo $subject; ?></div>
	  <?php echo $message; ?>
	</div>
  </td>
</tr>
<tr>
  <td style="background:#aaaaaa;padding:0px;height:1px;display:block;width:100%;" height="1px" width="100%"></td>
</tr>
<tr>
  <td style="background:#b0b0b0;padding:0px;height:1px;display:block;width:100%;" height="1px" width="100%"></td>
</tr>
<tr>
  <td style="background:#c1c1c1;padding:0px;height:1px;display:block;width:100%;" height="1px" width="100%"></td>
</tr>
<tr>
  <td style="background:#d2d2d2;padding:0px;height:1px;display:block;width:100%;" height="1px" width="100%"></td>
</tr>
<tr>
  <td style="background:#d9d9d9;padding:0px;height:1px;display:block;width:100%;" height="1px" width="100%"></td>
</tr>
<tr>
  <td style="background:#e8e8e8;padding:0px;height:1px;display:block;width:100%;" height="1px" width="100%"></td>
</tr>
<tr>
  <td style="background:#efefef;padding:0px;height:1px;display:block;width:100%;" height="1px" width="100%"></td>
</tr>
<tr>
  <td style="background:#f7f7f7;padding:0px;height:1px;display:block;width:100%;" height="1px" width="100%"></td>
</tr>
<tr>
  <td style="background:#fefefe;padding:0px;height:1px;display:block;width:100%;" height="1px" width="100%"></td>
</tr>
<tr>
  <td style="background:#ffffff;padding:0px;height:1px;display:block;width:100%;" height="1px" width="100%"></td>
</tr>
</table>
</body>
</html>
