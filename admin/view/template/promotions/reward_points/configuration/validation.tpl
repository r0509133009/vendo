<?php echo $header; ?>
<div id="content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
    <?php if ($error_warning) { ?>
    <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
    <div class="attention" style="display: none"></div>
    <div class="success" style="display: none"></div>
    <div class="warning" style="display: none"></div>
    <div class="box">
        <div class="heading">
            <h1><img src="view/image/setting.png" alt="" /> <?php echo $heading_title; ?></h1>
        </div>
        <div class="content">

            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                <table class="form">
                    <tr>
                        <td colspan="2">Sorry to bothering you, but please fill out the information below:</td>
                    </tr>
                    <tr>
                        <td>Order ID:</td>
                        <td>
                            <input type="text" name="order_id"/><br><br><i>(Order id on market opencart.com when purchased this module)</i>
                        </td>
                    </tr>
                    <tr>
                        <td>Email purchased:</td>
                        <td>
                            <input type="text" name="email" style="width: 260px;"/>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <div class="buttons">
                                <a href="javascript:void(1)" class="button validate" style="background: green;">Validate</a>
                                <a href="javascript:void(2)" class="button skip-validate">Skip</a> <i style="color:#ccc">You can skip this step, but you still need fill and validate your site in future</i>
                            </div>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    <!--
    $(document).ready(function(){
        $("#form .validate").click(function(){
            $(".attention").hide();
            $(".warning").hide();
            $(".success").hide();
            var order_id = $.trim($("input[name=order_id]").val());
            var email = $.trim($("input[name=email]").val());

            if(order_id == "" || email == ""){
                $(".attention").show().html("Enter your <b>order id</b> and <b>email</b> was use for purchased module");
                return false;
            }
            $(".attention").show().html("Validating. Please wait ...");
            $.ajax({
                type: "POST",
                url: "<?php echo $url.'&token='.$token?>",
                data: "validate_order=true&order_id="+order_id+"&email="+email,
                success: function(data){
                    var json = $.parseJSON(data);

                    if(!json.VALIDATE_ORDER_ID){
                        $(".attention").hide();
                        $(".success").hide();
                        if(json.NOTE != ""){
                            $(".warning").show().html("You have using module for multi domain, pls contact me to unlock this. Thanks you.");
                        }else{
                            $(".warning").show().html("Order ID or Email using for purchased on market is not valid. Pls try again.");
                        }

                        setTimeout(function(){
                            $(".warning").hide()
                        }, 4200);
                    }else{
                        $(".attention").hide();
                        $(".warning").hide();
                        $(".success").show().html("Checkin completed! Wait to reload");
                        setTimeout(function(){
                            $(".success").hide()
                        }, 4200);
                        setTimeout(function(){
                            location.reload();
                        }, 1000);
                    }
                }
            });
        });
        $("#form .skip-validate").click(function(){
            $(".attention").show().html("Skipping. Please wait ...");
            $.ajax({
                type: "POST",
                url: "<?php echo $url.'&token='.$token?>",
                data: "validate_skip=true",
                success: function(data){
                    var json = $.parseJSON(data);
                    if(typeof json.error == 'undefined'){
                        $(".attention").hide();
                        $(".success").show().html("Skip completed! Wait to reload");

                        setTimeout(function(){
                            location.reload();
                        }, 1000);
                    }else{
                        $(".attention").show().html("Skip not complete! Please validate your module.");
                    }
                }
            });
        });
    });
    //-->
</script>
<?php echo $footer; ?>