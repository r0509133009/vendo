<?php echo $header; ?>
<div id="content">
<div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
</div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
    <?php if ($warning) { ?>
    <div class="warning"><?php echo $warning; ?></div>
    <?php $this->session->data['warning'] = ''?>
    <?php } ?>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?></div>
<?php $this->session->data['success'] = ''?>
<?php } ?>
<div class="box">
<div class="heading">
    <h1><img src="view/image/setting.png" alt="" /> <?php echo $heading_title; ?></h1>
    <div class="buttons">
	    <a onclick="$('#form').submit();" class="button"><?php echo $this->language->get('button_save_configuration'); ?></a>
    </div>
</div>
<div class="content">
<div id="tabs" class="htabs">
	<a href="#tab-general"><?php echo $this->language->get('tab_general'); ?></a>
	<a href="#tab-display"><?php echo $this->language->get('tab_display'); ?></a>
	<a href="#tab-module-status" style="<?php echo (!$extensions['status'] ? 'color:red' : '')?>"><?php echo $this->language->get('Module Status'); ?></a>
</div>
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
	<div id="tab-general">
	    <table class="form">
	        <tr>
	            <td><?php echo $this->language->get('text_opt_enabled_module'); ?></td>
	            <td>
		            <select name="rwp_enabled_module" id="rwp_enabled_module">
			            <option value="0" <?php echo ($rwp_enabled_module == "0" ? 'selected="selected"' : '')?>>No</option>
			            <option value="1" <?php echo ($rwp_enabled_module == "1" ? 'selected="selected"' : '')?>>Yes</option>
		            </select>
	            </td>
	        </tr>
		    <tr>
			    <td><?php echo $this->language->get('text_point_exchange_rate')?></td>
			    <td>
				    <input type="text" name="currency_exchange_rate" value="<?php echo $currency_exchange_rate?>"/>
				    <p class="note"><span><?php echo $this->language->get('text_tip_exchange_rate')?></span></p>
			    </td>
		    </tr>
            <!-- DISPATCH_EVENT:CONFIGURATION_AFTER_RENDER_GENERAL_FIELDS -->
	    </table>
	</div>
	<div id="tab-display">
		<table class="form">
			<tr>
                <td><?php echo $this->language->get('text_unit_name_point'); ?></td>
				<td>
                    <div id="tabs-point" class="htabs">
                        <?php foreach ($languages as $language) { ?>
                            <a data-value="<?php echo $language['language_id']; ?>" id="rwp_language_point" href="#language<?php echo $language['language_id']; ?>"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a>
                        <?php } ?>
                    </div>
                    <?php foreach ($languages as $language) { ?>
                        <?php $var = "text_points_".$language['code']; ?>
                        <div id="language<?php echo $language['language_id']; ?>">
                            <input type="text" name="text_points_<?php echo $language['code']; ?>" value="<?php echo ${$var}?>"/>
                        </div>
                    <?php } ?>
                </td>
			</tr>
		</table>
	</div>
    <div id="tab-module-status">
        <i style="margin: 0 10px;">Please go to <b style="background: rgb(236, 236, 236);padding: 4px;color: dimgrey;font-weight: normal;">Extension -> Order Total</b> to install or enable both.</i>
        <table class="form">
            <tr>
                <td>Allow Earn Points</td>
                <td><div style="background: <?php echo ($extensions['earn_point']['status'] ? 'green' : 'red')?>; width: 15px;height: 15px"></div></td>
                <td>Sort order: <b><?php echo $extensions['earn_point']['sort_order']?></b></td>
            </tr>
            <tr>
                <td>Allow Redeem Points</td>
                <td><div style="background: <?php echo ($extensions['redeem_point']['status'] ? 'green' : 'red')?>; width: 15px;height: 15px"></div></td>
                <td>Sort order: <b><?php echo $extensions['redeem_point']['sort_order']?></b> (<i><u>Note:</u>Value this sort order must less than sort order of Total +1 unit</i>)</td>
            </tr>
        </table>
    </div>
</form>
</div>
</div>
</div>

<script type="text/javascript"><!--
    $('#tabs a').tabs();
    $('#tabs-point a').tabs();
    //--></script>
<?php echo $footer; ?>