<?php echo $header; ?>
<div id="content">
	<div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
  	</div>
    
    <?php if ($error_warning) { ?>
  		<div class="warning"><?php echo $error_warning; ?></div>
  	<?php } ?>
    <div class="box">
    	<div class="heading">
        	<h1><?php echo $heading_title; ?></h1>
            <div class="buttons">
            	<a onclick="$('#form-campaign').submit();" class="button"><?php echo $button_save; ?></a>
            	<a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a>
            </div>
        </div>
        <div class="content">
        	<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-campaign" class="form-horizontal">
        	<table class="form">
            	<tr>
                	<td><span class="required">*</span> <?php echo $text_name; ?></td>
                    <td>
                    	<input type="text" size="100" name="campaign_name" value="<?php echo $campaign_name; ?>" placeholder="<?php echo $text_campaign_name; ?>" id="input-campaign-name" class="form-control" />
                  			<?php if ($error_campaign_name) { ?>
                  				<div class="error"><?php echo $text_error_not_none; ?></div>
                  			<?php } ?>
                    </td>
                </tr>
                <tr>
                	<td><span class="required">*</span> <?php echo $text_description; ?></td>
                    <td>
                    	<textarea name="campaign_des" rows="2" cols="100" placeholder="<?php echo $text_campaign_des; ?>"><?php echo $campaign_des; ?></textarea>
                        	<?php if ($error_campaign_des) { ?>
                  				<div class="error"><?php echo $text_error_not_none; ?></div>
                  			<?php } ?>
                    </td>
               	</tr>
                <tr>
                	<td><?php echo $text_category; ?></td>
                    <td>
                    	<select name="campaign_cateid" id="input-campaign-cateid" class="form-control">
                            <option value=""><?php echo $text_select_category; ?></option>
                            
                            <?php foreach ($categories as $category) { ?>
                            <?php if ($category['category_id'] == $campaign_cateid) { ?>
                            <option value="<?php echo $category['category_id']; ?>" selected="selected"> <?php echo $category['name']; ?> </option>
                            <?php } else { ?>
                            <option value="<?php echo $category['category_id']; ?>"><?php echo $category['name']; ?></option>
                            <?php } ?>
                            <?php } ?>
                   		</select>
                    </td>
               	</tr>
                <tr>
                	<td><span class="required">*</span> <?php echo $text_country; ?></td>
                    <td>
                    	<select name="campaign_geo" id="input-campaign-geo" class="form-control">
                            <option value=""><?php echo $text_select_country; ?></option>
                            <?php foreach ($countries as $country) { ?>
                            <?php if ($country['iso_code_2'] == $campaign_geo) { ?>
                            <option value="<?php echo $country['iso_code_2']; ?>" selected="selected"> <?php echo $country['name']; ?> </option>
                            <?php } else { ?>
                            <option value="<?php echo $country['iso_code_2']; ?>"><?php echo $country['name']; ?></option>
                            <?php } ?>
                            <?php } ?>
                      	</select>
                      	<?php if ($error_campaign_geo) { ?>
                  			<div class="error"><?php echo $text_error_not_none; ?></div>
                  		<?php } ?>
                    </td>
               	</tr>
                <tr>
                	<td><?php echo $text_status; ?></td>
                    <td>
                    	<select name="campaign_status" id="input-campaign-status" class="form-control">
                            <?php if ($campaign_status) { ?>
                            <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                            <option value="0"><?php echo $text_disabled; ?></option>
                            <?php } else { ?>
                            <option value="1"><?php echo $text_enabled; ?></option>
                            <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                            <?php } ?>
                     	</select>
                    </td>
               	</tr>
            </table>
            </form>
        </div>
    </div>
</div>
<?php echo $footer; ?>