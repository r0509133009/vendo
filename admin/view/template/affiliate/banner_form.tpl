<?php echo $header; ?>
<div id="content">
<?php $domainUrl = "http://".$_SERVER['HTTP_HOST']; ?>
<script src="/admin/ckfinder/ckfinder.js" type="text/javascript"></script>
<script>
function BrowseServer_URL() {
	var finder_URL = new CKFinder();
	finder_URL.selectActionFunction = SetFileField_URL;
	finder_URL.popup();
}
function SetFileField_URL(fileUrl_URL) {
	document.getElementById('url_file').value = fileUrl_URL;
}

$(document).ready(function() {
	$('.preview_bn').bind('click', function() {
		var bnsize 	= $("#input-bn-size").val();
		var bnurl 	= $("#url_file").val();
		if(bnsize && bnurl){
			var url = '<?php echo $domainUrl; ?>/display/preview.php?size='+ bnsize +'&url='+ bnurl;
			myPreview = window.open(url, "myPreview", "width=760, height=650", "_newtab");
		} else {
			alert("<?php echo $text_please_choose_img; ?>");
		}
	});
});

</script>
<style>
.dropdown-menu {
    background-clip: padding-box;
    background-color: #ffffff;
    border: 1px solid rgba(0, 0, 0, 0.15);
    border-radius: 3px;
    box-shadow: 0 6px 12px rgba(0, 0, 0, 0.176);
    display: none;
    float: left;
    font-size: 12px;
    left: 0;
    list-style: outside none none;
    margin: 2px 0 0;
    min-width: 160px;
    padding: 5px 0;
    position: absolute;
    text-align: left;
    top: 100%;
    z-index: 1000;
	max-height: 200px;
	overflow:auto;
}
.dropdown-menu > li > a {
    clear: both;
    color: #333333;
    display: block;
    font-weight: normal;
    line-height: 1.42857;
    padding: 3px 20px;
    white-space: nowrap;
	text-decoration:none;
}
</style>
	<div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
  	</div>
    
    <?php if ($error_warning) { ?>
  		<div class="warning"><?php echo $error_warning; ?></div>
  	<?php } ?>
	<div class="box">
    	<div class="heading">
        	<h1><?php echo $heading_title; ?></h1>
            <div class="buttons">
            	<a onclick="$('#form-banner').submit();" class="button"><?php echo $text_save; ?></a>
                <a title="<?php echo $text_preview; ?>" class="button preview_bn"><?php echo $text_preview; ?></a>
            	<a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $text_cancel; ?></a>
            </div>
        </div>
        <div class="content">
        	<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-banner" class="form-horizontal">
            	<table class="form">
                	<tr>
                        <td><span class="required">*</span> <?php echo $text_name; ?></td>
                        <td>
                            <input type="text" name="bn_name" size="100" value="<?php echo $bn_name; ?>" placeholder="<?php echo $text_bn_name; ?>" id="input-bn-name" class="form-control" />
                  			<?php if ($error_bn_name) { ?>
                  				<div class="error"><?php echo $text_no_none; ?></div>
                  			<?php } ?>
                        </td>
                	</tr>
                    <tr>
                        <td><span class="required">*</span> <?php echo $text_description; ?></td>
                        <td>
                            <textarea name="bn_des" id="input-bn-des" class="form-control" cols="100" rows="2" placeholder="<?php echo $text_bn_description; ?>"><?php echo $bn_des; ?></textarea>
                        	<?php if ($error_bn_des) { ?>
                  				<div class="error"><?php echo $text_no_none; ?></div>
                  			<?php } ?>
                        </td>
                	</tr>
                    <tr>
                        <td><span class="required">*</span> <?php echo $text_url_image; ?></td>
                        <td>
                            <input type="text" name="bn_img" size="120" id="url_file" value="<?php echo $bn_img; ?>" placeholder="Url banner" class="form-control" />
                            <button class="btn btn-info form-control" type="button" onclick="BrowseServer_URL();"><i class="fa fa-save"></i> <?php echo $text_choose_img; ?></button>
                            <?php if ($error_bn_img) { ?>
                  				<br><div class="error"><?php echo $text_no_none; ?></div>
                  			<?php } ?>
                        </td>
                	</tr>
                    <tr>
                        <td><span class="required">*</span> <?php echo $text_campaign; ?></td>
                        <td>
                            <select name="bn_camid" id="input-banner-cateid" class="form-control">
                            	<option value=""><?php echo $text_select_campaign; ?></option>
                            
                                <?php foreach ($campaigns as $campaign) { ?>
                                <?php if ($campaign['campaign_id'] == $bn_camid) { ?>
                                <option value="<?php echo $campaign['campaign_id']; ?>" selected="selected"> <?php echo $campaign['campaign_name']; ?> </option>
                                <?php } else { ?>
                                <option value="<?php echo $campaign['campaign_id']; ?>"><?php echo $campaign['campaign_name']; ?></option>
                                <?php } ?>
                                <?php } ?>
                          	</select>
                          	<?php if ($error_bn_camid) { ?>
                  				<div class="error"><?php echo $text_no_none; ?></div>
                  			<?php } ?>
                        </td>
                	</tr>
                    <tr>
                        <td><?php echo $text_product; ?> <em>(<?php echo $text_option; ?>)</em></td>
                        <td>
                            <input type="text" name="product" value="<?php echo $product_name; ?>" placeholder="<?php echo $text_option; ?>" id="input-product" class="form-control" />
                  			<input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
                        </td>
                	</tr>
                    <tr>
                        <td><span class="required">*</span> <?php echo $text_size; ?></td>
                        <td>
                            <select name="bn_size" id="input-bn-size" class="form-control">
                                <option value=""><?php echo $text_select_size; ?></option>
                                <?php foreach ($banners as $banner) { ?>
                                <?php if ($banner['size_name'] == $bn_size) { ?>
                                <option value="<?php echo $banner['size_name']; ?>" selected="selected"> <?php echo $banner['size_name']; ?> </option>
                                <?php } else { ?>
                                <option value="<?php echo $banner['size_name']; ?>"><?php echo $banner['size_name']; ?></option>
                                <?php } ?>
                                <?php } ?>
                          	</select>
                          	<?php if ($error_bn_size) { ?>
                  				<div class="error"><?php echo $text_no_none; ?></div>
                  			<?php } ?>
                        </td>
                	</tr>
                    <tr>
                        <td><?php echo $text_status; ?></td>
                        <td>
                            <select name="bn_status" id="input-bn-status" class="form-control">
                                <?php if ($bn_status) { ?>
                                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                <option value="0"><?php echo $text_disabled; ?></option>
                                <?php } else { ?>
                                <option value="1"><?php echo $text_enabled; ?></option>
                                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                <?php } ?>
                          	</select>
                        </td>
                	</tr>
                </table>
            </form>
        </div>
	</div>
<script>
// Autocomplete */
(function($) {
	$.fn.autocomplete = function(option) {
		return this.each(function() {
			this.timer = null;
			this.items = new Array();
	
			$.extend(this, option);
	
			$(this).attr('autocomplete', 'off');
			
			// Focus
			$(this).on('focus', function() {
				this.request();
			});
			
			// Blur
			$(this).on('blur', function() {
				setTimeout(function(object) {
					object.hide();
				}, 200, this);				
			});
			
			// Keydown
			$(this).on('keydown', function(event) {
				switch(event.keyCode) {
					case 27: // escape
						this.hide();
						break;
					default:
						this.request();
						break;
				}				
			});
			
			// Click
			this.click = function(event) {
				event.preventDefault();
	
				value = $(event.target).parent().attr('data-value');
	
				if (value && this.items[value]) {
					this.select(this.items[value]);
				}
			}
			
			// Show
			this.show = function() {
				var pos = $(this).position();
	
				$(this).siblings('ul.dropdown-menu').css({
					top: pos.top + $(this).outerHeight(),
					left: pos.left
				});
	
				$(this).siblings('ul.dropdown-menu').show();
			}
			
			// Hide
			this.hide = function() {
				$(this).siblings('ul.dropdown-menu').hide();
			}		
			
			// Request
			this.request = function() {
				clearTimeout(this.timer);
		
				this.timer = setTimeout(function(object) {
					object.source($(object).val(), $.proxy(object.response, object));
				}, 200, this);
			}
			
			// Response
			this.response = function(json) {
				html = '';
	
				if (json.length) {
					for (i = 0; i < json.length; i++) {
						this.items[json[i]['value']] = json[i];
					}
	
					for (i = 0; i < json.length; i++) {
						if (!json[i]['category']) {
							html += '<li data-value="' + json[i]['value'] + '"><a href="#">' + json[i]['label'] + '</a></li>';
						}
					}
	
					// Get all the ones with a categories
					var category = new Array();
	
					for (i = 0; i < json.length; i++) {
						if (json[i]['category']) {
							if (!category[json[i]['category']]) {
								category[json[i]['category']] = new Array();
								category[json[i]['category']]['name'] = json[i]['category'];
								category[json[i]['category']]['item'] = new Array();
							}
	
							category[json[i]['category']]['item'].push(json[i]);
						}
					}
	
					for (i in category) {
						html += '<li class="dropdown-header">' + category[i]['name'] + '</li>';
	
						for (j = 0; j < category[i]['item'].length; j++) {
							html += '<li data-value="' + category[i]['item'][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[i]['item'][j]['label'] + '</a></li>';
						}
					}
				}
	
				if (html) {
					this.show();
				} else {
					this.hide();
				}
	
				$(this).siblings('ul.dropdown-menu').html(html);
			}
			
			$(this).after('<ul class="dropdown-menu"></ul>');
			$(this).siblings('ul.dropdown-menu').delegate('a', 'click', $.proxy(this.click, this));	
			
		});
	}
})(window.jQuery);
$('input[name=\'product\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',			
			success: function(json) {
				json.unshift({
					product_id: 0,
					name: '--None--'
				});
				
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'product\']').val(item['label']);
		$('input[name=\'product_id\']').val(item['value']);
	}	
});
</script>
</div>
<?php echo $footer; ?>