<?php echo $header; ?>
<style>
.row_detail {
	border-bottom: 1px solid #ddd;
	border-left: 1px solid #ddd;
	border-right: 1px solid #ddd;
	padding: 8px 0;
	width: 99.8%;
	display:table;
}
.row_detail .col-sm-6 {padding: 0 10px;}
.row_detail .links { text-align:left;float:left;}
.row_detail .links b { font-size: 14px; padding: 5px 7px;}
.row_detail .links a { font-size: 14px; text-decoration:none; padding: 5px 7px;}
.row_detail .results { text-align:right;float: right;}
</style>
<div id="content">
	<div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
  	</div>
    <?php if ($error_warning) { ?>
  		<div class="warning"><?php echo $error_warning; ?></div>
  	<?php } ?>
  	<?php if ($success) { ?>
  		<div class="success"><?php echo $success; ?></div>
  	<?php } ?>
	
    <div class="box">
    	<div class="heading">
        	<h1><?php echo $heading_title; ?></h1>
            <div class="buttons">
            	<a class="button" onclick="location = '<?php echo $add; ?>'"><?php echo $button_add; ?></a>
                <a class="button" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-campaign').submit() : false;"><?php echo $button_delete; ?></a>
            </div>
        </div>
        <div class="content">
        		<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-campaign">
                <table class="list" style="margin-bottom:0;">
              		<thead>
                    	<tr>
                        	<td style="width: 1px;" class="center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                            <td class="left">
                            	<?php if ($sort == 'name') { ?>
                    				<a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $text_name; ?></a>
                    			<?php } else { ?>
                    				<a href="<?php echo $sort_name; ?>"><?php echo $text_name; ?></a>
                    			<?php } ?>
                    		</td>
                            <td class="left">
                            	<?php echo $text_category; ?>
                            </td>
                            <td class="left">
                            	<?php echo $text_description; ?>
                            </td>
                            <td class="left">
                            	<?php echo $text_country; ?>
                            </td>
                            <td class="left">
                   				<?php echo $text_date_create; ?>
                            <td class="left">
                            	<?php echo $text_status; ?>
                            </td>
                            <td class="right"><?php echo $text_action; ?></td>
                        </tr>
                    </thead>
                    <tbody>
                    	<?php if ($campaigns) { ?>
                        <?php foreach ($campaigns as $campaign) { ?>
                    	<tr>
                        	<td class="center">
                            <?php if (in_array($campaign['campaign_id'], $selected)) { ?>
                    			<input type="checkbox" name="selected[]" value="<?php echo $campaign['campaign_id']; ?>" checked="checked" />
                    		<?php } else { ?>
                    			<input type="checkbox" name="selected[]" value="<?php echo $campaign['campaign_id']; ?>" />
                   			<?php } ?>
                    		</td>
                        	<td class="left"><?php echo $campaign['campaign_name']; ?></td>
                            <?php if(isset($campaign['cate_name'])){ ?>
                            	<td class="left"><?php echo $campaign['cate_name']; ?></td>
                            <?php } else { ?>
                            	<td class="left">Home page</td>
                            <?php } ?>
                            <td class="left"><?php echo $campaign['campaign_des']; ?></td>
                            <td class="left"><?php echo $campaign['country_name']; ?></td>
                            <td class="left"><?php echo $campaign['campaign_create']; ?></td>
                            <td class="left"><?php echo $campaign['campaign_status']; ?></td>
                            <td class="right">         
                    			<a href="<?php echo $campaign['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><?php echo $button_edit; ?></a>
                               
                    		</td>
                        </tr>
                        <?php } ?>
                        <?php } else { ?>
                        <tr>
                          	<td class="center" colspan="8">No campaign!</td>
                        </tr>
                        <?php } ?>
                    </tbody>
				</table>
                </form>
                <div class="row_detail">
          			<div class="col-sm-6"><?php echo $pagination; ?></div>
        		</div>
        </div>
    </div>

  <script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	url = 'index.php?route=affiliate/campaign&token=<?php echo $token; ?>';
	
	var filter_name = $('input[name=\'filter_name\']').val();
	
	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}
	
	var filter_email = $('input[name=\'filter_email\']').val();
	
	if (filter_email) {
		url += '&filter_email=' + encodeURIComponent(filter_email);
	}
		
	var filter_status = $('select[name=\'filter_status\']').val();
	
	if (filter_status != '*') {
		url += '&filter_status=' + encodeURIComponent(filter_status); 
	}	
	
	var filter_approved = $('select[name=\'filter_approved\']').val();
	
	if (filter_approved != '*') {
		url += '&filter_approved=' + encodeURIComponent(filter_approved);
	}	
	
	var filter_date_added = $('input[name=\'filter_date_added\']').val();
	
	if (filter_date_added) {
		url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
	}
	
	location = url;
});
//--></script> 
  <script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=affiliate/campaign/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',			
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['affiliate_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_name\']').val(item['label']);
	}	
});

$('input[name=\'filter_email\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=affiliate/campaign/autocomplete&token=<?php echo $token; ?>&filter_email=' +  encodeURIComponent(request),
			dataType: 'json',			
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['email'],
						value: item['affiliate_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_email\']').val(item['label']);
	}	
});
//--></script> 
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script>
</div>
<?php echo $footer; ?>