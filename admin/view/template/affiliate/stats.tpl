<?php echo $header; ?>
<div id="content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
  	</div>
    <?php if ($error_warning) { ?>
  		<div class="warning"><?php echo $error_warning; ?></div>
  	<?php } ?>
  	<?php if ($success) { ?>
  		<div class="success"><?php echo $success; ?></div>
  	<?php } ?>
<style>
.col_views_clicks {
	width: 65%;
	min-height:250px;
	float:left;
}
.col_event {
	width: 35%;
	min-height:250px;
}
.col_views, .col_clicks { padding: 0 10px 10px 0;}
#container_views, #container_stats { border: 1px solid #ddd;padding-top: 10px;}
.last_day_heade {
	clear: both;
}
.last_day_heade { padding: 10px; background:#4090D1;}
.last_day_heade h3 { display:inline-block; margin:0; color:#fff;}
.choose_month {display: inline-block;float: right;margin-top: -4px;}
</style>
<script>
$(document).ready(function() {
	/* Search */
	$('.button-month').bind('click', function() {
		var month = $("#value_month").val();
		if (month != '') {
			url = "<?php echo $breadcrumb['href']; ?>&month=" + month;
		}
		location = url;
	});
});
</script>
    <div class="box">
    	<div class="heading">
      		<h1><?php echo $heading_title; ?></h1>
        </div>
        <div class="content">
        	<div class="col_views_clicks">
            	<div class="col_views">
<!--<script src="../display/jquery.min.js"></script>-->
<script src="view/javascript/highcharts/exporting.js" type="text/javascript"></script>
<script src="view/javascript/highcharts/highcharts.js" type="text/javascript"></script>
    <style> .highcharts-button, .highcharts-legend { display: none !important;}</style>
<script type="text/javascript">
$(function () {
    $('#container_views').highcharts({
        chart: {
            type: 'areaspline'
        },
		colors: ["#2E9BE0"],
        title: {
            text: ''
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: 150,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        xAxis: {
            categories: <?php echo $category; ?>

        },
        yAxis: {
            title: {
                text: 'Impression'
            }
        },
        tooltip: {
            shared: true,
            valueSuffix: ''
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            areaspline: {
                fillOpacity: 0.5
            }
        },
        series: [{
            name: 'Impression',
            data: <?php echo $series1; ?>
        }]
    });
});
</script>
                	<?php if(isset($category)){ ?>
            		<div id="container_views" style="width:100%; height:180px; margin-bottom:5px;"></div>
                    <?php } else { ?>
                    	<p style="margin:0; padding:10px;" class="text-center"><?php echo $text_no_data; ?></p>
                    <?php } ?>
                </div>
                <div class="col_clicks">
<script type="text/javascript">
$(function () {
    $('#container_stats').highcharts({
        chart: {
            type: 'areaspline'
        },
		colors: ["#57c713", "#6fb3e0", "#d87311"],
        title: {
            text: ''
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: 150,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        xAxis: {
            categories: <?php echo $category; ?>

        },
        yAxis: {
            title: {
                text: 'Clicks'
            }
        },
        tooltip: {
            shared: true,
            valueSuffix: ''
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            areaspline: {
                fillOpacity: 0.5
            }
        },
        series: [{
            name: 'Click',
            data: <?php echo $series; ?>
        }]
    });
});
</script>
                	<?php if(isset($category)){ ?>
            		<div id="container_stats" style="width:100%; height:180px; margin-bottom:5px;"></div>
                    <?php } else { ?>
                    	<p style="margin:0; padding:10px;" class="text-center"><?php echo $text_no_data; ?></p>
                    <?php } ?>
                </div>
            </div>
            <div class="col_event">
            		<table class="list">
                   		<thead>
                       		<tr>
                           		<td class="left"><?php echo $text_email; ?></td>
                             	<td class="left"><?php echo $text_description; ?></td>
                            	<td colspan="2" class="left"><?php echo $text_amount; ?></td>
                       		</tr>
                   		</thead>
                        <tbody>
                        <?php if(isset($trans)){ ?>
                        	<?php foreach ($trans as $tran) { ?>
                        	<tr>
                           		<td class="left"><?php echo $tran['email']; ?></td>
                             	<td class="left"><?php echo $tran['description']; ?></td>
                            	<td class="left">
                                <?php if($tran['amount'] > 0){ ?>
                                <strong style="color: green;">
                                	$+<?php echo number_format($tran['amount'],2); ?>
                                </strong>
                                <?php } else { ?>
                                <strong style="color: red;">
                                	$<?php echo number_format($tran['amount'],2); ?>
                                </strong>
                                <?php } ?>
                                </td>
                                <td class="left"><a href="<?php echo $tran['link_account']; ?>" target="_blank">
                                <i class="fa fa-eye"></i></a></td>
                       		</tr>
                            <?php } ?>
                        <?php } else { ?>
                        	<tr>
                            	<td colspan="3" class="center"><?php echo $text_no_data; ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                	</table>
            </div>
            <div class="last_day_month">
            	<div class="last_day_heade">
                	<h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> <?php echo $text_last_day_month; ?>:</h3>
                    <div class="choose_month">
                    	<select class="month" id="value_month" name="month">
                            <?php foreach($group_months as $group_month){ ?>
                            <?php if($smonth == $group_month['statsd_month']){ ?>
                            	<option selected value="<?php echo $group_month['statsd_month']; ?>"><?php echo $group_month['statsd_month']; ?></option>
                           	<?php } else { ?>
                            	<option value="<?php echo $group_month['statsd_month']; ?>"><?php echo $group_month['statsd_month']; ?></option>
                           	<?php } ?>
                            
                            <?php } ?>
                        </select>
                        <button class="button-month" type="button">Apply</button>
                    </div>
                </div>
        			<table class="list" style=" margin-bottom:0;">
                   		<thead>
                       		<tr>
                           		<td class="left"><?php echo $text_date; ?></td>
                             	<td class="left"><?php echo $text_impression; ?></td>
                            	<td class="left"><?php echo $text_click; ?></td>
                            	<td class="left"><?php echo $text_sale; ?></td>
                                <td class="left"><?php echo $text_amount; ?></td>
                            	<td class="left"><?php echo $text_ctr; ?></td>
                          		<td class="left"><?php echo $text_cr; ?></td>
                       		</tr>
                   		</thead>
                        <tbody>
                        <?php if(isset($stats)){ ?>
                        	<?php
                            $total_views = 0;
                            $total_click = 0;
                            $total_sale = 0;
                            $total_amount = 0;
                            ?>
                            <?php foreach ($stats as $stat) { ?>
                            <?php
                            	$total_views = $total_views + $stat['statsd_views'];
                            	$total_click = $total_click + $stat['statsd_click'];
                            	$total_sale = $total_sale + $stat['total_sale'];
                                $total_amount = $total_amount + $stat['total_amount'];
                            ?>
                        	<tr>
                           		<td  class="left"><?php echo $stat['statsd_date']; ?></td>
                            	<td  class="left"><?php echo number_format($stat['statsd_views'],0); ?></td>
                             	<td  class="left"><?php echo number_format($stat['statsd_click'],0); ?></td>
                              	<td  class="left"><?php echo number_format($stat['total_sale'],0); ?></td>
                                <td  class="left">$<?php echo number_format($stat['total_amount'],2); ?></td>
                               	<td  class="left">
                                <?php if($stat['statsd_views'] > 0){ ?>
                                <?php echo number_format((@$stat['statsd_click']/@$stat['statsd_views'])*100,2); ?>%
                                <?php } else { ?>
                                0.00%
                                <?php } ?>
                                </td>
                               	<td  class="left">
                                <?php if($stat['statsd_click'] > 0){ ?>
                                <?php echo number_format((@$total_sale/@$stat['statsd_click'])*100,2); ?>%
                                <?php } else { ?>
                                0.00%
                                <?php } ?>
                                </td>
                       		</tr>
                        <?php } ?>
                        <?php } else { ?>
                        	<tr>
                            	<td colspan="7" class="center"><?php echo $text_no_data; ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                        <thead>
                        <?php if(isset($stats)){ ?>
                       		<tr>
                            	<td class="left"><?php echo $text_total; ?>: (<?php echo $smonth; ?>)</td>
                           		<td class="left"><?php echo number_format($total_views,0); ?></td>
                             	<td class="left"><?php echo number_format($total_click,0); ?></td>
                         		<td class="left"><?php echo number_format($total_sale,0); ?></td>
                                <td class="left">$<?php echo number_format($total_amount,2); ?></td>
                              	<td class="left"><?php echo number_format((@$total_click/@$total_views)*100,2); ?>%</td>
                             	<td class="left"><?php echo number_format((@$total_sale/@$total_click)*100,2); ?>%</td>
                      		</tr>
                        <?php } else { ?>
                        	<tr>
                            	<td class="left"><?php echo $text_total; ?>: (<?php echo $smonth; ?>)</td>
                           		<td class="left">0</td>
                             	<td class="left">0</td>
                         		<td class="left">0</td>
                                <td class="left">$0.00</td>
                              	<td class="left">0.00%</td>
                             	<td class="left">0.00%</td>
                      		</tr>
                        <?php } ?>
                   		</thead>
                	</table>
            </div>
        </div>
    </div>

</div>

<?php echo $footer; ?>
