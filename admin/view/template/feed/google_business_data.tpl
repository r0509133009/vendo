<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/feed.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
          <tr>
            <td><?php echo $entry_status; ?></td>
            <td><select name="google_business_data_status">
                <?php if ($google_business_data_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
          </tr>
          <tr>
            <td><?php echo $entry_file; ?></td>
            <td><select name="google_business_data_file">
                <?php if ($google_business_data_file) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
          </tr>
          <tr>
            <td><?php echo $entry_data_feed; ?></td>
            <td><textarea cols="40" rows="5"><?php echo $data_feed; ?></textarea></td>
          </tr>

          <tr>
            <td><?php echo $entry_google_business_data_description; ?></td>
            <td><select name="google_business_data_description">
                <?php if ($google_business_data_description) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
          </tr>

          <tr>
            <td><?php echo $entry_google_business_data_description_html; ?></td>
            <td><select name="google_business_data_description_html">
                <?php if ($google_business_data_description_html) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
          </tr>

		<tr>
			<td><?php echo $entry_feed_business_data_id1; ?></td>
			<td><select name="feed_business_data_id1">
				<?php if ($feed_business_data_id1 == 'model') { ?>
					<option value="model" selected="selected"><?php echo 'model'; ?></option>
				<?php } else { ?>
					<option value="model"><?php echo 'model'; ?></option>
				<?php } if ($feed_business_data_id1 == 'product_id') { ?>
					<option value="product_id" selected="selected"><?php echo 'product_id'; ?></option>
				<?php } else { ?>
					<option value="product_id"><?php echo 'product_id'; ?></option>
				<?php } ?>
			</select></td>
		</tr>
		<tr>
			<td><?php echo $entry_feed_business_data_id2; ?></td>
			<td><select name="feed_business_data_id2">
				<?php if ($feed_business_data_id2 == '') { ?>
					<option value="" selected="selected"></option>
				<?php } else { ?>
					<option value=""></option>
				<?php } ?>
				<?php if ($feed_business_data_id2 == 'model') { ?>
					<option value="model" selected="selected"><?php echo 'model'; ?></option>
				<?php } else { ?>
					<option value="model"><?php echo 'model'; ?></option>
				<?php } ?>
				<?php if ($feed_business_data_id2 == 'product_id') { ?>
					<option value="product_id" selected="selected"><?php echo 'product_id'; ?></option>
				<?php } else { ?>
					<option value="product_id"><?php echo 'product_id'; ?></option>
				<?php } ?>
			</select></td>
		</tr>
	
        </table>
      </form>
    </div>
  </div>
</div>
<?php echo $footer; ?>
