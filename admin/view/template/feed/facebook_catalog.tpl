<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/feed.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
          <tr>
            <td><?php echo $entry_status; ?></td>
            <td><select name="facebook_catalog_status">
                <?php if ($facebook_catalog_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
          </tr>
          <tr>
            <td><?php echo $entry_file; ?></td>
            <td><select name="facebook_catalog_file">
                <?php if ($facebook_catalog_file) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
          </tr>
          <tr>
            <td><?php echo $entry_data_feed; ?></td>
            <td><textarea cols="40" rows="5"><?php echo $data_feed; ?></textarea></td>
          </tr>
          <tr>
            <td><?php echo $entry_facebook_catalog_availability; ?></td>
            <td><select name="facebook_catalog_availability">
                <?php if ($facebook_catalog_availability==1) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
				<option value="2"><?php echo 'skip products'; ?></option>
				<?php } elseif ($facebook_catalog_availability==2) { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
				<option value="2" selected="selected"><?php echo 'skip products'; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
				<option value="2"><?php echo 'skip products'; ?></option>
                <?php } ?>
              </select></td>
          </tr>
          <tr>
            <td><?php echo $entry_facebook_catalog_description; ?></td>
            <td><select name="facebook_catalog_description">
                <?php if ($facebook_catalog_description) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
          </tr>

          <tr>
            <td><?php echo $entry_facebook_catalog_description_html; ?></td>
            <td><select name="facebook_catalog_description_html">
                <?php if ($facebook_catalog_description_html) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
          </tr>

		<tr>
              <td><?php echo $entry_facebook_attribute_type; ?></td>
              <td><select name="facebook_attribute_type" style="max-width: 90%;">
		<?php foreach ($facebook_attributes_type as $facebook_type) {
			if ($facebook_attribute_type==$facebook_type['attribute_id']) { ?>
				<option value="<?php echo $facebook_type['attribute_id']; ?>" selected="selected"><?php echo $facebook_type['name']; ?></option>
			<?php } else { ?>
				<option value="<?php echo $facebook_type['attribute_id']; ?>"><?php echo $facebook_type['name']; ?></option>
			<?php } ?>
		<?php } ?>
	    </select></td>
 	  </tr>

		<tr>
			<td><?php echo $entry_feed_facebook_id1; ?></td>
			<td><select name="feed_facebook_id1">
				<?php if ($feed_facebook_id1 == 'model') { ?>
					<option value="model" selected="selected"><?php echo 'model'; ?></option>
				<?php } else { ?>
					<option value="model"><?php echo 'model'; ?></option>
				<?php } if ($feed_facebook_id1 == 'product_id') { ?>
					<option value="product_id" selected="selected"><?php echo 'product_id'; ?></option>
				<?php } else { ?>
					<option value="product_id"><?php echo 'product_id'; ?></option>
				<?php } ?>
			</select></td>
		</tr>

        </table>
      </form>
    </div>
  </div>
</div>
<?php echo $footer; ?>
