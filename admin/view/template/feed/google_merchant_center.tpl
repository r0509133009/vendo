<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/feed.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
          <tr>
            <td><?php echo $entry_status; ?></td>
            <td><select name="google_merchant_center_status">
                <?php if ($google_merchant_center_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
          </tr>
          <tr>
            <td><?php echo $entry_file; ?></td>
            <td><select name="google_merchant_center_file">
                <?php if ($google_merchant_center_file) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
          </tr>
          <tr>
            <td><?php echo $entry_data_feed; ?></td>
            <td><textarea cols="40" rows="5"><?php echo $data_feed; ?></textarea></td>
          </tr>

           <tr>
              <td><?php echo $entry_google_merchant_base; ?></td>
              <td><div class="scrollbox">
                  <?php foreach ($google_merchant_base_category as $merchant_category) { ?>
                  <?php $class=''; $class = ($class == 'even' ? 'odd' : 'even'); ?>
                  <div class="<?php echo $class; ?>">
                    <?php if ($merchant_category['status']=="1") { ?>
                    <input type="checkbox" name="google_merchant_base[]" value="<?php echo $merchant_category['taxonomy_id']; ?>" checked="checked" />
                    <?php echo $merchant_category['name']; ?>
                    <?php } else { ?>
                    <input type="checkbox" name="google_merchant_base[]" value="<?php echo $merchant_category['taxonomy_id']; ?>" />
                    <?php echo $merchant_category['name']; ?>
                    <?php } ?>
                  </div>
                  <?php } ?>
                </div></td>
            </tr>
            <tr>
              <td><?php echo $entry_google_merchant_center_attribute; ?></td>
              <td><select name="google_merchant_center_attribute" style="max-width: 90%;">
		<?php foreach ($merchant_center_attributes as $merchant_center_attribute) {
			if ($google_merchant_center_attribute==$merchant_center_attribute['attribute_id']) { ?>
				<option value="<?php echo $merchant_center_attribute['attribute_id']; ?>" selected="selected"><?php echo $merchant_center_attribute['name']; ?></option>
			<?php } else { ?>
				<option value="<?php echo $merchant_center_attribute['attribute_id']; ?>"><?php echo $merchant_center_attribute['name']; ?></option>
			<?php } ?>
		<?php } ?>
	    </select></td>
 	  </tr>

		<tr>
              <td><?php echo $entry_google_merchant_center_attribute_type; ?></td>
              <td><select name="google_merchant_center_attribute_type" style="max-width: 90%;">
		<?php foreach ($merchant_center_attributes_type as $merchant_center_type) {
			if ($google_merchant_center_attribute_type==$merchant_center_type['attribute_id']) { ?>
				<option value="<?php echo $merchant_center_type['attribute_id']; ?>" selected="selected"><?php echo $merchant_center_type['name']; ?></option>
			<?php } else { ?>
				<option value="<?php echo $merchant_center_type['attribute_id']; ?>"><?php echo $merchant_center_type['name']; ?></option>
			<?php } ?>
		<?php } ?>
	    </select></td>
 	  </tr>

      <tr>
         <td><?php echo $entry_google_merchant_center_option; ?></td>
              <td><select name="google_merchant_center_option" style="max-width: 90%;">
		<?php foreach ($merchant_center_option as $merchant_option) {
			if ($google_merchant_center_option==$merchant_option['option_id']) { ?>
				<option value="<?php echo $merchant_option['option_id']; ?>" selected="selected"><?php echo $merchant_option['name']; ?></option>
			<?php } else { ?>
				<option value="<?php echo $merchant_option['option_id']; ?>"><?php echo $merchant_option['name']; ?></option>
			<?php } ?>
		<?php } ?>
	    </select></td>
 	  </tr>
          <tr>
            <td><?php echo $entry_google_merchant_center_availability; ?></td>
            <td><select name="google_merchant_center_availability">
                <?php if ($google_merchant_center_availability=='in stock') { ?>
                <option value="in stock" selected="selected"><?php echo 'in stock'; ?></option>
                <option value="out of stock"><?php echo 'out of stock'; ?></option>
				<option value="preorder"><?php echo 'preorder'; ?></option>
				<option value="skip products"><?php echo 'skip products'; ?></option>
                <?php } elseif ($google_merchant_center_availability=='out of stock') { ?>
                <option value="in stock"><?php echo 'in stock'; ?></option>
                <option value="out of stock" selected="selected"><?php echo 'out of stock'; ?></option>
				<option value="preorder"><?php echo 'preorder'; ?></option>
				<option value="skip products"><?php echo 'skip products'; ?></option>
                <?php } elseif ($google_merchant_center_availability=='preorder') { ?>
              	<option value="in stock"><?php echo 'in stock'; ?></option>
                <option value="out of stock"><?php echo 'out of stock'; ?></option>
				<option value="preorder" selected="selected"><?php echo 'preorder'; ?></option>
				<option value="skip products"><?php echo 'skip products'; ?></option>
				<?php } else { ?>
              	<option value="in stock"><?php echo 'in stock'; ?></option>
                <option value="out of stock"><?php echo 'out of stock'; ?></option>
				<option value="preorder"><?php echo 'preorder'; ?></option>
				<option value="skip products" selected="selected"><?php echo 'skip products'; ?></option>
                <?php } ?>
              </select></td>
          </tr>

          <tr>
            <td><?php echo $entry_google_merchant_center_description; ?></td>
            <td><select name="google_merchant_center_description">
                <?php if ($google_merchant_center_description) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
          </tr>

          <tr>
            <td><?php echo $entry_google_merchant_center_description_html; ?></td>
            <td><select name="google_merchant_center_description_html">
                <?php if ($google_merchant_center_description_html) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
          </tr>

          <tr>
            <td><?php echo $entry_google_merchant_center_shipping_flat; ?></td>
	    	<td><input name="google_merchant_center_shipping_flat" value="<?php echo $google_merchant_center_shipping_flat ?>" /></td>
          </tr>

		<tr>
			<td><?php echo $entry_feed_merchant_center_id1; ?></td>
			<td><select name="feed_merchant_center_id1">
				<?php if ($feed_merchant_center_id1 == 'model') { ?>
					<option value="model" selected="selected"><?php echo 'model'; ?></option>
				<?php } else { ?>
					<option value="model"><?php echo 'model'; ?></option>
				<?php } if ($feed_merchant_center_id1 == 'product_id') { ?>
					<option value="product_id" selected="selected"><?php echo 'product_id'; ?></option>
				<?php } else { ?>
					<option value="product_id"><?php echo 'product_id'; ?></option>
				<?php } ?>
			</select></td>
		</tr>

        </table>
      </form>
    </div>
  </div>
</div>
<?php echo $footer; ?>
