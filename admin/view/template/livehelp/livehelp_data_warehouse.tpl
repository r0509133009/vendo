<?php echo $header; ?>
<div id="content">
   <div class="breadcrumb">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
      <?php } ?>
   </div>
   <?php if ($error_warning) { ?>
   <div class="warning"><?php echo $error_warning; ?></div>
   <?php } ?>
   <?php if ($success) { ?>
   <div class="success"><?php echo $success; ?></div>
   <?php } ?>
   <div class="box">
      <div class="heading">
         <h1><?php echo $heading_title; ?></h1>
         <div class="buttons">
            <a onclick="$('form').submit();" class="button"><?php echo $button_delete; ?></a>
         </div>
      </div>
      <div class="content">
         <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
            <table class="list">
               <thead>
                  <tr>
                     <td width="1" style="text-align: center;">
                        <input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" />
                     </td>
                     <td class="left"><?php if ($sort == 'thread_id') { ?>
                        <a href="<?php echo $sort_thread_id; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_thread_id; ?></a>
                        <?php } else { ?>
                        <a href="<?php echo $sort_thread_id; ?>"><?php echo $column_thread_id; ?></a>
                        <?php } ?>
                     </td>
                     <td class="left"><?php if ($sort == 'customer_name') { ?>
                        <a href="<?php echo $sort_customer_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_customer_name; ?></a>
                        <?php } else { ?>
                        <a href="<?php echo $sort_customer_name; ?>"><?php echo $column_customer_name; ?></a>
                        <?php } ?>
                     </td>
                     <td class="left"><?php if ($sort == 'operator_name') { ?>
                        <a href="<?php echo $sort_operator_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_operator_name; ?></a>
                        <?php } else { ?>
                        <a href="<?php echo $sort_operator_name; ?>"><?php echo $column_operator_name; ?></a>
                        <?php } ?>
                     </td>
                     <td class="left"><?php echo $column_message; ?> <span class="btn-help" onclick="$('#fulltext-help').dialog({width: 500});"><i class="icon-info"></i></span></td>
                     <td class="right"><?php echo $column_action; ?></td>
                  </tr>
               </thead>
               <tbody>
                  <tr class="filter">
                     <td></td>
                     <td>
                        <select name="filter_thread_id" style="width:100%;">
                           <option value=""></option>
                           <?php foreach ($threads as $thread) { ?>
                           <?php if($filter_thread_id == $thread['thread_id']) { ?>
                           <option value="<?php echo $thread['thread_id']; ?>" selected="selected"><?php echo $thread['name']; ?></option>
                           <?php } else { ?>
                           <option value="<?php echo $thread['thread_id']; ?>"><?php echo $thread['name']; ?></option>
                           <?php } ?>
                           <?php } ?>
                        </select>
                     </td>
                     <td>
                        <input type="text" name="filter_customer_name" value="<?php echo $filter_customer_name; ?>" style="width: 95%;" />
                     </td>
                     <td>
                        <input type="text" name="filter_operator_name" value="<?php echo $filter_operator_name; ?>" style="width: 95%;" />
                     </td>
                     <td>
                        <input type="text" name="filter_message" value="<?php echo $filter_message; ?>" style="width: 95%;" />
                     </td>
                     <td align="right"><a onclick="filter();" class="button"><?php echo $button_filter; ?></a></td>
                  </tr>
                  <?php if ($rows) { ?>
                  <?php foreach ($rows as $row) { ?>
                  <tr class="<?php echo $row['tr_class']; ?>">
                     <td style="text-align: center;">
                        <?php if ($row['selected']) { ?>
                        <input type="checkbox" name="selected[]" value="<?php echo $row['row_id']; ?>" checked="checked" />
                        <?php } else { ?>
                        <input type="checkbox" name="selected[]" value="<?php echo $row['row_id']; ?>" />
                        <?php } ?>
                     </td>
                     <td class="left"><?php echo $row['thread_id']; ?></td>
                     <td class="left"><?php echo $row['customer_name']; ?></td>
                     <td class="left"><?php echo $row['operator_name']; ?></td>
                     <td class="left"><?php echo $row['message']; ?></td>
                     <td></td>
                  </tr>
                  <?php } ?>
                  <?php } else { ?>
                  <tr>
                     <td class="center" colspan="6"><?php echo $text_no_results; ?></td>
                  </tr>
                  <?php } ?>
               </tbody>
            </table>
         </form>
         <div class="pagination"><?php echo $pagination; ?></div>
      </div>
   </div>
</div>
<div id="fulltext-help" title="<?php echo $text_fulltext_help; ?>" style="display: none;">
   <table class="list">
      <thead>
         <tr>
            <td class="left"><?php echo $text_special_char; ?></td>
            <td class="center"><?php echo $text_description; ?></td>
            <td class="right"><?php echo $text_sample; ?></td>
         </tr>
      </thead>
      <tbody>
         <tr>
            <td class="center"> <code>+</code> </td>
            <td>
               <?php echo $text_char_plus_description; ?>
            </td>
            <td>
               <input type="text" value="red cheap +car">
            </td>
         </tr>
         <tr>
            <td class="center"> <code>-</code> </td>
            <td>
               <?php echo $text_char_minus_description; ?>
            </td>
            <td>
               <input type="text" value="red -expensive +car">
            </td>
         </tr>
         <tr>
            <td class="center"> <code>&lt;, &gt;</code> </td>
            <td>
               <?php echo $text_char_less_great_than_description; ?>
            </td>
            <td>
               <input type="text" value="red -expensive +car">
            </td>
         </tr>
         <tr>
            <td class="center"> <code>( )</code> </td>
            <td>
               <?php echo $text_char_bracket_description; ?>
            </td>
            <td>
               <input type="text" value="+car +(>cheap <new)">
            </td>
         </tr>
         <tr>
            <td class="center"> <code>~</code> </td>
            <td>
               <?php echo $text_char_swung_dash_description; ?>
            </td>
            <td>
               <input type="text" value="red +car ~blue">
            </td>
         </tr>
         <tr>
            <td class="center"> <code>*</code> </td>
            <td>
               <?php echo $text_char_star_description; ?>
            </td>
            <td>
               <input type="text" value="car*">
            </td>
         </tr>
         <tr>
            <td class="center"> <code>" "</code> </td>
            <td>
               <?php echo $text_char_quotes_description; ?>
            </td>
            <td>
               <input type="text" value='"new cheap red car"'>
            </td>
         </tr>
      </tbody>
   </table>
</div>
<script type="text/javascript">
   function filter() {
    url = 'index.php?route=livehelp/livehelp_data_warehouse&token=<?php echo $token; ?>';
   
    var filter_thread_id = $('select[name=\'filter_thread_id\'] option:selected').val();
    
    if (filter_thread_id) {
      url += '&filter_thread_id=' + encodeURIComponent(filter_thread_id);
    }
    
    var filter_customer_name = $('input[name=\'filter_customer_name\']').val();
    
    if (filter_customer_name) {
      url += '&filter_customer_name=' + encodeURIComponent(filter_customer_name);
    }
   
    var filter_operator_name = $('input[name=\'filter_operator_name\']').val();
    
    if (filter_operator_name) {
      url += '&filter_operator_name=' + encodeURIComponent(filter_operator_name);
    }
   
    var filter_message = $('input[name=\'filter_message\']').val();
    
    if (filter_message) {
      url += '&filter_message=' + encodeURIComponent(filter_message);
    }
    
    location = url;
   }
</script> 
<script type="text/javascript">
   $(document).ready(function() {
   $('#form input').keydown(function(e) {
   if (e.keyCode == 13) {
    filter();
   }
   });
   });
</script> 
<?php echo $footer; ?>