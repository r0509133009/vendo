<?php echo $header; ?>
<div id="content">
   <div class="breadcrumb">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
      <?php } ?>
   </div>
   <?php if ($error_warning) { ?>
   <div class="warning"><?php echo $error_warning; ?></div>
   <?php } ?>
   <div class="box">
      <div class="heading">
         <h1><img src="view/image/user.png" alt="" /> <?php echo $heading_title; ?></h1>
         <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
      </div>
      <div class="content">
         <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" role="form">
            <div class="attention">
               <?php echo $text_operator_description; ?>
            </div>
            <table class="form">
               <tr>
                  <td><span class="required">*</span> <?php echo $entry_user; ?></td>
                  <td>
                     <select name="user_id" id="input_user">
                        <option value="0"><?php echo $text_select; ?></option>
                        <?php foreach ($users as $user) { ?>
                        <?php if($user['user_id'] == $user_id) { ?>
                        <option value="<?php echo $user['user_id']; ?>" selected="selected"><?php echo $user['username']; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $user['user_id']; ?>"><?php echo $user['username']; ?></option>
                        <?php } ?>
                        <?php } ?>
                     </select>
                     <?php if (!empty($error_user)) { ?>
                     <span class="error"><?php echo $error_user; ?></span>
                     <?php } ?>
                  </td>
               </tr>
               <tr>
                  <td><span class="required">*</span> <?php echo $entry_name; ?></td>
                  <td><input type="text" name="name" value="<?php echo $name; ?>" class="form-control">
                     <?php if (!empty($error_name)) { ?>
                     <span class="error"><?php echo $error_name; ?></span>
                     <?php } ?>
                  </td>
               </tr>
               <tr>
                  <td><?php echo $entry_description; ?><br><span class="help"><?php echo $text_accepting_html; ?></span></td>
                  <td><textarea name="description" class="form-control" rows="2"><?php echo $description; ?></textarea></td>
               </tr>
               <tr>
                  <td><?php echo $entry_status; ?></td>
                  <td>
                     <input id="status_enable" name="status" value="1" type="radio"<?php echo ($status ? ' checked="checked"' : ''); ?>>
                     <label for="status_enable" onclick=""><?php echo $text_enabled; ?></label>
                     <input id="status_disable" name="status" value="0" type="radio"<?php echo (!$status ? ' checked="checked"' : ''); ?>>
                     <label for="status_disable" onclick=""><?php echo $text_disabled; ?></label>
                  </td>
               </tr>
            </table>
         </form>
      </div>
   </div>
</div>
<?php echo $footer; ?>