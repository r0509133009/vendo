<?php echo $header; ?>
<div id="content">
   <div class="breadcrumb">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
      <?php } ?>
   </div>
   <?php if ($error_warning) { ?>
   <div class="warning"><?php echo $error_warning; ?></div>
   <?php } ?>
   <?php if ($success) { ?>
   <div class="success"><?php echo $success; ?></div>
   <?php } ?>
   <div class="box">
      <div class="heading">
         <h1><img src="view/image/setting.png" alt="" /> <?php echo $heading_title; ?></h1>
         <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a></div>
      </div>
      <div class="content">
         <div id="tabs" class="htabs">
            <a href="#tab-general"><?php echo $tab_general; ?></a>
            <a href="#tab-admin"><?php echo $tab_admin; ?></a>
            <a href="#tab-store"><?php echo $tab_store; ?></a>
         </div>
         <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal group-border-dashed" id="form" role="form">
            <div id="tab-general">
               <table class="form">
                  <tr>
                     <td><?php echo $entry_absolute_path; ?><br>
                        <span class="help"><?php echo $title_absolute_path; ?></span>
                     </td>
                     <td>
                        <input type="text" name="absolute_path" value="<?php echo $absolute_path; ?>" id="input_absolute_path" />
                        <?php if($sample_emoticon) { ?>
                        <br>
                        <small><?php echo $text_sample_image_display; ?></small>
                        <p>
                           <img id="abs_path_verify" src="<?php echo $absolute_path . $sample_emoticon; ?>" data-src="<?php echo $sample_emoticon; ?>" alt="<?php echo $text_sample_image; ?>">
                        </p>
                        <?php } ?>
                     </td>
                  </tr>
                  <tr>
                     <td><span class="required">*</span> <?php echo $entry_text_max_length; ?></td>
                     <td><input type="text" name="text_max_length" value="<?php echo $text_max_length; ?>" />
                        <?php if (!empty($error_text_max_length)) { ?>
                        <span class="error"><?php echo $error_text_max_length; ?></span>
                        <?php } ?>
                     </td>
                  </tr>
                  <tr>
                     <td><?php echo $entry_data_warehouse_status; ?></td>
                     <td> 
                        <input id="data_warehouse_status_enable"  name="data_warehouse_status" type="radio" value="1" <?php echo ($data_warehouse_status ? "checked" : ""); ?>>
                        <label for="data_warehouse_status_enable" onclick=""><?php echo $text_enabled; ?></label>
                        <input id="data_warehouse_status_disable" name="data_warehouse_status" type="radio" value="0" <?php echo (!$data_warehouse_status ? "checked" : ""); ?>>
                        <label for="data_warehouse_status_disable" onclick=""><?php echo $text_disabled; ?></label>
                     </td>
                  </tr>
                  <tr>
                     <td colspan="2">
                        <h2><?php echo $text_plugin; ?></h2>
                     </td>
                  </tr>
                  <tr>
                     <td><?php echo $entry_smiley_status; ?></td>
                     <td> 
                        <input id="smiley_status_enable"  name="smiley_status" type="radio" value="1" <?php echo ($smiley_status ? "checked" : ""); ?>>
                        <label for="smiley_status_enable" onclick=""><?php echo $text_enabled; ?></label>
                        <input id="smiley_status_disable" name="smiley_status" type="radio" value="0" <?php echo (!$smiley_status ? "checked" : ""); ?>>
                        <label for="smiley_status_disable" onclick=""><?php echo $text_disabled; ?></label>
                     </td>
                  </tr>
                  <tr>
                     <td><?php echo $entry_bbcode_status; ?></td>
                     <td> 
                        <input id="bbcode_status_enable"  name="bbcode_status" type="radio" value="1" <?php echo ($bbcode_status ? "checked" : ""); ?>>
                        <label for="bbcode_status_enable" onclick=""><?php echo $text_enabled; ?></label>
                        <input id="bbcode_status_disable" name="bbcode_status" type="radio" value="0" <?php echo (!$bbcode_status ? "checked" : ""); ?>>
                        <label for="bbcode_status_disable" onclick=""><?php echo $text_disabled; ?></label>
                     </td>
                  </tr>
                  <tr>
                     <td colspan="2">
                        <h2><?php echo $text_offline; ?></h2>
                     </td>
                  </tr>
                  <tr>
                     <td><?php echo $entry_offline_form; ?></td>
                     <td> 
                        <input id="offline_form_enable"  name="offline_form" type="radio" value="1" <?php echo ($offline_form ? "checked" : ""); ?>>
                        <label for="offline_form_enable" onclick=""><?php echo $text_enabled; ?></label>
                        <input id="offline_form_disable" name="offline_form" type="radio" value="0" <?php echo (!$offline_form ? "checked" : ""); ?>>
                        <label for="offline_form_disable" onclick=""><?php echo $text_disabled; ?></label>
                     </td>
                  </tr>
                  <tr>
                     <td><?php echo $entry_offline_form_emails; ?>
                        <br><span class="help"><?php echo $title_offline_form_emails; ?></span>
                     </td>
                     <td valign="top"> 
                        <input type="text" name="offline_form_emails" value="<?php echo $offline_form_emails; ?>" placeholder="<?php echo $text_entry_email; ?>">
                     </td>
                  </tr>
                  <tr>
                     <td colspan="2">
                        <h2><?php echo $text_spam_filter; ?></h2>
                     </td>
                  </tr>
                  <tr>
                     <td><?php echo $entry_spam_filter_status; ?></td>
                     <td> 
                        <input id="spam_filter_status_enable"  name="spam_filter_status" type="radio" value="1" <?php echo ($spam_filter_status ? "checked" : ""); ?>>
                        <label for="spam_filter_status_enable" onclick=""><?php echo $text_enabled; ?></label>
                        <input id="spam_filter_status_disable" name="spam_filter_status" type="radio" value="0" <?php echo (!$spam_filter_status ? "checked" : ""); ?>>
                        <label for="spam_filter_status_disable" onclick=""><?php echo $text_disabled; ?></label>
                     </td>
                  </tr>
                  <tr>
                     <td><?php echo $entry_spam_filter_score_limit; ?></td>
                     <td> 
                        <input type="text" name="spam_filter_score_limit" value="<?php echo $spam_filter_score_limit; ?>" id="input_spam_filter_score_limit">
                     </td>
                  </tr>
                  <tr>
                     <td><?php echo $entry_spam_filter_penalty; ?><br><span class="help">
                        <?php echo $title_spam_filter_penalty; ?>
                        </span>
                     </td>
                     <td> 
                        <input type="text" name="spam_filter_penalty" value="<?php echo $spam_filter_penalty; ?>" id="input_spam_filter_penalty"> <?php echo $text_seconds; ?>
                     </td>
                  </tr>
                  <tr>
                     <td><?php echo $entry_spam_filter_range; ?><br><span class="help">
                        <?php echo $title_spam_filter_range; ?>
                        </span>
                     </td>
                     <td> 
                        <input type="text" class="form-control" name="spam_filter_range" value="<?php echo $spam_filter_range; ?>" id="input_spam_filter_range"> <?php echo $text_seconds; ?>
                     </td>
                  </tr>
               </table>
            </div>
            <!-- TAB ADMIN -->
            <div id="tab-admin">
               <table class="form">
                  <tr>
                     <td><span class="required">*</span> <?php echo $entry_refresh_rate; ?><br>
                        <span class="help"><?php echo $title_refresh_rate; ?></span>
                     </td>
                     <td>
                        <input type="text" name="admin_refresh_rate" value="<?php echo $admin_refresh_rate; ?>" id="input_admin_refresh_rate"> <?php echo $text_miliseconds; ?>
                        <?php if (!empty($error_admin_refresh_rate)) { ?>
                        <br>
                        <span class="error"><?php echo $error_admin_refresh_rate; ?></span>
                        <?php } ?>
                     </td>
                  </tr>
                  <tr>
                     <td><span class="required">*</span> <?php echo $entry_admin_inactive_timeout; ?><br>
                        <span class="help"><?php echo $title_admin_inactive_timeout; ?></span>
                     </td>
                     <td>
                        <input type="text" class="form-control" name="admin_inactive_timeout" value="<?php echo $admin_inactive_timeout; ?>"> <?php echo $text_seconds; ?>
                        <?php if (!empty($error_admin_inactive_timeout)) { ?>
                        <br>
                        <span class="error"><?php echo $error_admin_inactive_timeout; ?></span>
                        <?php } ?>
                     </td>
                  </tr>
                  <tr>
                     <td><?php echo $entry_admin_inactive_action; ?><br>
                        <span class="help"><?php echo $title_admin_inactive_action; ?></span>
                     </td>
                     <td>
                        <select name="admin_inactive_action" class="form-control selectpicker">
                           <option value="-1"><?php echo $text_logout; ?></option>
                           <?php if($statuses) { ?>
                           <optgroup label="<?php echo $text_toggle_status; ?>">
                              <?php foreach ($statuses as $status) { ?>
                              <option value="<?php echo $status['status_id']; ?>"><?php echo $status['name']; ?></option>
                              <?php } ?>
                           </optgroup>
                           <?php } ?>
                        </select>
                     </td>
                  </tr>
               </table>
               <h2><?php echo $text_sounds; ?></h2>
               <table class="list">
                  <thead>
                     <tr style="line-height: 40px; border-bottom: 1px solid #DDD; border-right:  1px solid #DDD;">
                        <th><?php echo $column_filename; ?></th>
                        <th><small><?php echo $column_new_message; ?></small></th>
                        <th><small><?php echo $column_message_sent; ?></small></th>
                        <th><small><?php echo $column_user_logged; ?></small></th>
                        <th><small><?php echo $column_user_logout; ?></small></th>
                        <th><small><?php echo $column_special_function; ?></small></th>
                        <th><?php echo $column_test_sound; ?></th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td>
                           <span class="text-danger"><?php echo $text_off; ?></span>
                        </td>
                        <td class="active text-center">
                           <input type="radio" name="admin_sound_new_message" value=""<?php echo (!$admin_sound_new_message ? " checked" : ""); ?>>
                        </td>
                        <td class="active text-center">
                           <input type="radio" name="admin_sound_message_sent" value=""<?php echo (!$admin_sound_message_sent ? " checked" : ""); ?>>
                        </td>
                        <td class="active text-center">
                           <input type="radio" name="admin_sound_user_logged" value=""<?php echo (!$admin_sound_user_logged ? " checked" : ""); ?>>
                        </td>
                        <td class="active text-center">
                           <input type="radio" name="admin_sound_user_logout" value=""<?php echo (!$admin_sound_user_logout ? " checked" : ""); ?>>
                        </td>
                        <td class="active text-center">
                           <input type="radio" name="admin_sound_special" value=""<?php echo (!$admin_sound_special ? " checked" : ""); ?>>
                        </td>
                        <td class="text-right">
                        </td>
                     </tr>
                     <?php $i = 1; ?>
                     <?php foreach ($sounds as $sound) { ?>
                     <tr>
                        <td><?php echo $sound; ?></td>
                        <td class="active text-center">
                           <input type="radio" name="admin_sound_new_message" value="<?php echo $sound; ?>"<?php echo ($admin_sound_new_message == $sound ? " checked" : ""); ?>>
                        </td>
                        <td class="active text-center">
                           <input type="radio" name="admin_sound_message_sent" value="<?php echo $sound; ?>"<?php echo ($admin_sound_message_sent == $sound ? " checked" : ""); ?>>
                        </td>
                        <td class="active text-center">
                           <input type="radio" name="admin_sound_user_logged" value="<?php echo $sound; ?>"<?php echo ($admin_sound_user_logged == $sound ? " checked" : ""); ?>>
                        </td>
                        <td class="active text-center">
                           <input type="radio" name="admin_sound_user_logout" value="<?php echo $sound; ?>"<?php echo ($admin_sound_user_logout == $sound ? " checked" : ""); ?>>
                        </td>
                        <td class="active text-center">
                           <input type="radio" name="admin_sound_special" value="<?php echo $sound; ?>"<?php echo ($admin_sound_special == $sound ? " checked" : ""); ?>>
                        </td>
                        <td class="text-right">
                           <audio id="sound-<?php echo $i; ?>" src="../image/sounds/<?php echo $sound; ?>">
                              <p>Your browser does not support the <code>audio</code> element</p>
                           </audio>
                           <button onclick="document.getElementById('sound-<?php echo $i; ?>').play();" type="button" class="btn btn-default btn-sm"> <?php echo $text_play; ?> </button>
                        </td>
                     </tr>
                     <?php $i++; ?>
                     <?php } ?>
                  </tbody>
               </table>
            </div>
            <!-- TAB STORE -->
            <div id="tab-store">
               <table class="form">
                  <tr>
                     <td><span class="required">*</span> <?php echo $entry_refresh_rate; ?><br>
                        <span class="help"><?php echo $title_refresh_rate; ?></span>
                     </td>
                     <td>
                        <input type="text" class="form-control" name="store_refresh_rate" value="<?php echo $store_refresh_rate; ?>" id="input_store_refresh_rate"> <?php echo $text_miliseconds; ?>
                        <?php if (!empty($error_store_refresh_rate)) { ?>
                        <br>
                        <span class="error"><?php echo $error_store_refresh_rate; ?></span>
                        <?php } ?>
                     </td>
                  </tr>
                  <tr>
                     <td><span class="required">*</span> <?php echo $entry_store_inactive_timeout; ?><br>
                        <span class="help"><?php echo $title_store_inactive_timeout; ?></span>
                     </td>
                     <td>
                        <input type="text" class="form-control" name="store_inactive_timeout" value="<?php echo $store_inactive_timeout; ?>"> <?php echo $text_seconds; ?>
                        <?php if (!empty($error_store_inactive_timeout)) { ?>
                        <br>
                        <span class="error"><?php echo $error_store_inactive_timeout; ?></span>
                        <?php } ?>
                     </td>
                  </tr>
                  <tr>
                     <td><?php echo $entry_cart_update_interval; ?><br>
                        <span class="help"><?php echo $title_cart_update_interval; ?></span>
                     </td>
                     <td>
                        <input type="text" class="form-control" name="cart_update_interval" value="<?php echo $cart_update_interval; ?>" id="input_cart_update_interval">
                     </td>
                  </tr>
                  <tr>
                     <td><?php echo $entry_store_heading_title; ?><br>
                        <span class="help"><?php echo $text_accepting_html; ?></span>
                     </td>
                     <td>  
                        <?php foreach ($languages as $language) { ?>
                        <input size="50" type="text" class="form-control" name="store_heading_title[<?php echo $language['language_id']; ?>]" value="<?php echo (isset($store_heading_title[$language['language_id']]) ? $store_heading_title[$language['language_id']] : ''); ?>">
                        <img src="view/image/flags/<?php echo $language['image'] ?>" title="English"><br>
                        <?php } ?>
                     </td>
                  </tr>
               </table>
               <h2><?php echo $text_sounds; ?></h2>
               <table class="list">
                  <thead>
                     <tr style="line-height: 40px; border-bottom: 1px solid #DDD; border-right:  1px solid #DDD;">
                        <th><?php echo $column_filename; ?></th>
                        <th class="active text-center"><small><?php echo $column_new_message; ?></small></th>
                        <th class="active text-center"><small><?php echo $column_message_sent; ?></small></th>
                        <th class="active text-center"><small><?php echo $column_operator_logged; ?></small></th>
                        <th class="active text-center"><small><?php echo $column_operator_logout; ?></small></th>
                        <th class="active text-center"><small><?php echo $column_special_function; ?></small></th>
                        <th class="text-right"><?php echo $column_test_sound; ?></th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td>
                           <span class="text-danger"><?php echo $text_off; ?></span>
                        </td>
                        <td class="active text-center">
                           <input type="radio" name="store_sound_new_message" value=""<?php echo (!$store_sound_new_message ? " checked" : ""); ?>>
                        </td>
                        <td class="active text-center">
                           <input type="radio" name="store_sound_message_sent" value=""<?php echo (!$store_sound_message_sent ? " checked" : ""); ?>>
                        </td>
                        <td class="active text-center">
                           <input type="radio" name="store_sound_operator_logged" value=""<?php echo (!$store_sound_operator_logged ? " checked" : ""); ?>>
                        </td>
                        <td class="active text-center">
                           <input type="radio" name="store_sound_operator_logout" value=""<?php echo (!$store_sound_operator_logout ? " checked" : ""); ?>>
                        </td>
                        <td class="active text-center">
                           <input type="radio" name="store_sound_special" value=""<?php echo (!$store_sound_special ? " checked" : ""); ?>>
                        </td>
                        <td class="text-right">
                        </td>
                     </tr>
                     <?php $i = 1; ?>
                     <?php foreach ($sounds as $sound) { ?>
                     <tr>
                        <td><?php echo $sound; ?></td>
                        <td class="active text-center">
                           <input type="radio" name="store_sound_new_message" value="<?php echo $sound; ?>"<?php echo ($store_sound_new_message == $sound ? " checked" : ""); ?>>
                        </td>
                        <td class="active text-center">
                           <input type="radio" name="store_sound_message_sent" value="<?php echo $sound; ?>"<?php echo ($store_sound_message_sent == $sound ? " checked" : ""); ?>>
                        </td>
                        <td class="active text-center">
                           <input type="radio" name="store_sound_operator_logged" value="<?php echo $sound; ?>"<?php echo ($store_sound_operator_logged == $sound ? " checked" : ""); ?>>
                        </td>
                        <td class="active text-center">
                           <input type="radio" name="store_sound_operator_logout" value="<?php echo $sound; ?>"<?php echo ($store_sound_operator_logout == $sound ? " checked" : ""); ?>>
                        </td>
                        <td class="active text-center">
                           <input type="radio" name="store_sound_special" value="<?php echo $sound; ?>"<?php echo ($store_sound_special == $sound ? " checked" : ""); ?>>
                        </td>
                        <td class="text-right">
                           <button onclick="document.getElementById('sound-<?php echo $i; ?>').play();" type="button" class="btn btn-default btn-sm"> <?php echo $text_play; ?> </button>
                        </td>
                     </tr>
                     <?php $i++; ?>
                     <?php } ?>
                  </tbody>
               </table>
            </div>
         </form>
      </div>
   </div>
</div>
<script>
   $('#tabs a').tabs();
   $(function(){
    <?php if($sample_emoticon) { ?>
    $('#input_absolute_path').on("change", function(){
      var img = $('#abs_path_verify');
      img.attr("src", $(this).val() + img.data("src"));
    });
    <?php } ?>
   });
</script>
<?php echo $footer; ?>