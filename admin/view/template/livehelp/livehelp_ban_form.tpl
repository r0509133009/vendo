<?php echo $header; ?>
<div id="content">
   <div class="breadcrumb">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
      <?php } ?>
   </div>
   <?php if ($error_warning) { ?>
   <div class="warning"><?php echo $error_warning; ?></div>
   <?php } ?>
   <div class="box">
      <div class="heading">
         <h1><img src="view/image/customer.png" alt="" /> <?php echo $heading_title; ?></h1>
         <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
      </div>
      <div class="content">
         <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" role="form">
            <table class="form">
               <tr>
                  <td><span class="required">*</span> <?php echo $entry_user_name; ?></td>
                  <td><input type="text" name="user_name" value="<?php echo $user_name; ?>" />
                     <?php if (!empty($error_user_name)) { ?>
                     <span class="error"><?php echo $error_user_name; ?></span>
                     <?php } ?>
                  </td>
               </tr>
               <tr>
                  <td><?php echo $entry_comment; ?></td>
                  <td><textarea name="comment" rows="2" class="form-control"><?php echo $comment; ?></textarea></td>
               </tr>
               <tr>
                  <td><?php echo $entry_expired; ?><br><span class="help"><?php echo $title_expired; ?></span></td>
                  <td><input name="date_expired" value="<?php echo $date_expired; ?>" class="date" type="text"></td>
               </tr>
               <tr>
                  <td><span class="required">*</span> <?php echo $entry_ip; ?></td>
                  <td>
                     <input type="text" name="ip" value="<?php echo $ip; ?>" class="form-control">
                     <?php if($error_ip) { ?>
                     <span class="error">
                     <?php echo $error_ip; ?>
                     </span>
                     <?php } ?>
                  </td>
               </tr>
            </table>
         </form>
      </div>
   </div>
</div>
<script type="text/javascript">
   $(document).ready(function() {
      $('.date').datepicker({dateFormat: 'yy-mm-dd'});
   });
</script> 
<?php echo $footer; ?>