<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/customer.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons">
	      <a href="<?php echo $insert; ?>" class="button"><?php echo $button_insert; ?></a>
	      <a onclick="$('form').submit();" class="button"><?php echo $button_delete; ?></a>
      </div>
    </div>

    <div class="content">
      <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="list">
          <thead>
            <tr>
              <td width="1" style="text-align: center;">
              	<input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" />
              </td>
              <td class="left">
              	<?php if ($sort == 'user_name') { ?>
                <a href="<?php echo $sort_user_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_user_name; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_user_name; ?>"><?php echo $column_user_name; ?></a>
                <?php } ?>
              </td>
              <td class="left">
              	<?php if ($sort == 'date_added') { ?>
                <a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_added; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_date_added; ?>"><?php echo $column_date_added; ?></a>
                <?php } ?>
              </td>
              <td class="left">
              	<?php if ($sort == 'date_expired') { ?>
                <a href="<?php echo $sort_date_expired; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_expired; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_date_expired; ?>"><?php echo $column_date_expired; ?></a>
                <?php } ?>
              </td>
              <td class="left">
              	<?php if ($sort == 'ip') { ?>
                <a href="<?php echo $sort_ip; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_ip; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_ip; ?>"><?php echo $column_ip; ?></a>
                <?php } ?>
              </td>
              <td class="right"><?php echo $column_action; ?></td>
            </tr>
          </thead>
          <tbody>
            <tr class="filter">
              <td></td>
              <td></td>
              <td><input type="text" name="filter_date_added" class="date" value="<?php echo $filter_date_added; ?>" /></td>
              <td><input type="text" name="filter_date_expired" class="date" value="<?php echo $filter_date_expired; ?>" /></td>
              <td></td>
              <td align="right"><a onclick="filter();" class="button"><?php echo $button_filter; ?></a></td>
            </tr>
            <?php if ($banned_users) { ?>
            <?php foreach ($banned_users as $banned_user) { ?>
            <tr>
              <td style="text-align: center;"><?php if ($banned_user['selected']) { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $banned_user['ban_id']; ?>" checked="checked" />
                <?php } else { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $banned_user['ban_id']; ?>" />
                <?php } ?></td>
              <td class="left"><?php echo $banned_user['user_name']; ?></td>
              <td class="left"><?php echo $banned_user['date_added']; ?></td>
              <td class="left"><?php echo $banned_user['date_expired']; ?></td>
              <td class="left"><?php echo $banned_user['ip']; ?></td>
              <td class="right"><?php foreach ($banned_user['action'] as $action) { ?>
                [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                <?php } ?></td>
            <?php } ?>
            <?php } else { ?>
            <tr>
              <td class="center" colspan="6"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </form>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>
<script type="text/javascript">
function filter() {
	url = 'index.php?route=livehelp/livehelp_ban&token=<?php echo $token; ?>';
	
	var filter_date_added = $('input[name=\'filter_date_added\']').val();
	
	if (filter_date_added) {
		url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
	}
	
	var filter_date_expired = $('input[name=\'filter_date_expired\']').val();
	
	if (filter_date_expired) {
		url += '&filter_date_expired=' + encodeURIComponent(filter_date_expired);
	}

	location = url;
}
</script> 
<script type="text/javascript">
   $(document).ready(function() {
		$('#form input').keydown(function(e) {
			if (e.keyCode == 13) {
				filter();
			}
		});

      	$('.date').datepicker({dateFormat: 'yy-mm-dd'});
   });
</script> 
<?php echo $footer; ?>