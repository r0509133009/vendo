<?php echo $header; ?>
<div id="content">
   <div class="breadcrumb">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
      <?php } ?>
   </div>
   <?php if ($error_warning) { ?>
   <div class="warning"><?php echo $error_warning; ?></div>
   <?php } ?>
   <div class="box">
      <div class="heading">
         <h1><img src="view/image/feed.png" alt="" /> <?php echo $heading_title; ?></h1>
         <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
      </div>
      <div class="content">
         <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
            <table class="form">
               <tr>
                  <td><span class="required">*</span> <?php echo $entry_name; ?></td>
                  <td>
                     <?php foreach ($languages as $language) { ?>                                     
                     <input type="text" name="status_text[<?php echo $language['language_id']; ?>][name]" class="form-control" value="<?php echo isset($status_text[$language['language_id']]) ? $status_text[$language['language_id']]['name'] : ''; ?>" />
                     <img src="view/image/flags/<?php echo $language['image']; ?>" alt="<?php echo $language['name']; ?>" /><br>                                  
                     <?php if (isset($error_name[$language['language_id']])) { ?>
                     <div class="error">                                     
                        <?php echo $error_name[$language['language_id']]; ?>
                     </div>
                     <?php } ?>    
                     <?php } ?> 
                  </td>
               </tr>
               <tr>
                  <td><span class="required">*</span> <?php echo $entry_text; ?></td>
                  <td>
                     <?php foreach ($languages as $language) { ?>                                     
                     <textarea name="status_text[<?php echo $language['language_id']; ?>][text]" id="" class="form-control" rows="3"><?php echo isset($status_text[$language['language_id']]) ? $status_text[$language['language_id']]['text'] : ''; ?></textarea>
                     <img src="view/image/flags/<?php echo $language['image']; ?>" alt="<?php echo $language['name']; ?>" /><br>                                  
                     <?php if (isset($error_text[$language['language_id']])) { ?>
                     <div class="error">                                     
                        <?php echo $error_text[$language['language_id']]; ?>
                     </div>
                     <?php } ?>    
                     <?php } ?> 
                  </td>
               </tr>
               <tr>
                  <td><?php echo $entry_color; ?><br><span class="help">
                     <?php echo $title_color; ?>
                     </span>
                  </td>
                  <td>
                     <input id="input_color" type="text" value="#<?php echo $color; ?>" class="colorpicker" readonly style="line-height: 30px; cursor: pointer;" />                                      
                     <input type="hidden" name="color" value="<?php echo $color; ?>">
                  </td>
               </tr>
            </table>
         </form>
      </div>
   </div>
</div>
<script>
   $(function(){
       $('.colorpicker').minicolors({
         theme: 'bootstrap',
         change: function(hex, opacity) {
           $('input[name="color"]').val(hex.replace("#", ""));
         }
       });
   });
</script>
<?php echo $footer; ?>