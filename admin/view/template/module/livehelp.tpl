<?php echo $header; ?>
<div id="content">
   <div class="breadcrumb">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
      <?php } ?>
   </div>
   <?php if ($error_warning) { ?>
   <div class="warning"><?php echo $error_warning; ?></div>
   <?php } ?>
   <div class="box">
      <div class="heading">
         <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
         <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
      </div>
      <div class="content">
         <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
            <table class="form">
               <tr>
                  <td><?php echo $entry_status; ?><br><span class="help"><?php echo $title_livehelp_status; ?></span></td>
                  <td style="vertical-align: top;"><?php if ($livehelp_status) { ?>
                     <input type="radio" name="livehelp_status" value="1" checked="checked" />
                     <?php echo $text_yes; ?>
                     <input type="radio" name="livehelp_status" value="0" />
                     <?php echo $text_no; ?>
                     <?php } else { ?>
                     <input type="radio" name="livehelp_status" value="1" />
                     <?php echo $text_yes; ?>
                     <input type="radio" name="livehelp_status" value="0" checked="checked" />
                     <?php echo $text_no; ?>
                     <?php } ?>
                  </td>
               </tr>
            </table>
            <h2><?php echo $text_livehelp_appearance; ?></h2>
            <table id="module" class="list">
               <thead>
                  <tr>
                     <td class="left"><?php echo $entry_layout; ?></td>
                     <td class="left"><?php echo $entry_heading_title; ?></td>
                     <td class="left"><?php echo $entry_status; ?></td>
                     <td></td>
                  </tr>
               </thead>
               <?php $module_row = 0; ?>
               <?php foreach ($modules as $module) { ?>
               <tbody id="module-row<?php echo $module_row; ?>">
                  <tr>
                     <td class="left">
                        <select name="livehelp_module[<?php echo $module_row; ?>][layout_id]">
                           <?php foreach ($layouts as $layout) { ?>
                           <?php if ($layout['layout_id'] == $module['layout_id']) { ?>
                           <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                           <?php } else { ?>
                           <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                           <?php } ?>
                           <?php } ?>
                        </select>
                     </td>
                     <td class="left">
                        <?php foreach ($languages as $language) { ?>
                        <input size="50" type="text" class="form-control" name="livehelp_module[<?php echo $module_row; ?>][title][<?php echo $language['language_id']; ?>]" value="<?php echo $module['title'][$language['language_id']]; ?>">
                        <img src="view/image/flags/<?php echo $language['image'] ?>" title="<?php echo $language['name']; ?>"><br>
                        <?php } ?>
                     </td>
                     <td class="left">
                        <select name="livehelp_module[<?php echo $module_row; ?>][status]">
                           <?php if ($module['status']) { ?>
                           <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                           <option value="0"><?php echo $text_disabled; ?></option>
                           <?php } else { ?>
                           <option value="1"><?php echo $text_enabled; ?></option>
                           <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                           <?php } ?>
                        </select>
                        <input type="hidden" name="livehelp_module[<?php echo $module_row; ?>][position]" value="content_bottom" />
                        <input type="hidden" name="livehelp_module[<?php echo $module_row; ?>][sort_order]" value="0" />
                     </td>
                     <td class="left"><a onclick="$('#module-row<?php echo $module_row; ?>').remove();" class="button"><?php echo $button_remove; ?></a></td>
                  </tr>
               </tbody>
               <?php $module_row++; ?>
               <?php } ?>
               <tfoot>
                  <tr>
                     <td colspan="3"></td>
                     <td class="left"><a onclick="javascript: addModule();" class="button"><?php echo $button_add_module; ?></a></td>
                  </tr>
               </tfoot>
            </table>
         </form>
      </div>
   </div>
</div>
<script type="text/javascript">
   var module_row = <?php echo $module_row; ?>;

   function addModule() {
      html = '<tbody id="module-row' + module_row + '">';
      html += '  <tr>';
      html += '    <td class="left"><select name="livehelp_module[' + module_row + '][layout_id]">';
      <?php foreach ($layouts as $layout) { ?>
      html += '      <option value="<?php echo $layout['layout_id']; ?>"><?php echo addslashes($layout['name']); ?></option>';
      <?php } ?>
      html += '    </select></td>';
      html += '    <td class="left">';
      <?php foreach ($languages as $language) { ?>
      html += '    <input size="50" type="text" name="livehelp_module[' + module_row + '][title][<?php echo $language['language_id']; ?>]" value="">';
      html += '     <img src="view/image/flags/<?php echo $language['image'] ?>" title="<?php echo $language['name']; ?>"><br>';
      <?php } ?>
      html += ' </td>';
      html += '    <td class="left"><select name="livehelp_module[' + module_row + '][status]">';
      html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
      html += '      <option value="0"><?php echo $text_disabled; ?></option>';
      html += '    </select>';
      html += '    <input type="hidden" name="livehelp_module[' + module_row + '][position]" value="content_bottom" />';
      html += '    <input type="hidden" name="livehelp_module[' + module_row + '][sort_order]" value="0" />';
      html += '   </td>';
      html += '    <td class="left"><a onclick="$(\'#module-row' + module_row + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
      html += '  </tr>';
      html += '</tbody>';
      $('#module tfoot').before(html);
      module_row++;
   }
</script>
<?php echo $footer; ?>
