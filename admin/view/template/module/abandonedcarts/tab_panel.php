<table class="table" style="margin-bottom: 0px;">
  <tr>
    <td class="col-xs-2">
    	<h5><span class="required">*</span> AbandonedCarts status:</h5>
		<span class="help">Enable or disable the module Abandoned Carts</span></td>
    <td class="col-xs-10">
    	<div class="col-md-4">
            <select name="AbandonedCarts[Enabled]" class="AbandonedCartsEnabled form-control">
              <option value="yes" <?php echo (!empty($data['AbandonedCarts']['Enabled']) && $data['AbandonedCarts']['Enabled'] == 'yes') ? 'selected=selected' : '' ?>>Enabled</option>
              <option value="no"  <?php echo (empty($data['AbandonedCarts']['Enabled']) || $data['AbandonedCarts']['Enabled']== 'no') ? 'selected=selected' : '' ?>>Disabled</option>
            </select>
        </div>
   </td>
  </tr>
  <tr>
    <td class="col-xs-2">
    	<h5>Date Format:</h5>
		<span class="help">Select date format for the end date of coupon validity</span></td>
    <td class="col-xs-10">
    	<div class="col-md-4">
            <select name="AbandonedCarts[DateFormat]" class="AbandonedCartsDateFormat form-control">
              <option value="d-m-Y" <?php echo (isset($data['AbandonedCarts']['DateFormat']) && $data['AbandonedCarts']['DateFormat'] == 'd-m-Y') ? 'selected=selected' : '' ?>>dd-mm-yyyy</option>
              <option value="m-d-Y" <?php echo (isset($data['AbandonedCarts']['DateFormat']) && $data['AbandonedCarts']['DateFormat']== 'm-d-Y') ? 'selected=selected' : '' ?>>mm-dd-yyyy</option>
              <option value="Y-m-d" <?php echo (isset($data['AbandonedCarts']['DateFormat']) && $data['AbandonedCarts']['DateFormat']== 'Y-m-d') ? 'selected=selected' : '' ?>>yyyy-mm-dd</option>
              <option value="Y-d-m" <?php echo (isset($data['AbandonedCarts']['DateFormat']) && $data['AbandonedCarts']['DateFormat']== 'Y-d-m') ? 'selected=selected' : '' ?>>yyyy-dd-mm</option>
            </select>
        </div>
   </td>
  </tr>
  <tr>
    <td class="col-xs-2">
    	<h5>Apply Taxes:</h5>
		<span class="help">Apply taxes (if any) for the products in the emails</span></td>
    <td class="col-xs-10">
    	<div class="col-md-4">
            <select name="AbandonedCarts[Taxes]" class="AbandonedCartsTaxes form-control">
              <option value="yes" <?php echo (isset($data['AbandonedCarts']['Taxes']) && $data['AbandonedCarts']['Taxes'] == 'yes') ? 'selected=selected' : '' ?>>Enabled</option>
              <option value="no" <?php echo (empty($data['AbandonedCarts']['Taxes']) || $data['AbandonedCarts']['Taxes']== 'no') ? 'selected=selected' : '' ?>>Disabled</option>
            </select>
        </div>
   </td>
  </tr>
	<tr>
      <td class="col-xs-2">
		<h5>Scheduled tasks:</h5>
		<span class="help">When activated, this function will send automatically emails to the customers who abandoned their carts.</span>
      </td>
      <td class="col-xs-10">
      	<div class="col-xs-4">
            <select id="ScheduleToggle" name="AbandonedCarts[ScheduleEnabled]" class="form-control">
              <option value="yes" <?php echo (!empty($data['AbandonedCarts']['ScheduleEnabled']) && $data['AbandonedCarts']['ScheduleEnabled'] == 'yes') ? 'selected=selected' : '' ?>>Enabled</option>
              <option value="no"  <?php echo (empty($data['AbandonedCarts']['ScheduleEnabled']) || $data['AbandonedCarts']['ScheduleEnabled']== 'no') ? 'selected=selected' : '' ?>>Disabled</option>
            </select>
        </div>
       </td>
    </tr>
</table>
<table class="table cronForm" id="mainSettings" style="border-top: 1px solid #ddd;
<? echo (!empty($data['AbandonedCarts']['ScheduleEnabled']) && $data['AbandonedCarts']['ScheduleEnabled'] == 'yes') ? '' : 'display:none;'; ?>;">
	<tr>
	  <td class="col-xs-2">
      	<h5><span class="required">*</span> Delay:</h5>
        <span class="help">After the defined days in the field the cart will be considered abandoned and can be sent a reminder</span>
      </td>
      <td class="col-xs-10">
		<div class="col-md-2">
            <div class="input-group">
				<input class="form-control" id="appendedInput" type="text" name="AbandonedCarts[Delay]" value="<?php echo (isset($data['AbandonedCarts']['Delay'])) ? $data['AbandonedCarts']['Delay'] : '3' ?>">
				<span class="input-group-addon">days</span>
            </div>
        </div>
	  </td>
	</tr>
  <tr>
  <tr>
    <td class="col-xs-2">
    	<h5><span class="required">*</span> Type:</h5>
    </td>
    <td class="col-xs-10">
    	<div class="col-md-2">
          <select name="AbandonedCarts[ScheduleType]" class="form-control">
            <option value="F" <?php if(!empty($data['AbandonedCarts']['ScheduleType']) && $data['AbandonedCarts']['ScheduleType'] == 'F') echo "selected" ?>>Fixed dates</option>
            <option value="P" <?php if(!empty($data['AbandonedCarts']['ScheduleType']) && $data['AbandonedCarts']['ScheduleType'] == 'P') echo "selected" ?>>Periodic</option>
          </select>
		</div>
	</td>
  </tr>
  <tr>
    <td class="col-xs-2">
    	<h5><span class="required">*</span> Schedule:</h5>
    </td>
    <td class="col-xs-10">
	  <div class="col-md-4">
            <div id="FixedDateOptions" class="row">
                <div class="col-md-5">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                       <input type="text" id="FixedDate" class="form-control" value="" placeholder="Date..." readonly />
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="input-group">
                       <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                    <input type="text" id="FixedDateTime" class="timepicker form-control" placeholder="Time..." readonly />
                    </div>
                </div>
                <div class="col-md-2">
                    <button class="btn addDate"><i class="fa fa-plus"></i> Add</button>
                </div>
            <div style="height:10px;clear:both;"></div>
            <div class="scrollbox dateList">
                <?php if(isset($data['AbandonedCarts']['FixedDates'])) { 
                        foreach($data['AbandonedCarts']['FixedDates'] as $date) {?>
                <div id="date<?php  $id = explode( '/', $date); $id=explode('.' , $id[0]); echo $id[0].$id[1].$id[2]; ?>"><?php echo $date ?> 
                <i class="fa fa-minus-circle removeIcon"></i>
                <input type="hidden" name="AbandonedCarts[FixedDates][]" value="<?php echo $date ?>" />
                </div>
                        <?php } } ?> 
             </div>
          </div>
          <div id="PeriodicOptions">
            <div id="CronSelector"></div>
            <input type="hidden" id="abCron" name="AbandonedCarts[PeriodicCronValue]" value="">
          </div>
      </div>
    </td>
  </tr>
  <tr>
    <td class="col-xs-2" style="vertical-align:middle;">
      <a id="TestCronAvailablity" class="btn btn-warning cronBtn"><i class="fa fa-certificate"></i> Test Cron</a>
	</td>
	<td class="col-xs-10">
	  <div class="well" style="margin-left: 15px;">
      	<i class="fa fa-question-circle"></i> If you want to use the scheduling features, your server has to support <strong>Cron</strong> functions.<br /><br />The cron daemon is a long running process that executes commands at specific dates and times. By clicking on the button <strong>Test Cron</strong> you can check if your server supports <strong>Cron</strong> commands. If your server <strong>does</strong> support Cron jobs, but this script shows that the feature is disabled, this means that the automatic creation of Cron commands is disabled. In that case, you can use this URL string - <strong>{path_to_your_site}/vendors/abandonedcarts/sendReminder.php</strong> - in your hosting config panel.
        <br /><br />
        <a id="MyHostDoesNotHaveCron" class="btn btn-info btn-mini genericCronBtn" data-toggle="modal" data-target="#genericCronModal"> My server does not support cron jobs</a>
      </div>  
    </td>
  </tr>
</table>
<!-- CronModal -->
<div class="modal" id="cronModal" tabindex="-1" role="dialog" aria-labelledby="cronModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="cronModalLabel">Schedule options & cron jobs</h4>
      </div>
      <div class="modal-body" id="cronModalBody">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- GenericCronModal -->
<div class="modal" id="genericCronModal" tabindex="-2" role="dialog" aria-labelledby="genericCronModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="genericCronModalLabel">Alternative cron solutions</h4>
      </div>
      <div class="modal-body">
        <p>If your server does not support cron jobs, you can try using services such as <strong>easycron.com</strong>, <strong>setcronjob.com</strong> or others which can provide you this feature.<br /><br />
        In order to do that, you have to register in the selected service and use this URL for execution:
        <ul>
            <li>- <?php echo HTTP_CATALOG; ?>index.php?route=module/abandonedcarts/sendReminderGeneric&amp;store_id=0</li>
        </ul>
        <span class="help">* After store_id you have to write your store ID. Your default store is with ID <strong>0</strong>.</span><br />
        You should also enable the <strong>Scheduled tasks</strong> feature in AbandonedCarts settings and set the <strong>Delay</strong> option.
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<script>
// Cron
$('.cronBtn').live('click', function(e){
	var modal = $('#cronModal'), modalBody = $('#cronModal .modal-body');
    modal
        .on('show.bs.modal', function () {
            modalBody.load('index.php?route=module/abandonedcarts/testcron&token=<?php echo $this->session->data['token']; ?>')
        })
        .modal();
    e.preventDefault();
});

// Date & Time picker
$(document).ready(function() {	
	$('#FixedDate').datepicker({ dateFormat: "dd.mm.yy" });
	$('.timepicker').timepicker();
	$('#CronSelector').cron({
		  initial: "<?php if(!empty($data['AbandonedCarts']['PeriodicCronValue'])) echo $data['AbandonedCarts']['PeriodicCronValue']; else echo "* * * * *";  ?>",
    onChange: function() {
        $('#abCron').val($(this).cron("value"));		 
    }
	});
});
	if($('select[name="AbandonedCarts[ScheduleType]"]').val() == 'P') {
		$('#FixedDateOptions').hide();
	 	$('#PeriodicOptions').show(200);
	} else {
		$('#PeriodicOptions').hide();
		$('#fixedDateOptions').show(200);	
	}
$('select[name="AbandonedCarts[ScheduleType]"]').on('change', function(e){ 
	if($(this).val() == 'P') {
		$('#FixedDateOptions').hide();
	 	$('#PeriodicOptions').show(200);	
	} else {
		$('#PeriodicOptions').hide();
		$('#FixedDateOptions').show(200);	
		}	
});
$('.btn.addDate').on('click', function(e){
		e.preventDefault();
		if($('#FixedDate').val() && $('#FixedDateTime').val() ){
		//	$('#date' + $('#FixedDate').val().replace(/\./g, '')).remove();
			$('.scrollbox.dateList').append('<div id="date' + $('#FixedDate').val().replace(/\./g,'') + '">' + $('#FixedDate').val() + '/' + $('#FixedDateTime').val() +'<i class="fa fa-minus-circle removeIcon"></i><input type="hidden" name="AbandonedCarts[FixedDates][]" value="' + $('#FixedDate').val() + '/' + $('#FixedDateTime').val() + '" /></div>');
			$('#FixedDate').val('');
			$('#FixedDateTime').val('');
		} else {
				alert('Please fill date and time!');
			}
});
$('.scrollbox.dateList div .removeIcon').live('click', function() {
	$(this).parent().remove();
});
// Hide & Show Scheduled table
$(function() {
    var $typeSelector = $('#ScheduleToggle');
    var $toggleArea = $('#mainSettings');
	 
    $typeSelector.change(function(){
        if ($typeSelector.val() === 'yes') {
            $toggleArea.show(500) 
        }
        else {
            $toggleArea.hide(500);
        }
    });
});
</script>