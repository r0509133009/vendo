<?php $this->load->model('tool/image'); ?>
<div id="SendReminderSuccess"></div>
<?php $result['customer_info'] = json_decode($result['customer_info'], true); ?>
<form id="SendReminderCustomForm">
    <input type="hidden" name="ABcart_id" value="<?php echo $_GET['id']; ?>" />
    <input type="hidden" name="AB_store" value="<?php echo $_GET['store_id']; ?>" />
    <input type="hidden" name="AB_email" id="customer_email" value="<?php if(!empty($result['customer_info']['email'])) echo $result['customer_info']['email']; ?>" />
    <table class="table" style="margin-bottom:0px;">
        <tr>
          <td class="col-xs-4"><label>To: <strong><?php if(!empty($result['customer_info']['email'])) echo $result['customer_info']['email']; ?></strong></label></td>
          <td class="col-xs-4"><label>Name: <strong><?php echo $result['customer_info']['firstname']." ". $result['customer_info']['lastname']; ?></strong></label></td>
          <td class="col-xs-4"><label>Phone: <strong><?php echo $result['customer_info']['telephone']; ?></strong></label></td>
        </tr>
    </table>
    <table class="table">
       <tr>
           <td class="col-xs-2"><h5><span class="required">* </span>Type of discount:</h5></td>
           <td class="col-xs-10">
                <div class="col-md-3">
                   <select name="DiscountType" class="form-control"> 
                       <option value="P" <?php if(!empty($data['AbandonedCarts']['DiscountType']) && $data['AbandonedCarts']['DiscountType'] == "P") echo "selected"; ?>>Percentage</option>
                       <option value="F" <?php if(!empty($data['AbandonedCarts']['DiscountType']) && $data['AbandonedCarts']['DiscountType'] == "F") echo "selected"; ?>>Fixed amount</option>
                       <option value="N" <?php if(!empty($data['AbandonedCarts']['DiscountType']) && $data['AbandonedCarts']['DiscountType'] == "N") echo "selected"; ?>>No discount</option>
                   </select>
                </div>
           </td>
       </tr>
       <tbody id="discountSettings1">
        <tr>
               <td class="col-xs-2"><h5><span class="required">* </span>Discount:</h5></td>
               <td class="col-xs-10">
                   <div class="col-md-3">
                        <div class="input-group">
                            <input type="text" class="form-control" name="Discount" value="<?php if(!empty($data['AbandonedCarts']['Discount'])) echo $data['AbandonedCarts']['Discount']?>">
                            <span class="input-group-addon">
                               <span style="display:none;" id="currencyAddon_custom"><?php echo $currency ?></span>
                               <span style="display:none;" id="percentageAddon_custom">%</span>
                           </span>
                       </div>
                  </div>
           </td>	
        </tr>
        <tr>
           <td class="col-xs-2"><h5>Total amount:</h5><span class="help">The total amount that must reached before the coupon is valid.</span></td>
           <td class="col-xs-10">
                <div class="col-md-3">
                    <div class="input-group">
                        <input type="text" class="form-control" name="TotalAmount" value="<?php if(!empty($data['AbandonedCarts']['TotalAmount'])) echo $data['AbandonedCarts']['TotalAmount']?>">
                        <span class="input-group-addon"><?php echo $currency ?></span>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="col-xs-2"><h5><span class="required">* </span>Discount validity:</h5><span class="help">Define how many days the discount code will be active after sending the reminder.</span></td>
            <td class="col-xs-10">
                <div class="col-md-3">
                    <div class="input-group">
                        <input type="text" class="form-control" value="<?php if(!empty($data['AbandonedCarts']['DiscountValidity'])) echo (int)$data['AbandonedCarts']['DiscountValidity']; else echo 7; ?>" name="DiscountValidity">
                        <span class="input-group-addon">days</span>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="col-xs-2"><h5><span class="required">* </span>Apply the discount for:</h5><span class="help">Choose the products that the discount will apply to:</span></td>
            <td class="col-xs-10">
                <div class="col-md-3">
                    <select name="DiscountApply" class="form-control"> 
                       <option value="all_products" <?php if(!empty($data['AbandonedCarts']['DiscountApply']) && $data['AbandonedCarts']['DiscountApply'] == "all_products") echo "selected"; ?>>All products in the store</option>
                       <option value="cart_products" <?php if(!empty($data['AbandonedCarts']['DiscountApply']) && $data['AbandonedCarts']['DiscountApply'] == "cart_products") echo "selected"; ?>>Products in the cart</option>
                   </select>
                </div>
            </td>
        </tr>
       </tbody>
        <tr>
            <td class="col-xs-2"><h5><span class="required">*</span> Product image dimensions:</h5></td>
            <td class="col-xs-10">
                <div class="col-md-3">
                    <div class="input-group">
                        <input class="form-control" id="appendedInput" type="text" name="ProductWidth" value="<?php echo (isset($data['AbandonedCarts']['ProductWidth'])) ? $data['AbandonedCarts']['ProductWidth'] : '60' ?>">
                      <span class="input-group-addon">px</span>
                    </div>
                    <br />
                    <div class="input-group">
                        <input class="form-control" id="appendedInput" type="text" name="ProductHeight" value="<?php echo (isset($data['AbandonedCarts']['ProductHeight'])) ? $data['AbandonedCarts']['ProductHeight'] : '60' ?>">
                        <span class="input-group-addon">px</span>
                    </div>
                </div>
           </td>
        </tr>
        <tr>
           <td class="col-xs-2"><h5><span class="required">* </span>Subject:</h5></td>
           <td class="col-xs-10">
                <div class="col-md-7">
                    <input placeholder="Mail subject" class="form-control" type="text" id="subject" name="Subject" value="<?php if (isset($result['customer_info']['language'])) echo $data['AbandonedCarts']['Subject'][$result['customer_info']['language']]; else echo $data['AbandonedCarts']['Subject'][$firstLanguageCode]; ?>" />
                </div>
           </td>
       </tr>
       <tr>
           <td class="col-xs-2"><h5><span class="required">* </span>Message to the customer:</h5><span class="help">Use the following shortcodes: <br /><br />
            {firstname} - First name<br />
            {lastname} - Last name<br />
            {cart content} - Cart content<br />
            {discount_code} - Discount code<br />
            {discount_value} - Discount code<br />
            {total_amount} - Total amount<br />
            {date_end} - End date of coupon validity</span></td> 
           <td class="col-xs-10">
               <textarea id="Message_Custom1" name="CustomMessage">
                   <?php if (isset($result['customer_info']['language'])) echo $data['AbandonedCarts']['Message'][$result['customer_info']['language']]; else echo $data['AbandonedCarts']['Message'][$firstLanguageCode]; ?>
               </textarea>
           </td>
       </tr>
       <tr>
           <td class="col-xs-2"><h5>Additional options:</h5></td>
           <td class="col-xs-10">
                <div class="col-md-12">
                    <div class="checkbox">
                        <label>
                      <input type="checkbox" name="RemoveAfterSend" value="yes" <?php echo !empty($data['AbandonedCarts']['RemoveAfterSend']) ? 'checked="checked"' : ''; ?>/> Remove the abandoned cart record after the email is sent
                        </label>
                    </div>
                </div>
           </td>
      </tr>
    </table>
</form>
<script type="text/javascript">
CKEDITOR.replace('Message_Custom1', { 
    filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $this->session->data['token']; ?>',
    filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $this->session->data['token']; ?>',
    filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $this->session->data['token']; ?>',
    filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $this->session->data['token']; ?>',
    filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $this->session->data['token']; ?>',
    filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $this->session->data['token']; ?>'
});
if($('select[name="DiscountType"]').val() == 'P'){
	$('#percentageAddon_custom').show();
} else {
	$('#currencyAddon_custom').show();
}
$('select[name="DiscountType"]').on('change', function(e){ 
	if($(this).val() == 'P') {
		$('#percentageAddon_custom').show();
		$('#currencyAddon_custom').hide();
	} else {
		$('#currencyAddon_custom').show();
		$('#percentageAddon_custom').hide();
	}
	if($(this).val() == 'N') {
		$('#discountSettings1').hide(300);
	} else {
		$('#discountSettings1').show(300);
	}	
});

if($('select[name="DiscountType"]').val() == 'N'){
	$('#discountSettings1').hide();
} else {
	$('#discountSettings1').show();
}

</script>