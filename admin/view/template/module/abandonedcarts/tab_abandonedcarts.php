<div id="abandonedCartsWrapper<?php echo $store['store_id']; ?>"> </div>
<!-- SendReminderModal -->
<div class="modal" id="sendReminderModal" tabindex="-3" role="dialog" aria-labelledby="sendReminderModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" style="width: 90% !important;margin: 20px auto;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="sendReminderModalLabel">Send reminder</h4>
      </div>
      <div class="modal-body" style="overflow:auto;" id="sendReminderModalBody">
      </div>
      <div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i> Close</button>
		<button class="btn btn-primary" id="sendMail"><i class="fa fa-envelope-square"></i> Send mail!</button>
      </div>
    </div>
  </div>
</div>
<script>
	$(window).load(function(){
		$('#sendReminderModalBody').css('height', $(window).height() * 0.72 + 'px');
    });
	$(window).resize(function() {
		$('#sendReminderModalBody').css('height', $(window).height() * 0.72 + 'px')
	})
	$('#sendMail').live('click', function(e){
		e.preventDefault();
		CKEDITOR.instances.Message_Custom1.updateElement();
	    var email_validate = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		var email = $('#customer_email').val();
		if (!email.match(email_validate)) {
			alert("The email entered by the customer is invalid.")
		} else {
			$.ajax({
				url: 'index.php?route=module/abandonedcarts/sendcustomemail&token=<?php echo $this->session->data['token']; ?>',
				type: 'post',
				data: $('#SendReminderCustomForm').serialize(),
				success: function(response) {
					 $('#sendReminderModal').modal('hide');
					 $('#messageResult').show();
					 $('#messageResult').delay(3000).fadeOut(600, function(){
						  $(this).show().css({'visibility':'hidden'}); 
					 }).slideUp(600);
				}
			});
		}
	});

    $(document).ready(function(){
         $.ajax({
            url: "index.php?route=module/abandonedcarts/getabandonedcarts&token=<?php echo $this->session->data['token']; ?>&page=1&store_id=<?php echo $store['store_id']; ?>",
            type: 'get',
            dataType: 'html',
            success: function(data) {		
                $("#abandonedCartsWrapper<?php echo $store['store_id']; ?>").html(data);
            }
    
         });
    });
	$('a[data-event-id]').live('click', function(e){
		e.preventDefault();
		e.stopPropagation();
		$('#sendReminderModalBody').load($(this).attr('href'));
		$('#sendReminderModal').modal();
	});	
	function removeItem(cartID) {      
		var r=confirm("Are you sure you want to remove this entry?");
		if (r==true) {
			$.ajax({
				url: 'index.php?route=module/abandonedcarts/removeabandonedcart&token=<?php echo $this->session->data['token']; ?>',
				type: 'post',
				data: {'cart_id': cartID},
				success: function(response) {
				location.reload();
			}
		});
	 }
	}
</script>