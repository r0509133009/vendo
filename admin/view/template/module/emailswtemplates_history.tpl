<script src="//code.jquery.com/jquery-1.9.1.js"></script> 
<?php echo $header;  ?>

<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<link href="view/javascript/summernote/summernote.css" rel="stylesheet">
<script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">	
<div id="content"> 
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
       
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  
    <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $view_history_item.' '.$_GET['id']; ?></h3>
      </div>
      <div class="panel-body">

	  
	  
	  <table  class="table">  
		 
		 <tr>      
			<td> <?php echo 'Date'; ?></td> 
			<td><input type="text" name="date" value="<?php echo $info['date']; ?>" /></td> 
		 </tr> 
		 <tr>      
			<td> <?php echo $entry_subject; ?></td> 
			<td><input type="text" name="subject" value="<?php echo $info['subject']; ?>" /></td> 
		 </tr> 
		 <tr>      
			<td> <?php echo 'To'; ?></td> 
			<td><input type="text" name="to" value="<?php echo $info['to']; ?>" /></td> 
		 </tr> 		 
		 <tr>      
		 <td> <?php echo $entry_message; ?></td>        
		 <td><textarea name="message" id="message" ><?php echo $info['content']; ?></textarea></td>  
		 </tr>    
	 </table> 	  
	 
	 
      </div>
    </div>
  </div>
  
   <script type="text/javascript"><!--
$('#message').summernote({
	height: 300
});
//--></script> 

 </div>
<?php echo $footer; ?>

  