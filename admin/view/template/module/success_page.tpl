<?php echo $header; ?><?php echo $column_left; ?>
<?php $bGljZW5zZV9kZXRhaWw = json_decode(base64_decode($license), true); ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-success-page" data-toggle="tooltip" title="<?php echo $button_save; ?>" id="button-save" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
		</div>
      <h1><?php echo $heading_title; ?></h1>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
	<?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
	<?php if (!isset($bGljZW5zZV9kZXRhaWw['status']) || !$bGljZW5zZV9kZXRhaWw['status']) { ?>
	<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> Module does not have a license key, <a onClick="$('a[href=\'#tab-support\']').click();">activate it</a> to have access to free update and support system.
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
	<?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $heading_title; ?></h3>
		<div class="pull-right"><select onChange="location.href = this.value">
	    <?php foreach ($stores as $store) { ?>
		<?php if ($store['store_id'] == $filter_store_id) { ?>
		<option value="<?php echo $store['href']; ?>" selected="selected"><?php echo $store['name']; ?></option>
		<?php } else { ?>
		<option value="<?php echo $store['href']; ?>"><?php echo $store['name']; ?></option>
		<?php } ?>
		<?php } ?>
	    </select></div>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-success-page" class="form-horizontal">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
            <li><a href="#tab-support" data-toggle="tab">Support</a></li>
          </ul>
		  <div class="tab-content">
		    <div class="tab-pane active in" id="tab-general">
			  <div class="form-group">
                <label class="col-sm-2 control-label" for="input-module-status"><?php echo $entry_module_status; ?></label>
                <div class="col-sm-4">
                  <select name="success_page[status]" id="input-module-status" class="form-control">
                    <?php if ($status == 1) { ?>
                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                    <option value="0"><?php echo $text_disabled; ?></option>
                    <?php } else { ?>
                    <option value="1"><?php echo $text_enabled; ?></option>
                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
			  <div class="form-group">
                <label class="col-sm-2 control-label s_help" for="input-coupon-id"><?php echo $entry_coupon; ?><b><?php echo $help_coupon; ?></b></label>
                <div class="col-sm-4">
                  <select name="success_page[coupon_id]" id="input-coupon-id" class="form-control">
                    <option value="0"><?php echo $text_no; ?></option>
					<?php foreach ($coupons as $coupon) { ?>
					<?php if ($coupon_id == $coupon['coupon_id']) { ?>
					<option value="<?php echo $coupon['coupon_id']; ?>" selected="selected"><?php echo $coupon['name']; ?></option>
					<?php } else { ?>
					<option value="<?php echo $coupon['coupon_id']; ?>"><?php echo $coupon['name']; ?></option>
					<?php } ?>
					<?php } ?>
                  </select>
                </div>
              </div>
			  <div class="form-group">
                <label class="col-sm-2 control-label" for="input-invoice-status"><?php echo $entry_invoice_status; ?></label>
                <div class="col-sm-4">
                  <select name="success_page[invoice_status]" id="input-invoice-status" class="form-control">
                    <?php if ($invoice_status == 1) { ?>
                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                    <option value="0"><?php echo $text_disabled; ?></option>
                    <?php } else { ?>
                    <option value="1"><?php echo $text_enabled; ?></option>
                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
			  <div class="form-group">
                <label class="col-sm-2 control-label" for="input-button-status"><?php echo $entry_button_status; ?></label>
                <div class="col-sm-4">
                  <select name="success_page[button_status]" id="input-button-status" class="form-control">
                    <?php if ($button_status == 1) { ?>
                    <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                    <option value="0"><?php echo $text_no; ?></option>
                    <?php } else { ?>
                    <option value="1"><?php echo $text_yes; ?></option>
                    <option value="0" selected="selected"><?php echo $text_no; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
			  <div class="form-group">
                <label class="col-sm-2 control-label" for="input-adwords-status"><?php echo $entry_adwords_status; ?></label>
                <div class="col-sm-4">
                  <select name="success_page[adwords_status]" id="input-adwords-status" class="form-control">
                    <?php if ($adwords_status == 1) { ?>
                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                    <option value="0"><?php echo $text_disabled; ?></option>
                    <?php } else { ?>
                    <option value="1"><?php echo $text_enabled; ?></option>
                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
			  <div class="form-group">
                <label class="col-sm-2 control-label s_help" for="input-adwords-conversion-label"><?php echo $entry_adwords_conversion_label; ?><b><?php echo $help_adwords_conversion_label; ?></b></label>
                <div class="col-sm-4">
                  <input type="text" name="success_page[adwords_conversion_label]" value="<?php echo $adwords_conversion_label; ?>" placeholder="<?php echo $entry_adwords_conversion_label; ?>" id="input-adwords-conversion-label" class="form-control" />
                </div>
              </div>
			  <div class="form-group">
                <label class="col-sm-2 control-label s_help" for="input-adwords-conversion-id"><?php echo $entry_adwords_conversion_id; ?><b><?php echo $help_adwords_conversion_id; ?></b></label>
                <div class="col-sm-4">
                  <input type="text" name="success_page[adwords_conversion_id]" value="<?php echo $adwords_conversion_id; ?>" placeholder="<?php echo $entry_adwords_conversion_id; ?>" id="input-adwords-conversion-id" class="form-control" />
                </div>
              </div>
			  <div class="form-group">
                <label class="col-sm-2 control-label" for="input-css"><?php echo $entry_css; ?></label>
                <div class="col-sm-4">
                  <textarea name="success_page[css]" rows="7" placeholder="<?php echo $entry_css; ?>" id="input-css" class="form-control"><?php echo $css; ?></textarea>
                </div>
              </div>
			  <div class="form-group">
                <label class="col-sm-2 control-label" for="input-javascript"><?php echo $entry_javascript; ?></label>
                <div class="col-sm-4">
                  <textarea name="success_page[javascript]" rows="7" placeholder="<?php echo $entry_javascript; ?>" id="input-javascript" class="form-control"><?php echo $javascript; ?></textarea>
                </div>
              </div>
			  <hr />
			  <div class="row">
                <div class="col-sm-2">
                  <ul class="nav nav-pills nav-stacked tab-switcher" id="section">
				    <?php $section_row = 0; ?>
				    <?php if (isset($section) && $section) { ?>
					<?php foreach ($section as $module) { ?>
				    <li><a href="#tab-section<?php echo $section_row; ?>" data-toggle="tab"><i class="fa fa-minus-circle"></i> <?php echo $sections[key($module)]; ?><div class="pull-right"><i class="fa fa-arrow-up" data-pos="up"></i> <i class="fa fa-arrow-down" data-pos="down"></i></div></a></li>
				    <?php $section_row++; ?>
				    <?php } ?>
					<?php } ?>
                  </ul>
                  <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <?php echo $entry_section; ?> <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu sections" role="menu">
                      <?php foreach ($sections as $key => $section_name) { ?>
                      <li><a rel="<?php echo $key; ?>"><?php echo $section_name; ?></a></li>
                      <?php }?>
                    </ul>
                  </div>
                </div>
                <div class="col-sm-10" id="tab-section">
				  <div class="tab-content">
				    <?php $section_row = 0; ?>
					<?php $for_row = 0; ?>
					<?php if (isset($section) && $section) { ?>
					<?php foreach ($section as $module) { ?>
					<?php $section_key = key($module); ?>
					<div class="tab-pane" id="tab-section<?php echo $section_row; ?>">
					  <div class="panel panel-default">
					    <div class="panel-heading">
						  <h3 class="panel-title"><?php echo $sections[$section_key]; ?></h3>
					    </div>
						<div class="panel-body">
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_status; ?></label>
						    <div class="col-sm-10">
						      <select name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][status]" id="input-value<?php echo $for_row; ?>" class="form-control">
						        <?php if ($module[$section_key]['status']) { ?>
								<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
						        <option value="0"><?php echo $text_disabled; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_enabled; ?></option>
								<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
								<?php } ?>
						      </select>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_wrapper; ?></label>
						    <div class="col-sm-10">
						      <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][wrapper]" value="<?php echo $module[$section_key]['wrapper']; ?>" placeholder="<?php echo $entry_wrapper; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_background_color; ?></label>
						    <div class="col-sm-10">
						      <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][background_color]" value="<?php echo $module[$section_key]['background_color']; ?>" placeholder="<?php echo $entry_background_color; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_padding; ?></label>
						    <div class="col-sm-10">
						      <div class="input-group">
						        <span class="input-group-addon"><?php echo $entry_top; ?></span>
						        <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][padding_top]" value="<?php echo $module[$section_key]['padding_top']; ?>" placeholder="<?php echo $entry_top; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						        <span class="input-group-addon" style="border-left: 0; border-right: 0;"><?php echo $entry_right; ?></span>
						        <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][padding_right]" value="<?php echo $module[$section_key]['padding_right']; ?>" placeholder="<?php echo $entry_right; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
								<span class="input-group-addon" style="border-left: 0; border-right: 0;"><?php echo $entry_bottom; ?></span>
						        <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][padding_bottom]" value="<?php echo $module[$section_key]['padding_bottom']; ?>" placeholder="<?php echo $entry_bottom; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
								<span class="input-group-addon" style="border-left: 0; border-right: 0;"><?php echo $entry_left; ?></span>
						        <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][padding_left]" value="<?php echo $module[$section_key]['padding_left']; ?>" placeholder="<?php echo $entry_left; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
							  </div>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_margin; ?></label>
						    <div class="col-sm-10">
						      <div class="input-group">
						        <span class="input-group-addon"><?php echo $entry_top; ?></span>
						        <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][margin_top]" value="<?php echo $module[$section_key]['margin_top']; ?>" placeholder="<?php echo $entry_top; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						        <span class="input-group-addon" style="border-left: 0; border-right: 0;"><?php echo $entry_right; ?></span>
						        <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][margin_right]" value="<?php echo $module[$section_key]['margin_right']; ?>" placeholder="<?php echo $entry_right; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
								<span class="input-group-addon" style="border-left: 0; border-right: 0;"><?php echo $entry_bottom; ?></span>
						        <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][margin_bottom]" value="<?php echo $module[$section_key]['margin_bottom']; ?>" placeholder="<?php echo $entry_bottom; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
								<span class="input-group-addon" style="border-left: 0; border-right: 0;"><?php echo $entry_left; ?></span>
						        <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][margin_left]" value="<?php echo $module[$section_key]['margin_left']; ?>" placeholder="<?php echo $entry_left; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
							  </div>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_border; ?></label>
						    <div class="col-sm-4">
						      <select name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][border_style]" id="input-value<?php echo $for_row; ?>" class="form-control">
						        <option value="0"><?php echo $text_no; ?></option>
						        <?php foreach ($lines as $line) { ?>
								<?php if ($line== $module[$section_key]['border_style']) { ?>
								<option value="<?php echo $line; ?>" selected="selected"><?php echo $line; ?></option>
								<?php } else { ?>
						        <option value="<?php echo $line; ?>"><?php echo $line; ?></option>
								<?php } ?>
						        <?php } ?>
						      </select>
						    </div>
						    <div class="col-sm-6">
						      <div class="input-group">
						        <span class="input-group-addon"><?php echo $entry_color; ?></span>
						        <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][border_color]" value="<?php echo $module[$section_key]['border_color']; ?>" placeholder="<?php echo $entry_color; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						        <span class="input-group-addon" style="border-left: 0; border-right: 0;"><?php echo $entry_size; ?></span>
						        <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][border_size]" value="<?php echo $module[$section_key]['border_size']; ?>" placeholder="<?php echo $entry_size; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						      </div>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_border_radius; ?></label>
						    <div class="col-sm-4">
						      <select name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][border_radius_status]" id="input-value<?php echo $for_row; ?>" class="form-control">
						        <?php if ($module[$section_key]['border_radius_status']) { ?>
								<option value="1" selected="selected"><?php echo $text_yes; ?></option>
						        <option value="0"><?php echo $text_no; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_yes; ?></option>
						        <option value="0" selected="selected"><?php echo $text_no; ?></option>
								<?php } ?>
						      </select>
						    </div>
						    <div class="col-sm-6">
						      <div class="input-group">
						        <span class="input-group-addon"><?php echo $entry_size; ?></span>
						        <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][border_radius_size]" value="<?php echo $module[$section_key]['border_radius_size']; ?>" placeholder="<?php echo $entry_size; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						      </div>
						    </div>
						  </div><hr />
						  <?php if ($section_key == 'text') { ?>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_logged; ?></label>
						    <div class="col-sm-10">
						      <select name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][logged_status]" id="input-value<?php echo $for_row; ?>" class="form-control">
						        <?php if ($module[$section_key]['logged_status']) { ?>
								<option value="1" selected="selected"><?php echo $text_yes; ?></option>
						        <option value="0"><?php echo $text_no; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_yes; ?></option>
						        <option value="0" selected="selected"><?php echo $text_no; ?></option>
								<?php } ?>
						      </select>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_description; ?></label>
						    <div class="col-sm-10">
						      <ul class="nav nav-tabs" id="language<?php echo $section_row; ?>">
						        <?php foreach ($languages as $language) { ?>
						        <li><a href="#language<?php echo $section_row; ?>_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></a></li>
						        <?php } ?>
						      </ul>
						      <div class="tab-content">
						        <?php foreach ($languages as $language) { ?>
						        <div class="tab-pane" id="language<?php echo $section_row; ?>_<?php echo $language['language_id']; ?>">
						          <textarea name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][<?php echo $language['language_id']; ?>][description]" placeholder="<?php echo $entry_description; ?>" id="input-description<?php echo $section_row; ?>_<?php echo $language['language_id']; ?>"><?php echo (isset($module[$section_key][$language['language_id']]['description'])) ? $module[$section_key][$language['language_id']]['description'] : ''; ?></textarea>
						        </div>
						        <?php } ?>
						      </div>
						      <div style="padding-top: 7px;"><b>{order_id}</b> - order ID <b>{invoice_id}</b> - invoice number <b>{total}</b> - amount of the order <b>{client}</b> - firstname and lastname customer <b>{customer_id}</b> - customer ID <b>{email}</b> - customer email <b>{coupon}</b> - coupon code</div>
						    </div>
						  </div>
						  <?php } else if ($section_key == 'payment') { ?>
						  <div class="form-group">
						    <div class="col-sm-2">
							  <ul class="nav nav-pills nav-stacked tab-switcher" id="payment<?php echo $section_row; ?>">
							    <?php foreach ($payments as $payment) { ?>
								<li><a href="#tab-payment<?php echo $section_row; ?>-<?php echo $payment['extension']; ?>" data-toggle="tab"><?php echo $payment['name']; ?></a></li>
								<?php } ?>
							  </ul>
						    </div>
						    <div class="col-sm-10" id="tab-section">
							  <div class="tab-content">
							    <?php foreach ($payments as $payment) { ?>
								<div class="tab-pane" id="tab-payment<?php echo $section_row; ?>-<?php echo $payment['extension']; ?>">
								  <div class="form-group">
								    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_status; ?></label>
									<div class="col-sm-10">
									  <select name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][<?php echo $payment['extension']; ?>][status]" id="input-value<?php echo $for_row; ?>" class="form-control">
									    <?php if (isset($module[$section_key][$payment['extension']]['status']) && $module[$section_key][$payment['extension']]['status']) { ?>
										<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
									    <option value="0"><?php echo $text_disabled; ?></option>
										<?php } else { ?>
										<option value="1"><?php echo $text_enabled; ?></option>
									    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
										<?php } ?>
									  </select>
									</div>
								  </div>
								  <div class="form-group">
								    <div class="col-sm-12">
									  <ul class="nav nav-tabs" id="language<?php echo $section_row; ?>">
									    <?php foreach ($languages as $language) { ?>
									    <li><a href="#language<?php echo $section_row; ?>_<?php echo $language['language_id']; ?>_<?php echo $payment['extension']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></a></li>
									    <?php } ?>
									  </ul>
									  <div class="tab-content">
									    <?php foreach ($languages as $language) { ?>
									    <div class="tab-pane" id="language<?php echo $section_row; ?>_<?php echo $language['language_id']; ?>_<?php echo $payment['extension']; ?>">
									      <textarea name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][<?php echo $payment['extension']; ?>][<?php echo $language['language_id']; ?>][description]" placeholder="<?php echo $entry_description; ?>" id="input-description<?php echo $section_row; ?>_<?php echo $language['language_id']; ?>_<?php echo $payment['extension']; ?>"><?php echo (isset($module[$section_key][$payment['extension']][$language['language_id']]['description'])) ? $module[$section_key][$payment['extension']][$language['language_id']]['description'] : ''; ?></textarea>
									    </div>
									    <?php } ?>
									  </div>
									  <div style="padding-top: 7px;"><b>{order_id}</b> - order ID <b>{invoice_id}</b> - invoice number <b>{total}</b> - amount of the order <b>{client}</b> - firstname and lastname customer <b>{customer_id}</b> - customer ID <b>{email}</b> - customer email</div>
								    </div>
								  </div>
								</div>
							    <?php } ?>
							  </div>
						    </div>
						  </div>
						  <?php } else if ($section_key == 'social') { ?>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_text_align; ?></label>
						    <div class="col-sm-10">
						      <select name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][align]" id="input-value<?php echo $for_row; ?>" class="form-control">
						        <?php foreach ($aligns as $key => $align) { ?>
								<?php if ($key == $module[$section_key]['align']) { ?>
								<option value="<?php echo $key; ?>" selected="selected"><?php echo $align; ?></option>
								<?php } else { ?>
						        <option value="<?php echo $key; ?>"><?php echo $align; ?></option>
								<?php } ?>
								<?php } ?>
						      </select>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_social_facebook; ?></label>
						    <div class="col-sm-10">
						      <select name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][social][facebook_status]" id="input-value<?php echo $for_row; ?>" class="form-control">
						        <?php if ($module[$section_key]['social']['facebook_status']) { ?>
								<option value="1" selected="selected"><?php echo $text_yes; ?></option>
						        <option value="0"><?php echo $text_no; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_yes; ?></option>
						        <option value="0" selected="selected"><?php echo $text_no; ?></option>
								<?php } ?>
						      </select>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_social_twitter; ?></label>
						    <div class="col-sm-10">
						      <select name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][social][twitter_status]" id="input-value<?php echo $for_row; ?>" class="form-control">
						        <?php if ($module[$section_key]['social']['twitter_status']) { ?>
								<option value="1" selected="selected"><?php echo $text_yes; ?></option>
						        <option value="0"><?php echo $text_no; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_yes; ?></option>
						        <option value="0" selected="selected"><?php echo $text_no; ?></option>
								<?php } ?>
						      </select>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_social_pinterest; ?></label>
						    <div class="col-sm-10">
						      <select name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][social][pinterest_status]" id="input-value<?php echo $for_row; ?>" class="form-control">
						        <?php if ($module[$section_key]['social']['pinterest_status']) { ?>
								<option value="1" selected="selected"><?php echo $text_yes; ?></option>
						        <option value="0"><?php echo $text_no; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_yes; ?></option>
						        <option value="0" selected="selected"><?php echo $text_no; ?></option>
								<?php } ?>
						      </select>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_social_googleplus; ?></label>
						    <div class="col-sm-10">
						      <select name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][social][googleplus_status]" id="input-value<?php echo $for_row; ?>" class="form-control">
						        <?php if ($module[$section_key]['social']['googleplus_status']) { ?>
								<option value="1" selected="selected"><?php echo $text_yes; ?></option>
						        <option value="0"><?php echo $text_no; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_yes; ?></option>
						        <option value="0" selected="selected"><?php echo $text_no; ?></option>
								<?php } ?>
						      </select>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_social_linkedin; ?></label>
						    <div class="col-sm-10">
						      <select name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][social][linkedin_status]" id="input-value<?php echo $for_row; ?>" class="form-control">
						        <?php if ($module[$section_key]['social']['linkedin_status']) { ?>
								<option value="1" selected="selected"><?php echo $text_yes; ?></option>
						        <option value="0"><?php echo $text_no; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_yes; ?></option>
						        <option value="0" selected="selected"><?php echo $text_no; ?></option>
								<?php } ?>
						      </select>
						    </div>
						  </div>
						  <?php } else if ($section_key == 'address') { ?>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_payment_address; ?></label>
						    <div class="col-sm-10">
						      <select name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][payment_address_status]" id="input-value<?php echo $for_row; ?>" class="form-control">
						        <?php if ($module[$section_key]['payment_address_status']) { ?>
								<option value="1" selected="selected"><?php echo $text_yes; ?></option>
						        <option value="0"><?php echo $text_no; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_yes; ?></option>
						        <option value="0" selected="selected"><?php echo $text_no; ?></option>
								<?php } ?>
						      </select>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_shipping_address; ?></label>
						    <div class="col-sm-10">
						      <select name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][shipping_address_status]" id="input-value<?php echo $for_row; ?>" class="form-control">
						        <?php if ($module[$section_key]['shipping_address_status']) { ?>
								<option value="1" selected="selected"><?php echo $text_yes; ?></option>
						        <option value="0"><?php echo $text_no; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_yes; ?></option>
						        <option value="0" selected="selected"><?php echo $text_no; ?></option>
								<?php } ?>
						      </select>
						    </div>
						  </div>
						  <div class="form-group">
						    <div class="col-sm-12">
						      <ul class="nav nav-tabs" id="language<?php echo $section_row; ?>">
						        <?php foreach ($languages as $language) { ?>
						        <li><a href="#language<?php echo $section_row; ?>_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></a></li>
						        <?php } ?>
						      </ul>
						      <div class="tab-content">
						        <?php foreach ($languages as $language) { ?>
						        <div class="tab-pane" id="language<?php echo $section_row; ?>_<?php echo $language['language_id']; ?>">
						          <div class="form-group">
						            <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_payment_title; ?></label>
						            <div class="col-sm-10">
						              <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][<?php echo $language['language_id']; ?>][payment_title]" placeholder="<?php echo $entry_payment_title; ?>" value="<?php echo (isset($module[$section_key][$language['language_id']]['payment_title'])) ? $module[$section_key][$language['language_id']]['payment_title'] : ''; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						            </div>
						          </div>
						          <div class="form-group">
						            <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_shipping_title; ?></label>
						            <div class="col-sm-10">
						              <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][<?php echo $language['language_id']; ?>][shipping_title]" placeholder="<?php echo $entry_shipping_title; ?>" value="<?php echo (isset($module[$section_key][$language['language_id']]['shipping_title'])) ? $module[$section_key][$language['language_id']]['shipping_title'] : ''; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						            </div>
						          </div>		
						        </div>
						        <?php } ?>
						      </div>
						    </div>
						  </div>
						  <?php } else if ($section_key == 'comment') { ?>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_table; ?></label>
						    <div class="col-sm-10">
						      <select name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][table_status]" id="input-value<?php echo $for_row; ?>" class="form-control">
							    <?php if ($module[$section_key]['table_status']) { ?>
								<option value="1" selected="selected"><?php echo $text_in_table; ?></option>
						        <option value="0"><?php echo $text_without_table; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_in_table; ?></option>
								<option value="0" selected="selected"><?php echo $text_without_table; ?></option>
								<?php } ?>
						      </select>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_title; ?></label>
						    <div class="col-sm-10">
						      <?php foreach ($languages as $language) { ?>
						      <div class="input-group"> <span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
						        <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][<?php echo $language['language_id']; ?>][title]" value="<?php echo (isset($module[$section_key][$language['language_id']]['title'])) ? $module[$section_key][$language['language_id']]['title'] : ''; ?>" placeholder="<?php echo $entry_title; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						      </div>
						      <?php } ?>
						    </div>
						  </div>
						  <?php } else if ($section_key == 'bank_transfer') { ?>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_table; ?></label>
						    <div class="col-sm-10">
						      <select name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][table_status]" id="input-value<?php echo $for_row; ?>" class="form-control">
							    <?php if ($module[$section_key]['table_status']) { ?>
								<option value="1" selected="selected"><?php echo $text_in_table; ?></option>
						        <option value="0"><?php echo $text_without_table; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_in_table; ?></option>
								<option value="0" selected="selected"><?php echo $text_without_table; ?></option>
								<?php } ?>
						      </select>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-2 control-label s_help" for="input-value<?php echo $for_row; ?>"><?php echo $entry_bank_transfer; ?><b><?php echo $help_bank_transfer; ?></b></label>
						    <div class="col-sm-10">
						      <select name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][instruction]" id="input-value<?php echo $for_row; ?>" class="form-control">
						        <?php if ($module[$section_key]['instruction']) { ?>
								<option value="1" selected="selected"><?php echo $text_yes; ?></option>
						        <option value="0"><?php echo $text_no; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_yes; ?></option>
						        <option value="0" selected="selected"><?php echo $text_no; ?></option>
								<?php } ?>
						      </select>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_title; ?></label>
						    <div class="col-sm-10">
						      <?php foreach ($languages as $language) { ?>
						      <div class="input-group"> <span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
						        <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][<?php echo $language['language_id']; ?>][title]" value="<?php echo (isset($module[$section_key][$language['language_id']]['title'])) ? $module[$section_key][$language['language_id']]['title'] : ''; ?>" placeholder="<?php echo $entry_title; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						      </div>
						      <?php } ?>
						    </div>
						  </div>
						  <?php } else if ($section_key == 'cart') { ?>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_image; ?></label>
						    <div class="col-sm-10">
						      <select name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][image_status]" id="input-value<?php echo $for_row; ?>" class="form-control">
						        <?php if ($module[$section_key]['image_status']) { ?>
								<option value="1" selected="selected"><?php echo $text_yes; ?></option>
						        <option value="0"><?php echo $text_no; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_yes; ?></option>
						        <option value="0" selected="selected"><?php echo $text_no; ?></option>
								<?php } ?>
						      </select>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_image_dimension; ?></label>
						    <div class="col-sm-5">
						      <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][width]" value="<?php echo $module[$section_key]['width']; ?>" placeholder="<?php echo $entry_width; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						    </div>
						    <div class="col-sm-5">
						      <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][height]" value="<?php echo $module[$section_key]['height']; ?>" placeholder="<?php echo $entry_height; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_name; ?></label>
						    <div class="col-sm-10">
						      <select name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][product_status]" id="input-value<?php echo $for_row; ?>" class="form-control">
						        <?php if ($module[$section_key]['product_status']) { ?>
								<option value="1" selected="selected"><?php echo $text_yes; ?></option>
						        <option value="0"><?php echo $text_no; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_yes; ?></option>
						        <option value="0" selected="selected"><?php echo $text_no; ?></option>
								<?php } ?>
						      </select>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_model; ?></label>
						    <div class="col-sm-10">
						      <select name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][model_status]" id="input-value<?php echo $for_row; ?>" class="form-control">
						        <?php if ($module[$section_key]['model_status']) { ?>
								<option value="1" selected="selected"><?php echo $text_yes; ?></option>
						        <option value="0"><?php echo $text_no; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_yes; ?></option>
						        <option value="0" selected="selected"><?php echo $text_no; ?></option>
								<?php } ?>
						      </select>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_sku; ?></label>
						    <div class="col-sm-10">
						      <select name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][sku_status]" id="input-value<?php echo $for_row; ?>" class="form-control">
						        <?php if ($module[$section_key]['sku_status']) { ?>
								<option value="1" selected="selected"><?php echo $text_yes; ?></option>
						        <option value="0"><?php echo $text_no; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_yes; ?></option>
						        <option value="0" selected="selected"><?php echo $text_no; ?></option>
								<?php } ?>
						      </select>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_quantity; ?></label>
						    <div class="col-sm-10">
						      <select name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][quantity_status]" id="input-value<?php echo $for_row; ?>" class="form-control">
						        <?php if ($module[$section_key]['quantity_status']) { ?>
								<option value="1" selected="selected"><?php echo $text_yes; ?></option>
						        <option value="0"><?php echo $text_no; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_yes; ?></option>
						        <option value="0" selected="selected"><?php echo $text_no; ?></option>
								<?php } ?>
						      </select>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_price; ?></label>
						    <div class="col-sm-10">
						      <select name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][price_status]" id="input-value<?php echo $for_row; ?>" class="form-control">
						        <?php if ($module[$section_key]['price_status']) { ?>
								<option value="1" selected="selected"><?php echo $text_yes; ?></option>
						        <option value="0"><?php echo $text_no; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_yes; ?></option>
						        <option value="0" selected="selected"><?php echo $text_no; ?></option>
								<?php } ?>
						      </select>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_total; ?></label>
						    <div class="col-sm-10">
						      <select name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][total_status]" id="input-value<?php echo $for_row; ?>" class="form-control">
						        <?php if ($module[$section_key]['total_status']) { ?>
								<option value="1" selected="selected"><?php echo $text_yes; ?></option>
						        <option value="0"><?php echo $text_no; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_yes; ?></option>
						        <option value="0" selected="selected"><?php echo $text_no; ?></option>
								<?php } ?>
						      </select>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_totals; ?></label>
						    <div class="col-sm-10">
						      <select name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][totals_status]" id="input-value<?php echo $for_row; ?>" class="form-control">
						        <?php if ($module[$section_key]['totals_status']) { ?>
								<option value="1" selected="selected"><?php echo $text_yes; ?></option>
						        <option value="0"><?php echo $text_no; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_yes; ?></option>
						        <option value="0" selected="selected"><?php echo $text_no; ?></option>
								<?php } ?>
						      </select>
						    </div>
						  </div>
						  <div class="form-group">
						    <div class="col-sm-12">
						      <ul class="nav nav-tabs" id="language<?php echo $section_row; ?>">
						        <?php foreach ($languages as $language) { ?>
						        <li><a href="#language<?php echo $section_row; ?>_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></a></li>
						        <?php } ?>
						      </ul>
						      <div class="tab-content">
						        <?php foreach ($languages as $language) { ?>
						        <div class="tab-pane" id="language<?php echo $section_row; ?>_<?php echo $language['language_id']; ?>">
						          <div class="form-group">
						            <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_column_image; ?></label>
						            <div class="col-sm-10">
						              <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][<?php echo $language['language_id']; ?>][column][image]" placeholder="<?php echo $entry_column_image; ?>" value="<?php echo isset($module[$section_key][$language['language_id']]['column']['image']) ? $module[$section_key][$language['language_id']]['column']['image'] : 'Image'; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						            </div>
						          </div>
						          <div class="form-group">
						            <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_column_product; ?></label>
						            <div class="col-sm-10">
						              <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][<?php echo $language['language_id']; ?>][column][product]" placeholder="<?php echo $entry_column_product; ?>" value="<?php echo isset($module[$section_key][$language['language_id']]['column']['product']) ? $module[$section_key][$language['language_id']]['column']['product'] : 'Product name'; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						            </div>
						          </div>
						          <div class="form-group">
						            <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_column_model; ?></label>
						            <div class="col-sm-10">
						              <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][<?php echo $language['language_id']; ?>][column][model]" placeholder="<?php echo $entry_column_model; ?>" value="<?php echo isset($module[$section_key][$language['language_id']]['column']['model']) ? $module[$section_key][$language['language_id']]['column']['model'] : 'Model'; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						            </div>
						          </div>
						          <div class="form-group">
						            <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_column_sku; ?></label>
						            <div class="col-sm-10">
						              <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][<?php echo $language['language_id']; ?>][column][sku]" placeholder="<?php echo $entry_column_sku; ?>" value="<?php echo isset($module[$section_key][$language['language_id']]['column']['sku']) ? $module[$section_key][$language['language_id']]['column']['sku'] : 'SKU'; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						            </div>
						          </div>
						          <div class="form-group">
						            <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_column_quantity; ?></label>
						            <div class="col-sm-10">
						              <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][<?php echo $language['language_id']; ?>][column][quantity]" placeholder="<?php echo $entry_column_quantity; ?>" value="<?php echo isset($module[$section_key][$language['language_id']]['column']['quantity']) ? $module[$section_key][$language['language_id']]['column']['quantity'] : 'Quantity'; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						            </div>
						          </div>
						          <div class="form-group">
						            <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_column_price; ?></label>
						            <div class="col-sm-10">
						              <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][<?php echo $language['language_id']; ?>][column][price]" placeholder="<?php echo $entry_column_price; ?>" value="<?php echo isset($module[$section_key][$language['language_id']]['column']['price']) ? $module[$section_key][$language['language_id']]['column']['price'] : 'Price'; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						            </div>
						          </div>
						          <div class="form-group">
						            <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_column_total; ?></label>
						            <div class="col-sm-10">
						              <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][<?php echo $language['language_id']; ?>][column][total]" placeholder="<?php echo $entry_column_total; ?>" value="<?php echo isset($module[$section_key][$language['language_id']]['column']['total']) ? $module[$section_key][$language['language_id']]['column']['total'] : 'Total'; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						            </div>
						          </div>
						        </div>
						      <?php } ?>
						      </div>
						    </div>
						  </div>
						  <?php } else if ($section_key == 'order') { ?>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_order_id; ?></label>
						    <div class="col-sm-10">
						      <select name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][order_id_status]" id="input-value<?php echo $for_row; ?>" class="form-control">
						        <?php if ($module[$section_key]['order_id_status']) { ?>
								<option value="1" selected="selected"><?php echo $text_yes; ?></option>
						        <option value="0"><?php echo $text_no; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_yes; ?></option>
						        <option value="0" selected="selected"><?php echo $text_no; ?></option>
								<?php } ?>
						      </select>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_date_added; ?></label>
						    <div class="col-sm-10">
						      <select name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][date_added_status]" id="input-value<?php echo $for_row; ?>" class="form-control">
						        <?php if ($module[$section_key]['date_added_status']) { ?>
								<option value="1" selected="selected"><?php echo $text_yes; ?></option>
						        <option value="0"><?php echo $text_no; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_yes; ?></option>
						        <option value="0" selected="selected"><?php echo $text_no; ?></option>
								<?php } ?>
						      </select>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_payment_method; ?></label>
						    <div class="col-sm-10">
						      <select name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][payment_method_status]" id="input-value<?php echo $for_row; ?>" class="form-control">
						        <?php if ($module[$section_key]['payment_method_status']) { ?>
								<option value="1" selected="selected"><?php echo $text_yes; ?></option>
						        <option value="0"><?php echo $text_no; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_yes; ?></option>
						        <option value="0" selected="selected"><?php echo $text_no; ?></option>
								<?php } ?>
						      </select>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_shipping_method; ?></label>
						    <div class="col-sm-10">
						      <select name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][shipping_method_status]" id="input-value<?php echo $for_row; ?>" class="form-control">
						        <?php if ($module[$section_key]['shipping_method_status']) { ?>
								<option value="1" selected="selected"><?php echo $text_yes; ?></option>
						        <option value="0"><?php echo $text_no; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_yes; ?></option>
						        <option value="0" selected="selected"><?php echo $text_no; ?></option>
								<?php } ?>
						      </select>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_email; ?></label>
						    <div class="col-sm-10">
						      <select name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][email_status]" id="input-value<?php echo $for_row; ?>" class="form-control">
						        <?php if ($module[$section_key]['email_status']) { ?>
								<option value="1" selected="selected"><?php echo $text_yes; ?></option>
						        <option value="0"><?php echo $text_no; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_yes; ?></option>
						        <option value="0" selected="selected"><?php echo $text_no; ?></option>
								<?php } ?>
						      </select>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_telephone; ?></label>
						    <div class="col-sm-10">
						      <select name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][telephone_status]" id="input-value<?php echo $for_row; ?>" class="form-control">
						        <?php if ($module[$section_key]['telephone_status']) { ?>
								<option value="1" selected="selected"><?php echo $text_yes; ?></option>
						        <option value="0"><?php echo $text_no; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_yes; ?></option>
						        <option value="0" selected="selected"><?php echo $text_no; ?></option>
								<?php } ?>
						      </select>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_invoice; ?></label>
						    <div class="col-sm-10">
						      <select name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][invoice_status]" id="input-value<?php echo $for_row; ?>" class="form-control">
						        <?php if ($module[$section_key]['invoice_status']) { ?>
								<option value="1" selected="selected"><?php echo $text_yes; ?></option>
						        <option value="0"><?php echo $text_no; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_yes; ?></option>
						        <option value="0" selected="selected"><?php echo $text_no; ?></option>
								<?php } ?>
						      </select>
						    </div>
						  </div>
						  <div class="form-group">
						    <div class="col-sm-12">
						      <ul class="nav nav-tabs" id="language<?php echo $section_row; ?>">
						        <?php foreach ($languages as $language) { ?>
						        <li><a href="#language<?php echo $section_row; ?>_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></a></li>
						        <?php } ?>
						      </ul>
						      <div class="tab-content">
						        <?php foreach ($languages as $language) { ?>
						        <div class="tab-pane" id="language<?php echo $section_row; ?>_<?php echo $language['language_id']; ?>">
						          <div class="form-group">
						            <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_title; ?></label>
						            <div class="col-sm-10">
						              <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][<?php echo $language['language_id']; ?>][title]" placeholder="<?php echo $entry_title; ?>" value="<?php echo isset($module[$section_key][$language['language_id']]['title']) ? $module[$section_key][$language['language_id']]['title'] : 'Order details'; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						            </div>
						          </div>
						          <div class="form-group">
						            <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_order_id; ?></label>
						            <div class="col-sm-10">
						              <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][<?php echo $language['language_id']; ?>][detail][order_id]" placeholder="<?php echo $entry_order_id; ?>" value="<?php echo isset($module[$section_key][$language['language_id']]['detail']['order_id']) ? $module[$section_key][$language['language_id']]['detail']['order_id'] : 'Order ID'; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						            </div>
						          </div>
						          <div class="form-group">
						            <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_date_added; ?></label>
						            <div class="col-sm-10">
						              <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][<?php echo $language['language_id']; ?>][detail][date_added]" placeholder="<?php echo $entry_date_added; ?>" value="<?php echo isset($module[$section_key][$language['language_id']]['detail']['date_added']) ? $module[$section_key][$language['language_id']]['detail']['date_added'] : 'Date added'; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						            </div>
						          </div>
						          <div class="form-group">
						            <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_payment_method; ?></label>
						            <div class="col-sm-10">
						              <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][<?php echo $language['language_id']; ?>][detail][payment_method]" placeholder="<?php echo $entry_payment_method; ?>" value="<?php echo isset($module[$section_key][$language['language_id']]['detail']['payment_method']) ? $module[$section_key][$language['language_id']]['detail']['payment_method'] : 'Payment method'; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						            </div>
						          </div>
						          <div class="form-group">
						            <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_shipping_method; ?></label>
						            <div class="col-sm-10">
						              <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][<?php echo $language['language_id']; ?>][detail][shipping_method]" placeholder="<?php echo $entry_shipping_method; ?>" value="<?php echo isset($module[$section_key][$language['language_id']]['detail']['shipping_method']) ? $module[$section_key][$language['language_id']]['detail']['shipping_method'] : 'Shipping method'; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						            </div>
						          </div>
						          <div class="form-group">
						            <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_email; ?></label>
						            <div class="col-sm-10">
						              <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][<?php echo $language['language_id']; ?>][detail][email]" placeholder="<?php echo $entry_email; ?>" value="<?php echo isset($module[$section_key][$language['language_id']]['detail']['email']) ? $module[$section_key][$language['language_id']]['detail']['email'] : 'Email'; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						            </div>
						          </div>
								  <div class="form-group">
						            <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_telephone; ?></label>
						            <div class="col-sm-10">
						              <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][<?php echo $language['language_id']; ?>][detail][telephone]" placeholder="<?php echo $entry_telephone; ?>" value="<?php echo isset($module[$section_key][$language['language_id']]['detail']['telephone']) ? $module[$section_key][$language['language_id']]['detail']['telephone'] : 'Telephone'; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						            </div>
						          </div>
								  <div class="form-group">
						            <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_invoice; ?></label>
						            <div class="col-sm-10">
						              <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][<?php echo $language['language_id']; ?>][detail][invoice]" placeholder="<?php echo $entry_invoice; ?>" value="<?php echo isset($module[$section_key][$language['language_id']]['detail']['invoice']) ? $module[$section_key][$language['language_id']]['detail']['invoice'] : 'Invoice No.'; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						            </div>
						          </div>
						        </div>
						      <?php } ?>
						      </div>
						    </div>
						  </div>
						  <?php } else if ($section_key == 'product') { ?>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_product; ?></label>
							<div class="col-sm-10">
							  <div style="position: relative;">
							    <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][autocomplete]" value="" placeholder="<?php echo $entry_product; ?>" data-row="<?php echo $section_row; ?>" data-element="product" id="input-value<?php echo $for_row; ?>" class="form-control" />
							  </div>
							  <div id="product<?php echo $section_row; ?>" class="well well-sm" style="height: 150px; overflow: auto;">
							    <?php if (isset($module[$section_key]['product']) && $module[$section_key]['product']) { ?>
							    <?php foreach ($module[$section_key]['product'] as $product) { ?>
							    <div id="product<?php echo $section_row; ?>_<?php echo $product['product_id']; ?>"><i class="fa fa-minus-circle" onClick="$('#product<?php echo $section_row; ?>_<?php echo $product['product_id']; ?>').remove();"></i> <?php echo $product['name']; ?><input type="hidden" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][product][]" value="<?php echo $product['product_id']; ?>" /></div>
							    <?php } ?>
							    <?php } ?>
							  </div>
							</div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_title_font; ?></label>
						    <div class="col-sm-10">
						      <div class="input-group">
						        <span class="input-group-addon"><?php echo $entry_size; ?></span>
						        <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][size]" value="<?php echo $module[$section_key]['size']; ?>" placeholder="<?php echo $entry_size; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						        <span class="input-group-addon" style="border-left: 0; border-right: 0;"><?php echo $entry_color; ?></span>
						        <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][color]" value="<?php echo $module[$section_key]['color']; ?>" placeholder="<?php echo $entry_color; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						        <span class="input-group-addon" style="border-left: 0; border-right: 0;"><?php echo $entry_weight; ?></span>
						        <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][weight]" value="<?php echo $module[$section_key]['weight']; ?>" placeholder="<?php echo $entry_weight; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						      </div>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_title; ?></label>
						    <div class="col-sm-10">
						      <?php foreach ($languages as $language) { ?>
						      <div class="input-group"> <span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
						        <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][<?php echo $language['language_id']; ?>][title]" value="<?php echo (isset($module[$section_key][$language['language_id']]['title'])) ? $module[$section_key][$language['language_id']]['title'] : ''; ?>" placeholder="<?php echo $entry_title; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						      </div>
						      <?php } ?>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_button_cart; ?></label>
						    <div class="col-sm-10">
						      <?php foreach ($languages as $language) { ?>
						      <div class="input-group"> <span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
						        <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][<?php echo $language['language_id']; ?>][button]" value="<?php echo (isset($module[$section_key][$language['language_id']]['button'])) ? $module[$section_key][$language['language_id']]['button'] : ''; ?>" placeholder="<?php echo $entry_button_cart; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						      </div>
						      <?php } ?>
						    </div>
						  </div>
						  <?php } else if ($section_key == 'facebook') { ?>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_facebook_url; ?></label>
						    <div class="col-sm-10">
						      <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][url]" value="<?php echo $module[$section_key]['url']; ?>" placeholder="<?php echo $entry_facebook_url; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_facebook_dimension; ?></label>
						    <div class="col-sm-5">
						      <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][width]" value="<?php echo $module[$section_key]['width']; ?>" placeholder="<?php echo $entry_width; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						    </div>
						    <div class="col-sm-5">
						      <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][height]" value="<?php echo $module[$section_key]['height']; ?>" placeholder="<?php echo $entry_height; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						    </div>
						  </div>
						  <?php } else if ($section_key == 'youtube') { ?>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_youtube_url; ?></label>
						    <div class="col-sm-10">
						      <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][url]" value="<?php echo $module[$section_key]['url']; ?>" placeholder="<?php echo $entry_youtube_url; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_youtube_dimension; ?></label>
						    <div class="col-sm-5">
						      <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][width]" value="<?php echo $module[$section_key]['width']; ?>" placeholder="<?php echo $entry_width; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						    </div>
						    <div class="col-sm-5">
						      <input type="text" name="success_page[section][<?php echo $section_row; ?>][<?php echo $section_key; ?>][height]" value="<?php echo $module[$section_key]['height']; ?>" placeholder="<?php echo $entry_height; ?>" id="input-value<?php echo $for_row; ?>" class="form-control" />
						    </div>
						  </div>
						  <?php } ?>
						</div>
					  </div>
				    </div>
					<?php $section_row++; ?>
					<?php $for_row++; ?>
					<?php } ?>
					<?php } ?>
				  </div>
                </div>
              </div>
			</div>
			<div class="tab-pane" id="tab-support">
			  <div id="rspLicense"></div>
			  <h3 style="margin-bottom: 25px;"><b>Your license</b></h3>
			  <?php if (isset($bGljZW5zZV9kZXRhaWw['status']) && $bGljZW5zZV9kZXRhaWw['status']) { ?>
			  <table class="table">
			    <tbody>
				  <tr>
				    <td style="width: 150px;">License key:</td>
					<td><span><?php echo $license_key; ?></span><input type="hidden" name="success_page[license]" class="license_detail form-control" value="<?php echo $license; ?>" />
				      <input type="text" name="success_page[license_key]" class="license_key form-control" style="display: none!important;" value="<?php echo $license_key; ?>" /> <a id="re-actLicense" class="btn btn-success">Re-activate</a></td>
				  </tr>
				  <tr>
				    <td>License for:</td>
					<td><?php echo $bGljZW5zZV9kZXRhaWw['customer']; ?>, <?php echo $bGljZW5zZV9kZXRhaWw['domain']; ?></td>
				  </tr>
				</tbody>
			  </table>
			  <?php } else { ?>
			  <div class="form-group">
                <label class="col-sm-2 control-label" for="input-enter-license-key">Enter your license key:</label>
                <div class="col-xs-4">
                  <input type="text" name="success_page[license_key]" value="<?php echo $license_key; ?>" placeholder="xxxxx-xxxxx-xxxxx-xxxxx-xxxxx" id="input-enter-license-key" class="license_key form-control" />
                </div>
				<div class="col-xs-6">
                  <a id="actLicense" class="btn btn-success">Activate</a>
				</div>
              </div>
			  <?php } ?>
			  <br /><br /><br />
			  If you not have a license key? <a onclick="window.open('http://www.adikon.eu/login')">Click here</a>.<br />
			  Need help? Use our the <a onclick="window.open('http://www.adikon.eu/support')">support system</a>.<br /><br />
			  Adikon.eu, All Rights Reserved.
			  <script type="text/javascript">
			  var mod_id = '2578';
			  var domain = '<?php echo base64_encode((!empty($_SERVER['HTTP_HOST'])) ? $_SERVER['HTTP_HOST'] : (defined('HTTP_SERVER') ? HTTP_SERVER : '')); ?>';
			  </script>
			  <script type="text/javascript" src="//www.adikon.eu/verify/"></script>
			</div>
		  </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
var section_row = <?php echo $section_row; ?>;
var for_row = <?php echo $for_row; ?>;

$("ul.sections li a").on('click', function(event) {
	event.preventDefault();

	var section_key = $(this).attr('rel');

	html = '<div class="tab-pane" id="tab-section' + section_row + '">';
	html += '  <div class="panel panel-default">';
    html += '    <div class="panel-heading">';
    html += '      <h3 class="panel-title">' + $(this).text() + '</h3>';
    html += '    </div>';
    html += '    <div class="panel-body">';
	html += '	   <div class="form-group">';
	html += '	     <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_status; ?></label>';
	html += '	     <div class="col-sm-10">';
	html += '	       <select name="success_page[section][' + section_row + '][' + section_key + '][status]" id="input-value' + for_row + '" class="form-control">';
	html += '	         <option value="1"><?php echo $text_enabled; ?></option>';
	html += '	         <option value="0"><?php echo $text_disabled; ?></option>';
	html += '	       </select>';
	html += '	     </div>';
	html += '	   </div>';
	html += '	   <div class="form-group">';
	html += '	     <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_wrapper; ?></label>';
	html += '	     <div class="col-sm-10">';
	html += '	       <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][wrapper]" value="100%" placeholder="<?php echo $entry_wrapper; ?>" id="input-value' + for_row + '" class="form-control" />';
	html += '	     </div>';
	html += '	   </div>';
	html += '	   <div class="form-group">';
	html += '	     <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_background_color; ?></label>';
	html += '	     <div class="col-sm-10">';
	html += '	       <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][background_color]" value="" placeholder="<?php echo $entry_background_color; ?>" id="input-value' + for_row + '" class="form-control" />';
	html += '	     </div>';
	html += '	   </div>';
	html += '	   <div class="form-group">';
	html += '	     <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_padding; ?></label>';
	html += '	     <div class="col-sm-10">';
	html += '	       <div class="input-group">';
	html += '	         <span class="input-group-addon"><?php echo $entry_top; ?></span>';
	html += '	         <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][padding_top]" value="12px" placeholder="<?php echo $entry_top; ?>" id="input-value' + for_row + '" class="form-control" />';
	html += '	         <span class="input-group-addon" style="border-left: 0; border-right: 0;"><?php echo $entry_right; ?></span>';
	html += '	         <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][padding_right]" value="15px" placeholder="<?php echo $entry_right; ?>" id="input-value' + for_row + '" class="form-control" />';
	html += '	         <span class="input-group-addon" style="border-left: 0; border-right: 0;"><?php echo $entry_bottom; ?></span>';
	html += '	         <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][padding_bottom]" value="12px" placeholder="<?php echo $entry_bottom; ?>" id="input-value' + for_row + '" class="form-control" />';
	html += '	         <span class="input-group-addon" style="border-left: 0; border-right: 0;"><?php echo $entry_left; ?></span>';
	html += '	         <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][padding_left]" value="15px" placeholder="<?php echo $entry_left; ?>" id="input-value' + for_row + '" class="form-control" />';
	html += '	       </div>';
	html += '	     </div>';
	html += '	   </div>';
	html += '	   <div class="form-group">';
	html += '	     <label class="col-sm-2 control-label" for="input-value<?php echo $for_row; ?>"><?php echo $entry_margin; ?></label>';
	html += '	     <div class="col-sm-10">';
	html += '	       <div class="input-group">';
	html += '	         <span class="input-group-addon"><?php echo $entry_top; ?></span>';
	html += '	         <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][margin_top]" value="0px" placeholder="<?php echo $entry_top; ?>" id="input-value' + for_row + '" class="form-control" />';
	html += '	         <span class="input-group-addon" style="border-left: 0; border-right: 0;"><?php echo $entry_right; ?></span>';
	html += '	         <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][margin_right]" value="0px" placeholder="<?php echo $entry_right; ?>" id="input-value' + for_row + '" class="form-control" />';
	html += '	         <span class="input-group-addon" style="border-left: 0; border-right: 0;"><?php echo $entry_bottom; ?></span>';
	html += '	         <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][margin_bottom]" value="20px" placeholder="<?php echo $entry_bottom; ?>" id="input-value' + for_row + '" class="form-control" />';
	html += '	         <span class="input-group-addon" style="border-left: 0; border-right: 0;"><?php echo $entry_left; ?></span>';
	html += '	         <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][margin_left]" value="0px" placeholder="<?php echo $entry_left; ?>" id="input-value' + for_row + '" class="form-control" />';
	html += '	       </div>';
	html += '	     </div>';
	html += '	   </div>';
	html += '	   <div class="form-group">';
	html += '	     <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_border; ?></label>';
	html += '	     <div class="col-sm-4">';
	html += '	       <select name="success_page[section][' + section_row + '][' + section_key + '][border_style]" id="input-value' + for_row + '" class="form-control">';
	html += '	         <option value="0"><?php echo $text_no; ?></option>';
	<?php foreach ($lines as $line) { ?>
	html += '	         <option value="<?php echo $line; ?>"><?php echo $line; ?></option>';
	<?php } ?>
	html += '	       </select>';
	html += '	     </div>';
	html += '	     <div class="col-sm-6">';
	html += '	       <div class="input-group">';
	html += '	         <span class="input-group-addon"><?php echo $entry_color; ?></span>';
	html += '	         <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][border_color]" value="#dddddd" placeholder="<?php echo $entry_color; ?>" id="input-value' + for_row + '" class="form-control" />';
	html += '	         <span class="input-group-addon" style="border-left: 0; border-right: 0;"><?php echo $entry_size; ?></span>';
	html += '	         <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][border_size]" value="1px" placeholder="<?php echo $entry_size; ?>" id="input-value' + for_row + '" class="form-control" />';
	html += '	       </div>';
	html += '	     </div>';
	html += '	   </div>';
	html += '	   <div class="form-group">';
	html += '	     <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_border_radius; ?></label>';
	html += '	     <div class="col-sm-4">';
	html += '	       <select name="success_page[section][' + section_row + '][' + section_key + '][border_radius_status]" id="input-value' + for_row + '" class="form-control">';
	html += '	         <option value="1"><?php echo $text_yes; ?></option>';
	html += '	         <option value="0" selected><?php echo $text_no; ?></option>';
	html += '	       </select>';
	html += '	     </div>';
	html += '	     <div class="col-sm-6">';
	html += '	       <div class="input-group">';
	html += '	         <span class="input-group-addon"><?php echo $entry_size; ?></span>';
	html += '	         <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][border_radius_size]" value="3px" placeholder="<?php echo $entry_size; ?>" id="input-value' + for_row + '" class="form-control" />';
	html += '	       </div>';
	html += '	     </div>';
	html += '	   </div><hr />';

	if (section_key == 'text') {
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_logged; ?></label>';
		html += '    <div class="col-sm-10">';
		html += '      <select name="success_page[section][' + section_row + '][' + section_key + '][logged_status]" id="input-value' + for_row + '" class="form-control">';
		html += '        <option value="1"><?php echo $text_yes; ?></option>';
		html += '        <option value="0" selected><?php echo $text_no; ?></option>';
		html += '      </select>';
		html += '    </div>';
		html += '  </div>';
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_description; ?></label>';
		html += '    <div class="col-sm-10">';
		html += '      <ul class="nav nav-tabs" id="language' + section_row + '">';
		<?php foreach ($languages as $language) { ?>
		html += '        <li><a href="#language' + section_row + '_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></a></li>';
		<?php } ?>
		html += '      </ul>';
		html += '      <div class="tab-content">';
		<?php foreach ($languages as $language) { ?>
		html += '        <div class="tab-pane" id="language' + section_row + '_<?php echo $language['language_id']; ?>">';
		html += '          <textarea name="success_page[section][' + section_row + '][' + section_key + '][<?php echo $language['language_id']; ?>][description]" placeholder="<?php echo $entry_description; ?>" id="input-description' + section_row + '_<?php echo $language['language_id']; ?>"></textarea>';
		html += '        </div>';
		<?php } ?>
		html += '      </div>';
		html += '      <div style="padding-top: 7px;"><b>{order_id}</b> - order ID <b>{invoice_id}</b> - invoice number <b>{total}</b> - amount of the order <b>{client}</b> - firstname and lastname customer <b>{customer_id}</b> - customer ID <b>{email}</b> - customer email <b>{coupon}</b> - coupon code</div>';
		html += '    </div>';
		html += '  </div>';
	} else if (section_key == 'payment') {
		html += '  <div class="form-group">';
		html += '    <div class="col-sm-2">';
		html += '      <ul class="nav nav-pills nav-stacked tab-switcher" id="payment' + section_row + '">';
		<?php foreach ($payments as $payment) { ?>
		html += '        <li><a href="#tab-payment' + section_row + '-<?php echo $payment['extension']; ?>" data-toggle="tab"><?php echo $payment['name']; ?></a></li>';
		<?php } ?>
		html += '      </ul>';
		html += '    </div>';
		html += '    <div class="col-sm-10" id="tab-section">';
		html += '      <div class="tab-content">';
		<?php foreach ($payments as $payment) { ?>
		html += '        <div class="tab-pane" id="tab-payment' + section_row + '-<?php echo $payment['extension']; ?>">';
		html += '          <div class="form-group">';
		html += '            <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_status; ?></label>';
		html += '            <div class="col-sm-10">';
		html += '              <select name="success_page[section][' + section_row + '][' + section_key + '][<?php echo $payment['extension']; ?>][status]" id="input-value' + for_row + '" class="form-control">';
		html += '                <option value="1"><?php echo $text_enabled; ?></option>';
		html += '                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>';
		html += '              </select>';
		html += '            </div>';
		html += '          </div>';
		html += '          <div class="form-group">';
		html += '            <div class="col-sm-12">';
		html += '              <ul class="nav nav-tabs" id="language' + section_row + '">';
		<?php foreach ($languages as $language) { ?>
		html += '                <li><a href="#language' + section_row + '_<?php echo $language['language_id']; ?>_<?php echo $payment['extension']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></a></li>';
		<?php } ?>
		html += '              </ul>';
		html += '              <div class="tab-content">';
		<?php foreach ($languages as $language) { ?>
		html += '              <div class="tab-pane" id="language' + section_row + '_<?php echo $language['language_id']; ?>_<?php echo $payment['extension']; ?>">';
		html += '                <textarea name="success_page[section][' + section_row + '][' + section_key + '][<?php echo $payment['extension']; ?>][<?php echo $language['language_id']; ?>][description]" placeholder="<?php echo $entry_description; ?>" id="input-description' + section_row + '_<?php echo $language['language_id']; ?>_<?php echo $payment['extension']; ?>"></textarea>';
		html += '              </div>';
		<?php } ?>
		html += '              </div>';
		html += '              <div style="padding-top: 7px;"><b>{order_id}</b> - order ID <b>{invoice_id}</b> - invoice number <b>{total}</b> - amount of the order <b>{client}</b> - firstname and lastname customer <b>{customer_id}</b> - customer ID <b>{email}</b> - customer email</div>';
		html += '            </div>';
		html += '          </div>';
		html += '        </div>';
		<?php } ?>
		html += '      </div>';
		html += '    </div>';
		html += '  </div>';
	} else if (section_key == 'social') {
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_text_align; ?></label>';
		html += '    <div class="col-sm-10">';
		html += '      <select name="success_page[section][' + section_row + '][' + section_key + '][align]" id="input-value' + for_row + '" class="form-control">';
		<?php foreach ($aligns as $key => $align) { ?>
		html += '        <option value="<?php echo $key; ?>"><?php echo $align; ?></option>';
		<?php } ?>
		html += '      </select>';
		html += '    </div>';
		html += '  </div>';
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_social_facebook; ?></label>';
		html += '    <div class="col-sm-10">';
		html += '      <select name="success_page[section][' + section_row + '][' + section_key + '][social][facebook_status]" id="input-value' + for_row + '" class="form-control">';
		html += '        <option value="1"><?php echo $text_yes; ?></option>';
		html += '        <option value="0"><?php echo $text_no; ?></option>';
		html += '      </select>';
		html += '    </div>';
		html += '  </div>';
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_social_twitter; ?></label>';
		html += '    <div class="col-sm-10">';
		html += '      <select name="success_page[section][' + section_row + '][' + section_key + '][social][twitter_status]" id="input-value' + for_row + '" class="form-control">';
		html += '        <option value="1"><?php echo $text_yes; ?></option>';
		html += '        <option value="0"><?php echo $text_no; ?></option>';
		html += '      </select>';
		html += '    </div>';
		html += '  </div>';
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_social_pinterest; ?></label>';
		html += '    <div class="col-sm-10">';
		html += '      <select name="success_page[section][' + section_row + '][' + section_key + '][social][pinterest_status]" id="input-value' + for_row + '" class="form-control">';
		html += '        <option value="1"><?php echo $text_yes; ?></option>';
		html += '        <option value="0"><?php echo $text_no; ?></option>';
		html += '      </select>';
		html += '    </div>';
		html += '  </div>';
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_social_googleplus; ?></label>';
		html += '    <div class="col-sm-10">';
		html += '      <select name="success_page[section][' + section_row + '][' + section_key + '][social][googleplus_status]" id="input-value' + for_row + '" class="form-control">';
		html += '        <option value="1"><?php echo $text_yes; ?></option>';
		html += '        <option value="0"><?php echo $text_no; ?></option>';
		html += '      </select>';
		html += '    </div>';
		html += '  </div>';
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_social_linkedin; ?></label>';
		html += '    <div class="col-sm-10">';
		html += '      <select name="success_page[section][' + section_row + '][' + section_key + '][social][linkedin_status]" id="input-value' + for_row + '" class="form-control">';
		html += '        <option value="1"><?php echo $text_yes; ?></option>';
		html += '        <option value="0"><?php echo $text_no; ?></option>';
		html += '      </select>';
		html += '    </div>';
		html += '  </div>';
	} else if (section_key == 'address') {
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_payment_address; ?></label>';
		html += '    <div class="col-sm-10">';
		html += '      <select name="success_page[section][' + section_row + '][' + section_key + '][payment_address_status]" id="input-value' + for_row + '" class="form-control">';
		html += '        <option value="1"><?php echo $text_yes; ?></option>';
		html += '        <option value="0"><?php echo $text_no; ?></option>';
		html += '      </select>';
		html += '    </div>';
		html += '  </div>';
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_shipping_address; ?></label>';
		html += '    <div class="col-sm-10">';
		html += '      <select name="success_page[section][' + section_row + '][' + section_key + '][shipping_address_status]" id="input-value' + for_row + '" class="form-control">';
		html += '        <option value="1"><?php echo $text_yes; ?></option>';
		html += '        <option value="0"><?php echo $text_no; ?></option>';
		html += '      </select>';
		html += '    </div>';
		html += '  </div>';
		html += '  <div class="form-group">';
		html += '    <div class="col-sm-12">';
		html += '      <ul class="nav nav-tabs" id="language' + section_row + '">';
		<?php foreach ($languages as $language) { ?>
		html += '        <li><a href="#language' + section_row + '_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></a></li>';
		<?php } ?>
		html += '      </ul>';
		html += '      <div class="tab-content">';
		<?php foreach ($languages as $language) { ?>
		html += '        <div class="tab-pane" id="language' + section_row + '_<?php echo $language['language_id']; ?>">';
		html += '          <div class="form-group">';
		html += '            <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_payment_title; ?></label>';
		html += '            <div class="col-sm-10">';
		html += '              <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][<?php echo $language['language_id']; ?>][payment_title]" placeholder="<?php echo $entry_payment_title; ?>" value="Payment address" id="input-value' + for_row + '" class="form-control" />';
		html += '            </div>';
		html += '          </div>';
		html += '          <div class="form-group">';
		html += '            <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_shipping_title; ?></label>';
		html += '            <div class="col-sm-10">';
		html += '              <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][<?php echo $language['language_id']; ?>][shipping_title]" placeholder="<?php echo $entry_shipping_title; ?>" value="Shipping address" id="input-value' + for_row + '" class="form-control" />';
		html += '            </div>';
		html += '          </div>';		
		html += '        </div>';
		<?php } ?>
		html += '      </div>';
		html += '    </div>';
		html += '  </div>';
	} else if (section_key == 'comment') {
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_table; ?></label>';
		html += '    <div class="col-sm-10">';
		html += '      <select name="success_page[section][' + section_row + '][' + section_key + '][table_status]" id="input-value' + for_row + '" class="form-control">';
		html += '        <option value="1"><?php echo $text_in_table; ?></option>';
		html += '        <option value="0"><?php echo $text_without_table; ?></option>';
		html += '      </select>';
		html += '    </div>';
		html += '  </div>';
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_title; ?></label>';
		html += '    <div class="col-sm-10">';
		<?php foreach ($languages as $language) { ?>
		html += '      <div class="input-group"> <span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>';
		html += '        <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][<?php echo $language['language_id']; ?>][title]" value="Comment" placeholder="<?php echo $entry_title; ?>" id="input-value' + for_row + '" class="form-control" />';
		html += '      </div>';
		<?php } ?>
		html += '    </div>';
		html += '  </div>';
	} else if (section_key == 'bank_transfer') {
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_table; ?></label>';
		html += '    <div class="col-sm-10">';
		html += '      <select name="success_page[section][' + section_row + '][' + section_key + '][table_status]" id="input-value' + for_row + '" class="form-control">';
		html += '        <option value="1"><?php echo $text_in_table; ?></option>';
		html += '        <option value="0"><?php echo $text_without_table; ?></option>';
		html += '      </select>';
		html += '    </div>';
		html += '  </div>';
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label s_help" for="input-value' + for_row + '"><?php echo $entry_bank_transfer; ?><b><?php echo $help_bank_transfer; ?></b></label>';
		html += '    <div class="col-sm-10">';
		html += '      <select name="success_page[section][' + section_row + '][' + section_key + '][instruction]" id="input-value' + for_row + '" class="form-control">';
		html += '        <option value="1"><?php echo $text_yes; ?></option>';
		html += '        <option value="0"><?php echo $text_no; ?></option>';
		html += '      </select>';
		html += '    </div>';
		html += '  </div>';
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_title; ?></label>';
		html += '    <div class="col-sm-10">';
		<?php foreach ($languages as $language) { ?>
		html += '      <div class="input-group"> <span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>';
		html += '        <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][<?php echo $language['language_id']; ?>][title]" value="Bank transfer instructions" placeholder="<?php echo $entry_title; ?>" id="input-value' + for_row + '" class="form-control" />';
		html += '      </div>';
		<?php } ?>
		html += '    </div>';
		html += '  </div>';
	} else if (section_key == 'cart') {
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_image; ?></label>';
		html += '    <div class="col-sm-10">';
		html += '      <select name="success_page[section][' + section_row + '][' + section_key + '][image_status]" id="input-value' + for_row + '" class="form-control">';
		html += '        <option value="1"><?php echo $text_yes; ?></option>';
		html += '        <option value="0"><?php echo $text_no; ?></option>';
		html += '      </select>';
		html += '    </div>';
		html += '  </div>';
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_image_dimension; ?></label>';
		html += '    <div class="col-sm-5">';
		html += '      <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][width]" value="80px" placeholder="<?php echo $entry_width; ?>" id="input-value' + for_row + '" class="form-control" />';
		html += '    </div>';
		html += '    <div class="col-sm-5">';
		html += '      <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][height]" value="80px" placeholder="<?php echo $entry_height; ?>" id="input-value' + for_row + '" class="form-control" />';
		html += '    </div>';
		html += '  </div>';
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_name; ?></label>';
		html += '    <div class="col-sm-10">';
		html += '      <select name="success_page[section][' + section_row + '][' + section_key + '][product_status]" id="input-value' + for_row + '" class="form-control">';
		html += '        <option value="1"><?php echo $text_yes; ?></option>';
		html += '        <option value="0"><?php echo $text_no; ?></option>';
		html += '      </select>';
		html += '    </div>';
		html += '  </div>';
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_model; ?></label>';
		html += '    <div class="col-sm-10">';
		html += '      <select name="success_page[section][' + section_row + '][' + section_key + '][model_status]" id="input-value' + for_row + '" class="form-control">';
		html += '        <option value="1"><?php echo $text_yes; ?></option>';
		html += '        <option value="0"><?php echo $text_no; ?></option>';
		html += '      </select>';
		html += '    </div>';
		html += '  </div>';
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_sku; ?></label>';
		html += '    <div class="col-sm-10">';
		html += '      <select name="success_page[section][' + section_row + '][' + section_key + '][sku_status]" id="input-value' + for_row + '" class="form-control">';
		html += '        <option value="1"><?php echo $text_yes; ?></option>';
		html += '        <option value="0"><?php echo $text_no; ?></option>';
		html += '      </select>';
		html += '    </div>';
		html += '  </div>';
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_quantity; ?></label>';
		html += '    <div class="col-sm-10">';
		html += '      <select name="success_page[section][' + section_row + '][' + section_key + '][quantity_status]" id="input-value' + for_row + '" class="form-control">';
		html += '        <option value="1"><?php echo $text_yes; ?></option>';
		html += '        <option value="0"><?php echo $text_no; ?></option>';
		html += '      </select>';
		html += '    </div>';
		html += '  </div>';
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_price; ?></label>';
		html += '    <div class="col-sm-10">';
		html += '      <select name="success_page[section][' + section_row + '][' + section_key + '][price_status]" id="input-value' + for_row + '" class="form-control">';
		html += '        <option value="1"><?php echo $text_yes; ?></option>';
		html += '        <option value="0"><?php echo $text_no; ?></option>';
		html += '      </select>';
		html += '    </div>';
		html += '  </div>';
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_total; ?></label>';
		html += '    <div class="col-sm-10">';
		html += '      <select name="success_page[section][' + section_row + '][' + section_key + '][total_status]" id="input-value' + for_row + '" class="form-control">';
		html += '        <option value="1"><?php echo $text_yes; ?></option>';
		html += '        <option value="0"><?php echo $text_no; ?></option>';
		html += '      </select>';
		html += '    </div>';
		html += '  </div>';
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_totals; ?></label>';
		html += '    <div class="col-sm-10">';
		html += '      <select name="success_page[section][' + section_row + '][' + section_key + '][totals_status]" id="input-value' + for_row + '" class="form-control">';
		html += '        <option value="1"><?php echo $text_yes; ?></option>';
		html += '        <option value="0"><?php echo $text_no; ?></option>';
		html += '      </select>';
		html += '    </div>';
		html += '  </div>';
		html += '  <div class="form-group">';
		html += '    <div class="col-sm-12">';
		html += '      <ul class="nav nav-tabs" id="language' + section_row + '">';
		<?php foreach ($languages as $language) { ?>
		html += '        <li><a href="#language' + section_row + '_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></a></li>';
		<?php } ?>
		html += '      </ul>';
		html += '      <div class="tab-content">';
		<?php foreach ($languages as $language) { ?>
		html += '        <div class="tab-pane" id="language' + section_row + '_<?php echo $language['language_id']; ?>">';
		html += '          <div class="form-group">';
		html += '            <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_column_image; ?></label>';
		html += '            <div class="col-sm-10">';
		html += '              <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][<?php echo $language['language_id']; ?>][column][image]" placeholder="<?php echo $entry_column_image; ?>" value="Image" id="input-value' + for_row + '" class="form-control" />';
		html += '            </div>';
		html += '          </div>';
		html += '          <div class="form-group">';
		html += '            <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_column_product; ?></label>';
		html += '            <div class="col-sm-10">';
		html += '              <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][<?php echo $language['language_id']; ?>][column][product]" placeholder="<?php echo $entry_column_product; ?>" value="Product name" id="input-value' + for_row + '" class="form-control" />';
		html += '            </div>';
		html += '          </div>';
		html += '          <div class="form-group">';
		html += '            <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_column_model; ?></label>';
		html += '            <div class="col-sm-10">';
		html += '              <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][<?php echo $language['language_id']; ?>][column][model]" placeholder="<?php echo $entry_column_model; ?>" value="Model" id="input-value' + for_row + '" class="form-control" />';
		html += '            </div>';
		html += '          </div>';
		html += '          <div class="form-group">';
		html += '            <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_column_sku; ?></label>';
		html += '            <div class="col-sm-10">';
		html += '              <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][<?php echo $language['language_id']; ?>][column][sku]" placeholder="<?php echo $entry_column_sku; ?>" value="SKU" id="input-value' + for_row + '" class="form-control" />';
		html += '            </div>';
		html += '          </div>';
		html += '          <div class="form-group">';
		html += '            <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_column_quantity; ?></label>';
		html += '            <div class="col-sm-10">';
		html += '              <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][<?php echo $language['language_id']; ?>][column][quantity]" placeholder="<?php echo $entry_column_quantity; ?>" value="Quantity" id="input-value' + for_row + '" class="form-control" />';
		html += '            </div>';
		html += '          </div>';
		html += '          <div class="form-group">';
		html += '            <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_column_price; ?></label>';
		html += '            <div class="col-sm-10">';
		html += '              <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][<?php echo $language['language_id']; ?>][column][price]" placeholder="<?php echo $entry_column_price; ?>" value="Price" id="input-value' + for_row + '" class="form-control" />';
		html += '            </div>';
		html += '          </div>';
		html += '          <div class="form-group">';
		html += '            <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_column_total; ?></label>';
		html += '            <div class="col-sm-10">';
		html += '              <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][<?php echo $language['language_id']; ?>][column][total]" placeholder="<?php echo $entry_column_total; ?>" value="Total" id="input-value' + for_row + '" class="form-control" />';
		html += '            </div>';
		html += '          </div>';
		html += '        </div>';
		<?php } ?>
		html += '      </div>';
		html += '    </div>';
		html += '  </div>';
	} else if (section_key == 'order') {
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_order_id; ?></label>';
		html += '    <div class="col-sm-10">';
		html += '      <select name="success_page[section][' + section_row + '][' + section_key + '][order_id_status]" id="input-value' + for_row + '" class="form-control">';
		html += '        <option value="1"><?php echo $text_yes; ?></option>';
		html += '        <option value="0"><?php echo $text_no; ?></option>';
		html += '      </select>';
		html += '    </div>';
		html += '  </div>';
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_date_added; ?></label>';
		html += '    <div class="col-sm-10">';
		html += '      <select name="success_page[section][' + section_row + '][' + section_key + '][date_added_status]" id="input-value' + for_row + '" class="form-control">';
		html += '        <option value="1"><?php echo $text_yes; ?></option>';
		html += '        <option value="0"><?php echo $text_no; ?></option>';
		html += '      </select>';
		html += '    </div>';
		html += '  </div>';
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_payment_method; ?></label>';
		html += '    <div class="col-sm-10">';
		html += '      <select name="success_page[section][' + section_row + '][' + section_key + '][payment_method_status]" id="input-value' + for_row + '" class="form-control">';
		html += '        <option value="1"><?php echo $text_yes; ?></option>';
		html += '        <option value="0"><?php echo $text_no; ?></option>';
		html += '      </select>';
		html += '    </div>';
		html += '  </div>';
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_shipping_method; ?></label>';
		html += '    <div class="col-sm-10">';
		html += '      <select name="success_page[section][' + section_row + '][' + section_key + '][shipping_method_status]" id="input-value' + for_row + '" class="form-control">';
		html += '        <option value="1"><?php echo $text_yes; ?></option>';
		html += '        <option value="0"><?php echo $text_no; ?></option>';
		html += '      </select>';
		html += '    </div>';
		html += '  </div>';
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_email; ?></label>';
		html += '    <div class="col-sm-10">';
		html += '      <select name="success_page[section][' + section_row + '][' + section_key + '][email_status]" id="input-value' + for_row + '" class="form-control">';
		html += '        <option value="1"><?php echo $text_yes; ?></option>';
		html += '        <option value="0"><?php echo $text_no; ?></option>';
		html += '      </select>';
		html += '    </div>';
		html += '  </div>';
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_telephone; ?></label>';
		html += '    <div class="col-sm-10">';
		html += '      <select name="success_page[section][' + section_row + '][' + section_key + '][telephone_status]" id="input-value' + for_row + '" class="form-control">';
		html += '        <option value="1"><?php echo $text_yes; ?></option>';
		html += '        <option value="0"><?php echo $text_no; ?></option>';
		html += '      </select>';
		html += '    </div>';
		html += '  </div>';
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_invoice; ?></label>';
		html += '    <div class="col-sm-10">';
		html += '      <select name="success_page[section][' + section_row + '][' + section_key + '][invoice_status]" id="input-value' + for_row + '" class="form-control">';
		html += '        <option value="1"><?php echo $text_yes; ?></option>';
		html += '        <option value="0"><?php echo $text_no; ?></option>';
		html += '      </select>';
		html += '    </div>';
		html += '  </div>';
		html += '  <div class="form-group">';
		html += '    <div class="col-sm-12">';
		html += '      <ul class="nav nav-tabs" id="language' + section_row + '">';
		<?php foreach ($languages as $language) { ?>
		html += '        <li><a href="#language' + section_row + '_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></a></li>';
		<?php } ?>
		html += '      </ul>';
		html += '      <div class="tab-content">';
		<?php foreach ($languages as $language) { ?>
		html += '        <div class="tab-pane" id="language' + section_row + '_<?php echo $language['language_id']; ?>">';
		html += '          <div class="form-group">';
		html += '            <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_title; ?></label>';
		html += '            <div class="col-sm-10">';
		html += '              <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][<?php echo $language['language_id']; ?>][title]" placeholder="<?php echo $entry_title; ?>" value="Order details" id="input-value' + for_row + '" class="form-control" />';
		html += '            </div>';
		html += '          </div>';
		html += '          <div class="form-group">';
		html += '            <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_order_id; ?></label>';
		html += '            <div class="col-sm-10">';
		html += '              <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][<?php echo $language['language_id']; ?>][detail][order_id]" placeholder="<?php echo $entry_order_id; ?>" value="Order ID:" id="input-value' + for_row + '" class="form-control" />';
		html += '            </div>';
		html += '          </div>';
		html += '          <div class="form-group">';
		html += '            <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_date_added; ?></label>';
		html += '            <div class="col-sm-10">';
		html += '              <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][<?php echo $language['language_id']; ?>][detail][date_added]" placeholder="<?php echo $entry_date_added; ?>" value="Date added:" id="input-value' + for_row + '" class="form-control" />';
		html += '            </div>';
		html += '          </div>';
		html += '          <div class="form-group">';
		html += '            <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_payment_method; ?></label>';
		html += '            <div class="col-sm-10">';
		html += '              <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][<?php echo $language['language_id']; ?>][detail][payment_method]" placeholder="<?php echo $entry_payment_method; ?>" value="Payment method:" id="input-value' + for_row + '" class="form-control" />';
		html += '            </div>';
		html += '          </div>';
		html += '          <div class="form-group">';
		html += '            <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_shipping_method; ?></label>';
		html += '            <div class="col-sm-10">';
		html += '              <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][<?php echo $language['language_id']; ?>][detail][shipping_method]" placeholder="<?php echo $entry_shipping_method; ?>" value="Shipping method:" id="input-value' + for_row + '" class="form-control" />';
		html += '            </div>';
		html += '          </div>';
		html += '          <div class="form-group">';
		html += '            <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_email; ?></label>';
		html += '            <div class="col-sm-10">';
		html += '              <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][<?php echo $language['language_id']; ?>][detail][email]" placeholder="<?php echo $entry_email; ?>" value="Email:" id="input-value' + for_row + '" class="form-control" />';
		html += '            </div>';
		html += '          </div>';
		html += '          <div class="form-group">';
		html += '            <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_telephone; ?></label>';
		html += '            <div class="col-sm-10">';
		html += '              <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][<?php echo $language['language_id']; ?>][detail][telephone]" placeholder="<?php echo $entry_telephone; ?>" value="Telephone:" id="input-value' + for_row + '" class="form-control" />';
		html += '            </div>';
		html += '          </div>';
		html += '          <div class="form-group">';
		html += '            <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_invoice; ?></label>';
		html += '            <div class="col-sm-10">';
		html += '              <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][<?php echo $language['language_id']; ?>][detail][invoice]" placeholder="<?php echo $entry_invoice; ?>" value="Invoice No.:" id="input-value' + for_row + '" class="form-control" />';
		html += '            </div>';
		html += '          </div>';
		html += '        </div>';
		<?php } ?>
		html += '      </div>';
		html += '    </div>';
		html += '  </div>';
	} else if (section_key == 'product') {
		html += '	<div class="form-group">';
		html += '	  <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_product; ?></label>';
		html += '	  <div class="col-sm-10">';
		html += '	    <div style="position: relative;">';
		html += '	      <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][autocomplete]" value="" placeholder="<?php echo $entry_product; ?>" data-row="' + section_row + '" data-element="product" id="input-value' + for_row + '" class="form-control" />';
		html += '	    </div>';
		html += '	    <div id="product' + section_row + '" class="well well-sm" style="height: 150px; overflow: auto;">';
		html += '	    </div>';
		html += '	  </div>';
		html += '	</div>';
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_title_font; ?></label>';
		html += '    <div class="col-sm-10">';
		html += '      <div class="input-group">';
		html += '        <span class="input-group-addon"><?php echo $entry_size; ?></span>';
		html += '        <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][size]" value="15px" placeholder="<?php echo $entry_size; ?>" id="input-value' + for_row + '" class="form-control" />';
		html += '        <span class="input-group-addon" style="border-left: 0; border-right: 0;"><?php echo $entry_color; ?></span>';
		html += '        <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][color]" value="#e26703" placeholder="<?php echo $entry_color; ?>" id="input-value' + for_row + '" class="form-control" />';
		html += '        <span class="input-group-addon" style="border-left: 0; border-right: 0;"><?php echo $entry_weight; ?></span>';
		html += '        <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][weight]" value="bold" placeholder="<?php echo $entry_weight; ?>" id="input-value' + for_row + '" class="form-control" />';
		html += '      </div>';
		html += '    </div>';
		html += '  </div>';
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_title; ?></label>';
		html += '    <div class="col-sm-10">';
		<?php foreach ($languages as $language) { ?>
		html += '      <div class="input-group"> <span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>';
		html += '        <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][<?php echo $language['language_id']; ?>][title]" value="You may also be interested in the following products" placeholder="<?php echo $entry_title; ?>" id="input-value' + for_row + '" class="form-control" />';
		html += '      </div>';
		<?php } ?>
		html += '    </div>';
		html += '  </div>';
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_button_cart; ?></label>';
		html += '    <div class="col-sm-10">';
		<?php foreach ($languages as $language) { ?>
		html += '      <div class="input-group"> <span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>';
		html += '        <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][<?php echo $language['language_id']; ?>][button]" value="Add to cart" placeholder="<?php echo $entry_button_cart; ?>" id="input-value' + for_row + '" class="form-control" />';
		html += '      </div>';
		<?php } ?>
		html += '    </div>';
		html += '  </div>';
	} else if (section_key == 'facebook') {
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_facebook_url; ?></label>';
		html += '    <div class="col-sm-10">';
		html += '      <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][url]" value="" placeholder="<?php echo $entry_facebook_url; ?>" id="input-value' + for_row + '" class="form-control" />';
		html += '    </div>';
		html += '  </div>';
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_facebook_dimension; ?></label>';
		html += '    <div class="col-sm-5">';
		html += '      <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][width]" value="500px" placeholder="<?php echo $entry_width; ?>" id="input-value' + for_row + '" class="form-control" />';
		html += '    </div>';
		html += '    <div class="col-sm-5">';
		html += '      <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][height]" value="210px" placeholder="<?php echo $entry_height; ?>" id="input-value' + for_row + '" class="form-control" />';
		html += '    </div>';
		html += '  </div>';
	} else if (section_key == 'youtube') {
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_youtube_url; ?></label>';
		html += '    <div class="col-sm-10">';
		html += '      <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][url]" value="" placeholder="<?php echo $entry_youtube_url; ?>" id="input-value' + for_row + '" class="form-control" />';
		html += '    </div>';
		html += '  </div>';
		html += '  <div class="form-group">';
		html += '    <label class="col-sm-2 control-label" for="input-value' + for_row + '"><?php echo $entry_youtube_dimension; ?></label>';
		html += '    <div class="col-sm-5">';
		html += '      <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][width]" value="200px" placeholder="<?php echo $entry_width; ?>" id="input-value' + for_row + '" class="form-control" />';
		html += '    </div>';
		html += '    <div class="col-sm-5">';
		html += '      <input type="text" name="success_page[section][' + section_row + '][' + section_key + '][height]" value="210px" placeholder="<?php echo $entry_height; ?>" id="input-value' + for_row + '" class="form-control" />';
		html += '    </div>';
		html += '  </div>';
	}

	html += '    </div>';
	html += '  </div>';
	html += '</div>';

	$('#tab-section > .tab-content').append(html);

	$('#section').append('<li><a href="#tab-section' + section_row + '" data-toggle="tab"><i class="fa fa-minus-circle"></i> ' + $(this).text() + '<div class="pull-right"><i class="fa fa-arrow-up" data-pos="up"></i> <i class="fa fa-arrow-down" data-pos="down"></i></div></a></li>');

	$('#section a[href=\'#tab-section' + section_row + '\']').tab('show');

	if (section_key == 'product') {
		product();
	}

	if (section_key == 'text' || section_key == 'address' || section_key == 'cart' || section_key == 'order' || section_key == 'payment') {
		<?php foreach ($languages as $language) { ?>
		$('#input-description' + section_row + '_<?php echo $language['language_id']; ?>').summernote({height: 300});

			<?php foreach ($payments as $payment) { ?>
			$('#input-description' + section_row + '_<?php echo $language['language_id']; ?>_<?php echo $payment['extension']; ?>').summernote({height: 300});
			<?php } ?>
		<?php } ?>

		$('#language' + section_row + ' a:first').tab('show');

		if (section_key == 'payment') {
			$('#payment' + section_row + ' a:first').tab('show');
		}
	}

	section_row++;
	for_row++;
});

$('body').delegate('ul[id^="payment"] > li > a', 'click', function(event) {
	lng_id = $(this).attr('href').replace('#', '');

	$('#' + lng_id).find('ul a:first').tab('show');
});

$('body').delegate('li > a > i.fa-minus-circle', 'click', function(event) {
	id = $(this).parents('a').attr('href').replace(/#tab-section(\d+)/g, "$1");

	$(this).parents('a').remove();
	$('#tab-section' + id).remove();
	$('#section a:first').tab('show');

	return false;
});

$('body').delegate('i.fa-arrow-up, i.fa-arrow-down', 'click', function(event) {
	$('div').removeClass('selector');

	current_id = $(this).parents('a').attr('href').replace(/#tab-section(\d+)/g, "$1");
	position = $(this).data('pos');

	var $node = $(this).parents('li');

	if (position == 'up') {
		if ($node.is(':first-child')) {
			return false;
		}

		new_id = $node.prev().find('a').attr('href').replace(/#tab-section(\d+)/g, "$1");

		$(this).parents('a').attr('href', '#tab-section' + new_id);
		$node.prev().find('a').attr('href', '#tab-section' + current_id);
	} else if (position == 'down') {
		if ($node.is(':last-child')) {
			return false;
		}

		new_id = $node.next().find('a').attr('href').replace(/#tab-section(\d+)/g, "$1");

		$(this).parents('a').attr('href', '#tab-section' + new_id);
		$node.next().find('a').attr('href', '#tab-section' + current_id);
	}

	$('div#tab-section' + current_id).attr('id', 'tab-section' + new_id).addClass('selector');
	$('div#tab-section' + new_id).not('.selector').attr('id', 'tab-section' + current_id);

	$('div#tab-section' + new_id).find(':input[name^="success_page"]').each(function(i) {
		name = $(this).attr('name');

		$(this).attr('name', name.replace('success_page[section][' + current_id + ']', 'success_page[section][' + new_id + ']'));
	});

	$('div#tab-section' + current_id).find(':input[name^="success_page"]').each(function(i) {
		name = $(this).attr('name');

		$(this).attr('name', name.replace('success_page[section][' + new_id + ']', 'success_page[section][' + current_id + ']'));
	});

	if (position == 'up') {
		$node.prev().before($node);
	} else if (position == 'down') {
		$node.next().after($node);
	}

	return false;
});

function product() {
	$('input[name$="[autocomplete]"]').autocomplete({
		source: function(request, response) {
			$.ajax({
				url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
				dataType: 'json',
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: item['name'],
							value: item['product_id']
						}
					}));
				}
			});
		},
		select: function(item) {
			row = $(this).attr('data-row');
			element = $(this).attr('data-element');

			$(this).val('');

			$('#product' + row + '_' + item['value']).remove();
			$('#product' + row).append('<div id="product' + row + '_' + item['value'] + '"><i class="fa fa-minus-circle" onClick="$(\'#product' + row + '_' + item['value'] + '\').remove();"></i> ' + item['label'] + '<input type="hidden" name="success_page[section][' + row + '][' + element + '][product][]" value="' + item['value'] + '" /></div>');		
		}
	});
}

product();

<?php $section_row = 0; ?>
<?php if (isset ($section) && $section) { ?>
<?php foreach ($section as $module) { ?>
	<?php $section_key = key($module); ?>

	<?php if ($section_key == 'text' || $section_key == 'address' || $section_key == 'cart' || $section_key == 'order' || $section_key == 'payment') { ?>
		<?php foreach ($languages as $language) { ?>
		$('#input-description<?php echo $section_row; ?>_<?php echo $language['language_id']; ?>').summernote({height: 300});

			<?php foreach ($payments as $payment) { ?>
			$('#input-description<?php echo $section_row; ?>_<?php echo $language['language_id']; ?>_<?php echo $payment['extension']; ?>').summernote({height: 300});
			<?php } ?>
		<?php } ?>

		$('#language<?php echo $section_row; ?> a:first').tab('show');

		<?php if ($section_key == 'payment') { ?>
			$('#payment<?php echo $section_row; ?> a:first').tab('show');
		<?php } ?>
	<?php } ?>

	<?php $section_row++; ?>
<?php } ?>
<?php } ?>

$('#section a:first').tab('show');
//--></script>
<?php echo $footer; ?>