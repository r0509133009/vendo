<script src="//code.jquery.com/jquery-1.9.1.js"></script> 
<?php echo $header;  ?>

<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<link href="view/javascript/summernote/summernote.css" rel="stylesheet">
<script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>
<script type="text/javascript" src="/admin/view/javascript/jqueryuploader/ajaxupload.3.5.js" ></script>
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
<div id="content">

  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-templates" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } 
		
		ksort($emailswtemplates_modules);
        reset($emailswtemplates_modules);

		//echo '<pre>';		print_r($emailswtemplates_modules);		echo '</pre>';
		
		?>
      </ul>
    </div>
  </div>
  
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-templates" class="form-horizontal">

          <div class="row">
         

		 <div class="col-sm-2">
              <ul class="nav nav-pills nav-stacked" id="module">
                <?php $module_row = 1; 
				foreach ($emailswtemplates_modules as $emailswtemplates_module) { 
				//for  ($x=1; $x<=count($emailswtemplates_modules); $x++) { 
               
				?>
				
                <li>
				<a href="#tab-module<?php echo $emailswtemplates_module['key']; ?>" data-toggle="tab">
					<i class="fa fa-minus-circle" onclick="$('a[href=\'#tab-module<?php echo $emailswtemplates_module['key']; ?>\']').parent().remove(); $('#tab-module<?php echo $emailswtemplates_module['key']; ?>').remove(); $('#module a:first').tab('show');"></i> 
					<?php echo $tab_module . ' ' . $emailswtemplates_module['key']; ?>
				</a></li>
				<?php $module_row++; 	} ?>
                <li id="module-add"><a onclick="addModule();"><i class="fa fa-plus-circle"></i> <?php echo $button_module_add; ?></a></li>
              </ul>
            </div>
			
			
            <div class="col-sm-10">
              <div class="tab-content">
                <?php foreach ($emailswtemplates_modules as $emailswtemplates_module) { ?>
                <div class="tab-pane" id="tab-module<?php echo $emailswtemplates_module['key']; ?>">
				
                <ul class="nav nav-tabs" id="language<?php echo $emailswtemplates_module['key']; ?>">    
                    <li><a href="#tab-module<?php echo $emailswtemplates_module['key']; ?>-language" data-toggle="tab"> Html</a></li>
                </ul>
                  <div class="tab-content">
                    <?php foreach ($languages as $language) { ?>
                    <div class="tab-pane" id="tab-module<?php echo $emailswtemplates_module['key']; ?>-language">
                     
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-description<?php echo $emailswtemplates_module['key']; ?>-language"><?php echo $entry_description; ?></label>
                        <div class="col-sm-10">
                          <textarea name="emailswtemplates_module[<?php echo $emailswtemplates_module['key']; ?>][description]" placeholder="<?php echo $entry_description; ?>" id="input-description<?php echo $emailswtemplates_module['key']; ?>-language" class="form-control"><?php echo isset($emailswtemplates_module['description'] ) ? $emailswtemplates_module['description'] : ''; ?></textarea>
                        </div>
						<input type="hidden" name="emailswtemplates_module[<?php echo $emailswtemplates_module['key']; ?>][layout_id]" value="none" >
						<input type="hidden" name="emailswtemplates_module[<?php echo $emailswtemplates_module['key']; ?>][position]" value="none" >
                      </div>
                    </div>
                    <?php } ?>
                  </div>
                </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>





  
   



  <script type="text/javascript"><!--
<?php foreach ($emailswtemplates_modules as $emailswtemplates_module) { ?>

$('#input-description<?php echo $emailswtemplates_module['key']; ?>-language').summernote({
	height: 300
});

<?php } ?>
//--></script> 

  <script type="text/javascript"><!--
  var module_row = <?php echo $module_row; ?>;
 
function addModule() {

	var token = module_row;
	
	html  = '<div class="tab-pane" id="tab-module' + module_row + '">';
	html += '  <ul class="nav nav-tabs" id="language' + module_row + '">';
    html += '    <li><a href="#tab-module' + module_row + '-language" data-toggle="tab">Html</a></li>';   
	html += '  </ul>';

	html += '  <div class="tab-content">';

	html += '    <div class="tab-pane" id="tab-module' + module_row + '-language">';
	html += '      <div class="form-group">';
	html += '        <label class="col-sm-2 control-label" for="input-description' + module_row + '-language"><?php echo $entry_description; ?></label>';
	html += '        <div class="col-sm-10"><textarea name="emailswtemplates_module[' + module_row + '][description]" placeholder="<?php echo $entry_description; ?>" id="input-description' + token + '-language"></textarea></div>';
	html += '        <input type="hidden" name="emailswtemplates_module[' + module_row + '][position]" value="none" >';
	html += '        <input type="hidden" name="emailswtemplates_module[' + module_row + '][layout_id]" value="" >';
	html += '      </div>';
	html += '    </div>';


	html += '  </div>';
	html += '</div>';

	$('.tab-content:first-child').prepend(html);

	<?php foreach ($languages as $language) { ?>
	$('#input-description' + token + '-language').summernote({
		height: 300
	});
	<?php } ?>

	$('#module-add').before('<li><a href="#tab-module' + token + '" data-toggle="tab"><i class="fa fa-minus-circle" onclick="$(\'a[href=\\\'#tab-module' + token + '\\\']\').parent().remove(); $(\'#tab-module' + token + '\').remove(); $(\'#module a:first\').tab(\'show\');"></i> <?php echo $tab_module; ?> ' + module_row + '</a></li>');

	$('#module a[href=\'#tab-module' + token + '\']').tab('show');

	$('#language' + token + ' li:first-child a').tab('show');

	module_row++;
}
//--></script> 
  <script type="text/javascript"><!--
$('#module li:first-child a').tab('show');
<?php foreach ($emailswtemplates_modules as $emailswtemplates_module) { ?>
$('#language<?php echo $emailswtemplates_module['key']; ?> li:first-child a').tab('show');
<?php } ?>
//--></script>


</div>
<?php echo $footer; ?>