<?php echo $header; ?>
<style>
  
  input[type="text"], input[type="email"] {
    width: 100%;
    height: 30px;
  }

  .delete {
    text-indent: -9999em;
    display: block;
    background: url('view/image/delete-ordermailclone.png') no-repeat;
    width: 32px;
    height: 32px;
  }

  input[type="submit"] {
    text-indent: -9999em;
    display: block;
    background: url('view/image/add-ordermailclone.png') no-repeat;
    width: 32px;
    height: 32px;
    border: 0;
    cursor: pointer;
  }

  .email-data-table {
  font-family: sans-serif;
  -webkit-font-smoothing: antialiased;
  font-size: 115%;
  overflow: auto;
  display: block;
  width: 100%;
  display: table;
  border: none;
  }
  .email-data-table th {
  background-color: rgb(112, 196, 105);
  font-weight: normal;
  color: white;
  padding: 20px 30px;
  text-align: center;
  }
  .email-data-table td {
  background-color: rgb(238, 238, 238);
  padding: 20px 30px;
  color: rgb(111, 111, 111);
  }

  .email-data-table tr:nth-child(even) td {
    background-color: white;
  }

  .email-data-table td + td + td {
    padding: 20px 20px 20px 41px;
  }

</style>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if (isset($error_warning)) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
    </div>
    <div class="content">
      <table class="email-data-table" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th style="width: 45%">Who's that guy?</th>
            <th style="width: 45%">Email</th>
            <th style="width: 10%">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($clone_mails as $mail): ?>
          <tr id="data-row-<?php echo $mail['order_mail_clone_id'];?>">
            <td><?php echo $mail['description']; ?></td>
            <td><?php echo $mail['email']; ?></td>
            <td><a href="#" data-delete-id="<?php echo $mail['order_mail_clone_id'];?>" class="delete">Delete</a></td>
          </tr>
          <?php endforeach; ?>
          <tr>
          <form id="input_form">
          <td><input type="text" name="description" placeholder="Guy's/Girl's Name/Role" /></td>
          <td><input type="email" name="email" placeholder="newmail@example.com" /></td>     
          <td><input type="submit" data-add-url="<?php echo $ajax_url['add']; ?>" class="add" value="Add"></td>
          <input type="hidden" name="delete-url" value="<?php echo $ajax_url['delete']; ?>" />
          </form>
          </tr>
          
          
        </tbody>
      </table>    
    </div>
  </div>
</div>
<script type="text/javascript">
$(function() {

  // DELETE
  function set_delete_callback() {
    $('.delete').on('click', function(e) {
      e.preventDefault();
      
      delete_record($(this).attr('data-delete-id'));
    });
  }

  set_delete_callback();

  
  // ADD
  $('.add').on('click', function(e) {
    e.preventDefault();

    add_record();
  });

  $('.input_form').on('submit', function(e) {
    e.preventDefault();

    add_record();
  });



  function add_record() {

    var description = $('input[name="description"]').val();
    var email       = $('input[name="email"]').val();

    if (!validateEmail(email)) {
      alert('Please enter the valid email address');
    } else {
      $.ajax({
        url: $('.add').attr('data-add-url'),
        data: {email: email, description: description},
        success: function(data) {
          data = $.parseJSON(data);
          if (data.status == 'success') {
            $('.content table tr:last').before('<tr id="data-row-' + data.id + '"><td>' + description + '</td><td>' + email + '</td><td><a href="#" data-delete-id="' + data.id + '" class="delete">Delete</a></td></tr>');

            $('input[name="description"]').val('');
            $('input[name="email"]').val('');
            $('input[name="description"]').focus();

            set_delete_callback();
          }  
        }
      });
    }
  }

  function delete_record(id) {
    $.ajax({
        url: $('input[name="delete-url"]').val(),
        data: {order_mail_clone_id: id},
        success: function(data) {
          data = $.parseJSON(data);
          if (data.status == 'success') {
            $('#data-row-' + id).remove();
          }  
        }
      });
  }

  // Validate Email Address
  function validateEmail(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  } 

});
</script> 
<?php echo $footer; ?>