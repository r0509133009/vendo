<?php echo $header;  ?>
<script src="//code.jquery.com/jquery-1.9.1.js"></script> 
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<link href="view/javascript/summernote/summernote.css" rel="stylesheet">
<script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>
<script type="text/javascript" src="/admin/view/javascript/jqueryuploader/ajaxupload.3.5.js" ></script>
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">	
<script>

</script>

<?php
if(isset($_GET['tab']) && $_GET['tab']='history' ){ 
echo "<script>
  $(function () {
$('#mytabs a[href=\"#tab-3\"]').tab('show')
})
</script> ";

}
?>
<style>
.btn-group a{
color:#fff;
}
</style>
<?php echo $column_left; ?>


  
<div id="content"> 

  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
             <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  
    <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
	
	
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
	  
	  <div style="display: block; float: left; height: 35px; margin: 20px 0 10px; width: 100%;">
		 <a id="button-send" href="index.php?route=module/emailswtemplates/templates&token=<?php echo $token; ?>"> 
		 <button type="button" class="btn btn-primary"> <?php echo $edit_template; ?></button></a>
			
		<a id="button-send" href="index.php?route=module/emailswtemplates/subscribers&token=<?php echo $token; ?>" >
		 <button type="button" class="btn btn-primary"> <?php echo $edit_subscribers; ?></button>
		</a>
	</div>
	
	  <!-- Nav tabs -->
	<ul class="nav nav-tabs" id="mytabs">
	  <li  class="active"><a href="#tab-1" data-toggle="tab">Send Mail</a></li>
	  <li><a href="#tab-2" data-toggle="tab">Upload Subscribers</a></li>
	  <li><a href="#tab-3" data-toggle="tab">History</a></li>
	</ul>




<!-- Tab panes -->
<div class="tab-content">

<!-- Tab 1 -->
  <div class="tab-pane active" id="tab-1">
	       <div class="container-fluid">
      <div class="pull-right">
	 <a id="button-send" onclick="send('index.php?route=module/emailswtemplates/send&token=<?php echo $token; ?>');" class=""> <button type="button" class="btn btn-info"> <?php echo $button_send; ?></button></a>
	<!-- <a onclick="location = '<?php echo $cancel; ?>';" class="button"><button type="button" class="btn btn-primary"> <?php echo $button_cancel; ?></button></a>-->
	 
	 </div>  
	 </div>  
	  
<!--<ul class="nav nav-tabs" id="language">
                    <?php foreach ($languages as $language) { ?>
                    <li><a href="#tab-module-language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                    <?php } ?>
 </ul>-->

		  
   
        <form class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-store"><?php echo $entry_store; ?></label>
            <div class="col-sm-10">
                <input type="text" id="from_email" class="form-control" name="from_email" value="<?php echo 'test@gmail.com'; ?>" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-to"><?php echo $entry_to; ?></label>
            <div class="col-sm-10">
              <select name="to" id="input-to" class="form-control">
			  <option value="test_email"><?php echo $text_test_email; ?></option>  
                <option value="newsletter"><?php echo $text_newsletter.' ('.$text_newsletter_c.')'; ?></option>
                <option value="module_newsletter"><?php echo $text_module_newsletter.' ('.$text_nynewsletter_c.')'; ?></option>
                <option value="customer_all"><?php echo $text_customer_all; ?></option>
                <option value="customer_group"><?php echo $text_customer_group; ?></option>
                <option value="customer"><?php echo $text_customer; ?></option>
                <option value="affiliate_all"><?php echo $text_affiliate_all; ?></option>
                <option value="affiliate"><?php echo $text_affiliate; ?></option>
                <option value="product"><?php echo $text_product; ?></option>
              </select>
            </div>
          </div>
		  
		   <div class="form-group test_email_block ">
            <label class="col-sm-2 control-label" for="test_email"><?php echo $entry_test_email; ?></label>
            <div class="col-sm-10">
              <input type="text" id="test_email" class="form-control" name="test_email" value="" />
            </div>
          </div>
		  
		  <div class="form-group">
            <label class="col-sm-2 control-label" for="templ_id"><?php echo $entry_template; ?></label>
            <div class="col-sm-10">

			 <select name="templ_id" id="templ_id" lang='<?php echo $language['language_id']; ?>' class="form-control">         
			 <?php foreach ($templates as $k => $v ) {  ?>               
			 <option value="<?php echo $k; ?>" class="tpl_<?php echo $k; ?>" tpl="<?php echo $v['description']; ?>" ><?php echo $k; ?></option>  
			 <?php }  ?> 
		  </select> 
            </div>
          </div>
		  
		  
          <div class="form-group to" id="to-customer-group">
            <label class="col-sm-2 control-label" for="input-customer-group"><?php echo $entry_customer_group; ?></label>
            <div class="col-sm-10">
              <select name="customer_group_id" id="input-customer-group" class="form-control">
                <?php foreach ($customer_groups as $customer_group) { ?>
                <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group to" id="to-customer">
            <label class="col-sm-2 control-label" for="input-customer"><span data-toggle="tooltip" title="<?php echo $help_customer; ?>"><?php echo $entry_customer; ?></span></label>
            <div class="col-sm-10">
              <input type="text" name="customers" value="" placeholder="<?php echo $entry_customer; ?>" id="input-customer" class="form-control" />
              <div id="customer" class="well well-sm" style="height: 150px; overflow: auto;"></div>
            </div>
          </div>
          <div class="form-group to" id="to-affiliate">
            <label class="col-sm-2 control-label" for="input-affiliate"><span data-toggle="tooltip" title="<?php echo $help_affiliate; ?>"><?php echo $entry_affiliate; ?></span></label>
            <div class="col-sm-10">
              <input type="text" name="affiliates" value="" placeholder="<?php echo $entry_affiliate; ?>" id="input-affiliate" class="form-control" />
              <div id="affiliate" class="well well-sm" style="height: 150px; overflow: auto;"></div>
            </div>
          </div>
          <div class="form-group to" id="to-product">
            <label class="col-sm-2 control-label" for="input-product"><span data-toggle="tooltip" title="<?php echo $help_product; ?>"><?php echo $entry_product; ?></span></label>
            <div class="col-sm-10">
              <input type="text" name="products" value="" placeholder="<?php echo $entry_product; ?>" id="input-product" class="form-control" />
              <div id="product" class="well well-sm" style="height: 150px; overflow: auto;"></div>
            </div>
          </div>
          <div class="form-group ">
            <label class="col-sm-2 control-label" for="input-subject"><?php echo $entry_subject; ?>*</label>
            <div class="col-sm-10">
              <input type="text" name="subject" value="" placeholder="<?php echo $entry_subject; ?>" id="input-subject" class="form-control" />
            </div>
          </div>
          <div class="form-group ">
            <label class="col-sm-2 control-label" for="input-message"><?php echo $entry_message; ?>*</label>
            <div class="col-sm-10">
              <textarea name="message" class="message_lang_<?php echo $language['language_id']; ?>" placeholder="<?php echo $entry_message; ?>" id="input-message" class="form-control"></textarea>
            </div>
          </div>
        </form>
	
      
		
  
    </div>
 
<!-- Tab 2 -->
    <div class="tab-pane" id="tab-2">
  		 <div class="alert " style="display:none;"><p ></p></div>
		 	<?php echo $entry_alowed_format; ?><br>
			<?php echo $entry_example_text; ?>
            <table class="form">
			  <tr></td>
                <td>

				</td>
              </tr>
			  
              <tr>
                <td><br><div id="upload" class="btn btn-info" cid="1"><span>Upload File<span></div><span id="status" ></span></td>
                <td>
				<div class="progress progress-striped active" style="display:none;">
					<span class="pr-bar-info">0% Complete</span>
					<div class="progress-bar"  role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
						<span class="sr-only">45% Complete</span>
					</div>
				</div>
				</td>
              </tr>
			  <tr>
                <td></td>
                <td><ul id="files" ></ul></td>
              </tr>
			  
            </table> 
	</div>
			
			
  <div class="tab-pane" id="tab-3">
  		<div style="display: block; text-align: right; height: 35px; margin: 0px 0 10px; width: 100%;">
			<a  onclick="$('#form3').submit();" class=""><button type="button" class="btn btn-primary" > <?php echo $button_delete_history; ?></button></a>
			<a id="button-send" href="index.php?route=module/emailswtemplates/view_all_history&token=<?php echo $token; ?>"><button type="button" class="btn btn-primary" > <?php echo $view_all_history; ?></button></a>
			
		</div>
		
		<div class="content">

		<form action="<?php echo $delete_history; ?>" method="post" enctype="multipart/form-data" id="form3">
      <table class="table table-striped">
          <tr>
            <td width="1" ><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
            <td class=""><?php echo $column_id; ?></td>
            <td class=""><?php echo $column_date; ?></td>
            <td class=""><?php echo $column_to; ?></td>
            <td class=""><?php echo $column_subject; ?></td>
            <td class=""><?php echo $entry_action; ?></td>
          </tr>
        

      
          <?php if ($history) {         
       foreach ($history as $email) {
              ?>
          <tr>
            <td >
              <input type="checkbox" name="selected[]" value="<?php echo $email['id']; ?>"/>
             </td>
            <td class=""><?php echo $email['id']; ?></td>
            <td class=""><?php echo $email['date']; ?></td>
            <td class=""><?php echo $email['to']; ?></td>
            <td class=""><?php echo $email['subject']; ?></td>
			 <td class=""><?php echo  ' <a href="/admin/index.php?route=module/emailswtemplates/view_history&token='.$token.'&id='.$email['id'].' ">'.$entry_view.'</a> ';  ?></td>
          </tr>
          <?php } ?>
          <?php } else { ?>
          <tr>
            <td class="text-center" colspan="6"><?php echo $text_no_results; ?></td>
          </tr>
          <?php } ?>
       
      </table>
    </form>
  </div>
	<div class="alert " style="display:none;"><p ></p></div>
		    <table class="form">
			  <tr>
                <td>
				</td>
              </tr>
            </table>
			</div>
			
			
			
</div>

	  
	  
	  
      
		
		
      </div>
    </div>
  </div>


  

<style>

.darkbg{
	background:#ddd !important;
}

ul#files{ list-style:none; padding:0;  margin: 15px 0 0; }
ul#exist_files{ list-style:none; padding:0;  margin: 15px 0 0; }
ul#files li{ padding:10px; margin-bottom:2px; width:200px;height: 200px; float:left; margin-right:10px;}
ul#exist_files li{ 
 background: none repeat scroll 0 0 #f2f2f2;
    float: left;
    height: 200px;
    margin-bottom: 10px;
    margin-right: 10px;
    padding: 10px;
    width: 200px;
	}
ul#files li img{ max-width:180px; max-height:150px; }
ul#exist_files li img{ max-width:180px; max-height:150px; }
.success{ 
    background: none repeat scroll 0 0 #b0e5f8;
    border: 1px solid #5489c6;
    transition: all 0.4s ease 0s; 
}
.error{ background:#f0c6c3; border:1px solid #cc6622; }
.del_img{
    display: block;
    float: right;
}
#exist_files_block{
  display: block;
    float: left;
}
</style>


  <script type="text/javascript"><!--
  $.noConflict();
jQuery( document ).ready(function( $ ) {

jQuery('#input-message').summernote({
		height: 300
	});
	

jQuery('.message_lang_1').code(  $('.tpl_1').attr('tpl') ); 



});
// Code that uses other library's $ can follow here.



  $(document).ready(function(){
   // alert( $('.tpl_1').attr('tpl') );
 

	
  });
 
	

$('select[name=\'templ_id\']').on('change', function() {
var id= $(this).val(); 
var lang = $(this).attr('lang');
var new_tpl = $('.tpl_'+id+'').attr('tpl');

	jQuery('.message_lang_'+lang+'').code(new_tpl);
	
	
});
	

	// Customers
$('input[name=\'customers\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=sale/customer/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',			
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['customer_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'customers\']').val('');
		
		$('#customer' + item['value']).remove();
		
		$('#customer').append('<div id="customer' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="customer[]" value="' + item['value'] + '" /></div>');	
	}	
});



// Products
$('input[name=\'products\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',			
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'products\']').val('');
		
		$('#product' + item['value']).remove();
		
		$('#product').append('<div id="product' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product[]" value="' + item['value'] + '" /></div>');	
	}	
});

  
  // Affiliates
$('input[name=\'affiliates\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=sale/customer/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',			
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['customer_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'affiliates\']').val('');
		
		$('#affiliate' + item['value']).remove();
		
		$('#affiliate').append('<div id="affiliate' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="affiliate[]" value="' + item['value'] + '" /></div>');	
	}	
});




	
$('select[name=\'to\']').on('change', function() {
	$('.to').hide();
	
	$('#to-' + this.value.replace('_', '-')).show();
	if($(this).val() =='test_email'){
		$('.test_email_block').show();
	}else{
    	$('.test_email_block').hide();
	}
	
});

$('select[name=\'to\']').trigger('change');







$('#customer').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});



$('#affiliate').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});



$('#product').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});

function send(url) {
var sHTML = jQuery('#input-message').code();
$('textarea[name=\'message\']').html(sHTML);
	$.ajax({
		url: url,
		type: 'post',
		data: $('#content select, #content input, #content textarea'),		
		dataType: 'json',
		beforeSend: function() {
			$('#button-send').button('loading');	
		},
		complete: function() {
			$('#button-send').button('reset');
		},				
		success: function(json) {
			$('.alert, .text-danger').remove();
			
			if (json['error']) {
				if (json['error']['warning']) {
					$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '</div>');
				}
				
				if (json['error']['subject']) {
					$('input[name=\'subject\']').after('<div class="text-danger">' + json['error']['subject'] + '</div>');
				}	
				
				if (json['error']['message']) {
					$('textarea[name=\'message\']').parent().append('<div class="text-danger">' + json['error']['message'] + '</div>');
				}									
			}			
			
			if (json['next']) {
				if (json['success']) {
					$('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i>  ' + json['success'] + '</div>');
					
					send(json['next']);
				}		
			} else {
				if (json['success']) {
					$('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
				}					
			}				
		}
	});
}
//--></script>






 <script type="text/javascript">

$(document).ready(function(){

        var btnUpload=$('#upload');
		var cid=$('#upload').attr('cid');
		var status=$('#status'); 
var timer;
		new AjaxUpload(btnUpload, {
			action: '/admin/index.php?route=module/emailswtemplates&token=<?php echo $token; ?>&action=upload_file', 
			name: 'uploadfile',
			onSubmit: function(file, ext){
				 if (! (ext && /^(txt|csv)$/.test(ext))){ 
                    // extension is not allowed 
					
					$('.alert').removeClass('alert-success').addClass('alert-danger').css('display','block');
					$('.alert p').text('Only .txt, .csv files are allowed');
					return false;
				}
				status.text('Uploading...');
				$('.progress').fadeTo('100',1);
					
					
					
					var x = 1;
                   timer = setInterval(function() { 
						// alert(x++);
						if(x>=100){
							clearInterval(timer);
						}else{
							$('.pr-bar-info').html(x+'% Complete ');
							$('.progress-bar').attr('aria-valuenow', x);
							$('.progress-bar').css('width', x+'%');
							x=x+5;
						}
						
				   }, 1000);
				   
				
			},
			onComplete: function(file, response){
			//stop interval
			status.text('');
			clearInterval(timer);
				//On completion clear the status
				//status.text('');
				//Add uploaded file to list
			//alert(response);
			       $('.pr-bar-info').text('100% Complete ');
			       $('.progress-bar').attr('aria-valuenow', 100);
					$('.progress-bar').css('width', '100%');
					setTimeout(function(){
						$('.progress').css('display','none');
						//$('.progress').fadeTo('100',0);
						$('.pr-bar-info').text('0% Complete ');
						$('.progress-bar').attr('aria-valuenow', 0);
						$('.progress-bar').css('width', '0%');
					}, 3000); 
				
				if(response==="error"){ 
					$('.alert').removeClass('alert-success').addClass('alert-danger');
					$('.alert p').html('Error');
				}
				else{
				$('.alert').removeClass('alert-danger').addClass('alert-success');
					$('.alert').fadeTo('900',0).fadeTo('900',1);
					$('.alert p').html(response+' emails was uploaded');
					
					
				} 
			}
		});
		

		
		
/*	$('#message').summernote({
		height: 300
	});*/
	

});
</script>
 </div>
  <?php echo $footer; ?>