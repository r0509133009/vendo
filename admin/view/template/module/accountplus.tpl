<?php echo $header; ?>
<div id="content">
<div class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
  <?php } ?>
</div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?></div>
<?php } ?>
<div class="box">
  <div class="heading">
    <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
    <div class="buttons">
        <a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a>
        <a onclick="$('#action').val('save-stay');$('#form').submit();" class="button"><?php echo $button_save_stay; ?></a>
        <a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a>
    </div>
  </div>
  <div class="content">
  
   <div id="tabs-accountplus" class="htabs">
      <a href="#tab-account"><?php echo $text_tab_account; ?></a>
      <a href="#tab-affiliate"><?php echo $text_tab_affiliate; ?></a>
   </div>
   
   <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
   <input type="hidden" name="action" id="action" />
   <div id="tab-account">
         <div id="tabs-account" class="vtabs">
              <a href="#tab-titles-account"><?php echo $text_custom_title; ?></a>
              <a href="#tab-display-account"><?php echo $text_display; ?></a>
              <a href="#tab-style-account"><?php echo $text_styles; ?></a>
          </div>

     <div id="tab-titles-account" class="vtabs-content">
          <div id="tab_account_titles" class="htabs">
            <?php foreach ($languages as $language) { ?>
            <a href="#title_account_language<?php echo $language['language_id']; ?>"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a>
            <?php } ?>
          </div>
          
            <?php foreach ($languages as $language) { ?>
            <?php if ($language['status']) { ?>
            <div id="title_account_language<?php echo $language['language_id']; ?>" class="htabs-content">    
                <table class="form">      
                    <tr> 
    					<td><?php echo $entry_title; ?></td> 
    					<td> 
    					<input type="text" name="accountplus_title<?php echo $language['language_id']; ?>" id="accountplus_title<?php echo $language['language_id']; ?>" size="48" value="<?php echo ${'accountplus_title' . $language['language_id']}; ?>" />
    					</td>
    				</tr>
                    
                   <tr> 
    					<td><?php echo $entry_title_account; ?></td> 
    					<td> 
    					<input type="text" name="accountplus_title_account<?php echo $language['language_id']; ?>" id="accountplus_title_account<?php echo $language['language_id']; ?>" size="48" value="<?php echo ${'accountplus_title_account' . $language['language_id']}; ?>" />
    					</td>                    
    				</tr>
                    <tr> 
    					<td><?php echo $entry_title_orders; ?></td> 
    					<td> 
    					<input type="text" name="accountplus_title_orders<?php echo $language['language_id']; ?>" id="accountplus_title_orders<?php echo $language['language_id']; ?>" size="48" value="<?php echo ${'accountplus_title_orders' . $language['language_id']}; ?>" />
    					</td>                    
    				</tr>
                    <tr> 
    					<td><?php echo $entry_title_newsletter; ?></td> 
    					<td> 
    					<input type="text" name="accountplus_title_newsletter<?php echo $language['language_id']; ?>" id="accountplus_title_newsletter<?php echo $language['language_id']; ?>" size="48" value="<?php echo ${'accountplus_title_newsletter' . $language['language_id']}; ?>" />
    					</td>                    
    				</tr>
             </table>  
            </div>   
          <?php } ?>
          <?php } ?>
     </div>
     
     
    <div id="tab-display-account" class="vtabs-content">

          <div id="tab_display_account_general" class="htabs">
            <a href="#general_display_account"><?php echo $text_general_display; ?></a>
            <a href="#account_link"><?php echo $text_links_display; ?></a>
            <a href="#extra_link_account"><?php echo $text_extra_display; ?></a>
          </div>

       <div id="general_display_account" class="htabs-content">         
       <table class="form"> 
            <tr>
				<td colspan="2" bgcolor="#F7F7F7"><b><?php echo $text_general_display; ?></b></td>
			</tr>
            
            <tr> 
				<td><?php echo $entry_tabs; ?></td> 
				<td> 
					<?php if($accountplus_tabs) { 
					$checked1 = ' checked="checked"'; 
					$checked0 = ''; 
					} else { 
					$checked1 = ''; 
					$checked0 = ' checked="checked"'; 
					} ?> 
				<label for="accountplus_tabs_1"><?php echo $entry_yes; ?></label> 
				<input type="radio"<?php echo $checked1; ?> id="accountplus_tabs_1" name="accountplus_tabs" value="1" /> 
				<label for="accountplus_tabs_0"><?php echo $entry_no; ?></label> 
				<input type="radio"<?php echo $checked0; ?> id="accountplus_tabs_0" name="accountplus_tabs" value="0" /> 
				</td>
			</tr>

            <tr> 
				<td><?php echo $entry_secs; ?></td> 
				<td> 
					<?php if($accountplus_secs) { 
					$checked1 = ' checked="checked"'; 
					$checked0 = ''; 
					} else { 
					$checked1 = ''; 
					$checked0 = ' checked="checked"'; 
					} ?> 
				<label for="accountplus_secs_1"><?php echo $entry_yes; ?></label> 
				<input type="radio"<?php echo $checked1; ?> id="accountplus_secs_1" name="accountplus_secs" value="1" /> 
				<label for="accountplus_secs_0"><?php echo $entry_no; ?></label> 
				<input type="radio"<?php echo $checked0; ?> id="accountplus_secs_0" name="accountplus_secs" value="0" /> 
				</td>
			</tr>

            <tr> 
				<td><?php echo $entry_welcome_a; ?></td> 
				<td> 
					<?php if($accountplus_welcome_a) { 
					$checked1 = ' checked="checked"'; 
					$checked0 = ''; 
					} else { 
					$checked1 = ''; 
					$checked0 = ' checked="checked"'; 
					} ?> 
				<label for="accountplus_welcome_a_1"><?php echo $entry_yes; ?></label> 
				<input type="radio"<?php echo $checked1; ?> id="accountplus_welcome_a_1" name="accountplus_welcome_a" value="1" /> 
				<label for="accountplus_welcome_a_0"><?php echo $entry_no; ?></label> 
				<input type="radio"<?php echo $checked0; ?> id="accountplus_welcome_a_0" name="accountplus_welcome_a" value="0" /> 
				</td>
			</tr>

            <tr> 
				<td><?php echo $entry_acc_icon_a; ?></td> 
				<td> 
					<?php if($accountplus_acc_icon_a) { 
					$checked1 = ' checked="checked"'; 
					$checked0 = ''; 
					} else { 
					$checked1 = ''; 
					$checked0 = ' checked="checked"'; 
					} ?> 
				<label for="accountplus_acc_icon_a_1"><?php echo $entry_yes; ?></label> 
				<input type="radio"<?php echo $checked1; ?> id="accountplus_acc_icon_a_1" name="accountplus_acc_icon_a" value="1" /> 
				<label for="accountplus_acc_icon_a_0"><?php echo $entry_no; ?></label> 
				<input type="radio"<?php echo $checked0; ?> id="accountplus_acc_icon_a_0" name="accountplus_acc_icon_a" value="0" /> 
				</td>
			</tr>

            <tr> 
				<td><?php echo $entry_balance_a; ?></td> 
				<td> 
					<?php if($accountplus_balance_a) { 
					$checked1 = ' checked="checked"'; 
					$checked0 = ''; 
					} else { 
					$checked1 = ''; 
					$checked0 = ' checked="checked"'; 
					} ?> 
				<label for="accountplus_balance_a_1"><?php echo $entry_yes; ?></label> 
				<input type="radio"<?php echo $checked1; ?> id="accountplus_balance_a_1" name="accountplus_balance_a" value="1" /> 
				<label for="accountplus_balance_a_0"><?php echo $entry_no; ?></label> 
				<input type="radio"<?php echo $checked0; ?> id="accountplus_balance_a_0" name="accountplus_balance_a" value="0" /> 
				</td>
			</tr>

            <tr> 
				<td><?php echo $entry_reward_a; ?></td> 
				<td> 
					<?php if($accountplus_reward_a) { 
					$checked1 = ' checked="checked"'; 
					$checked0 = ''; 
					} else { 
					$checked1 = ''; 
					$checked0 = ' checked="checked"'; 
					} ?> 
				<label for="accountplus_reward_a_1"><?php echo $entry_yes; ?></label> 
				<input type="radio"<?php echo $checked1; ?> id="accountplus_reward_a_1" name="accountplus_reward_a" value="1" /> 
				<label for="accountplus_reward_a_0"><?php echo $entry_no; ?></label> 
				<input type="radio"<?php echo $checked0; ?> id="accountplus_reward_a_0" name="accountplus_reward_a" value="0" /> 
				</td>
			</tr>

            <tr> 
				<td><?php echo $entry_register_a; ?></td> 
				<td> 
					<?php if($accountplus_register_a) { 
					$checked1 = ' checked="checked"'; 
					$checked0 = ''; 
					} else { 
					$checked1 = ''; 
					$checked0 = ' checked="checked"'; 
					} ?> 
				<label for="accountplus_register_a_1"><?php echo $entry_yes; ?></label> 
				<input type="radio"<?php echo $checked1; ?> id="accountplus_register_a_1" name="accountplus_register_a" value="1" /> 
				<label for="accountplus_register_a_0"><?php echo $entry_no; ?></label> 
				<input type="radio"<?php echo $checked0; ?> id="accountplus_register_a_0" name="accountplus_register_a" value="0" /> 
				</td>
			</tr>

        </table>
    </div>

    <div id="account_link" class="htabs-content">
       <table class="form">           
            <tr>
				<td colspan="2" bgcolor="#F7F7F7"><b><?php echo $text_links_display_desc; ?></b></td>
			</tr>
            
            <td colspan="1" bgcolor="#F9F9F9"><b><?php echo $text_account_link; ?></b></td>
            
             <tr> 
				<td><?php echo $entry_edit_link; ?></td> 
				<td> 
					<?php if($accountplus_edit_link) { 
					$checked1 = ' checked="checked"'; 
					$checked0 = ''; 
					} else { 
					$checked1 = ''; 
					$checked0 = ' checked="checked"'; 
					} ?> 
				<label for="accountplus_edit_link_1"><?php echo $entry_yes; ?></label> 
				<input type="radio"<?php echo $checked1; ?> id="accountplus_edit_link_1" name="accountplus_edit_link" value="1" /> 
				<label for="accountplus_edit_link_0"><?php echo $entry_no; ?></label> 
				<input type="radio"<?php echo $checked0; ?> id="accountplus_edit_link_0" name="accountplus_edit_link" value="0" /> 
				</td> 
			</tr>
            
             <tr> 
				<td><?php echo $entry_password_link; ?></td> 
				<td> 
					<?php if($accountplus_password_link) { 
					$checked1 = ' checked="checked"'; 
					$checked0 = ''; 
					} else { 
					$checked1 = ''; 
					$checked0 = ' checked="checked"'; 
					} ?> 
				<label for="accountplus_password_link_1"><?php echo $entry_yes; ?></label> 
				<input type="radio"<?php echo $checked1; ?> id="accountplus_password_link_1" name="accountplus_password_link" value="1" /> 
				<label for="accountplus_password_link_0"><?php echo $entry_no; ?></label> 
				<input type="radio"<?php echo $checked0; ?> id="accountplus_password_link_0" name="accountplus_password_link" value="0" /> 
				</td> 
			</tr>
            
             <tr> 
				<td><?php echo $entry_address_link; ?></td> 
				<td> 
					<?php if($accountplus_address_link) { 
					$checked1 = ' checked="checked"'; 
					$checked0 = ''; 
					} else { 
					$checked1 = ''; 
					$checked0 = ' checked="checked"'; 
					} ?> 
				<label for="accountplus_address_link_1"><?php echo $entry_yes; ?></label> 
				<input type="radio"<?php echo $checked1; ?> id="accountplus_address_link_1" name="accountplus_address_link" value="1" /> 
				<label for="accountplus_address_link_0"><?php echo $entry_no; ?></label> 
				<input type="radio"<?php echo $checked0; ?> id="accountplus_address_link_0" name="accountplus_address_link" value="0" /> 
				</td> 
			</tr>
            
             <tr> 
				<td><?php echo $entry_wishlist_link; ?></td> 
				<td> 
					<?php if($accountplus_wishlist_link) { 
					$checked1 = ' checked="checked"'; 
					$checked0 = ''; 
					} else { 
					$checked1 = ''; 
					$checked0 = ' checked="checked"'; 
					} ?> 
				<label for="accountplus_wishlist_link_1"><?php echo $entry_yes; ?></label> 
				<input type="radio"<?php echo $checked1; ?> id="accountplus_wishlist_link_1" name="accountplus_wishlist_link" value="1" /> 
				<label for="accountplus_wishlist_link_0"><?php echo $entry_no; ?></label> 
				<input type="radio"<?php echo $checked0; ?> id="accountplus_wishlist_link_0" name="accountplus_wishlist_link" value="0" /> 
				</td> 
			</tr>
            
             <tr> 
				<td><?php echo $entry_logout_link; ?></td> 
				<td> 
					<?php if($accountplus_logout_link) { 
					$checked1 = ' checked="checked"'; 
					$checked0 = ''; 
					} else { 
					$checked1 = ''; 
					$checked0 = ' checked="checked"'; 
					} ?> 
				<label for="accountplus_logout_link_1"><?php echo $entry_yes; ?></label> 
				<input type="radio"<?php echo $checked1; ?> id="accountplus_logout_link_1" name="accountplus_logout_link" value="1" /> 
				<label for="accountplus_logout_link_0"><?php echo $entry_no; ?></label> 
				<input type="radio"<?php echo $checked0; ?> id="accountplus_logout_link_0" name="accountplus_logout_link" value="0" /> 
				</td> 
			</tr>
            
            <td colspan="1" bgcolor="#F9F9F9"><b><?php echo $text_order_link; ?></b></td>
            
             <tr> 
				<td><?php echo $entry_order_link; ?></td> 
				<td> 
					<?php if($accountplus_order_link) { 
					$checked1 = ' checked="checked"'; 
					$checked0 = ''; 
					} else { 
					$checked1 = ''; 
					$checked0 = ' checked="checked"'; 
					} ?> 
				<label for="accountplus_order_link_1"><?php echo $entry_yes; ?></label> 
				<input type="radio"<?php echo $checked1; ?> id="accountplus_order_link_1" name="accountplus_order_link" value="1" /> 
				<label for="accountplus_order_link_0"><?php echo $entry_no; ?></label> 
				<input type="radio"<?php echo $checked0; ?> id="accountplus_order_link_0" name="accountplus_order_link" value="0" /> 
				</td> 
			</tr>
            
             <tr> 
				<td><?php echo $entry_download_link; ?></td> 
				<td> 
					<?php if($accountplus_download_link) { 
					$checked1 = ' checked="checked"'; 
					$checked0 = ''; 
					} else { 
					$checked1 = ''; 
					$checked0 = ' checked="checked"'; 
					} ?> 
				<label for="accountplus_download_link_1"><?php echo $entry_yes; ?></label> 
				<input type="radio"<?php echo $checked1; ?> id="accountplus_download_link_1" name="accountplus_download_link" value="1" /> 
				<label for="accountplus_download_link_0"><?php echo $entry_no; ?></label> 
				<input type="radio"<?php echo $checked0; ?> id="accountplus_download_link_0" name="accountplus_download_link" value="0" /> 
				</td> 
			</tr>
            
            <tr> 
				<td><?php echo $entry_reward_link; ?></td> 
				<td> 
					<?php if($accountplus_reward_link) { 
					$checked1 = ' checked="checked"'; 
					$checked0 = ''; 
					} else { 
					$checked1 = ''; 
					$checked0 = ' checked="checked"'; 
					} ?> 
				<label for="accountplus_reward_link_1"><?php echo $entry_yes; ?></label> 
				<input type="radio"<?php echo $checked1; ?> id="accountplus_reward_link_1" name="accountplus_reward_link" value="1" /> 
				<label for="accountplus_reward_link_0"><?php echo $entry_no; ?></label> 
				<input type="radio"<?php echo $checked0; ?> id="accountplus_reward_link_0" name="accountplus_reward_link" value="0" /> 
				</td> 
			</tr>
            
             <tr> 
				<td><?php echo $entry_return_link; ?></td> 
				<td> 
					<?php if($accountplus_return_link) { 
					$checked1 = ' checked="checked"'; 
					$checked0 = ''; 
					} else { 
					$checked1 = ''; 
					$checked0 = ' checked="checked"'; 
					} ?> 
				<label for="accountplus_return_link_1"><?php echo $entry_yes; ?></label> 
				<input type="radio"<?php echo $checked1; ?> id="accountplus_return_link_1" name="accountplus_return_link" value="1" /> 
				<label for="accountplus_return_link_0"><?php echo $entry_no; ?></label> 
				<input type="radio"<?php echo $checked0; ?> id="accountplus_return_link_0" name="accountplus_return_link" value="0" /> 
				</td> 
			</tr>
            
             <tr> 
				<td><?php echo $entry_transaction_link; ?></td> 
				<td> 
					<?php if($accountplus_transaction_link) { 
					$checked1 = ' checked="checked"'; 
					$checked0 = ''; 
					} else { 
					$checked1 = ''; 
					$checked0 = ' checked="checked"'; 
					} ?> 
				<label for="accountplus_transaction_link_1"><?php echo $entry_yes; ?></label> 
				<input type="radio"<?php echo $checked1; ?> id="accountplus_transaction_link_1" name="accountplus_transaction_link" value="1" /> 
				<label for="accountplus_transaction_link_0"><?php echo $entry_no; ?></label> 
				<input type="radio"<?php echo $checked0; ?> id="accountplus_transaction_link_0" name="accountplus_transaction_link" value="0" /> 
				</td> 
			</tr>
            
             <tr> 
				<td><?php echo $entry_recurring_link; ?></td> 
				<td> 
					<?php if($accountplus_recurring_link) { 
					$checked1 = ' checked="checked"'; 
					$checked0 = ''; 
					} else { 
					$checked1 = ''; 
					$checked0 = ' checked="checked"'; 
					} ?> 
				<label for="accountplus_recurring_link_1"><?php echo $entry_yes; ?></label> 
				<input type="radio"<?php echo $checked1; ?> id="accountplus_recurring_link_1" name="accountplus_recurring_link" value="1" /> 
				<label for="accountplus_recurring_link_0"><?php echo $entry_no; ?></label> 
				<input type="radio"<?php echo $checked0; ?> id="accountplus_recurring_link_0" name="accountplus_recurring_link" value="0" /> 
				</td> 
			</tr>

             <td colspan="1" bgcolor="#F9F9F9"><b><?php echo $text_newsletter_link; ?></b></td>
             <tr> 
				<td><?php echo $entry_newsletter_link; ?></td> 
				<td> 
					<?php if($accountplus_newsletter_link) { 
					$checked1 = ' checked="checked"'; 
					$checked0 = ''; 
					} else { 
					$checked1 = ''; 
					$checked0 = ' checked="checked"'; 
					} ?> 
				<label for="accountplus_newsletter_link_1"><?php echo $entry_yes; ?></label> 
				<input type="radio"<?php echo $checked1; ?> id="accountplus_newsletter_link_1" name="accountplus_newsletter_link" value="1" /> 
				<label for="accountplus_newsletter_link_0"><?php echo $entry_no; ?></label> 
				<input type="radio"<?php echo $checked0; ?> id="accountplus_newsletter_link_0" name="accountplus_newsletter_link" value="0" /> 
				</td> 
			</tr>
        </table>
      </div>

      <div id="extra_link_account" class="htabs-content">         
       <table class="form"> 
            <tr>
				<td colspan="2" bgcolor="#F7F7F7"><?php echo $text_extra_display_desc; ?></td>
			</tr>
            <td colspan="1" bgcolor="#F9F9F9"><b><?php echo $text_account_link; ?></b></td>
        </table>

        <table class="list">
          <thead>
            <tr>
              <td class="left"><?php echo $entry_title; ?></td>
              <td class="left"><?php echo $entry_description; ?></td>
              <td class="left"><?php echo $entry_link; ?></td>
              <td class="left"><?php echo $entry_image; ?></td>
              <td class="left"><?php echo $entry_status; ?></td>
            </tr>
          </thead>

          <tbody>
            <tr>
              <td class="left"><?php foreach ($languages as $language) { ?>
                <input type="text" name="accountplus_extra_link_atitle1<?php echo $language['language_id']; ?>" value="<?php echo ${'accountplus_extra_link_atitle1' . $language['language_id']}; ?>" size="32"/>
                <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
                <?php if (isset(${'error_accountplus_extra_link_atitle1' . $language['language_id']})) { ?>
                <span class="error"><?php echo ${'error_accountplus_extra_link_atitle1' . $language['language_id']}; ?></span>
                <?php } ?>
                <?php } ?></td>
              <td class="left"><?php foreach ($languages as $language) { ?>
                <input type="text" name="accountplus_extra_link_adesc1<?php echo $language['language_id']; ?>" value="<?php echo ${'accountplus_extra_link_adesc1' . $language['language_id']}; ?>" size="32"/>
                <img src="view/image/flags/<?php echo $language['image']; ?>" desc="<?php echo $language['name']; ?>" /><br />
                <?php if (isset(${'error_accountplus_extra_link_adesc1' . $language['language_id']})) { ?>
                <span class="error"><?php echo ${'error_accountplus_extra_link_adesc1' . $language['language_id']}; ?></span>
                <?php } ?>
                <?php } ?></td>
              <td class="left"><input type="text" name="accountplus_extra_link_url1" value="<?php echo $accountplus_extra_link_url1; ?>" size="32"/>
                <?php if (isset($error_accountplus_extra_link_url1)) { ?>
                <span class="error"><?php echo $error_accountplus_extra_link_url1; ?></span>
                <?php } ?>
              </td>

              <td class="center"><div class="image" style="border: none;"><img src="<?php echo $accountplus_extra_link_pic1; ?>" alt="" id="accountplus-extra-link-1-pic" />
                <input type="hidden" name="accountplus_extra_link_image1" value="<?php echo $accountplus_extra_link_image1; ?>" id="accountplus-extra-link-1-pic2" />
                <br />
                <a onclick="image_upload('accountplus-extra-link-1-pic2', 'accountplus-extra-link-1-pic');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#accountplus-extra-link-1-pic').attr('src', '<?php echo $no_image; ?>'); $('#accountplus-extra-link-1-pic2').attr('value', '');"><?php echo $text_clear; ?></a></div></td>
              <td><select name="accountplus_extra_link_status1">
                  <?php if ($accountplus_extra_link_status1) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <td class="left"><?php foreach ($languages as $language) { ?>
                <input type="text" name="accountplus_extra_link_atitle2<?php echo $language['language_id']; ?>" value="<?php echo ${'accountplus_extra_link_atitle2' . $language['language_id']}; ?>" size="32"/>
                <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
                <?php if (isset(${'error_accountplus_extra_link_atitle2' . $language['language_id']})) { ?>
                <span class="error"><?php echo ${'error_accountplus_extra_link_atitle2' . $language['language_id']}; ?></span>
                <?php } ?>
                <?php } ?></td>
              <td class="left"><?php foreach ($languages as $language) { ?>
                <input type="text" name="accountplus_extra_link_adesc2<?php echo $language['language_id']; ?>" value="<?php echo ${'accountplus_extra_link_adesc2' . $language['language_id']}; ?>" size="32"/>
                <img src="view/image/flags/<?php echo $language['image']; ?>" desc="<?php echo $language['name']; ?>" /><br />
                <?php if (isset(${'error_accountplus_extra_link_adesc2' . $language['language_id']})) { ?>
                <span class="error"><?php echo ${'error_accountplus_extra_link_adesc2' . $language['language_id']}; ?></span>
                <?php } ?>
                <?php } ?></td>
              <td class="left"><input type="text" name="accountplus_extra_link_url2" value="<?php echo $accountplus_extra_link_url2; ?>" size="32"/>
                <?php if (isset($error_accountplus_extra_link_url2)) { ?>
                <span class="error"><?php echo $error_accountplus_extra_link_url2; ?></span>
                <?php } ?>
              </td>
              <td class="center"><div class="image" style="border: none;"><img src="<?php echo $accountplus_extra_link_pic2; ?>" alt="" id="accountplus-extra-link-2-pic" />
                <input type="hidden" name="accountplus_extra_link_image2" value="<?php echo $accountplus_extra_link_image2; ?>" id="accountplus-extra-link-2-pic2" />
                <br />
                <a onclick="image_upload('accountplus-extra-link-2-pic2', 'accountplus-extra-link-2-pic');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#accountplus-extra-link-2-pic').attr('src', '<?php echo $no_image; ?>'); $('#accountplus-extra-link-2-pic2').attr('value', '');"><?php echo $text_clear; ?></a></div></td>
              <td><select name="accountplus_extra_link_status2">
                  <?php if ($accountplus_extra_link_status2) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <td class="left"><?php foreach ($languages as $language) { ?>
                <input type="text" name="accountplus_extra_link_atitle3<?php echo $language['language_id']; ?>" value="<?php echo ${'accountplus_extra_link_atitle3' . $language['language_id']}; ?>" size="32"/>
                <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
                <?php if (isset(${'error_accountplus_extra_link_atitle3' . $language['language_id']})) { ?>
                <span class="error"><?php echo ${'error_accountplus_extra_link_atitle3' . $language['language_id']}; ?></span>
                <?php } ?>
                <?php } ?></td>
              <td class="left"><?php foreach ($languages as $language) { ?>
                <input type="text" name="accountplus_extra_link_adesc3<?php echo $language['language_id']; ?>" value="<?php echo ${'accountplus_extra_link_adesc3' . $language['language_id']}; ?>" size="32"/>
                <img src="view/image/flags/<?php echo $language['image']; ?>" desc="<?php echo $language['name']; ?>" /><br />
                <?php if (isset(${'error_accountplus_extra_link_adesc3' . $language['language_id']})) { ?>
                <span class="error"><?php echo ${'error_accountplus_extra_link_adesc3' . $language['language_id']}; ?></span>
                <?php } ?>
                <?php } ?></td>
              <td class="left"><input type="text" name="accountplus_extra_link_url3" value="<?php echo $accountplus_extra_link_url3; ?>" size="32"/>
                <?php if (isset($error_accountplus_extra_link_url3)) { ?>
                <span class="error"><?php echo $error_accountplus_extra_link_url3; ?></span>
                <?php } ?>
              </td>
              <td class="center"><div class="image" style="border: none;"><img src="<?php echo $accountplus_extra_link_pic3; ?>" alt="" id="accountplus-extra-link-3-pic" />
                <input type="hidden" name="accountplus_extra_link_image3" value="<?php echo $accountplus_extra_link_image3; ?>" id="accountplus-extra-link-3-pic2" />
                <br />
                <a onclick="image_upload('accountplus-extra-link-3-pic2', 'accountplus-extra-link-3-pic');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#accountplus-extra-link-3-pic').attr('src', '<?php echo $no_image; ?>'); $('#accountplus-extra-link-3-pic2').attr('value', '');"><?php echo $text_clear; ?></a></div></td>
              <td><select name="accountplus_extra_link_status3">
                  <?php if ($accountplus_extra_link_status3) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
            </tr>
          </tbody>
        </table>

       <table class="form"> 
            <td colspan="1" bgcolor="#F9F9F9"><b><?php echo $text_order_link; ?></b></td>
       </table>

        <table class="list">
          <thead>
            <tr>
              <td class="left"><?php echo $entry_title; ?></td>
              <td class="left"><?php echo $entry_description; ?></td>
              <td class="left"><?php echo $entry_link; ?></td>
              <td class="left"><?php echo $entry_image; ?></td>
              <td class="left"><?php echo $entry_status; ?></td>
            </tr>
          </thead>

          <tbody>
            <tr>
              <td class="left"><?php foreach ($languages as $language) { ?>
                <input type="text" name="accountplus_extra_link_atitle4<?php echo $language['language_id']; ?>" value="<?php echo ${'accountplus_extra_link_atitle4' . $language['language_id']}; ?>" size="32"/>
                <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
                <?php if (isset(${'error_accountplus_extra_link_atitle4' . $language['language_id']})) { ?>
                <span class="error"><?php echo ${'error_accountplus_extra_link_atitle4' . $language['language_id']}; ?></span>
                <?php } ?>
                <?php } ?></td>
              <td class="left"><?php foreach ($languages as $language) { ?>
                <input type="text" name="accountplus_extra_link_adesc4<?php echo $language['language_id']; ?>" value="<?php echo ${'accountplus_extra_link_adesc4' . $language['language_id']}; ?>" size="32"/>
                <img src="view/image/flags/<?php echo $language['image']; ?>" desc="<?php echo $language['name']; ?>" /><br />
                <?php if (isset(${'error_accountplus_extra_link_adesc4' . $language['language_id']})) { ?>
                <span class="error"><?php echo ${'error_accountplus_extra_link_adesc4' . $language['language_id']}; ?></span>
                <?php } ?>
                <?php } ?></td>
              <td class="left"><input type="text" name="accountplus_extra_link_url4" value="<?php echo $accountplus_extra_link_url4; ?>" size="32"/>
                <?php if (isset($error_accountplus_extra_link_url4)) { ?>
                <span class="error"><?php echo $error_accountplus_extra_link_url4; ?></span>
                <?php } ?>
              </td>
              <td class="center"><div class="image" style="border: none;"><img src="<?php echo $accountplus_extra_link_pic4; ?>" alt="" id="accountplus-extra-link-4-pic" />
                <input type="hidden" name="accountplus_extra_link_image4" value="<?php echo $accountplus_extra_link_image4; ?>" id="accountplus-extra-link-4-pic2" />
                <br />
                <a onclick="image_upload('accountplus-extra-link-4-pic2', 'accountplus-extra-link-4-pic');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#accountplus-extra-link-4-pic').attr('src', '<?php echo $no_image; ?>'); $('#accountplus-extra-link-4-pic2').attr('value', '');"><?php echo $text_clear; ?></a></div></td>
              <td><select name="accountplus_extra_link_status4">
                  <?php if ($accountplus_extra_link_status4) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <td class="left"><?php foreach ($languages as $language) { ?>
                <input type="text" name="accountplus_extra_link_atitle5<?php echo $language['language_id']; ?>" value="<?php echo ${'accountplus_extra_link_atitle5' . $language['language_id']}; ?>" size="32"/>
                <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
                <?php if (isset(${'error_accountplus_extra_link_atitle5' . $language['language_id']})) { ?>
                <span class="error"><?php echo ${'error_accountplus_extra_link_atitle5' . $language['language_id']}; ?></span>
                <?php } ?>
                <?php } ?></td>
              <td class="left"><?php foreach ($languages as $language) { ?>
                <input type="text" name="accountplus_extra_link_adesc5<?php echo $language['language_id']; ?>" value="<?php echo ${'accountplus_extra_link_adesc5' . $language['language_id']}; ?>" size="32"/>
                <img src="view/image/flags/<?php echo $language['image']; ?>" desc="<?php echo $language['name']; ?>" /><br />
                <?php if (isset(${'error_accountplus_extra_link_adesc5' . $language['language_id']})) { ?>
                <span class="error"><?php echo ${'error_accountplus_extra_link_adesc5' . $language['language_id']}; ?></span>
                <?php } ?>
                <?php } ?></td>
              <td class="left"><input type="text" name="accountplus_extra_link_url5" value="<?php echo $accountplus_extra_link_url5; ?>" size="32"/>
                <?php if (isset($error_accountplus_extra_link_url5)) { ?>
                <span class="error"><?php echo $error_accountplus_extra_link_url5; ?></span>
                <?php } ?>
              </td>
              <td class="center"><div class="image" style="border: none;"><img src="<?php echo $accountplus_extra_link_pic5; ?>" alt="" id="accountplus-extra-link-5-pic" />
                <input type="hidden" name="accountplus_extra_link_image5" value="<?php echo $accountplus_extra_link_image5; ?>" id="accountplus-extra-link-5-pic5" />
                <br />
                <a onclick="image_upload('accountplus-extra-link-5-pic5', 'accountplus-extra-link-5-pic');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#accountplus-extra-link-5-pic').attr('src', '<?php echo $no_image; ?>'); $('#accountplus-extra-link-5-pic5').attr('value', '');"><?php echo $text_clear; ?></a></div></td>
              <td><select name="accountplus_extra_link_status5">
                  <?php if ($accountplus_extra_link_status5) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <td class="left"><?php foreach ($languages as $language) { ?>
                <input type="text" name="accountplus_extra_link_atitle6<?php echo $language['language_id']; ?>" value="<?php echo ${'accountplus_extra_link_atitle6' . $language['language_id']}; ?>" size="32"/>
                <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
                <?php if (isset(${'error_accountplus_extra_link_atitle6' . $language['language_id']})) { ?>
                <span class="error"><?php echo ${'error_accountplus_extra_link_atitle6' . $language['language_id']}; ?></span>
                <?php } ?>
                <?php } ?></td>
              <td class="left"><?php foreach ($languages as $language) { ?>
                <input type="text" name="accountplus_extra_link_adesc6<?php echo $language['language_id']; ?>" value="<?php echo ${'accountplus_extra_link_adesc6' . $language['language_id']}; ?>" size="32"/>
                <img src="view/image/flags/<?php echo $language['image']; ?>" desc="<?php echo $language['name']; ?>" /><br />
                <?php if (isset(${'error_accountplus_extra_link_adesc6' . $language['language_id']})) { ?>
                <span class="error"><?php echo ${'error_accountplus_extra_link_adesc6' . $language['language_id']}; ?></span>
                <?php } ?>
                <?php } ?></td>
              <td class="left"><input type="text" name="accountplus_extra_link_url6" value="<?php echo $accountplus_extra_link_url6; ?>" size="32"/>
                <?php if (isset($error_accountplus_extra_link_url6)) { ?>
                <span class="error"><?php echo $error_accountplus_extra_link_url6; ?></span>
                <?php } ?>
              </td>
              <td class="center"><div class="image" style="border: none;"><img src="<?php echo $accountplus_extra_link_pic6; ?>" alt="" id="accountplus-extra-link-6-pic" />
                <input type="hidden" name="accountplus_extra_link_image6" value="<?php echo $accountplus_extra_link_image6; ?>" id="accountplus-extra-link-6-pic2" />
                <br />
                <a onclick="image_upload('accountplus-extra-link-6-pic2', 'accountplus-extra-link-6-pic');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#accountplus-extra-link-6-pic').attr('src', '<?php echo $no_image; ?>'); $('#accountplus-extra-link-6-pic2').attr('value', '');"><?php echo $text_clear; ?></a></div></td>
              <td><select name="accountplus_extra_link_status6">
                  <?php if ($accountplus_extra_link_status6) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
            </tr>
          </tbody>
        </table>

      </div>

    </div>

     <div id="tab-style-account" class="vtabs-content">
        <table class="form">
          
           <tr>
             <td><label><?php echo $entry_style; ?></label></td>
    	  <td>
            <select name="style_a" id="selectStyle_a">
            	<option value="default" <?php if($style_a == 'default') { echo " selected"; }?> >Simple</option>
                <option value="icons-1" <?php if($style_a == 'icons-1') { echo " selected"; }?> >Simple Icons</option>
                <option value="icons-2" <?php if($style_a == 'icons-2') { echo " selected"; }?> >Page Icons</option>
                <option value="icons-3" <?php if($style_a == 'icons-3') { echo " selected"; }?> >Modern List</option>
                <option value="icons-4" <?php if($style_a == 'icons-4') { echo " selected"; }?> >Accordion - Simple</option>
                <option value="icons-5" <?php if($style_a == 'icons-5') { echo " selected"; }?> >Accordion - Icons</option>
                <option value="icons-6" <?php if($style_a == 'icons-6') { echo " selected"; }?> >Metro - Style 1</option>
                <option value="icons-7" <?php if($style_a == 'icons-7') { echo " selected"; }?> >Metro - Style 2</option>
                <option value="icons-8" <?php if($style_a == 'icons-8') { echo " selected"; }?> >Flexy</option>
                <option value="icons-9" <?php if($style_a == 'icons-9') { echo " selected"; }?> >Square</option>
                <option value="icons-10" <?php if($style_a == 'icons-10') { echo " selected"; }?> >Simple Box</option>
                <option value="icons-11" <?php if($style_a == 'icons-11') { echo " selected"; }?> >Reality</option>
                <option value="icons-12" <?php if($style_a == 'icons-12') { echo " selected"; }?> >Simpla</option>
                <option value="icons-13" <?php if($style_a == 'icons-13') { echo " selected"; }?> >Light Box</option>
                <option value="icons-14" <?php if($style_a == 'icons-14') { echo " selected"; }?> >Wuxia</option>
            </select>
           </td>
           <td>
               <div id="style_a_default" class="style_abox"><img src="view/image/accountplus/default.png" alt="" /></div>
               <div id="style_a_icons-1" class="style_abox"><img src="view/image/accountplus/icons-1.png" alt="" /></div>
               <div id="style_a_icons-2" class="style_abox"><img src="view/image/accountplus/icons-2.png" alt="" /></div>
               <div id="style_a_icons-3" class="style_abox"><img src="view/image/accountplus/icons-3.png" alt="" /></div>
               <div id="style_a_icons-4" class="style_abox"><img src="view/image/accountplus/icons-4.png" alt="" /></div>
               <div id="style_a_icons-5" class="style_abox"><img src="view/image/accountplus/icons-5.png" alt="" /></div>
               <div id="style_a_icons-6" class="style_abox"><img src="view/image/accountplus/icons-6.png" alt="" /></div>
               <div id="style_a_icons-7" class="style_abox"><img src="view/image/accountplus/icons-7.png" alt="" /></div>
               <div id="style_a_icons-8" class="style_abox"><img src="view/image/accountplus/icons-8.png" alt="" /></div>
               <div id="style_a_icons-9" class="style_abox"><img src="view/image/accountplus/icons-9.png" alt="" /></div>
               <div id="style_a_icons-10" class="style_abox"><img src="view/image/accountplus/icons-10.png" alt="" /></div>
               <div id="style_a_icons-11" class="style_abox"><img src="view/image/accountplus/icons-11.png" alt="" /></div>
               <div id="style_a_icons-12" class="style_abox"><img src="view/image/accountplus/icons-12.png" alt="" /></div>
               <div id="style_a_icons-13" class="style_abox"><img src="view/image/accountplus/icons-13.png" alt="" /></div>
               <div id="style_a_icons-14" class="style_abox"><img src="view/image/accountplus/icons-14.png" alt="" /></div>
           </td>
    	   </tr>
            
            <tr>
                 <td><label><?php echo $entry_icotype; ?></label></td>
    		  <td>
                <select name="icotype_a" id="selectIcotype_a">
                    <option value="ballicons" <?php if($icotype_a == 'ballicons') { echo " selected"; }?> >Ballicons</option>
                    <option value="balloonica" <?php if($icotype_a == 'balloonica') { echo " selected"; }?> >Balloonica</option>
                    <option value="black_moon" <?php if($icotype_a == 'black_moon') { echo " selected"; }?> >Black Moon</option>
                    <option value="blue_velvet" <?php if($icotype_a == 'blue_velvet') { echo " selected"; }?> >Blue Velvet</option>
                    <option value="bubbles_colorful" <?php if($icotype_a == 'bubbles_colorful') { echo " selected"; }?> >Bubbles Colorful</option>
                    <option value="bubbles" <?php if($icotype_a == 'bubbles') { echo " selected"; }?> >Bubbles</option>
                    <option value="colorful_stickers" <?php if($icotype_a == 'colorful_stickers') { echo " selected"; }?> >Colorful Stickers</option>
                    <option value="coquette" <?php if($icotype_a == 'coquette') { echo " selected"; }?> >Coquette</option>
                    <option value="distortion" <?php if($icotype_a == 'distortion') { echo " selected"; }?> >Distortion</option>
                    <option value="fresh_buttons" <?php if($icotype_a == 'fresh_buttons') { echo " selected"; }?> >Fresh Buttons</option>
                    <option value="goodies" <?php if($icotype_a == 'goodies') { echo " selected"; }?> >Goodies</option>
                    <option value="handy" <?php if($icotype_a == 'handy') { echo " selected"; }?> >Handy</option>
                    <option value="luna_blue" <?php if($icotype_a == 'luna_blue') { echo " selected"; }?> >Luna Blue</option>
                    <option value="luna_grey" <?php if($icotype_a == 'luna_grey') { echo " selected"; }?> >Luna Grey</option>
                    <option value="metro_black" <?php if($icotype_a == 'metro_black') { echo " selected"; }?> >Metro Black</option>
                    <option value="metro_white" <?php if($icotype_a == 'metro_white') { echo " selected"; }?> >Metro White</option>
                    <option value="minimalistica_blue" <?php if($icotype_a == 'minimalistica_blue') { echo " selected"; }?> >Minimalistica Blue</option>
                    <option value="minimalistica_red" <?php if($icotype_a == 'minimalistica_red') { echo " selected"; }?> >Minimalistica Red</option>
                    <option value="mobi" <?php if($icotype_a == 'mobi') { echo " selected"; }?> >Mobi</option>
                    <option value="modern_round" <?php if($icotype_a == 'modern_round') { echo " selected"; }?> >Modern Round</option>
                    <option value="moon_blue" <?php if($icotype_a == 'moon_blue') { echo " selected"; }?> >Moon Blue</option>
                    <option value="moon_red" <?php if($icotype_a == 'moon_red') { echo " selected"; }?> >Moon Red</option>
                    <option value="reflection_blue" <?php if($icotype_a == 'reflection_blue') { echo " selected"; }?> >Reflection Blue</option>
                    <option value="reflection_red" <?php if($icotype_a == 'reflection_red') { echo " selected"; }?> >Reflection Red</option>
                    <option value="softies" <?php if($icotype_a == 'softies') { echo " selected"; }?> >Softies</option>
                    <option value="square_blue" <?php if($icotype_a == 'square_blue') { echo " selected"; }?> >Square Blue</option>
                    <option value="stickers" <?php if($icotype_a == 'stickers') { echo " selected"; }?> >Stickers</option>
                    <option value="stylish" <?php if($icotype_a == 'stylish') { echo " selected"; }?> >Stylish</option>
                    <option value="stylistica" <?php if($icotype_a == 'stylistica') { echo " selected"; }?> >Stylistica</option>
                    <option value="symbolize" <?php if($icotype_a == 'symbolize') { echo " selected"; }?> >Symbolize</option>
                    <option value="web_colorful" <?php if($icotype_a == 'web_colorful') { echo " selected"; }?> >Web Colorful</option>
                    <option value="web_purple" <?php if($icotype_a == 'web_purple') { echo " selected"; }?> >Web Purple</option>
            </select>
           </td>
           <td>

               <div id="icotype_a_ballicons" class="icotype_abox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/ballicons/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/ballicons/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/ballicons/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/ballicons/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/ballicons/id.png" alt="" />
               </div>

               <div id="icotype_a_balloonica" class="icotype_abox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/balloonica/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/balloonica/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/balloonica/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/balloonica/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/balloonica/id.png" alt="" />
               </div>
               
               <div id="icotype_a_black_moon" class="icotype_abox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/black_moon/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/black_moon/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/black_moon/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/black_moon/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/black_moon/id.png" alt="" />
               </div>
               
               <div id="icotype_a_blue_velvet" class="icotype_abox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/blue_velvet/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/blue_velvet/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/blue_velvet/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/blue_velvet/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/blue_velvet/id.png" alt="" />
               </div>

               <div id="icotype_a_bubbles" class="icotype_abox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/bubbles/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/bubbles/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/bubbles/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/bubbles/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/bubbles/id.png" alt="" />
               </div>

               <div id="icotype_a_bubbles_colorful" class="icotype_abox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/bubbles_colorful/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/bubbles_colorful/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/bubbles_colorful/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/bubbles_colorful/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/bubbles_colorful/id.png" alt="" />
               </div>
               
               <div id="icotype_a_colorful_stickers" class="icotype_abox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/colorful_stickers/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/colorful_stickers/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/colorful_stickers/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/colorful_stickers/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/colorful_stickers/id.png" alt="" />
               </div>
               
               <div id="icotype_a_coquette" class="icotype_abox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/coquette/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/coquette/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/coquette/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/coquette/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/coquette/id.png" alt="" />
               </div>

               <div id="icotype_a_distortion" class="icotype_abox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/distortion/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/distortion/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/distortion/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/distortion/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/distortion/id.png" alt="" />
               </div>

               <div id="icotype_a_fresh_buttons" class="icotype_abox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/fresh_buttons/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/fresh_buttons/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/fresh_buttons/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/fresh_buttons/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/fresh_buttons/id.png" alt="" />
               </div>
               
               <div id="icotype_a_goodies" class="icotype_abox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/goodies/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/goodies/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/goodies/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/goodies/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/goodies/id.png" alt="" />
               </div>

               <div id="icotype_a_handy" class="icotype_abox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/handy/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/handy/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/handy/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/handy/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/handy/id.png" alt="" />
               </div>
               
               <div id="icotype_a_luna_blue" class="icotype_abox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/luna_blue/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/luna_blue/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/luna_blue/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/luna_blue/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/luna_blue/id.png" alt="" />
               </div>
               <div id="icotype_a_luna_grey" class="icotype_abox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/luna_grey/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/luna_grey/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/luna_grey/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/luna_grey/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/luna_grey/id.png" alt="" />
               </div>
               <div id="icotype_a_metro_black" class="icotype_abox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/metro_black/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/metro_black/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/metro_black/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/metro_black/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/metro_black/id.png" alt="" />
               </div>
               <div id="icotype_a_metro_white" class="icotype_abox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/metro_white/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/metro_white/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/metro_white/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/metro_white/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/metro_white/id.png" alt="" />
               </div>
               <div id="icotype_a_minimalistica_blue" class="icotype_abox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/minimalistica_blue/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/minimalistica_blue/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/minimalistica_blue/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/minimalistica_blue/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/minimalistica_blue/id.png" alt="" />
               </div>
               <div id="icotype_a_minimalistica_red" class="icotype_abox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/minimalistica_red/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/minimalistica_red/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/minimalistica_red/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/minimalistica_red/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/minimalistica_red/id.png" alt="" />
               </div>

               <div id="icotype_a_mobi" class="icotype_abox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/mobi/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/mobi/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/mobi/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/mobi/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/mobi/id.png" alt="" />
               </div>

               <div id="icotype_a_modern_round" class="icotype_abox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/modern_round/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/modern_round/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/modern_round/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/modern_round/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/modern_round/id.png" alt="" />
               </div>

               <div id="icotype_a_moon_blue" class="icotype_abox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/moon_blue/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/moon_blue/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/moon_blue/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/moon_blue/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/moon_blue/id.png" alt="" />
               </div>
               <div id="icotype_a_moon_red" class="icotype_abox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/moon_red/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/moon_red/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/moon_red/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/moon_red/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/moon_red/id.png" alt="" />
               </div>

               <div id="icotype_a_reflection_blue" class="icotype_abox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/reflection_blue/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/reflection_blue/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/reflection_blue/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/reflection_blue/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/reflection_blue/id.png" alt="" />
               </div>

               <div id="icotype_a_reflection_red" class="icotype_abox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/reflection_red/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/reflection_red/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/reflection_red/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/reflection_red/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/reflection_red/id.png" alt="" />
               </div>

               <div id="icotype_a_softies" class="icotype_abox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/softies/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/softies/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/softies/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/softies/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/softies/id.png" alt="" />
               </div>

               <div id="icotype_a_square_blue" class="icotype_abox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/square_blue/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/square_blue/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/square_blue/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/square_blue/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/square_blue/id.png" alt="" />
               </div>

               <div id="icotype_a_stickers" class="icotype_abox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/stickers/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/stickers/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/stickers/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/stickers/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/stickers/id.png" alt="" />
               </div>

               <div id="icotype_a_stylish" class="icotype_abox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/stylish/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/stylish/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/stylish/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/stylish/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/stylish/id.png" alt="" />
               </div>

               <div id="icotype_a_stylistica" class="icotype_abox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/stylistica/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/stylistica/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/stylistica/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/stylistica/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/stylistica/id.png" alt="" />
               </div>

               <div id="icotype_a_symbolize" class="icotype_abox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/symbolize/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/symbolize/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/symbolize/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/symbolize/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/symbolize/id.png" alt="" />
               </div>

               <div id="icotype_a_web_colorful" class="icotype_abox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/web_colorful/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/web_colorful/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/web_colorful/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/web_colorful/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/web_colorful/id.png" alt="" />
               </div>

               <div id="icotype_a_web_purple" class="icotype_abox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/web_purple/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/web_purple/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/web_purple/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/web_purple/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/web_purple/id.png" alt="" />
               </div>
           </td>
    	 	</tr>
           </table> 
       </div>        
   </div>   
        
   <div id="tab-affiliate">

      <div id="tabs-affiliate" class="vtabs">
          <a href="#tab-titles-affiliate"><?php echo $text_custom_title; ?></a>
          <a href="#tab-display-affiliate"><?php echo $text_display; ?></a>
          <a href="#tab-style-affiliate"><?php echo $text_styles; ?></a>
      </div>

     <div id="tab-titles-affiliate" class="vtabs-content">

          <div id="tab_affiliate_titles" class="htabs">
            <?php foreach ($languages as $language) { ?>
            <a href="#title_affiliate_language<?php echo $language['language_id']; ?>"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a>
            <?php } ?>
          </div>
          
            <?php foreach ($languages as $language) { ?>
            <?php if ($language['status']) { ?>
            <div id="title_affiliate_language<?php echo $language['language_id']; ?>" class="htabs-content">    
                <table class="form">      
                    <tr> 
    					<td><?php echo $entry_title_a; ?></td> 
    					<td> 
    					<input type="text" name="accountplus_title_a<?php echo $language['language_id']; ?>" id="accountplus_title_a<?php echo $language['language_id']; ?>" size="48" value="<?php echo ${'accountplus_title_a' . $language['language_id']}; ?>" />
    					</td>
    				</tr>
                    
                   <tr> 
    					<td><?php echo $entry_title_account_a; ?></td> 
    					<td> 
    					<input type="text" name="accountplus_title_account_a<?php echo $language['language_id']; ?>" id="accountplus_title_account_a<?php echo $language['language_id']; ?>" size="48" value="<?php echo ${'accountplus_title_account_a' . $language['language_id']}; ?>" />
    					</td>                    
    				</tr>
                    <tr> 
    					<td><?php echo $entry_title_tracking_a; ?></td> 
    					<td> 
    					<input type="text" name="accountplus_title_tracking_a<?php echo $language['language_id']; ?>" id="accountplus_title_orders_a<?php echo $language['language_id']; ?>" size="48" value="<?php echo ${'accountplus_title_tracking_a' . $language['language_id']}; ?>" />
    					</td>                    
    				</tr>
                    <tr> 
    					<td><?php echo $entry_title_transaction_a; ?></td> 
    					<td> 
    					<input type="text" name="accountplus_title_transaction_a<?php echo $language['language_id']; ?>" id="accountplus_title_newsletter_a<?php echo $language['language_id']; ?>" size="48" value="<?php echo ${'accountplus_title_transaction_a' . $language['language_id']}; ?>" />
    					</td>                    
    				</tr>
             </table>  
            </div>   
          <?php } ?>
          <?php } ?>
     </div> 

    <div id="tab-display-affiliate" class="vtabs-content">

          <div id="tab_display_affiliate_general" class="htabs">
            <a href="#general_display_affiliate"><?php echo $text_general_display; ?></a>
            <a href="#affiliate_link"><?php echo $text_links_display; ?></a>
            <a href="#extra_link_affiliate"><?php echo $text_extra_display; ?></a>
          </div>

    <div id="general_display_affiliate" class="htabs-content">
        <table class="form">
            <tr>
				<td colspan="2" bgcolor="#F7F7F7"><b><?php echo $text_general_display; ?></b></td>
			</tr>
            
            <tr> 
				<td><?php echo $entry_tabs_a; ?></td> 
				<td> 
					<?php if($accountplus_tabs_a) { 
					$checked1 = ' checked="checked"'; 
					$checked0 = ''; 
					} else { 
					$checked1 = ''; 
					$checked0 = ' checked="checked"'; 
					} ?> 
				<label for="accountplus_tabs_a_1"><?php echo $entry_yes; ?></label> 
				<input type="radio"<?php echo $checked1; ?> id="accountplus_tabs_a_1" name="accountplus_tabs_a" value="1" /> 
				<label for="accountplus_tabs_a_0"><?php echo $entry_no; ?></label> 
				<input type="radio"<?php echo $checked0; ?> id="accountplus_tabs_a_0" name="accountplus_tabs_a" value="0" /> 
				</td>
			</tr>

            <tr> 
				<td><?php echo $entry_secs_af; ?></td> 
				<td> 
					<?php if($accountplus_secs_af) { 
					$checked1 = ' checked="checked"'; 
					$checked0 = ''; 
					} else { 
					$checked1 = ''; 
					$checked0 = ' checked="checked"'; 
					} ?> 
				<label for="accountplus_secs_af_1"><?php echo $entry_yes; ?></label> 
				<input type="radio"<?php echo $checked1; ?> id="accountplus_secs_af_1" name="accountplus_secs_af" value="1" /> 
				<label for="accountplus_secs_af_0"><?php echo $entry_no; ?></label> 
				<input type="radio"<?php echo $checked0; ?> id="accountplus_secs_af_0" name="accountplus_secs_af" value="0" /> 
				</td>
			</tr>

            <tr> 
				<td><?php echo $entry_welcome_af; ?></td> 
				<td> 
					<?php if($accountplus_welcome_af) { 
					$checked1 = ' checked="checked"'; 
					$checked0 = ''; 
					} else { 
					$checked1 = ''; 
					$checked0 = ' checked="checked"'; 
					} ?> 
				<label for="accountplus_welcome_af_1"><?php echo $entry_yes; ?></label> 
				<input type="radio"<?php echo $checked1; ?> id="accountplus_welcome_af_1" name="accountplus_welcome_af" value="1" /> 
				<label for="accountplus_welcome_af_0"><?php echo $entry_no; ?></label> 
				<input type="radio"<?php echo $checked0; ?> id="accountplus_welcome_af_0" name="accountplus_welcome_af" value="0" /> 
				</td>
			</tr>

            <tr> 
				<td><?php echo $entry_acc_icon_af; ?></td> 
				<td> 
					<?php if($accountplus_acc_icon_af) { 
					$checked1 = ' checked="checked"'; 
					$checked0 = ''; 
					} else { 
					$checked1 = ''; 
					$checked0 = ' checked="checked"'; 
					} ?> 
				<label for="accountplus_acc_icon_af_1"><?php echo $entry_yes; ?></label> 
				<input type="radio"<?php echo $checked1; ?> id="accountplus_acc_icon_af_1" name="accountplus_acc_icon_af" value="1" /> 
				<label for="accountplus_acc_icon_af_0"><?php echo $entry_no; ?></label> 
				<input type="radio"<?php echo $checked0; ?> id="accountplus_acc_icon_af_0" name="accountplus_acc_icon_af" value="0" /> 
				</td>
			</tr>

            <tr> 
				<td><?php echo $entry_balance_af; ?></td> 
				<td> 
					<?php if($accountplus_balance_af) { 
					$checked1 = ' checked="checked"'; 
					$checked0 = ''; 
					} else { 
					$checked1 = ''; 
					$checked0 = ' checked="checked"'; 
					} ?> 
				<label for="accountplus_balance_af_1"><?php echo $entry_yes; ?></label> 
				<input type="radio"<?php echo $checked1; ?> id="accountplus_balance_af_1" name="accountplus_balance_af" value="1" /> 
				<label for="accountplus_balance_af_0"><?php echo $entry_no; ?></label> 
				<input type="radio"<?php echo $checked0; ?> id="accountplus_balance_af_0" name="accountplus_balance_af" value="0" /> 
				</td>
			</tr>

            <tr> 
				<td><?php echo $entry_commi_af; ?></td> 
				<td> 
					<?php if($accountplus_commi_af) { 
					$checked1 = ' checked="checked"'; 
					$checked0 = ''; 
					} else { 
					$checked1 = ''; 
					$checked0 = ' checked="checked"'; 
					} ?> 
				<label for="accountplus_commi_af_1"><?php echo $entry_yes; ?></label> 
				<input type="radio"<?php echo $checked1; ?> id="accountplus_commi_af_1" name="accountplus_commi_af" value="1" /> 
				<label for="accountplus_commi_af_0"><?php echo $entry_no; ?></label> 
				<input type="radio"<?php echo $checked0; ?> id="accountplus_commi_af_0" name="accountplus_commi_af" value="0" /> 
				</td>
			</tr>

            <tr> 
				<td><?php echo $entry_order_af; ?></td> 
				<td> 
					<?php if($accountplus_order_af) { 
					$checked1 = ' checked="checked"'; 
					$checked0 = ''; 
					} else { 
					$checked1 = ''; 
					$checked0 = ' checked="checked"'; 
					} ?> 
				<label for="accountplus_order_af_1"><?php echo $entry_yes; ?></label> 
				<input type="radio"<?php echo $checked1; ?> id="accountplus_order_af_1" name="accountplus_order_af" value="1" /> 
				<label for="accountplus_order_af_0"><?php echo $entry_no; ?></label> 
				<input type="radio"<?php echo $checked0; ?> id="accountplus_order_af_0" name="accountplus_order_af" value="0" /> 
				</td>
			</tr>

            <tr> 
				<td><?php echo $entry_register_af; ?></td> 
				<td> 
					<?php if($accountplus_register_af) { 
					$checked1 = ' checked="checked"'; 
					$checked0 = ''; 
					} else { 
					$checked1 = ''; 
					$checked0 = ' checked="checked"'; 
					} ?> 
				<label for="accountplus_register_af_1"><?php echo $entry_yes; ?></label> 
				<input type="radio"<?php echo $checked1; ?> id="accountplus_register_af_1" name="accountplus_register_af" value="1" /> 
				<label for="accountplus_register_af_0"><?php echo $entry_no; ?></label> 
				<input type="radio"<?php echo $checked0; ?> id="accountplus_register_af_0" name="accountplus_register_af" value="0" /> 
				</td>
			</tr>

         </table>   
    </div>
                
    <div id="affiliate_link" class="htabs-content">
        <table class="form">
            <tr>
				<td colspan="2" bgcolor="#F7F7F7"><b><?php echo $text_links_display_desc; ?></b></td>
			</tr>
            
            <td colspan="1" bgcolor="#F9F9F9"><b><?php echo $text_account_link_a; ?></b></td>
            
             <tr> 
				<td><?php echo $entry_edit_link_a; ?></td> 
				<td> 
					<?php if($accountplus_edit_link_a) { 
					$checked1 = ' checked="checked"'; 
					$checked0 = ''; 
					} else { 
					$checked1 = ''; 
					$checked0 = ' checked="checked"'; 
					} ?> 
				<label for="accountplus_edit_link_a_1"><?php echo $entry_yes; ?></label> 
				<input type="radio"<?php echo $checked1; ?> id="accountplus_edit_link_a_1" name="accountplus_edit_link_a" value="1" /> 
				<label for="accountplus_edit_link_a_0"><?php echo $entry_no; ?></label> 
				<input type="radio"<?php echo $checked0; ?> id="accountplus_edit_link_a_0" name="accountplus_edit_link_a" value="0" /> 
				</td> 
			</tr>
            
             <tr> 
				<td><?php echo $entry_password_link_a; ?></td> 
				<td> 
					<?php if($accountplus_password_link_a) { 
					$checked1 = ' checked="checked"'; 
					$checked0 = ''; 
					} else { 
					$checked1 = ''; 
					$checked0 = ' checked="checked"'; 
					} ?> 
				<label for="accountplus_password_link_a_1"><?php echo $entry_yes; ?></label> 
				<input type="radio"<?php echo $checked1; ?> id="accountplus_password_link_1" name="accountplus_password_link_a" value="1" /> 
				<label for="accountplus_password_link_a_0"><?php echo $entry_no; ?></label> 
				<input type="radio"<?php echo $checked0; ?> id="accountplus_password_link_0" name="accountplus_password_link_a" value="0" /> 
				</td> 
			</tr>
            
             <tr> 
				<td><?php echo $entry_payment_link_a; ?></td> 
				<td> 
					<?php if($accountplus_payment_link_a) { 
					$checked1 = ' checked="checked"'; 
					$checked0 = ''; 
					} else { 
					$checked1 = ''; 
					$checked0 = ' checked="checked"'; 
					} ?> 
				<label for="accountplus_payment_link_a_1"><?php echo $entry_yes; ?></label> 
				<input type="radio"<?php echo $checked1; ?> id="accountplus_payment_link_1" name="accountplus_payment_link_a" value="1" /> 
				<label for="accountplus_payment_link_a_0"><?php echo $entry_no; ?></label> 
				<input type="radio"<?php echo $checked0; ?> id="accountplus_payment_link_0" name="accountplus_payment_link_a" value="0" /> 
				</td> 
			</tr>
            
             <tr> 
				<td><?php echo $entry_logout_link_a; ?></td> 
				<td> 
					<?php if($accountplus_logout_link_a) { 
					$checked1 = ' checked="checked"'; 
					$checked0 = ''; 
					} else { 
					$checked1 = ''; 
					$checked0 = ' checked="checked"'; 
					} ?> 
				<label for="accountplus_logout_link_a_1"><?php echo $entry_yes; ?></label> 
				<input type="radio"<?php echo $checked1; ?> id="accountplus_logout_link_a_1" name="accountplus_logout_link_a" value="1" /> 
				<label for="accountplus_logout_link_a_0"><?php echo $entry_no; ?></label> 
				<input type="radio"<?php echo $checked0; ?> id="accountplus_logout_link_a_0" name="accountplus_logout_link_a" value="0" /> 
				</td> 
			</tr>
            
            <td colspan="1" bgcolor="#F9F9F9"><b><?php echo $text_tracking_link_a; ?></b></td>
            
             <tr> 
				<td><?php echo $entry_tracking_link_a; ?></td> 
				<td> 
					<?php if($accountplus_tracking_link_a) { 
					$checked1 = ' checked="checked"'; 
					$checked0 = ''; 
					} else { 
					$checked1 = ''; 
					$checked0 = ' checked="checked"'; 
					} ?> 
				<label for="accountplus_tracking_link_a_1"><?php echo $entry_yes; ?></label> 
				<input type="radio"<?php echo $checked1; ?> id="accountplus_tracking_link_a_1" name="accountplus_tracking_link_a" value="1" /> 
				<label for="accountplus_tracking_link_a_0"><?php echo $entry_no; ?></label> 
				<input type="radio"<?php echo $checked0; ?> id="accountplus_tracking_link_a_0" name="accountplus_tracking_link_a" value="0" /> 
				</td> 
			</tr>
            
             <td colspan="1" bgcolor="#F9F9F9"><b><?php echo $text_transaction_link_a; ?></b></td>
            <tr> 
				<td><?php echo $entry_transaction_link_a; ?></td> 
				<td> 
					<?php if($accountplus_transaction_link_a) { 
					$checked1 = ' checked="checked"'; 
					$checked0 = ''; 
					} else { 
					$checked1 = ''; 
					$checked0 = ' checked="checked"'; 
					} ?> 
				<label for="accountplus_transaction_link_a_1"><?php echo $entry_yes; ?></label> 
				<input type="radio"<?php echo $checked1; ?> id="accountplus_transaction_link_a_1" name="accountplus_transaction_link_a" value="1" /> 
				<label for="accountplus_transaction_link_a_0"><?php echo $entry_no; ?></label> 
				<input type="radio"<?php echo $checked0; ?> id="accountplus_transaction_link_a_0" name="accountplus_transaction_link_a" value="0" /> 
				</td> 
			</tr>
        </table>
      </div>


      <div id="extra_link_affiliate" class="htabs-content">         
       <table class="form"> 
            <tr>
				<td colspan="2" bgcolor="#F7F7F7"><?php echo $text_extra_display_desc; ?></td>
			</tr>
            <td colspan="1" bgcolor="#F9F9F9"><b><?php echo $text_account_link_a; ?></b></td>
        </table>

        <table class="list">
          <thead>
            <tr>
              <td class="left"><?php echo $entry_title; ?></td>
              <td class="left"><?php echo $entry_description; ?></td>
              <td class="left"><?php echo $entry_link; ?></td>
              <td class="left"><?php echo $entry_image; ?></td>
              <td class="left"><?php echo $entry_status; ?></td>
            </tr>
          </thead>

          <tbody>
            <tr>
              <td class="left"><?php foreach ($languages as $language) { ?>
                <input type="text" name="accountplus_extra_link_afatitle1<?php echo $language['language_id']; ?>" value="<?php echo ${'accountplus_extra_link_afatitle1' . $language['language_id']}; ?>" size="32"/>
                <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
                <?php if (isset(${'error_accountplus_extra_link_afatitle1' . $language['language_id']})) { ?>
                <span class="error"><?php echo ${'error_accountplus_extra_link_afatitle1' . $language['language_id']}; ?></span>
                <?php } ?>
                <?php } ?></td>
              <td class="left"><?php foreach ($languages as $language) { ?>
                <input type="text" name="accountplus_extra_link_afadesc1<?php echo $language['language_id']; ?>" value="<?php echo ${'accountplus_extra_link_afadesc1' . $language['language_id']}; ?>" size="32"/>
                <img src="view/image/flags/<?php echo $language['image']; ?>" desc="<?php echo $language['name']; ?>" /><br />
                <?php if (isset(${'error_accountplus_extra_link_afadesc1' . $language['language_id']})) { ?>
                <span class="error"><?php echo ${'error_accountplus_extra_link_afadesc1' . $language['language_id']}; ?></span>
                <?php } ?>
                <?php } ?></td>
              <td class="left"><input type="text" name="accountplus_extra_link_afurl1" value="<?php echo $accountplus_extra_link_afurl1; ?>" size="32"/>
                <?php if (isset($error_accountplus_extra_link_afurl1)) { ?>
                <span class="error"><?php echo $error_accountplus_extra_link_afurl1; ?></span>
                <?php } ?>
              </td>
              <td class="center"><div class="image" style="border: none;"><img src="<?php echo $accountplus_extra_link_afpic1; ?>" alt="" id="accountplus-extra-linkaf-1-pic" />
                <input type="hidden" name="accountplus_extra_link_afimage1" value="<?php echo $accountplus_extra_link_afimage1; ?>" id="accountplus-extra-linkaf-1-pic2" />
                <br />
                <a onclick="image_upload('accountplus-extra-linkaf-1-pic2', 'accountplus-extra-linkaf-1-pic');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#accountplus-extra-linkaf-1-pic').attr('src', '<?php echo $no_image; ?>'); $('#accountplus-extra-linkaf-1-pic2').attr('value', '');"><?php echo $text_clear; ?></a></div></td>
              <td><select name="accountplus_extra_link_afstatus1">
                  <?php if ($accountplus_extra_link_afstatus1) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <td class="left"><?php foreach ($languages as $language) { ?>
                <input type="text" name="accountplus_extra_link_afatitle2<?php echo $language['language_id']; ?>" value="<?php echo ${'accountplus_extra_link_afatitle2' . $language['language_id']}; ?>" size="32"/>
                <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
                <?php if (isset(${'error_accountplus_extra_link_afatitle2' . $language['language_id']})) { ?>
                <span class="error"><?php echo ${'error_accountplus_extra_link_afatitle2' . $language['language_id']}; ?></span>
                <?php } ?>
                <?php } ?></td>
              <td class="left"><?php foreach ($languages as $language) { ?>
                <input type="text" name="accountplus_extra_link_afadesc2<?php echo $language['language_id']; ?>" value="<?php echo ${'accountplus_extra_link_afadesc2' . $language['language_id']}; ?>" size="32"/>
                <img src="view/image/flags/<?php echo $language['image']; ?>" desc="<?php echo $language['name']; ?>" /><br />
                <?php if (isset(${'error_accountplus_extra_link_afadesc2' . $language['language_id']})) { ?>
                <span class="error"><?php echo ${'error_accountplus_extra_link_afadesc2' . $language['language_id']}; ?></span>
                <?php } ?>
                <?php } ?></td>
              <td class="left"><input type="text" name="accountplus_extra_link_afurl2" value="<?php echo $accountplus_extra_link_afurl2; ?>" size="32"/>
                <?php if (isset($error_accountplus_extra_link_afurl2)) { ?>
                <span class="error"><?php echo $error_accountplus_extra_link_afurl2; ?></span>
                <?php } ?>
              </td>
              <td class="center"><div class="image" style="border: none;"><img src="<?php echo $accountplus_extra_link_afpic2; ?>" alt="" id="accountplus-extra-linkaf-2-pic" />
                <input type="hidden" name="accountplus_extra_link_afimage2" value="<?php echo $accountplus_extra_link_afimage2; ?>" id="accountplus-extra-linkaf-2-pic2" />
                <br />
                <a onclick="image_upload('accountplus-extra-linkaf-2-pic2', 'accountplus-extra-linkaf-2-pic');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#accountplus-extra-linkaf-2-pic').attr('src', '<?php echo $no_image; ?>'); $('#accountplus-extra-linkaf-2-pic2').attr('value', '');"><?php echo $text_clear; ?></a></div></td>
              <td><select name="accountplus_extra_link_afstatus2">
                  <?php if ($accountplus_extra_link_afstatus2) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
            </tr>
          </tbody>
        </table>

       <table class="form"> 
            <td colspan="1" bgcolor="#F9F9F9"><b><?php echo $text_tracking_link_a; ?></b></td>
       </table>

        <table class="list">
          <thead>
            <tr>
              <td class="left"><?php echo $entry_title; ?></td>
              <td class="left"><?php echo $entry_description; ?></td>
              <td class="left"><?php echo $entry_link; ?></td>
              <td class="left"><?php echo $entry_image; ?></td>
              <td class="left"><?php echo $entry_status; ?></td>
            </tr>
          </thead>

          <tbody>
            <tr>
              <td class="left"><?php foreach ($languages as $language) { ?>
                <input type="text" name="accountplus_extra_link_afatitle3<?php echo $language['language_id']; ?>" value="<?php echo ${'accountplus_extra_link_afatitle3' . $language['language_id']}; ?>" size="32"/>
                <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
                <?php if (isset(${'error_accountplus_extra_link_afatitle3' . $language['language_id']})) { ?>
                <span class="error"><?php echo ${'error_accountplus_extra_link_afatitle3' . $language['language_id']}; ?></span>
                <?php } ?>
                <?php } ?></td>
              <td class="left"><?php foreach ($languages as $language) { ?>
                <input type="text" name="accountplus_extra_link_afadesc3<?php echo $language['language_id']; ?>" value="<?php echo ${'accountplus_extra_link_afadesc3' . $language['language_id']}; ?>" size="32"/>
                <img src="view/image/flags/<?php echo $language['image']; ?>" desc="<?php echo $language['name']; ?>" /><br />
                <?php if (isset(${'error_accountplus_extra_link_afadesc3' . $language['language_id']})) { ?>
                <span class="error"><?php echo ${'error_accountplus_extra_link_afadesc3' . $language['language_id']}; ?></span>
                <?php } ?>
                <?php } ?></td>
              <td class="left"><input type="text" name="accountplus_extra_link_afurl3" value="<?php echo $accountplus_extra_link_afurl3; ?>" size="32"/>
                <?php if (isset($error_accountplus_extra_link_afurl3)) { ?>
                <span class="error"><?php echo $error_accountplus_extra_link_afurl3; ?></span>
                <?php } ?>
              </td>
              <td class="center"><div class="image" style="border: none;"><img src="<?php echo $accountplus_extra_link_afpic3; ?>" alt="" id="accountplus-extra-linkaf-3-pic" />
                <input type="hidden" name="accountplus_extra_link_afimage3" value="<?php echo $accountplus_extra_link_afimage3; ?>" id="accountplus-extra-linkaf-3-pic2" />
                <br />
                <a onclick="image_upload('accountplus-extra-linkaf-3-pic2', 'accountplus-extra-linkaf-3-pic');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#accountplus-extra-linkaf-3-pic').attr('src', '<?php echo $no_image; ?>'); $('#accountplus-extra-linkaf-3-pic2').attr('value', '');"><?php echo $text_clear; ?></a></div></td>
              <td><select name="accountplus_extra_link_afstatus3">
                  <?php if ($accountplus_extra_link_afstatus3) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
            </tr>
          </tbody>

          <tbody>
            <tr>
              <td class="left"><?php foreach ($languages as $language) { ?>
                <input type="text" name="accountplus_extra_link_afatitle4<?php echo $language['language_id']; ?>" value="<?php echo ${'accountplus_extra_link_afatitle4' . $language['language_id']}; ?>" size="32"/>
                <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
                <?php if (isset(${'error_accountplus_extra_link_afatitle4' . $language['language_id']})) { ?>
                <span class="error"><?php echo ${'error_accountplus_extra_link_afatitle4' . $language['language_id']}; ?></span>
                <?php } ?>
                <?php } ?></td>
              <td class="left"><?php foreach ($languages as $language) { ?>
                <input type="text" name="accountplus_extra_link_afadesc4<?php echo $language['language_id']; ?>" value="<?php echo ${'accountplus_extra_link_afadesc4' . $language['language_id']}; ?>" size="32"/>
                <img src="view/image/flags/<?php echo $language['image']; ?>" desc="<?php echo $language['name']; ?>" /><br />
                <?php if (isset(${'error_accountplus_extra_link_afadesc4' . $language['language_id']})) { ?>
                <span class="error"><?php echo ${'error_accountplus_extra_link_afadesc4' . $language['language_id']}; ?></span>
                <?php } ?>
                <?php } ?></td>
              <td class="left"><input type="text" name="accountplus_extra_link_afurl4" value="<?php echo $accountplus_extra_link_afurl4; ?>" size="32"/>
                <?php if (isset($error_accountplus_extra_link_afurl4)) { ?>
                <span class="error"><?php echo $error_accountplus_extra_link_afurl4; ?></span>
                <?php } ?>
              </td>
              <td class="center"><div class="image" style="border: none;"><img src="<?php echo $accountplus_extra_link_afpic4; ?>" alt="" id="accountplus-extra-linkaf-4-pic" />
                <input type="hidden" name="accountplus_extra_link_afimage4" value="<?php echo $accountplus_extra_link_afimage4; ?>" id="accountplus-extra-linkaf-4-pic2" />
                <br />
                <a onclick="image_upload('accountplus-extra-linkaf-4-pic2', 'accountplus-extra-linkaf-4-pic');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#accountplus-extra-linkaf-4-pic').attr('src', '<?php echo $no_image; ?>'); $('#accountplus-extra-linkaf-4-pic2').attr('value', '');"><?php echo $text_clear; ?></a></div></td>
              <td><select name="accountplus_extra_link_afstatus4">
                  <?php if ($accountplus_extra_link_afstatus4) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
            </tr>
          </tbody>
        </table>

       <table class="form"> 
            <td colspan="1" bgcolor="#F9F9F9"><b><?php echo $text_transaction_link_a; ?></b></td>
       </table>

        <table class="list">
          <thead>
            <tr>
              <td class="left"><?php echo $entry_title; ?></td>
              <td class="left"><?php echo $entry_description; ?></td>
              <td class="left"><?php echo $entry_link; ?></td>
              <td class="left"><?php echo $entry_image; ?></td>
              <td class="left"><?php echo $entry_status; ?></td>
            </tr>
          </thead>

          <tbody>
            <tr>
              <td class="left"><?php foreach ($languages as $language) { ?>
                <input type="text" name="accountplus_extra_link_afatitle5<?php echo $language['language_id']; ?>" value="<?php echo ${'accountplus_extra_link_afatitle5' . $language['language_id']}; ?>" size="32"/>
                <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
                <?php if (isset(${'error_accountplus_extra_link_afatitle5' . $language['language_id']})) { ?>
                <span class="error"><?php echo ${'error_accountplus_extra_link_afatitle5' . $language['language_id']}; ?></span>
                <?php } ?>
                <?php } ?></td>
              <td class="left"><?php foreach ($languages as $language) { ?>
                <input type="text" name="accountplus_extra_link_afadesc5<?php echo $language['language_id']; ?>" value="<?php echo ${'accountplus_extra_link_afadesc5' . $language['language_id']}; ?>" size="32"/>
                <img src="view/image/flags/<?php echo $language['image']; ?>" desc="<?php echo $language['name']; ?>" /><br />
                <?php if (isset(${'error_accountplus_extra_link_afadesc5' . $language['language_id']})) { ?>
                <span class="error"><?php echo ${'error_accountplus_extra_link_afadesc5' . $language['language_id']}; ?></span>
                <?php } ?>
                <?php } ?></td>
              <td class="left"><input type="text" name="accountplus_extra_link_afurl5" value="<?php echo $accountplus_extra_link_afurl5; ?>" size="32"/>
                <?php if (isset($error_accountplus_extra_link_afurl5)) { ?>
                <span class="error"><?php echo $error_accountplus_extra_link_afurl5; ?></span>
                <?php } ?>
              </td>
              <td class="center"><div class="image" style="border: none;"><img src="<?php echo $accountplus_extra_link_afpic5; ?>" alt="" id="accountplus-extra-linkaf-5-pic" />
                <input type="hidden" name="accountplus_extra_link_afimage5" value="<?php echo $accountplus_extra_link_afimage5; ?>" id="accountplus-extra-linkaf-5-pic5" />
                <br />
                <a onclick="image_upload('accountplus-extra-linkaf-5-pic5', 'accountplus-extra-linkaf-5-pic');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#accountplus-extra-linkaf-5-pic').attr('src', '<?php echo $no_image; ?>'); $('#accountplus-extra-linkaf-5-pic5').attr('value', '');"><?php echo $text_clear; ?></a></div></td>
              <td><select name="accountplus_extra_link_afstatus5">
                  <?php if ($accountplus_extra_link_afstatus5) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <td class="left"><?php foreach ($languages as $language) { ?>
                <input type="text" name="accountplus_extra_link_afatitle6<?php echo $language['language_id']; ?>" value="<?php echo ${'accountplus_extra_link_afatitle6' . $language['language_id']}; ?>" size="32"/>
                <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
                <?php if (isset(${'error_accountplus_extra_link_afatitle6' . $language['language_id']})) { ?>
                <span class="error"><?php echo ${'error_accountplus_extra_link_afatitle6' . $language['language_id']}; ?></span>
                <?php } ?>
                <?php } ?></td>
              <td class="left"><?php foreach ($languages as $language) { ?>
                <input type="text" name="accountplus_extra_link_afadesc6<?php echo $language['language_id']; ?>" value="<?php echo ${'accountplus_extra_link_afadesc6' . $language['language_id']}; ?>" size="32"/>
                <img src="view/image/flags/<?php echo $language['image']; ?>" desc="<?php echo $language['name']; ?>" /><br />
                <?php if (isset(${'error_accountplus_extra_link_afadesc6' . $language['language_id']})) { ?>
                <span class="error"><?php echo ${'error_accountplus_extra_link_afadesc6' . $language['language_id']}; ?></span>
                <?php } ?>
                <?php } ?></td>
              <td class="left"><input type="text" name="accountplus_extra_link_afurl6" value="<?php echo $accountplus_extra_link_afurl6; ?>" size="32"/>
                <?php if (isset($error_accountplus_extra_link_afurl6)) { ?>
                <span class="error"><?php echo $error_accountplus_extra_link_afurl6; ?></span>
                <?php } ?>
              </td>
              <td class="center"><div class="image" style="border: none;"><img src="<?php echo $accountplus_extra_link_afpic6; ?>" alt="" id="accountplus-extra-linkaf-6-pic" />
                <input type="hidden" name="accountplus_extra_link_afimage6" value="<?php echo $accountplus_extra_link_afimage6; ?>" id="accountplus-extra-linkaf-6-pic2" />
                <br />
                <a onclick="image_upload('accountplus-extra-linkaf-6-pic2', 'accountplus-extra-linkaf-6-pic');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#accountplus-extra-linkaf-6-pic').attr('src', '<?php echo $no_image; ?>'); $('#accountplus-extra-linkaf-6-pic2').attr('value', '');"><?php echo $text_clear; ?></a></div></td>
              <td><select name="accountplus_extra_link_afstatus6">
                  <?php if ($accountplus_extra_link_afstatus6) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
            </tr>
          </tbody>
        </table>


      </div>

   </div>
      
     <div id="tab-style-affiliate" class="vtabs-content">
        <table class="form">
          
           <tr>
             <td><label><?php echo $entry_style; ?></label></td>
    	  <td>
            <select name="style_af" id="selectStyle_af">
            	<option value="default" <?php if($style_af == 'default') { echo " selected"; }?> >Simple</option>
                <option value="icons-1" <?php if($style_af == 'icons-1') { echo " selected"; }?> >Simple Icons</option>
                <option value="icons-2" <?php if($style_af == 'icons-2') { echo " selected"; }?> >Page Icons</option>
                <option value="icons-3" <?php if($style_af == 'icons-3') { echo " selected"; }?> >Modern List</option>
                <option value="icons-4" <?php if($style_af == 'icons-4') { echo " selected"; }?> >Accordion - Simple</option>
                <option value="icons-5" <?php if($style_af == 'icons-5') { echo " selected"; }?> >Accordion - Icons</option>
                <option value="icons-6" <?php if($style_af == 'icons-6') { echo " selected"; }?> >Metro - Style 1</option>
                <option value="icons-7" <?php if($style_af == 'icons-7') { echo " selected"; }?> >Metro - Style 2</option>
                <option value="icons-8" <?php if($style_af == 'icons-8') { echo " selected"; }?> >Flexy</option>
                <option value="icons-9" <?php if($style_af == 'icons-9') { echo " selected"; }?> >Square</option>
                <option value="icons-10" <?php if($style_af == 'icons-10') { echo " selected"; }?> >Simple Box</option>
                <option value="icons-11" <?php if($style_af == 'icons-11') { echo " selected"; }?> >Reality</option>
                <option value="icons-12" <?php if($style_af == 'icons-12') { echo " selected"; }?> >Simpla</option>
                <option value="icons-13" <?php if($style_af == 'icons-13') { echo " selected"; }?> >Light Box</option>
                <option value="icons-14" <?php if($style_af == 'icons-14') { echo " selected"; }?> >Wuxia</option>
            </select>
           </td>
           <td>
               <div id="style_af_default" class="style_afbox"><img src="view/image/accountplus/default.png" alt="" /></div>
               <div id="style_af_icons-1" class="style_afbox"><img src="view/image/accountplus/icons-1.png" alt="" /></div>
               <div id="style_af_icons-2" class="style_afbox"><img src="view/image/accountplus/icons-2.png" alt="" /></div>
               <div id="style_af_icons-3" class="style_afbox"><img src="view/image/accountplus/icons-3.png" alt="" /></div>
               <div id="style_af_icons-4" class="style_afbox"><img src="view/image/accountplus/icons-4.png" alt="" /></div>
               <div id="style_af_icons-5" class="style_afbox"><img src="view/image/accountplus/icons-5.png" alt="" /></div>
               <div id="style_af_icons-6" class="style_afbox"><img src="view/image/accountplus/icons-6.png" alt="" /></div>
               <div id="style_af_icons-7" class="style_afbox"><img src="view/image/accountplus/icons-7.png" alt="" /></div>
               <div id="style_af_icons-8" class="style_afbox"><img src="view/image/accountplus/icons-8.png" alt="" /></div>
               <div id="style_af_icons-9" class="style_afbox"><img src="view/image/accountplus/icons-9.png" alt="" /></div>
               <div id="style_af_icons-10" class="style_afbox"><img src="view/image/accountplus/icons-10.png" alt="" /></div>
               <div id="style_af_icons-11" class="style_afbox"><img src="view/image/accountplus/icons-11.png" alt="" /></div>
               <div id="style_af_icons-12" class="style_afbox"><img src="view/image/accountplus/icons-12.png" alt="" /></div>
               <div id="style_af_icons-13" class="style_afbox"><img src="view/image/accountplus/icons-13.png" alt="" /></div>
               <div id="style_af_icons-14" class="style_afbox"><img src="view/image/accountplus/icons-14.png" alt="" /></div>
           </td>
    	 </tr>

            <tr>
                 <td><label><?php echo $entry_icotype; ?></label></td>
    		  <td>
                <select name="icotype_af" id="selectIcotype_af">
                    <option value="ballicons" <?php if($icotype_af == 'ballicons') { echo " selected"; }?> >Ballicons</option>
                    <option value="balloonica" <?php if($icotype_af == 'balloonica') { echo " selected"; }?> >Balloonica</option>
                    <option value="black_moon" <?php if($icotype_af == 'black_moon') { echo " selected"; }?> >Black Moon</option>
                    <option value="blue_velvet" <?php if($icotype_af == 'blue_velvet') { echo " selected"; }?> >Blue Velvet</option>
                    <option value="bubbles" <?php if($icotype_af == 'bubbles') { echo " selected"; }?> >Bubbles</option>
                    <option value="bubbles_colorful" <?php if($icotype_af == 'bubbles_colorful') { echo " selected"; }?> >Bubbles Colorful</option>
                    <option value="colorful_stickers" <?php if($icotype_af == 'colorful_stickers') { echo " selected"; }?> >Colorful Stickers</option>
                    <option value="coquette" <?php if($icotype_af == 'coquette') { echo " selected"; }?> >Coquette</option>
                    <option value="distortion" <?php if($icotype_af == 'distortion') { echo " selected"; }?> >Distortion</option>
                    <option value="fresh_buttons" <?php if($icotype_af == 'fresh_buttons') { echo " selected"; }?> >Fresh Buttons</option>
                    <option value="goodies" <?php if($icotype_af == 'goodies') { echo " selected"; }?> >Goodies</option>
                    <option value="handy" <?php if($icotype_af == 'handy') { echo " selected"; }?> >Handy</option>
                    <option value="luna_blue" <?php if($icotype_af == 'luna_blue') { echo " selected"; }?> >Luna Blue</option>
                    <option value="luna_grey" <?php if($icotype_af == 'luna_grey') { echo " selected"; }?> >Luna Grey</option>
                    <option value="metro_black" <?php if($icotype_af == 'metro_black') { echo " selected"; }?> >Metro Black</option>
                    <option value="metro_white" <?php if($icotype_af == 'metro_white') { echo " selected"; }?> >Metro White</option>
                    <option value="minimalistica_blue" <?php if($icotype_af == 'minimalistica_blue') { echo " selected"; }?> >Minimalistica Blue</option>
                    <option value="minimalistica_red" <?php if($icotype_af == 'minimalistica_red') { echo " selected"; }?> >Minimalistica Red</option>
                    <option value="mobi" <?php if($icotype_af == 'mobi') { echo " selected"; }?> >Mobi</option>
                    <option value="modern_round" <?php if($icotype_af == 'modern_round') { echo " selected"; }?> >Modern Round</option>
                    <option value="moon_blue" <?php if($icotype_af == 'moon_blue') { echo " selected"; }?> >Moon Blue</option>
                    <option value="moon_red" <?php if($icotype_af == 'moon_red') { echo " selected"; }?> >Moon Red</option>
                    <option value="reflection_blue" <?php if($icotype_af == 'reflection_blue') { echo " selected"; }?> >Reflection Blue</option>
                    <option value="reflection_red" <?php if($icotype_af == 'reflection_red') { echo " selected"; }?> >Reflection Red</option>
                    <option value="softies" <?php if($icotype_af == 'softies') { echo " selected"; }?> >Softies</option>
                    <option value="square_blue" <?php if($icotype_af == 'square_blue') { echo " selected"; }?> >Square Blue</option>
                    <option value="stickers" <?php if($icotype_af == 'stickers') { echo " selected"; }?> >Stickers</option>
                    <option value="stylish" <?php if($icotype_af == 'stylish') { echo " selected"; }?> >Stylish</option>
                    <option value="stylistica" <?php if($icotype_af == 'stylistica') { echo " selected"; }?> >Stylistica</option>
                    <option value="symbolize" <?php if($icotype_af == 'symbolize') { echo " selected"; }?> >Symbolize</option>
                    <option value="web_colorful" <?php if($icotype_af == 'web_colorful') { echo " selected"; }?> >Web Colorful</option>
                    <option value="web_purple" <?php if($icotype_af == 'web_purple') { echo " selected"; }?> >Web Purple</option>
            </select>
           </td>
           <td>

               <div id="icotype_af_ballicons" class="icotype_afbox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/ballicons/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/ballicons/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/ballicons/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/ballicons/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/ballicons/id.png" alt="" />
               </div>

               <div id="icotype_af_balloonica" class="icotype_afbox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/balloonica/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/balloonica/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/balloonica/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/balloonica/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/balloonica/id.png" alt="" />
               </div>
               
               <div id="icotype_af_black_moon" class="icotype_afbox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/black_moon/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/black_moon/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/black_moon/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/black_moon/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/black_moon/id.png" alt="" />
               </div>
               
               <div id="icotype_af_blue_velvet" class="icotype_afbox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/blue_velvet/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/blue_velvet/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/blue_velvet/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/blue_velvet/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/blue_velvet/id.png" alt="" />
               </div>

               <div id="icotype_af_bubbles" class="icotype_afbox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/bubbles/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/bubbles/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/bubbles/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/bubbles/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/bubbles/id.png" alt="" />
               </div>

               <div id="icotype_af_bubbles_colorful" class="icotype_afbox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/bubbles_colorful/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/bubbles_colorful/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/bubbles_colorful/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/bubbles_colorful/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/bubbles_colorful/id.png" alt="" />
               </div>
               
               <div id="icotype_af_colorful_stickers" class="icotype_afbox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/stickers/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/stickers/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/stickers/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/stickers/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/stickers/id.png" alt="" />
               </div>
               
               <div id="icotype_af_coquette" class="icotype_afbox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/coquette/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/coquette/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/coquette/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/coquette/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/coquette/id.png" alt="" />
               </div>

               <div id="icotype_af_distortion" class="icotype_afbox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/distortion/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/distortion/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/distortion/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/distortion/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/distortion/id.png" alt="" />
               </div>

               <div id="icotype_af_fresh_buttons" class="icotype_afbox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/fresh_buttons/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/fresh_buttons/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/fresh_buttons/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/fresh_buttons/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/fresh_buttons/id.png" alt="" />
               </div>
               
               <div id="icotype_af_goodies" class="icotype_afbox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/goodies/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/goodies/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/goodies/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/goodies/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/goodies/id.png" alt="" />
               </div>

               <div id="icotype_af_handy" class="icotype_afbox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/handy/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/handy/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/handy/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/handy/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/handy/id.png" alt="" />
               </div>
               
               <div id="icotype_af_luna_blue" class="icotype_afbox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/luna_blue/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/luna_blue/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/luna_blue/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/luna_blue/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/luna_blue/id.png" alt="" />
               </div>
               <div id="icotype_af_luna_grey" class="icotype_afbox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/luna_grey/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/luna_grey/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/luna_grey/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/luna_grey/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/luna_grey/id.png" alt="" />
               </div>
               <div id="icotype_af_metro_black" class="icotype_afbox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/metro_black/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/metro_black/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/metro_black/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/metro_black/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/metro_black/id.png" alt="" />
               </div>
               <div id="icotype_af_metro_white" class="icotype_afbox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/metro_white/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/metro_white/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/metro_white/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/metro_white/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/metro_white/id.png" alt="" />
               </div>
               <div id="icotype_af_minimalistica_blue" class="icotype_afbox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/minimalistica_blue/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/minimalistica_blue/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/minimalistica_blue/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/minimalistica_blue/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/minimalistica_blue/id.png" alt="" />
               </div>
               <div id="icotype_af_minimalistica_red" class="icotype_afbox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/minimalistica_red/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/minimalistica_red/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/minimalistica_red/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/minimalistica_red/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/minimalistica_red/id.png" alt="" />
               </div>

               <div id="icotype_af_mobi" class="icotype_afbox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/mobi/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/mobi/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/mobi/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/mobi/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/mobi/id.png" alt="" />
               </div>

               <div id="icotype_af_modern_round" class="icotype_afbox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/modern_round/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/modern_round/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/modern_round/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/modern_round/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/modern_round/id.png" alt="" />
               </div>

               <div id="icotype_af_moon_blue" class="icotype_afbox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/moon_blue/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/moon_blue/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/moon_blue/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/moon_blue/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/moon_blue/id.png" alt="" />
               </div>
               <div id="icotype_af_moon_red" class="icotype_afbox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/moon_red/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/moon_red/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/moon_red/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/moon_red/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/moon_red/id.png" alt="" />
               </div>

               <div id="icotype_af_reflection_blue" class="icotype_afbox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/reflection_blue/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/reflection_blue/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/reflection_blue/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/reflection_blue/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/reflection_blue/id.png" alt="" />
               </div>

               <div id="icotype_af_reflection_red" class="icotype_afbox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/reflection_red/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/reflection_red/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/reflection_red/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/reflection_red/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/reflection_red/id.png" alt="" />
               </div>

               <div id="icotype_af_softies" class="icotype_afbox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/softies/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/softies/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/softies/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/softies/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/softies/id.png" alt="" />
               </div>

               <div id="icotype_af_square_blue" class="icotype_afbox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/square_blue/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/square_blue/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/square_blue/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/square_blue/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/square_blue/id.png" alt="" />
               </div>

               <div id="icotype_af_stickers" class="icotype_afbox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/stickers/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/stickers/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/stickers/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/stickers/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/stickers/id.png" alt="" />
               </div>

               <div id="icotype_af_stylish" class="icotype_afbox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/stylish/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/stylish/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/stylish/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/stylish/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/stylish/id.png" alt="" />
               </div>

               <div id="icotype_af_stylistica" class="icotype_afbox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/stylistica/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/stylistica/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/stylistica/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/stylistica/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/stylistica/id.png" alt="" />
               </div>

               <div id="icotype_af_symbolize" class="icotype_afbox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/symbolize/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/symbolize/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/symbolize/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/symbolize/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/symbolize/id.png" alt="" />
               </div>

               <div id="icotype_af_web_colorful" class="icotype_afbox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/web_colorful/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/web_colorful/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/web_colorful/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/web_colorful/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/web_colorful/id.png" alt="" />
               </div>

               <div id="icotype_af_web_purple" class="icotype_afbox">
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/web_purple/account.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/web_purple/address.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/web_purple/downloads.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/web_purple/edit.png" alt="" />
                    <img src= "<?php echo $server; ?>catalog/view/theme/default/image/accountplus/icons/web_purple/id.png" alt="" />
               </div>
           </td>
    	 </tr>
        </table> 
       </div>        
    </form>
  </div>
</div>
</div>
<link rel="stylesheet" type="text/css" href="view/stylesheet/s_modules.css" />
<script type="text/javascript"><!--
function image_upload(field, thumb) {
	$('#dialog').remove();
	$('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');
	$('#dialog').dialog({
		title: '<?php echo $text_image_manager; ?>',
		close: function (event, ui) {
			if ($('#' + field).attr('value')) {
				$.ajax({
					url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).val()),
					dataType: 'text',
					success: function(data) {
						$('#' + thumb).replaceWith('<img src="' + data + '" alt="" id="' + thumb + '" />');
					}
				});
			}
		},	
		bgiframe: false,
		width: 800,
		height: 400,
		resizable: false,
		modal: false
	});
};
//--></script>
<script type="text/javascript"><!--
$('#tabs-accountplus a').tabs();
$('#tabs-account a').tabs();
$('#tabs-affiliate a').tabs();
$('#tab_account_titles a').tabs();
$('#tab_affiliate_titles a').tabs();
$('#tab_display_account_general a').tabs();
$('#tab_display_affiliate_general a').tabs();
//--></script>
<script type="text/javascript">
<!--
	$(document).ready(function () {
    var am_var = "<?php echo $this->config->get('style_a'); ?>";
	   
    $('.style_abox').hide();
    if (am_var == '')  {
        $('#style_a_default').show();
    } else {
        $('#style_a_'+am_var).show();    
    }
    
    $('#selectStyle_a').change(function () {
        $('.style_abox').hide();
        $('#style_a_'+$(this).val()).show();
    });
});

	$(document).ready(function () {
    var ai_var = "<?php echo $this->config->get('icotype_a'); ?>";
	   
    $('.icotype_abox').hide();
    if (ai_var == '')  {
        $('#icotype_a_ballicons').show();
    } else {
        $('#icotype_a_'+ai_var).show();    
    }
    
    $('#selectIcotype_a').change(function () {
        $('.icotype_abox').hide();
        $('#icotype_a_'+$(this).val()).show();
    });
});

-->
</script>
<script type="text/javascript">
<!--
	$(document).ready(function () {
    var afm_var = "<?php echo $this->config->get('style_af'); ?>";
	   
    $('.style_afbox').hide();
    if (afm_var == '')  {
        $('#style_af_default').show();
    } else {
        $('#style_af_'+afm_var).show();    
    }
    
    $('#selectStyle_af').change(function () {
        $('.style_afbox').hide();
        $('#style_af_'+$(this).val()).show();
    });
});

	$(document).ready(function () {
    var afi_var = "<?php echo $this->config->get('icotype_af'); ?>";
	   
    $('.icotype_afbox').hide();
    if (afi_var == '')  {
        $('#icotype_af_ballicons').show();
    } else {
        $('#icotype_af_'+afi_var).show();    
    }
    
    $('#selectIcotype_af').change(function () {
        $('.icotype_afbox').hide();
        $('#icotype_af_'+$(this).val()).show();
    });
});
-->
</script>
<?php echo $footer; ?>