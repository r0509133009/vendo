<?php echo $header; ?>
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
<div id="content">

  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
           <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  
   <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $view_all_history; ?></h3>
      </div>
	  
	    <div class="container-fluid">
		<div class="pull-right">
		<a onclick="$('form').submit();" class=""> <button class="btn btn-primary" type="button"> <?php echo $button_delete; ?></button></a>
		</div>
		</div>
	
      <div class="panel-body">
            <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form"  class="form-horizontal">
			  <table class="table table-striped">
				  <tr>
					<td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
					<td class="left"><?php echo $column_name; ?></td>
					<td class="left"><?php echo $column_date; ?></td>
					<td class="left"><?php echo $column_email; ?></td>
					<td class="right"><?php echo $column_action; ?></td>
				  </tr>
				
				  <?php if ($emails) { ?>
				  <?php foreach ($emails as $email) { 		  /*echo '<pre>';		  print_r($email);		  echo '</pre>';*/		  ?>
				  <tr>       
				  <td style="text-align: center;">  <input type="checkbox" name="selected[]" value="<?php echo $email['id']; ?>"/> </td>  
				  <td class="left"><?php echo $email['id']; ?></td>     
				  <td class="left"><?php echo $email['date']; ?></td>     
				  <td class="left"><?php echo $email['subject']; ?></td>    
				  <td class="right"><?php foreach ($email['action'] as $action) { ?>   
				  [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]   
				  <?php } ?></td>          </tr>          <?php } ?>          <?php } 
				  else { ?>   
				  <tr>    
				  <td class="center" colspan="4"><?php echo $text_no_results; ?></td>   
				  </tr>        
				  <?php } ?>
				
			  </table>
			</form>
		

		
		
      </div>
    </div>
  </div>
    





  <div class="pagination"><?php echo $pagination; ?></div>
  

<?php echo $footer; ?>
