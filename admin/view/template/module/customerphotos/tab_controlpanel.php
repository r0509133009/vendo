<table class="form">
  <tr>
    <td><span class="required">*</span> <strong><?php echo $entry_code; ?></strong></td>
    <td>
        <select name="CustomerPhotos[Enabled]" class="CustomerPhotosEnabled">
            <option value="yes" <?php echo (!empty($data['CustomerPhotos']['Enabled']) && $data['CustomerPhotos']['Enabled'] == 'yes') ? 'selected=selected' : '' ?>>Enabled</option>
			<option value="no"  <?php echo (empty($data['CustomerPhotos']['Enabled']) || $data['CustomerPhotos']['Enabled']== 'no') ? 'selected=selected' : '' ?>>Disabled</option>
        </select>
   </td>
  </tr>
  <tr class="CustomerPhotosActiveTR">
     <td colspan="2">
       			<table id="module__" class="table table-bordered table-hover">
  <thead>
    <tr class="table-header">
      <td class="left"><strong><?php echo $entry_layout_options; ?></strong></td>
      <td class="left"><strong><?php echo $entry_position_options; ?></strong></td>
      <td class="left"><strong>Actions:</strong></td>
    </tr>
  </thead>
  <?php $module__row = 0; ?>
  <?php foreach ($modules as $module) { ?>
  <tbody id="module__row<?php echo $module__row; ?>">
    <tr>
      <td class="left span4">
        <label class="module-row-label"><div style="width:80px;display:inline-block;"><?php echo $entry_status; ?></div> <select class="span2" name="customerphotos_module[<?php echo $module__row; ?>][status]">
          <?php if ($module['status']) { ?>
          <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
          <option value="0"><?php echo $text_disabled; ?></option>
          <?php } else { ?>
          <option value="1"><?php echo $text_enabled; ?></option>
          <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
          <?php } ?>
        </select></label>
        <label class="module-row-label"><div style="width:80px;display:inline-block;"><?php echo $entry_layout; ?></div> <select class="span2 layout_name" name="customerphotos_module[<?php echo $module__row; ?>][layout_id]">
          <?php foreach ($layouts as $layout) { ?>
          <?php if ($layout['layout_id'] == $module['layout_id']) { ?>
          <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
          <?php } else { ?>
          <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
          <?php } ?>
          <?php } ?>
        </select></label>
      	<label class="module-row-label"><div style="width:80px;display:inline-block;"><?php echo $entry_sort_order; ?></div> <input class="span1 module-row-input-number" type="number" name="customerphotos_module[<?php echo $module__row; ?>][sort_order]" value="<?php echo $module['sort_order']; ?>" /></label>
        <label class="module-row-label"><div style="width:80px;display:inline-block;">Tag:</div> <div class="input-prepend">
  <span class="add-on">#</span><input class="span2 module-row-input-text tagName" type="text" <?php echo ($module['layout_id']==2) ? 'disabled="disabled"' : ''; ?>name="customerphotos_module[<?php echo $module__row; ?>][tag]" value="<?php echo (isset($module['tag'])) ? $module['tag'] : ''; ?>" /></div></label>
      </td>
      <td class="left" style="vertical-align:middle;">
        <div class="buttonPositionOpenCart" style="float:left; padding-right:10px;">
        <div class="leftBoxInput" style="text-align:center;"><input <?php if ($module['position'] == 'content_top') echo 'checked="checked"'; ?> type="radio" style="width:auto" name="customerphotos_module[<?php echo $module__row; ?>][position]" id="buttonPos<?php echo $module__row; ?>_1" class="buttonPositionOptionBox" data-checkbox="#buttonPosCheckbox_<?php echo $module__row; ?>" value="content_top" /></div><div style="text-align:center;" class="leftBoxTitle posTitleLabel"><label for="buttonPos1"><strong><?php echo $text_content_top; ?></strong></label></div>
        <div class="positionSampleBox"><label for="buttonPos<?php echo $module__row; ?>_1"><img class="img-polaroid" src="view/image/customerphotos/content_top.png" title="<?php echo $text_content_top; ?>" border="0" /></label></div>        
    </div>
        <div class="buttonPositionOpenCart" style="float:left; padding-right:10px;">
        <div class="leftBoxInput" style="text-align:center;"><input <?php if ($module['position'] == 'content_bottom') echo 'checked="checked"'; ?> type="radio" style="width:auto" name="customerphotos_module[<?php echo $module__row; ?>][position]" id="buttonPos<?php echo $module__row; ?>_2" class="buttonPositionOptionBox" data-checkbox="#buttonPosCheckbox_<?php echo $module__row; ?>" value="content_bottom" /></div><div style="text-align:center;" class="leftBoxTitle posTitleLabel"><label for="buttonPos2"><strong><?php echo $text_content_bottom; ?></strong></label></div>
        <div class="positionSampleBox"><label for="buttonPos<?php echo $module__row; ?>_2"><img class="img-polaroid" src="view/image/customerphotos/content_bottom.png" title="<?php echo $text_content_bottom; ?>" border="0" /></label></div>        
    </div>
        <div class="buttonPositionOpenCart" style="float:left; padding-right:10px;">
        <div class="leftBoxInput" style="text-align:center;"><input <?php if ($module['position'] == 'column_left') echo 'checked="checked"'; ?> type="radio" style="width:auto" name="customerphotos_module[<?php echo $module__row; ?>][position]" id="buttonPos<?php echo $module__row; ?>_3" class="buttonPositionOptionBox" data-checkbox="#buttonPosCheckbox_<?php echo $module__row; ?>" value="column_left" /></div><div style="text-align:center;" class="leftBoxTitle posTitleLabel"><label for="buttonPos3"><strong><?php echo $text_column_left; ?></strong></label></div>
        <div class="positionSampleBox"><label for="buttonPos<?php echo $module__row; ?>_3"><img class="img-polaroid" src="view/image/customerphotos/column_left.png" title="<?php echo $text_column_left; ?>" border="0" /></label></div>        
    </div>
        <div class="buttonPositionOpenCart last" style="float:left; padding-right:10px;">
        <div class="leftBoxInput" style="text-align:center;"><input <?php if ($module['position'] == 'column_right') echo 'checked="checked"'; ?> type="radio" style="width:auto" name="customerphotos_module[<?php echo $module__row; ?>][position]" id="buttonPos<?php echo $module__row; ?>_4" class="buttonPositionOptionBox" data-checkbox="#buttonPosCheckbox_<?php echo $module__row; ?>" value="column_right" /></div><div style="text-align:center;" class="leftBoxTitle posTitleLabel"><label for="buttonPos4"><strong><?php echo $text_column_right; ?></strong></label></div>
        <div class="positionSampleBox"><label for="buttonPos<?php echo $module__row; ?>_4"><img class="img-polaroid" src="view/image/customerphotos/column_right.png" title="<?php echo $text_column_right; ?>" border="0" /></label></div>
    </div></td>
      <td class="left" style="vertical-align:middle;"><a onclick="$('#module__row<?php echo $module__row; ?>').remove();" class="btn btn-small btn-danger"><i class="icon-remove"></i> <?php echo $button_remove; ?></a></td>
    </tr>
  </tbody>
  <?php $module__row++; ?>
  <?php } ?>
  <tfoot>
    <tr>
      <td colspan="2"></td>
      <td class="left"><a onclick="addPosition();" class="btn btn-small btn-primary"><i class="icon-plus"></i> <?php echo $button_add_module; ?></a></td>
    </tr>
  </tfoot>
</table>
<script type="text/javascript"><!--
var module__row = <?php echo $module__row; ?>;
function addPosition() {
	html  = '<tbody style="display:none;" id="module__row' + module__row + '">';
	html += '  <tr>';
	html += '    <td class="left span4">';
	html += '    <label class="module-row-label"><div style="width:80px;display:inline-block;"><?php echo $entry_status; ?></div> <select class="span2" name="customerphotos_module[' + module__row + '][status]">';
    html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
    html += '      <option value="0"><?php echo $text_disabled; ?></option>';
    html += '    </select></label>';
	html += '    <label class="module-row-label"><div style="width:80px;display:inline-block;"><?php echo $entry_layout; ?></div> <select class="span2 layout_name" name="customerphotos_module[' + module__row + '][layout_id]">';
	<?php foreach ($layouts as $layout) { ?>
	html += '      <option value="<?php echo $layout['layout_id']; ?>"><?php echo addslashes($layout['name']); ?></option>';
	<?php } ?>
	html += '    </select></label>';
	html += '    <label class="module-row-label"><div style="width:80px;display:inline-block;"><?php echo $entry_sort_order; ?></div> <input class="span1 module-row-input-number" type="number" name="customerphotos_module[' + module__row + '][sort_order]" value="0" />';
	html += '    </label>';
	html += ' <label class="module-row-label"><div style="width:80px;display:inline-block;">Tag:</div> <div class="input-prepend"><span class="add-on">#</span><input class="span2 module-row-input-text tagName" type="text" name="customerphotos_module[<?php echo $module__row; ?>][tag]" value="" /></div></label></td>';
	html += '    <td class="left" style="vertical-align:middle;">';
	html += '<div class="buttonPositionOpenCart" style="float:left; padding-right:10px;"><div class="leftBoxInput" style="text-align:center;"><input checked="checked" type="radio" style="width:auto" name="customerphotos_module[' + module__row + '][position]" id="buttonPos' + module__row + '_1" class="buttonPositionOptionBox" data-checkbox="#buttonPosCheckbox_' + module__row + '" value="content_top" /></div><div style="text-align:center;" class="leftBoxTitle posTitleLabel"><label for="buttonPos1"><strong><?php echo $text_content_top; ?></strong></label></div><div class="positionSampleBox"><label for="buttonPos' + module__row + '_1"><img class="img-polaroid" src="view/image/customerphotos/content_top.png" title="<?php echo $text_content_top; ?>" border="0" /></label></div></div>';
	html += '<div class="buttonPositionOpenCart" style="float:left; padding-right:10px;"><div class="leftBoxInput" style="text-align:center;"><input type="radio" style="width:auto" name="customerphotos_module[' + module__row + '][position]" id="buttonPos' + module__row + '_2" class="buttonPositionOptionBox" data-checkbox="#buttonPosCheckbox_' + module__row + '" value="content_bottom" /></div><div style="text-align:center;" class="leftBoxTitle posTitleLabel"><label for="buttonPos2"><strong><?php echo $text_content_bottom; ?></strong></label></div><div class="positionSampleBox"><label for="buttonPos' + module__row + '_2"><img class="img-polaroid"  src="view/image/customerphotos/content_bottom.png" title="<?php echo $text_content_bottom; ?>" border="0" /></label></div></div>';
	html += '<div class="buttonPositionOpenCart" style="float:left; padding-right:10px;"><div class="leftBoxInput" style="text-align:center;"><input type="radio" style="width:auto" name="customerphotos_module[' + module__row + '][position]" id="buttonPos' + module__row + '_3" class="buttonPositionOptionBox" data-checkbox="#buttonPosCheckbox_' + module__row + '" value="column_left" /></div><div style="text-align:center;" class="leftBoxTitle posTitleLabel"><label for="buttonPos3"><strong><?php echo $text_column_left; ?></strong></label></div><div class="positionSampleBox"><label for="buttonPos' + module__row + '_3"><img class="img-polaroid" src="view/image/customerphotos/column_left.png" title="<?php echo $text_column_left; ?>" border="0" /></label></div></div>';
	html += '<div class="buttonPositionOpenCart last" style="float:left; padding-right:10px;"><div class="leftBoxInput" style="text-align:center;"><input type="radio" style="width:auto" name="customerphotos_module[' + module__row + '][position]" id="buttonPos' + module__row + '_4" class="buttonPositionOptionBox" data-checkbox="#buttonPosCheckbox_' + module__row + '" value="column_right" /></div><div style="text-align:center;" class="leftBoxTitle posTitleLabel"><label for="buttonPos4"><strong><?php echo $text_column_right; ?></strong></label></div><div class="positionSampleBox"><label for="buttonPos' + module__row + '_4"><img class="img-polaroid" src="view/image/customerphotos/column_right.png" title="<?php echo $text_column_right; ?>" border="0" /></label></div></div>';
	html += '    </td>';
	html += '    <td class="left" style="vertical-align:middle;"><a onclick="$(\'#module__row' + module__row + '\').remove();" class="btn btn-small btn-danger" style="text-decoration:none;"><i class="icon-remove"></i> <?php echo $button_remove; ?></a></td>';
	html += '  </tr>';
	html += '</tbody>';
	
	$('#module__ tfoot').before(html);
	$('#module__row' + module__row).fadeIn();
	
	
	module__row++;
}
//--></script>
     </td>
  </tr>
</table>
<script>
$('.CustomerPhotosLayout input[type=checkbox]').change(function() {
    if ($(this).is(':checked')) { 
        $('.CustomerPhotosItemStatusField', $(this).parent()).val(1);
    } else {
        $('.CustomerPhotosItemStatusField', $(this).parent()).val(0);
    }
});
$('.CustomerPhotosEnabled').change(function() {
    toggleCustomerPhotosActive(true);
});
var toggleCustomerPhotosActive = function(animated) {
   if ($('.CustomerPhotosEnabled').val() == 'yes') {
        if (animated) 
            $('.CustomerPhotosActiveTR').fadeIn();
        else 
            $('.CustomerPhotosActiveTR').show();
    } else {
        if (animated) 
            $('.CustomerPhotosActiveTR').fadeOut();
        else 
            $('.CustomerPhotosActiveTR').hide();
    }
}
toggleCustomerPhotosActive(false);

$('.layout_name').live('change' , function() {
	if ($(this).val()==2) {
		$(this).parents('.left').find('.tagName').attr('disabled', 'disabled');
	} else {
		$(this).parents('.left').find('.tagName').removeAttr('disabled');	
	}
});
</script>