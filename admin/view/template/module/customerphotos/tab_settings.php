<div class="row-fluid">
	<div class="span12">
        <div class="minification-tabbable-parent">
			<div class="tabbable tabs-left"> 
                  <ul class="nav nav-tabs">
                    <li class="active"><a href="#general" data-toggle="tab"><i class="icon-chevron-right"></i> General Options</a></li>
                    <li><a href="#instagram" data-toggle="tab"><i class="icon-chevron-right"></i> Instagram</a></li>
                    <li><a href="#twitter" data-toggle="tab"><i class="icon-chevron-right"></i> Twitter</a></li>
                    <li><a href="#customTags" data-toggle="tab"><i class="icon-chevron-right"></i> Custom Tags</a></li>
                  </ul>
                 <div class="tab-content">
                    <div id="general" class="tab-pane active">
                        <div class="box-heading">
                            <h1 style="float:left;">General Options</h1>
                            <br /><br />
                        </div>
         				<table class="form">
						<tr>
                        <td>Main message<span class="help"><strong>%s</strong> will be replaced with the corresponding tag.</span></td>
                        <td valign="top">
							<?php foreach ($languages as $language) { ?>
                                <div class="input-prepend">   
                                  <span class="add-on"><?php echo $language['name']; ?></span>   
									<input name="CustomerPhotos[MainMessage][<?php echo $language['code']; ?>]" class="input-xxlarge" type="text" value="<?php echo (isset($data['CustomerPhotos']['MainMessage'][$language['code']])) ? $data['CustomerPhotos']['MainMessage'][$language['code']] : 'Tag your photos using %s on Instagram and Twitter!' ?>" />
								</div>
                             <?php } ?>
                       </td>
                      </tr>
					  <tr>
                       <td>Site short-tag<span class="help">This short-tag will be appended before any product on your store. For example: #mysiteiphone5, where <i>mysite</i> is your short-tag.</span></td>
                        <td valign="top">
                      <div class="input-prepend"><span class="add-on">#</span> <input type="text" name="CustomerPhotos[ShortTag]" value="<?php echo(!empty($data['CustomerPhotos']['ShortTag'])) ? $data['CustomerPhotos']['ShortTag'] : '' ; ?>" /></div>
                      </td>
                      </tr>
						<tr>
                       <td>Tag character limit<span class="help">Example: If the tag is very long it will be stripped to xx characters.</span></td>
                        <td valign="top">
                        <div class="input-append"> <input type="text" name="CustomerPhotos[TagCharacterLimit]" value="<?php echo(!empty($data['CustomerPhotos']['TagCharacterLimit'])) ? $data['CustomerPhotos']['TagCharacterLimit'] : '10' ; ?>" /><span class="add-on">chars</span>
</div>
                      </td>
                      </tr>
	                    <tr>
                        <td>Photo Height<span class="help">In Pixels</span></td>
                        <td valign="top">
                            <div class="input-append"> <input type="text" name="CustomerPhotos[PhotoHeight]" value="<?php echo(!empty($data['CustomerPhotos']['PhotoHeight'])) ? $data['CustomerPhotos']['PhotoHeight'] : '80' ; ?>" /><span class="add-on">px</span>
</div>
                       </td>
                      </tr>
					  <tr>
                          <td>Wrap in widget</td>
                            <td>
                                <select name="CustomerPhotos[WrapInWidget]" class="CustomerPhotosWrapInWidget">
                                    <option value="yes" <?php echo (!empty($data['CustomerPhotos']['WrapInWidget']) && ($data['CustomerPhotos']['WrapInWidget'] == 'yes')) ? 'selected=selected' : '' ?>>Enabled</option>
                                   <option value="no" <?php echo (!empty($data['CustomerPhotos']['WrapInWidget']) && ($data['CustomerPhotos']['WrapInWidget'] == 'no')) ? 'selected=selected' : '' ?>>Disabled</option>
                                </select>
                           </td>
					  </tr>
					  <tr>
                          <td>Product tags:<span class="help">Choose what string to be used for the creation of tags: Product name or product model.</span></td>
                            <td>
                                <select name="CustomerPhotos[TagType]" class="CustomerPhotosTagType">
                                    <option value="product_name" <?php echo (!empty($data['CustomerPhotos']['TagType']) && ($data['CustomerPhotos']['TagType'] == 'product_name')) ? 'selected=selected' : '' ?>>use Product Name</option>
                                   <option value="product_model" <?php echo (!empty($data['CustomerPhotos']['TagType']) && ($data['CustomerPhotos']['TagType'] == 'product_model')) ? 'selected=selected' : '' ?>>use Product Model</option>
                                </select>
                           </td>
					  </tr>
                      <tr>
                        <td>Show CustomerPhotos in product tab<span class="help">This option is active only for the product page and works if the template support tabs.</span></td>
                        <td>
                            <select name="CustomerPhotos[showInTab]">
                                <option value="no" <?php echo(!empty($data['CustomerPhotos']['showInTab']) && ($data['CustomerPhotos']['showInTab'] == 'no')) ? 'selected' : ''; ?>>No</option>
                                <option value="yes" <?php echo(!empty($data['CustomerPhotos']['showInTab']) && ($data['CustomerPhotos']['showInTab'] == 'yes')) ? 'selected' : ''; ?>>Yes</option>   
                            </select>
                       </td>
                      </tr>
                    </table>
                        <div class="clearfix"></div>
                    </div>
                    <div id="instagram" class="tab-pane">                          
                            <div class="box-heading">
                                <h1 style="float:left;">Instagram Settings</h1>
								<br /><br />
                            </div>					
                     <table class="form"> 
                        <tr>
                        <td>Enable Instagram</td>
                        <td>
                            <select name="CustomerPhotos[InstagramEnable]">
                                <option value="no" <?php echo(!empty($data['CustomerPhotos']['InstagramEnable']) && ($data['CustomerPhotos']['InstagramEnable'] == 'no')) ? 'selected' : ''; ?>>No</option>
                                <option value="yes" <?php echo(!empty($data['CustomerPhotos']['InstagramEnable']) && ($data['CustomerPhotos']['InstagramEnable'] == 'yes')) ? 'selected' : ''; ?>>Yes</option> 
                            </select>
                       </td>
                      </tr>                  
                         <tr>
                        <td>Instragram Client ID<span class="help">Get API key for your application at http://instagram.com/developer/</span></td>
                        <td valign="top">
                            <input type="text" name="CustomerPhotos[InstagramAPIKey]" value="<?php echo(!empty($data['CustomerPhotos']['InstagramAPIKey'])) ? $data['CustomerPhotos']['InstagramAPIKey'] : '' ; ?>" />
                       </td>
                      </tr>
                      <tr>
                           <tr>
                        <td>Photos from Instagram<span class="help">Limit: 33 photos</span></td>
                        <td valign="top">
                            <input type="text" name="CustomerPhotos[InstagramPhotos]" value="<?php echo(!empty($data['CustomerPhotos']['InstagramPhotos'])) ? $data['CustomerPhotos']['InstagramPhotos'] : '5' ; ?>" />
                       </td>
                      </tr>                    
                    </table>
                            <div class="clearfix"></div>
                    </div>
                    <div id="twitter" class="tab-pane">                            
                            <div class="box-heading">
                                <h1 style="float:left;">Twitter Settings</h1>
                                <br /><br />
                            </div>				
                	<table class="form"> 
                        <tr>
                        <td>Enable Twitter</td>
                        <td>
                            <select name="CustomerPhotos[TwitterEnable]">
                                <option value="no" <?php echo(!empty($data['CustomerPhotos']['TwitterEnable']) && ($data['CustomerPhotos']['TwitterEnable'] == 'no')) ? 'selected' : ''; ?>>No</option>
                                <option value="yes" <?php echo(!empty($data['CustomerPhotos']['TwitterEnable']) && ($data['CustomerPhotos']['TwitterEnable'] == 'yes')) ? 'selected' : ''; ?>>Yes</option> 
                            </select>
                       </td>
                      </tr>                  
                         <tr>
                        <td>Twitter Consumer Key<span class="help">Get all keys for your application at https://dev.twitter.com/apps</span></td>
                        <td valign="top">
                            <input type="text" name="CustomerPhotos[TwitterConsumerKey]" value="<?php echo(!empty($data['CustomerPhotos']['TwitterConsumerKey'])) ? $data['CustomerPhotos']['TwitterConsumerKey'] : '' ; ?>" />
                       </td>
                      </tr>
                      <tr>
                           <tr>
                        <td>Twitter Consumer Key Secret</td>
                        <td valign="top">
                            <input type="text" name="CustomerPhotos[TwitterConsumerKeySecret]" value="<?php echo(!empty($data['CustomerPhotos']['TwitterConsumerKeySecret'])) ? $data['CustomerPhotos']['TwitterConsumerKeySecret'] : '' ; ?>" />
                       </td>
                      </tr>
                         <tr>
                        <td>Twitter Access Token</td>
                        <td valign="top">
                            <input type="text" name="CustomerPhotos[TwitterAccessToken]" value="<?php echo(!empty($data['CustomerPhotos']['TwitterAccessToken'])) ? $data['CustomerPhotos']['TwitterAccessToken'] : '' ; ?>" />
                       </td>
                      </tr>
                      <tr>
                           <tr>
                        <td>Twitter Access Token Secret</td>
                        <td valign="top">
                            <input type="text" name="CustomerPhotos[TwitterAccessTokenSecret]" value="<?php echo(!empty($data['CustomerPhotos']['TwitterAccessTokenSecret'])) ? $data['CustomerPhotos']['TwitterAccessTokenSecret'] : '' ; ?>" />
                       </td>
                      </tr>  
                           <tr>
                        <td>Photos from Twitter<span class="help">Limit: 100 photos</span></td>
                        <td valign="top">
                            <input type="text" name="CustomerPhotos[TwitterPhotos]" value="<?php echo(!empty($data['CustomerPhotos']['TwitterPhotos'])) ? $data['CustomerPhotos']['TwitterPhotos'] : '5' ; ?>" />
                       </td>
                      </tr>              
                    </table>
                            <div class="clearfix"></div>
                    </div>

					 <div id="customTags" class="tab-pane">                            
                            <div class="box-heading">
                                <h1 style="float:left;">Custom Tags</h1>
                                <br /><br />
                            </div>
                        <table class="form">
                            <tr>
                                    <td><strong>Use site short-name</strong>:<span class="help">Adds the site short-name before the custom tags.</span></td>
                                    <td>
                                        <div class="well" style="float:right;text-align:left;width:50%">
                                         <i class="icon-warning-sign"></i> If you enable this feature, your site short-name will be added automatically to all custom tags. For example: If you have a custom tag <strong>"productone"</strong> and your store short-name is <strong>"std"</strong>, the final tag will be <strong>"stdproductone"</strong>.
                                        </div><br />
                                        <select name="CustomerPhotos[UseSiteShortName]" class="CustomerPhotosUseSiteShortName">
                                            <option value="yes" <?php echo (isset($data['CustomerPhotos']['UseSiteShortName']) && ($data['CustomerPhotos']['UseSiteShortName'] == 'yes')) ? 'selected=selected' : '' ?>>Enabled</option>
                                           <option value="no" <?php echo (isset($data['CustomerPhotos']['UseSiteShortName']) && ($data['CustomerPhotos']['UseSiteShortName'] == 'no')) ? 'selected=selected' : '' ?>>Disabled</option>
                                        </select>
                                   </td>
                                </tr>
                         </table>
						<?php $token = $_GET['token']; ?>
                         <table id="module" class="table table-bordered table-hover" width="100%" >
                          <thead>
                            <tr class="table-header">
                              <td class="left" width="40%"><strong>Product Name</strong></td>
                              <td class="left" width="40%"><strong>Product Tag</strong></td>
                              <td width="20%"><strong>Actons:</strong></td>
                            </tr>
                          </thead>
                          <?php $module_row = 0; ?>
                          <?php if (isset($CustomTags)) { 
						  	foreach ($CustomTags as $module) { ?>
                          <tbody id="module-row<?php echo $module_row; ?>">
                            <tr>
                              <td class="left">
								<?php $product = $this->model_catalog_product->getProduct($module['pid']); ?>
                                <input type="text" name="productName[<?php echo $module_row; ?>]" value="<?php echo $product['name']; ?>" />
                                <input type="hidden" name="customerphotos_customtags[<?php echo $module_row; ?>][pid]" value="<?php echo $module['pid'] ?>" />
                          </td>
                              <td class="left">
                              <div class="input-prepend"><span class="add-on">#</span>
								<input type="text" name="customerphotos_customtags[<?php echo $module_row; ?>][tag]" value="<?php echo $module['tag']; ?>" />                        	 </div>
                        </td>
                             <td class="left" style="vertical-align:middle;"><a onclick="$('#module-row<?php echo $module_row; ?>').remove();" class="btn btn-small btn-danger" style="text-decoration:none;"><i class="icon-remove"></i> <?php echo $button_remove; ?></a></td>
                           </tr>
                          </tbody>
                          <?php $module_row++; ?>
                          <?php } } ?>
                          <tfoot>
                            <tr>
                              <td colspan="2"></td>
                              <td class="left"><a onclick="addModule();" class="btn btn-small btn-primary"><i class="icon-plus"></i> Add New</a></td>
                            </tr>
                          </tfoot>
                        </table>
                        <?php if (isset($CustomTags)) { 
							for ($i = 0; $i<sizeof($CustomTags); $i++) { ?>
                        <script>
                        // ProductsCart
                        $('input[name=\'productName[<?php echo $i; ?>]\']').autocomplete({
                            delay: 500,
                            source: function(request, response) {
                                $.ajax({
                                    url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
                                    dataType: 'json',
                                    success: function(json) {		
                                        response($.map(json, function(item) {
                                            return {
                                                label: item.name,
                                                value: item.product_id
                                            }
                                        }));
                                    }
                                });
                            }, 
                            select: function(event, ui) {
								$('input[name=\'productName[<?php echo $i; ?>]\']').attr('value', ui.item.label);
								$('input[name=\'customerphotos_customtags[<?php echo $i; ?>][pid]\']').attr('value', ui.item.value);
                                return false;
                            },
                            focus: function(event, ui) {
                              return false;
                           }
                        });
                        //--></script>
                        <?php }  }?>
                        
						<script type="text/javascript"><!--
                        var module_row = <?php echo $module_row; ?>;
                        function addModule() {
                            html  = '<tbody style="display:none;" id="module-row' + module_row + '">';
                            html += '  <tr>';
                            html += '    <td class="left">';
							html += '<input type="text" name="productName[' + module_row + ']" value="" />';
							html += '<input type="hidden" name="customerphotos_customtags[' + module_row + '][pid]" value="" />';
                            html += '    </td>';
                            html += '    <td class="left"><div class="input-prepend"><span class="add-on">#</span>';
							html += '<input type="text" name="customerphotos_customtags[' + module_row + '][tag]" value="" />';
                          	html += '    </div></td>';
                            html += '    <td class="left" style="vertical-align:middle;"><a onclick="$(\'#module-row' + module_row + '\').remove();" class="btn btn-small btn-danger" style="text-decoration:none;"><i class="icon-remove"></i> <?php echo $button_remove; ?></a></td>';
                            html += '  </tr>';
                            html += '</tbody>';
                            
                            $('#module tfoot').before(html);
                            $('#module-row' + module_row).fadeIn();
                            // ProductsCart
                            (function(module_row){
                                $('input[name=\'productName['+ module_row +']\']').autocomplete({
                                    delay: 500,
                                    source: function(request, response) {
                                        $.ajax({
                                            url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
                                            dataType: 'json',
                                            success: function(json) {		
                                                response($.map(json, function(item) {
                                                    return {
                                                        label: item.name,
                                                        value: item.product_id
                                                    }
                                                }));
                                            }
                                        });
                                    }, 
                                    select: function(event, ui) {
										$('input[name=\'productName[' + module_row +']\']').attr('value', ui.item.label);
										$('input[name=\'customerphotos_customtags[' + module_row + '][pid]\']').attr('value', ui.item.value);
                                        return false;
                                    },
                                    focus: function(event, ui) {
                                      return false;
                                   }
                                });
                            })(module_row);
                            module_row++;
                        }
                        //-->
                        </script>
                            <div class="clearfix"></div>
                    </div>
                 </div>
               </div>
            </div>
	</div>
</div>