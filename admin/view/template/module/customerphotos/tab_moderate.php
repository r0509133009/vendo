<style>
.table th, .table td {
	border-top:none;
	border-bottom: 1px solid #DDD;
}
.prodid {
	font-size: 28px;
	width: 80px;
	vertical-align: middle;
	color: #333;
	padding-left: 20px;
}
.hideWrapper {
	display:none;	
}
</style>
<div class="row-fluid">
	<div class="span12">
        <div class="span8">
			<h2>Product tags moderation</h2>
            <div class="input-append" style="width:100%;">
              <input id="filterProductText" style="width:80%;" type="text" placeholder="Start typing product name">
              <button class="btn btn-filter-product" type="button"><i class="icon-filter"></i></button>
            </div>
            <table class="table">
              <thead>
                  <tr>
                    <th style="font-weight:bold; width:10%;"># ID</th>
                    <th style="font-weight:bold; width:30%;">Product name</th>
                    <th style="font-weight:bold; width:30%;">Tag</th>
                    <th style="font-weight:bold; width:15%;">Price</th>
                    <th style="font-weight:bold; width:15%;">Actions</th>
                  </tr>
              </thead>
              <tbody class="tbodyResults">
              </tbody>
            </table>
            
            <?php if (sizeof($modules)>0) { ?>
                <h2>Layout tags moderation</h2>
                <table class="table">
                  <thead>
                      <tr>
                        <th style="font-weight:bold; width:10%;"># ID</th>
                        <th style="font-weight:bold; width:30%;">Layout name</th>
                        <th style="font-weight:bold; width:30%;">Tag</th>
                        <th style="font-weight:bold; width:15%;">Position</th>
                        <th style="font-weight:bold; width:15%;">Actions</th>
                      </tr>
                  </thead>
                  <tbody class="tbodyResultsLayouts">
					  <?php foreach ($modules as $module) { ?>
                          <?php foreach ($layouts as $layout) { ?>
                                <?php if ($layout['layout_id'] == $module['layout_id']) { ?>
                                        <tr>
                                            <td class="prodid"><?php echo $layout['layout_id']; ?></td>
                                            <td class="prodname"><?php echo $layout['name']; ?></td>
                                            <td class="prodshortname"><?php if ($module['layout_id']==2) { ?> *no tag*<?php } else { echo $module['tag']; } ?> </td>
                                            <td class="price"><?php echo $module['position']; ?></td>
                                            <td><input type="button" class="btn btn-small <?php if ($module['layout_id']==2) { ?> disabled"<?php } else { ?>moderatePhotosButton" tag="<?php echo $module['tag']; ?>" <?php } ?> value="Moderate Photos"></td>
                                        </tr>
                                <?php } ?>
                          <?php } ?>
                      <?php } ?>
                  </tbody>
                </table>
            <?php } ?>
		</div>
         <input type="hidden" name="CustomerPhotos[IgnoredPhotos]" id="customerIgnoredPhotos" value="<?php echo(!empty($data['CustomerPhotos']['IgnoredPhotos'])) ? $data['CustomerPhotos']['IgnoredPhotos'] : '' ; ?>" />
        <div class="span4">
            <div class="box-heading">
              <h1><i class="icon-info-sign"></i>Moderation</h1>
            </div>
            <div class="box-content" style="min-height:100px; line-height:20px;">
            <p>Since CustomerPhotos directly fetches the photos from Instagram and Twitter, sometimes you may get tagged photos that are not relevant to your products. When you spot an irrelevant photo, you should find the product on the left and then click "Moderate photos" button. Then point to the irrelevant photo and exclude it from the list</p>
            </div>
        	<div class="recycleBinWrapper <?php if(empty($data['CustomerPhotos']['IgnoredPhotos'])) { echo 'hideWrapper'; } ?>">
                <div class="box-heading">
                  <h1><i class="icon-trash"></i>Trash</h1>
                </div>
                <div class="box-content" style="min-height:100px; line-height:20px;">
                <table class="table">
                  <tbody class="tbodyRecycled">
                  <?php $data['CustomerPhotos']['IgnoredPhotos'] = (isset($data['CustomerPhotos']['IgnoredPhotos'])) ? explode('###',$data['CustomerPhotos']['IgnoredPhotos']) : array(); ?>
                  <?php foreach ($data['CustomerPhotos']['IgnoredPhotos'] as $photo): if ($photo == '') continue; ?>
                  	<tr><td><img style="width:30px; margin-right:5px;" src="<?php echo $photo;  ?>" /></td><td style="text-align:right;"><a class="btn btn-mini btnRestorePhoto" imgurl="<?php echo $photo;  ?>">Restore </a></td></tr>
                  <?php endforeach; ?>
                  </tbody>
                </table>
                </div>
            </div>
        </div>
	</div>

</div>

<script>
var filterAndDisplayProducts = function() {
	$.ajax({
		url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $_GET['token']; ?>&store_id=<?php echo $this->request->get['store_id']; ?>&filter_name=%' +  encodeURIComponent($('#filterProductText').val()),
		dataType: 'json',
		success: function(json) {
			$('.tbodyResults').empty();
			$(json).each(function(i, e) {
				$('.tbodyResults').append('<tr><td class="prodid">'+e.product_id+'</td><td class="prodname">'+e.name+'</td><td class="prodshortname">'+e.short_name+'</td><td class="prodprice">'+e.price+'</td><td><input type="button" class="btn btn-small moderatePhotosButton" tag="'+e.short_name+'" value="Moderate Photos" /></td></tr>');
            });
			
		}
	});	
}

$('#filterProductText').bind('keyup', function() {
	if ($(this).val().length > 1) {
		filterAndDisplayProducts();
	}
})

$('.btn-filter-product').bind('click', function () {
	filterAndDisplayProducts();
	
});

$('.moderatePhotosButton').live('click',function() {
	loadInstagramPhotosByTag($(this).attr('tag'),$(this));
	loadTwitterPhotosByTag($(this).attr('tag'),$(this));
});


var loadInstagramPhotosByTag = function(tag,caller) {
	var instagramClientId = '<?php echo isset($data['CustomerPhotos']['InstagramAPIKey']) ? $data['CustomerPhotos']['InstagramAPIKey'] : 'test'; ?>';
	var instagramTag = tag;
	$('.tagsTable').remove();
	$.ajax({
		url: 'https://api.instagram.com/v1/tags/'+instagramTag+'/media/recent?client_id='+instagramClientId+'&count=<?php echo (isset($data['CustomerPhotos']['InstagramPhotos'])) ? $data['CustomerPhotos']['InstagramPhotos'] : '5'; ?>',
		dataType: 'jsonp',
		crossDomain: true,
		success: function(data) {
			if (data.data) {
				if (data.data.length > 0) {
					$(caller).after('<table class="tagsTable tag-'+tag+'"><thead><tr><th colspan="2">Instagram Photos ('+data.data.length+')</th></tr></thead><tbody></tbody></table>');
					var tc = 0;
					$(data.data).each(function(index, element) {
						if ($('#customerIgnoredPhotos').val().indexOf(element.images.low_resolution.url) == -1) {
							$('.tag-'+tag+' tbody').append('<tr><td><a href="'+element.link+'" target="_blank"><img style="width:30px; margin-right:5px;" src="'+element.images.low_resolution.url+'" /></a></td><td><a class="btn btnIgnorePhoto" imgurl="'+element.images.low_resolution.url+'"><i class="icon-trash"></i></a></td></tr>');
						} else {
							tc++;
						}
					});
					if (tc>0) {
						$('.tag-'+tag+' tbody').append('<tr><td colspan=2><span class="label">'+tc+' photo in the recycle bin</span></td></tr>');	
					}
				} else {
					$("div.label-info").hide();
					$(caller).after('<p><div class="label label-info" style="font-weight:normal">Instagram: No photos tagged with #'+tag+'</div></p>');
				}
			}
		}
	});
}

var loadTwitterPhotosByTag = function(tag,caller) {
	$('.tagsTableTwitter').remove();
	var twitterTag = tag;
	$.ajax({
		url: '../index.php?route=module/customerphotos/twitterTags&hashtag='+twitterTag,
		dataType: 'json',
		success: function(data) { 
			if (data && data.length>0) {
				$(caller).after('<table class="tagsTableTwitter ttag-'+tag+'"><thead><tr><th colspan="2">Twitter Photos ('+data.length+')</th></tr></thead><tbody></tbody></table>');
				var tc = 0;
				$(data).each(function(index, element) {
					if ($('#customerIgnoredPhotos').val().indexOf(element.image) == -1) {
						$('.ttag-'+tag+' tbody').append('<tr><td><a href="'+element.link+'" target="_blank"><img style="width:30px; margin-right:5px;" src="'+element.image+'" /></a></td><td><a class="btn btnIgnorePhoto" imgurl="'+element.image+'"><i class="icon-trash"></i></a></td></tr>');
					} else {
						tc++;
					}
				});
				if (tc>0) {
					$('.ttag-'+tag+' tbody').append('<tr><td colspan=2><span class="label">'+tc+' photo in the recycle bin</span></td></tr>');	
				}
			} else {
				$(caller).after('<p><div class="label label-info" style="font-weight:normal">Twitter: No photos tagged with #'+tag+'</div></p>')	
			}
		}
	});
	}

$('.btnIgnorePhoto').live('click', function() {
	if ($('#customerIgnoredPhotos').val().indexOf($(this).attr('imgurl')) == -1) {
		$('#customerIgnoredPhotos').val($('#customerIgnoredPhotos').val() + $(this).attr('imgurl') + '###') ;
		$(this).parents('tr').eq(0).css({opacity: 0.2});
		$('.save-changes').popover('show');
	}
});

$('.btnRestorePhoto').live('click', function() {
	if ($('#customerIgnoredPhotos').val().indexOf($(this).attr('imgurl')) != -1) {
		$('#customerIgnoredPhotos').val($('#customerIgnoredPhotos').val().replace($(this).attr('imgurl') + '###','')) ;
		$(this).parents('tr').eq(0).css({opacity: 0.2});
		$('.save-changes').popover('show');
	}
});

filterAndDisplayProducts();

</script>