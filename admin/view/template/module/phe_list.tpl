<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').attr('action', '<?php echo $email; ?>'); $('#form').submit();" class="button"><?php echo $button_email; ?></a><a onclick="$('#form').submit();" class="button"><?php echo $button_delete; ?></a><a href="<?php echo $insert; ?>" class="button"><?php echo $button_insert; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
	    <div id="tabs" class="htabs"><a href="#tab-list"><?php echo $tab_list; ?></a><a href="#tab-about"><?php echo $tab_about; ?></a></div>
	    <div id="tab-list" class="tab-content">
        <table class="list">
		  <thead>
		    <tr>
			  <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
			  <td class="left"><?php if ($sort == 'name') { ?>
			    <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
				<?php } else { ?>
				<a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
				<?php } ?></td>
			  <td class="left"><?php if ($sort == 'type') { ?>
			    <a href="<?php echo $sort_type; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_type; ?></a>
				<?php } else { ?>
				<a href="<?php echo $sort_type; ?>"><?php echo $column_type; ?></a>
				<?php } ?></td>
			  <td class="left"><?php echo $column_store; ?></td>
			  <td class="left"><?php echo $column_priority; ?></td>
			  <td class="right"><?php if ($sort == 'date_start') { ?>
			    <a href="<?php echo $sort_date_start; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_start; ?></a>
				<?php } else { ?>
				<a href="<?php echo $sort_date_start; ?>"><?php echo $column_date_start; ?></a>
				<?php } ?></td>
			  <td class="right"><?php if ($sort == 'date_end') { ?>
			    <a href="<?php echo $sort_date_end; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_end; ?></a>
				<?php } else { ?>
				<a href="<?php echo $sort_date_end; ?>"><?php echo $column_date_end; ?></a>
				<?php } ?></td>
			  <td class="right"><?php echo $column_action; ?></td>
			</tr>
		  </thead>
		  <tbody>
		  <?php if ($templates) { ?>
			<?php foreach ($templates as $template) { ?>
		    <tr>
			  <td style="text-align: center;"><?php if ($template['selected']) { ?>
				<input type="checkbox" name="selected[]" value="<?php echo $template['email_template_id']; ?>" checked="checked" />
                <?php } else { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $template['email_template_id']; ?>" />
                <?php } ?></td>
			  <td class="left"><?php echo $template['name']; ?></td>
			  <td class="left"><?php echo $template['type']; ?></td>
			  <td class="left"><?php echo $template['store']; ?></td>
			  <td class="left"><?php echo $template['priority']; ?></td>
			  <td class="right"><?php echo $template['date_start']; ?></td>
			  <td class="right"><?php echo $template['date_end']; ?></td>
			  <td class="right"><?php foreach ($template['action'] as $action) { ?>
				[ <a href="<?php echo $action['link']; ?>"><?php echo $action['name']; ?></a> ]<br />
				<?php } ?>
			</tr>
			<?php } ?>
		  <?php } else { ?>
		    <tr>
			  <td class="center" colspan="6"><?php echo $text_no_results; ?></td>
			</tr>
		  <?php } ?>
		  </tbody>
		</table>
		<div class="pagination"><?php echo $pagination; ?></div>
		</div>
		<div id="tab-about" class="tab-content">
		  <div style="float:left;margin-right:15px;">
		  <h2>Support</h2>
		  <table class="form">
			<tr>
			  <td colspan="2"><?php echo $text_support; ?></td>
			</tr>
			<tr>
			  <td><?php echo $entry_mail_name; ?></td>
			  <td><input type="text" name="mail_name" value="" /></td>
			</tr>
			<tr>
			  <td><?php echo $entry_mail_email; ?></td>
			  <td><input type="text" name="mail_email" value="" /></td>
			</tr>
			<tr>
			  <td><?php echo $entry_mail_order_id; ?></td>
			  <td><input type="text" name="mail_order_id" value="" /></td>
			</tr>
			<tr>
			  <td><?php echo $entry_mail_message; ?></td>
			  <td><textarea name="mail_message" style="width:300px; height:100px;"></textarea></td>
			</tr>
			<tr>
			  <td colspan="2"><a class="button" id="button-mail"><span><?php echo $button_mail; ?></span></a></td>
			</tr>
			<tr>
			  <td><a class="button" href="http://www.opencart.com/index.php?route=extension/extension/info&extension_id=11490" target="_blank" rel="nofollow"><span><?php echo $text_review; ?></span></a></td>
			  <td><a class="button" href="http://www.marketinsg.com/pretty-html-email" target="_blank"><span><?php echo $text_purchase; ?></span></a></td>
			</tr>
		  </table>
		  </div>
		  <div style="float:left;">
		  <h2>Follow Us</h2>
		  <iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2FMarketInSG&amp;width=300&amp;height=558&amp;show_faces=true&amp;colorscheme=light&amp;stream=true&amp;show_border=false&amp;header=false&amp;appId=391573267589280" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:300px; height:558px;" allowTransparency="true"></iframe>
		  </div>
		</div>
      </form>
    </div>
  </div>
  <div style="text-align:center; color:#222222;">Pretty HTML Email v2.4.3 by <a target="_blank" href="http://www.marketinsg.com/">MarketInSG</a></div>
</div>
<script type="text/javascript">
$('#button-mail').live('click', function() {
	$.ajax({
		url: 'index.php?route=module/phe/mail&token=<?php echo $token; ?>',
		type: 'post',
		data: $('input[name=\'mail_name\'], input[name=\'mail_email\'], input[name=\'mail_order_id\'], textarea[name=\'mail_message\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-mail').after('<span class="wait">&nbsp;<img src="view/image/loading.gif" alt="" /></span>');
		},
		success: function(json) {
			$('.wait, .error').remove();
			
			if (json['error']) {
				if (json['error']['warning']) {
					alert(json['error']['warning']);
				}
				
				if (json['error']['name']) {
					$('input[name=\'mail_name\']').after('<span class="error">' + json['error']['name'] + '</span>');
				}
				
				if (json['error']['email']) {
					$('input[name=\'mail_email\']').after('<span class="error">' + json['error']['email'] + '</span>');
				}
				
				if (json['error']['order_id']) {
					$('input[name=\'mail_order_id\']').after('<span class="error">' + json['error']['order_id'] + '</span>');
				}
				
				if (json['error']['message']) {
					$('textarea[name=\'mail_message\']').after('<span class="error">' + json['error']['message'] + '</span>');
				}
			} else {
				alert(json['success']);
				
				$('input[name=\'mail_name\']').val('');
				$('input[name=\'mail_email\']').val('');
				$('input[name=\'mail_order_id\']').val('');
				$('textarea[name=\'mail_message\']').val('');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});	
});

$('#tabs a').tabs();
</script>
<?php echo $footer; ?>