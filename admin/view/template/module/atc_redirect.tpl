<?php echo $header; ?>
<script type="text/javascript" src="view/javascript/jquery/colorpicker.js"></script>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:600' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="view/stylesheet/css/colorpicker.css" />
<div id="content">
<div class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
  <?php } ?>
</div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>

    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
		<div class="atc-redirect-wrapper" id="atc_redirect">
			<h3><?php echo $heading_title; ?></h3>
			<div class="content">
				<div>
					<div class="module-body clearfix">
						
						<div id="tabs" class="htabs main-tabs">	
							<a href="#settings" class=""><?php echo $text_settings_tab; ?></a>
							<a href="#about" class=""><?php echo $text_about_tab; ?></a>
						</div>
										
							<div id="settings" class="tab-content">	
								<h4>General</h4>
								<div class="input clearfix">
									<p><?php echo $entry_status; ?></p>								
									<?php if($atc_redirect_status == 1 && $atc_redirect_status != '') { echo '<div class="status status-on" title="1" rel="atc_redirect_status"></div>'; } else { echo '<div class="status status-off" title="0" rel="atc_redirect_status"></div>'; } ?>
									<input name="atc_redirect_status" value="<?php echo $atc_redirect_status; ?>" id="atc_redirect_status" type="hidden" />
									<div class="clear"></div>
								</div>
								
								
								<div class="input clearfix">
									<p><?php echo $entry_redirect_to; ?></p>
									<select name="atc_redirect_to">
										<option value="1" <?php if($atc_redirect_to == 1) { echo 'selected="selected"'; } ?>><?php echo $text_cart_page; ?></option>
										<option value="0" <?php if($atc_redirect_to == 0) { echo 'selected="selected"'; } ?>><?php echo $text_checkout_page; ?></option>
									</select>
									<div class="clear"></div>
								</div>
								
	
								<div class="input clearfix">
									<p><?php echo $entry_appearance; ?></p>
									<select name="button_appearance">
										<option value="1" <?php if($button_appearance == 1) { echo 'selected="selected"'; } ?>><?php echo $text_replace_current_button; ?></option>
										<option value="0" <?php if($button_appearance == 0) { echo 'selected="selected"'; } ?>><?php echo $text_additional_button; ?></option>
									</select>
									<div class="clear"></div>
								</div>
								<h4>Button options</h4>
								<div class="input clearfix">
									<p><?php echo $entry_button_text; ?></br>
									<span style="font-size:11px;color:#808080">Add to Cart, Buy Now</span></p>
									
									<input type="text" name="button_text"  value="<?php echo $button_text; ?>">
								</div>
								
								<div class="input-color  clearfix">
									<p><?php echo $entry_button_text_color; ?></br>
										<span style="font-size:11px;color:#808080">Default: <i>#FFFFFF</i></span>
									</p>
									<div><input type="text" name="button_text_color"  value="<?php echo $button_text_color; ?>" <?php if($button_text_color != '') { echo 'style="border-left: 30px solid '.$button_text_color.'"'; }  ?>></div>
								
									<p style="margin-left: 100px;"><?php echo $entry_button_text_color_hover; ?></br>
										<span style="font-size:11px;color:#808080">Default: <i>#FFFFFF</i></span>
									</p>
									<div><input type="text" name="button_text_color_hover"  value="<?php echo $button_text_color_hover; ?>" <?php if($button_text_color_hover != '') { echo 'style="border-left: 30px solid '.$button_text_color_hover.'"'; } ?>></div>
								</div>
								
								<div class="input-color  clearfix">
									<p><?php echo $entry_button_background; ?></br>
										<span style="font-size:11px;color:#808080">Default: <i>#17A5E4</i></span>
									</p>
									<div><input type="text" name="button_background"  value="<?php echo $button_background; ?>" <?php if($button_background != '') { echo 'style="border-left: 30px solid '.$button_background.'"'; }  ?>></div>
									
									<p style="margin-left: 100px;"><?php echo $entry_button_background_hover; ?></br>
										<span style="font-size:11px;color:#808080">Default: <i>#0E6389</i></span>
									</p>
									<div><input type="text" name="button_background_hover"  value="<?php echo $button_background_hover;?>" <?php if($button_background_hover != '') { echo 'style="border-left: 30px solid '.$button_background_hover.'"'; }  ?>></div>
								</div>

							<h4>Custom Status for Modules/Pages</h4>	
								<h5>Pages</h5>
								
								<div class="input statuses clearfix">
									<p>Product</p>
									<select name="atc_product_page_status">										
										<option value="1" <?php if($atc_product_page_status == 1) { echo 'selected="selected"'; } ?>>Enabled</option>
										<option value="0" <?php if($atc_product_page_status == 0 && $atc_product_page_status != '') { echo 'selected="selected"'; } ?>>Disabled</option>
									</select>
									<div class="clear"></div>
								</div>

								<div class="input statuses clearfix">
									<p>Category</p>
									<select name="atc_category_page_status">										
										<option value="1" <?php if($atc_category_page_status == 1) { echo 'selected="selected"'; } ?>>Enabled</option>
										<option value="0" <?php if($atc_category_page_status == 0 && $atc_category_page_status != '') { echo 'selected="selected"'; } ?>>Disabled</option>
									</select>
									<div class="clear"></div>
								</div>
								
								<div class="input statuses clearfix">
									<p>Manufacturer</p>
									<select name="atc_manufacturer_page_status">										
										<option value="1" <?php if($atc_manufacturer_page_status == 1) { echo 'selected="selected"'; } ?>>Enabled</option>
										<option value="0" <?php if($atc_manufacturer_page_status == 0 && $atc_manufacturer_page_status != '') { echo 'selected="selected"'; } ?>>Disabled</option>
									</select>
									<div class="clear"></div>
								</div>
								
								<div class="input statuses clearfix">
									<p>Search</p>
									<select name="atc_search_page_status">										
										<option value="1" <?php if($atc_search_page_status == 1) { echo 'selected="selected"'; } ?>>Enabled</option>
										<option value="0" <?php if($atc_search_page_status == 0 && $atc_search_page_status != '') { echo 'selected="selected"'; } ?>>Disabled</option>
									</select>
									<div class="clear"></div>
								</div>
								
								<div class="input statuses clearfix">
									<p>Special</p>
									<select name="atc_special_page_status">										
										<option value="1" <?php if($atc_special_page_status == 1) { echo 'selected="selected"'; } ?>>Enabled</option>
										<option value="0" <?php if($atc_special_page_status == 0 && $atc_special_page_status != '') { echo 'selected="selected"'; } ?>>Disabled</option>
									</select>
									<div class="clear"></div>
								</div>
								
								<h5>Modules</h5>
								
								<div class="input statuses clearfix">
									<p>Bestseller</p>
									<select name="atc_bestseller_module_status">										
										<option value="1" <?php if($atc_bestseller_module_status == 1) { echo 'selected="selected"'; } ?>>Enabled</option>
										<option value="0" <?php if($atc_bestseller_module_status == 0 && $atc_bestseller_module_status != '') { echo 'selected="selected"'; } ?>>Disabled</option>
									</select>
									<div class="clear"></div>
								</div>
								
								<div class="input statuses clearfix">
									<p>Featured</p>
									<select name="atc_featured_module_status">										
										<option value="1" <?php if($atc_featured_module_status == 1) { echo 'selected="selected"'; } ?>>Enabled</option>
										<option value="0" <?php if($atc_featured_module_status == 0 && $atc_featured_module_status != '') { echo 'selected="selected"'; } ?>>Disabled</option>
									</select>
									<div class="clear"></div>
								</div>
								
								<div class="input statuses clearfix">
									<p>Latest</p>
									<select name="atc_latest_module_status">										
										<option value="1" <?php if($atc_latest_module_status == 1) { echo 'selected="selected"'; } ?>>Enabled</option>
										<option value="0" <?php if($atc_latest_module_status == 0 && $atc_latest_module_status != '') { echo 'selected="selected"'; } ?>>Disabled</option>
									</select>
									<div class="clear"></div>
								</div>
								
								<div class="input statuses clearfix">
									<p>Special</p>
									<select name="atc_special_module_status">										
										<option value="1" <?php if($atc_special_module_status == 1) { echo 'selected="selected"'; } ?>>Enabled</option>
										<option value="0" <?php if($atc_special_module_status == 0 && $atc_special_module_status != '') { echo 'selected="selected"'; } ?>>Disabled</option>
									</select>
									<div class="clear"></div>
								</div>
							
							</div>
										
						<div id="about" class="tab-content">
							<div class="input clearfix">
								<p>Version:</p>
								<p>1.0</p>
								<div class="clear"></div>
							</div>		
							<div class="input clearfix">
								<p>Author:</p>
								<p>MB Labs</p>
								<div class="clear"></div>
							</div>
							<div class="input clearfix">
								<p>Compatibility:</p>
								<p style="width: 500px;">1.5.2, 1.5.2.1, 1.5.3, 1.5.3.1, 1.5.4, 1.5.4.1, 1.5.5, 1.5.5.1, 1.5.6, 1.5.6.1, 1.5.6.2, 1.5.6.3, 1.5.6.4</p>
								<div class="clear"></div>
							</div>
							
							<div class="input clearfix">
								<p>Support:</p>
								<p style="width: 500px;">purchaser.mblabs@gmail.com (48 hours)</p>
								<div class="clear"></div>
							</div>
						</div>
						</div>
					
					<div class="buttons">
						<a class="button-save" onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a>
						<a class="button-cancel" onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a>
						
					</div>
					
				</div>
			</div>
		</div>
    </form>
</div>

	  
	<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".status").live("click", function () {			
			var styl = $(this).attr("rel");
			var co = $(this).attr("title");			
			if(co == 1) {			
				$(this).removeClass('status-on');
				$(this).addClass('status-off');
				$(this).attr("title", "0");
				$("#"+styl+"").val(0);			
			}			
			if(co == 0) {			
				$(this).addClass('status-on');
				$(this).removeClass('status-off');
				$(this).attr("title", "1");
				$("#"+styl+"").val(1);			
			}			
		});

	});	
	</script>
	  
	<script type="text/javascript">

	$(document).ready(function() {

		$('.input-color input').ColorPicker({
			onChange: function (hsb, hex, rgb, el) {
				$(el).val("#" +hex);
				$(el).css("border-left", "30px solid #" + hex);
			},
			onShow: function (colpkr) {
				$(colpkr).show();
				return false;
			},
			onHide: function (colpkr) {
				$(colpkr).hide();
				return false;
			}
		});
	});
	</script>	   

<script type="text/javascript"><!--
		$(document).ready(function(){ 
			$('#tabs a').tabs();
			if($.cookie('tabs_cookie') > 0) {
				$('#tabs a').eq($.cookie('tabs_cookie')).trigger("click");
		}
			$('#tabs a').click(function() {
				var element_index = $('#tabs a').index(this);
				$.cookie('tabs_cookie', element_index); 
			});
		
		});
	//--></script>
	
<?php echo $footer; ?>