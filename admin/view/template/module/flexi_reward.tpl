<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<!-- <div class="warning">WE ARE CURRENTLY ADDING NEW FEATURES. Errors and Warning messages will show. Feature marked with [IN PROGRESS] are not included in the version you can purchase.</div>-->
<div class="box">
  <div class="heading">
    <h1><img src="view/image/module.png" alt="" /><?php echo $heading_title; ?></h1>
    <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
  </div>
  <?php if($flexi_group == 'no') {?>
  <div>
  	<h1>Please choose a customer group to work on:</h1>
      <?php foreach($customer_groups as $group) {      ?>
           <a href="<?php echo $action.'&flexi_group='.$group['customer_group_id']; ?>" > <?php echo $group['name']; ?></a><br>
      <?php } ?>
  </div>
  <?php } else { ?>
	<h1><a href="<?php echo $action;?>">Customer group</a>: <?php echo $flexi_group_name;  ?></h1>

  <?php $action = $action.'&flexi_group='.$flexi_group; ?>

  <div class="content">
        <div id="tabs" class="vtabs">
        	<a href="#tab-general"><?php echo $tab_general; ?></a>
            <a href="#tab-setting"><?php echo $tab_setting; ?></a>
            <a href="#tab-expiration"><?php echo 'Expiration';//$tab_expiration. ?></a>
            <a href="#tab-manufacturer"><?php echo 'Manufacturer';//$tab_manufacturer. ?></a>
            <a href="#tab-categories"><?php echo 'Categories';//$tab_categories. ?></a>
        </div>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <div id="tab-general" class="vtabs-content">
          <table class="form">
            <tr>
              <td><?php echo $entry_status; ?></td>
              <td><select name="flexi_reward_status">
                  <?php if ($flexi_reward_status) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
            </tr>
          </table>
        </div>
          <div id="tab-setting" class="vtabs-content">
          <table class="form">
            <tr>
              <td><?php echo $entry_order_status ; ?></td>
              <td>
               <select name="flexi_reward_order_status">
                <?php foreach ($order_statuses as $order_statuses) { ?>
                <?php if ($order_statuses['order_status_id'] == $flexi_reward_order_status_id) { ?>
                <option value="<?php echo $order_statuses['order_status_id']; ?>" selected="selected"><?php echo $order_statuses['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $order_statuses['order_status_id']; ?>"><?php echo $order_statuses['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
              </td>
            </tr>
            <tr>
              <td><?php echo $entry_rate; ?></td>
              <td><input name="flexi_reward_rate" type="text" value="<?php echo ${'flexi_reward_rate'}; ?>" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_purchase_text; ?></td>
              <td><input name="flexi_purchase_rate" type="text" value="<?php echo ${'flexi_purchase_rate'}; ?>" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_total_subtotal_operator; ?></td>
              <td>
                  <div style="width:100px; display:inline-block;">Total <br><input name="flexi_total_subtotal_operator" type="radio" value="1" <?php echo ${'flexi_total_subtotal_operator'} > 0 ? 'checked="checked"' :''; ?> /> </div>
                  <div style="width:100px; display:inline-block;">Sub-total <br><input name="flexi_total_subtotal_operator" type="radio" value="0" <?php echo ${'flexi_total_subtotal_operator'} < 1 ? 'checked="checked"' :''; ?> /></div>
         	  </td>
            </tr>
            
            <tr>
              <td><?php echo $entry_redeem_min_order_amount; ?></td>
              <td><input name="flexi_redeem_min_order_amount" type="text" value="<?php echo ${'flexi_redeem_min_order_amount'}; ?>" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_redeem_max; ?></td>
              <td><input name="flexi_redeem_max" type="text" value="<?php echo ${'flexi_redeem_max'}; ?>" /></td>
            </tr>
            <tr>
              <td colspan="2">
              		<h2><?php echo $text_head_ignorepoints; ?></h2>
                    <p> <?php echo $text_head_ignore_description; ?></p>
              </td>
            </tr>            
            <tr>
              <td><?php echo $entry_ignore_product_points_value; ?></td>
              <td>
                  <div style="width:100px; display:inline-block;">Yes <br><input name="flexi_ignore_product_points_value" type="radio" value="1" <?php echo ${'flexi_ignore_product_points_value'} > 0 ? 'checked="checked"' :''; ?> /> </div>
                  <div style="width:100px; display:inline-block;">No <br><input name="flexi_ignore_product_points_value" type="radio" value="0" <?php echo ${'flexi_ignore_product_points_value'} < 1 ? 'checked="checked"' :''; ?> /></div>
         	  </td>
            </tr>
            <tr>
              <td><?php echo $entry_ignore_product_rewards_value; ?></td>
              <td>
                  <div style="width:100px; display:inline-block;">Yes <br><input name="flexi_ignore_product_rewards_value" type="radio" value="1" <?php echo ${'flexi_ignore_product_rewards_value'} > 0 ? 'checked="checked"' :''; ?> /> </div>
                  <div style="width:100px; display:inline-block;">No <br><input name="flexi_ignore_product_rewards_value" type="radio" value="0" <?php echo ${'flexi_ignore_product_rewards_value'} < 1 ? 'checked="checked"' :''; ?> /></div>
         	  </td>
            </tr>
            
            <tr>
              <td colspan="2"><h2><?php echo $text_head_useractions; ?></h2></td>
            </tr>
            <tr>
              <td><?php echo $entry_registration; ?></td>
              <td><input name="flexi_registration_points" type="text" value="<?php echo ${'flexi_registration_points'}; ?>" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_newsletter_signup; ?></td>
              <td><input name="flexi_newsletter_sign_up_points" type="text" value="<?php echo ${'flexi_newsletter_sign_up_points'}; ?>" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_productreview; ?></td>
              <td><input name="flexi_productreview_points" type="text" value="<?php echo ${'flexi_productreview_points'}; ?>" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_firstorder; ?></td>
              <td><input name="flexi_firstorder_points" type="text" value="<?php echo ${'flexi_firstorder_points'}; ?>" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_firstorder_operator; ?></td>
              <td>
                  <div style="width:100px; display:inline-block;">Fixed <br><input name="flexi_firstorder_operator" type="radio" value="1" <?php echo ${'flexi_firstorder_operator'} > 0 ? 'checked="checked"' :''; ?> /> </div>
                  <div style="width:100px; display:inline-block;">Percent <br><input name="flexi_firstorder_operator" type="radio" value="0" <?php echo ${'flexi_firstorder_operator'} < 1 ? 'checked="checked"' :''; ?> /></div>
         	  </td>
            </tr>


          </table>
        </div>
          <div id="tab-expiration" class="vtabs-content">
          <table class="form">
            <tr>
              <td class="left" colspan="5"><?php echo $entry_exp_days_init_text; ?></td>
            </tr>
            <?php    $xpi = 0;     
             if(count($flexi_exp_days)>0) {
            	 foreach($flexi_exp_days as $days)  {
             ?>
            <tr id="module-row<?php echo $xpi ?>">
              <td class="left"><?php echo $entry_exp_days_text; ?></td>
              <td class=""> <input name="flexi_exp_days[<?php echo $xpi; ?>][days]" type="text" value="<?php echo $days['days']; ?>" /></td>
              <td class="left"><?php echo $entry_exp_days_price_multiplier; ?></td>
              <td class=""> <input name="flexi_exp_days[<?php echo $xpi; ?>][rate]" type="text" value="<?php echo $days['rate']; ?>" /></td>
              <td class="">
              <?php if($xpi > 0) {?>
         	     <a onclick="$('#module-row<?php echo $xpi; ?>').remove();" class="button"><?php echo $button_remove; ?></a>
              <?php }?>
              </td>
            </tr>
            <?php $xpi++; }}?>

            <tfoot>
            <tr>
              <td class="left"><a onclick="addModule();" class="button"><?php echo $button_add_rule; ?></a></td>
            </tr>
            </tfoot>

          </table>
        </div>

          <div id="tab-manufacturer" class="vtabs-content">
          <table class="form">
            <?php    
            $mfci = 0;  
              
             if(count($flexi_manufacturers)>0) {
            	 foreach($flexi_manufacturers as $mfc_row)  {
             ?>
            <tr id="mfc-row<?php echo $mfci; ?>">
              <td class="left"><?php //echo $entry_mfc_text; ?>Choose Manufacturers this multiplier will apply to</td>
			  <td>
              	<div class="scrollbox">
	              <?php  $mfcii = 1; foreach($flexi_all_manufacturers as $manufacturer) { ?>
						<div class="<?php echo (int)($mfcii/2)*2 == $mfcii ? 'even': 'odd';?>">
                	        <input type="checkbox" value="<?php echo $manufacturer['manufacturer_id']; ?>" name="flexi_manufacturers[<?php echo $mfci; ?>][ids][]" <?php echo in_array($manufacturer['manufacturer_id'], $mfc_row['ids'] ) ? 'checked="checked"':''; ?> >
            	    		<?php echo $manufacturer['name']; ?> 
        	           </div>
        	          <?php $mfcii++ ;} ?>
        	       </div>
             	  <a onclick="$(this).parent().find(':checkbox').attr('checked', true);">Маркирай всичко</a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);">Изтрий всичко</a>
               </td>
               <td class="left"><?php echo $entry_rate; ?></td>
               <td class=""> <input name="flexi_manufacturers[<?php echo $mfci; ?>][rate]" type="text" value="<?php echo $mfc_row['rate']; ?>" /></td>
               <td class=""><a onclick="$('#mfc-row<?php echo $mfci; ?>').remove();" class="button"><?php echo $button_remove; ?></a></td>
            </tr>
            <?php $mfci++; }}?>

            <tfoot>
            <tr>
              <td class="left"><a onclick="addMfc();" class="button"><?php echo $button_add_rule; ?></a></td>
            </tr>
            </tfoot>

          </table>
        </div>
        
        
          <div id="tab-categories" class="vtabs-content">
          <table class="form">
            <?php    
            $cati = 0;  
               
             if(count($flexi_categories)>0) {
            	 foreach($flexi_categories as $cat_row)  {
             ?>
            <tr id="cat-row<?php echo $cati; ?>">
              <td class="left"><?php //echo $entry_cat_text; ?>Choose Categories this multiplier will apply to. Note that selecting a root category won't automatically select all sub categories.</td>
			  <td>
              	<div class="scrollbox">
	              <?php  $catii = 1; foreach($flexi_all_categories as $category) { ?>
						<div class="<?php echo (int)($catii/2)*2 == $catii ? 'even': 'odd';?>">
                	        <input type="checkbox" value="<?php echo $category['category_id']; ?>" name="flexi_categories[<?php echo $cati; ?>][ids][]" <?php echo in_array($category['category_id'], $cat_row['ids'] ) ? 'checked="checked"':''; ?> >
            	    		<?php echo $category['name']; ?> 
        	           </div>
        	          <?php $catii++ ;} ?>
        	       </div>
             	  <a onclick="$(this).parent().find(':checkbox').attr('checked', true);">Маркирай всичко</a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);">Изтрий всичко</a>
               </td>
               <td class="left"><?php echo $entry_rate; ?></td>
               <td class=""> <input name="flexi_categories[<?php echo $cati; ?>][rate]" type="text" value="<?php echo $cat_row['rate']; ?>" /></td>
               <td class=""><a onclick="$('#cat-row<?php echo $cati; ?>').remove();" class="button"><?php echo $button_remove; ?></a></td>
            </tr>
            <?php $cati++; }}?>

            <tfoot>
            <tr>
              <td class="left"><a onclick="addCat();" class="button"><?php echo $button_add_rule; ?></a></td>
            </tr>
            </tfoot>

          </table>
        </div>

        
    </form>
      </div>
 
   <?php } ?>
  </div>
</div>
<script type="text/javascript"><!--
$('.vtabs a').tabs();
//--></script>
<script type="text/javascript"><!--
var module_row = <?php echo $xpi?$xpi:0; ?>;
var mfc_row = <?php echo $mfci?$mfci:0; ?>;
var cat_row = <?php echo $cati?$cati:0; ?>;

/*TODO::::  add rows for each range*/
function addModule() {	
	html  = '<tr id="module-row' + module_row + '">';
   	html += ' <td class="left"><?php echo $entry_exp_days_text; ?></td>';
   	html += ' <td class=""> <input name="flexi_exp_days['+ module_row +'][days]" type="text" value="30" /></td>';
   	html += ' <td class="left"><?php echo $entry_rate; ?></td>';
   	html += ' <td class=""> <input name="flexi_exp_days['+ module_row +'][rate]" type="text" value="0.02" /></td>';
   	html += ' <td class=""><a onclick="$(\'#module-row'+ module_row +'\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
	html += '</tr>';
	
	$('#tab-expiration tfoot').before(html);
	
	module_row++;
}

function addMfc() {	

	html  = '<tr id="mfc-row' + mfc_row + '">';
   	html += ' <td class="left"><?php //echo $entry_mfc_text; ?>Choose Manufacturers this multiplier will apply to</td>';
	html += ' <td>';
	html += '	<div class="scrollbox">';
			<?php  $mfcii = 1; foreach($flexi_all_manufacturers as $manufacturer) { ?>
	html += '		<div class="<?php echo (int)($mfcii/2)*2 == $mfcii ? 'even': 'odd';?>">';
	html += '			    <input type="checkbox" value="<?php echo $manufacturer['manufacturer_id']; ?>" name="flexi_manufacturers['+mfc_row+'][ids][]"  >';
	html += '			    <?php echo $manufacturer['name']; ?> ';
	html += '		</div>';
        	          <?php $mfcii++ ;} ?>
	html += '      </div>';
    html += '  	  <a onclick="$(this).parent().find(\':checkbox\').attr(\'checked\', true);">Маркирай всичко</a> / <a onclick="$(this).parent().find(\':checkbox\').attr(\'checked\', false);">Изтрий всичко</a>';
    html += '  </td>';
    html += '  <td class="left"><?php echo $entry_rate; ?></td>';
    html += '  <td class=""> <input name="flexi_manufacturers[' + mfc_row + '][rate]" type="text" value="0.01" /></td>';
    html += '  <td class=""><a onclick="$(\'#mfc-row' + mfc_row + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
    html += '  </tr>';

	
	$('#tab-manufacturer tfoot').before(html);
	
	mfc_row++;
}

function addCat() {	

	html  = '<tr id="cat-row' + cat_row + '">';
   	html += ' <td class="left"><?php //echo $entry_cat_text; ?>Note that selecting a root category won\'t automatically select all sub categories.</td>';
	html += ' <td>';
	html += '	<div class="scrollbox">';
			<?php  $catii = 1; foreach($flexi_all_categories as $category) { ?>
	html += '		<div class="<?php echo (int)($catii/2)*2 == $catii ? 'even': 'odd';?>">';
	html += '			    <input type="checkbox" value="<?php echo $category['category_id']; ?>" name="flexi_categories['+cat_row+'][ids][]"  >';
	html += '			    <?php echo $category['name']; ?> ';
	html += '		</div>';
        	          <?php $catii++ ;} ?>
	html += '      </div>';
    html += '  	  <a onclick="$(this).parent().find(\':checkbox\').attr(\'checked\', true);">Маркирай всичко</a> / <a onclick="$(this).parent().find(\':checkbox\').attr(\'checked\', false);">Изтрий всичко</a>';
    html += '  </td>';
    html += '  <td class="left"><?php echo $entry_rate; ?></td>';
    html += '  <td class=""> <input name="flexi_categories[' + cat_row + '][rate]" type="text" value="0.01" /></td>';
    html += '  <td class=""><a onclick="$(\'#cat-row' + cat_row + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
    html += '  </tr>';

	
	$('#tab-categories tfoot').before(html);
	
	cat_row++;
}

//--></script> 

<?php echo $footer; ?>