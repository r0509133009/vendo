<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>

  <div class="box">
    <div class="heading">
      <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
		<div id="tabs" class="htabs">
			<a href="#tab-setting"><?php echo 'Setting'; ?></a>
			<a href="#tab-data"><?php echo 'Data'; ?></a>
		</div>
		<div id="tab-setting">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
				<table class="form cutstom-produt">
				  <tr>
					<td><?php echo $entry_coupon; ?></td>
					<td>
						<select name="colorboxpopup" id="input-banner" class="form-control">
							<?php foreach ($coupons as $coupon) { ?>
							<?php if ($coupon['coupon_id'] == $colorboxpopup) { ?>
							<option value="<?php echo $coupon['coupon_id']; ?>" selected="selected"><?php echo $coupon['name']; ?></option>
							<?php } else { ?>
							<option value="<?php echo $coupon['coupon_id']; ?>"><?php echo $coupon['name']; ?></option>
							<?php } ?>
							<?php } ?>
						</select>
					</td>
				  </tr>
				  <tr>
					<td><?php echo $entry_status; ?></td>
					<td>
						  <select name="colorboxpopup_status" id="input-status" class="form-control">
							<?php if ($colorboxpopup_status) { ?>
							<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
							<option value="0"><?php echo $text_disabled; ?></option>
							<?php } else { ?>
							<option value="1"><?php echo $text_enabled; ?></option>
							<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
							<?php } ?>
						  </select>
					</td>
				  </tr>
					<tr>
						<td><h3>First Popup</h3></td>
						<td></td>
					</tr>
					<tr>
						<td><?php echo $entry_dicount_title_txt; ?></td>
						<td>
							<input type="text" name="colorboxpopup_dicount_title" value="<?php echo $colorboxpopup_dicount_title; ?>" placeholder="<?php echo $entry_dicount_title_txt; ?>" id="input-name" class="form-control" />
						</td>
					</tr>
					<tr>
						<td><?php echo $entry_dicount_desc; ?></td>
						<td>
							<input type="text" name="colorboxpopup_dicount_desc" value="<?php echo $colorboxpopup_dicount_desc; ?>" placeholder="<?php echo $entry_dicount_desc; ?>" id="input-name" class="form-control" />
						</td>
					</tr>
					<tr>
						<td><h3>Second Popup</h3></td>
						<td></td>
					</tr>
					<tr>
						<td><?php echo $entry_dicount_title_txt_second; ?></td>
						<td>
							<input type="text" name="colorboxpopup_dicount_title_second" value="<?php echo $colorboxpopup_dicount_title_second; ?>" placeholder="<?php echo $entry_dicount_title_txt_second; ?>" id="input-name" class="form-control" />
						</td>
					</tr>
					<tr>
						<td><?php echo $entry_dicount_desc_second; ?></td>
						<td>
							<input type="text" name="colorboxpopup_dicount_desc_second" value="<?php echo $colorboxpopup_dicount_desc_second; ?>" placeholder="<?php echo $entry_dicount_desc_second; ?>" id="input-name" class="form-control" />
						</td>
					</tr>
				</table>
				</form>
		</div>
		<div id="tab-data">
			<table class="list">
				  <thead>
					<tr>
					  <td class="left"><?php echo 'Name'; ?></td>
					  <td class="left"><?php echo 'E-mail'; ?></td>
					  <td class="left"><?php echo 'Date Added'; ?></td>
					</tr>
				  </thead>
				  <tbody>
					<?php if ($popupdata) { ?>
					<?php foreach ($popupdata as $data) { ?>
					<tr>
					  <td class="left"><?php echo $data['name']; ?></td>
					  <td class="left"><?php echo $data['email_id']; ?></td>
					  <td class="left"><?php echo $data['date_added']; ?></td>
					</tr>
					<?php } ?>
					<?php } else { ?>
					<tr>
					  <td class="center" colspan="4"><?php echo $text_no_results; ?></td>
					</tr>
					<?php } ?>
				  </tbody>
			</table>
			<div class="pagination"><?php echo $pagination; ?></div>
		</div>
    </div>
 </div>
</div>
<script type="text/javascript"><!--
$('#tabs a').tabs();
//--></script>
<?php echo $footer; ?>