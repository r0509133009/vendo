<?php echo $header; ?>
<div id="content">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php echo (empty($data['CustomerPhotos']['LicensedOn'])) ? base64_decode('PGRpdiBjbGFzcz0iYWxlcnQgYWxlcnQtZGFuZ2VyIGZhZGUgaW4iPg0KICAgICAgICA8YnV0dG9uIHR5cGU9ImJ1dHRvbiIgY2xhc3M9ImNsb3NlIiBkYXRhLWRpc21pc3M9ImFsZXJ0IiBhcmlhLWhpZGRlbj0idHJ1ZSI+w5c8L2J1dHRvbj4NCiAgICAgICAgPGg0Pldhcm5pbmchIFVubGljZW5zZWQgdmVyc2lvbiBvZiB0aGUgbW9kdWxlITwvaDQ+DQogICAgICAgIDxwIHN0eWxlPSJtYXJnaW4tdG9wOjVweDsiPllvdSBhcmUgcnVubmluZyBhbiB1bmxpY2Vuc2VkIHZlcnNpb24gb2YgdGhpcyBtb2R1bGUhIFlvdSBuZWVkIHRvIGVudGVyIHlvdXIgbGljZW5zZSBjb2RlIHRvIGVuc3VyZSBwcm9wZXIgZnVuY3Rpb25pbmcsIGFjY2VzcyB0byBzdXBwb3J0IGFuZCB1cGRhdGVzLjwvcD48ZGl2IHN0eWxlPSJoZWlnaHQ6NXB4OyI+PC9kaXY+DQogICAgICAgIDxhIGNsYXNzPSJidG4gYnRuLWRhbmdlciIgIG9uY2xpY2s9IiQoJ2FbaHJlZj0jc3VwcF0nKS5jbGljaygpIj5FbnRlciB5b3VyIGxpY2Vuc2UgY29kZTwvYT4NCiAgICA8L2Rpdj4=') : '' ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-error"><i class="icon-exclamation-sign"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
    <?php if (!empty($this->session->data['success'])) { ?>
    <div class="alert alert-success autoSlideUp"><i class="icon-ok-sign"></i> <?php echo $this->session->data['success']; ?></div>
    <script> $('.autoSlideUp').delay(3000).fadeOut(600, function(){ $(this).show().css({'visibility':'hidden'}); }).slideUp(600);</script>
    <?php $this->session->data['success'] = null; } ?>
  <div class="box">
    <div class="box-heading">
		<div class="currentStoreName btn-group">
        	<a class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
        		<?php echo $store['name']; if($store['store_id'] == 0) echo ' <strong>(Default)</strong>'; ?>&nbsp;<span class="caret"></span>		
        	</a>
        	<ul class="dropdown-menu">
            	<?php foreach ($stores as $st) { ?>
            		<li <?php if($store['store_id'] == $st['store_id']){ echo 'class="disabled"';}?>>
            			<a href="index.php?route=module/customerphotos&store_id=<?php echo $st['store_id'];?>&token=<?php echo $this->session->data['token']; ?>">
            				<?php echo $st['name']; ?>
            			</a>
            		</li>
            	<?php } ?> 
        	</ul>
    	</div>
      <h1><i class="icon-picture"></i> <?php echo $heading_title; ?></h1>
    </div>
    <div class="content fadeInOnLoad">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
		<input type="hidden" name="store_id" value="<?php echo $store['store_id']; ?>">
        <div class="tabbable">
		  <div class="tab-navigation">        
          <ul class="nav nav-tabs mainMenuTabs">
            <li class="active"><a href="#controlpanel" data-toggle="tab"><i class="icon-wrench"></i> Control Panel</a></li>
            <li><a href="#moderate" data-toggle="tab"><i class="icon-eye-open"></i> Moderate</a></li>
            <li><a href="#settings" data-toggle="tab"><i class="icon-cog"></i> Settings</a></li>
            <li><a href="#supp" data-toggle="tab"><i class="icon-share"></i> Support</a></li>        
          </ul>
          <div class="tab-buttons">
            <button type="submit" class="btn btn-primary save-changes" data-toggle="popover" data-placement="bottom" data-content="Your changes require clicking this button if you wish to be saved" title="" data-original-title="Required action"><i class="icon-ok"></i> Save changes</button>        
          </div>
          </div>
         <div class="tab-content">
			<div id="controlpanel" class="tab-pane active">
              <?php require_once(DIR_APPLICATION.'view/template/module/customerphotos/tab_controlpanel.php'); ?>                        
            </div>         
			<div id="moderate" class="tab-pane">
              <?php require_once(DIR_APPLICATION.'view/template/module/customerphotos/tab_moderate.php'); ?>                        
            </div>
			<div id="settings" class="tab-pane">
              <?php require_once(DIR_APPLICATION.'view/template/module/customerphotos/tab_settings.php'); ?>                        
            </div>
			<div id="supp" class="tab-pane">
              <?php require_once(DIR_APPLICATION.'view/template/module/customerphotos/tab_support.php'); ?>                        
            </div>
          </div><!-- /.tab-content -->
        </div><!-- /.tabbable -->
      </form>
    </div>
  </div>
</div>
<script>
if (window.localStorage && window.localStorage['currentTab']) {
	$('.mainMenuTabs a[href='+window.localStorage['currentTab']+']').trigger('click');  
}
if (window.localStorage && window.localStorage['currentSubTab']) {
	$('a[href='+window.localStorage['currentSubTab']+']').trigger('click');  
}
$('.fadeInOnLoad').css('visibility','visible');
$('.mainMenuTabs a[data-toggle="tab"]').click(function() {
	if (window.localStorage) {
		window.localStorage['currentTab'] = $(this).attr('href');
	}
});
$('a[data-toggle="tab"]:not(.mainMenuTabs a[data-toggle="tab"])').click(function() {
	if (window.localStorage) {
		window.localStorage['currentSubTab'] = $(this).attr('href');
	}
});
</script>

<?php echo $footer; ?>