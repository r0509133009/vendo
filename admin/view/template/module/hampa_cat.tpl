<?php
/**
 * @total-module	Hide Admin Products and Category fields
 * @author-name 	◘ Dotbox Creative
 * @copyright		Copyright (C) 2014 ◘ Dotbox Creative www.dotboxcreative.com
 * @license			GNU/GPL, see http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
 */ 
 ?>
<?php echo $header; ?>
<div id="content">
<div class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
  <?php } ?>
</div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<div class="box">
  <div class="heading">
    <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
   <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
  </div>
  <div class="content">  
      <div id="tabs" class="htabs"><a href="#tab-product"><?php echo $tab_product; ?></a><a href="#tab-category"><?php echo $tab_category; ?></a></div>
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
         
         <div id="tab-product">
                <h2><?php echo $tab_general; ?></h2>
          		<table class="form">
            		<tr>
            			<td><?php echo $tab_meta_tag_des; ?></td>
              			<td><?php if ($hampa_hidden_meta_description) { ?>
               					<label><input type="radio" name="hampa_hidden_meta_description" value="1" checked="checked" /><?php echo $hidden; ?></label>  
                				<label><input type="radio" name="hampa_hidden_meta_description" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_meta_description" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_meta_description" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr> 
            		<tr>
            			<td><?php echo $tab_meta_tag_key; ?></td>
              			<td><?php if ($hampa_hidden_meta_keywords) { ?>
               					<label><input type="radio" name="hampa_hidden_meta_keywords" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_meta_keywords" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_meta_keywords" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_meta_keywords" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr> 
            		<tr>
            			<td><?php echo $tab_product_tag; ?></td>
              			<td><?php if ($hampa_hidden_product_tabs) { ?>
               					<label><input type="radio" name="hampa_hidden_product_tags" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_product_tags" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_product_tags" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_product_tags" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr>            		
          		</table>
          		<h2><?php echo $tab_data; ?></h2>
          		<table class="form">
            		<tr>
            			<td><?php echo $tab_sku; ?></td>
              			<td><?php if ($hampa_hidden_sku) { ?>
               					<label><input type="radio" name="hampa_hidden_sku" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_sku" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_sku" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_sku" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr> 
            		<tr>
            			<td><?php echo $tab_upc; ?></td>
              			<td><?php if ($hampa_hidden_upc) { ?>
               					<label><input type="radio" name="hampa_hidden_upc" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_upc" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_upc" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_upc" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr>
            		<tr>
            			<td><?php echo $tab_ean; ?></td>
              			<td><?php if ($hampa_hidden_ean) { ?>
               					<label><input type="radio" name="hampa_hidden_ean" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_ean" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_ean" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_ean" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr>
            		<tr>
            			<td><?php echo $tab_jan; ?></td>
              			<td><?php if ($hampa_hidden_jan) { ?>
               					<label><input type="radio" name="hampa_hidden_jan" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_jan" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_jan" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_jan" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr>
            		<tr>
            			<td><?php echo $tab_isbn; ?></td>
              			<td><?php if ($hampa_hidden_isbn) { ?>
               					<label><input type="radio" name="hampa_hidden_isbn" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_isbn" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_isbn" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_isbn" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr>
            		<tr>
            			<td><?php echo $tab_mnp; ?></td>
              			<td><?php if ($hampa_hidden_mpn) { ?>
               					<label><input type="radio" name="hampa_hidden_mpn" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_mpn" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_mpn" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_mpn" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr>
            		<tr>
            			<td><?php echo $tab_location; ?></td>
              			<td><?php if ($hampa_hidden_location) { ?>
               					<label><input type="radio" name="hampa_hidden_location" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_location" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_location" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_location" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr>          		
            		<tr>
            			<td><?php echo $tab_price; ?></td>
              			<td><?php if ($hampa_hidden_price) { ?>
               					<label><input type="radio" name="hampa_hidden_price" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_price" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_price" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_price" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?>
                		&nbsp;&nbsp;&nbsp;<label><input type="text" size="5" name="hampa_hidden_price_default" value="<?php if (!empty($hampa_hidden_price_default)) { echo $hampa_hidden_price_default; } else echo 0; ?>" />&nbsp;&nbsp;<?php echo $tab_price_info; ?> 
                		</td>
            		</tr>         		    		
            		<tr>
            			<td><?php echo $tab_tax; ?></td>
              			<td><?php if ($hampa_hidden_tax_class) { ?>
               					<label><input type="radio" name="hampa_hidden_tax_class" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_tax_class" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_tax_class" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_tax_class" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr>
            		<tr>
            			<td><?php echo $tab_quantity; ?></td>
              			<td><?php if ($hampa_hidden_quantity) { ?>
               					<label><input type="radio" name="hampa_hidden_quantity" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_quantity" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_quantity" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_quantity" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?>
                		&nbsp;&nbsp;&nbsp;<label><input type="text" size="5" name="hampa_hidden_quantity_default" value="<?php if (!empty($hampa_hidden_quantity_default)) { echo $hampa_hidden_quantity_default; } else echo 1; ?>" />&nbsp;&nbsp;<?php echo $tab_quantity_info; ?>
                		</td>
            		</tr> 
            		<tr>
            			<td><?php echo $tab_min_quantity; ?></td>
              			<td><?php if ($hampa_hidden_entry_minimum) { ?>
               					<label><input type="radio" name="hampa_hidden_entry_minimum" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_entry_minimum" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_entry_minimum" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_entry_minimum" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr>
            		<tr>
            			<td><?php echo $tab_sub_stock; ?></td>
              			<td><?php if ($hampa_hidden_subtract) { ?>
               					<label><input type="radio" name="hampa_hidden_subtract" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_subtract" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_subtract" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_subtract" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr>
            		<tr>
            			<td><?php echo $tab_ooss; ?></td>
              			<td><?php if ($hampa_hidden_stock_status) { ?>
               					<label><input type="radio" name="hampa_hidden_stock_status" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_stock_status" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_stock_status" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_stock_status" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr>
            		<tr>
            			<td><?php echo $tab_req_shipping; ?></td>
              			<td><?php if ($hampa_hidden_entry_shipping) { ?>
               					<label><input type="radio" name="hampa_hidden_entry_shipping" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_entry_shipping" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_entry_shipping" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_entry_shipping" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr>
            		<tr>
            			<td><?php echo $tab_seo; ?></td>
              			<td><?php if ($hampa_hidden_seo_keyword) { ?>
               					<label><input type="radio" name="hampa_hidden_seo_keyword" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_seo_keyword" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_seo_keyword" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_seo_keyword" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr>
            		<tr>
            			<td><?php echo $tab_date_ava; ?></td>
              			<td><?php if ($hampa_hidden_date_available) { ?>
               					<label><input type="radio" name="hampa_hidden_date_available" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_date_available" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_date_available" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_date_available" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr>
            		<tr>
            			<td><?php echo $tab_dimensions; ?></td>
              			<td><?php if ($hampa_hidden_dimension) { ?>
               					<label><input type="radio" name="hampa_hidden_dimension" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_dimension" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_dimension" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_dimension" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr>
            		<tr>
            			<td><?php echo $tab_length; ?></td>
              			<td><?php if ($hampa_hidden_length) { ?>
               					<label><input type="radio" name="hampa_hidden_length" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_length" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_length" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_length" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr>
            		<tr>
            			<td><?php echo $tab_weight; ?></td>
              			<td><?php if ($hampa_hidden_weight) { ?>
               					<label><input type="radio" name="hampa_hidden_weight" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_weight" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_weight" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_weight" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr>
            		<tr>
            			<td><?php echo $tab_weight_class; ?></td>
              			<td><?php if ($hampa_hidden_weight_class) { ?>
               					<label><input type="radio" name="hampa_hidden_weight_class" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_weight_class" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_weight_class" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_weight_class" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr>
            		<tr>
            			<td><?php echo $tab_sort; ?></td>
              			<td><?php if ($hampa_hidden_sort_order) { ?>
               					<label><input type="radio" name="hampa_hidden_sort_order" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_sort_order" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_sort_order" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_sort_order" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr>
                    </table>
            		<h2><?php echo $tab_link; ?></h2>
                    <table class="form">
            		<tr>
            			<td><?php echo $tab_manufacturer; ?></td>
              			<td><?php if ($hampa_hidden_manufacturer) { ?>
               					<label><input type="radio" name="hampa_hidden_manufacturer" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_manufacturer" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_manufacturer" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_manufacturer" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr>
            		<tr>
            			<td><?php echo $tab_filters; ?></td>
              			<td><?php if ($hampa_hidden_filter) { ?>
               					<label><input type="radio" name="hampa_hidden_filter" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_filter" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_filter" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_filter" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr>
            		<tr>
            			<td><?php echo $tab_stores; ?></td>
              			<td><?php if ($hampa_hidden_store) { ?>
               					<label><input type="radio" name="hampa_hidden_store" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_store" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_store" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_store" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr>
            		<tr>
            			<td><?php echo $tab_downloads; ?></td>
              			<td><?php if ($hampa_hidden_download) { ?>
               					<label><input type="radio" name="hampa_hidden_download" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_download" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_download" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_download" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr>
            		<tr>
            			<td><?php echo $tab_related; ?></td>
              			<td><?php if ($hampa_hidden_related) { ?>
               					<label><input type="radio" name="hampa_hidden_related" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_related" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_related" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_related" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr> 		           		
          		</table> 
          		<h2><?php echo $tab_atribute; ?></h2>
          		<table class="form">
            		<tr>
            			<td><?php echo $tab_atribute_sec; ?></td>
              			<td><?php if ($hampa_hidden_attribute_tab) { ?>
               					<label><input type="radio" name="hampa_hidden_attribute_tab" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_attribute_tab" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>

                				<label><input type="radio" name="hampa_hidden_attribute_tab" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_attribute_tab" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr> 
            	</table>   
            	<h2><?php echo $tab_option; ?></h2>
          		<table class="form">
            		<tr>
            			<td><?php echo $tab_option_sec; ?></td>
              			<td><?php if ($hampa_hidden_option_tab) { ?>
               					<label><input type="radio" name="hampa_hidden_option_tab" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_option_tab" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_option_tab" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_option_tab" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr> 
            	</table>
                <h2><?php echo $tab_profile; ?></h2>
          		<table class="form">
            		<tr>
            			<td><?php echo $tab_profile_sec; ?></td>
              			<td><?php if ($hampa_hidden_profile_tab) { ?>
               					<label><input type="radio" name="hampa_hidden_profile_tab" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_profile_tab" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_profile_tab" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_profile_tab" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr> 
            	</table>     
            	<h2><?php echo $tab_discount; ?></h2>
          		<table class="form">
            		<tr>
            			<td><?php echo $tab_discount_sec; ?></td>
              			<td><?php if ($hampa_hidden_discount_tab) { ?>
               					<label><input type="radio" name="hampa_hidden_discount_tab" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_discount_tab" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_discount_tab" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_discount_tab" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr> 
            	</table> 
            	<h2><?php echo $tab_special; ?></h2>
          		<table class="form">
            		<tr>
            			<td><?php echo $tab_special_sec; ?></td>
              			<td><?php if ($hampa_hidden_special_tab) { ?>
               					<label><input type="radio" name="hampa_hidden_special_tab" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_special_tab" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_special_tab" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_special_tab" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr> 
            	</table>   	
            	<h2><?php echo $tab_image; ?></h2>
            	<table class="form">
            		<tr>
            			<td><?php echo $tab_image_sec; ?></td>
              			<td><?php if ($hampa_hidden_image_tab) { ?>
               					<label><input type="radio" name="hampa_hidden_image_tab" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_image_tab" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_image_tab" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_image_tab" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr> 
            	</table> 
            	<h2><?php echo $tab_reward; ?></h2>
          		<table class="form">
            		<tr>
            			<td><?php echo $tab_reward_sec; ?></td>
              			<td><?php if ($hampa_hidden_reward_tab) { ?>
               					<label><input type="radio" name="hampa_hidden_reward_tab" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_reward_tab" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_reward_tab" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_reward_tab" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr> 
            	</table>              	
            	<h2><?php echo $tab_design; ?></h2>
          		<table class="form">
            		<tr>
            			<td><?php echo $tab_design_sec; ?></td>
              			<td><?php if ($hampa_hidden_design_tab) { ?>
               					<label><input type="radio" name="hampa_hidden_design_tab" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_design_tab" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_design_tab" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_design_tab" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr> 
            	</table>   
                <h2><?php echo $tab_marketplace; ?></h2>
          		<table class="form">
            		<tr>
            			<td><?php echo $tab_marketplace_sec; ?></td>
              			<td><?php if ($hampa_hidden_marketplace_tab) { ?>
               					<label><input type="radio" name="hampa_hidden_marketplace_tab" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_marketplace_tab" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_marketplace_tab" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_marketplace_tab" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr> 
            	</table>  
                <h2><?php echo $tab_status; ?></h2>
                 <table class="form">
            		<tr>
            			<td><?php echo $tab_collabse; ?></td>
              			<td><?php if ($hampa_hidden_collabse) { ?>
               					<label><input type="radio" name="hampa_hidden_collabse" value="1" checked="checked" /><?php echo $text_enabled; ?></label>
                				<label><input type="radio" name="hampa_hidden_collabse" value="0" /><?php echo $text_disabled; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_collabse" value="1" /><?php echo $text_enabled; ?></label>
                				<label><input type="radio" name="hampa_hidden_collabse" value="0" checked="checked" /><?php echo $text_disabled; ?></label>
                		<?php } ?></td>
            		</tr>            		
          		</table>         
                <table class="form">
            		<tr>
            			<td><?php echo $tab_status_sec; ?></td>
              			<td><?php if ($hampa_hidden_fields_enabled) { ?>
               					<label><input type="radio" name="hampa_hidden_fields_enabled" value="1" checked="checked" /><?php echo $text_enabled; ?></label>
                				<label><input type="radio" name="hampa_hidden_fields_enabled" value="0" /><?php echo $text_disabled; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_fields_enabled" value="1" /><?php echo $text_enabled; ?></label>
                				<label><input type="radio" name="hampa_hidden_fields_enabled" value="0" checked="checked" /><?php echo $text_disabled; ?></label>
                		<?php } ?></td>
            		</tr>            		
          		</table>               		
    </div>
    
    <div id="tab-category">
    
    <h2><?php echo $tab_general_cat; ?></h2>

    <table class="form">
        <tr>
            <td><?php echo $tab_meta_tag_des_cat; ?></td>
            <td><?php if ($hampa_hidden_meta_description_cat) { ?>
                    <label><input type="radio" name="hampa_hidden_meta_description_cat" value="1" checked="checked" /><?php echo $hidden; ?> <label>
                    <label><input type="radio" name="hampa_hidden_meta_description_cat" value="0" /><?php echo $visible; ?></label>
                <?php } else { ?>
                    <label><input type="radio" name="hampa_hidden_meta_description_cat" value="1" /><?php echo $hidden; ?></label>
                    <label><input type="radio" name="hampa_hidden_meta_description_cat" value="0" checked="checked" /><?php echo $visible; ?></label>
            	<?php } ?></td>
        </tr> 
        <tr>
            <td><?php echo $tab_meta_tag_key_cat; ?></td>
            <td><?php if ($hampa_hidden_meta_keywords_cat) { ?>
                    <label><input type="radio" name="hampa_hidden_meta_keywords_cat" value="1" checked="checked" /><?php echo $hidden; ?></label>
                    <label><input type="radio" name="hampa_hidden_meta_keywords_cat" value="0" /><?php echo $visible; ?></label>
                <?php } else { ?>
                    <label><input type="radio" name="hampa_hidden_meta_keywords_cat" value="1" /><?php echo $hidden; ?></label>
                    <label><input type="radio" name="hampa_hidden_meta_keywords_cat" value="0" checked="checked" /><?php echo $visible; ?></label>
                <?php } ?></td>
        </tr> 
    </table>
    
    <h2><?php echo $tab_data_cat; ?></h2>
    <table class="form">
    
       	<tr>
            <td><?php echo $tab_filters_cat; ?></td>
            <td><?php if ($hampa_hidden_filter_cat) { ?>
                    <label><input type="radio" name="hampa_hidden_filter_cat" value="1" checked="checked" /><?php echo $hidden; ?></label>
                    <label><input type="radio" name="hampa_hidden_filter_cat" value="0" /><?php echo $visible; ?></label>
                <?php } else { ?>
                    <label><input type="radio" name="hampa_hidden_filter_cat" value="1" /><?php echo $hidden; ?></label>
                    <label><input type="radio" name="hampa_hidden_filter_cat" value="0" checked="checked" /><?php echo $visible; ?></label>
            <?php } ?></td>
        </tr>
            <tr>
            <td><?php echo $tab_stores_cat; ?></td>
            <td><?php if ($hampa_hidden_store_cat) { ?>
                        <label><input type="radio" name="hampa_hidden_store_cat" value="1" checked="checked" /><?php echo $hidden; ?></label>
                        <label><input type="radio" name="hampa_hidden_store_cat" value="0" /><?php echo $visible; ?></label>
                    <?php } else { ?>
                        <label><input type="radio" name="hampa_hidden_store_cat" value="1" /><?php echo $hidden; ?></label>
                        <label><input type="radio" name="hampa_hidden_store_cat" value="0" checked="checked" /><?php echo $visible; ?></label>
                <?php } ?></td>
        </tr>
		<tr>
            <td><?php echo $tab_seo_cat; ?></td>
            <td><?php if ($hampa_hidden_seo_keyword_cat) { ?>
                    <label><input type="radio" name="hampa_hidden_seo_keyword_cat" value="1" checked="checked" /><?php echo $hidden; ?></label>
                    <label><input type="radio" name="hampa_hidden_seo_keyword_cat" value="0" /><?php echo $visible; ?></label>
                <?php } else { ?>
                    <label><input type="radio" name="hampa_hidden_seo_keyword_cat" value="1" /><?php echo $hidden; ?></label>
                    <label><input type="radio" name="hampa_hidden_seo_keyword_cat" value="0" checked="checked" /><?php echo $visible; ?></label>
            <?php } ?></td>
        </tr>
        <tr>
            <td><?php echo $tab_image_cat; ?></td>
            <td><?php if ($hampa_hidden_image_cat) { ?>
                    <label><input type="radio" name="hampa_hidden_image_cat" value="1" checked="checked" /><?php echo $hidden; ?></label>
                    <label><input type="radio" name="hampa_hidden_image_cat" value="0" /><?php echo $visible; ?></label>
                <?php } else { ?>
                    <label><input type="radio" name="hampa_hidden_image_cat" value="1" /><?php echo $hidden; ?></label>
                    <label><input type="radio" name="hampa_hidden_image_cat" value="0" checked="checked" /><?php echo $visible; ?></label>
            <?php } ?></td>
        </tr>
		<tr>
            <td><?php echo $tab_top_cat; ?></td>
            <td><?php if ($hampa_hidden_top_cat) { ?>
                    <label><input type="radio" name="hampa_hidden_top_cat" value="1" checked="checked" /><?php echo $hidden; ?></label>
                    <label><input type="radio" name="hampa_hidden_top_cat" value="0" /><?php echo $visible; ?></label>
                <?php } else { ?>
                    <label><input type="radio" name="hampa_hidden_top_cat" value="1" /><?php echo $hidden; ?></label>
                    <label><input type="radio" name="hampa_hidden_top_cat" value="0" checked="checked" /><?php echo $visible; ?></label>
            <?php } ?></td>
        </tr>
		<tr>
            <td><?php echo $tab_columns_cat; ?></td>
            <td><?php if ($hampa_hidden_columns_cat) { ?>
                    <label><input type="radio" name="hampa_hidden_columns_cat" value="1" checked="checked" /><?php echo $hidden; ?></label>
                    <label><input type="radio" name="hampa_hidden_columns_cat" value="0" /><?php echo $visible; ?></label>
                <?php } else { ?>
                    <label><input type="radio" name="hampa_hidden_columns_cat" value="1" /><?php echo $hidden; ?></label>
                    <label><input type="radio" name="hampa_hidden_columns_cat" value="0" checked="checked" /><?php echo $visible; ?></label>
            <?php } ?></td>
        </tr>
		<tr>
            <td><?php echo $tab_sort_cat; ?></td>
            <td><?php if ($hampa_hidden_sort_order_cat) { ?>
                    <label><input type="radio" name="hampa_hidden_sort_order_cat" value="1" checked="checked" /><?php echo $hidden; ?></label>
                    <label><input type="radio" name="hampa_hidden_sort_order_cat" value="0" /><?php echo $visible; ?></label>
                <?php } else { ?>
                    <label><input type="radio" name="hampa_hidden_sort_order_cat" value="1" /><?php echo $hidden; ?></label>
                    <label><input type="radio" name="hampa_hidden_sort_order_cat" value="0" checked="checked" /><?php echo $visible; ?></label>
            <?php } ?></td>
        </tr>
       </table>
            <h2><?php echo $tab_design_cat; ?></h2>
          		<table class="form">
            		<tr>
            			<td><?php echo $tab_design_sec_cat; ?></td>
              			<td><?php if ($hampa_hidden_design_tab_cat) { ?>
               					<label><input type="radio" name="hampa_hidden_design_tab_cat" value="1" checked="checked" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_design_tab_cat" value="0" /><?php echo $visible; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_design_tab_cat" value="1" /><?php echo $hidden; ?></label>
                				<label><input type="radio" name="hampa_hidden_design_tab_cat" value="0" checked="checked" /><?php echo $visible; ?></label>
                		<?php } ?></td>
            		</tr> 
            	</table>       
			<h2><?php echo $tab_status_cat; ?></h2>
             <table class="form">
            		<tr>
            			<td><?php echo $tab_collabse_cat; ?></td>
              			<td><?php if ($hampa_hidden_collabse_cat) { ?>
               					<label><input type="radio" name="hampa_hidden_collabse_cat" value="1" checked="checked" /><?php echo $text_enabled; ?></label>
                				<label><input type="radio" name="hampa_hidden_collabse_cat" value="0" /><?php echo $text_disabled; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_collabse_cat" value="1" /><?php echo $text_enabled; ?></label>
                				<label><input type="radio" name="hampa_hidden_collabse_cat" value="0" checked="checked" /><?php echo $text_disabled; ?></label>
                		<?php } ?></td>
            		</tr>            		
          		</table>       
                <table class="form">
            		<tr>
            			<td><?php echo $tab_status_sec_cat; ?></td>
              			<td><?php if ($hampa_hidden_fields_enabled_cat) { ?>
               					<label><input type="radio" name="hampa_hidden_fields_enabled_cat" value="1" checked="checked" /><?php echo $text_enabled; ?></label>
                				<label><input type="radio" name="hampa_hidden_fields_enabled_cat" value="0" /><?php echo $text_disabled; ?></label>
                			<?php } else { ?>
                				<label><input type="radio" name="hampa_hidden_fields_enabled_cat" value="1" /><?php echo $text_enabled; ?></label>
                				<label><input type="radio" name="hampa_hidden_fields_enabled_cat" value="0" checked="checked" /><?php echo $text_disabled; ?></label>
                		<?php } ?></td>
            		</tr>            		
          		</table>       
   		 </div>    
    </form>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
$('#tabs a').tabs(); 
//--></script> 
<?php echo $footer; ?>