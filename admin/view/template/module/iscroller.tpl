<?php
/**
 * iScroller 1.2.1, May 7, 2016
 * Copyright 2014-2016 Igor Bukin / egozza88@gmail.com
 * License: Commercial. Reselling of this software or its derivatives is not allowed. You may use this software for one website ONLY including all its subdomains if the top level domain belongs to you and all subdomains are parts of the same OpenCart store.
 * Support: oc.iscroller@gmail.com
 */
?>
<?php echo $header; ?>
<div id="content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) : ?>
            <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php endforeach; ?>
    </div>
    <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
    <?php if ($success) { ?>
        <div class="success"><?php echo $success; ?></div>
    <?php } ?>
    <div class="box">
        <div class="heading">
            <h1><img src="view/image/module.png" alt="" /> <?php echo $lang_heading_title_adv; ?></h1>
            <div class="buttons">
                <a onclick="jQuery('#form-iscroller').submit();return false;" class="button"><?php echo $lang_save_and_stay; ?></a>
                <a onclick="jQuery('#form-iscroller [name=action]').val('save');jQuery('#form-iscroller').submit();return false;" class="button"><?php echo $lang_save_and_close; ?></a>
                <a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a>
            </div>
        </div>
    <div class="content">
        <div class="panel panel-default">
            <div class="panel-body">
                <div id="vtab-option" class="vtabs">
                    <a href="<?php echo $action; ?>" class="<?php if (!$moduleId) : ?>selected<?php endif; ?>">
                        <?php echo $lang_add_instance; ?>
                        &nbsp;
                        <img src="view/image/add.png" />
                    </a>
                    <?php if (count($modules)) : ?>
                    <?php foreach ($modules as $i => $module) : ?>
                    <a href="<?php echo $action; ?>&module_id=<?php echo $i; ?>" class="<?php if ($moduleId == $i) : ?>selected<?php endif; ?>">
                        <?php echo $module['name']; ?>
                        &nbsp;
                        <img src="view/image/delete.png" alt="" onclick="window.location.href='<?php echo $removeModuleAction;?>&module_id=<?php echo $i; ?>';return false;">
                    </a>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </div>
                <div class="vtabs-content">
                <form action="<?php echo $action; ?><?php if ($moduleId) : echo "&module_id=" . $moduleId; endif; ?>" method="post" enctype="multipart/form-data" id="form-iscroller" class="form-horizontal">
                    <input type="hidden" name="action" value="apply" />
                    <div class="form-group">
                        <label for="container-selector" class="control-label col-sm-4 col-md-3"><?php echo $lang_module_name; ?></label>
                        <div class="col-sm-8 col-md-9">
                            <input type="text" name="iscroller_module[name]" value="" id="show-btn-after" class="form-control" />
                        </div>
                    </div>
                    <div id="tabs" class="htabs">
                        <a href="#infinite-scroller" class="selected" style="display: inline;"><?php echo $lang_infinite_scroller; ?></a>
                        <a href="#scroll-to-top" style="display: inline;"><?php echo $lang_scroll_to_top; ?></a>
                        <a href="#translation" style="display: inline;"><?php echo $lang_translation; ?></a>
                        <a href="#layouts" style="display: inline;"><?php echo $lang_layouts; ?></a>
                    </div>
                    <div class="tab-content">
                        <div id="infinite-scroller">
                            <div class="form-group">
                                <label class="control-label col-sm-4 col-md-3"><?php echo $lang_enable_infinite_scroll; ?></label>
                                <div class="col-sm-8 col-md-9">
                                    <label><input type="radio" name="iscroller_module[infScroll][enabled]" value="1" /> <?php echo $text_yes; ?></label>
                                    <label><input type="radio" name="iscroller_module[infScroll][enabled]" value="0" /> <?php echo $text_no; ?></label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 col-md-3 control-label" for="input-loading-mode"><?php echo $lang_loading_mode; ?></label>
                                <div class="col-sm-8 col-md-9">
                                    <select name="iscroller_module[infScroll][loadingMode]" id="input-status" class="form-control">
                                            <option value="auto"><?php echo $lang_auto; ?></option>
                                            <option value="button"><?php echo $lang_with_button; ?></option>
                                            <option value="smart"><?php echo $lang_smart; ?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group" id="show-btn-after-row" style="display:none;">
                                <label for="show-btn-after" class="control-label col-sm-4 col-md-3"><?php echo $lang_show_button_after; ?></label>
                                <div class="col-sm-8 col-md-9">
                                    <input type="text" name="iscroller_module[infScroll][show_btn_after]" value="" id="show-btn-after" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4 col-md-3"><?php echo $lang_stateful_scroll; ?></label>
                                <div class="col-sm-8 col-md-9">
                                    <label><input type="radio" name="iscroller_module[infScroll][statefulScroll]" value="1" class="inf-scr-stateful" /> <?php echo $text_yes; ?></label>
                                    <label><input type="radio" name="iscroller_module[infScroll][statefulScroll]" value="0" class="inf-scr-stateful" /> <?php echo $text_no; ?></label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4 col-md-3"><?php echo $lang_show_page_separator; ?></label>
                                <div class="col-sm-8 col-md-9">
                                    <label><input type="radio" name="iscroller_module[infScroll][showSeparator]" value="1" /> <?php echo $text_yes; ?></label>
                                    <label><input type="radio" name="iscroller_module[infScroll][showSeparator]" value="0" /> <?php echo $text_no; ?></label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4 col-md-3"><?php echo $lang_animation; ?></label>
                                <div class="col-sm-8 col-md-9">
                                    <label><input type="radio" name="iscroller_module[infScroll][animation]" value="1" /> <?php echo $text_yes; ?></label>
                                    <label><input type="radio" name="iscroller_module[infScroll][animation]" value="0" /> <?php echo $text_no; ?></label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="container-selector" class="control-label col-sm-4 col-md-3"><?php echo $lang_container_selector; ?></label>
                                <div class="col-sm-8 col-md-9">
                                    <input type="text" name="iscroller_module[infScroll][containerSelector]" value="" id="container-selector" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="paginator-selector" class="control-label col-sm-4 col-md-3"><?php echo $lang_paginator_selector; ?></label>
                                <div class="col-sm-8 col-md-9">
                                    <input type="text" name="iscroller_module[infScroll][paginatorSelector]" value="" id="paginator-selector" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="item-selector" class="control-label col-sm-4 col-md-3"><?php echo $lang_item_selector; ?></label>
                                <div class="col-sm-8 col-md-9">
                                    <input type="text" name="iscroller_module[infScroll][itemSelector]" value="" id="item-selector" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="min-dist-to-bottom" class="control-label col-sm-4 col-md-3"><?php echo $lang_min_dist_to_bottom; ?></label>
                                <div class="col-sm-8 col-md-9">
                                    <input type="text" name="iscroller_module[infScroll][minDistToBottom]" value="" id="min-dist-to-bottom" class="form-control" />
                                </div>
                            </div>

                            <div class="form-separator"></div>
                            <div class="form-group">
                                <label class="control-label col-sm-4 col-md-3"><?php echo $lang_enhanced_compatibility; ?></label>
                                <div class="col-sm-8 col-md-9">
                                    <label><input type="radio" name="iscroller_module[infScroll][domObserverEnabled]" value="1" /> <?php echo $text_yes; ?></label>
                                    <label><input type="radio" name="iscroller_module[infScroll][domObserverEnabled]" value="0" /> <?php echo $text_no; ?></label>
                                    &nbsp;&nbsp;&nbsp;
                                    <a class="btn btn-default btn-sm button" onclick="jQuery('#compatibility-notice').toggle();">
                                        <?php echo $lang_read_the_notice; ?>&nbsp;↓
                                    </a>
                                </div>
                            </div>
                            <div class="collapse" id="compatibility-notice" style="display:none;">
                                <div class="alert alert-warning" role="alert">
                                    <?php echo $lang_dom_observer_desrcription; ?>
                                </div>
                                <div class="alert alert-info" role="alert">
                                    <?php echo $lang_dom_observer_advanced; ?>
                                </div>
                            </div>
                            <div class="form-separator"></div>
                            <div class="form-group">
                                <label for="custom-js-code" class="control-label col-sm-4 col-md-3"><?php echo $lang_custom_js_code; ?></label>
                                <div class="col-sm-8 col-md-9">
                                    <textarea name="iscroller_module[infScroll][customJsCode]" id="custom-js-code" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="custom-css-code" class="control-label col-sm-4 col-md-3"><?php echo $lang_custom_css_code; ?></label>
                                <div class="col-sm-8 col-md-9">
                                    <textarea name="iscroller_module[infScroll][customCssCode]" id="custom-css-code" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4 col-md-3"><?php echo $lang_custom_loading_animation; ?></label>
                                <div class="col-sm-8 col-md-9">
                                    <span id="thumb-image">
                                        <img src="<?php echo !empty($settings['infScroll']['customGif']) 
                                                ? $catalog . 'image/' . $settings['infScroll']['customGif'] 
                                                : $catalog . 'catalog/view/theme/default/image/oc-iscroller-loader.gif'; ?>" 
                                             alt="" title="" 
                                             data-placeholder="<?php echo HTTP_CATALOG . 'catalog/view/theme/default/image/oc-iscroller-loader.gif'; ?>">
                                    </span>
                                    <input name="iscroller_module[infScroll][customGif]" id="loader-img" class="form-control" type="hidden" />
                                    <a id="button-image" class="btn btn-primary button"><i class="fa fa-pencil"></i><?php echo $lang_edit; ?></a> 
                                    <a id="button-clear" class="btn btn-danger button"><i class="fa fa-trash-o"></i><?php echo $lang_reset; ?></a>
                                </div>
                            </div>
                        </div>
                        
                        <div id="scroll-to-top">
                            <div class="form-group">
                                <label class="control-label col-sm-4 col-md-3"><?php echo $lang_enable_sroll_to_top; ?></label>
                                <div class="col-sm-8 col-md-9">
                                    <label><input type="radio" name="iscroller_module[scrollTop][enabled]" value="1" /> <?php echo $text_yes; ?></label>
                                    <label><input type="radio" name="iscroller_module[scrollTop][enabled]" value="0" /> <?php echo $text_no; ?></label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 col-md-3 control-label" for="scroll-top-pos"><?php echo $lang_scroll_top_pos; ?></label>
                                <div class="col-sm-8 col-md-9">
                                    <select name="iscroller_module[scrollTop][position]" id="scroll-top-pos" class="form-control">
                                            <option value="left"><?php echo $lang_left; ?></option>
                                            <option value="right"><?php echo $lang_right; ?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4 col-md-3"><?php echo $lang_fit_to_container; ?></label>
                                <div class="col-sm-8 col-md-9">
                                    <input type="text" class="form-control" name="iscroller_module[scrollTop][fitTo]" value="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4 col-md-3"><?php echo $lang_bar_color; ?></label>
                                <div class="col-sm-8 col-md-9">
                                    <input type="text" class="form-control colorpic" name="iscroller_module[scrollTop][barColor]" value="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4 col-md-3"><?php echo $lang_arrow_color; ?></label>
                                <div class="col-sm-8 col-md-9">
                                    <input type="text" class="form-control colorpic" name="iscroller_module[scrollTop][arrowColor]" value="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4 col-md-3"><?php echo $lang_scroll_bar_speed; ?></label>
                                <div class="col-sm-8 col-md-9">
                                    <input type="text" class="form-control" name="iscroller_module[scrollTop][speed]" value="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4 col-md-3"><?php echo $lang_min_width_to_show_bar; ?></label>
                                <div class="col-sm-8 col-md-9">
                                    <input type="text" class="form-control" name="iscroller_module[scrollTop][minWidthToShow]" value="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4 col-md-3"><?php echo $lang_min_width_show_bar; ?></label>
                                <div class="col-sm-8 col-md-9">
                                    <input type="text" class="form-control" name="iscroller_module[scrollTop][minWidthToShowAsBar]" value="" />
                                </div>
                            </div>
                        </div>
                        
                        <div class="tab-pane" id="translation">
                            <div id="langs" class="htabs col-sm-8 col-md-9 col-sm-offset-4 col-md-offset-3">
                                <?php $i = 0; foreach ($languages as $language) { $i++; ?>
                                    <a href="#loading-msg-lang-<?php echo $language['language_id']; ?>" <?php if ($i===1) { ?> class="selected"<?php } ?> style="display: inline;"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a>
                                <?php } ?>
                            </div>
                            <div class="tab-content">
                            <?php $i = 0; foreach ($languages as $language) { $i++; ?>
                                <div <?php if ($i!==1) { ?> style="display: none;"<?php } else { ?> style="display: block;"<?php } ?> id="loading-msg-lang-<?php echo $language['language_id']; ?>">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4 col-md-3"><?php echo $lang_loading_msg; ?></label>
                                        <div class="col-sm-8 col-md-9">
                                            <input type="text" class="form-control" name="iscroller_module[infScroll][loadingMsg][<?php echo $language['code']; ?>]" value="<?php echo $lang_loading_def_msg; ?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4 col-md-3"><?php echo $lang_finish_msg; ?></label>
                                        <div class="col-sm-8 col-md-9">
                                            <input type="text" class="form-control" name="iscroller_module[infScroll][finishMsg][<?php echo $language['code']; ?>]" value="<?php echo $lang_finish_def_msg; ?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4 col-md-3"><?php echo $lang_button_label; ?></label>
                                        <div class="col-sm-8 col-md-9">
                                            <input type="text" class="form-control" name="iscroller_module[infScroll][buttonLabel][<?php echo $language['code']; ?>]" value="<?php echo $lang_button_def_label; ?>" />
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            </div>
                        </div>
                        <div id="layouts">
                            <input type="hidden" name="iscroller_module[module_numb]" value="0" />
                            <table id="module" class="list">
                                <thead>
                                    <tr>
                                        <td class="left"><?php echo $lang_layout; ?></td>
                                        <td class="left"><?php echo $lang_position; ?></td>
                                        <td></td>
                                    </tr>
                                </thead>
                                <tbody id="module-row">
                                    <tr>
                                        <td class="left"><select name="iscroller_module[layout][{n}][layout_id]">
                                                <?php foreach ($layouts as $layout) { ?>
                                                    <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                                                <?php } ?>
                                            </select></td>
                                        <td class="left"><select name="iscroller_module[layout][{n}][position]">
                                                <option value="content_top"><?php echo $lang_content_top; ?></option>
                                                <option value="content_bottom" selected="selected"><?php echo $lang_content_bottom; ?></option>
                                                <option value="column_left"><?php echo $lang_column_left; ?></option>
                                                <option value="column_right"><?php echo $lang_column_right; ?></option>
                                            </select></td>
                                        <td class="left"><a class="button remove-module-btn"><?php echo $lang_button_remove; ?></a></td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="2"></td>
                                        <td class="left"><a class="button add-module-btn"><?php echo $lang_button_add_module; ?></a></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
<script>
(function($){
    var convertToFormNames = function(obj) {
        var out = {};
        if (typeof obj === 'object' || typeof obj === 'array') {
            for (var k in obj) {
                var arr = convertToFormNames(obj[k]);
                for (var k2 in arr) {
                    out['['+k+']'+k2] = arr[k2];
                }
            }
        } else {
            out[''] = obj;
        }
        return out;
    };
    var fillForm = function(form, settingsObj) {
        var settings = convertToFormNames(settingsObj);
        for (var name in settings) {
            var field = form.find('[name="iscroller_module'+name+'"]');
            if (field.size()) {
                var type = field.eq(0).attr('type') ? field.eq(0).attr('type').toLowerCase() : '';
                var tag  = field.eq(0).prop('tagName').toLowerCase();
                if (type === 'text' || tag === 'select' || tag === 'textarea') {
                    field.val(settings[name]);
                } else if (type === 'radio' || type === 'checkbox') {
                    field.filter('[value="'+settings[name]+'"]').attr('checked', 'checked');
                }
            }
        }
    };
    var addModule = function(tpl) {
        var $tr = $('<tr />'),
            $table = $('#module'),
            n = $table.find('tbody tr').size();
        $tr.html(tpl.html().split('{n}').join(n));
        $tr.find('.remove-module-btn').on('click', removeModule);
        $table.find('tbody').append($tr);
        var $mn = $('[name="iscroller_module[module_numb]"]');
        $mn.val(~~$mn.val() + 1);
    };
    var removeModule = function() {
        $(this).closest('tr').remove();
        var $mn = $('[name="iscroller_module[module_numb]"]');
        $mn.val(~~$mn.val() - 1);
    };

    var moduleTpl = $('#module tbody tr').clone(),
        moduleNumb = <?php echo "$module_numb"; ?> || 1;
    $('#module tbody tr').remove();
    $('#module .add-module-btn').on('click', function(){ addModule(moduleTpl); });
    
    for (var i = 0; i < moduleNumb; i ++) {
        addModule(moduleTpl);
    }
    
    fillForm($('#form-iscroller'), <?php echo json_encode($settings); ?>);
    
    $('#input-status').on('change', function(){
        if ($(this).val() === 'smart') {
            $('#show-btn-after-row').slideDown(300);
        } else {
            $('#show-btn-after-row').slideUp(300);
        }
    });
    if ($('#input-status').val() === 'smart') {
        $('#show-btn-after-row').css('display', 'block');
    }
    
    $("input.colorpic").ColorPickerSliders({
        size: 'sm',
        placement: 'top',
        swatches: false,
        sliders: false,
        hsvpanel: true
    });
    
    $('#tabs a').tabs();
    $('#langs a').tabs();
    $('[data-toggle=\'tooltip\']').tooltip({container: 'body', html: true});

    
    $('#button-image').on('click', function() {
        image_upload('loader-img', 'thumb-image');
    });
    function image_upload(field, thumb) {
        $('#dialog').remove();

        $('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="<?php echo str_replace('&amp;', '&', $filemanager_route); ?>&field=' + encodeURIComponent(field) 
                + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');

        $('#dialog').dialog({
            title: 'Image Manager',
            close: function (event, ui) {
                    var inp = $('#thumb-image').parent().find('input');
                    if (inp.val()) {
                        $('#thumb-image').find('img').attr('src', '<?php echo $catalog; ?>image/' + inp.val());
                    } else {
                        $('#thumb-image').find('img').attr('src', $('#thumb-image').find('img').attr('data-placeholder'));
                    }
            },	
            bgiframe: false,
            width: 800,
            height: 400,
            resizable: false,
            modal: false
        });
    };
    $('#button-clear').on('click', function() {
        $('#thumb-image').find('img').attr('src', $('#thumb-image').find('img').attr('data-placeholder'));
        $('#thumb-image').parent().find('input').attr('value', '');
    });
})(jQuery);
</script>
<style>
    input[type=radio] {
        vertical-align: middle;
        margin-bottom: 5px;
    }
    .form-group .col-sm-8 {
        padding-top: 7px;
    }
    .form-separator {
        border-top: 1px solid #eee; 
        padding-top: 15px;
    }
    .box > .content, .box > .content * {
        box-sizing: border-box;
    }
    .box > .content, .box > .content .vtabs a {
        box-sizing: content-box;
    }
    .box > .content div {
        display: block;
        position: relative;
    }
    .htabs {
        height: 31px;
    }
    #form-iscroller {
        display: inline-block;
        width: 100%;
    }
    .form-group {
        /*height: 20px;*/
        /*padding: 0;*/
        /*border-bottom: 1px dotted #ccc;*/
        margin: 0 0 15px;
    }
    .form-group:after, .form-group:before {
        display: table;
        content: ' ';
        clear: both;
        position: relative;
    }
    .form-group .control-label {
        text-align: right;
        font-weight: bold;
        padding: 8px 13px;
        line-height: 1.42857143;
    }
    .form-group select, .form-group input[type=text], .form-control {
        box-sizing: border-box;
        width: 100%;
        border: 1px solid #B9B9B9;
        border-top-color: #A0A0A0;
        box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1);
        display: block;
        height: 35px;
        padding: 8px 13px;
        font-size: 12px;
        line-height: 1.42857143;
        color: #555;
        background-color: #FFF;
        background-image: none;
        border-radius: 3px;
        transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    }
    textarea.form-control {
        height: auto;
    }
/*    .form-group .col-sm-8 {
        line-height: 1.42857143;
    }*/
    .form-group input[type=radio] {
        margin-top: 9px;
    }
    .col-sm-8, .col-sm-4 {
        float: left;
        padding: 0 15px;
        box-sizing: border-box;
    }
    .col-sm-8 {
        width: 75%;
    }
    .col-sm-4 {
        width: 25%;
    }
    .col-sm-offset-4 {
        margin-left: 25%;
    }
    .tab-content {
        display: block;
        content: ' ';
        clear: both;
    }
    #langs {
        margin-top: 20px;
    }
    /* bootstrap styles */
    .tooltip {
        position: absolute;
        z-index: 1070;
        display: block;
        font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 12px;
        font-weight: normal;
        line-height: 1.4;
        visibility: visible;
        filter: alpha(opacity=0);
        opacity: 0;
    }
    .tooltip.in {
        filter: alpha(opacity=90);
        opacity: .9;
    }
    .tooltip.top {
        padding: 5px 0;
        margin-top: -3px;
    }
    .tooltip.right {
        padding: 0 5px;
        margin-left: 3px;
    }
    .tooltip.bottom {
        padding: 5px 0;
        margin-top: 3px;
    }
    .tooltip.left {
        padding: 0 5px;
        margin-left: -3px;
    }
    .tooltip-inner {
        max-width: 200px;
        padding: 3px 8px;
        color: #fff;
        text-align: center;
        text-decoration: none;
        background-color: #000;
        border-radius: 4px;
    }
    .tooltip-arrow {
        position: absolute;
        width: 0;
        height: 0;
        border-color: transparent;
        border-style: solid;
    }
    .tooltip.top .tooltip-arrow {
        bottom: 0;
        left: 50%;
        margin-left: -5px;
        border-width: 5px 5px 0;
        border-top-color: #000;
    }
    .tooltip.top-left .tooltip-arrow {
        right: 5px;
        bottom: 0;
        margin-bottom: -5px;
        border-width: 5px 5px 0;
        border-top-color: #000;
    }
    .tooltip.top-right .tooltip-arrow {
        bottom: 0;
        left: 5px;
        margin-bottom: -5px;
        border-width: 5px 5px 0;
        border-top-color: #000;
    }
    .tooltip.right .tooltip-arrow {
        top: 50%;
        left: 0;
        margin-top: -5px;
        border-width: 5px 5px 5px 0;
        border-right-color: #000;
    }
    .tooltip.left .tooltip-arrow {
        top: 50%;
        right: 0;
        margin-top: -5px;
        border-width: 5px 0 5px 5px;
        border-left-color: #000;
    }
    .tooltip.bottom .tooltip-arrow {
        top: 0;
        left: 50%;
        margin-left: -5px;
        border-width: 0 5px 5px;
        border-bottom-color: #000;
    }
    .tooltip.bottom-left .tooltip-arrow {
        top: 0;
        right: 5px;
        margin-top: -5px;
        border-width: 0 5px 5px;
        border-bottom-color: #000;
    }
    .tooltip.bottom-right .tooltip-arrow {
        top: 0;
        left: 5px;
        margin-top: -5px;
        border-width: 0 5px 5px;
        border-bottom-color: #000;
    }
    .popover {
        position: absolute;
        top: 0;
        left: 0;
        z-index: 1060;
        display: none;
        max-width: 276px;
        padding: 1px;
        font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 14px;
        font-weight: normal;
        line-height: 1.42857143;
        text-align: left;
        white-space: normal;
        background-color: #fff;
        -webkit-background-clip: padding-box;
        background-clip: padding-box;
        border: 1px solid #ccc;
        border: 1px solid rgba(0, 0, 0, .2);
        border-radius: 6px;
        -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
        box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
    }
    .popover.top {
        margin-top: -10px;
    }
    .popover.right {
        margin-left: 10px;
    }
    .popover.bottom {
        margin-top: 10px;
    }
    .popover.left {
        margin-left: -10px;
    }
    .popover-title {
        padding: 8px 14px;
        margin: 0;
        font-size: 14px;
        background-color: #f7f7f7;
        border-bottom: 1px solid #ebebeb;
        border-radius: 5px 5px 0 0;
    }
    .popover-content {
        padding: 9px 14px;
    }
    .popover > .arrow,
    .popover > .arrow:after {
        position: absolute;
        display: block;
        width: 0;
        height: 0;
        border-color: transparent;
        border-style: solid;
    }
    .popover > .arrow {
        border-width: 11px;
    }
    .popover > .arrow:after {
        content: "";
        border-width: 10px;
    }
    .popover.top > .arrow {
        bottom: -11px;
        left: 50%;
        margin-left: -11px;
        border-top-color: #999;
        border-top-color: rgba(0, 0, 0, .25);
        border-bottom-width: 0;
    }
    .popover.top > .arrow:after {
        bottom: 1px;
        margin-left: -10px;
        content: " ";
        border-top-color: #fff;
        border-bottom-width: 0;
    }
    .popover.right > .arrow {
        top: 50%;
        left: -11px;
        margin-top: -11px;
        border-right-color: #999;
        border-right-color: rgba(0, 0, 0, .25);
        border-left-width: 0;
    }
    .popover.right > .arrow:after {
        bottom: -10px;
        left: 1px;
        content: " ";
        border-right-color: #fff;
        border-left-width: 0;
    }
    .popover.bottom > .arrow {
        top: -11px;
        left: 50%;
        margin-left: -11px;
        border-top-width: 0;
        border-bottom-color: #999;
        border-bottom-color: rgba(0, 0, 0, .25);
    }
    .popover.bottom > .arrow:after {
        top: 1px;
        margin-left: -10px;
        content: " ";
        border-top-width: 0;
        border-bottom-color: #fff;
    }
    .popover.left > .arrow {
        top: 50%;
        right: -11px;
        margin-top: -11px;
        border-right-width: 0;
        border-left-color: #999;
        border-left-color: rgba(0, 0, 0, .25);
    }
    .popover.left > .arrow:after {
        right: 1px;
        bottom: -10px;
        content: " ";
        border-right-width: 0;
        border-left-color: #fff;
    }
    .hidden {
        display: none!important;
    }
    .alert {
        padding: 10px;
        margin-bottom: 17px;
        border: 1px solid rgba(0, 0, 0, 0);
        border-radius: 3px;
        line-height: 18px;
    }
    .alert-warning {
        background-color: #FEF8F4;
        border-color: #FCE4DD;
        color: #F38733;
    }
    .alert > p, .alert > ul {
        margin-bottom: 0;
    }
    .alert > p + p {
        margin-top: 5px;
    }
    .alert-info {
        background-color: #D9EEF9;
        border-color: #BAEAF4;
        color: #115376;
    }
    .close {
        float: right;
        font-size: 18px;
        font-weight: bold;
        line-height: 1;
        color: #000;
        text-shadow: 0 1px 0 #FFF;
        opacity: 0.2;
        filter: alpha(opacity=20);
    }
    button.close {
        padding: 0;
        cursor: pointer;
        background: rgba(0, 0, 0, 0);
        border: 0;
        -webkit-appearance: none;
    }
    .close:hover, .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
        opacity: 0.5;
        filter: alpha(opacity=50);
    }
</style>
<?php echo $footer;