<?php echo $header; ?>
<div id="content">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php echo (empty($data['AbandonedCarts']['LicensedOn'])) ? base64_decode('ICAgIDxkaXYgY2xhc3M9ImFsZXJ0IGFsZXJ0LWRhbmdlciBmYWRlIGluIj4NCiAgICAgICAgPGJ1dHRvbiB0eXBlPSJidXR0b24iIGNsYXNzPSJjbG9zZSIgZGF0YS1kaXNtaXNzPSJhbGVydCIgYXJpYS1oaWRkZW49InRydWUiPsOXPC9idXR0b24+DQogICAgICAgIDxoND5XYXJuaW5nISBVbmxpY2Vuc2VkIHZlcnNpb24gb2YgdGhlIG1vZHVsZSE8L2g0Pg0KICAgICAgICA8cD5Zb3UgYXJlIHJ1bm5pbmcgYW4gdW5saWNlbnNlZCB2ZXJzaW9uIG9mIHRoaXMgbW9kdWxlISBZb3UgbmVlZCB0byBlbnRlciB5b3VyIGxpY2Vuc2UgY29kZSB0byBlbnN1cmUgcHJvcGVyIGZ1bmN0aW9uaW5nLCBhY2Nlc3MgdG8gc3VwcG9ydCBhbmQgdXBkYXRlcy48L3A+PGRpdiBzdHlsZT0iaGVpZ2h0OjVweDsiPjwvZGl2Pg0KICAgICAgICA8YSBjbGFzcz0iYnRuIGJ0bi1kYW5nZXIiIGhyZWY9ImphdmFzY3JpcHQ6dm9pZCgwKSIgb25jbGljaz0iJCgnYVtocmVmPSNpc2Vuc2Vfc3VwcG9ydF0nKS50cmlnZ2VyKCdjbGljaycpIj5FbnRlciB5b3VyIGxpY2Vuc2UgY29kZTwvYT4NCiAgICA8L2Rpdj4=') : '' ?>
    <?php if ($error_warning) { ?><div class="alert alert-danger" > <i class="fa fa-exclamation-triangle"></i>&nbsp;<?php echo $error_warning; ?></div><?php } ?>
    <?php if (!empty($this->session->data['success'])) { ?>
        <div class="alert alert-success autoSlideUp"> <i class="fa fa-info"></i>&nbsp;<?php echo $this->session->data['success']; ?> </div>
        <script>$('.autoSlideUp').delay(3000).fadeOut(600, function(){ $(this).show().css({'visibility':'hidden'}); }).slideUp(600);</script>
    <?php $this->session->data['success'] = null; } ?>
    <div id="messageResult" style="display:none;"><div class="alert alert-success autoSlideUp2"><i class="fa fa-info"></i> The message was sent successfully!</div></div>
    <!--UPDATE INFO-->
	<?php if(!isset($data['AbandonedCarts']['Updated'])) { ?>
   		<div class="alert alert-info"><i class="fa fa-info"></i> This version of AbandonedCarts require changes in the database. <a onclick="upgrade();">Click here to update the database</a>.</div>
    <?php } ?>
	<!--UPDATE INFO-->
  <div class="panel panel-default">
        <div class="panel-heading">
            <div class="storeSwitcherWidget">
            	<div class="form-group">
                	<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-pushpin"></span>&nbsp;<?php echo $store['name']; if($store['store_id'] == 0) echo " <strong>(".$text_default.")</strong>"; ?>&nbsp;<span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button>
                	<ul class="dropdown-menu" role="menu">
                    	<?php foreach ($stores  as $st) { ?>
                    		<li><a href="index.php?route=module/abandonedcarts&store_id=<?php echo $st['store_id'];?>&token=<?php echo $this->session->data['token']; ?>"><?php echo $st['name']; ?></a></li>
                    	<?php } ?> 
                	</ul>
            	</div>
            </div>
            <h3 class="panel-title"><i class="fa fa-shopping-cart"></i>&nbsp;<span style="vertical-align:middle;font-weight:bold;"><?php echo $heading_title; ?></span></h3>
        </div>
        <div class="panel-body">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                <input type="hidden" name="store_id" value="<?php echo $store['store_id']; ?>">
                <?php if (isset($data['AbandonedCarts']['Updated'])) { ?>
                <input type="hidden" name="AbandonedCarts[Updated]" value="<?php echo $data['AbandonedCarts']['Updated']; ?>">
                <?php } ?> 
                <div class="tabbable">
                    <div class="tab-navigation form-inline">
                          <ul class="nav nav-tabs mainMenuTabs">
                            <li><a href="#controlpanel" data-toggle="tab"><i class="fa fa-power-off"></i>&nbsp;Control Panel</a></li>
                            <li><a href="#abandonedCarts" data-toggle="tab" class="active"><i class="fa fa-list-alt"></i>&nbsp;Abandoned Carts</a></li>
                            <li><a href="#mail" data-toggle="tab"><i class="fa fa-envelope-o"></i>&nbsp;Mail Template</a></li>
                            <li><a href="#analytics" data-toggle="tab"><i class="fa fa-bar-chart-o"></i>&nbsp;Statistics</a></li>
                            <li class="dropdown">
                              <a href="#"  data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-gift"></i>&nbsp;Coupons<b class="caret"></b></a>
                              <ul class="dropdown-menu">
                                <li><a href="#givenCoupons" data-toggle="tab"/><i class="fa fa-tags"></i>&nbsp;Given Coupons</a></li>
                                <li><a href="#usedCoupons" data-toggle="tab"/><i class="fa fa-check-square-o"></i>&nbsp;Used Coupons</a></li>
                              </ul>
                            </li>
                            <li><a href="#isense_support" data-toggle="tab"><i class="fa fa-external-link"></i>&nbsp;Support</a></li>        
                          </ul>
                        <div class="tab-buttons">
                            <button type="submit" class="btn btn-success save-changes"><i class="fa fa-check"></i>&nbsp;Save Changes</button>
                            <a onclick="location = '<?php echo $cancel; ?>'" class="btn btn-warning"><i class="fa fa-times"></i>&nbsp;<?php echo $button_cancel?></a>
                        </div> 
                    </div><!-- /.tab-navigation --> 
                  <div class="tab-content">
                    <div id="abandonedCarts" class="tab-pane active">
                      <?php require_once(DIR_APPLICATION.'view/template/module/abandonedcarts/tab_abandonedcarts.php'); ?>                        
                    </div>
                    <div id="mail" class="tab-pane">
                      <?php require_once(DIR_APPLICATION.'view/template/module/abandonedcarts/tab_mail.php'); ?>                        
                    </div>  
                    <div id="analytics" class="tab-pane">
                      <?php require_once(DIR_APPLICATION.'view/template/module/abandonedcarts/tab_analytics.php'); ?>                        
                    </div> 
                    <div id="controlpanel" class="tab-pane">
                      <?php require_once(DIR_APPLICATION.'view/template/module/abandonedcarts/tab_panel.php'); ?>                        
                    </div>     
                    <div id="givenCoupons" class="tab-pane"></div>
                    <div id="usedCoupons" class="tab-pane"></div>           
                    <div id="isense_support" class="tab-pane">
                      <?php require_once(DIR_APPLICATION.'view/template/module/abandonedcarts/tab_support.php'); ?>                        
                    </div>
                  </div><!-- /.tab-content -->
                </div><!-- /.tabbable -->
            </form>
        </div> 
    </div>
 </div>
<script>
$(function() {
	$('#mainTabs a:first').tab('show'); // Select first tab
	if (window.localStorage && window.localStorage['currentTab']) {
		$('.mainMenuTabs a[href="'+window.localStorage['currentTab']+'"]').tab('show');
	}
	if (window.localStorage && window.localStorage['currentSubTab']) {
		$('a[href="'+window.localStorage['currentSubTab']+'"]').tab('show');
	}
	$('.fadeInOnLoad').css('visibility','visible');
	$('.mainMenuTabs a[data-toggle="tab"]').click(function() {
		if (window.localStorage) {
			window.localStorage['currentTab'] = $(this).attr('href');
		}
	});
	$('a[data-toggle="tab"]:not(.mainMenuTabs a[data-toggle="tab"], .followup_tabs a[data-toggle="tab"])').click(function() {
		if (window.localStorage) {
			window.localStorage['currentSubTab'] = $(this).attr('href');
		}
	});
});
</script>
<script>
$(document).ready(refreshData());
  function refreshData(){
      $.ajax({
         url: "index.php?route=module/abandonedcarts/givenCoupons&token=<?php echo $this->session->data['token']; ?>",
         type: 'get',
         dataType: 'html',
         success: function(data) { 
          $('#givenCoupons').html(data);
         }
      });
      $.ajax({
         url: "index.php?route=module/abandonedcarts/usedCoupons&token=<?php echo $this->session->data['token']; ?>",
         type: 'get',
         dataType: 'html',
         success: function(data) { 
          $('#usedCoupons').html(data);
         }
      });
    }
</script> 
<script>
function upgrade() {
	$.ajax({
		url: "index.php?route=module/abandonedcarts/upgrade&token=<?php echo $this->session->data['token']; ?>",
		type: 'get',
		dataType: 'json',
		success: function(json) {				
			if (json) {
				alert("The database was updated successfully!");
				location.reload();
			} else {
				alert("There was an unexpected error!");
			}
		}
	});
}
</script> 
<?php echo $footer; ?>