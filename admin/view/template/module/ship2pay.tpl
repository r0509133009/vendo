<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
    
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
      <table class="list">
      <thead>
      <tr>
      <td class="left" style="width: 50%"><?php echo $entry_shipping_method; ?></td>
      <td class="left" style="width: 50%"><?php echo $entry_allowed_payment; ?></td>
      </tr>
      </thead>
      <tbody>
          <?php if ($shipping_methods) {
          $allowed_payments = array();
          foreach ($shipping_methods as $shipping_method) {
          if ($shipping_method['status']) { ?>
          <tr>
            <td class="left"><?php echo $shipping_method['name']; ?></td>
            <td class="left"><?php if ($extensions) {
                               foreach ($extensions as $extension) {
                               if ($extension['status']) {
                               if (array_key_exists($shipping_method['code'], $modules)) {
                               $allowed_payments = explode ( ";", $modules[$shipping_method['code']]);
                               }?>
            <input type="checkbox" name="<?php echo $shipping_method['code'];?>[]" <?php if (in_array($extension['code'],$allowed_payments)) { echo 'checked'; } ?> value="<?php echo $extension['code']; ?>" /><?php echo $extension['name']; ?>
            <input type="hidden" name="<?php echo $shipping_method['code'];?>[]" value=""><br />
          <?php }
                } ?>
          <?php } else { ?>
          <?php echo $text_no_results; ?>
          <?php } ?>
      </td>
          </tr>
          <?php }
                } ?>
          <?php } else { ?>
         <?php echo $text_no_results; ?>
         <?php } ?>
      </tbody>
      </table>
    </form>
    </div>
  </div>
</div>
<?php echo $footer; ?>
