<?php echo $header; ?>
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-subscribers" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  
    <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } 
	
	 if (isset($error_email_name) && !empty($error_email_name)) { 
       echo '<div class="alert alert-danger">'.$error_email_name.'</div>';
	 } 
	 
     if (isset($error_email_exist) && !empty($error_email_exist)) {
		echo '<div class="alert alert-danger">'.$error_email_exist.'</div>';
	 } 
	 ?>
			
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit=''; ?></h3>
      </div>
      <div class="panel-body">
       
	   <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-subscribers" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-1 control-label" for="email_name"><?php echo $entry_first_name; ?></label>
            <div class="col-sm-3">
             <input type="text" name="email_name" value="<?php echo isset($email_name)?$email_name:""; ?>" class="form-control"/>
            </div>
          </div>
			
		<div class="form-group">
            <label class="col-sm-1 control-label" for="last_name"><?php echo $entry_last_name; ?></label>
            <div class="col-sm-3">
             <input type="text" name="last_name" value="<?php echo isset($last_name)?$last_name:""; ?>" class="form-control"/>
            </div>
          </div>
		  
		  <div class="form-group">
			<label class="col-sm-1 control-label" for="input-status"><?php echo $entry_code; ?></label>
            <div class="col-sm-3">
              <input type="text" name="email_id" id="email_id" value="<?php echo isset($email_id)?$email_id:''; ?>"  class="form-control" >
            </div>
		 </div>
		 	
        </form>
		
		
      </div>
    </div>
  </div>
  
  
 
<?php echo $footer; ?>

