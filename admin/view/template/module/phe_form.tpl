<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
	  <div id="tabs" class="htabs"><a href="#tab-general"><?php echo $tab_general; ?></a><a href="#tab-style"><?php echo $tab_style; ?></a><a href="#tab-preview" id="button-preview"><?php echo $tab_preview; ?></a></div>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <div id="tab-general">
		  <table class="form">
		    <tr>
			  <td><?php echo $entry_name; ?></td>
			  <td><input type="text" name="name" value="<?php echo $name; ?>" /></td>
			</tr>
			<tr>
			  <td><?php echo $entry_type; ?></td>
			  <td><select name="type">
					<?php if ($type == 'register') { ?>
					<option value="register" selected="selected"><?php echo $text_register; ?></option>
					<?php } else { ?>
					<option value="register"><?php echo $text_register; ?></option>
					<?php } ?>
					<?php if ($type == 'affiliate') { ?>
					<option value="affiliate" selected="selected"><?php echo $text_affiliate; ?></option>
					<?php } else { ?>
					<option value="affiliate"><?php echo $text_affiliate; ?></option>
					<?php } ?>
					<?php if ($type == 'order') { ?>
					<option value="order" selected="selected"><?php echo $text_order; ?></option>
					<?php } else { ?>
					<option value="order"><?php echo $text_order; ?></option>
					<?php } ?>
					<?php if ($type == 'contact') { ?>
					<option value="contact" selected="selected"><?php echo $text_contact; ?></option>
					<?php } else { ?>
					<option value="contact"><?php echo $text_contact; ?></option>
					<?php } ?>
					<?php if ($type == 'forgotten') { ?>
					<option value="forgotten" selected="selected"><?php echo $text_forgotten; ?></option>
					<?php } else { ?>
					<option value="forgotten"><?php echo $text_forgotten; ?></option>
					<?php } ?>
					<?php if ($type == 'reward') { ?>
					<option value="reward" selected="selected"><?php echo $text_reward; ?></option>
					<?php } else { ?>
					<option value="reward"><?php echo $text_reward; ?></option>
					<?php } ?>
					<?php if ($type == 'account_approve') { ?>
					<option value="account_approve" selected="selected"><?php echo $text_account_approve; ?></option>
					<?php } else { ?>
					<option value="account_approve"><?php echo $text_account_approve; ?></option>
					<?php } ?>
					<?php if ($type == 'account_transaction') { ?>
					<option value="account_transaction" selected="selected"><?php echo $text_account_transaction; ?></option>
					<?php } else { ?>
					<option value="account_transaction"><?php echo $text_account_transaction; ?></option>
					<?php } ?>
					<?php if ($type == 'affiliate_approve') { ?>
					<option value="affiliate_approve" selected="selected"><?php echo $text_affiliate_approve; ?></option>
					<?php } else { ?>
					<option value="affiliate_approve"><?php echo $text_affiliate_approve; ?></option>
					<?php } ?>
					<?php if ($type == 'affiliate_transaction') { ?>
					<option value="affiliate_transaction" selected="selected"><?php echo $text_affiliate_transaction; ?></option>
					<?php } else { ?>
					<option value="affiliate_transaction"><?php echo $text_affiliate_transaction; ?></option>
					<?php } ?>
					<?php if ($type == 'gift_voucher') { ?>
					<option value="gift_voucher" selected="selected"><?php echo $text_gift_voucher; ?></option>
					<?php } else { ?>
					<option value="gift_voucher"><?php echo $text_gift_voucher; ?></option>
					<?php } ?>
					<?php foreach ($return_statuses as $return_status) { ?>
					<?php if ('return_' . $return_status['return_status_id'] == $type) { ?>
					  <option value="return_<?php echo $return_status['return_status_id']; ?>" selected="selected"><?php echo $text_return; ?> <?php echo $return_status['name']; ?></option>
					<?php } else { ?>
					  <option value="return_<?php echo $return_status['return_status_id']; ?>"><?php echo $text_return; ?> <?php echo $return_status['name']; ?></option>
					<?php } ?>
					<?php } ?>
					<?php foreach ($order_statuses as $order_status) { ?>
					<?php if ($order_status['order_status_id'] == $type) { ?>
					  <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $text_status; ?> <?php echo $order_status['name']; ?></option>
					<?php } else { ?>
					  <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $text_status; ?> <?php echo $order_status['name']; ?></option>
					<?php } ?>
					<?php } ?>
				  </select></td>
			</tr>
			<tr>
			  <td><?php echo $entry_priority; ?></td>
			  <td><input type="text" name="priority" value="<?php echo $priority; ?>" /></td>
			</tr>
			<tr>
			  <td><?php echo $entry_date_start; ?></td>
			  <td><input type="text" name="date_start" value="<?php echo $date_start; ?>" class="date" /></td>
			</tr>
			<tr>
			  <td><?php echo $entry_date_end; ?></td>
			  <td><input type="text" name="date_end" value="<?php echo $date_end; ?>" class="date" /></td>
			</tr>
			<tr>
			  <td><?php echo $entry_store; ?></td>
			  <td><select name="store_id">
					<?php if (!$store_id) { ?>
					<option value="0" selected="selected"><?php echo $text_default; ?></option>
					<?php } else { ?>
					<option value="0"><?php echo $text_default; ?></option>
					<?php } ?>
					<?php foreach ($stores as $store) { ?>
					  <?php if ($store_id == $store['store_id']) { ?>
					  <option value="<?php echo $store['store_id']; ?>" selected="selected"><?php echo $store['name']; ?></option>
					  <?php } else { ?>
					  <option value="<?php echo $store['store_id']; ?>"><?php echo $store['name']; ?></option>
					 <?php } ?>
					<?php } ?></select></td>
			</tr>
		  </table>
		  <div id="languages" class="htabs">
            <?php foreach ($languages as $language) { ?>
            <a href="#language<?php echo $language['language_id']; ?>"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a>
            <?php } ?>
          </div>
		  <?php foreach ($languages as $language) { ?>
          <div id="language<?php echo $language['language_id']; ?>">
		    <table class="form">
			  <tr>
			    <td><?php echo $entry_subject; ?></td>
				<td><input type="text" name="description[<?php echo $language['language_id']; ?>][subject]" value="<?php echo isset($description[$language['language_id']]['subject']) ? $description[$language['language_id']]['subject'] : ''; ?>" /></td>
			  </tr>
			  <tr>
			    <td><?php echo $entry_message; ?></td>
				<td><textarea name="description[<?php echo $language['language_id']; ?>][message]" id="message<?php echo $language['language_id']; ?>"><?php echo isset($description[$language['language_id']]['message']) ? $description[$language['language_id']]['message'] : ''; ?></textarea></td>
			  </tr>
			</table>
		  </div>
		  <?php } ?>
		  <table class="form">
		    <tr>
			  <td><?php echo $text_code; ?></td>
			  <td><div id="code"></div></td>
		    </tr>
		  </table>
		</div>
		<div id="tab-style">
		  <table class="form">
		    <tr>
			  <td><?php echo $entry_background; ?></td>
			  <td><input type="text" name="background" value="<?php echo $background; ?>" /></td>
			</tr>
			<tr>
			  <td><?php echo $entry_heading; ?></td>
			  <td><input type="text" name="heading" value="<?php echo $heading; ?>" /></td>
			</tr>
			<tr>
			  <td><?php echo $entry_body; ?></td>
			  <td><input type="text" name="body" value="<?php echo $body; ?>" /></td>
			</tr>
			<tr>
			  <td><?php echo $entry_image; ?></td>
			  <td><a onclick="image_upload('image', 'logo');"><?php echo $text_select_image; ?></a><input type="hidden" name="image" value="<?php echo $image; ?>" id="image" /></td>
			</tr>
		  </table>
		</div>
		<div id="tab-preview" style="text-align:center;">
		  <table style="width:700px;margin-left:auto;margin-right:auto;text-align:left;border-collapse:collapse;">
			<tr>
			  <td id="t-header" style="text-align:center;verticle-align:middle;height:100px;background:<?php echo $background; ?>">
				  <img src="<?php echo $logo; ?>" alt="<?php echo $store_name; ?>" title="<?php echo $store_name; ?>" id="logo" />
			  </td>
			</tr>
			<tr>
			  <td id="t-body" style="padding:20px;background:<?php echo $body; ?>">
				  <div id="t-heading" style="font-size:16px;font-weight:bold;border-bottom:1px solid #aaaaaa;color:<?php echo $heading; ?>"><?php echo $t_heading; ?></div>
				  <?php echo $t_message; ?>
				</div>
			  </td>
			</tr>
			<tr>
			  <td style="background:#aaaaaa;padding:0px;height:1px;display:block;"></td>
			</tr>
			<tr>
			  <td style="background:#b0b0b0;padding:0px;height:1px;display:block;"></td>
			</tr>
			<tr>
			  <td style="background:#c1c1c1;padding:0px;height:1px;display:block;"></td>
			</tr>
			<tr>
			  <td style="background:#d2d2d2;padding:0px;height:1px;display:block;"></td>
			</tr>
			<tr>
			  <td style="background:#d9d9d9;padding:0px;height:1px;display:block;"></td>
			</tr>
			<tr>
			  <td style="background:#e8e8e8;padding:0px;height:1px;display:block;"></td>
			</tr>
			<tr>
			  <td style="background:#efefef;padding:0px;height:1px;display:block;"></td>
			</tr>
			<tr>
			  <td style="background:#f7f7f7;padding:0px;height:1px;display:block;"></td>
			</tr>
			<tr>
			  <td style="background:#fefefe;padding:0px;height:1px;display:block;"></td>
			</tr>
			<tr>
			  <td style="background:#ffffff;padding:0px;height:1px;display:block;"></td>
			</tr>
		  </table>
		</div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script> 
<script type="text/javascript"><!--
<?php foreach ($languages as $language) { ?>
CKEDITOR.replace('message<?php echo $language['language_id']; ?>', {
	filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
});
<?php } ?>
//--></script> 
<script type="text/javascript"><!--
$('select[name=\'type\']').on('change', function() {
	var html = '';
	
	var value = $('select[name=\'type\']').attr('value');
	
	if (value == 'register') {
		html += '<table class="form">';
		html += '  <tr>';
		html += '    <td>First Name - {firstname}</td>';
		html += '    <td>Last Name - {lastname}</td>';
		html += '    <td>Email - {email}</td>';
		html += '    <td>Telephone - {telephone}</td>';
		html += '    <td>Password - {password}</td>';
		html += '  </tr>';
		html += '</table>';
	} else if (value == 'affiliate') {
		html += '<table class="form">';
		html += '  <tr>';
		html += '    <td>First Name - {firstname}</td>';
		html += '    <td>Last Name - {lastname}</td>';
		html += '    <td>Email - {email}</td>';
		html += '    <td>Telephone - {telephone}</td>';
		html += '    <td>Password - {password}</td>';
		html += '  </tr>';
		html += '</table>';
	} else if (value == 'order') {
		html += '<table class="form">';
		html += '  <tr>';
		html += '    <td>Order ID - {order_id}</td>';
		html += '    <td>First Name - {firstname}</td>';
		html += '    <td>Last Name - {lastname}</td>';
		html += '    <td>Email - {email}</td>';
		html += '    <td>Telephone - {telephone}</td>';
		html += '  </tr>';
		html += '  <tr>';
		html += '    <td>Date Added - {date_added}</td>';
		html += '    <td>Payment Method - {payment_method}</td>';
		html += '    <td>Shipping Method - {shipping_method}</td>';
		html += '    <td>IP Address - {ip}</td>';
		html += '    <td>Comment Table - {comment_table}</td>';
		html += '  </tr>';
		html += '  <tr>';
		html += '    <td>Payment Address - {payment_address}</td>';
		html += '    <td>Shipping Address - {shipping_address}</td>';
		html += '    <td>Purchased Products - {product_table}</td>';
		html += '  </tr>';
		html += '</table>';
	} else if (value == 'contact') {
		html += '<table class="form">';
		html += '  <tr>';
		html += '    <td>First Name - {firstname}</td>';
		html += '    <td>Email - {email}</td>';
		html += '    <td>Enquiry - {enquiry}</td>';
		html += '  </tr>';
		html += '</table>';
	} else if (value == 'forgotten') {
		html += '<table class="form">';
		html += '  <tr>';
		html += '    <td>First Name - {firstname}</td>';
		html += '    <td>Last Name - {lastname}</td>';
		html += '    <td>Email - {email}</td>';
		html += '    <td>Password - {password}</td>';
		html += '  </tr>';
		html += '</table>';
	} else if (value == 'reward') {
		html += '<table class="form">';
		html += '  <tr>';
		html += '    <td>First Name - {firstname}</td>';
		html += '    <td>Last Name - {lastname}</td>';
		html += '    <td>Email - {email}</td>';
		html += '    <td>Points - {points}</td>';
		html += '    <td>Total Points - {total_points}</td>';
		html += '  </tr>';
		html += '</table>';
	} else if (value == 'account_approve') {
		html += '<table class="form">';
		html += '  <tr>';
		html += '    <td>First Name - {firstname}</td>';
		html += '    <td>Last Name - {lastname}</td>';
		html += '    <td>Email - {email}</td>';
		html += '  </tr>';
		html += '</table>';
	} else if (value == 'account_transaction') {
		html += '<table class="form">';
		html += '  <tr>';
		html += '    <td>First Name - {firstname}</td>';
		html += '    <td>Last Name - {lastname}</td>';
		html += '    <td>Email - {email}</td>';
		html += '    <td>Credits - {credits}</td>';
		html += '    <td>Total Credits - {total_credits}</td>';
		html += '  </tr>';
		html += '</table>';
	} else if (value == 'affiliate_approve') {
		html += '<table class="form">';
		html += '  <tr>';
		html += '    <td>First Name - {firstname}</td>';
		html += '    <td>Last Name - {lastname}</td>';
		html += '    <td>Email - {email}</td>';
		html += '  </tr>';
		html += '</table>';
	} else if (value == 'affiliate_transaction') {
		html += '<table class="form">';
		html += '  <tr>';
		html += '    <td>First Name - {firstname}</td>';
		html += '    <td>Last Name - {lastname}</td>';
		html += '    <td>Email - {email}</td>';
		html += '    <td>Commission - {commission}</td>';
		html += '    <td>Total Commission - {total_commission}</td>';
		html += '  </tr>';
		html += '</table>';
	} else if (value == 'gift_voucher') {
		html += '<table class="form">';
		html += '  <tr>';
		html += '    <td>To Name - {to_name}</td>';
		html += '    <td>From Name - {from_name}</td>';
		html += '    <td>To Email - {to_email}</td>';
		html += '    <td>Amount - {amount}</td>';
		html += '    <td>Code - {code}</td>';
		html += '  </tr>';
		html += '  <tr>';
		html += '    <td colspan="2">Voucher Theme Image - {voucher_theme}</td>';
		html += '    <td>Message - {message}</td>';
		html += '  </tr>';
		html += '</table>';
	} else {
		html += '<table class="form">';
		html += '  <tr>';
		html += '    <td>Order ID - {order_id}</td>';
		html += '    <td>First Name - {firstname}</td>';
		html += '    <td>Last Name - {lastname}</td>';
		html += '    <td>Email - {email}</td>';
		html += '    <td>Telephone - {telephone}</td>';
		html += '  <tr>';
		html += '    <td>Comment - {comment}</td>';
		html += '    <td colspan="2">Return ID - {return_id} (Product Return Status Only)</td>';
		html += '  </tr>';
		html += '</table>';
	}

	$('#code').html(html);
});

$('select[name=\'type\']').trigger('change');
//--></script> 
<script type="text/javascript"><!--
$('.date').datepicker({dateFormat: 'yy-mm-dd'});

$('#tabs a').tabs(); 
$('#languages a').tabs();

$('input[name=\'background\']').ColorPicker({
	onSubmit: function(hsb, hex, rgb, el) {
		$(el).val('#' + hex);
		$(el).ColorPickerHide();
		$('#t-header').css('background', '#' + hex);
	},
	onBeforeShow: function () {
		$(this).ColorPickerSetColor(this.value);
	},
	onChange: function (hsb, hex, rgb) {
		$('input[name=\'background\']').val('#' + hex);
		$('#t-header').css('background', '#' + hex);
	}
})
.bind('keyup', function(){
	$(this).ColorPickerSetColor(this.value);
});

$('input[name=\'heading\']').ColorPicker({
	onSubmit: function(hsb, hex, rgb, el) {
		$(el).val('#' + hex);
		$(el).ColorPickerHide();
		$('#t-heading').css('color', '#' + hex);
	},
	onBeforeShow: function () {
		$(this).ColorPickerSetColor(this.value);
	},
	onChange: function (hsb, hex, rgb) {
		$('input[name=\'heading\']').val('#' + hex);
		$('#t-heading').css('color', '#' + hex);
	}
})
.bind('keyup', function(){
	$(this).ColorPickerSetColor(this.value);
});

$('input[name=\'body\']').ColorPicker({
	onSubmit: function(hsb, hex, rgb, el) {
		$(el).val('#' + hex);
		$(el).ColorPickerHide();
		$('#t-body').css('background', '#' + hex);
	},
	onBeforeShow: function () {
		$(this).ColorPickerSetColor(this.value);
	},
	onChange: function (hsb, hex, rgb) {
		$('input[name=\'body\']').val('#' + hex);
		$('#t-body').css('background', '#' + hex);
	}
})
.bind('keyup', function(){
	$(this).ColorPickerSetColor(this.value);
});
//--></script> 
<script type="text/javascript"><!--
function image_upload(field, thumb) {
	$('#dialog').remove();
	
	$('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');
	
	$('#dialog').dialog({
		title: '<?php echo $text_image_manager; ?>',
		close: function (event, ui) {
			if ($('#' + field).attr('value')) {
				$.ajax({
					url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).attr('value')),
					dataType: 'text',
					success: function(text) {
						text = '<?php echo $url; ?>' + $('#' + field).val();
						$('#' + thumb).attr('src', text);
					}
				});
			}
		},	
		bgiframe: false,
		width: 800,
		height: 400,
		resizable: false,
		modal: false
	});
};
//--></script> 
<?php echo $footer; ?>