<?php echo $header; ?>
<style>
	.onoffswitch {
        position: relative;
		width: 75px;
		background-color: white;
		background-image: linear-gradient(to bottom, #eeeeee, white 25px);
		border-radius: 18px;
		box-shadow: 0 -1px white inset, 0 1px 1px rgba(0, 0, 0, 0.05) inset;
		padding: 3px;
        -webkit-user-select:none;
		-moz-user-select:none;
		-ms-user-select: none;
    }
    .onoffswitch-checkbox {
        display: none;
    }
    .onoffswitch-label {
        display: block;
		overflow: hidden;
		cursor: pointer;
		border-radius: 15px;
		margin-bottom:1px;
    }
    .onoffswitch-inner {
        display: block;
		width: 200%;
		margin-left: -100%;
        transition: background 0.2s ease-in 0s;
    }
    .onoffswitch-inner:before, .onoffswitch-inner:after {
        display: block;
		float: left;
		width: 50%;
		height: 29px;
		padding: 0;
		line-height: 29px;
        font-size: 14px;
		color: white;
		font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
        box-sizing: border-box;
    }
    .onoffswitch-inner:before {
        content: "ON";
        padding-left: 13px;
		color: #ffffff;
		background-color: #47a8d8;
		box-shadow: 0 1px 2px rgba(0, 0, 0, 0.15) inset, 0 0 3px rgba(0, 0, 0, 0.2) inset;
		text-shadow: 0 1px rgba(0, 0, 0, 0.2);
		border-radius: 18px;
		font-size:13px;
    }
    .onoffswitch-inner:after {
        content: "OFF";
        padding-right: 9px;
		box-shadow: 0 1px 2px rgba(0, 0, 0, 0.15) inset, 0 0 3px rgba(0, 0, 0, 0.2) inset;
		text-shadow: 0 1px rgba(255, 255, 255, 0.5);
		color: #aaa;
		background:#eceeef none repeat scroll 0 0;
        text-align: right;
		border-radius: 18px;
		font-size:13px;
    }
    .onoffswitch-switch {
        display: block;
		width: 17px;
		height:17px;
        box-shadow:-1px 1px 5px rgba(0, 0, 0, 0.2), 0 1px rgba(0, 0, 0, 0.02) inset;
        background: #f9f9f9 linear-gradient(to bottom, #eeeeee, white) repeat scroll 0 0;
        position: absolute;
		top: 5px;
		bottom: 4px;
        right: 51px;
        border: 4px solid #FFFFFF;
		border-radius: 15px;
        transition: all 0.2s ease-in 0s; 
    }
    .onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
        margin-left: 0;
    }
    .onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
        right: 5px; 
		box-shadow:-1px 1px 5px rgba(0, 0, 0, 0.2), 0 1px rgba(0, 0, 0, 0.02) inset;
        background: #f9f9f9 linear-gradient(to bottom, #eeeeee, white) repeat scroll 0 0;
		
    }
	
	input.form-control-fastorder {
	  background-color: #fff;
	  background-image: none;
	  border: 1px solid #ccc;
	  border-radius: 3px;
	  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
	  color: #555;
	  display: inline-block;
	  font-size: 12px;
	  height: 25px;
	  line-height: 1.42857;
	  padding: 3px 7px !important;
	  transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
	  width: 25%;
	  margin: 4px 0px;
}
	select.form-control-fastorder {
	  background-color: #fff;
	  background-image: none;
	  border: 1px solid #ccc;
	  border-radius: 3px;
	  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
	  color: #555;
	  display: inline-block;
	  font-size: 12px;
	  height: 35px;
	  line-height: 1.42857;
	  padding: 3px 7px !important;
	  transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
	  width: 25%;
	  margin: 4px 0px;
}
textarea.form-control-fastorder {
	  background-color: #fff;
	  background-image: none;
	  border: 1px solid #ccc;
	  border-radius: 3px;
	  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
	  color: #555;
	  display: inline-block;
	  font-size: 12px;
	  line-height: 1.42857;
	  padding: 3px 7px !important;
	  transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
	  width: 25%;
	  margin: 4px 0px;
}
	.form-control-fastorder:focus,.form-control-fastorder:hover {
		border-color: #66afe9;
		box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset, 0 0 8px rgba(102, 175, 233, 0.6);
		outline: 0 none;
	}
	table.form > tbody > tr > td:first-child {
		width: 300px;
	}
	.form-control-fastorder.jpicker-fastorder {
		display: inline-block;
		float: left;
		margin-right: 5px;
		width: 25%;
	}
	.title-fields {
		font-size:16px;
		text-align:right;
		color:orange !important;
	}
	.fo-label {
		text-align:right;
		font-size:12px;
		font-weight:bold;
		color:#666666 !important;
	}
	.quick-order table tr {
		border-bottom:1px solid #ddd;
	}
	</style>
	
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <div class="box">
    <div class="heading">
     <h1> <?php echo $heading_title_setting; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a></div>
    </div>
    <div class="content quick-order">
	<div id="tabs" class="htabs">
		<a href="#tab-form-fields"><?php echo $tab_fields_setting; ?></a>
		<a href="#tab-general-setting"><?php echo $tab_general_setting; ?></a>
		<a href="#tab-designed"><?php echo $tab_design_setting; ?></a>		
		<a href="#tab-sms-setting"><?php echo $tab_sms_setting; ?></a>
		<a href="#tab-email-setting"><?php echo $tab_email_setting; ?></a>
	</div>
	<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
		
		<div id="tab-form-fields">
		<?php 
		if(!isset($config_on_off_fields_firstname)) $config_on_off_fields_firstname = '1';
		if(!isset($config_fields_firstname_requared)) $config_fields_firstname_requared = '1';
		if(!isset($config_on_off_fields_phone)) $config_on_off_fields_phone = '1';
		if(!isset($config_fields_phone_requared)) $config_fields_phone_requared = '1';
		
		?>
			<table class="form">
			<!--ПОЛЕ FIRST-->
				<tr>
					<td class="title-fields">
						<?php echo $text_on_off_fields_firstname;?>
					</td>
				</tr>
				<tr>
					<td class="fo-label"><?php echo $text_status_fields;?></td>
					<td>
					<div class="onoffswitch">
						<input value="1" type="checkbox" name="config_on_off_fields_firstname" class="onoffswitch-checkbox" id="config_on_off_fields_firstname" <?php echo $config_on_off_fields_firstname == '1' ? 'checked' : '' ; ?>>
						<label class="onoffswitch-label" for="config_on_off_fields_firstname">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
					</td>
				</tr>
				<tr>
					<td class="fo-label"><?php echo $text_requared_fields?></td>
					<td>
					<div class="onoffswitch">
						<input value="1" type="checkbox" name="config_fields_firstname_requared" class="onoffswitch-checkbox" id="config_fields_firstname_requared" <?php echo $config_fields_firstname_requared == '1' ? 'checked' : '' ; ?>>
						<label class="onoffswitch-label" for="config_fields_firstname_requared">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
					</td>
				</tr>
				<tr>
					<td class="fo-label"><?php echo $text_placeholder_fields; ?></td>
					<td><?php foreach ($languages as $language) { ?>
						<input class="form-control-fastorder" type="text" name="config_placeholder_fields_firstname[<?php echo $language['language_id']; ?>][config_placeholder_fields_firstname]" value="<?php echo isset($config_placeholder_fields_firstname[$language['language_id']]) ? $config_placeholder_fields_firstname[$language['language_id']]['config_placeholder_fields_firstname'] : ''; ?>"size="50" />
						<img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br>
				<?php } ?></td>
				</tr>
				<!--ПОЛЕ PHONE-->
				<tr>
					<td class="title-fields">
						<?php echo $text_on_off_fields_phone;?>
					</td>
				</tr>
				<tr>
					<td class="fo-label"><?php echo $text_status_fields;?></td>
					<td>
					<div class="onoffswitch">
						<input value="1" type="checkbox" name="config_on_off_fields_phone" class="onoffswitch-checkbox" id="config_on_off_fields_phone" <?php echo $config_on_off_fields_phone == '1' ? 'checked' : '' ; ?>>
						<label class="onoffswitch-label" for="config_on_off_fields_phone">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
					</td>
				</tr>
				<tr>
					<td class="fo-label"><?php echo $text_requared_fields?></td>
					<td>
					<div class="onoffswitch">
						<input value="1" type="checkbox" name="config_fields_phone_requared" class="onoffswitch-checkbox" id="config_fields_phone_requared" <?php echo $config_fields_phone_requared == '1' ? 'checked' : '' ; ?>>
						<label class="onoffswitch-label" for="config_fields_phone_requared">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
					</td>
				</tr>
				<tr>
					<td class="fo-label"><?php echo $text_placeholder_fields; ?></td>
					<td><?php foreach ($languages as $language) { ?>
						<input class="form-control-fastorder" type="text" name="config_placeholder_fields_phone[<?php echo $language['language_id']; ?>][config_placeholder_fields_phone]" value="<?php echo isset($config_placeholder_fields_phone[$language['language_id']]) ? $config_placeholder_fields_phone[$language['language_id']]['config_placeholder_fields_phone'] : ''; ?>"size="50" />
						<img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br>
				<?php } ?></td>
				</tr>
				<tr>
					<td class="fo-label"><?php echo $entry_mask_phone_number ?></td>
					<td><input class="form-control-fastorder" type="text" name="config_mask_phone_number" value="<?php echo $config_mask_phone_number ?>" size="25" /></td>
				</tr>
				
				<!--ПОЛЕ COMMENT-->
				
				<tr>
					<td class="title-fields">
						<?php echo $text_on_off_fields_comment;?>
					</td>
				</tr>
				<tr>
					<td class="fo-label"><?php echo $text_status_fields;?></td>
					<td>
					<div class="onoffswitch">
						<input value="1" type="checkbox" name="config_on_off_fields_comment" class="onoffswitch-checkbox" id="config_on_off_fields_comment" <?php echo $config_on_off_fields_comment == '1' ? 'checked' : '' ; ?>>
						<label class="onoffswitch-label" for="config_on_off_fields_comment">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
					</td>
				</tr>
				<tr>
					<td class="fo-label"><?php echo $text_requared_fields?></td>
					<td>
					<div class="onoffswitch">
						<input value="1" type="checkbox" name="config_fields_comment_requared" class="onoffswitch-checkbox" id="config_fields_comment_requared" <?php echo $config_fields_comment_requared == '1' ? 'checked' : '' ; ?>>
						<label class="onoffswitch-label" for="config_fields_comment_requared">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
					</td>
				</tr>
				<tr>
					<td class="fo-label"><?php echo $text_placeholder_fields; ?></td>
					<td><?php foreach ($languages as $language) { ?>
						<input class="form-control-fastorder" type="text" name="config_placeholder_fields_comment[<?php echo $language['language_id']; ?>][config_placeholder_fields_comment]" value="<?php echo isset($config_placeholder_fields_comment[$language['language_id']]) ? $config_placeholder_fields_comment[$language['language_id']]['config_placeholder_fields_comment'] : ''; ?>"size="50" />
						<img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br>
				<?php } ?></td>
				</tr>
				<!--ПОЛЕ EMAIL-->
				<tr>
					<td class="title-fields">
						<?php echo $text_on_off_fields_email;?>
					</td>
				</tr>
				<tr>
					<td class="fo-label"><?php echo $text_status_fields;?></td>
					<td>
					<div class="onoffswitch">
						<input value="1" type="checkbox" name="config_on_off_fields_email" class="onoffswitch-checkbox" id="config_on_off_fields_email" <?php echo $config_on_off_fields_email == '1' ? 'checked' : '' ; ?>>
						<label class="onoffswitch-label" for="config_on_off_fields_email">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
					</td>
				</tr>
				<tr>
					<td class="fo-label"><?php echo $text_requared_fields?></td>
					<td>
					<div class="onoffswitch">
						<input value="1" type="checkbox" name="config_fields_email_requared" class="onoffswitch-checkbox" id="config_fields_email_requared" <?php echo $config_fields_email_requared == '1' ? 'checked' : '' ; ?>>
						<label class="onoffswitch-label" for="config_fields_email_requared">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
					</td>
				</tr>
				<tr>
					<td class="fo-label"><?php echo $text_placeholder_fields; ?></td>
					<td><?php foreach ($languages as $language) { ?>
						<input class="form-control-fastorder" type="text" name="config_placeholder_fields_email[<?php echo $language['language_id']; ?>][config_placeholder_fields_email]" value="<?php echo isset($config_placeholder_fields_email[$language['language_id']]) ? $config_placeholder_fields_email[$language['language_id']]['config_placeholder_fields_email'] : ''; ?>"size="50" />
						<img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br>
				<?php } ?></td>
				</tr>
				
			</table>
        </div>
		<div id="tab-general-setting">
			<table class="form">
				<tr>
					<td class="title-fields">
						<?php echo $text_setting_popup_and_button_on_off;?>
					</td>
				</tr>
				<tr>
					<td class="fo-label"><?php echo $text_general_image_product_popup?></td>
					<td>
					<div class="onoffswitch">
						<input value="1" type="checkbox" name="config_general_image_product_popup" class="onoffswitch-checkbox" id="config_general_image_product_popup" <?php echo $config_general_image_product_popup == '1' ? 'checked' : '' ; ?>>
						<label class="onoffswitch-label" for="config_general_image_product_popup">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
					</td>
				</tr>
				<tr>
					<td class="fo-label"><?php echo $text_on_off_qo_fm?></td>
					<td>
					<div class="onoffswitch">
						<input value="1" type="checkbox" name="config_on_off_qo_fm" class="onoffswitch-checkbox" id="config_on_off_qo_fm" <?php echo $config_on_off_qo_fm == '1' ? 'checked' : '' ; ?>>
						<label class="onoffswitch-label" for="config_on_off_qo_fm">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
					</td>
				</tr>
				<tr>
					<td class="fo-label"><?php echo $text_on_off_qo_sm?></td>
					<td>
					<div class="onoffswitch">
						<input value="1" type="checkbox" name="config_on_off_qo_sm" class="onoffswitch-checkbox" id="config_on_off_qo_sm" <?php echo $config_on_off_qo_sm == '1' ? 'checked' : '' ; ?>>
						<label class="onoffswitch-label" for="config_on_off_qo_sm">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
					</td>
				</tr>
				<tr>
					<td class="fo-label"><?php echo $text_on_off_qo_bm?></td>
					<td>
					<div class="onoffswitch">
						<input value="1" type="checkbox" name="config_on_off_qo_bm" class="onoffswitch-checkbox" id="config_on_off_qo_bm" <?php echo $config_on_off_qo_bm == '1' ? 'checked' : '' ; ?>>
						<label class="onoffswitch-label" for="config_on_off_qo_bm">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
					</td>
				</tr>
				<tr>
					<td class="fo-label"><?php echo $text_on_off_qo_lm?></td>
					<td>
					<div class="onoffswitch">
						<input value="1" type="checkbox" name="config_on_off_qo_lm" class="onoffswitch-checkbox" id="config_on_off_qo_lm" <?php echo $config_on_off_qo_lm == '1' ? 'checked' : '' ; ?>>
						<label class="onoffswitch-label" for="config_on_off_qo_lm">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
					</td>
				</tr>
				<tr>
					<td class="fo-label"><?php echo $text_on_off_qo_cpage?></td>
					<td>
					<div class="onoffswitch">
						<input value="1" type="checkbox" name="config_on_off_qo_cpage" class="onoffswitch-checkbox" id="config_on_off_qo_cpage" <?php echo $config_on_off_qo_cpage == '1' ? 'checked' : '' ; ?>>
						<label class="onoffswitch-label" for="config_on_off_qo_cpage">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
					</td>
				</tr>
				<tr>
					<td class="fo-label"><?php echo $text_on_off_qo_special_page?></td>
					<td>
					<div class="onoffswitch">
						<input value="1" type="checkbox" name="config_on_off_qo_special_page" class="onoffswitch-checkbox" id="config_on_off_qo_special_page" <?php echo $config_on_off_qo_special_page == '1' ? 'checked' : '' ; ?>>
						<label class="onoffswitch-label" for="config_on_off_qo_special_page">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
					</td>
				</tr>
				<tr>
					<td class="fo-label"><?php echo $text_on_off_qo_search_page?></td>
					<td>
					<div class="onoffswitch">
						<input value="1" type="checkbox" name="config_on_off_qo_search_page" class="onoffswitch-checkbox" id="config_on_off_qo_search_page" <?php echo $config_on_off_qo_search_page == '1' ? 'checked' : '' ; ?>>
						<label class="onoffswitch-label" for="config_on_off_qo_search_page">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
					</td>
				</tr>
				<tr>
					<td class="fo-label"><?php echo $text_on_off_qo_manufacturer_page?></td>
					<td>
					<div class="onoffswitch">
						<input value="1" type="checkbox" name="config_on_off_qo_manufacturer_page" class="onoffswitch-checkbox" id="config_on_off_qo_manufacturer_page" <?php echo $config_on_off_qo_manufacturer_page == '1' ? 'checked' : '' ; ?>>
						<label class="onoffswitch-label" for="config_on_off_qo_manufacturer_page">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
					</td>
				</tr>
				<tr>
					<td class="fo-label"><?php echo $text_on_off_qo_product_page?></td>
					<td>
					<div class="onoffswitch">
						<input value="1" type="checkbox" name="config_on_off_qo_product_page" class="onoffswitch-checkbox" id="config_on_off_qo_product_page" <?php echo $config_on_off_qo_product_page == '1' ? 'checked' : '' ; ?>>
						<label class="onoffswitch-label" for="config_on_off_qo_product_page">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
					</td>
				</tr>
				<tr>
					<td class="fo-label"><?php echo $text_on_off_qo_shopping_cart?></td>
					<td>
					<div class="onoffswitch">
						<input value="1" type="checkbox" name="config_on_off_qo_shopping_cart" class="onoffswitch-checkbox" id="config_on_off_qo_shopping_cart" <?php echo $config_on_off_qo_shopping_cart == '1' ? 'checked' : '' ; ?>>
						<label class="onoffswitch-label" for="config_on_off_qo_shopping_cart">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
					</td>
				</tr>
				
				
				<tr>
					<td><?php echo $entry_title_popup_quickorder; ?></td>
					<td><?php foreach ($languages as $language) { ?>
						<input rows="5" class="form-control-fastorder" type="text" name="config_title_popup_quickorder[<?php echo $language['language_id']; ?>][config_title_popup_quickorder]" value="<?php echo isset($config_title_popup_quickorder[$language['language_id']]) ? $config_title_popup_quickorder[$language['language_id']]['config_title_popup_quickorder'] : ''; ?>"size="50" />
						<img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br>
				<?php } ?></td>
				</tr>
				<tr>
					<td>Header Title Popup Success</td>
					<td><?php foreach ($languages as $language) { ?>
						<input rows="5" class="form-control-fastorder" type="text" name="config_title_popup_quickorder[<?php echo $language['language_id']; ?>][config_title_success]" value="<?php echo isset($config_title_popup_quickorder[$language['language_id']]) ? $config_title_popup_quickorder[$language['language_id']]['config_title_success'] : ''; ?>"size="50" />
						<img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br>
				<?php } ?></td>
				</tr>
				<tr>
					<td><?php echo $text_complete_quickorder; ?></td>
					<td><?php foreach ($languages as $language) { ?>
						<textarea  rows="5" class="form-control-fastorder" type="text" name="config_complete_quickorder[<?php echo $language['language_id']; ?>][config_complete_quickorder]" ><?php echo isset($config_complete_quickorder[$language['language_id']]) ? $config_complete_quickorder[$language['language_id']]['config_complete_quickorder'] : ''; ?></textarea>
						<img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br>
				<?php } ?></td>
				</tr>
				<tr>
					<td colspan="3"><?php echo $entry_instruction_icon_change_fastorder; ?></td>
				</tr>
				<tr>
					<td><?php echo $entry_icon_send_fastorder ?></td>
					<td><input class="form-control-fastorder" type="text" name="config_icon_send_fastorder" value="<?php echo $config_icon_send_fastorder ?>" size="20" /></td>
				</tr>
				<tr>
					<td><?php echo $entry_background_button_send_fastorder ?></td>
					<td><input class="jpicker-fastorder form-control-fastorder" type="text" name="config_background_button_send_fastorder" value="<?php echo $config_background_button_send_fastorder ?>" size="7" /></td>
				</tr>
				<tr>
					<td><?php echo $entry_background_button_send_fastorder_hover ?></td>
					<td><input class="jpicker-fastorder form-control-fastorder" type="text" name="config_background_button_send_fastorder_hover" value="<?php echo $config_background_button_send_fastorder_hover ?>" size="7" /></td>
				</tr>
				<tr>
					<td><?php echo $entry_background_button_open_form_send_order ?></td>
					<td><input class="jpicker-fastorder form-control-fastorder" type="text" name="config_background_button_open_form_send_order" value="<?php echo $config_background_button_open_form_send_order ?>" size="7" /></td>
				</tr>
				<tr>
					<td><?php echo $entry_background_button_open_form_send_order_hover ?></td>
					<td><input class="jpicker-fastorder form-control-fastorder" type="text" name="config_background_button_open_form_send_order_hover" value="<?php echo $config_background_button_open_form_send_order_hover ?>" size="7" /></td>
				</tr>
				<tr>
					<td><?php echo $entry_icon_open_form_send_order ?></td>
					<td><input class="form-control-fastorder" type="text" name="config_icon_open_form_send_order" value="<?php echo $config_icon_open_form_send_order ?>" size="20" /></td>
				</tr>
				<tr>
					<td><?php echo $entry_color_button_open_form_send_order ?></td>
					<td><input class="jpicker-fastorder form-control-fastorder" type="text" name="config_color_button_open_form_send_order" value="<?php echo $config_color_button_open_form_send_order ?>" size="7" /></td>
				</tr>
				<tr>
					<td><?php echo $entry_text_open_form_send_order; ?></td>
					<td><?php foreach ($languages as $language) { ?>
						<input class="form-control-fastorder" type="text" name="config_text_open_form_send_order[<?php echo $language['language_id']; ?>][config_text_open_form_send_order]" value="<?php echo isset($config_text_open_form_send_order[$language['language_id']]) ? $config_text_open_form_send_order[$language['language_id']]['config_text_open_form_send_order'] : ''; ?>"size="50" />
						<img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br>
				<?php } ?></td>
				</tr>
				<tr>
					<td><?php echo $entry_any_text_at_the_bottom; ?></td>
					<td><?php foreach ($languages as $language) { ?>
						<input class="form-control-fastorder" type="text" name="config_any_text_at_the_bottom[<?php echo $language['language_id']; ?>][config_any_text_at_the_bottom]" value="<?php echo isset($config_any_text_at_the_bottom[$language['language_id']]) ? $config_any_text_at_the_bottom[$language['language_id']]['config_any_text_at_the_bottom'] : ''; ?>"size="50" />
						<img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br>
				<?php } ?></td>
				</tr>
				<tr>
					<td><?php echo $entry_any_text_at_the_bottom_color ?></td>
					<td><input class="jpicker-fastorder form-control-fastorder" type="text" name="config_any_text_at_the_bottom_color" value="<?php echo $config_any_text_at_the_bottom_color; ?>" size="7" /></td>
				</tr>
			</table>
        </div>
		<div id="tab-designed">
			<table class="form">
				<tr>
					<td class="title-fields"><?php echo $select_design_fast_order;?></td>
					<td></td>
				</tr>
				<tr>
				<td colspan="2">
				
					<select name="config_select_design_fastorder" class="form-control-fastorder">
					<option  <?php if ($config_select_design_fastorder == 1 ) echo 'selected' ; ?> value="1"><?php echo $text_theme1;?></option>
					<option  <?php if ($config_select_design_fastorder == 2 ) echo 'selected' ; ?> value="2"><?php echo $text_theme2;?></option>
					<option  <?php if ($config_select_design_fastorder == 3 ) echo 'selected' ; ?> value="3"><?php echo $text_theme3;?></option>
					<option  <?php if ($config_select_design_fastorder == 4 ) echo 'selected' ; ?> value="4"><?php echo $text_theme4;?></option>
					</select>
				</td>
				</tr>
				<tr>
					<td><div style="max-width:300px;text-align:center;font-weight:600;font-size:16px;"><?php echo "1";?></div><br /><img src="view/image/fasttheme_1.png" style="max-width:300px;"></td>
					
					<td><div style="max-width:390px;text-align:center;font-weight:600;font-size:16px;"><?php echo "2";?></div><br /><img src="view/image/fasttheme_2.png" style="max-width:390px;"></td>
				<tr>
			</table>
        </div>	
		<div id="tab-sms-setting">
			<table class="form">
				<tr>
					<td><?php echo $register_site_fastorder; ?></td>
					<td><a href="http://my.smscab.ru/"><?php echo "http://my.smscab.ru/";?></a></td>
				</tr>
			
			<tr>
				<td><?php echo $on_off_sms_fastorder ; ?></td>
				<td>
					<div class="onoffswitch">
						<input value="1" type="checkbox" name="config_send_sms_on_off_fastorder" class="onoffswitch-checkbox" id="config_send_sms_on_off_fastorder" <?php echo $config_send_sms_on_off_fastorder == '1' ? 'checked' : '' ; ?>>
						<label class="onoffswitch-label" for="config_send_sms_on_off_fastorder">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
				</td>
			</tr>
			<tr>
				<td><?php echo $entry_phone_number_send_sms_fastorder ?></td>
				<td><input class="form-control-fastorder" type="text" name="config_phone_number_send_sms_fastorder" value="<?php echo $config_phone_number_send_sms_fastorder ?>" size="20" /></td>
			</tr>
			<tr>
				<td><?php echo $entry_login_send_sms_fastorder ?></td>
				<td><input class="form-control-fastorder" type="text" name="config_login_send_sms_fastorder" value="<?php echo $config_login_send_sms_fastorder ?>" size="20" /></td>
			</tr>
			<tr>
				<td><?php echo $entry_pass_send_sms_fastorder ?></td>
				<td><input class="form-control-fastorder" type="text" name="config_pass_send_sms_fastorder" value="<?php echo $config_pass_send_sms_fastorder ?>" size="20" /></td>
			</tr>
			</table>
        </div>
		<div id="tab-email-setting">
			<table class="form">
				<tr>
					<td class="title-fields">
						<?php echo $form_latter_from_buyer;?>
					</td>
				</tr>
				<tr>
					<td><?php echo $text_on_off_send_buyer_mail ; ?></td>
					<td>
						<div class="onoffswitch">
							<input value="1" type="checkbox" name="config_on_off_send_buyer_mail" class="onoffswitch-checkbox" id="config_on_off_send_buyer_mail" <?php echo $config_on_off_send_buyer_mail == '1' ? 'checked' : '' ; ?>>
							<label class="onoffswitch-label" for="config_on_off_send_buyer_mail">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					</td>
				</tr>
				<tr>
					<td><?php echo $text_form_latter_products ; ?></td>
					<td>
						<div class="onoffswitch">
							<input value="1" type="checkbox" name="config_buyer_html_products" class="onoffswitch-checkbox" id="config_buyer_html_products" <?php echo $config_buyer_html_products == '1' ? 'checked' : '' ; ?>>
							<label class="onoffswitch-label" for="config_buyer_html_products">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					</td>
				</tr>
			</table>	
			<div id="languages" class="htabs">
				<?php foreach ($languages as $language) { ?>
					<a href="#language<?php echo $language['language_id']; ?>"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a>
				<?php } ?>
			</div>	
			<?php foreach ($languages as $language) { ?>			
				<div id="language<?php echo $language['language_id']; ?>">
					<table class="form">					
						<tr>
							<td><?php echo $quickorder_subject_buyer;?><br /><?php echo $subject_text_variables;?></td>
							<td><textarea name="quickorder_subject<?php echo $language['language_id'] ?>" id="quickorder_subject<?php echo $language['language_id'] ?>" cols="50" rows="3"><?php echo isset(${'quickorder_subject' . $language['language_id']}) ? ${'quickorder_subject' . $language['language_id']} : ''; ?></textarea><br />																			
						</tr>
						<tr>
							<td><?php echo $quickorder_description_buyer;?><br /><?php echo $subject_text_variables;?><br /><?php echo $list_of_variables_entry;?></td>
							<td><textarea name="quickorder_description<?php echo $language['language_id'] ?>" id="quickorder_description<?php echo $language['language_id'] ?>" cols="50" rows="3"><?php echo isset(${'quickorder_description' . $language['language_id']}) ? ${'quickorder_description' . $language['language_id']} : ''; ?></textarea>					
						</tr>					
					</table>
				</div>	
			<?php } ?>	

			<table class="form">	
				<tr>
					<td class="title-fields">
						<?php echo $form_latter_from_me;?>
					</td>
				</tr>
				<tr>
					<td><?php echo "Email" ?></td>
					<td><input class="form-control-fastorder" type="text" name="config_email_me" value="<?php echo $config_email_me ?>"/></td>
				</tr>
				<tr>
					<td><?php echo $text_on_off_send_me_mail ; ?></td>
					<td>
						<div class="onoffswitch">
							<input value="1" type="checkbox" name="config_on_off_send_me_mail" class="onoffswitch-checkbox" id="config_on_off_send_me_mail" <?php echo $config_on_off_send_me_mail == '1' ? 'checked' : '' ; ?>>
							<label class="onoffswitch-label" for="config_on_off_send_me_mail">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					<td>
				</tr>
				<tr>
					<td><?php echo $text_form_latter_products ; ?></td>
					<td>
						<div class="onoffswitch">
							<input value="1" type="checkbox" name="config_me_html_products" class="onoffswitch-checkbox" id="config_me_html_products" <?php echo $config_me_html_products == '1' ? 'checked' : '' ; ?>>
							<label class="onoffswitch-label" for="config_me_html_products">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					</td>
				</tr>
			</table>
			<div id="languages2" class="htabs">
				<?php foreach ($languages as $language) { ?>
					<a href="#language2<?php echo $language['language_id']; ?>"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a>
				<?php } ?>
			</div>	
			<?php foreach ($languages as $language) { ?>			
				<div id="language2<?php echo $language['language_id']; ?>">
					<table class="form">					
						<tr>
							<td><?php echo $quickorder_subject_buyer;?><br /><?php echo $subject_text_variables;?></td>
							<td><textarea name="quickorder_subject_me<?php echo $language['language_id'] ?>" id="quickorder_subject_me<?php echo $language['language_id'] ?>" cols="50" rows="3"><?php echo isset(${'quickorder_subject_me' . $language['language_id']}) ? ${'quickorder_subject_me' . $language['language_id']} : ''; ?></textarea><br />																			
						</tr>
						<tr>
							<td><?php echo $quickorder_description_buyer;?><br /><?php echo $subject_text_variables;?><br /><?php echo $list_of_variables_entry;?></td>
							<td><textarea name="quickorder_description_me<?php echo $language['language_id'] ?>" id="quickorder_description_me<?php echo $language['language_id'] ?>" cols="50" rows="3"><?php echo isset(${'quickorder_description_me' . $language['language_id']}) ? ${'quickorder_description_me' . $language['language_id']} : ''; ?></textarea>					
						</tr>					
					</table>
				</div>	
			<?php } ?>			
		</div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script> 
<script type="text/javascript">
<?php foreach ($languages as $language) { ?>
CKEDITOR.replace('quickorder_description<?php echo $language['language_id']; ?>', {
	filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
});
<?php } ?>
<?php foreach ($languages as $language) { ?>
CKEDITOR.replace('quickorder_description_me<?php echo $language['language_id']; ?>', {
	filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
});
<?php } ?>
</script>
<script type="text/javascript"><!--
function image_upload(field, thumb) {
	$('#dialog').remove();
	
	$('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');
	
	$('#dialog').dialog({
		title: '<?php echo $text_image_manager; ?>',
		close: function (event, ui) {
			if ($('#' + field).attr('value')) {
				$.ajax({
					url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).val()),
					dataType: 'text',
					success: function(data) {
						$('#' + thumb).replaceWith('<img src="' + data + '" alt="" id="' + thumb + '" />');
					}
				});
			}
		},	
		bgiframe: false,
		width: 800,
		height: 400,
		resizable: false,
		modal: false
	});
};
//--></script> 
<script type="text/javascript"><!--
$('#tabs a').tabs();
$('#languages a').tabs();  
$('#languages2 a').tabs();  
//--></script> 
<?php echo $footer; ?>
