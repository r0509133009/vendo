<?php echo $header; ?>
<div id="content">
<style>
.form-control {
     width: 100%; 
     box-sizing: border-box;
     -webkit-box-sizing:border-box;
     -moz-box-sizing: border-box;
	  background-color: #fff;
  background-image: none;
  border: 1px solid #ccc;
  border-radius: 4px;
  height:30px;
}
input.form-control:focus {
  border-color: #66afe9;
  outline: 0;
  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
          box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
}
.btn-filter, .edit-fastorder, .btnurl {
	background-color: #1ea7d8;
	border-color: #0790c1;
	border-radius:4px;
	border-style: solid;
	border-width: 1px 1px 2px;
	color: #ffffff !important;
	font-size: 14px;
	padding: 4px 7px;
	text-decoration:none !important;
}
.btn-filter:hover, .edit-fastorder:hover,.btnurl:hover {
	background-color: #0790c1;
}
.edit-setting {
	background: url("view/image/edit-setting.png") no-repeat center center;
	width:40px;
}
</style>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').attr('action', '<?php echo $update; ?>'); $('#form').submit();" class="button"><span><?php echo $status_done; ?></span></a><a onclick="$('form').submit();" class="button"><span><?php echo $button_delete; ?></span></a><a class="button" href="<?php echo $fastorder_setting ?>"><span><?php echo $button_fastorder_setting;?></span></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="list">
          <thead>
            <tr>
              <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>

              <td class="left"><?php if ($sort == 'fast_id') { ?>
                <a href="<?php echo $sort_fast_id; ?>" class="<?php echo strtolower($order); ?>"><?php echo "№"; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_fast_id; ?>"><?php echo "№"; ?></a>
                <?php } ?></td>
              <td class="left"><?php if ($sort == 'name') { ?>
                <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                <?php } ?></td>
            
			  <td class="center"><?php echo $text_comment_buyer; ?></td>
			  <td class="center"><?php echo $number_order_id; ?></td>
			  <td class="center"><?php echo $product_name_fast; ?></td>
			  <td class="center" style="min-width:100px;"><?php echo $text_total_all; ?></td>
			  <td class="center"><?php echo $text_newfastorder_url; ?></td>
			  <td class="center"><?php echo $text_comment; ?></td>
			  <td class="center"><?php if ($sort == 'username') { ?>
                <a href="<?php echo $sort_username; ?>" class="<?php echo strtolower($order); ?>"><?php echo $text_manager; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_username; ?>"><?php echo $text_manager; ?></a>
                <?php } ?></td>
			 
              <td class="right"><?php echo $text_status; ?></td>
              <td class="right"><?php echo $text_added; ?></td>
              <td class="right"><?php echo $text_modified; ?></td>
              <td class="right edit-setting"></td>
            </tr>
			<tr>
				<td class="center"></td>
				<td class="center"></td>
				<td class="center"><input type="text" name="searh_info_user" class="form-control" value=""/></td>
				<td class="center"></td>
				<td class="center"></td>
				<td class="center"></td>
				<td class="center"></td>
				<td class="center"><input type="text" name="filter_url" class="form-control" value=""/></td>
				<td class="center"></td>
				<td class="text-center">
					<select class="form-control" name="filter_manager">
						<?php if (!empty($filter_manager)) { ?>
							<option value="" selected="selected"><?php echo " --- ";?></option>
						<?php } else { ?>
							<option value=""><?php echo " --- ";?></option>
						<?php } ?>	
											
					<?php foreach ($users as $user){?>
						<?php if (!empty($filter_manager)) { ?>
							<?php if ($filter_manager == $user['username'])?>
							<option selected="selected" value="<?php echo $user['username']?>"><?php echo $user['username']?></option>
						<?php } else { ?>
						<option value="<?php echo $user['username']?>"><?php echo $user['username']?></option>
						<?php } ?>
					<?php }?>					
					</select>
				</td>
				<td class="center">
					<select class="form-control" name="filter_status">
						<option value="" selected="selected"><?php echo " --- ";?></option>
						<option <?php if($filter_status == '1') { ?>selected="selected" <?php } ?> value="1"><?php echo $status_done;?></option>
						<option <?php if($filter_status == '0') { ?>selected="selected" <?php } ?>  value="0"><?php echo $status_wait;?></option>
					</select>
				</td>
				<td><input class="form-control date" type="text" name="filter_date_added" value="<?php echo $filter_date_added; ?>" size="12" /></td>
				<td><input class="form-control date" type="text" name="filter_date_modified" value="<?php echo $filter_date_modified; ?>" size="12" /></td>
				<td class="center"><button type="button" id="button-filter" class="btn-filter"><i class="fa fa-search"></i></button></td>
			</tr>
          </thead>
          <tbody>
            <?php if ($newfastorders) { ?>
            <?php foreach ($newfastorders as $newfastorder) { ?>
            <tr>
              <td style="text-align: center;"><?php if ($newfastorder['selected']) { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $newfastorder['newfastorder_id']; ?>" checked="checked" />
                <?php } else { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $newfastorder['newfastorder_id']; ?>" />
                <?php } ?></td>
              <td class="left"><?php echo $newfastorder['newfastorder_id']; ?></td>
              <td class="left" style="min-width:180px;">
				<div class="firstname"><i class="fa fa-user fa-fw"></i> <?php echo $newfastorder['name']; ?></div>
				<div class="telephone"><i class="fa fa-phone fa-fw"></i><?php echo $newfastorder['telephone']; ?></div>
				<?php if($newfastorder['email_buyer'] !=''){ ?><div class="email"><i class="fa fa-envelope-o fa-fw"></i> <?php echo $newfastorder['email_buyer']; ?></div><?php } ?>
			  </td>
              
              <td class="left"><?php echo utf8_substr(strip_tags($newfastorder['comment_buyer']),0,50); ?></td>
             
              
              <td class="center"><a href="index.php?route=sale/order/update&token=<?php echo $token;?>&order_id=<?php echo $newfastorder['order_id']?>"><?php echo $text_number_order_id_;?><?php echo $newfastorder['order_id']; ?></a></td>
              <td class="left">
			  
				<?php foreach ($newfastorder['fastproduct'] as $result_prod){ ?>
					<div class="product_name"><a target="_blank" href="index.php?route=catalog/product/update&token=<?php echo $token;?>&product_id=<?php echo $result_prod['product_id'];?>"><?php echo $result_prod['product_name'];?></a></div>
					<div class="product_model"><?php echo $result_prod['model'];?></div>
						<?php foreach ($result_prod['options'] as $res_option) { ?>
							<small>- <?php echo $res_option['name'];?>: <?php echo $res_option['value'];?></small><br>
						<?php } ?>
					<div><img style="max-height:50px;" src="<?php echo $result_prod['product_image']; ?>"/></div>
					
					<div class="price_fast"><?php echo $result_prod['price'];?> x <?php echo $result_prod['quantity'];?> = <?php echo $result_prod['total'];?></div>	
				<br>
				<?php } ?>
			  </td>
			 <style>
				.price_fast {
					display: block;
					min-width: 180px;
				}
				.product_model{
					font-weight:bold;
				}
			 </style>
				<td class="left" style="font-weight:bold;"><?php echo $newfastorder['total']; ?></td>
              <td class="center"><a class="btnurl" title="<?php echo $text_link_description;?>" href="<?php echo $newfastorder['url_site']; ?>" target="_blank"><?php echo $text_link; ?></td>
			  <td class="left"><?php echo utf8_substr(strip_tags($newfastorder['comment']),0,50); ?></td> 
			  <td class="center"><?php echo $newfastorder['username']; ?></td>
           <?php if ($newfastorder['status'] == $status_done) { ?>
              <td class="center" style="background:#0BED0B !important;"><?php echo $newfastorder['status']; ?></td>
            <?php } else { ?>
              <td class="center" style="background:#EDB40B !important;"><?php echo $newfastorder['status']; ?></td>
            <?php } ?>
              <td class="center"><?php echo $newfastorder['date_added']; ?></td>
             <td class="center"><?php echo $newfastorder['date_modified']; ?></td>
              <td class="center"><a class="edit-fastorder" href="<?php echo $newfastorder['action']; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
            </tr>
            <?php } ?>

<script type="text/javascript"><!--
$(document).ready(function() {
	$('.date').datepicker({dateFormat: 'yy-mm-dd'});
});
//--></script> 
<script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	url = 'index.php?route=sale/newfastorder&token=<?php echo $token; ?>';

	var searh_info_user = $('input[name=\'searh_info_user\']').val();

	if (searh_info_user) {
		url += '&searh_info_user=' + encodeURIComponent(searh_info_user);
	}
	var filter_url = $('input[name=\'filter_url\']').val();

	if (filter_url) {
		url += '&filter_url=' + encodeURIComponent(filter_url);
	}
	var filter_manager = $('select[name=\'filter_manager\']').val();

	if (filter_manager) {
		url += '&filter_manager=' + encodeURIComponent(filter_manager);
	}
	
	var filter_status = $('select[name=\'filter_status\']').val();

	if (filter_status) {
		url += '&filter_status=' + encodeURIComponent(filter_status);
	}
	
	var filter_date_added = $('input[name=\'filter_date_added\']').val();

	if (filter_date_added) {
		url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
	}
	var filter_date_modified = $('input[name=\'filter_date_modified\']').val();

	if (filter_date_modified) {
		url += '&filter_date_modified=' + encodeURIComponent(filter_date_modified);
	}

	location = url;
});
//--></script>
 <script type="text/javascript"><!--
 $('input[name=\'searh_info_user\']').autocomplete({
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=sale/newfastorder/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.name,
						value: item.fast_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'searh_info_user\']').val(ui.item.label);
						
		return false;
	},
	focus: function(event, ui) {
      	return false;
   	}
});
//--></script>	
            <?php } else { ?>
            <tr>
              <td class="center" colspan="9"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </form>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>
<?php echo $footer; ?>
