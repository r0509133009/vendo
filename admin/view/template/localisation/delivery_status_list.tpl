<?php 
/*
  Opencart Delivery Status Extension for Opencart
  Version: 1.0, Feb 2014
  Author: Danny Kessels opencart@parkstadmedia.nl
*/

?>
<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/stock-status.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a href="<?php echo $insert; ?>" class="button"><?php echo $button_insert; ?></a><a onclick="$('form').submit();" class="button"><?php echo $button_delete; ?></a></div>
    </div>
    <div class="content">
    <div id="tabs" class="htabs"><a href="#tab-statuses"><?php echo $tab_statuses; ?></a><a href="#tab-info"><?php echo $tab_info; ?></a><a href="#tab-contact"><?php echo $tab_contact; ?></a></div>
      
      <div id="tab-statuses">
      <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="list">
          <thead>
            <tr>
              <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
              <td class="left"><?php if ($sort == 'name') { ?>
                <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                <?php } ?></td>
              <td class="right"><?php echo $column_action; ?></td>
            </tr>
          </thead>
          <tbody>
            <?php if ($delivery_statuses) { ?>
            <?php foreach ($delivery_statuses as $delivery_status) { ?>
            <tr>
              <td style="text-align: center;"><?php if ($delivery_status['selected']) { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $delivery_status['delivery_status_id']; ?>" checked="checked" />
                <?php } else { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $delivery_status['delivery_status_id']; ?>" />
                <?php } ?></td>
              <td class="left"><?php echo $delivery_status['name']; ?></td>
              <td class="right"><?php foreach ($delivery_status['action'] as $action) { ?>
                [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                <?php } ?></td>
            </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
              <td class="center" colspan="3"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
        </div>
        <div id="tab-info">
		<table class="form">
  			<tr>
         	 	<td><?php echo $description; ?></td>
       		</tr>
  		</table>
	  </div>
	
	<div id="tab-contact">
		<table class="form">
		<tr>
			<td><img src="view/image/logopsm.jpg" /></td><td></td>
		</tr>
		<tr>
          <td>Developer:</td>
          <td>Danny Kessels</td>
        </tr>
        <tr>
          <td>Company:</td>
          <td>Parkstadmedia</td>
        </tr>
        <tr>
          <td>Website</td>
          <td><a href="http://www.parkstadmedia.nl" target="_blank">Parkstadmedia.nl</a></td>
        </tr>
        <tr>
          <td>Questions:</td>
          <td><a title="Send your questions here!" href="mailto:opencart@parkstadmedia.nl?SUBJECT=Opencart Delivery Status Extension Question (Bol.com)">opencart@parkstadmedia.nl</a></td>
        </tr>
        <tr>
          <td>Current Version:</td>
          <td><?php echo $text_current_version; ?></td>
        </tr>
        <tr>
          <td>Latest Version:</td>
          <td><?php
			$latest_version_url="http://www.opencart.parkstadmedia.nl/feed/delivery_status/version.txt";
			$latest_version = file_get_contents($latest_version_url);
			echo $latest_version; 
			if ($text_current_version < $latest_version){
				echo " <em style='color:#ff0000;'> You need to update your version!</em>";
			}
			if ($text_current_version == $latest_version){
				echo " <em style='color:#008000;'> You are working with the latest version!</em>";
			}
			?>
			
			</td>
        </tr>
        <tr>
          <td>Date:</td>
          <td>March 2014</td>
        </tr>
        <tr>
          <td>Changelog:</td>
          <td><?php
			$changelog_url="http://www.opencart.parkstadmedia.nl/feed/delivery_status/changelog.txt";
			$changelog = file_get_contents($changelog_url);
			echo $changelog; ?></td>
        </tr>
		</table>
	</div>

      </form>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>
</script> 
<script type="text/javascript"><!--
$('#tabs a').tabs(); 
$('#languages a').tabs();
//--></script> 
<?php echo $footer; ?>