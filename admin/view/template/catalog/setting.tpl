<?php  echo $header; ?>
<link rel='stylesheet' type='text/css' href='view/stylesheet/allseo.css' />
<div id="content">
   <div id="cssmenu">
    <ul>
    <?php foreach ($links as $link) { ?>
    <li><a class="top" href="<?php echo $link['href']; ?>"><?php echo $link['text']; ?></a></li>
    <?php } ?>
  </ul>
  </div>
    <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
   <div class="box grsnippet">
      <div class="heading">
        <h1><img src="view/image/setting.png" alt="" /> <?php echo $heading_title; ?></h1>
        <div class="buttons">
          <a class="button green" onclick="$('#settingdialog').dialog('open');"> <span>Help guide</span></a>
          <a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a>
        </div>
      </div>
        <div class="content">

          <form action="<?php echo $action; ?>" id="form" method="post" enctype="multipart/form-data"> 
            <div id="tab-company">
                  <table class="form">
                <div class="buttons">
                    <a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a>
                </div>
                    <h2>Settings - Enable / Disable different Features</h2>
                    <tr>
                      <td class="title"><?php echo $text_direct_links; ?></td>
                      <td class="inputbox">
                        <div class="onoffswitch">
                            <input type="checkbox" name="config_direct_links" class="onoffswitch-checkbox config_direct_links" id="myonoffswitch3"  <?php if($config_direct_links) echo "checked"; ?> >
                            <label class="onoffswitch-label" for="myonoffswitch3">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td class="title"><?php echo $text_multi_lang; ?></td>
                      <td class="inputbox">
                        <div class="onoffswitch">
                        
                            <input type="checkbox" name="config_breadcrumblink" class="onoffswitch-checkbox config_breadcrumblink" id="myonoffswitch1" <?php if($config_breadcrumblink) echo "checked"; ?> >

                            <label class="onoffswitch-label" for="myonoffswitch1">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                          </div>
                      </td>
                    </tr>
                    <tr>
                      <td class="title"><?php echo $text_social_widgets; ?></td>
                      <td class="inputbox">
                         <div class="onoffswitch">
                            <input type="checkbox" name="config_display_social_widgets" class="onoffswitch-checkbox config_display_social_widgets" id="myonoffswitch2"  <?php if($config_display_social_widgets) echo "checked"; ?> >
                            <label class="onoffswitch-label" for="myonoffswitch2">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                          </div>
                      </td>
                    </tr>

                  </table>
          </div>
      </form>
  </div>
  <br>
   <div id="settingdialog"><?php echo $text_about; ?></div>
  </div>
  </div>
  <script type="text/javascript">
  $('#tabs a').tabs(); 
  </script>
  <script type="text/javascript">
$('#content #cssmenu ul li:nth-child(8)').addClass('active'); 
</script>
<script type="text/javascript" src="view/javascript/jquery/ui/external/bgiframe/helper.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#settingdialog').dialog({
        title: 'This page contains snippet setting used by search engines and social sites',
        bgiframe: false,
        width: 600,
        height: 150,
        resizable: false,
        modal: true,
        autoOpen: false
    });
});
</script>
<?php echo $footer; ?>