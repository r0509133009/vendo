<?php echo $header; ?>
<link rel='stylesheet' type='text/css' href='view/stylesheet/allseo.css' />
<div id="content">
  <div id="cssmenu">
    <ul>
    <?php foreach ($links as $link) { ?>
    <li><a class="top" href="<?php echo $link['href']; ?>"><?php echo $link['text']; ?></a></li>
    <?php } ?>
  </ul>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/review.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a class="button green" onclick="$('#settingdialog').dialog('open');"> <span>Help guide</span></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
        <form action="<?php echo $sitemapg; ?>" method="post" enctype="multipart/form-data" id="sitemapg">
          <p class="helper-msg"><?php echo $seordata; ?>
            <?php if(isset($sitemapexists)) { ?>
              <br><div style="text-align:center;"><a href="<?php echo $sitemapexists; ?>" target="_blank" class="sitemap">Sitemap is found. Click To View</a></div>
            <?php } else { ?>
             <br><div style="text-align:center;"><a class="sitemap">Sitemap Doesn't Exist. Click Generate Below</a></div>
            <?php } ?>
          </p>
          <div class="buttons sbutton"><a onclick="$('#sitemapg').submit();" class="button mbutton" ><?php echo $button_generate; ?></a></div>
        </form>
      </div>
    </div>
</div>
<div id="settingdialog">
    <?php echo $help; ?>
</div>
<?php echo $footer; ?>
<script type="text/javascript">
$('#content #caamenu ul li:nth-child(5)').addClass('active'); 
</script>
<script type="text/javascript" src="view/javascript/jquery/ui/external/bgiframe/helper.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#settingdialog').dialog({
        title: "This page contains a tool for generating latest and updated sitemap",
        bgiframe: false,
        width: 600,
        height: 300,
        resizable: false,
        modal: true,
        autoOpen: false
    });
});
</script>