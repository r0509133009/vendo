<?php  echo $header; ?>
<link rel='stylesheet' type='text/css' href='view/stylesheet/allseo.css' />
<div id="content">
   <div id="cssmenu">
    <ul>
    <?php foreach ($links as $link) { ?>
    <li><a class="top" href="<?php echo $link['href']; ?>"><?php echo $link['text']; ?></a></li>
    <?php } ?>
  </ul>
  </div>
    <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
   <div class="box grsnippet">
      <div class="heading">
        <h1><img src="view/image/feed.png" alt="" /> <?php echo $heading_title; ?></h1>
        <div class="buttons">
          <a class="button green" onclick="$('#settingdialog').dialog('open');"> <span>Help guide</span></a>
          <a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a>
        </div>
      </div>
        <div class="content">
          <div id="tabs" class="htabs"><a href="#tab-company"><?php echo $tab_company; ?></a><a href="#tab-achieve"><?php echo $tab_achieve; ?></a><a href="#tab-verify"><?php echo $tab_verify; ?></a></div>

          <form action="<?php echo $action; ?>" id="form" method="post" enctype="multipart/form-data"> 
            <div id="tab-company">
            <div class="helper-msg">
              <?php echo $store_help; ?>
            </div>
              <table class="form">
                <div class="buttons">
                    <a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a>
                </div>
                    <h2>Store Details - Will be inserted on Home Page</h2>
                    <tr>
                      <td class="title"><?php echo $company_name;?></td>
                      <td valign="top" align="left"><input type="text" placeholder="Enter Company Name" name="config_gr_companyname" value="<?php echo $config_gr_companyname; ?>"></td>
                    </tr>
                     <tr>
                      <td class="title"><?php echo $country_name;?></td>
                      <td valign="top" align="left"><input type="text" placeholder="Enter Country" name="config_gr_countryname" value="<?php echo $config_gr_countryname; ?>"></td>
                    </tr>
                     <tr>
                      <td class="title"><?php echo $locality_name;?></td>
                      <td valign="top" align="left"><input type="text" placeholder="Locality Near by" name="config_gr_localityname" value="<?php echo $config_gr_localityname; ?>"></td>
                    </tr>
                     <tr>
                      <td class="title"><?php echo $postal_code;?></td>
                      <td valign="top" align="left"><input type="text" placeholder="Postal Code" name="config_gr_postalcode" value="<?php echo $config_gr_postalcode; ?>"></td>
                    </tr>
                    <tr>
                      <td class="title"><?php echo $street_address;?></td>
                      <td valign="top" align="left"><textarea placeholder="Enter the company address" cols="25" rows="5" name="config_gr_streetaddress"><?php echo $config_gr_streetaddress; ?></textarea></td>
                    </tr>
                    <tr>
                      <td class="title"><?php echo $telephone_number;?></td>
                      <td valign="top" align="left"><input type="text" placeholder="Telephone Number" name="config_gr_telephonenumber" value="<?php echo $config_gr_telephonenumber; ?>"></td>
                    </tr>
                  </table>
                  <table class="form">
                    <h2>Username Details - Will be used by different Snippet</h2>
                     <tr>
                      <td class="title"><?php echo $text_googlepageid;?></td>
                      <td valign="top" align="left"><img src="view/image/seo/googleplusicon.jpg"><input type="text" placeholder="Google Page Id" name="config_google_pageid" value="<?php echo $config_google_pageid; ?>"></td>
                      <td valign="top" align="left"> <div class="social-helper-msg"><?php echo $text_googlepageid_help; ?></div></td>
                    </tr>
                     <tr>
                      <td class="title"><?php echo $text_facebookadminid;?></td>
                      <td valign="top" align="left"><img src="view/image/seo/facebookicon.png"><input type="text" placeholder="Facebook Admin Id" name="config_facebook_adminid" value="<?php echo $config_facebook_adminid; ?>"></td>
                      <td valign="top" align="left"> <div class="social-helper-msg"><?php echo $text_facebookadminid_help; ?></div></td>
                    </tr>
                     <tr>
                      <td class="title"><?php echo $text_twitterusername;?></td>
                      <td valign="top" align="left"><img src="view/image/seo/twittericon.png"><input type="text" placeholder="Twitter UserName" name="config_twitter_uername" value="<?php echo $config_twitter_uername; ?>"></td>
                      <td valign="top" align="left"> <div class="social-helper-msg"><?php echo $text_twitterusername_help; ?></div></td>
                    </tr>
                  </table>
                  <table class="form">
                    <h2>Settings - Enable / Disable different Snippets</h2>
                    <tr>
                      <td class="title"><?php echo $entry_google_status; ?></td>
                      <td class="inputbox">
                        <select name="config_gr_status">
                          <?php if ($config_gr_status) { ?>
                            <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                            <option value="0"><?php echo $text_disabled; ?></option>
                          <?php } else { ?>
                            <option value="1"  ><?php echo $text_enabled; ?></option>
                            <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                          <?php }?>
                        </select> 
                      </td>
                      <td valign="top" align="left"> <div class="snippet-helper-msg"><?php echo $check_impact; ?></div></td>
                    </tr>
                    <tr>
                      <td class="title"><?php echo $entry_facebook_status; ?></td>
                      <td class="inputbox">
                        <select name="config_twitter_status">
                          <?php if ($config_twitter_status) { ?>
                            <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                            <option value="0"><?php echo $text_disabled; ?></option>
                          <?php } else { ?>
                            <option value="1"  ><?php echo $text_enabled; ?></option>
                            <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                          <?php } ?>
                        </select> 
                      </td>
                       <td valign="top" align="left"> <div class="snippet-helper-msg"><?php echo $check_impact; ?></div></td>
                    </tr>
                    <tr>
                      <td class="title"><?php echo $entry_twitter_status; ?></td>
                      <td class="inputbox">
                        <select name="config_facebook_status">
                          <?php if ($config_facebook_status) { ?>
                            <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                            <option value="0"><?php echo $text_disabled; ?></option>
                          <?php } else { ?>
                            <option value="1"  ><?php echo $text_enabled; ?></option>
                            <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                          <?php } ?>
                        </select> 
                      </td>
                       <td valign="top" align="left"> <div class="snippet-helper-msg"><?php echo $check_impact; ?></div></td>
                    </tr>
                  </table>
                  <div class="buttons">
          <a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a>
        </div>
          </div>
        <div id="tab-achieve">
            <div class="helper-msg">
              <?php echo $achieve_help; ?>
            </div>
             <table class="form">
                    <h2>What you achieve in Google search results</h2>
                    <tr>
                      <td class="title"><img src="view/image/seo/nerdherd seo result.png" alt="google achievement" title="what you will achieve in google"></td>
                      <td align="left"> <div class="helper-msg">Product rating and review</div><vr><div class="helper-msg">Google Plus Page Link</div></td>
                    </tr>
             </table>
             <table class="form">
                    <h2>What you achieve when someone tweets about your product page</h2>
                    <tr>
                      <td class="title"><img src="view/image/seo/twitter rich snippet.png" alt="Twitter achievement" title="What you will achieve when someoone tweets"></td>
                      <td align="left"> <div class="helper-msg">Details Info about product</div><vr><div class="helper-msg">Also a new look on smart phone</div></td>
                    </tr>
             </table>
             <table class="form">
                    <h2>What you achieve when someone shares on facebook</h2>
                    <tr>
                      <td class="title"><img src="view/image/seo/Facebook share.png" alt="Facebook achievement" title="What you will achieve when someoone shared product link"></td>
                      <td align="left"> <div class="helper-msg">Share post will also show image</div><vr><div class="helper-msg">Link to web site and actual link</div></td>
                    </tr>
             </table>
          </div>
          <div id="tab-verify">
            <div class="helper-msg">
              <?php echo $verify_help; ?>
            </div>
             <table class="form">
                    <h2>Google Structured Data Testing Tool</h2>
                    <tr>
                      <td class="title"><img src="view/image/seo/googleplusicon.jpg" alt="Google testing" title="what you will achieve in google"></td>
                      <td align="left"> <div class="helper-msg">For testing sign into google account and click this link: <a href="http://www.google.com/webmasters/tools/richsnippets" target="_blank">Google Tester Tool</a></div><vr><div class="helper-msg">You can see what types of meta data Google is able to extract from each page. Try  link below for testing<br> <b><a href="http://store.nerdherd.in/Opencart-Seo-Module" target="_blank">http://store.nerdherd.in/Opencart-Seo-Module</a></b></div></td>
                    </tr>
             </table>
             <table class="form">
                    <h2>Twiiter Product Cards Testing Tool</h2>
                    <tr>
                      <td class="title"><img src="view/image/seo/twittericon.png" alt="Twitter Testing" title="what you will achieve in twitter"></td>
                      <td align="left"> <div class="helper-msg">For testing sign into twitter account and click this link: <a href="https://dev.twitter.com/docs/cards/validation/validator" target="_blank">Twiiter Product Cards testing</a></div><vr><div class="helper-msg">Simply enter sample URL given below into the validation tool and click Go<br><b><a href="http://store.nerdherd.in/Opencart-Seo-Module" target="_blank">http://store.nerdherd.in/Opencart-Seo-Module</a></b></div></td>
                    </tr>
             </table>
             <table class="form">
                    <h2>Google Structured Data Testing Tool</h2>
                    <tr>
                      <td class="title"><img src="view/image/seo/facebookicon.png" alt="Facebook achievement" title="what you will achieve on facebook"></td>
                      <td align="left"> <div class="helper-msg">For testing sign into facebook account and click this link: <a href="https://developers.facebook.com/tools/debug" target="_blank">Facebook Debugger Tool</a></div><vr><div class="helper-msg">You can see what types of meta data facebook is able to extract from each page. Try  link below for testing<br> <b><a href="http://store.nerdherd.in/Opencart-Seo-Module" target="_blank">http://store.nerdherd.in/Opencart-Seo-Module</a></b></div></td>
                    </tr>
             </table>
          </div>
      </form>
  </div>
  <br>
   <div id="settingdialog"><?php echo $text_about; ?></div>
  </div>
  </div>
  <script type="text/javascript">
  $('#tabs a').tabs(); 
  </script>
  <script type="text/javascript">
$('#content #cssmenu ul li:nth-child(4)').addClass('active'); 
</script>
<script type="text/javascript" src="view/javascript/jquery/ui/external/bgiframe/helper.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#settingdialog').dialog({
        title: 'This page contains snippet setting used by search engines and social sites',
        bgiframe: false,
        width: 600,
        height: 150,
        resizable: false,
        modal: true,
        autoOpen: false
    });
});
</script>
<?php echo $footer; ?>