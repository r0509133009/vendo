<?php echo $header; ?>
<link rel='stylesheet' type='text/css' href='view/stylesheet/allseo.css' />
<style type="text/css">
select {
   font-size: 12px;
  padding: 4px;
    margin: 0;
    -webkit-border-radius:4px;
    -moz-border-radius:4px;
    border-radius:4px;
    background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #25AC1A), color-stop(1, #007F05) );
    background:-moz-linear-gradient( center top, #25AC1A 5%, #007F05 100% );
    filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#25AC1A', endColorstr='#007F05');
    background-color:#007F05;
    color: white;
    border:none;
    outline:none;
    display: inline-block;
    cursor:pointer;
    position: relative;

}
</style>
<div id="content">
  <div id="cssmenu">
    <ul>
    <?php foreach ($links as $link) { ?>
    <li><a class="top" href="<?php echo $link['href']; ?>"><?php echo $link['text']; ?></a></li>
    <?php } ?>
  </ul>
  </div>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/product.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a class="button green" onclick="$('#settingdialog').dialog('open');"> <span>Help guide</span></a><a onclick="addRow();" class="button darkgreen">+ <?php echo $button_insert; ?></a></a><a href="<?php echo $cancel; ?>" class="button crimson"><?php echo $button_cancel ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form" style="border-bottom:1px dotted black;">
          <tr>
            <td><?php echo $text_redirect_table; ?></td>
            <td>                
              <a href="<?php echo $redirect_table; ?>" class="button crimson"><?php echo $failed; ?></a>  
            </td>
          </tr>
          <tr>
            <td><?php echo $mstatus; ?></td>
            <td>                
              <select name="redirectstatus" onchange="javascript:handleSelect(this)">
                  <?php if ($redirectstatus) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
          </tr>
        </table>
        <table class="list">
          <thead>
            <tr>
               <td class="center" style="width:8%;"><?php if ($sort == 'times_used') { ?>
                <a href="<?php echo $sort_times_used; ?>" title="Number of times redirected"  class="<?php echo strtolower($order); ?>"><?php echo $times_used; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_times_used; ?>" title="Number of times redirected" ><?php echo $times_used; ?></a>
                <?php } ?>
              </td>
              <td class="center"><?php if ($sort == 'fromUrl') { ?>
                <a href="<?php echo $sort_fromUrl; ?>" title="The url which needs to be redirected" class="<?php echo strtolower($order); ?>"><?php echo $from_url; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_fromUrl; ?>" title="The url which needs to be redirected"><?php echo $from_url; ?></a>
                <?php } ?></td>
              <td class="center"><?php if ($sort == 'toUrl') { ?>
                <a href="<?php echo $sort_toUrl; ?>"  title="The target Url" class="<?php echo strtolower($order); ?>"><?php echo $to_url; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_toUrl; ?>" title="The target Url"><?php echo $to_url; ?></a>
                <?php } ?></td>
              <td class="center" title="Test Url"  style="width:8%;"><?php echo $url_test; ?></td>
              <td class="center" width="12" title="Delete"><?php echo $delete; ?></td>
              <td class="center" width="12"><?php if ($sort == 'status') { ?>
                <a href="<?php echo $sort_status; ?>" title="Enable and disable redirection individually" class="<?php echo strtolower($order); ?>"><?php echo $status; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_status; ?>" title="Enable and disable redirection individually"><?php echo $status; ?></a>
                <?php } ?></td>
              <td class="center ract" width="14" title="Save">Action</td>
            </tr>
          </thead>
          <tbody>
           <?php if($fromTable) { ?>
             <tr>
              <td>-</td>
              <td class="center"><textarea style="width:95%" name="redirectf-100"/><?php echo $fromTable; ?></textarea></td>
              <td class="center"><textarea style="width:95%" name="redirectt-100"/></textarea></td>
              <td class="center">-</td>
             <td class="center"><a onclick="$(this).parent().parent().remove()"><img src="view/image/error.png" title="Delete Redirect" /></a></td>
             <td class="center">
                <select name="redirects-100">
                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                    <option value="0"><?php echo $text_disabled; ?></option>
                  </select>
               </td>
             <td class="center"><a onclick="insert(-100)">Insert</a></td>
            </tr>
            <?php } ?>
            <?php if ($redirectlist) { ?>
            <?php foreach ($redirectlist as $redirect) { ?>
            <tr class="delete<?php echo $redirect['index']; ?>">
              <td class="center">
                <label class="resetvalue" name="resetvalue<?php echo $redirect['index']; ?>"><?php echo $redirect['times_used']; ?></label><br><br>
                <span><a onclick="reset1(<?php echo $redirect['index']; ?>)" class="button reset">Reset</a></span>
              </td>
              <td class="center"><textarea style="width:95%" name="redirectf<?php echo $redirect['index']; ?>"/><?php echo $redirect['fromUrl']; ?></textarea></td>
              <td class="center"><textarea style="width:95%" name="redirectt<?php echo $redirect['index']; ?>"/><?php echo $redirect['toUrl']; ?></textarea></td>
              <td class="center"><a href="<?php echo $redirect['fromUrl']; ?>" name="test<?php echo $redirect['index']; ?>" target="_blank" class="button deepskyblue"><span><?php echo $url_test_redirect; ?></span></a></td>
              <td class="center"><a onclick="delete1(<?php echo $redirect['index']; ?>)"><img src="view/image/error.png" title="Delete Redirect" /></a></td>
              <td class="center"><select name="redirects<?php echo $redirect['index']; ?>">
                    <?php if($redirect['status']) { ?>
                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                    <option value="0"><?php echo $text_disabled; ?></option>
                    <?php } else { ?>
                    <option value="1"><?php echo $text_enabled; ?></option>
                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                    <?php } ?>
                </select>
              </td>
              <td class="center"><a class="update<?php echo $redirect['index']; ?> ract" onclick="update(<?php echo $redirect['index']; ?>)">Update</a></td>
            </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
              <td class="center" colspan="8"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </form>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>
<div id="settingdialog">
    <?php echo $help; ?>
</div>
<script type="text/javascript">
newRow = -1;
  function addRow() {
    $('\
      <tr class="delete'+newRow+'">\
        <td class="center">Insert Details First</td>\
        <td class="center"><textarea style="width:95%" name="redirectf' + newRow + '"></textarea></td>\
        <td class="center"><textarea style="width:95%" name="redirectt' + newRow + '"></textarea></td>\
        <td class="center"><a href="" name="test' + newRow + '" ></a></td>\
        <td class="center"><a onclick="$(this).parent().parent().remove()"><img src="view/image/error.png" title="Delete Redirect" /></a></td>\
         <td class="center"><select name="redirects' + newRow + '"><option value="1" selected="selected"><?php echo $text_enabled; ?></option>\
                    <option value="0"><?php echo $text_disabled; ?></option></select>\
        </td>\
        <td class="center"><a class="insert'+newRow+' ract" onclick="insert('+newRow+')">Insert</a></td>\
      </tr>\
    ').insertBefore('table.list tbody tr:first');
    newRow--;
  };
</script>
<script>
function update(id) {
    var to = $('textarea[name = "redirectt'+id+'"]').val();
    var fr = $('textarea[name = "redirectf'+id+'"]').val();
    var s = $('select[name = "redirects'+id+'"]').val();
    if(to != '' && fr != '') {
      $.ajax({
        url: 'index.php?route=catalog/seomanager/update&token=<?php echo $token ?>',
        type: 'POST',
        data: { 'id': id, 'toUrl' : to,'fromUrl': fr, 'status' : s },
        success: function(data) {
              $('.update'+id+'').html('Updated').css('background-color','green');
              $('a[name="test'+id+'"]').attr("href",fr).html("Test Redirection");
        }
      });
    }
};

</script>
<script>
function insert(id) {
    var to = $('textarea[name = "redirectt'+id+'"]').val();
    var fr = $('textarea[name = "redirectf'+id+'"]').val();
    var s = $('select[name = "redirects'+id+'"]').val();
    if(to != '' && fr != '') {
      $.ajax({
        url: 'index.php?route=catalog/seomanager/inserting&token=<?php echo $token ?>',
        type: 'POST',
        data: { 'id': id, 'toUrl' : to,'fromUrl': fr, 'status' : s },
        success: function(data) {
              $('.insert'+id+'').html('Inserted').css('background-color','green');
              $('a[name="test'+id+'"]').attr("href",fr).html("Test Redirection");
        }
      });
    }  
};

</script>
<script type="text/javascript">
function handleSelect(elm) {
    $.ajax({
      url: 'index.php?route=catalog/seomanager/insert&token=<?php echo $token; ?>&value=' + elm.value,
      dataType: 'json',
      success: function(data) {
           console.log("Success");
      }
    });
};
</script>
<script>
function delete1(id) {
    $.ajax({
    url: 'index.php?route=catalog/seomanager/delete&token=<?php echo $token; ?>&id=' +  id,
    dataType: 'json',
    success: function(data) {
          $('.delete'+id+'').remove();
    }
  });
}
</script>
<script>
function reset1(id) {
  $.ajax({
    url: 'index.php?route=catalog/seomanager/reset&token=<?php echo $token; ?>&id=' +  id,
    dataType: 'json',
    success: function(data) {
           $('label[name="resetvalue'+id+'"]').html("0");
    }
  });
};
</script>
<script type="text/javascript">
$('#content #cssmenu ul li:nth-child(7)').addClass('active'); 
</script>
<script type="text/javascript" src="view/javascript/jquery/ui/external/bgiframe/helper.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#settingdialog').dialog({
        title: "This page contains a manager waiting for adding redirection of failed url's",
        bgiframe: false,
        width: 600,
        height: 300,
        resizable: false,
        modal: true,
        autoOpen: false
    });
});
</script>
<?php echo $footer; ?>