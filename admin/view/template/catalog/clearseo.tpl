<?php echo $header; ?>
<link rel='stylesheet' type='text/css' href='view/stylesheet/allseo.css' />
<div id="content">
   <div id="cssmenu">
    <ul>
    <?php foreach ($links as $link) { ?>
    <li><a class="top" href="<?php echo $link['href']; ?>"><?php echo $link['text']; ?></a></li>
    <?php } ?>
  </ul>
  </div>
  <div class="box">
     <div class="left"></div>
    <div class="right"></div>
    <div class="heading">
      <h1><img src="view/image/delete.png" alt="" /> <?php echo $heading_title; ?></h1>
     <div class="buttons"><a class="button green" onclick="$('#settingdialog').dialog('open');"> <span>Help guide</span></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a></div>
    </div>
    <div class="content">
             <div class="othersetting clearseo">
            <div class="helper-msg">
             Each Seo data can be individually be cleared as per the store requirements.
            </div>
                   <table class="form">
                            <tbody>
                              <tr><td colspan="2"><h2>Product Seo details Clear</h2></td></tr>
                               <tr>
                                <td colspan="2"><button class="productseokeyword">Clear Product Seo Keywords</button></td>
                              </tr>
                              <tr>
                                <td colspan="2"><button class="producttitle">Clear Product Title</button></td>
                              </tr>
                              <tr>
                                <td colspan="2"><button class="productmetakeyword">Clear Product Meta Keywords</button></td>
                              </tr>
                               <tr>
                                <td colspan="2"><button class="productmetadescription">Clear Product Meta Description</button></td>
                              </tr>
                               <tr>
                                <td colspan="2"><button class="producttags">Clear Product Tags</button></td>
                              </tr>
                              <tr><td colspan="2"><h2>Category Seo details Clear</h2></td></tr>
                               <tr>
                                <td colspan="2"><button class="categoryseokeyword">Clear Category Seo Keywords</button></td>
                              </tr>
                               <tr>
                                <td colspan="2"><button class="categorytitle">Clear Category Title</button></td>
                              </tr>
                              <tr>
                                <td colspan="2"><button class="categorymetakeyword">Clear Category Meta Keywords</button></td>
                              </tr>
                               <tr>
                                <td colspan="2"><button class="categorymetadescription">Clear Category Meta Description</button></td>
                              </tr>
                              <tr><td colspan="2"><h2>Manufacturer Seo details Clear</h2></td></tr>
                              <tr>
                                <td colspan="2"><button class="manufacturerseokeyword">Clear Manufacturer Seo Keywords</button></td>
                              </tr>
                               <tr>
                                <td colspan="2"><button class="manufacturertitle">Clear Manufacturer Title</button></td>
                              </tr>
                              <tr>
                                <td colspan="2"><button class="manufacturermetakeyword">Clear Manufacturer Meta Keywords</button></td>
                              </tr>
                               <tr>
                                <td colspan="2"><button class="manufacturermetadescription">Clear Manufacturer Meta Description</button></td>
                              </tr>
                              <tr><td colspan="2"><h2>Information Seo details Clear</h2></td></tr>
                              <tr>
                                <td colspan="2"><button class="informationseokeyword">Clear Information Seo Keywords</button></td>
                              </tr>
                               <tr>
                                <td colspan="2"><button class="informationtitle">Clear Information Title</button></td>
                              </tr>
                              <tr>
                                <td colspan="2"><button class="informationmetakeyword">Clear Information Meta Keywords</button></td>
                              </tr>
                               <tr>
                                <td colspan="2"><button class="informationmetadescription">Clear Information Meta Description</button></td>
                              </tr>
                        </tbody>
                      </table>
                </div>
        </div>
</div>
</div>
<div id="settingdialog">
    <?php echo $help; ?>
</div>
<script type="text/javascript">
$(".clearseo button").addClass("red");
$('.clearseo button').click(function(){
   if (confirm('Are you sure you want to delete this?')) {
      $(this).removeClass("red");
      var name = $(this).attr("class");
      $.ajax({
          url: 'index.php?route=catalog/clearseo/deletedata&token=<?php echo $token; ?>&name=' +  name,
          dataType: 'json',
          success: function(data) {
              $('.'+name).addClass("greens");
          }
        });
  } 
});
</script>
<script type="text/javascript">
$('#content #cssmenu ul li:nth-child(6)').addClass('active'); 
</script>
<script type="text/javascript" src="view/javascript/jquery/ui/external/bgiframe/helper.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#settingdialog').dialog({
        title: "This page contains a tool for clearing seo data individually if required",
        bgiframe: false,
        width: 600,
        height: 150,
        resizable: false,
        modal: true,
        autoOpen: false
    });
});
</script>
<?php echo $footer; ?>