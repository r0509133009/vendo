<?php echo $header; ?>
<link rel='stylesheet' type='text/css' href='view/stylesheet/allseo.css' />
<div id="content">
   <div id="cssmenu">
    <ul>
    <?php foreach ($links as $link) { ?>
    <li><a class="top" href="<?php echo $link['href']; ?>"><?php echo $link['text']; ?></a></li>
    <?php } ?>
  </ul>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if (isset($success) && $success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="generate-helper-msg">
      <?php echo $autogenerate_help; ?>
  </div>
  <div class="box">
    <div class="left"></div>
    <div class="right"></div>
    <div class="heading">
      <h1><img src="view/image/product.png" alt="" /> <?php echo $heading_title; ?></h1>
     <div class="buttons"><a class="button green" onclick="$('#settingdialog').dialog('open');"> <span>Help guide</span></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a></div>
    </div>
    <div class="content">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
          <h1>Seo Report For Products</h1>
          <table class="CSSTableGenerator">
            <tr>
              <td>Current Details</td>
              <td> Total Product - <?php echo $productreport['count']; ?></td>
              <?php if($productreport['seok'] == 0) { ?>
              <td class="tbgreen"> Seo Keywords Found Empty - <?php echo $productreport['seok']; ?></td>
              <?php } else { ?>
              <td class="tbred"> Seo Keywords Found Empty - <?php echo $productreport['seok']; ?></td>
              <?php } ?>
               <?php if($productreport['metak'] == 0) { ?>
              <td class="tbgreen"> Meta Keywords Found Empty - <?php echo $productreport['metak']; ?></td>
              <?php } else { ?>
              <td class="tbred"> Meta Keywords Found Empty - <?php echo $productreport['metak']; ?></td>
              <?php } ?>
               <?php if($productreport['metad'] == 0) { ?>
              <td class="tbgreen"> Meta Description Found Empty - <?php echo $productreport['metad']; ?></td>
              <?php } else { ?>
             <td class="tbred"> Meta Description Found Empty - <?php echo $productreport['metad']; ?></td>
              <?php } ?>
            </tr>
          </table>
          <h1>Generate Product Seo Below</h1>
          <table class="list store_seo">
            <thead>
              <tr>
                <td class="left description" ><?php echo $text_description; ?></td>
                <td class="left pattern"><?php echo $text_pattern; ?></td>
                <td class="left action"><?php echo $text_action; ?></td>
              </tr>
            </thead>  
            <tbody>
              <tr>
                <td class="help_seo">
                  <?php echo $text_product_url_keyword; ?>
                </td>
                <td><input type="text" id="products_url_template" name="products_url_template" value="<?php echo $products_url_template;?>" size="80" class="blueprint"><br> <div class="pattern-helper-msg"><?php echo $help_product_seo_description; ?></div></td>
                <td><div class="buttons"><button type="submit" name="products_url" value="products_url" class="button button_adjust generate"><span><?php echo $generate;?> Product Url</span></button></div></td>
              </tr>
              <tr class="bb2">
                <td class="help_seo">
                  <?php echo $text_product_title; ?>
                </td>
                <td><input type="text" id="products_title_template" name="products_title_template" value="<?php echo $products_title_template;?>" size="80" class="blueprint"><br> <div class="pattern-helper-msg"><?php echo $help_product_title; ?></div></td>
                <td><div class="buttons"><button type="submit" name="products_title" value="products_title" class="button button_adjust generate"><span><?php echo $generate;?> Product Title</span></button></div></td>
              </tr>
              <tr>
                <td class="help_seo">
                  <?php echo $text_product_meta_keywords; ?>
                </td>
                <td>
                  <input type="text" id="product_keywords_template" name="product_keywords_template" value="<?php echo $product_keywords_template;?>" size="80" class="blueprint"><br> <div class="pattern-helper-msg"><?php echo $help_meta_keywords; ?></div>
                </td>
                <td><div class="buttons"><button type="submit" name="product_keywords" value="product_keywords" class="button button_adjust generate"><span><?php echo $generate;?> Product Meta keywords</span></button></div></td>
              </tr>
              <tr>
                <td class="help_seo">
                  <?php echo $text_product_meta_description; ?>
                </td>
                <td><input type="text" id="product_description_template" name="product_description_template" value="<?php echo $product_description_template;?>" size="80" class="blueprint"><br> <div class="pattern-helper-msg"><?php echo $help_product_description; ?></div></td>
                <td><div class="buttons"><button type="submit" name="product_description" value="product_description" class="button button_adjust generate"><span><?php echo $generate;?> Product Meta Description</span></button></div></td>
              </tr>
              <tr>
                <td class="help_seo">
                  <?php echo $text_tags; ?>
                </td>
                <td><input type="text" id="product_tags_template" name="product_tags_template" value="<?php echo $product_tags_template;?>" size="80" class="blueprint"><br> <div class="pattern-helper-msg"><?php echo $help_product_tags; ?></div></td>
                <td><div class="buttons"><button type="submit" name="product_tags" value="product_tags" class="button button_adjust generate"><span><?php echo $generate;?> Product Tags</span></button></div></td>
              </tr>
              <tr>
                <td class="help_seo">
                  <?php echo $text_image_name; ?>
                </td>
                <td><input type="text" id="product_image_template" name="product_image_template" value="<?php echo $product_image_template;?>" size="80" class="blueprint"><br> <div class="pattern-helper-msg"><?php echo $help_product_image_description; ?></div></td>
                <td><div class="buttons"><button type="submit" name="product_image" value="product_image" class="button button_adjust generate"><span><?php echo $generate;?> Product SEO image name</span></button></div></td>
              </tr>
              </tbody>
           </table>
          <h1>Seo Report For Categories</h1>
          <table class="CSSTableGenerator">
             <tr>
              <td>Current Details</td>
              <td> Total Categories - <?php echo $catreport['count']; ?></td>
              <?php if($catreport['seok'] == 0) { ?>
              <td class="tbgreen"> Seo Keywords Found Empty - <?php echo $catreport['seok']; ?></td>
              <?php } else { ?>
              <td class="tbred"> Seo Keywords Found Empty - <?php echo $catreport['seok']; ?></td>
              <?php } ?>
               <?php if($catreport['metak'] == 0) { ?>
              <td class="tbgreen"> Meta Keywords Found Empty - <?php echo $catreport['metak']; ?></td>
              <?php } else { ?>
              <td class="tbred"> Meta Keywords Found Empty - <?php echo $catreport['metak']; ?></td>
              <?php } ?>
               <?php if($catreport['metad'] == 0) { ?>
              <td class="tbgreen"> Meta Description Found Empty - <?php echo $catreport['metad']; ?></td>
              <?php } else { ?>
             <td class="tbred"> Meta Description Found Empty - <?php echo $catreport['metad']; ?></td>
              <?php } ?>
            </tr>
          </table>
          <h1>Generate Category Seo Below</h1>
           <table class="list store_seo">
            <thead>
              <tr>
                <td class="left description"><?php echo $text_description; ?></td>
                <td class="left pattern"><?php echo $text_pattern; ?></td>
                <td class="left action"><?php echo $text_action; ?></td>
              </tr>
            </thead>  
            <tbody>
              <tr>
                <td class="help_seo">
                  <?php echo $text_category_url_keyword; ?>
                </td>
                <td><input type="text" id="categories_url_template" name="categories_url_template" value="<?php echo $categories_url_template;?>" size="80" class="blueprint"><br> <div class="pattern-helper-msg"><?php echo $help_category_seo_description; ?></div></td>
                <td><div class="buttons"><button type="submit" name="categories_url" value="categories_url" class="button button_adjust generate"><span><?php echo $generate;?> Category Url</span></button></div></td>
              </tr>
              <tr class="bb2">
                <td class="help_seo">
                  <?php echo $text_category_title; ?>
                </td>
                <td><input type="text" id="categories_title_template" name="categories_title_template" value="<?php echo $categories_title_template;?>" size="80" class="blueprint"><br><div class="pattern-helper-msg"><?php echo $help_category_title; ?></div></td>
                <td><div class="buttons"><button type="submit" name="categories_title" value="categories_title" class="button button_adjust generate"><span><?php echo $generate;?> Category Title</span></button></div></td>
              </tr>
              <tr class="bb2">
                <td class="help_seo">
                 <?php echo $text_category_meta_keywords; ?>
                </td>
                <td><input type="text" id="categories_keyword_template" name="categories_keyword_template" value="<?php echo $categories_keyword_template;?>" size="80" class="blueprint"><br><div class="pattern-helper-msg"><?php echo $help_category_meta_keyword; ?></div></td>
                <td><div class="buttons"><button type="submit" name="categories_keyword" value="categories_keyword" class="button button_adjust generate"><span><?php echo $generate;?> Category Meta Keywords</span></button></div></td>
              </tr>
              <tr class="bb2">
                <td class="help_seo">
                  <?php echo $text_category_meta_description; ?>
                </td>
                <td><input type="text" id="category_description_template" name="category_description_template" value="<?php echo $category_description_template;?>" size="80" class="blueprint"><br><div class="pattern-helper-msg"><?php echo $help_category_description; ?></div></td>
                <td><div class="buttons"><button type="submit" name="category_description" value="category_description" class="button button_adjust generate"><span><?php echo $generate;?> Category Meta Description</span></button></div></td>
              </tr>
           </tbody>
           </table>
           <h1>Seo Report For Manufacturer</h1>
          <table class="CSSTableGenerator">
             <tr>
              <td>Current Details</td>
              <td> Total Manufacturer - <?php echo $manreport['count']; ?></td>
              <?php if($manreport['seok'] == 0) { ?>
              <td class="tbgreen"> Seo Keywords Found Empty - <?php echo $manreport['seok']; ?></td>
              <?php } else { ?>
              <td class="tbred"> Seo Keywords Found Empty - <?php echo $manreport['seok']; ?></td>
              <?php } ?>
               <?php if($manreport['metak'] == 0) { ?>
              <td class="tbgreen"> Meta Keywords Found Empty - <?php echo $manreport['metak']; ?></td>
              <?php } else { ?>
              <td class="tbred"> Meta Keywords Found Empty - <?php echo $manreport['metak']; ?></td>
              <?php } ?>
               <?php if($manreport['metad'] == 0) { ?>
              <td class="tbgreen"> Meta Description Found Empty - <?php echo $manreport['metad']; ?></td>
              <?php } else { ?>
             <td class="tbred"> Meta Description Found Empty - <?php echo $manreport['metad']; ?></td>
              <?php } ?>
            </tr>
          </table>
          <h1>Generate Manufacturer Seo Below</h1>
           <table class="list store_seo">
            <thead>
              <tr>
                <td class="left description"><?php echo $text_description; ?></td>
                <td class="left pattern"><?php echo $text_pattern; ?></td>
                <td class="left action"><?php echo $text_action; ?></td>
              </tr>
            </thead>  
            <tbody>
              <tr class="bb2">
                <td class="help_seo">
                  <?php echo $text_manufacturer_url_keyword; ?>
                </td>
                <td><input type="text" id="manufacturers_url_template" name="manufacturers_url_template" value="<?php echo $manufacturers_url_template;?>" size="80" class="blueprint"><br><div class="pattern-helper-msg"><?php echo $help_manufacturer_seo_description; ?></div></td>
                <td><div class="buttons"><button type="submit" name="manufacturers_url" value="manufacturers_url" class="button button_adjust generate"><span><?php echo $generate;?> Manufacturer Url</span></button></div></td>
              </tr>
               <tr class="bb2">
                <td class="help_seo">
                  <?php echo $text_manufacturer_title; ?>
                </td>
                <td><input type="text" id="manufacturer_title_template" name="manufacturer_title_template" value="<?php echo $manufacturer_title_template;?>" size="80" class="blueprint"><br><div class="pattern-helper-msg"><?php echo $help_manufacturer_title; ?></div></td>
                <td><div class="buttons"><button type="submit" name="manufacturer_title" value="manufacturer_title" class="button button_adjust generate"><span><?php echo $generate;?> Manufacturer Title</span></button></div></td>
              </tr>
              <tr class="bb2">
                <td class="help_seo">
                 <?php echo $text_manufacturer_meta_keywords; ?>
                </td>
                <td><input type="text" id="manufacturer_keyword_template" name="manufacturer_keyword_template" value="<?php echo $manufacturer_keyword_template;?>" size="80" class="blueprint"><br><div class="pattern-helper-msg"><?php echo $help_manufacturer_meta_keyword; ?></div></td>
                <td><div class="buttons"><button type="submit" name="manufacturer_keyword" value="manufacturer_keyword" class="button button_adjust generate"><span><?php echo $generate;?> Manufacturer Meta Keywords</span></button></div></td>
              </tr>
              <tr class="bb2">
                <td class="help_seo">
                  <?php echo $text_manufacturer_meta_description; ?>
                </td>
                <td><input type="text" id="manufacturer_description_template" name="manufacturer_description_template" value="<?php echo $manufacturer_description_template;?>" size="80" class="blueprint"><br><div class="pattern-helper-msg"><?php echo $help_manufacturer_description; ?></div></td>
                <td><div class="buttons"><button type="submit" name="manufacturer_description" value="manufacturer_description" class="button button_adjust generate"><span><?php echo $generate;?> manufacturer Meta Description</span></button></div></td>
              </tr>
            </tbody>
           </table>
           <h1>Seo Report For Information Page</h1>
          <table class="CSSTableGenerator">
            <tr>
              <td>Current Details</td>
              <td> Total Information - <?php echo $inforeport['count']; ?></td>
              <?php if($inforeport['seok'] == 0) { ?>
              <td class="tbgreen"> Seo Keywords Found Empty - <?php echo $inforeport['seok']; ?></td>
              <?php } else { ?>
              <td class="tbred"> Seo Keywords Found Empty - <?php echo $inforeport['seok']; ?></td>
              <?php } ?>
               <?php if($inforeport['metak'] == 0) { ?>
              <td class="tbgreen"> Meta Keywords Found Empty - <?php echo $inforeport['metak']; ?></td>
              <?php } else { ?>
              <td class="tbred"> Meta Keywords Found Empty - <?php echo $inforeport['metak']; ?></td>
              <?php } ?>
               <?php if($inforeport['metad'] == 0) { ?>
              <td class="tbgreen"> Meta Description Found Empty - <?php echo $inforeport['metad']; ?></td>
              <?php } else { ?>
             <td class="tbred"> Meta Description Found Empty - <?php echo $inforeport['metad']; ?></td>
              <?php } ?>
            </tr>
          </table>
          <h1>Generate Information Page Seo Below</h1>
           <table class="list store_seo">
            <thead>
              <tr>
                <td class="left description"><?php echo $text_description; ?></td>
                <td class="left pattern"><?php echo $text_pattern; ?></td>
                <td class="left action"><?php echo $text_action; ?></td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td class="help_seo">
                  <?php echo $text_information_url_keyword; ?>
                </td>
                <td><input type="text" id="information_pages_template" name="information_pages_template" value="<?php echo $information_pages_template;?>" size="80" class="blueprint"><br><div class="pattern-helper-msg"><?php echo $help_information_seo_description; ?></div></td>
                <td><div class="buttons"><button type="submit" name="information_pages" value="information_pages" class="button button_adjust generate"><span><?php echo $generate;?> Information Url</span></button></div></td>
              </tr>
 
              <tr class="bb2">
                <td class="help_seo">
                  <?php echo $text_information_title; ?>
                </td>
                <td><input type="text" id="information_pages_title_template" name="information_pages_title_template" value="<?php echo $information_pages_title_template;?>" size="80" class="blueprint"><br><div class="pattern-helper-msg"><?php echo $help_information_title; ?></div></td>
                <td><div class="buttons"><button type="submit" name="information_pages_title" value="information_pages_title" class="button button_adjust generate"><span><?php echo $generate;?> Information Title</span></button></div></td>
              </tr>
              <tr class="bb2">
                <td class="help_seo">
                 <?php echo $text_information_meta_keywords; ?>
                </td>
                <td><input type="text" id="information_keyword_template" name="information_keyword_template" value="<?php echo $information_keyword_template;?>" size="80" class="blueprint"><br><div class="pattern-helper-msg"><?php echo $help_information_meta_keyword; ?></div></td>
                <td><div class="buttons"><button type="submit" name="information_keyword" value="information_keyword" class="button button_adjust generate"><span><?php echo $generate;?> Information Meta Keywords</span></button></div></td>
              </tr>
              <tr class="bb2">
                <td class="help_seo">
                  <?php echo $text_information_meta_description; ?>
                </td>
                <td><input type="text" id="information_description_template" name="information_description_template" value="<?php echo $information_description_template;?>" size="80" class="blueprint"><br><div class="pattern-helper-msg"><?php echo $help_information_description; ?></div></td>
                <td><div class="buttons"><button type="submit" name="information_description" value="information_description" class="button button_adjust generate"><span><?php echo $generate;?> Information Meta Description</span></button></div></td>
              </tr>
            </tbody>
          </table>
          <table class="list store_seo">
            <thead>
              <tr>
                <td class="left description"><?php echo $text_description; ?></td>
                <td class="left pattern"><?php echo $text_pattern; ?></td>
                <td class="left action"><?php echo $text_action; ?></td>
              </tr>
            </thead>
            <tbody>
              <h1>Generate General Pages Seo Data</h1>
              <tr>
                <td class="help_seo">
                  <?php echo $text_general_data; ?>
                </td>
                <td><?php echo $help_general_data; ?><div class="pattern-helper-msg"><?php echo $help_general1_data; ?></div></td>
                <td><div class="buttons"><button type="submit" name="general_pages" value="general_pages" class="button button_adjust generate"><span><?php echo $generate;?> General Page Seo </span></button></div></td>
              </tr>
            </tbody>
          </table>
        </form>
    </div>
</div>
</div>
<div id="settingdialog">
    <?php echo $helpauto; ?>
</div>
<script type="text/javascript">
  $(document).ready(function(){
      $(".clearSeoButton button").click(function(){
          var me = $(this);
          if (!confirm('Are you sure you want to delete this?')) {
              return false;
          } else {
            $('#seo_clear').val(me.val());
            $('#form').submit();
          }
      }); 
     $('input[name=\'config_multilang_on\']').click(function() {
      var temp =  $('input[name=\'config_multilang_on\']:checked').val();
      $.ajax({
      url: 'index.php?route=catalog/seo/multisetting&token=<?php echo $token; ?>&config_multilang_on=' + temp,
      dataType: 'json',
      success: function(data) { 
      }
    });  
     });
  });
</script>
<script type="text/javascript">
$('#content #cssmenu ul li:nth-child(1)').addClass('active'); 
</script>
<script type="text/javascript" src="view/javascript/jquery/ui/external/bgiframe/helper.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#settingdialog').dialog({
        title: 'This page contains complete set of seo generators',
        bgiframe: false,
        width: 600,
        height: 360,
        resizable: false,
        modal: true,
        autoOpen: false
    });
});
</script>
<?php echo $footer; ?>