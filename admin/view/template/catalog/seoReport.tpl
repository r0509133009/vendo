<?php echo $header; ?>
<link rel='stylesheet' type='text/css' href='view/stylesheet/allseo.css' />
<div id="content">
   <div id="cssmenu">
    <ul>
    <?php foreach ($links as $link) { ?>
    <li><a class="top" href="<?php echo $link['href']; ?>"><?php echo $link['text']; ?></a></li>
    <?php } ?>
  </ul>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/report.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a class="button green" onclick="$('#settingdialog').dialog('open');"> <span>Help guide</span></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <div class="helper-msg">Click below to see if your Store Seo gets positive results.</div>
      <div class="sbutton"><a onclick="getreport();"><button class="mbutton"><?php echo $create_report; ?></button></a></div>
      <div class="report">
            <table border="5"    width="90%"   cellpadding="4" cellspacing="3">
               <tr class="main">
                  <th colspan="4"><br><h1>Products Report</h1>
                  </th>
               </tr>
               <tr>
                  <th><?php echo $total; ?> products found</th>
                  <th><?php echo $sd; ?></th>
                  <th><?php echo $md; ?></th>
                  <th><?php echo $mk; ?></th>
               </tr>
               <tr align="center" class="product">
                   <td class="p"><h3>Data 1</h3></td>
                  <td class="s"><h3>Data 1</h3></td>
                  <td class="d"><h3>Data 1</h3></td>
                  <td class="k"><h3>Data 1</h3></td>
               </tr>
            </table>
            <table border="5"    width="90%"   cellpadding="4" cellspacing="3">
               <tr class="main">
                  <th colspan="4"><br><h1>Category Report</h1>
                  </th>
               </tr>
               <tr>
                  <th><?php echo $total; ?> Categories found</th>
                  <th><?php echo $sd; ?></th>
                  <th><?php echo $md; ?></th>
                  <th><?php echo $mk; ?></th>
               </tr>
               <tr align="center" class="category">
                   <td class="p"><h3>Data 1</h3></td>
                   <td class="s"><h3>Data 1</h3></td>
                  <td class="d"><h3>Data 1</h3></td>
                  <td class="k"><h3>Data 1</h3></td>
               </tr>
            </table>
            <table border="5"    width="90%"   cellpadding="4" cellspacing="3">
               <tr class="main">
                  <th colspan="4"><br><h1>Information Report</h1>
                  </th>
               </tr>
               <tr>
                  <th><?php echo $total; ?> Information Pages</th>
                  <th><?php echo $sd; ?></th>
                  <th><?php echo $md; ?></th>
                  <th><?php echo $mk; ?></th>
               </tr>
               <tr align="center" class="information">
                   <td class="p"><h3>Data 1</h3></td>
                   <td class="s"><h3>Data 1</h3></td>
                  <td class="d"><h3>Data 1</h3></td>
                  <td class="k"><h3>Data 1</h3></td>
               </tr>
            </table>
            <table border="5"    width="90%"   cellpadding="4" cellspacing="3">
               <tr class="main">
                  <th colspan="4"><br><h1>Manufacturer Report</h1>
                  </th>
               </tr>
               <tr>
                  <th><?php echo $total; ?> Manufacturers Available</th>
                  <th><?php echo $sd; ?></th>
                  <th><?php echo $md; ?></th>
                  <th><?php echo $mk; ?></th>
               </tr>
               <tr align="center" class="manufacturer">
                   <td class="p"><h3>Data 1</h3></td>
                   <td class="s"><h3>Data 1</h3></td>
                  <td class="d"><h3>Data 1</h3></td>
                  <td class="k"><h3>Data 1</h3></td>
               </tr>
            </table>
             <table border="5"    width="90%"   cellpadding="4" cellspacing="3">
               <tr class="main">
                  <th colspan="3"><br><h1>Sitemap.xml</h1><br><h3><?php echo $sitemap; ?></h3>
                  </th>
               </tr>
               <tr>
                  <th>Status</th>
                  <th class="ss">Solution</th>
               </tr>
               <tr align="center" class="sitemap">
                   <td class="d"><h4>Sitemap.xml was not found.</h4></td>
                  <td class="so"><h4><?php echo $sitemapso; ?></h4></td>
               </tr>
            </table>
      </div>
    </div>
  </div>
</div>
<div id="settingdialog">
    <?php echo $seordata; ?>
</div>
<script>
$('.report').hide();
var tips = '<?php echo $sitemapt; ?>'; 

function getreport() {

    $.ajax({
    url: 'index.php?route=catalog/seoReport/getreport&token=<?php echo $token; ?>',
    dataType: 'json',
    beforeSend: function() {
    $('.sbutton').after('<img src="../catalog/view/theme/default/image/loading.gif" class="loading" style="padding-left: 5px;" />');
    $('.sbutton').hide();
    $('.helper-msg').hide();
    },
    complete: function() {
      $('.report').show(500);
      $('.loading').hide();
    },
    success: function(data) {
      console.log(data);
       var html = '';
       var p = data.products;
       var c = data.categories;
       var i = data.information;
       var m = data.manufacturer;
       var s = data.sitemap;
       var r = data.robots;
       $('.product .p h3').html(p.count);
       $('.product .s h3').html(p.seok+' Found Empty');
       $('.product .d h3').html(p.metad+' Found Empty');
       $('.product .k h3').html(p.metak+' Found Empty');
       $('.category .p h3').html(c.count);
       $('.category .s h3').html(c.seok+' Found Empty');
       $('.category .d h3').html(c.metad+' Found Empty');
       $('.category .k h3').html(c.metak+' Found Empty');
       $('.information .p h3').html(i.count);
       $('.information .s h3').html(i.seok+' Found Empty');
       $('.information .d h3').html(i.metad+' Found Empty');
       $('.information .k h3').html(i.metak+' Found Empty');
       $('.manufacturer .p h3').html(m.count);
       $('.manufacturer .s h3').html(m.seok+' Found Empty');
       $('.manufacturer .d h3').html(m.metad+' Found Empty');
       $('.manufacturer .k h3').html(m.metak+' Found Empty');
       if(p.metad == 0) {
        $('.product .d h3').append(' ✔');
        $('.product .d').removeClass("d").addClass("g");
       } else {
        $('.product .d h3').append(' ✘');
       }
       if(p.seok == 0) {
        $('.product .s h3').append(' ✔');
        $('.product .s').removeClass("s").addClass("g");
       } else {
        $('.product .s h3').append(' ✘');
       }
       if(c.seok == 0) {
        $('.category .s h3').append(' ✔');
        $('.category .s').removeClass("s").addClass("g");
       } else {
        $('.category .s h3').append(' ✘');
       }
       if(i.seok == 0) {
        $('.information .s h3').append(' ✔');
        $('.information .s').removeClass("s").addClass("g");
       } else {
        $('.information .s h3').append(' ✘');
       }
       if(m.seok == 0) {
        $('.manufacturer .s h3').append(' ✔');
        $('.manufacturer .s').removeClass("s").addClass("g");
       } else {
        $('.manufacturer .s h3').append(' ✘');
       }
        if(i.metad == 0) {
        $('.information .d h3').append(' ✔');
        $('.information .d').removeClass("d").addClass("g");
       }else {
        $('.information .d h3').append(' ✘');
       }
        if(c.metad == 0) {
        $('.category .d h3').append(' ✔');
        $('.category .d').removeClass("d").addClass("g");
       } else {
        $('.category .d h3').append(' ✘');
       }
        if(m.metad == 0) {
          $('.manufacturer .d h3').append(' ✔');
        $('.manufacturer .d').removeClass("d").addClass("g");
       } else {
        $('.manufacturer .d h3').append(' ✘');
       }
       if(p.metak == 0) {
        $('.product .k h3').append(' ✔');
        $('.product .k').removeClass("k").addClass("g");
       }else {
        $('.product .k h3').append(' ✘');
       }
        if(i.metak == 0) {
          $('.information .k h3').append(' ✔');
        $('.information .k').removeClass("k").addClass("g");
       }else {
        $('.information .k h3').append(' ✘');
       }
        if(c.metak == 0) {
          $('.category .k h3').append(' ✔');
        $('.category .k').removeClass("k").addClass("g");
       }else {
        $('.category .k h3').append(' ✘');
       }
        if(m.metak == 0) {
          $('.manufacturer .k h3').append(' ✔');
        $('.manufacturer .k').removeClass("k").addClass("g");
       }else {
        $('.manufacturer .k h3').append(' ✘');
       }
       if(s){
        $('.ss').html('Tip');
        $('.sitemap .so h4').html(tips);
        $('.sitemap .d h4').html('Sitemap.xml is present');
        $('.sitemap .d').removeClass("d").addClass("g");
       }
     }
  });  
}
</script>
<script type="text/javascript">
$('#content #cssmenu ul li:nth-child(3)').addClass('active'); 
</script>
<script type="text/javascript" src="view/javascript/jquery/ui/external/bgiframe/helper.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#settingdialog').dialog({
        title: 'This page contains a doctor for seo health checkup',
        bgiframe: false,
        width: 600,
        height: 150,
        resizable: false,
        modal: true,
        autoOpen: false
    });
});
</script>
<?php echo $footer; ?>