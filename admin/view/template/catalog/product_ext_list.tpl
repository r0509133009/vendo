<?php echo $header; ?>
<!-- display length configuration -->
<div class="modal fade" id="iDisplayLengthModal" tabindex="-1" role="dialog" aria-labelledby="iDisplayLengthModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="iDisplayLengthModalLabel"><?php echo $text_items_per_page; ?></h4>
      </div>
      <div class="modal-body aqe-container">
        <div class="aqe-overlay fade">
          <div class="aqe-tbl">
            <div class="aqe-tbl-cell"><i class="fa fa-refresh fa-spin fa-5x text-muted"></i></div>
          </div>
        </div>
        <div class="notice">
        </div>
        <form method="post" action="<?php echo $settings; ?>" class="form-horizontal ajax-form" role="form" id="iDisplayLengthForm">
          <fieldset>
            <div class="form-group">
              <label for="iDisplayLength" class="col-sm-4 control-label"><?php echo $entry_products_per_page; ?></label>
              <div class="col-sm-2">
                <input type="text" class="form-control" name="aqe_items_per_page" id="iDisplayLength" value="<?php echo $items_per_page; ?>">
              </div>
              <div class="col-sm-offset-4 col-sm-8 error-container">
              </div>
            </div>
            <span class="help-block"><?php echo $help_items_per_page; ?></span>
          </fieldset>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default cancel" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo $button_close; ?></button>
        <button type="button" class="btn btn-primary submit" data-form="#iDisplayLengthForm"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- confirm deletion -->
<div class="modal fade" id="confirmDelete" tabindex="-1" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="confirmDeleteLabel"><?php echo $text_confirm_delete; ?></h4>
      </div>
      <div class="modal-body">
        <p><?php echo $text_are_you_sure; ?></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-ban"></i> <?php echo $button_cancel; ?></button>
        <button type="button" class="btn btn-danger delete"><i class="fa fa-trash-o"></i> <?php echo $button_delete; ?></button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- column settings -->
<div class="modal fade" id="pageColumnsModal" tabindex="-1" role="dialog" aria-labelledby="pageColumnsModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="pageColumnsModalLabel"><?php echo $text_choose_columns; ?></h4>
      </div>
      <div class="modal-body aqe-container">
        <div class="aqe-overlay fade">
          <div class="aqe-tbl">
            <div class="aqe-tbl-cell"><i class="fa fa-refresh fa-spin fa-5x text-muted"></i></div>
          </div>
        </div>
        <div class="notice">
        </div>
        <form method="post" action="<?php echo $settings; ?>" class="form-horizontal ajax-form" role="form" id="pageColumnsModalForm">
          <fieldset>
            <h4><?php echo $entry_columns; ?></h4>
            <table class="table table-hover table-condensed page-columns">
              <thead>
                <tr>
                  <th>#</th>
                  <th><?php echo $text_column; ?></th>
                  <th class="text-center"><?php echo $text_display; ?></th>
                  <th class="text-center"><?php echo $text_editable; ?></th>
                </tr>
              </thead>
              <tbody class="sortable">
              <?php foreach ($product_columns as $column => $attr) { ?>
                <tr data-id="<?php echo $column; ?>"<?php echo (!(int)$attr['display']) ? ' class="danger"' : ''; ?>>
                  <td><i class="fa fa-arrows-v"></i></td>
                  <td><?php echo $attr['name']; ?><input name="index[columns][<?php echo $column; ?>]" type="hidden" class="index-column" value="<?php echo $attr['index']; ?>"></td>
                  <td class="text-center"><input name="display[columns][<?php echo $column; ?>]" type="checkbox" class="display-column" <?php echo ((int)$attr['display']) ? ' checked' : ''; ?>></td>
                  <td class="text-center"><?php echo ((int)$attr['editable']) ? $text_yes : $text_no; ?></td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
            <h4><?php echo $entry_actions; ?></h4>
            <table class="table table-hover table-condensed page-actions">
              <thead>
                <tr>
                  <th>#</th>
                  <th><?php echo $text_action; ?></th>
                  <th class="text-center"><?php echo $text_display; ?></th>
                </tr>
              </thead>
              <tbody class="sortable">
              <?php foreach ($product_actions as $action => $attr) { ?>
                <tr data-id="<?php echo $column; ?>"<?php echo (!(int)$attr['display']) ? ' class="danger"' : ''; ?>>
                  <td><i class="fa fa-arrows-v"></i></td>
                  <td><?php echo $attr['name']; ?><input name="index[actions][<?php echo $action; ?>]" type="hidden" class="index-column" value="<?php echo $attr['index']; ?>"></td>
                  <td class="text-center"><input name="display[actions][<?php echo $action; ?>]" type="checkbox" class="display-column" <?php echo ((int)$attr['display']) ? ' checked' : ''; ?>></td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
          </fieldset>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default cancel" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo $button_close; ?></button>
        <button type="button" class="btn btn-primary submit" data-form="#pageColumnsModalForm"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- other settings -->
<div class="modal fade" id="otherSettingsModal" tabindex="-1" role="dialog" aria-labelledby="otherSettingsModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="otherSettingsModalLabel"><?php echo $text_other_settings; ?></h4>
      </div>
      <div class="modal-body aqe-container">
        <div class="aqe-overlay fade">
          <div class="aqe-tbl">
            <div class="aqe-tbl-cell"><i class="fa fa-refresh fa-spin fa-5x text-muted"></i></div>
          </div>
        </div>
        <div class="notice">
        </div>
        <form method="post" action="<?php echo $settings; ?>" class="form-horizontal ajax-form" role="form" id="otherSettingsForm">
          <fieldset>
            <div class="form-group">
              <label for="listViewImageWidth" class="col-sm-4 control-label"><?php echo $entry_list_view_image_size; ?></label>
              <div class="col-sm-8">
                <input type="text" class="form-control input-inline width-50px" name="aqe_list_view_image_width" id="listViewImageWidth" value="<?php echo $aqe_list_view_image_width; ?>"> x <input type="text" class="form-control input-inline width-50px" name="aqe_list_view_image_height" id="listViewImageHeight" value="<?php echo $aqe_list_view_image_height; ?>">
              </div>
              <div class="col-sm-offset-4 col-sm-8 error-container">
              </div>
            </div>
            <div class="form-group">
              <label for="quickEditOn" class="col-sm-4 control-label"><?php echo $entry_quick_edit_on; ?></label>
              <div class="col-sm-3 fc-auto-width">
                <select name="aqe_quick_edit_on" id="quickEditOn" class="form-control">
                  <option value="click"<?php echo ($aqe_quick_edit_on == 'click' || $aqe_quick_edit_on != 'dblclick') ? ' selected' : ''; ?>><?php echo $text_single_click; ?></option>
                  <option value="dblclick"<?php echo ($aqe_quick_edit_on == 'dblclick') ? ' selected' : ''; ?>><?php echo $text_double_click; ?></option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="serverSideCaching" class="col-sm-4 control-label"><?php echo $entry_server_side_caching; ?></label>
              <div class="col-sm-8">
                <label class="radio-inline">
                  <input type="radio" name="aqe_server_side_caching" id="serverSideCaching" value="1"<?php echo ((int)$aqe_server_side_caching) ? ' checked' : ''; ?>> <?php echo $text_yes; ?>
                </label>
                <label class="radio-inline">
                  <input type="radio" name="aqe_server_side_caching" id="serverSideCachingNo" value="0"<?php echo (!(int)$aqe_server_side_caching) ? ' checked' : ''; ?>> <?php echo $text_no; ?>
                </label>
                <span class="help-block"><?php echo $help_server_side_caching; ?></span>
              </div>
            </div>
            <div class="form-group">
              <label for="clientSideCaching" class="col-sm-4 control-label"><?php echo $entry_client_side_caching; ?></label>
              <div class="col-sm-8">
                <label class="radio-inline">
                  <input type="radio" name="aqe_client_side_caching" id="clientSideCaching" value="1"<?php echo ((int)$aqe_client_side_caching) ? ' checked' : ''; ?>> <?php echo $text_yes; ?>
                </label>
                <label class="radio-inline">
                  <input type="radio" name="aqe_client_side_caching" id="clientSideCachingNo" value="0"<?php echo (!(int)$aqe_client_side_caching) ? ' checked' : ''; ?>> <?php echo $text_no; ?>
                </label>
                <span class="help-block"><?php echo $help_client_side_caching; ?></span>
                <input type="hidden" name="aqe_cache_size" value="<?php echo (int)$aqe_cache_size ? $aqe_cache_size : 500; ?>">
              </div>
            </div>
            <div class="form-group">
              <label for="alternateRowColour" class="col-sm-4 control-label"><?php echo $entry_alternate_row_colour; ?></label>
              <div class="col-sm-8">
                <label class="radio-inline">
                  <input type="radio" name="aqe_alternate_row_colour" id="alternateRowColour" value="1"<?php echo ((int)$aqe_alternate_row_colour) ? ' checked' : ''; ?>> <?php echo $text_yes; ?>
                </label>
                <label class="radio-inline">
                  <input type="radio" name="aqe_alternate_row_colour" id="alternateRowColourNo" value="0"<?php echo (!(int)$aqe_alternate_row_colour) ? ' checked' : ''; ?>> <?php echo $text_no; ?>
                </label>
              </div>
            </div>
            <div class="form-group">
              <label for="rowHoverHighlighting" class="col-sm-4 control-label"><?php echo $entry_row_hover_highlighting; ?></label>
              <div class="col-sm-8">
                <label class="radio-inline">
                  <input type="radio" name="aqe_row_hover_highlighting" id="rowHoverHighlighting" value="1"<?php echo ((int)$aqe_row_hover_highlighting) ? ' checked' : ''; ?>> <?php echo $text_yes; ?>
                </label>
                <label class="radio-inline">
                  <input type="radio" name="aqe_row_hover_highlighting" id="rowHoverHighlightingNo" value="0"<?php echo (!(int)$aqe_row_hover_highlighting) ? ' checked' : ''; ?>> <?php echo $text_no; ?>
                </label>
                <span class="help-block"><?php echo $help_row_hover_highlighting; ?></span>
              </div>
            </div>
            <div class="form-group">
              <label for="highlightStatus" class="col-sm-4 control-label"><?php echo $entry_highlight_status; ?></label>
              <div class="col-sm-8">
                <label class="radio-inline">
                  <input type="radio" name="aqe_highlight_status" id="highlightStatus" value="1"<?php echo ((int)$aqe_highlight_status) ? ' checked' : ''; ?>> <?php echo $text_yes; ?>
                </label>
                <label class="radio-inline">
                  <input type="radio" name="aqe_highlight_status" id="highlightStatusNo" value="0"<?php echo (!(int)$aqe_highlight_status) ? ' checked' : ''; ?>> <?php echo $text_no; ?>
                </label>
                <span class="help-block"><?php echo $help_highlight_status; ?></span>
              </div>
            </div>
            <div class="form-group">
              <label for="filterSubCategory" class="col-sm-4 control-label"><?php echo $entry_filter_sub_category; ?></label>
              <div class="col-sm-8">
                <label class="radio-inline">
                  <input type="radio" name="aqe_filter_sub_category" id="filterSubCategory" value="1"<?php echo ((int)$aqe_filter_sub_category) ? ' checked' : ''; ?>> <?php echo $text_yes; ?>
                </label>
                <label class="radio-inline">
                  <input type="radio" name="aqe_filter_sub_category" id="filterSubCategoryNo" value="0"<?php echo (!(int)$aqe_filter_sub_category) ? ' checked' : ''; ?>> <?php echo $text_no; ?>
                </label>
                <span class="help-block"><?php echo $help_filter_sub_category; ?></span>
              </div>
            </div>
          </fieldset>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default cancel" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo $button_close; ?></button>
        <button type="button" class="btn btn-primary submit" data-form="#otherSettingsForm"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php if (in_array("image", $columns) || in_array("images", $actions)) { ?>
<!-- image manager -->
<div class="modal fade" id="imageManagerModal" tabindex="-1" role="dialog" aria-labelledby="imageManagerModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="imageManagerModalLabel"><?php echo $text_image_manager; ?></h4>
      </div>
      <div class="modal-body">
        <input type="hidden" value="" id="im-new-image" />
        <div class="image-manager"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-ban"></i> <?php echo $button_cancel; ?></button>
        <button type="button" class="btn btn-primary select"><i class="fa fa-check"></i> <?php echo $button_accept; ?></button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php } ?>

<!-- action menu modal -->
<div class="modal fade" id="actionQuickEditModal" tabindex="-1" role="dialog" aria-labelledby="actionQuickEditModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="actionQuickEditModalLabel"></h4>
      </div>
      <div class="modal-body aqe-container">
        <div class="aqe-overlay fade">
          <div class="aqe-tbl">
            <div class="aqe-tbl-cell"><i class="fa fa-refresh fa-spin fa-5x text-muted"></i></div>
          </div>
        </div>
        <div class="notice">
        </div>
        <form enctype="multipart/form-data" id="actionQuickEditForm" onsubmit="return false;">
          <fieldset>
          </fieldset>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default cancel" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo $button_close; ?></button>
        <button type="button" class="btn btn-primary submit" data-form="#actionQuickEditForm"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="alerts">
  <div class="container" id="alerts">
    <?php foreach ($alerts as $type => $_alerts) { ?>
      <?php foreach ((array)$_alerts as $alert) { ?>
        <?php if ($alert) { ?>
    <div class="alert alert-<?php echo ($type == "error") ? "danger" : $type; ?> fade in">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <?php echo $alert; ?>
    </div>
        <?php } ?>
      <?php } ?>
    <?php } ?>
  </div>
</div>

<div id="content" class="main-content">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li<?php echo ($breadcrumb['active']) ? ' class="active"' : ''; ?>><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>

  <div class="navbar-placeholder">
    <nav class="navbar navbar-default" role="navigation" id="aqe-navbar">
      <div class="nav-container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
            <span class="sr-only"><?php echo $text_toggle_navigation; ?></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <span class="navbar-brand"><i class="fa fa-cog"></i> <?php echo $heading_title; ?></span>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
          <div class="navbar-right">
            <div class="nav navbar-nav navbar-checkbox hidden" id="batch-edit-container">
              <div class="checkbox">
                <label>
                  <input type="checkbox" id="batch-edit"> <?php echo $text_batch_edit; ?>
                </label>
              </div>
            </div>
            <div class="nav navbar-nav navbar-form">
              <div class="form-group search-form">
                <label for="global-search" class="sr-only"><?php echo $text_search;?></label>
                <div class="search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="<?php echo $text_search;?>" id="global-search">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button" id="global-search-btn"><i class="fa fa-search fa-fw"></i></button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="nav navbar-nav btn-group navbar-btn">
              <button type="button" class="btn btn-default" data-action="<?php echo $insert; ?>" id="btn-insert"><i class="fa fa-plus"></i> <?php echo $button_insert; ?></button>
              <button type="button" class="btn btn-default" data-action="<?php echo $copy; ?>" id="btn-copy" disabled><i class="fa fa-files-o"></i> <?php echo $button_copy; ?></button>
              <button type="button" class="btn btn-default" data-action="<?php echo $delete; ?>" id="btn-delete" disabled><i class="fa fa-trash-o"></i> <?php echo $button_delete; ?></button>
            </div>
            <ul class="nav navbar-nav">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $text_settings; ?> <b class="caret"></b></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#" data-toggle="modal" data-target="#iDisplayLengthModal"><?php echo $text_items_per_page; ?></a></li>
                  <li><a href="#" data-toggle="modal" data-target="#pageColumnsModal"><?php echo $text_choose_columns; ?></a></li>
                  <li class="divider"></li>
                  <li><a href="#" data-toggle="modal" data-target="#otherSettingsModal"><?php echo $text_other_settings; ?></a></li>
                  <li class="divider"></li>
                  <li><a href="#" id="clearCache" data-url="<?php echo $clear_cache; ?>"><?php echo $text_clear_cache; ?></a></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>
  </div>

  <div class="content">
    <div id="dT_processing" class="dataTables_processing fade"><i class="fa fa-refresh fa-spin fa-5x"></i></div>
    <form method="post" enctype="multipart/form-data" id="form" class="form-horizontal" role="form">
      <fieldset>
        <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-condensed<?php echo ($aqe_row_hover_highlighting) ? ' table-hover' : ''; ?><?php echo ($aqe_alternate_row_colour) ? ' table-striped' : ''; ?>" id="dT">
          <thead>
            <tr>
              <?php foreach ($columns as $col) {
               switch($col) {
                case 'selector': ?>
              <th class="<?php echo $column_info[$col]['align']; ?> col_<?php echo $col; ?>" width="1"><input type="checkbox" id="dT-selector" /></th>
                <?php break;
                case 'image': ?>
              <th class="<?php echo $column_info[$col]['align']; ?> col_<?php echo $col; ?>" width="1"><?php echo $column_info[$col]['name']; ?></th>
                <?php break;
                default: ?>
              <th class="<?php echo $column_info[$col]['align']; ?> col_<?php echo $col; ?>"><?php echo $column_info[$col]['name']; ?></th>
                <?php break;
               } ?>
              <?php } ?>
            </tr>
            <tr class="filters">
              <?php foreach ($columns as $col) {
               switch($col) {
                case 'view_in_store':
                case 'selector':
                case 'image': ?>
              <td></td>
                <?php break;
                case 'status': ?>
              <td class="<?php echo $column_info[$col]['align']; ?>">
                <select name="filter_<?php echo $col; ?>" class="form-control input-sm search_init fltr <?php echo $col; ?>" data-column="<?php echo $col; ?>">
                  <option value="" selected></option>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                </select>
              </td>
                <?php break;
                case 'subtract':
                case 'shipping': ?>
              <td class="<?php echo $column_info[$col]['align']; ?>">
                <select name="filter_<?php echo $col; ?>" class="form-control input-sm search_init fltr <?php echo $col; ?>" data-column="<?php echo $col; ?>">
                  <option value="" selected></option>
                  <option value="1"><?php echo $text_yes; ?></option>
                  <option value="0"><?php echo $text_no; ?></option>
                </select>
              </td>
                <?php break;
                case 'action': ?>
              <td class="<?php echo $column_info[$col]['align']; ?>">
                <div class="btn-group" style="white-space:nowrap;">
                  <button type="button" class="btn btn-sm btn-default" id="filter" title="<?php echo $text_filter; ?>"><i class="fa fa-filter fa-fw"></i></button>
                  <button type="button" class="btn btn-sm btn-default" id="clear-filter" title="<?php echo $text_clear_filter; ?>"><i class="fa fa-times fa-fw"></i></button>
                </div>
              </td>
                <?php break;
                case 'manufacturer': ?>
                <?php if (in_array($col, array_keys($typeahead))) { ?>
              <td class="<?php echo $column_info[$col]['align']; ?>">
                <input type="text" class="form-control input-sm fltr <?php echo $col; ?> typeahead id_based" placeholder="<?php echo $text_autocomplete; ?>">
                <input type="hidden" name="filter_<?php echo $col; ?>" class="search_init fltr <?php echo $col; ?>" data-column="<?php echo $col; ?>">
              </td>
                <?php } else { ?>
              <td class="<?php echo $column_info[$col]['align']; ?>">
                <select name="filter_<?php echo $col; ?>" class="form-control input-sm search_init fltr <?php echo $col; ?>" data-column="<?php echo $col; ?>">
                  <option value="" selected></option>
                  <option value="*"><?php echo $text_none; ?></option>
                  <?php foreach ($manufacturers as $m) { ?>
                  <option value="<?php echo $m['manufacturer_id']; ?>"><?php echo $m['name']; ?></option>
                  <?php } ?>
                </select>
              </td>
                <?php } ?>
                <?php break;
                case 'category': ?>
                <?php if (in_array($col, array_keys($typeahead))) { ?>
              <td class="<?php echo $column_info[$col]['align']; ?>">
                <input type="text" class="form-control input-sm fltr <?php echo $col; ?> typeahead id_based" placeholder="<?php echo $text_autocomplete; ?>">
                <input type="hidden" name="filter_<?php echo $col; ?>" class="search_init fltr <?php echo $col; ?>" data-column="<?php echo $col; ?>">
              </td>
                <?php } else { ?>
              <td class="<?php echo $column_info[$col]['align']; ?>">
                <select name="filter_<?php echo $col; ?>" class="form-control input-sm search_init fltr <?php echo $col; ?>" data-column="<?php echo $col; ?>">
                  <option value="" selected></option>
                  <option value="*"><?php echo $text_none; ?></option>
                  <?php foreach ($categories as $c) { ?>
                  <option value="<?php echo $c['category_id']; ?>"><?php echo $c['name']; ?></option>
                  <?php } ?>
                </select>
              </td>
                <?php } ?>
                <?php break;
                case 'download': ?>
                <?php if (in_array($col, array_keys($typeahead))) { ?>
              <td class="<?php echo $column_info[$col]['align']; ?>">
                <input type="text" class="form-control input-sm fltr <?php echo $col; ?> typeahead id_based" placeholder="<?php echo $text_autocomplete; ?>">
                <input type="hidden" name="filter_<?php echo $col; ?>" class="search_init fltr <?php echo $col; ?>" data-column="<?php echo $col; ?>">
              </td>
                <?php } else { ?>
              <td class="<?php echo $column_info[$col]['align']; ?>">
                <select name="filter_<?php echo $col; ?>" class="form-control input-sm search_init fltr <?php echo $col; ?>" data-column="<?php echo $col; ?>">
                  <option value="" selected></option>
                  <option value="*"><?php echo $text_none; ?></option>
                  <?php foreach ($downloads as $dl) { ?>
                  <option value="<?php echo $dl['download_id']; ?>"><?php echo $dl['name']; ?></option>
                  <?php } ?>
                </select>
              </td>
                <?php } ?>
                <?php break;
                case 'filter': ?>
                <?php if (in_array($col, array_keys($typeahead))) { ?>
              <td class="<?php echo $column_info[$col]['align']; ?>">
                <input type="text" class="form-control input-sm fltr <?php echo $col; ?> typeahead id_based" placeholder="<?php echo $text_autocomplete; ?>">
                <input type="hidden" name="filter_<?php echo $col; ?>" class="search_init fltr <?php echo $col; ?>" data-column="<?php echo $col; ?>">
              </td>
                <?php } else { ?>
              <td class="<?php echo $column_info[$col]['align']; ?>">
                <select name="filter_<?php echo $col; ?>" class="form-control input-sm search_init fltr <?php echo $col; ?>" data-column="<?php echo $col; ?>">
                  <option value="" selected></option>
                  <option value="*"><?php echo $text_none; ?></option>
                  <?php foreach ($filters as $fg) { ?>
                  <optgroup label="<?php echo addslashes($fg['name']); ?>">
                  <?php foreach ($fg['filters'] as $f) { ?>
                    <option value="<?php echo $f['filter_id']; ?>"><?php echo $f['name']; ?></option>
                  <?php } ?>
                  </optgroup>
                  <?php } ?>
                </select>
              </td>
                <?php } ?>
                <?php break;
                case 'store': ?>
              <td class="<?php echo $column_info[$col]['align']; ?>">
                <select name="filter_<?php echo $col; ?>" class="form-control input-sm search_init fltr <?php echo $col; ?>" data-column="<?php echo $col; ?>">
                  <option value="" selected></option>
                  <option value="*"><?php echo $text_none; ?></option>
                  <?php foreach ($stores as $s) { ?>
                  <option value="<?php echo $s['store_id']; ?>"><?php echo $s['name']; ?></option>
                  <?php } ?>
                </select>
              </td>
                <?php break;
                case 'length_class': ?>
              <td class="<?php echo $column_info[$col]['align']; ?>">
                <select name="filter_<?php echo $col; ?>" class="form-control input-sm search_init fltr <?php echo $col; ?>" data-column="<?php echo $col; ?>">
                  <option value="" selected></option>
                  <?php foreach ($length_classes as $lc) { ?>
                  <option value="<?php echo $lc['length_class_id']; ?>"><?php echo $lc['title']; ?></option>
                  <?php } ?>
                </select>
              </td>
                <?php break;
                case 'weight_class': ?>
              <td class="<?php echo $column_info[$col]['align']; ?>">
                <select name="filter_<?php echo $col; ?>" class="form-control input-sm search_init fltr <?php echo $col; ?>" data-column="<?php echo $col; ?>">
                  <option value="" selected></option>
                  <?php foreach ($weight_classes as $wc) { ?>
                  <option value="<?php echo $wc['weight_class_id']; ?>"><?php echo $wc['title']; ?></option>
                  <?php } ?>
                </select>
              </td>
                <?php break;
                case 'stock_status': ?>
              <td class="<?php echo $column_info[$col]['align']; ?>">
                <select name="filter_<?php echo $col; ?>" class="form-control input-sm search_init fltr <?php echo $col; ?>" data-column="<?php echo $col; ?>">
                  <option value="" selected></option>
                  <?php foreach ($stock_statuses as $ss) { ?>
                  <option value="<?php echo $ss['stock_status_id']; ?>"><?php echo $ss['name']; ?></option>
                  <?php } ?>
                </select>
              </td>
                <?php break;
                case 'tax_class': ?>
              <td class="<?php echo $column_info[$col]['align']; ?>">
                <select name="filter_<?php echo $col; ?>" class="form-control input-sm search_init fltr <?php echo $col; ?>" data-column="<?php echo $col; ?>">
                  <option value="" selected></option>
                  <option value="*"><?php echo $text_none; ?></option>
                  <?php foreach ($tax_classes as $tc) { ?>
                  <option value="<?php echo $tc['tax_class_id']; ?>"><?php echo $tc['title']; ?></option>
                  <?php } ?>
                </select>
              </td>
                <?php break;
                case 'price': ?>
              <td class="<?php echo $column_info[$col]['align']; ?>">
                <div class="input-group">
                  <input type="text" name="filter_<?php echo $col; ?>" class="form-control input-sm search_init fltr <?php echo $col; ?>" id="filter_price" data-column="<?php echo $col; ?>">
                  <input type="hidden" value="" id="filter_special_price">
                  <div class="input-group-btn" data-toggle="buttons">
                    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" tabindex="-1">
                      <span class="caret"></span>
                      <span class="sr-only"><?php echo $text_toggle_dropdown; ?></span>
                    </button>
                    <ul class="dropdown-menu text-left pull-right price" role="menu">
                      <li class="active"><a href="#" class="filter-special-price" data-value="" id="special-price-off"><i class="fa fa-fw fa-check"></i> <?php echo $text_special_off; ?></a></li>
                      <li><a href="#" class="filter-special-price" data-value="active"><i class="fa fa-fw"></i> <?php echo $text_special_active; ?></a></li>
                      <li><a href="#" class="filter-special-price" data-value="expired"><i class="fa fa-fw"></i> <?php echo $text_special_expired; ?></a></li>
                      <li><a href="#" class="filter-special-price" data-value="future"><i class="fa fa-fw"></i> <?php echo $text_special_future; ?></a></li>
                    </ul>
                  </div>
                </div>
              </td>
                <?php break;
                case 'name':
                case 'model':
                case 'sku':
                case 'upc':
                case 'ean':
                case 'jan':
                case 'isbn':
                case 'mpn':
                case 'location':
                case 'seo': ?>
              <td class="<?php echo $column_info[$col]['align']; ?>"><input type="text" name="filter_<?php echo $col; ?>" class="form-control input-sm search_init fltr <?php echo $col; ?> typeahead"  placeholder="<?php echo $text_autocomplete; ?>" data-column="<?php echo $col; ?>"></td>
                <?php break;
                default: ?>
              <td class="<?php echo $column_info[$col]['align']; ?>"><input type="text" name="filter_<?php echo $col; ?>" class="form-control input-sm search_init fltr <?php echo $col; ?>" data-column="<?php echo $col; ?>"></td>
                <?php break;
               } ?>
              <?php } ?>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </fieldset>
    </form>
  </div>

</div>
<script type="text/javascript"><!--
(function(bull5i,$,undefined){var oTable,xhr,related=<?php echo json_encode($related); ?>;
bull5i.texts=$.extend({},bull5i.texts,{error_ajax_request:'<?php echo addslashes($error_ajax_request); ?>'});
$.fn.editableform.loading='<div class="editableform-loading"><div class="aqe-tbl"><div class="aqe-tbl-cell"><i class="fa fa-refresh fa-spin fa-2x text-muted"></i></div></div></div>';
$.fn.editableform.clear='<span class="editable-clear-x"><i class="fa fa-times-circle"></i></span>';
function responseHandler(e,l){if(l="undefined"==typeof l?!0:l,e.success){var t=!0;return e.msg&&(bull5i.display_alert(e.msg,"success",5e3,t),t=!1),e.alerts&&$.each(e.alerts,function(e,l){"object"==typeof l?$.each(l,function(l,a){a&&(bull5i.display_alert(a,e,0,t),t=!1)}):l&&(bull5i.display_alert(l,e,0,t),t=!1)}),!0}if(e.alerts){var t=!0;$.each(e.alerts,function(e,l){"object"==typeof l?$.each(l,function(l,a){a&&(bull5i.display_alert(a,e,0,t),t=!1)}):l&&(bull5i.display_alert(l,e,0,t),t=!1)})}return e.msg?e.msg:(l&&$(this).editable("hide"),!1)}
function setupEditables(){var defaultParams={ajaxOptions:{type:"POST",dataType:"json",cache:!1},type:"text",url:"<?php echo $update; ?>",toggle:"<?php echo $aqe_quick_edit_on; ?>",highlight:!1,container:"body",title:"",emptytext:"",pk:bull5i.fnGetPkParams,value:function(){return oTable.fnGetData(this)},params:function(e){var a={};return a.id=e.pk.id,a.column=e.pk.column,a.old=e.pk.old,a.value=$.isArray(e.value)&&0==e.value.length?null:e.value,$("#batch-edit").is(":checked")&&$("input[name*='selected']:checked").length&&(a.ids=$("input[name*='selected']:checked").serializeObject().selected),a},success:function(e,a){if(result=responseHandler.call(this,e),editable=$(this).data("editable"),result===!0){var t=oTable.fnGetPosition(this),l=oTable.fnSettings(),n=l.aoColumns[t[1]].sName;if(e.value&&(a=e.value),$.isArray(e.results.done)){var s=e.results.done.length>1;$.each(e.results.done,function(u,i){var r=$("#p_"+i).get(0);if(r){var o=oTable.fnGetPosition(r),d=l.aoData[o]._aData;d[n]=a,void 0!=e.values&&(e.values.hasOwnProperty("*")&&$.extend(d,e.values["*"]),e.values.hasOwnProperty(i)&&$.extend(d,e.values[i])),bull5i.fnDataTablesUpdateCache(o,d),s||oTable.fnUpdate(a,o,t[1])}}),updateRelated(n,e.results.done)===!1&&s&&oTable.fnDraw(!1)}return{newValue:a}}return result===!1?{newValue:$(this).html()}:result},display:function(){}}
<?php if (array_reduce(array("status_qe", "yes_no_qe", "manufac_qe", "stock_qe", "tax_cls_qe", "length_qe", "weight_qe"), function($result, $type) use ($types) { return $result | in_array($type, $types); }, false) !== false) { ?>,selectParams={type:'select',showbuttons:true}<?php } ?>
<?php if (array_reduce(array("cat_qe", "dl_qe", "filter_qe", "store_qe"), function($result, $type) use ($types) { return $result | in_array($type, $types); }, false) !== false) { ?>
,select2Params={type:'select2',select2:{multiple:true,allowClear:true,placeholder:'<?php echo $text_autocomplete; ?>',},viewseparator:'<br/>'}
<?php } ?>;
<?php if (in_array("seo_qe", $types) && !$multilingual_seo) { ?>
$("td.seo_qe").editable(defaultParams);
<?php } ?>
<?php if (in_array("qe", $types)) { ?>
$("td.qe").editable(defaultParams);
<?php } ?>
<?php if (in_array("date_qe", $types)) { ?>
$("td.date_qe").editable($.extend(true,{},defaultParams,{type:'combodate',format:'YYYY-MM-DD',template:'D / MMMM / YYYY',combodate:{smartDays:true,maxYear:<?php echo date("Y") + 5; ?>}}));
<?php } ?>
<?php if (in_array("datetime_qe", $types)) { ?>
$("td.datetime_qe").editable($.extend(true,{},defaultParams,{type:'combodate',format:'YYYY-MM-DD HH:mm:ss',template:'D / MMM / YYYY  HH:mm:ss',combodate:{smartDays:true,maxYear:<?php echo date("Y") + 5; ?>,minuteStep:1}}));
<?php } ?>
<?php if (in_array("status_qe", $types)) { ?>
$("td.status_qe").editable($.extend({},defaultParams,selectParams,{source:[{value:0,text:'<?php echo addslashes($text_disabled); ?>'},{value:1,text:'<?php echo addslashes($text_enabled); ?>'}]}));
<?php } ?>
<?php
$multilingual_text = array();
foreach (array('name_qe', 'tag_qe', 'seo_qe') as $type) {
  if (in_array($type, $types) && ($type == 'seo_qe' && $multilingual_seo || $type != 'seo_qe')) $multilingual_text[] = 'td.' . $type;
} ?>
<?php if ($multilingual_text) { ?>
$("<?php echo implode(',', $multilingual_text); ?>").editable($.extend({},defaultParams,{type:"multilingual_text",source:"<?php echo $load; ?>",sourceOptions:function(){var e=bull5i.oTable,a=e.fnGetPosition(this),t=e.fnSettings(),l={type:"POST",dataType:"json",data:{id:t.aoData[a[0]]._aData.id,column:t.aoColumns[a[1]].sName}};return l},sourceLoaded:function(e){if(e.success)return e.data;if(e.alerts){var a=!0;$.each(e.alerts,function(e,t){"object"==typeof t?$.each(t,function(t,l){l&&(bull5i.display_alert(l,e,0,a),a=!1)}):t&&(bull5i.display_alert(t,e,0,a),a=!1)})}return e.msg?e.msg:null},showbuttons:"bottom",value:null,success:function(e,a){if(result=responseHandler.call(this,e,!1),result===!0){{var t=oTable.fnGetPosition(this),l=oTable.fnSettings(),s=l.aoColumns[t[1]].sName;l.aoData[t[0]]._aData}if(e.value&&(a=e.value),$.isArray(e.results.done)){var o=e.results.done.length>1;$.each(e.results.done,function(r,n){var u=$("#p_"+n).get(0);if(u){var i=oTable.fnGetPosition(u),d=l.aoData[i]._aData;d[s]=a,void 0!=e.values&&(e.values["*"]&&$.extend(!0,d,e.values["*"]),e.values[n]&&$.extend(!0,d,e.values[n])),bull5i.fnDataTablesUpdateCache(i,d),o||oTable.fnUpdate(a,i,t[1])}}),updateRelated(s,e.results.done)===!1&&o&&oTable.fnDraw(!1)}return{newValue:a}}var r=$(this).data("editable").input.$tpl;return r&&(r.find(".form-group").removeClass("has-error"),r.find(".form-group .help-block").remove()),result===!1?e.errors&&$.isArray(e.errors.value)&&r?($.each(e.errors.value,function(e,a){var t=a.lang,l=a.text,s=r.find('.form-group[data-lang="'+t+'"]');s.addClass("has-error").append($("<span/>",{"class":"help-block"}).html(l))}),!1):{newValue:$(this).html()}:result}}));
<?php } ?>
<?php if (in_array("yes_no_qe", $types)) { ?>
$("td.yes_no_qe").editable($.extend({},defaultParams,selectParams,{source:[{value:0,text:"<?php echo addslashes($text_no); ?>"},{value:1,text:"<?php echo addslashes($text_yes); ?>"}]}));<?php } ?>
<?php if (in_array("manufac_qe", $types) && $manufacturer_select !== false) { ?>
$("td.manufac_qe").editable($.extend({},defaultParams,selectParams,{source:<?php echo $manufacturer_select; ?>,prepend:[{value:0,text:'<?php echo addslashes($text_none); ?>'}]}));
<?php } else { ?>
$("td.manufac_qe").editable($.extend({},defaultParams,selectParams,{type:"typeaheadjs",showbuttons:!0,typeahead:{name:"manufacturer"<?php if (isset($typeahead['manufacturer']['prefetch'])) { ?>,prefetch:"<?php echo $typeahead['manufacturer']['prefetch']; ?>"<?php }; if (isset($typeahead['manufacturer']['remote'])) { ?>,remote:"<?php echo $typeahead['manufacturer']['remote']; ?>"<?php } ?>,limit:10,template:['<p><span style="white-space:nowrap;">{{value}}</span></p>'].join(""),engine:Hogan},tpl:'<input type="text" placeholder="<?php echo $text_autocomplete; ?>">',value2input:function(e){var t=oTable.fnGetPosition(this.options.scope),a=oTable.fnSettings(),n=a.aoColumns[t[1]].sName,p=a.aoData[t[0]]._aData,o="";return o=""!=p[n+"_text"]?p[n+"_text"]:"<?php echo $text_none; ?>",this.$input.data("ta-selected",{value:o,id:e}),o},input2value:function(){var e=this.$input.data("ta-selected");return"undefined"!=typeof e?"*"!=e.id?e.id:0:null}}));
<?php } ?>
<?php if (in_array("cat_qe", $types)) { ?>
$("td.cat_qe").editable($.extend(!0,{},defaultParams,select2Params,{<?php if ($category_select !== false) { ?>source:<?php echo $category_select; ?>,<?php } else { ?>select2:{minimumInputLength:1,ajax:{type:"GET",url:"<?php echo $filter; ?>",dataType:"json",cache:!1,quietMillis:150,data:function(e){return{query:e,type:"category",token:"<?php echo $token; ?>"}},results:function(e){var t=[];return $.each(e,function(e,a){t.push({id:a.id,text:a.full_name})}),{results:t}}},initSelection:function(e,t){var a=[];$(e.val().split(",")).each(function(){a.push(this)}),$.ajax({type:"GET",url:"<?php echo $filter; ?>",dataType:"json",data:{query:a,type:"category",multiple:!0,token:"<?php echo $token; ?>"}}).done(function(e){var a=[];$.each(e,function(e,t){a.push({id:t.id,text:t.full_name})}),t(a)})}}<?php } ?>}));
<?php } ?>
<?php if (in_array("dl_qe", $types)) { ?>
$("td.dl_qe").editable($.extend(!0,{},defaultParams,select2Params,{<?php if ($download_select !== false) { ?>source:<?php echo $download_select; ?>,<?php } else { ?>select2:{minimumInputLength:1,ajax:{type:"GET",url:"<?php echo $filter; ?>",dataType:"json",cache:!1,quietMillis:150,data:function(e){return{query:e,type:"download",token:"<?php echo $token; ?>"}},results:function(e){var t=[];return $.each(e,function(e,n){t.push({id:n.id,text:n.value})}),{results:t}}},initSelection:function(e,t){var n=[];$(e.val().split(",")).each(function(){n.push(this)}),$.ajax({type:"GET",url:"<?php echo $filter; ?>",dataType:"json",data:{query:n,type:"download",multiple:!0,token:"<?php echo $token; ?>"}}).done(function(e){var n=[];$.each(e,function(e,t){n.push({id:t.id,text:t.value})}),t(n)})}}<?php } ?>}));
<?php } ?>
<?php if (in_array("filter_qe", $types)) { ?>
$("td.filter_qe").editable($.extend(!0,{},defaultParams,select2Params,{<?php if ($filter_select !== false) { ?>source:<?php echo $filter_select; ?>,<?php } else { ?>select2:{minimumInputLength:1,ajax:{type:"GET",url:"<?php echo $filter; ?>",dataType:"json",cache:!1,quietMillis:150,data:function(e){return{query:e,type:"filter",token:"<?php echo $token; ?>"}},results:function(e){var t=[],n=[];return $.each(e,function(e,r){var u=$.inArray(r.group_name);-1==u&&(u=n.length,n.push(r.group_name),t[u]={text:r.group_name,children:[]}),t[u].children.push({id:r.id,text:r.value,group:r.group})}),{results:t}}},initSelection:function(e,t){var n=[];$(e.val().split(",")).each(function(){n.push(this)}),$.ajax({type:"GET",url:"<?php echo $filter; ?>",dataType:"json",data:{query:n,type:"filter",multiple:!0,token:"<?php echo $token; ?>"}}).done(function(e){var n=[];$.each(e,function(e,t){n.push({id:t.id,text:t.full_name})}),t(n)})},formatSelection:function(e){return void 0!=e.group?e.group+e.text:e.text}}<?php } ?>}));
<?php } ?>
<?php if (in_array("store_qe", $types)) { ?>
$("td.store_qe").editable($.extend({},defaultParams,select2Params,{source:<?php echo $store_select; ?>}));
<?php } ?>
<?php if (in_array("stock_qe", $types)) { ?>
$("td.stock_qe").editable($.extend({},defaultParams,selectParams,{source:<?php echo $stock_status_select; ?>,}));
<?php } ?>
<?php if (in_array("tax_cls_qe", $types)) { ?>
$("td.tax_cls_qe").editable($.extend({},defaultParams,selectParams,{source:<?php echo $tax_class_select; ?>,}));
<?php } ?>
<?php if (in_array("length_qe", $types)) { ?>
$("td.length_qe").editable($.extend({},defaultParams,selectParams,{source:<?php echo $length_class_select; ?>}));
<?php } ?>
<?php if (in_array("weight_qe", $types)) { ?>
$("td.weight_qe").editable($.extend({},defaultParams,selectParams,{source:<?php echo $weight_class_select; ?>,}));
<?php } ?>
<?php if (in_array("qty_qe", $types)) { ?>
$("td.qty_qe").editable($.extend({}, defaultParams));
<?php } ?>
<?php if (in_array("image_qe", $types)) { ?>
$("td.image_qe").editable($.extend(!0,{},defaultParams,{type:"image",noImage:"<?php echo $no_image; ?>",clearText:"<?php echo $text_clear; ?>",browseText:"<?php echo $text_browse; ?>",value:function(){var e={},a=oTable.fnGetPosition(this),i=oTable.fnSettings(),o=i.aoData[a[0]]._aData;return e.image=oTable.fnGetData(this),e.thumbnail=o.image_thumb,e.alt=o.image_alt,e.title=o.image_title,e},params:function(e){var a={};return a.id=e.pk.id,a.column=e.pk.column,a.old=e.pk.old,a.value=e.value,$("#batch-edit").is(":checked")&&$("input[name*='selected']:checked").length&&(a.ids=$("input[name*='selected']:checked").serializeObject().selected),a},chooseImage:function(e,a){var i="im-new-image",o="";$("#"+i).val(""),$("#imageManagerModal .modal-body .image-manager").html('<iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field='+encodeURIComponent(i)+'" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto" id="im-iframe"></iframe>'),$("#imageManagerModal").modal("show"),$("#im-iframe").load(function(){var e=$(this).contents();e.on("dblclick","#column-right a",function(){$("#"+i).val("data/"+$(this).find("input[name='image']").attr("value")),$("#imageManagerModal").modal("hide")}),$("#imageManagerModal .modal-footer .select").on("click",function(){var a=$("#column-right a.selected",e);a.length&&($("#"+i).val("data/"+a.find("input[name='image']").attr("value")),$("#imageManagerModal").modal("hide"),$("#imageManagerModal .modal-footer .select").off("click"))})}),$("#imageManagerModal").on("hide.bs.modal",function(){$("#"+i).val()?(o=$("#"+i).val(),$.ajax({url:"index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image="+encodeURIComponent(o)+"&width=<?php echo $list_view_image_width; ?>&height=<?php echo $list_view_image_height; ?>",dataType:"text",success:function(i){e.image=o,e.thumbnail=i,a.call(null,e)}})):"function"==typeof a&&a.call(null,null),$("#imageManagerModal").off("hide.bs.modal"),$("#imageManagerModal .modal-footer .select").off("click")}).on("hidden.bs.modal",function(){$("#imageManagerModal .modal-body .image-manager").empty(),$("#imageManagerModal").off("hidden.bs.modal")})}}));
<?php } ?>
<?php if (in_array("price_qe", $types)) { ?>
$("td.price_qe").editable($.extend({}, defaultParams));
<?php } ?>
}
function updateRelated(e,a){if(a&&related[e]&&related[e].length){var t={},n=oTable.fnSettings();return $.each(related[e],function(e,n){t[n]=a}),$.when($.ajax({url:"<?php echo $reload; ?>",type:"POST",cache:!1,dataType:"json",data:{data:t}})).done(function(e){$.each(e.values,function(e,a){var t=$("#p_"+e).get(0);if(t){var r=oTable.fnGetPosition(t),l=n.aoData[r]._aData;$.extend(l,a),bull5i.fnDataTablesUpdateCache(r,l)}}),oTable.fnDraw(!1)}).fail(function(e){if(bull5i.display_alert){var a="string"==typeof e?e:e.responseText||e.statusText||"Unknown error!";bull5i.display_alert(bull5i.texts.error_ajax_request+" "+a,"error",0)}}),!0}return!1}
$(function(){bull5i.items_cache_size=<?php echo (int)$aqe_cache_size ? (int)$aqe_cache_size : 500; ?>;
oTable=$('#dT').dataTable({"sDom":"t<'row'<'col-xs-6'i><'col-xs-6'p>>","bServerSide":true,"sAjaxSource":'<?php echo $source; ?>',"fnServerParams":function(aoData){aoData.push({"name":"token","value":"<?php echo $token; ?>"});if($("#filter_special_price").length&&$("#filter_special_price").val()){aoData.push({"name":"filter_special_price","value":$("#filter_special_price").val()});}},<?php if ((int)$aqe_client_side_caching) { ?>"fnServerData": bull5i.fnDataTablesPipeline,<?php } ?>"fnInitComplete":function(e){""!=e.oPreviousSearch.sSearch&&($("#global-search").val(e.oPreviousSearch.sSearch),$("#global-search-btn").data("last-search",e.oPreviousSearch.sSearch),bull5i.update_search_btn());for(var a=0,o=e.aoColumns.length;o>a;a++)e.aoPreSearchCols[a].sSearch.length>0&&$("tr.filters .fltr."+e.aoColumns[a].sName+":input").val(e.aoPreSearchCols[a].sSearch);if(e.oLoadedState&&void 0!=e.oLoadedState.sSpecialPrice&&""!=e.oLoadedState.sSpecialPrice&&($special=$('ul.price .filter-special-price[data-value="'+e.oLoadedState.sSpecialPrice+'"]'),$special.length&&bull5i.update_special_price_menu($special)),e.oLoadedState&&void 0!=e.oLoadedState.aoTypeaheads&&void 0!=e.oLoadedState.aoTypeaheads)for(key in e.oLoadedState.aoTypeaheads)$(".fltr.typeahead."+key).data("ta-selected",{value:e.oLoadedState.aoTypeaheads[key]}).data("ta-name",key).typeahead("setQuery",e.oLoadedState.aoTypeaheads[key])},fnStateSaveParams:function(a,e){e.sSpecialPrice=$("#filter_special_price").length?$("#filter_special_price").val():"",e.aoTypeaheads={},$(".fltr.typeahead").each(function(){var t=$(this).val(),i=a.aoColumns[$(this).closest("td").index()].sName;e.aoTypeaheads[i]=t})},fnStateLoadParams:function(a,e){if(void 0!=e.sSpecialPrice&&""!=e.sSpecialPrice&&$("#filter_special_price").length&&$("#filter_special_price").val(e.sSpecialPrice),void 0!=e.aoTypeaheads)for(key in e.aoTypeaheads)$(".fltr.typeahead."+key).val(e.aoTypeaheads[key])},fnStateSave:function(a,e){void 0!=window.localStorage?window.localStorage.setItem("aqe_dt",this.oApi._fnJsonString(e)):this.oApi._fnCreateCookie(a.sCookiePrefix+a.sInstance,this.oApi._fnJsonString(e),a.iCookieDuration,a.sCookiePrefix,a.fnCookieCallback)},fnStateLoad:function(oSettings){if(void 0!=window.localStorage)return sValue=localStorage.getItem("aqe_dt"),void 0!=window.JSON?JSON.parse(sValue):"function"==typeof $.parseJSON?$.parseJSON(sValue):eval("("+sValue+")");var sData=this.oApi._fnReadCookie(oSettings.sCookiePrefix+oSettings.sInstance),oData;try{oData="function"==typeof $.parseJSON?$.parseJSON(sData):eval("("+sData+")")}catch(e){oData=null}return oData},fnRowCallback:function(a,e){$(a).attr("data-id",e.id)},fnDrawCallback:function(){setupEditables(),bull5i.update_nav_buttons(!0)},
"oLanguage":{"oAria":{"sSortAscending":"<?php echo addslashes($text_sort_ascending); ?>","sSortDescending":"<?php echo addslashes($text_sort_descending); ?>"},"oPaginate":{"sFirst":"<?php echo addslashes($text_first_page); ?>","sLast":"<?php echo addslashes($text_last_page); ?>","sNext":"<?php echo addslashes($text_next_page); ?>","sPrevious":"<?php echo addslashes($text_previous_page); ?>"},"sEmptyTable":"<?php echo addslashes($text_empty_table); ?>","sInfo":"<?php echo addslashes($text_showing_info); ?>","sInfoEmpty":"<?php echo addslashes($text_showing_info_empty); ?>","sInfoFiltered":"<?php echo addslashes($text_showing_info_filtered); ?>","sInfoPostFix":"","sInfoThousands":",","sLoadingRecords":"<?php echo addslashes($text_loading_records); ?>","sZeroRecords":"<?php echo addslashes($text_no_matching_records); ?>"},"bDeferRender":true,"bProcessing":false,"bStateSave":true,"bScrollCollapse":true,"bAutoWidth":false,"bSortCellsTop":true,"iDisplayLength":<?php echo $items_per_page; ?>,
"aoColumnDefs":[
<?php if (in_array("selector", $columns)) { ?>
{aTargets:['col_selector'],fnCreatedCell:function(nTd,sData,oData,iRow,iCol){$(nTd).html($("<input/>",{"type":"checkbox","name":"selected[]","value":oData.id}));}},
<?php } ?>
<?php if (in_array("view_in_store", $columns)) { ?>
{aTargets:['col_view_in_store'],fnCreatedCell:function(nTd,sData,oData,iRow,iCol){var $select=$("<select/>",{"class":"form-control view_in_store"}).append($("<option/>",{"value":""}).html('<?php echo addslashes($text_select); ?>'));for(var i=0;i<sData.length;i++){$select.append($("<option/>",{"value":sData[i].url}).html(sData[i].name));};$(nTd).html($select);}},
<?php } ?>
<?php if (in_array("action", $columns)) { ?>
{aTargets:["col_action"],fnCreatedCell:function(t,n,a){for(var e=$("<div/>",{"class":"btn-group"}),l=0;l<n.length;l++)$btn=n[l].url?$("<a/>",{href:n[l].url,"class":"btn btn-default btn-xs "+n[l].type,id:n[l].action+"-"+a.id,title:n[l].title}):$("<button/>",{type:"button","class":"btn btn-default btn-xs action "+n[l].type,id:n[l].action+"-"+a.id,"data-column":n[l].action,title:n[l].title}),n[l].name&&$btn.html(n[l].name),n[l].ref&&$btn.attr("data-ref",n[l].ref),n[l].icon&&$btn.prepend($("<i/>",{"class":"fa fa-"+n[l].icon})),e.append($btn);$(t).html(e)}},
<?php } ?>
<?php if (in_array("image", $columns)) { ?>
{aTargets:['col_image'],fnCreatedCell:function(nTd,sData,oData,iRow,iCol){$(nTd).html($("<img/>",{"src":oData.image_thumb,"alt":oData.image_alt,"title":oData.image_title,"data-id":oData.id,"class":"img-thumbnail"}));}},
<?php } ?>
<?php if (in_array("price", $columns)) { ?>
{aTargets:['col_price'],fnCreatedCell:function(nTd,sData,oData,iRow,iCol){if(oData.special){$(nTd).html($("<s/>").html(sData)).append($("<br/>")).append($("<span/>",{"class":"text-danger"}).html(oData.special));}else{$(nTd).html(sData);}}},
<?php } ?>
<?php if (in_array("status", $columns)) { ?>
{aTargets:['col_status'],fnCreatedCell:function(nTd,sData,oData,iRow,iCol){if(oData.status_class){$(nTd).html($("<span/>",{"class":"label label-"+oData.status_class}).html(oData.status_text));}else{$(nTd).html(oData.status_text);}}},
<?php } ?>
<?php foreach (array("category", "download", "filter", "store") as $col) { ?>
<?php if (in_array($col, $columns)) { ?>
{aTargets:['col_<?php echo $col; ?>'],fnCreatedCell:function(nTd,sData,oData,iRow,iCol){var html=[];$.each(oData.<?php echo $col; ?>_data,function(i,v){html.push(v.text)});$(nTd).html(html.join("<br/>"));}},
<?php } ?>
<?php } ?>
<?php foreach (array("shipping", "subtract", "stock_status", "tax_class", "length_class", "weight_class", "manufacturer", "date_added", "date_modified", "date_available") as $col) { ?>
<?php if (in_array($col, $columns)) { ?>
{aTargets:['col_<?php echo $col; ?>'],fnCreatedCell:function(nTd,sData,oData,iRow,iCol){$(nTd).html(oData.<?php echo $col; ?>_text);}},
<?php } ?>
<?php } ?>
<?php if (in_array("quantity", $columns)) { ?>
{aTargets:["col_quantity"],fnCreatedCell:function(a,t){var s=$("<span/>").html(t);t=parseInt(t),0>t?s.addClass("text-danger"):5>t?s.addClass("text-warning"):s.addClass("text-success"),$(a).html(s)}},
<?php } ?>
{"bSortable":false,"aTargets":<?php echo $non_sortable_columns; ?>},{"bSearchable":false,"aTargets":['col_selector','col_action']}
],"aoColumns":[
<?php foreach ($column_classes as $idx => $class) { ?>
<?php if ($class) { ?>
{<?php if (!in_array($columns[$idx], array())) { ?>"mData":"<?php echo $columns[$idx]; ?>",<?php } ?>"sName":"<?php echo $columns[$idx]; ?>","sClass":"<?php echo $class; ?>"},
<?php } else { ?>
{<?php if (!in_array($columns[$idx], array())) { ?>"mData":"<?php echo $columns[$idx]; ?>",<?php } ?>"sName":"<?php echo $columns[$idx]; ?>"},
<?php } ?>
<?php } ?>
]});
bull5i.oTable=oTable;
$(".sortable").sortable({axis:"y",opacity:.8,containment:"parent",forceHelperSize:!0,forcePlaceholderSize:!0,cursor:"move",helper:function(t,e){return e.children().each(function(){$(this).width($(this).width())}),e},update:function(){$("tr",$(this)).each(function(t){$("input.index-column",$(this)).val(t)}),$("tr td",$(this)).each(function(){$(this).css({width:"",opacity:"","z-index":""})})}}).disableSelection();
  <?php foreach ($typeahead as $column => $attr) { ?>
    <?php switch ($column) {
      case 'category':?>
$('.<?php echo $column; ?>.typeahead').typeahead({name:'<?php echo $column; ?>',<?php if (isset($attr['prefetch'])) { ?>prefetch:'<?php echo $attr['prefetch']; ?>',<?php }; if (isset($attr['remote'])) { ?>remote:'<?php echo $attr['remote']; ?>',<?php } ?>limit:10,template:['<p><span style="white-space:nowrap;">{{path}}<strong>{{value}}</strong></span></p>'].join(''),engine:Hogan});
        <?php break;
      case 'filter':?>
$('.<?php echo $column; ?>.typeahead').typeahead({name:'<?php echo $column; ?>',<?php if (isset($attr['prefetch'])) { ?>prefetch:'<?php echo $attr['prefetch']; ?>',<?php }; if (isset($attr['remote'])) { ?>remote:'<?php echo $attr['remote']; ?>',<?php } ?>limit:10,template:['<p><span style="white-space:nowrap;">{{group}}<strong>{{value}}</strong></span></p>'].join(''),engine:Hogan});
        <?php break;
      case 'name':
      case 'model':
      case 'sku':
      case 'upc':
      case 'ean':
      case 'jan':
      case 'isbn':
      case 'mpn':
      case 'location':
      case 'seo':
      case 'download':
      case 'manufacturer':?>
$('.<?php echo $column; ?>.typeahead').typeahead({name:'<?php echo $column; ?>',<?php if (isset($attr['prefetch'])) { ?>prefetch:'<?php echo $attr['prefetch']; ?>',<?php }; if (isset($attr['remote'])) { ?>remote:'<?php echo $attr['remote']; ?>',<?php } ?>limit:10,template:['<p><span style="white-space:nowrap;">{{value}}</span></p>'].join(''),engine:Hogan});
        <?php break;
      default:?>
        <?php break;
    } ?>
  <?php } ?>
if($('.typeahead.input-sm').length){$('.typeahead.input-sm').siblings('input.tt-hint').addClass('hint-small');}
$.extend($.fn.select2.defaults,{formatNoMatches:function(){return"<?php echo addslashes($text_no_matches_found); ?>"},formatInputTooShort:function(e,t){var o=t-e.length;return"<?php echo addslashes($text_input_too_short); ?>".replace("%d",o)},formatInputTooLong:function(e,t){var o=e.length-t;return"<?php echo addslashes($text_input_too_long); ?>".replace("%d",o)},formatSelectionTooBig:function(e){return"<?php echo addslashes($text_selection_too_big); ?>".replace("%d",e)},formatLoadMore:function(){return"<?php echo addslashes($text_loading_more_results); ?>"},formatSearching:function(){return"<?php echo addslashes($text_searching); ?>"}}),void 0!=moment&&moment.lang("<?php echo $code; ?>");
function show_quick_edit_modal(i,o){if(i.success)$("#actionQuickEditModal .modal-body form fieldset").html(i.data),i.title&&$("#actionQuickEditModal .modal-title").html(i.title),$("#actionQuickEditModal").modal("show"),$("#actionQuickEditModal").on("click",".modal-footer .cancel",function(){xhr&&xhr.abort()}).on("click",".modal-footer .submit",function(){if("function"==typeof o){if("undefined"!=typeof CKEDITOR&&CKEDITOR.instances)for(var i in CKEDITOR.instances)CKEDITOR.instances[i].updateElement();var t=$("#actionQuickEditModal .modal-body form").serializeObject();o.call(this,t),"undefined"!=typeof CKEDITOR&&CKEDITOR.instances&&(CKEDITOR.instances={})}else $(this).closest(".modal").modal("hide")}),$("#actionQuickEditModal").on("hide.bs.modal",function(){$("#actionQuickEditModal").off("hide.bs.modal")}).on("hidden.bs.modal",function(){$("#actionQuickEditModal .modal-body form fieldset").empty(),$("#actionQuickEditModal .modal-title").empty(),$("#actionQuickEditModal").off("hidden.bs.modal"),$("#actionQuickEditModal").off("click",".modal-footer .cancel"),$("#actionQuickEditModal").off("click",".modal-footer .submit")});else{var t=!0;i.alerts&&$.each(i.alerts,function(i,o){"object"==typeof o?$.each(o,function(o,a){a&&(bull5i.display_alert(a,i,0,t),t=!1)}):o&&(bull5i.display_alert(o,i,0,t),t=!1)}),i.msg&&bull5i.display_alert(i.msg,"error",0,t)}}
$("body").on("click","td button.action",function(e){var t=$(this).closest("td").get(0),a=$(this).attr("data-column"),s=oTable.fnGetPosition(t),l=(oTable.fnGetData(t),oTable.fnSettings()),i=l.aoData[s[0]]._aData.id;void 0!=i&&void 0!=a&&(bull5i.processing&&bull5i.processing(!0),$.when($.ajax({url:"<?php echo $load; ?>",type:"POST",cache:!1,dataType:"json",data:{id:i,column:a}})).then(function(t,s,l){show_quick_edit_modal(t,function(t){var s=$(this).closest(".modal"),n=$($(this).attr("data-form")),o=s.find(".aqe-overlay"),d=$(this),t={id:i,column:a,value:$.isEmptyObject(t)?null:t,old:""};$("#batch-edit").is(":checked")&&$("input[name*='selected']:checked").length&&(t.ids=$("input[name*='selected']:checked").serializeObject().selected),l=$.ajax({type:"POST",url:"<?php echo $update; ?>",dataType:"json",data:t,beforeSend:function(){o&&(o.css("display","block"),setTimeout(function(){o.addClass("in")},0)),$("fieldset",n).attr("disabled",!0),d.attr("disabled",!0)},success:function(e){$(".has-error span.help-block",n).remove(),$("div.form-group.has-error",n).removeClass("has-error"),$("div.notice",s).empty(),e&&e.success?(s.modal("hide"),(e.alerts||e.msg)&&bull5i.display_alert&&setTimeout(function(){var t=!0;e.msg&&(bull5i.display_alert(e.msg,"success",5e3,t),t=!1),e.alerts&&$.each(e.alerts,function(e,a){"object"==typeof a?$.each(a,function(a,s){s&&(bull5i.display_alert(s,e,0,t),t=!1)}):a&&(bull5i.display_alert(a,e,0,t),t=!1)})},500),updateRelated(a,e.results.done)):(e.errors&&$.each(e.errors,function(e,t){$el=$("[name='"+e+"']",n),$el.closest(".form-group").addClass("has-error"),$el.parent().append($("<span/>",{"class":"help-block"}).html(t))}),e.alerts&&$.each(e.alerts,function(e,t){"object"==typeof t?$.each(t,function(t,a){a&&(bull5i.alert_classes[e]?$("<div/>",{"class":"fade in alert "+bull5i.alert_classes[e]}).html(a).prepend($("<button/>",{type:"button","class":"close","data-dismiss":"alert","aria-hidden":"true"}).html("&times;")).appendTo($("div.notice",s)):$("<div/>",{"class":"fade in alert alert-danger"}).html(a).prepend($("<button/>",{type:"button","class":"close","data-dismiss":"alert","aria-hidden":"true"}).html("&times;")).appendTo($("div.notice",s)))}):t&&(bull5i.alert_classes[e]?$("<div/>",{"class":"fade in alert "+bull5i.alert_classes[e]}).html(t).prepend($("<button/>",{type:"button","class":"close","data-dismiss":"alert","aria-hidden":"true"}).html("&times;")).appendTo($("div.notice",s)):$("<div/>",{"class":"fade in alert alert-danger"}).html(t).prepend($("<button/>",{type:"button","class":"close","data-dismiss":"alert","aria-hidden":"true"}).html("&times;")).appendTo($("div.notice",s)))}))},complete:function(){function e(){o.css("display","none")}o&&(o.removeClass("in"),$.support.transition&&o.hasClass("fade")?o.one($.support.transition.end,e).emulateTransitionEnd(500):e()),$("fieldset",n).attr("disabled",!1),d.attr("disabled",!1)}}),e.preventDefault()})},function(){bull5i.display_alert&&bull5i.display_alert(bull5i.texts.error_ajax_request,"error",0)}).always(function(){bull5i.processing&&bull5i.processing(!1)}))});
});
}(window.bull5i=window.bull5i||{},jQuery));
//--></script>
<?php echo $footer; ?>
