var base = $('base').attr('href');
var separator = '<span style="visibility: hidden;">&nbsp;&nbsp;&nbsp;&raquo;&raquo;&raquo;&nbsp;&nbsp;&nbsp;';

$(function() {
	$('.osp-live').each(function(){
		
		var t = $(this);
		var osct = parseInt(t.attr('data-status-change-to'));
		
		if(osct > 0) {
			t.html(t.html() + separator + osp[osct].name + '</span>');
		}
		
		$(this).hover(function(){
			$(this).find('span').css('visibility', 'visible');
		}, function(){
			$(this).find('span').css('visibility', 'hidden');
		});
		
		$(this).css('cursor', 'pointer').on('dblclick.jg', function() {
			var t = $(this);
			var oid = t.attr('data-order-id');
			var osct = parseInt(t.attr('data-status-change-to'));
			
			$.getJSON(
				base + 'index.php',
				{
					'route'				: 'sale/order/liveUpdate',
					'token'				: token,
					'order_id'			: oid,
					'status_change_to'	: osct
				},
				function(data) {
					
				}
			);
			
			var o = osp[osct];
			
			t.html(o.name);
			
			t.css({
				'backgroundColor'	: (o.bgcolor == null ? '#ffffff' : o.bgcolor),
				'color'				: (o.fgcolor == null ? '#000000' : o.fgcolor)
			});
			
			if(o.status_change_to > 0) {
				t.attr('data-status-change-to', o.status_change_to);
				t.html(t.html() + separator + osp[o.status_change_to].name + '</span>');
			} else {
				t.removeAttr('data-status-change-to');
				t.css('cursor', 'inherit');
				t.removeClass('osp-live').off('dblclick.jg');
			}
			
		});
	});
});