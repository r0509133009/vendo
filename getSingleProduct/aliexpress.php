<?php


function mspro_aliexpress_title($html){

		$res = explode('itemprop="name">' , $html , 2);
		if(count($res) > 1){
			$res = explode('</h' , $res[1] , 2);
			if(count($res) > 1){
				return $res[0];
			}
		}
		
		$instruction = 'h1#product-name';
        $parser = new nokogiri($html);
        $data = $parser->get($instruction)->toArray();
        //echo '<pre>'.print_r($data , 1).'</pre>';exit;
        unset($parser);
	    if (isset($data['#text']) && !is_array($data['#text'])) {
	    	return trim(str_replace(array("&nbsp;" , "&amp;") , array(" " , "`" ) ,$data['#text']));
        }
 		if (isset($data[0]['#text']) && !is_array($data[0]['#text'])) {
 			//echo $data[0]['#text'];exit;
	    	return trim(str_replace(array("&nbsp;" , "&amp;") , array(" " , "`" ) ,$data[0]['#text']));
        }
        
		$instruction = 'h1.product-name';
        $parser = new nokogiri($html);
        $data = $parser->get($instruction)->toArray();
        //echo '<pre>'.print_r($data , 1).'</pre>';exit;
        unset($parser);
	    if (isset($data['#text']) && !is_array($data['#text'])) {
	    	return trim(str_replace(array("&nbsp;" , "&amp;") , array(" " , "`" ) ,$data['#text']));
        }
 		if (isset($data[0]['#text']) && !is_array($data[0]['#text'])) {
 			//echo $data[0]['#text'];exit;
	    	return trim(str_replace(array("&nbsp;" , "&amp;") , array(" " , "`" ) ,$data[0]['#text']));
        }
        return '';
}

function mspro_aliexpress_description($html){
		$out = '';
		//echo $html;exit;
		
		// Item Specific
		preg_match_all('|<div class="params">(.*)</div>|isU', $html, $result, PREG_SET_ORDER);
		if(isset($result[0][1])){
			$out .= $result[0][1];
		}
		
		//   КАКОГО ТО ХУЯ ЭТО ПЕРЕСТАЛО РАБОТАТЬ
		// product-params if <div class="params"> not available
		$pq = phpQuery::newDocumentHTML($html);
		$temp  = $pq->find('div.product-params');
		foreach ($temp as $block){
			$out .= $temp->html();
		}
		
		// Item Description
		$pq = phpQuery::newDocumentHTML($html);
		$temp  = $pq->find('div#custom-description');
		foreach ($temp as $block){
			$t_res = $temp->html();
			// убрать ссылки из рекламного блока
			$t_res = preg_replace ("!<a.*?href=\"?'?([^ \"'>]+)\"?'?.*?>(.*?)</a>!is", "\\2", $t_res);
			$out .= $t_res;
		}
		
		// try to get custom description by ajax
		$prod_id = explode('id="hid-product-id" value="' , $html , 2);
		if(count($prod_id) > 1){
			$prod_id = explode('"' , $prod_id[1] , 2);
			if(count($prod_id) > 1){
				$custom_desc_url = 'http://www.aliexpress.com/getDescModuleAjax.htm?productId=' . $prod_id[0] . '&productSrc=is';
				$custom_desc_res = getUrl($custom_desc_url);
				if($custom_desc_res){
					$custom_desc_res = str_ireplace(array("window.productDescription='") , array("") , $custom_desc_res);
					$custom_desc_res = substr(trim($custom_desc_res) , 0 , -2);
					// убрать ссылки из рекламного блока
					$custom_desc_res = preg_replace ("!<a.*?href=\"?'?([^ \"'>]+)\"?'?.*?>(.*?)</a>!is", "\\2", $custom_desc_res);
					$out = str_replace(array('<div class="loading32 desc-loading"></div>') , array($custom_desc_res) , $out);
				}
			}
		}

		// Packaging Details
		$pq = phpQuery::newDocumentHTML($html);
		$temp  = $pq->find('div#pdt-pnl-packaging');
		foreach ($temp as $block){
			$out .= $temp->html();
		}
		
		// ТЕПЕРЬ ПРОБУЕМ ТАК:
		if(strlen($out) < 10){
			$res = explode('<div id="product-desc" class="product-desc">' , $html);
			if(count($res) > 1){
				$res = explode('<div id="transaction-history">' , $res[1]);
				if(count($res) > 1){
					$out .= $res[0];
				}
			}
		}
		
		//echo $out;exit;
        return $out;
}


function mspro_aliexpress_price($html){
	
		$instruction = 'span[itemprop=price]';
        $parser = new nokogiri($html);
        $data = $parser->get($instruction)->toArray();
        //echo '<pre>'.print_r($data , 1).'</pre>';exit;
        if (isset($data[0]['#text']) && !is_array($data[0]['#text']) ) {
         	$price = mspro_aliexpress_prepare_price( $data[0]['#text'] );
        	return $price;
         }
	
	
		$instruction = 'input#sku-price-store';
        $parser = new nokogiri($html);
        $data = $parser->get($instruction)->toArray();
        unset($parser);
        if (isset($data['value']) && !is_array($data['value']) ) {
        	$price = mspro_aliexpress_prepare_price( $data['value'] );
        	return $price;
        }

        $instruction = 'span#sku-price';
        $parser = new nokogiri($html);
        $data = $parser->get($instruction)->toArray();
        //echo '<pre>'.print_r($data , 1).'</pre>';exit;
        unset($parser);
        if ($data){
        	if(isset($data[0]['span'][0]['#text']) && !is_array($data[0]['span'][0]['#text'])){
        		$price = mspro_aliexpress_prepare_price( $data[0]['span'][0]['#text'] );
        		return $price;
        	}elseif(isset($data['span'][0]['#text']) && !is_array($data['span'][0]['#text'])){
        		$price = mspro_aliexpress_prepare_price( $data['span'][0]['#text'] );
        		return $price;
        	}elseif(isset($data['#text']) && !is_array($data['#text'])){
        		$price = mspro_aliexpress_prepare_price( $data['#text'] );
        		return $price;
        	}elseif(isset($data[0]['#text']) && !is_array($data[0]['#text'])){
        		$price = mspro_aliexpress_prepare_price( $data[0]['#text'] );
        		return $price;
        	}
        }
        
        return '';
}

function mspro_aliexpress_prepare_price($price){
	$res = preg_replace("/[^0-9,.]/", "",  $price);
	$res = str_replace("," , "." , $res);
	$res = round( (float) $res , 2);
	return $res;
}


function mspro_aliexpress_sku($html){
		$res =  explode('<input type="hidden" name="objectId" value="' , $html);
       if(count($res) > 1){
       		$res = explode('"' , $res[1]);
       		if(count($res) > 1){
       			return $res[0];	
       		}
       		 
       }
       return '';
}

function mspro_aliexpress_model($html){
	return mspro_aliexpress_sku($html);
}


function mspro_aliexpress_meta_description($html){
	   $res =  explode('<meta name="description" content="' , $html);
       if(count($res) > 1){
       		$res = explode('"' , $res[1]);
       		if(count($res) > 1){
       			return str_replace(array("&nbsp;" , "&amp;") , array(" " , "`") , $res[0]);	
       		}
       		 
       }
       return '';
}

function mspro_aliexpress_meta_keywords($html){
       $res =  explode('<meta name="keywords" content="' , $html);
       if(count($res) > 1){
       		$res = explode('"' , $res[1]);
       		if(count($res) > 1){
       			return str_replace(array("&nbsp;" , "&amp;") , array(" " , "`") , $res[0]);	
       		}
       		 
       }
       return '';
}


function mspro_aliexpress_main_image($html){
    $arr = mspro_aliexpress_get_images($html);
    if(isset($arr[0]) && strlen($arr[0]) > 0){
        return $arr[0];
    }
    return '';
}



function mspro_aliexpress_other_images($html){
    $arr = mspro_aliexpress_get_images($html);
    if(count($arr) > 1){
        unset($arr[0]);
        return $arr;
    }
    return array();
}


function mspro_aliexpress_get_images($html){
        $out = array();
		// imageURL
		$start = strpos($html, 'imageURL');
        $end = strpos($html, ']', $start);
        
        
        if($start > 0){
	        $images = substr($html, $start, $end - $start);
	        $images = substr($images, strpos($images, '[') + 1);
	        //$images = str_replace("'", '"', $images);
	        $images = explode(',', $images);
	
	        if (count($images) > 0){
		        foreach ($images as $index => $value) {
		            $images[$index] = trim(str_replace(array('"' , "'"), array('' , ''), $value));
		        }
	        }
	        $images = ali_prepare_img_array($images);
	        if (count($images) > 0) {
	            foreach($images as $image){
	                $out[] = $image;
	            }
	        }
        }
        
        //imageBigViewURL
		$start = strpos($html, 'imageBigViewURL');
        $end = strpos($html, ']', $start);

        if($start > 0){
	        $images = substr($html, $start, $end - $start);
	        $images = substr($images, strpos($images, '[') + 1);
	        //$images = str_replace("'", '"', $images);
	        $images = explode(',', $images);
	
	        if (count($images) > 0){
		        foreach ($images as $index => $value) {
		            $images[$index] = trim(str_replace(array('"' , "'"), array('' , ''), $value));
		        }
			}
	        $images = ali_prepare_img_array($images);
	        if (count($images) > 0) {
	           foreach($images as $image){
	                $out[] = $image;
	            }
	        }
        }
        
        // ADDITIONAL IMAGES FOR DIFFERENT  COLORS
        /*$instruction = 'ul#sku-color li a.sku-value';
        $parser = new nokogiri($html);
        $data = $parser->get($instruction)->toArray();
        //echo '<pre>'.print_r($data , 1).'</pre>';exit;
        unset($parser);
        if(isset($data) && is_array($data) && count($data) > 0){
            foreach($data as $pos_image){
                if(isset($pos_image['img']['bigpic']) && !is_array($pos_image['img']['bigpic']) && strlen(trim($pos_image['img']['bigpic'])) > 0){
                    $out[] = $pos_image['img']['bigpic'];
                }
            }
        }*/
        
        $out = array_unique($out);
        //echo '<pre>'.print_r($out , 1).'</pre>';exit;

        return $out;
}


function ali_prepare_img_array($arr){
	foreach($arr as $key => $val){
		if(strpos($val , "f IE" ) > 0){
			unset($arr[$key]);
		}else{
			$arr[$key] = str_ireplace(array("_350x350.jpg") , array("") , $val);
		}
	}
	return (array) $arr;
}



function mspro_aliexpress_options($html){
	$out = array();

	// get prices block
	$originalPrice = mspro_aliexpress_price($html);
	//echo $originalPrice;
	$priceBlockSemafor = false;
	$priceBlockRes = explode('skuProducts=' , $html , 2);
	if(count($priceBlockRes) > 1){
		$priceBlockRes = explode('skuAttrIds' , $priceBlockRes[1] , 2);
		if(count($priceBlockRes) > 1){
			$priceBlockRes = $priceBlockRes[0];
			$priceBlockSemafor = true;
		}
	}
	
	
	$instruction = 'div#product-info-sku';
	$parser = new nokogiri($html);
	$res = $parser->get($instruction)->toArray();
	//echo '<pre>s'.print_r($res , 1).'</pre>';exit;
	unset($parser);
	
	if(isset($res[0]['dl']) && is_array($res[0]['dl']) && count($res[0]['dl'])> 0){
		$res = $res[0]['dl'];
		$optionNumber = 1;
		foreach($res as $pos_option){
		    //echo '<pre>'.print_r($pos_option , 1).'</pre>';exit;
			if(isset($pos_option['dt'][0]['#text']) && !is_array($pos_option['dt'][0]['#text']) && isset($pos_option['dd'][0]['ul'][0]['li']) && is_array($pos_option['dd'][0]['ul'][0]['li']) && count($pos_option['dd'][0]['ul'][0]['li']) > 0){
				//$opt_name = str_replace( array(":") , array("") , ali_options_get_name($optionNumber , $html) );
				$opt_name = str_replace( array(":") , array("") , trim($pos_option['dt'][0]['#text']) );
				$optionNumber++;
				$opt_values = $pos_option['dd'][0]['ul'][0]['li'];
				$OPTION = array();
				$OPTION['name'] = $opt_name;
				$OPTION['type'] = "select";
				$OPTION['required'] = true;
				$OPTION['values'] = array();
				foreach($opt_values as $option_value){
					if(isset($option_value['a']['span']['#text']) && !is_array($option_value['a']['span']['#text']) && isset($option_value['a']['id']) && !is_array($option_value['a']['id']) ){
						$opt_SKU = trim($option_value['a']['id']);
						$optPrice = 0;
						if($priceBlockSemafor && $optionNumber < 3){
							$optPrice = ali_options_get_price($opt_SKU , $priceBlockRes , $originalPrice);
						}
						$optName = $option_value['a']['span']['#text'];
						if($optName){
							$OPTION['values'][] = array(
									'name' =>  $optName,
									'price' => $optPrice
							);
						}
					}elseif(isset($option_value['a']['title']) && !is_array($option_value['a']['title']) && isset($option_value['a']['id']) && !is_array($option_value['a']['id']) ){
					    $opt_SKU = $option_value['a']['id'];
					    $optPrice = 0;
					    if($priceBlockSemafor && $optionNumber < 3){
					        $optPrice = ali_options_get_price($opt_SKU , $priceBlockRes , $originalPrice);
					    }
					    $optName = $option_value['a']['title'];
					    if($optName){
					        $OPTION['values'][] = array(
					            'name' =>  $optName,
					            'price' => $optPrice
					        );
					    }
					}
				}
				//echo '<pre>s'.print_r($OPTION , 1).'</pre>';exit;
				if(count($OPTION['values']) > 0){
					$out[] = $OPTION;
				}
				//echo '<pre>'.print_r($opt_values , 1).'</pre>';exit;
			}
		}
		//echo '<pre>'.print_r($res , 1).'</pre>';exit;
	}
	
	
	
	//echo '<pre>'.print_r($out , 1).'</pre>';exit;
	return $out;
}



function ali_options_get_price($sku , $priceBlock , $price){
	$sku_res = explode('-' , $sku);
	if(count($sku_res) > 1){
		$sku = $sku_res[count($sku_res) - 1];
	}
	
	$res = explode(':'.$sku , $priceBlock);
	if(count($res) > 1){
		$res = $res[1];
		$res = explode('actSkuMultiCurrencyCalPrice":"' , $res , 2);
		if(count($res) > 1){
			$res = explode('"' , $res[1] , 2);
			if(count($res) > 1){
				$res = (float) $res[0];
				//echo 'res - '. $res .'<br />';
				return round( ($res - $price) , 2);
			}
		}
	}
	
	return 0;
}



function ali_options_get_name($num , $html){
	$res = explode('<dt class="pp-dt-ln sku-title">' , $html);
	if(isset($res[$num])){
		$res = explode('</dt>' , $res[$num] , 2);
		if(count($res) > 1){
			return trim($res[0]);
		}
	}
	return '';
}

function ali_option_value_get_name($sku , $html){
	$res = explode('<a class="sku-value attr-checkbox" id="'.$sku.'" href="javascript:void(0)"><span>' , $html  , 2);
	if(count($res) > 1){
		$res = explode('</span>' , $res[1] , 2);
		if(count($res) > 1){
			return trim($res[0]);
		}
	}
	return false;
}






