<?php
class ModelCatalogRandomizer extends Model {

	// 'randomization' => array(
	// 				'page' => $page,
	// 				'limit' => $limit,
	// 				'xpage' => isset($this->request->get["xpage"]) ? $this->request->get["xpage"] : null
	// 				),

	protected $_data = array();

	protected $_category_id = null;

	protected $_category_pages = array();

	protected $_page = 1;

	protected $_limit = 20;

	public function __construct ($registry) {

		parent::__construct($registry);

		if(!isset($this->session->data['randomization'])) {
			$this->session->data['randomization'] = array();
		}
	}

	public function setData ($data) {
		$this->_data = $data;
		//file_put_contents('1.txt', json_encode($this->_data).PHP_EOL.__LINE__.PHP_EOL,FILE_APPEND);
		return $this;
	}

	public function setCategory($category_id) {

		$this->_category_id = $category_id;
		if( !isset($this->session->data['randomization'][$category_id])) {
			$this->session->data['randomization'][$category_id] = array();
		}

		return $this;
	}

	public function setUp() {
		$this->_page = isset($this->_data['xpage']) ? $this->_data['xpage'] : $this->_data['page'];
		$this->_limit = $this->_data['limit'];
	
		for ($pages = count($this->session->data['randomization'][$this->_category_id]); $pages  >= 0 ; --$pages) { 
			if($pages > $this->_page){
				unset($this->session->data['randomization'][$this->_category_id][$pages]);
			}else{
				if($this->_page == 1) {
					$this->session->data['randomization'][$this->_category_id][$this->_page] = array(); 
					break;
				}
				for ($i=1; $i <= $this->_page ; ++$i) { 
					if(!isset($this->session->data['randomization'][$this->_category_id][$i]) || isset($this->_data['xpage']) ) {
						$this->session->data['randomization'][$this->_category_id][$i] = array(); 
					}
				}
				break;
			}
			
		}

		return $this;
	}


	public function pushProduct($product_id) {
		foreach ($this->session->data['randomization'][$this->_category_id] as $page => $products) {
			if(count($this->session->data['randomization'][$this->_category_id][$page]) < $this->_limit){
				array_push($this->session->data['randomization'][$this->_category_id][$page], $product_id);
				break;
			}
		}

	}

	// public function __destruct(){
	// 		// $this->session->data['randomization'][$this->_category_id] = array_merge($this->session->data['randomization'][$this->_category_id],$this->_category_pages);
	// 	//$this->session->data['randomization'][$this->_category_id] = $this->_category_pages;
	// 	file_put_contents('1.txt', json_encode($this->session->data['randomization'][$this->_category_id]).PHP_EOL.__LINE__.PHP_EOL,FILE_APPEND);
	// }

	public function getPredictate() {
		$predictate = '';

		if(isset($this->_data['xpage'])
		   && isset($this->session->data['randomization'][$this->_category_id]) 
		   && count($this->session->data['randomization'][$this->_category_id])) {
		   		$products = $this->getProductsIds();
		   		if($products){
		   			$predictate .= "AND p.product_id IN (" ;
					$predictate .= $this->getProductsIds();
					$predictate .= ")";
		   		}
				
		}elseif($this->_data['page'] > 1){
			if(isset($this->session->data['randomization'][$this->_category_id]) 
		   		&& count($this->session->data['randomization'][$this->_category_id])) {
				$products = $this->getProductsIds();
				if($products){
					$predictate = "AND p.product_id NOT IN (";
					$predictate .= $products;
					$predictate .= ")";
				}
				
			}
		}

		//file_put_contents('1.txt',  $predictate.PHP_EOL.__LINE__.PHP_EOL,FILE_APPEND);
		return $predictate;
	}

	protected function getProductsIds(){
		$comma = false;
		$predictate = '';
		foreach ($this->session->data['randomization'][$this->_category_id] as $page => $products) {
			if($page < $this->_page && count($products)) {
				if(!$comma){	
					$comma = !$comma;
				}else{
					$predictate .= ",";
				}
				$predictate .= implode(',', $products);
			}
		}
		return $predictate;
	}

	public function log() {
		 // $this->session->data['randomization'][$this->_category_id] = array_merge($this->session->data['randomization'][$this->_category_id],$this->_category_pages);
		//file_put_contents('1.txt', json_encode($this->session->data['randomization']).PHP_EOL.__LINE__.PHP_EOL,FILE_APPEND);
		// 		 $this->session->data['randomization'][$this->_category_id] = array_replace($this->session->data['randomization'][$this->_category_id],$this->_category_pages);
		//file_put_contents('1.txt', json_encode($this->session->data['randomization'][$this->_category_id]).PHP_EOL.__LINE__.PHP_EOL,FILE_APPEND);
		
	}
}