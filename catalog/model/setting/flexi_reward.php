<?php
////////////////////
// FRONT END 15

// BUG - 02:50 ч. 15.1.2016 г. 
/// front end points differ from back end!!!!!

class ModelSettingFlexiReward extends Model {
	public $action_order_id = array(
		'registration'=>'-1000000',
		'newsletter_sign_up'=> '-2000000',
		'productreview'=> '-3000000',
		'firstorder'=>'-4000000'
		);
	// flexi_registration_points
	
	public function addReward($action_label) {
		// If PayPal sends callback user won't be logged...
		//if (!$this->customer->isLogged()) 		return false;
		// TODO:: FIND potential issues after removing this check
		
        if(!array_key_exists($action_label,$this->action_order_id))	{
			// Real Order 
			////////////////

			$order_id = $action_label;
			$order_info = $this->getOrder($order_id);
		
			// Not a valid order - abort
			if(!$order_info) return false;
			
			$flexi_sett 	= $this->config->get('flexi_reward_'.$order_info['customer_group_id']) ; 
			
			$reward_points = 0;
			
			if ($flexi_sett['flexi_reward_status']) {
				$this->language->load('module/flexi_reward');
				
				//Checking for already added reward points
				if ($this->checkForAlreadyAddedRewards($order_id, $flexi_sett['flexi_reward_order_status'],$order_info['customer_id'])) {
					return $this->language->get('error_reward_added');
				}
				
				// Add Rewards for first order
				$this->addRewardFirstOrder($order_info['customer_id'], $order_info['customer_group_id'],$order_id) ;
				
				// Points to add
				$reward_points = $this->calcReward($order_id,  $order_info['customer_group_id'], $order_info['customer_id']);
			}
		
			if($reward_points > 0)	 {
				$this->db->query("INSERT INTO " . DB_PREFIX . "customer_reward SET customer_id = '" . (int)$order_info['customer_id'] . "', order_id = '" . (int)$order_id . "', points = '" . (int)$reward_points . "', description = '" . $this->db->escape($this->language->get('text_order_id') . ' #' . $order_id) . "', date_added = NOW()");
			return true;
			} else {

			return false;			
			
				
			}
				
			// END or real order rewards
		} else {
			// Customer action rewards
			///////////////////////////
			$flexi_sett 	= $this->config->get('flexi_reward_'.$this->customer->getCustomerGroupId()) ; 
			
			$this->language->load('module/flexi_reward');
			$customer_id	= (int)$this->customer->getId();
			$order_id       = $this->action_order_id[$action_label];
			$points 		= isset($flexi_sett['flexi_'.$action_label.'_points']) ? $flexi_sett['flexi_'.$action_label.'_points'] : 0 ;
			$description 	= isset($flexi_sett['flexi_'.$action_label.'_desc']) ? $flexi_sett['flexi_'.$action_label.'_desc'] : '' ;
	
			if(!$description)		$description 	= $this->language->get('text_flexi_'.$action_label) ? $this->language->get('text_flexi_'.$action_label) : $action_label ;
			if( $this->getTotalCustomerRewardsByOrderId( $this->action_order_id[$action_label] , $customer_id) > 0)		return false;
	
			//flexi_registration_points
			if($points > 0)	$this->db->query("INSERT INTO " . DB_PREFIX . "customer_reward SET customer_id = '" . (int)$customer_id . "', order_id = '" . (int)$order_id . "', points = '" . (int)$points . "', description = '" . $this->db->escape($description) . "', date_added = NOW()");
			return true;
		}
	}



	// First order
    public function addRewardFirstOrder($customer_id=false, $customer_group_id=false,$order_id=false) {
		$flexi_sett = $this->config->get('flexi_reward_'.$customer_group_id);

		if($flexi_sett['flexi_firstorder_points'] == '0') {
		   return false;
		}

		if( $this->getTotalCustomerRewardsByOrderId( ($this->action_order_id['firstorder']), $customer_id ) > 0){
		   return false;
		}
////////////// Order amount
		$order_total_info = $this->getOrderTotals($order_id);
		$order_sub_total = 0;
		$order_coupon_amount = 0;
		$order_case_total = 0;
		$order_voucher_amount = 0;
		$order_reward  = 0;
		foreach ($order_total_info as $order_total) {
//			var_dump('$order_total',$order_total);
			switch ($order_total['code']) {
				case 'sub_total':
					$order_sub_total = $order_total['value'];
					break;
				case 'coupon':
					$order_coupon_amount =  $order_total['value'];
					break;
				case 'voucher':
					$order_voucher_amount =  $order_total['value'];
					break;
				case 'reward':
					$order_reward =  $order_total['value'];
					break;
				case 'total':
					$order_case_total =  $order_total['value'];
					break;
					
			}
		}
		//TODO:::  REWARD not included --- why?
		$cart_subtotal = $order_reward + $order_sub_total + $order_coupon_amount + $order_voucher_amount;
		$order_amount = $cart_subtotal;
		
		// Redeem discount case where total is 42 and subtotal 60
		// Set Total if setting says so 
		if($order_case_total < $cart_subtotal || $flexi_sett['flexi_total_subtotal_operator'] == 1){
			$order_amount = $order_case_total;
		}

////////////// END order amount


		$this->language->load('module/flexi_reward');
		$description 	= isset($flexi_sett['flexi_firstorder_desc']) ? $flexi_sett['flexi_firstorder_desc'] : '' ;

		if(!$description){
			$description 	= $this->language->get('text_flexi_firstorder') ? $this->language->get('text_flexi_firstorder') : 'first order' ;
		}
		if($flexi_sett['flexi_firstorder_operator'] > 0) 	{
			$points = $flexi_sett['flexi_firstorder_points'];
		} else {
			// We assume we get final order_amount wheter total / sub-total from addReward.
			$points = round($order_amount * $flexi_sett['flexi_firstorder_points'] / 100.00);
		}
 		
		if($points < 1) return false;
		
 //       $this->load->model('sale/customer');
 //       $this->model_sale_customer->addReward($customer_id, $description . ' #'.$order_id, $points, ($this->reli['firstorder']));
		if($points > 0)	$this->db->query("INSERT INTO " . DB_PREFIX . "customer_reward SET customer_id = '" . (int)$customer_id . "', order_id = '" . (int)$this->action_order_id['firstorder'] . "', points = '" . (int)$points . "', description = '" . $this->db->escape($description . ' #' . $order_id) . "', date_added = NOW()");


		return true;
	}



	// Calculate and return
	public function calcReward($order_id, $customer_group_id=false, $customer_id) {
		if($customer_group_id === false) $customer_group_id = $this->customer->getCustomerGroupId();
		$flexi_sett 	= $this->config->get('flexi_reward_'.  $customer_group_id ) ; 

	 	$order_products = $this->getOrderProducts($order_id);
	    $rates = $flexi_sett['flexi_reward_rate'];
	    $reward_points = 0;
		$reward_points_cur = 0;
   		
        if ($flexi_sett['flexi_reward_status']) {
//            $this->language->load('module/flexi_reward');

            $order_total_info = $this->getOrderTotals($order_id);
            $order_sub_total = 0;
            $order_coupon_amount = 0;
            $order_voucher_amount = 0;
            foreach ($order_total_info as $order_total) {
	//			var_dump('$order_total',$order_total);
                switch ($order_total['code']) {
                    case 'sub_total':
                        $order_sub_total = (float) $order_total['value'];
                        break;
                    case 'coupon':
                        $order_coupon_amount = (float) $order_total['value'];
                        break;
                    case 'voucher':
                        $order_voucher_amount = (float) $order_total['value'];
                        break;
                    case 'reward':
                        $order_reward = (float) $order_total['value'];
                        break;
                    case 'total':
                        $order_case_total = (float) $order_total['value'];
                        break;
						
                }
            }

		$cart_discounts = $order_reward + $order_coupon_amount + $order_voucher_amount;
	
			// Manufacturer and category
			$this->load->model('catalog/product');
			
			$mfc_sett = isset($flexi_sett['flexi_manufacturers']) ? $flexi_sett['flexi_manufacturers']:array();
			$cat_sett = isset($flexi_sett['flexi_categories']) ? $flexi_sett['flexi_categories']:array();
			
			// get points for sub-total  and manufacturer / category 
			foreach( $order_products as $key=>$val) {
				$reward_points_cur = 0;
				$tmp_product_info = $this->model_catalog_product->getProduct($val['product_id']);
				$tmp_product_cats = $this->getProductCategories($val['product_id']) 	;
	
				$reward_points_mfc = 0;
				$reward_points_cat = 0;
				
				foreach($mfc_sett as $mkey=>$mvalue) {
					if(in_array($tmp_product_info['manufacturer_id'],$mvalue['ids']))	{ 
						// calc rewards for this manufacturer and add them
						$reward_points_mfc += $val['total'] * $mvalue['rate'];
					}
				}
				foreach($cat_sett as $ckey=>$cvalue) {
					if(array_intersect($cvalue['ids'], $tmp_product_cats)) {
						// calc rewards for this category and add them
						$reward_points_cat += $val['total'] * $cvalue['rate'];
						break; // to avoid multiple categories adding more points I know I can do it better ;)
					}
				}
				
				if ($reward_points_cat === 0 && $reward_points_mfc === 0 ){
					$reward_points_cur += $val['total'] * $rates;
				}elseif($reward_points_cat === 0 && $reward_points_mfc > 0 ) {
					$reward_points_cur += $reward_points_mfc ;
				}elseif($reward_points_cat > 0 && $reward_points_mfc === 0 ) {
					$reward_points_cur += $reward_points_cat ;
				}elseif($reward_points_cat > 0 && $reward_points_mfc > 0 ) {
					$reward_points_cur += $this->theBigger($reward_points_cat, $reward_points_mfc);
				}

				// Ignore product rewards value
				// IMPORTANT NOTE: Options have points... but points for purchase not rewards?!!
				if(isset($flexi_sett['flexi_ignore_product_rewards_value']) && !$flexi_sett['flexi_ignore_product_rewards_value'] && $val['reward'] == 0 || $flexi_sett['flexi_ignore_product_rewards_value'] == 1) {
					$reward_points	+= $reward_points_cur;
				}elseif(isset($flexi_sett['flexi_ignore_product_rewards_value']) && !$flexi_sett['flexi_ignore_product_rewards_value'] && $val['reward'] > 0) {
					$reward_points	+= $val['reward'];
				}

			} //foreach $order_products 
        }

		// Redeem discount case where total is 42 and subtotal 60
		// Set Total if setting says so 
		if( $flexi_sett['flexi_total_subtotal_operator'] == 1){
			// Total
			$reward_points = $reward_points * ($order_case_total / $order_sub_total);
		}	else {
			// Sub-total - finalize reward points by handling discounts
			$reward_points += $cart_discounts * $rates; 
		}
		
        if ($reward_points > 0) {
            return round($reward_points);
        } 

	return 0;
	}

	public function getOrderProducts($order_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");
		
		return $query->rows;
	}
	
	public function getProductCategories($product_id) {
		$product_category_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");

		foreach ($query->rows as $result) {
			$product_category_data[] = $result['category_id'];
		}

		return $product_category_data;
	}
	
	public function getOrderTotals($order_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_total WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order");

		return $query->rows;
	}

    public function theBigger($a,$b) {
		if($a > $b) return $a;
		if($b > $a) return $b;
	}
	
	public function getDaysSinceLastTransaction($customer_id){
		$query = $this->db->query("SELECT DATEDIFF(NOW(),date_added) AS days FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '". (int) $customer_id . "' ORDER BY date_added DESC LIMIT 0,1");
		return $query->row['days'] ? $query->row['days'] :0;
	}
	
	public function getRewardTotal($customer_id) {
		$query = $this->db->query("SELECT SUM(points) AS total FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->row['total'];
	}		
	
	public function getTotalCustomerRewardsByOrderId($order_id, $customer_id=0) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_reward WHERE order_id = '" . (int)$order_id . "' AND customer_id = '" . (int) $customer_id . "'");

		return $query->row['total'];
	}
	
	public function calcTotal($customer_id){	
		$total =  $this->getRewardTotal($customer_id);
		// NK edit 29-3-2016   -- added return to prevent points expiration calculation. 
		return $total;
				
		$days =	  $this->getDaysSinceLastTransaction($customer_id);
		$exp_sett = $flexi_sett['flexi_exp_days'];
	
		$end_total = $total ;
		
		// SORT_DESC 
		function sortByOrder($a, $b) {
  			return $b['days'] - $a['days'];
		}
		usort($exp_sett, 'sortByOrder');
 
 		foreach($exp_sett as $exp) {
			//var_dump($exp);
			if($days > $exp['days'] ) {
				$end_total = round($exp['rate']*$total);
				break;
			}
		}
		
		return $end_total;
	}
	
    public function checkForAlreadyAddedRewards($order_id, $flexi_reward_order_status,$customer_id) {
        $query = $this->db->query("SELECT COUNT(1) AS total_count FROM " . DB_PREFIX . "customer_reward cr WHERE cr.order_id = '" . (int) $order_id . "' AND cr.customer_id = '". (int) $customer_id . "'");
        if ($query->row['total_count'] > 0) {
            return true;
        } else {
            return false;
        }
    }

	// Copy of admin / model_sale_order
	public function getOrder($order_id) { 
		$order_query = $this->db->query("SELECT *, (SELECT CONCAT(c.firstname, ' ', c.lastname) FROM " . DB_PREFIX . "customer c WHERE c.customer_id = o.customer_id) AS customer FROM `" . DB_PREFIX . "order` o WHERE o.order_id = '" . (int)$order_id . "'");

		if ($order_query->num_rows) {
			$reward = 0;

			$order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

			foreach ($order_product_query->rows as $product) {
				$reward += $product['reward'];
			}			

			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['payment_country_id'] . "'");

			if ($country_query->num_rows) {
				$payment_iso_code_2 = $country_query->row['iso_code_2'];
				$payment_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$payment_iso_code_2 = '';
				$payment_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['payment_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$payment_zone_code = $zone_query->row['code'];
			} else {
				$payment_zone_code = '';
			}

			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['shipping_country_id'] . "'");

			if ($country_query->num_rows) {
				$shipping_iso_code_2 = $country_query->row['iso_code_2'];
				$shipping_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$shipping_iso_code_2 = '';
				$shipping_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['shipping_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$shipping_zone_code = $zone_query->row['code'];
			} else {
				$shipping_zone_code = '';
			}

			if ($order_query->row['affiliate_id']) {
				$affiliate_id = $order_query->row['affiliate_id'];
			} else {
				$affiliate_id = 0;
			}				

	/*		$this->load->model('sale/affiliate');

			$affiliate_info = $this->model_sale_affiliate->getAffiliate($affiliate_id);

			if ($affiliate_info) {
				$affiliate_firstname = $affiliate_info['firstname'];
				$affiliate_lastname = $affiliate_info['lastname'];
			} else {
				$affiliate_firstname = '';
				$affiliate_lastname = '';				
			}
*/
			$this->load->model('localisation/language');

			$language_info = $this->model_localisation_language->getLanguage($order_query->row['language_id']);

			if ($language_info) {
				$language_code = $language_info['code'];
				$language_filename = $language_info['filename'];
				$language_directory = $language_info['directory'];
			} else {
				$language_code = '';
				$language_filename = '';
				$language_directory = '';
			}

			$amazonOrderId = '';

			if ($this->config->get('amazon_status') == 1) {
				$amazon_query = $this->db->query("
					SELECT `amazon_order_id`
					FROM `" . DB_PREFIX . "amazon_order`
					WHERE `order_id` = " . (int)$order_query->row['order_id'] . "
					LIMIT 1")->row;

				if (isset($amazon_query['amazon_order_id']) && !empty($amazon_query['amazon_order_id'])) {
					$amazonOrderId = $amazon_query['amazon_order_id'];
				}
			}

			if ($this->config->get('amazonus_status') == 1) {
				$amazon_query = $this->db->query("
						SELECT `amazonus_order_id`
						FROM `" . DB_PREFIX . "amazonus_order`
						WHERE `order_id` = " . (int)$order_query->row['order_id'] . "
						LIMIT 1")->row;

				if (isset($amazon_query['amazonus_order_id']) && !empty($amazon_query['amazonus_order_id'])) {
					$amazonOrderId = $amazon_query['amazonus_order_id'];
				}
			}

			return array(
				'amazon_order_id'         => '',/*$amazonOrderId,*/
				'order_id'                => $order_query->row['order_id'],
				'invoice_no'              => $order_query->row['invoice_no'],
				'invoice_prefix'          => $order_query->row['invoice_prefix'],
				'store_id'                => $order_query->row['store_id'],
				'store_name'              => $order_query->row['store_name'],
				'store_url'               => $order_query->row['store_url'],
				'customer_id'             => $order_query->row['customer_id'],
				'customer'                => $order_query->row['customer'],
				'customer_group_id'       => $order_query->row['customer_group_id'],
				'firstname'               => $order_query->row['firstname'],
				'lastname'                => $order_query->row['lastname'],
				'telephone'               => $order_query->row['telephone'],
				'fax'                     => $order_query->row['fax'],
				'email'                   => $order_query->row['email'],
				'payment_firstname'       => $order_query->row['payment_firstname'],
				'payment_lastname'        => $order_query->row['payment_lastname'],
				'payment_company'         => $order_query->row['payment_company'],
				'payment_company_id'      => $order_query->row['payment_company_id'],
				'payment_tax_id'          => $order_query->row['payment_tax_id'],
				'payment_address_1'       => $order_query->row['payment_address_1'],
				'payment_address_2'       => $order_query->row['payment_address_2'],
				'payment_postcode'        => $order_query->row['payment_postcode'],
				'payment_city'            => $order_query->row['payment_city'],
				'payment_zone_id'         => $order_query->row['payment_zone_id'],
				'payment_zone'            => $order_query->row['payment_zone'],
				'payment_zone_code'       => $payment_zone_code,
				'payment_country_id'      => $order_query->row['payment_country_id'],
				'payment_country'         => $order_query->row['payment_country'],
				'payment_iso_code_2'      => $payment_iso_code_2,
				'payment_iso_code_3'      => $payment_iso_code_3,
				'payment_address_format'  => $order_query->row['payment_address_format'],
				'payment_method'          => $order_query->row['payment_method'],
				'payment_code'            => $order_query->row['payment_code'],				
				'shipping_firstname'      => $order_query->row['shipping_firstname'],
				'shipping_lastname'       => $order_query->row['shipping_lastname'],
				'shipping_company'        => $order_query->row['shipping_company'],
				'shipping_address_1'      => $order_query->row['shipping_address_1'],
				'shipping_address_2'      => $order_query->row['shipping_address_2'],
				'shipping_postcode'       => $order_query->row['shipping_postcode'],
				'shipping_city'           => $order_query->row['shipping_city'],
				'shipping_zone_id'        => $order_query->row['shipping_zone_id'],
				'shipping_zone'           => $order_query->row['shipping_zone'],
				'shipping_zone_code'      => $shipping_zone_code,
				'shipping_country_id'     => $order_query->row['shipping_country_id'],
				'shipping_country'        => $order_query->row['shipping_country'],
				'shipping_iso_code_2'     => $shipping_iso_code_2,
				'shipping_iso_code_3'     => $shipping_iso_code_3,
				'shipping_address_format' => $order_query->row['shipping_address_format'],
				'shipping_method'         => $order_query->row['shipping_method'],
				'shipping_code'           => $order_query->row['shipping_code'],
				'comment'                 => $order_query->row['comment'],
				'total'                   => $order_query->row['total'],
				'reward'                  => $reward,
				'order_status_id'         => $order_query->row['order_status_id'],
			/*	'affiliate_id'            => $order_query->row['affiliate_id'],
				'affiliate_firstname'     => $affiliate_firstname,
				'affiliate_lastname'      => $affiliate_lastname,
			*/	'commission'              => $order_query->row['commission'],
				'language_id'             => $order_query->row['language_id'],
				'language_code'           => $language_code,
				'language_filename'       => $language_filename,
				'language_directory'      => $language_directory,				
				'currency_id'             => $order_query->row['currency_id'],
				'currency_code'           => $order_query->row['currency_code'],
				'currency_value'          => $order_query->row['currency_value'],
				'ip'                      => $order_query->row['ip'],
				'forwarded_ip'            => $order_query->row['forwarded_ip'], 
				'user_agent'              => $order_query->row['user_agent'],	
				'accept_language'         => $order_query->row['accept_language'],					
				'date_added'              => $order_query->row['date_added'],
				'date_modified'           => $order_query->row['date_modified']
			);
		} else {
			return false;
		}
	}

public function return_var_dump(){//works like var_dump, but returns a string instead of printing it.
		$args=func_get_args(); //for <5.3.0 support ...
		ob_start();
		call_user_func_array('var_dump',$args);
		
		$return = ob_get_clean();
		
			$fh = fopen(DIR_APPLICATION.'ggw_flexi_log.log','a');
				fwrite($fh,'---------'.$return ."--".date('H:i:s')."\r\n");
				fclose($fh);
}



}

?>