<?php
class ModelPaymentXpayment extends Model {
	function getMethod($address, $total) {
	
		$this->load->language('payment/xpayment');
		$this->load->model('catalog/product');

		$language_id=$this->config->get('config_language_id');
	
		$method_data = array();
	    $x_method = array();
		$sort_data = array();
	    
	    $xpayment_debug=$this->config->get('xpayment_debug');
	    
		$xpayment=$this->config->get('xpayment');
		if($xpayment)
		$xpayment=unserialize($xpayment);
		
		$currency_code=$this->config->get('config_currency');
        $order_info='';
        if(isset($this->session->data['order_id'])){
            $this->load->model('checkout/order');
            $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
        }
       if($order_info){
            $currency_code=$order_info['currency_code'];  
        }
		
		$cart_products=$this->cart->getProducts();
		$cart_weight=$this->cart->getWeight(); 
		
		$debugging=array();
		
		if(!isset($xpayment['name']))$xpayment['name']=array();
		if(!is_array($xpayment['name']))$xpayment['name']=array();

                
        foreach($xpayment['name'] as $no_of_tab=>$names){
		    
			$debugging_message=array();
	   	 
			if(!isset($xpayment['customer_group'][$no_of_tab]))$xpayment['customer_group'][$no_of_tab]=array();
		    if(!isset($xpayment['geo_zone_id'][$no_of_tab]))$xpayment['geo_zone_id'][$no_of_tab]=array();
		 	if(!isset($xpayment['product_category'][$no_of_tab]))$xpayment['product_category'][$no_of_tab]=array();
			if(!isset($xpayment['store'][$no_of_tab]))$xpayment['store'][$no_of_tab]=array();
		 	if(!isset($xpayment['manufacturer'][$no_of_tab]))$xpayment['manufacturer'][$no_of_tab]=array();
		 
		 	if(!is_array($xpayment['customer_group'][$no_of_tab]))$xpayment['customer_group'][$no_of_tab]=array();
		 	if(!is_array($xpayment['geo_zone_id'][$no_of_tab]))$xpayment['geo_zone_id'][$no_of_tab]=array();
		 	if(!is_array($xpayment['product_category'][$no_of_tab]))$xpayment['product_category'][$no_of_tab]=array();
		 	if(!is_array($xpayment['store'][$no_of_tab]))$xpayment['store'][$no_of_tab]=array();
		 	if(!is_array($xpayment['manufacturer'][$no_of_tab]))$xpayment['manufacturer'][$no_of_tab]=array();
		 	
		 	if(!is_array($names))$names=array();
		 
		 	if(!isset($xpayment['desc'][$no_of_tab]))$xpayment['desc'][$no_of_tab]=array();
		 	if(!is_array($xpayment['desc'][$no_of_tab]))$xpayment['desc'][$no_of_tab]=array();
		 	
		 	if(!isset($xpayment['instruction'][$no_of_tab]))$xpayment['instruction'][$no_of_tab]=array();
		 	if(!is_array($xpayment['instruction'][$no_of_tab]))$xpayment['instruction'][$no_of_tab]=array();
		 
		  	if(!isset($xpayment['customer_group_all'][$no_of_tab]))$xpayment['customer_group_all'][$no_of_tab]='';
		  	if(!isset($xpayment['geo_zone_all'][$no_of_tab]))$xpayment['geo_zone_all'][$no_of_tab]='';
		  	if(!isset($xpayment['store_all'][$no_of_tab]))$xpayment['store_all'][$no_of_tab]='';
		  	if(!isset($xpayment['manufacturer_all'][$no_of_tab]))$xpayment['manufacturer_all'][$no_of_tab]='';
		 	if(!isset($xpayment['postal_all'][$no_of_tab]))$xpayment['postal_all'][$no_of_tab]='';
		  	if(!isset($xpayment['postal'][$no_of_tab]))$xpayment['postal'][$no_of_tab]='';
		  	if(!isset($xpayment['postal_rule'][$no_of_tab]))$xpayment['postal_rule'][$no_of_tab]='inclusive';
			
				$status = true;
				
				if($xpayment['geo_zone_id'][$no_of_tab] && $xpayment['geo_zone_all'][$no_of_tab]!=1){
				   $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id in (" . implode(',',$xpayment['geo_zone_id'][$no_of_tab]) . ") AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')"); 
				}
                
				if($xpayment['geo_zone_all'][$no_of_tab]!=1){
				  if ($xpayment['geo_zone_id'][$no_of_tab] && $query->num_rows==0) {
					$status = false;
					$debugging_message[]='GEO Zone';
				  } 
				}
				
				if (!$xpayment['status'][$no_of_tab]) {
				  $status = false;
				  $debugging_message[]='Status';
				}
				
				/*store checking*/
				if($xpayment['store_all'][$no_of_tab]!=1){
				  if(!in_array((int)$this->config->get('config_store_id'),$xpayment['store'][$no_of_tab])){
				  $status = false;
				  $debugging_message[]='Store';
				  }
				}
				
				/*Manufacturer checking*/
				if($xpayment['manufacturer_all'][$no_of_tab]!=1){
				  
				  foreach($cart_products as $product){
					     $product_info=$this->model_catalog_product->getProduct($product['product_id']);
					    
						 if($product_info){
						   if(!in_array($product_info['manufacturer_id'],$xpayment['manufacturer'][$no_of_tab])) {
						     $status = false;
						     $debugging_message[]='Manufacturer';
						   }
						 }
				   }		  
				}
				
				/*Customer group checking*/
				if ($this->customer->isLogged()) {
					 $customer_group_id = $this->customer->getCustomerGroupId();
				 } else {
					 $customer_group_id = $this->config->get('config_customer_group_id');
				 }
				 
				if (!in_array($customer_group_id,$xpayment['customer_group'][$no_of_tab]) && $xpayment['customer_group_all'][$no_of_tab]!=1) {
				   $status = false; 
				   $debugging_message[]='Customer Group';
				}
				
				/*postal checking*/
				if($xpayment['postal_all'][$no_of_tab]!=1){
				  $postal=$xpayment['postal'][$no_of_tab]; 
				  $postal_rule=$xpayment['postal_rule'][$no_of_tab];
				  if($postal){
				    $postal=explode(',',trim($postal));
				    $postal_rule=($postal_rule=='inclusive')?false:true;
				   
			        if(in_array(trim($address['postcode']),$postal)===$postal_rule){
				        $status = false;
				        $debugging_message[]='Zip/Postal';
				    } 
				  }	  
				}
				
				
				$cart_categories=array();
				$cart_product_ids=array();
				$cart_volume=0;
				foreach($cart_products as $product){
				     $product_categories=$this->model_catalog_product->getCategories($product['product_id']);
				     $cart_product_ids[]=$product['product_id']; 
				    
					 if($product_categories){
						   foreach($product_categories as $category){
							  $cart_categories[]=$category['category_id'];  
							} 
					  }
					
					$cart_volume+=(($product['width']*$product['height']*$product['length'])*$product['quantity']);  
				} 
				
				$cart_categories=array_unique($cart_categories);
				$resultant_category=array_intersect($xpayment['product_category'][$no_of_tab],$cart_categories);
				
				/*category checking*/
				if ($xpayment['category'][$no_of_tab]==2){
				  if(count($resultant_category)!=count($xpayment['product_category'][$no_of_tab])){
				    $status = false; 
				    $debugging_message[]='Category';
				  }
				}
				if ($xpayment['category'][$no_of_tab]==3){
				   if(!$resultant_category){
				     $status = false; 
				     $debugging_message[]='Category';
				   }
				}
				
				if ($xpayment['category'][$no_of_tab]==4){
				  if(count($resultant_category)!=count($xpayment['product_category'][$no_of_tab]) || count($resultant_category)!=count($cart_categories)){
				    $status = false; 
				    $debugging_message[]='Category';
				  }
				}
				
				if ($xpayment['category'][$no_of_tab]==5){
				   if($resultant_category){
				     $status = false; 
				     $debugging_message[]='Category';
				   }
				}
				/* End of category */
				
				$order_total_start=$this->currency->format((float)$xpayment['order_total_start'][$no_of_tab], $currency_code, false, false);
                $order_total_end=$this->currency->format((float)$xpayment['order_total_end'][$no_of_tab], $currency_code, false, false);
				if($order_total_end>0){
				    
					 if ($total < $order_total_start || $total> $order_total_end) {
					   $status = false;
					   $debugging_message[]='Order Total';
					 }
					 
				}
				
				if($xpayment['weight_end'][$no_of_tab]>0){
				    
					 if ($this->cart->getWeight() < $xpayment['weight_start'][$no_of_tab] || $this->cart->getWeight() > $xpayment['weight_end'][$no_of_tab])           {
					   $status = false;
					   $debugging_message[]='Weight';
					 }
				}


				if(!isset($names[$language_id]) || !$names[$language_id]){
				  $status = false;
				  $debugging_message[]='Name';
				}
				
				
				if(!$status){
				 $debugging[]=array('name'=>$names[$language_id],'filter'=>$debugging_message,'index'=>$no_of_tab);
				}
				
				
				$method_desc=($xpayment['desc'][$no_of_tab][$language_id])?'<br /><span style="color: #999999;font-size: 11px;" class="x-payment-desc">'.$xpayment['desc'][$no_of_tab][$language_id].'</span>':'';
				
				if ($status) {
					$x_method['xpayment'.$no_of_tab] = array(
						'code'         => 'xpayment'.'.xpayment'.$no_of_tab,
						'title'        => html_entity_decode($names[$language_id].$method_desc),
						'sort_order'         => intval($xpayment['sort_order'][$no_of_tab])
					);
				}
			}
			
		   
		    /*Sorting final method*/
		    $sort_order = array();
			foreach ($x_method as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $x_method);
			
			
			
			$method_data = array(
				'code'       => 'xpayment',
				'title'      => $this->language->get('text_title'),
				'methods'      => $x_method,
				'sort_order' => $this->config->get('xpayment_sort_order')
			);	
	   
	    if($xpayment_debug && $debugging){ 
	      echo '<div style="border: 1px solid #FF0000; margin: 20px 5px;padding: 10px;">';
	      foreach($debugging as $debug){
	        echo '<b>'.$debug['name'].'-'.$debug['index'].'</b> : ('.implode(',',$debug['filter']).') <br />';
	      }
	      echo '</div>';
	    }		
		
        if(!$x_method) return array();
		return $method_data;
 }
}
?>