<?php 
class ModelPaymentOfflinepay extends Model {
  	public function getMethod($address, $total) {
		$this->language->load('payment/offlinepay');
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('offlinepay_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");
	
		if ($this->config->get('offlinepay_total') > 0 && $this->config->get('offlinepay_total') > $total) {
			$status = false;
		} elseif (!$this->config->get('offlinepay_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}
		
		$method_data = array();
	
		if ($status) {  
      		$method_data = array( 
        		'code'       => 'offlinepay',
        		'title'      => $this->language->get('text_title'),
				'sort_order' => $this->config->get('offlinepay_sort_order')
      		);
    	}
   
    	return $method_data;
  	}

  	public function pay($transactionId, $orderId, $payDate, $paidAmount) {
  		$this->db->query("INSERT INTO " . DB_PREFIX . "offlinepay SET order_id = '" . (int)$orderId . "', payment_date = '" . $this->db->escape($payDate) . "', transaction_id = '" . $this->db->escape($transactionId) . "', total = '" . (float)$paidAmount . "'");

  		return (bool)$this->db->countAffected();
  	}

  	public function isExists($transactionId) {
  		$order_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "offlinepay` op WHERE op.transaction_id = '" . $this->db->escape($transactionId) . "'");

  		return (bool)$order_query->num_rows;
  	}
}
?>