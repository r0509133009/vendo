<?php
class ModelAccountPopuplogginRegister extends Model {
	public function addCustomer($data) {

		$country_id = !empty($data['country_id']) ? $data['country_id'] : $this->config->get('config_country_id');
		$zone_id =  !empty($data['zone_id']) ? $data['zone_id'] : $this->config->get('config_zone_id');

		if (isset($data['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($data['customer_group_id'], $this->config->get('config_customer_group_display'))) {
			$customer_group_id = $data['customer_group_id'];
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}

		$this->load->model('account/customer_group');

		$customer_group_info = $this->model_account_customer_group->getCustomerGroup($customer_group_id);

		$this->db->query("INSERT INTO " . DB_PREFIX . "customer SET store_id = '" . (int)$this->config->get('config_store_id') . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '', email = '" . $this->db->escape($data['email']) . "', telephone = '', fax = '', salt = '" . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', newsletter = '" . (isset($data['newsletter']) ? (int)$data['newsletter'] : 0) . "', customer_group_id = '" . (int)$customer_group_id . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', status = '1', approved = '" . (int)!$customer_group_info['approval'] . "', date_added = NOW()");

		$customer_id = $this->db->getLastId();

		$this->db->query("INSERT INTO " . DB_PREFIX . "address SET customer_id = '" . (int)$customer_id . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '', company = '', company_id = '', tax_id = '', address_1 = '', address_2 = '', city = '', postcode = '', country_id = '{$country_id}', zone_id = '{$zone_id}'");

		$address_id = $this->db->getLastId();

		$this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");

		$this->language->load('mail/customer');


		$subject = sprintf($this->language->get('text_subject'), $this->config->get('config_name'));

		$message = sprintf($this->language->get('text_welcome'), $this->config->get('config_name')) . "\n\n";

		if (!$customer_group_info['approval']) {
			$message .= $this->language->get('text_login') . "\n";
		} else {
			$message .= $this->language->get('text_approval') . "\n";
		}

		$message .= $this->url->link('account/login', '', 'SSL') . "\n\n";
		$message .= $this->language->get('text_services') . "\n\n";
		$message .= $this->language->get('text_thanks') . "\n";
		$message .= $this->config->get('config_name');

		$mail = new Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->hostname = $this->config->get('config_smtp_host'); 
		$mail->username = $this->config->get('config_smtp_username');
		$mail->password = $this->config->get('config_smtp_password');
		$mail->port = $this->config->get('config_smtp_port');
		$mail->timeout = $this->config->get('config_smtp_timeout');				
		$mail->setTo($data['email']);
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender($this->config->get('config_name'));
		$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
		$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));

		$this->load->model('module/phe');
		$phe = $this->model_module_phe->getTemplate('register');
		if ($phe && !empty($phe['message'])) { 			
			$template = new Template();
			$template->data['store_name'] = $this->config->get('config_name');
			$template->data['url'] = $this->config->get('config_ssl');
			$template->data['logo'] = $this->config->get('config_ssl') . 'image/' . $this->config->get('config_logo');
			
			if (!empty($phe['image'])) {
				$template->data['logo'] = $this->config->get('config_ssl') . 'image/' . $phe['image'];
			}
			
			$template->data['body'] = $phe['body'];
			$template->data['heading'] = $phe['heading'];
			$template->data['background'] = $phe['background'];
			
			$message = $phe['message'];
			
			$search = array(
				'{firstname}',
				'{lastname}',
				'{email}',
				'{telephone}',
				'{password}'
			);
			
			$replace = array(
				$data['firstname'],
				 '',
				$data['email'],
				'',
				$data['password']
			);
			
			$template->data['subject'] = str_replace($search, $replace, html_entity_decode($phe['subject']));
			$template->data['message'] = str_replace($search, $replace, html_entity_decode($phe['message']));
			
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/phe.tpl')) {
				$html = $template->fetch($this->config->get('config_template') . '/template/mail/phe.tpl');
			} else {
				$html = $template->fetch('default/template/mail/phe.tpl');
			}
			
			$mail->setSubject($template->data['subject']);
			$mail->setHtml($html);
		}
		$mail->send();

		// Send to main admin email if new account email is enabled
		if ($this->config->get('config_account_mail')) {
			$message  = $this->language->get('text_signup') . "\n\n";
			$message .= $this->language->get('text_website') . ' ' . $this->config->get('config_name') . "\n";
			$message .= $this->language->get('text_firstname') . ' ' . $data['firstname'] . "\n";
			$message .= $this->language->get('text_lastname') . "\n";
			$message .= $this->language->get('text_customer_group') . ' ' . $customer_group_info['name'] . "\n";

			if ($data['company']) {
				$message .= $this->language->get('text_company') ."\n";
			}

			$message .= $this->language->get('text_email') . ' '  .  $data['email'] . "\n";
			$message .= $this->language->get('text_telephone') . "\n";;

			$mail->setTo($this->config->get('config_email'));
			$mail->setSubject(html_entity_decode($this->language->get('text_new_customer'), ENT_QUOTES, 'UTF-8'));
			$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
			$mail->send();

			// Send to additional alert emails if new account email is enabled
			$emails = explode(',', $this->config->get('config_alert_emails'));

			foreach ($emails as $email) {
				if (strlen($email) > 0 && preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $email)) {
					$mail->setTo($email);
					$mail->send();
				}
			}
		}
	}
}
?>
