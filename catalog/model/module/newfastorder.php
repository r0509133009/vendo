<?php
class ModelModuleNewfastorder extends Model {	
	public function addOrder($data, $product_options) {

		$tax_rates_f_p = $this->tax->getRates($data['price_no_tax'], $data['tax_class_id_total']);			
		$this_product = $this->db->query("SELECT * FROM ". DB_PREFIX ."product WHERE product_id = '". $data['prod_id'] ."'");
		
		$this_product_info = $this_product->row;
		if($this_product_info['subtract'] =='1'){
			$quantity_new = $this_product_info['quantity'] - $data['quantity'];
			$this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = '". $quantity_new ."' WHERE product_id = '". $data['prod_id'] ."'");
		}	
					
		$this->db->query("INSERT INTO `" . DB_PREFIX . "order` SET
			invoice_prefix = 'fastorder-',
			store_id = '" . (int)$data['store_id'] . "',
			store_name = '" . $this->db->escape($data['store_name']) . "',
			store_url = '" . $this->db->escape($data['store_url']) . "',
			customer_id = '" . (int)$data['customer_id'] . "',
			customer_group_id = '" . (int)$data['customer_group_id'] . "',
			firstname = '" . $this->db->escape($data['name_fastorder']) . "',
			email = '" . $this->db->escape($data['email_buyer']) . "',
			telephone = '" . $this->db->escape($data['phone']) . "',									
			payment_method = '" . $this->db->escape($data['payment_title']) . "',
			payment_code = '" . $this->db->escape($data['payment_code_quickorder']) . "',				
			shipping_method = '" . $this->db->escape($data['shipping_title']) . "',
			shipping_code = '" . $this->db->escape($data['shipping_code_quickorder']) . "',	
			comment = '" . $this->db->escape($data['comment_buyer']) . "',
			total = '" . (float)$data['total_fast'] . "',				
			language_id = '" . (int)$data['language_id'] . "',
			currency_id = '" . (int)$data['currency_id'] . "',
			currency_code = '" . $this->db->escape($data['currency_code']) . "',
			currency_value = '" . (float)$data['currency_value'] . "',
			ip = '" . $this->db->escape($data['ip']) . "',
			forwarded_ip = '" .  $this->db->escape($data['forwarded_ip']) . "',
			user_agent = '" . $this->db->escape($data['user_agent']) . "',
			accept_language = '" . $this->db->escape($data['accept_language']) . "',
			order_status_id = '". $this->config->get('config_order_status_id')."',
			date_added = NOW(),
			date_modified = NOW()");	
		
		$order_id = $this->db->getLastId();
		
			$this->db->query("INSERT INTO " . DB_PREFIX . "order_product SET 
				order_id = '" . (int)$order_id . "',
				product_id = '" . (int)$data['prod_id'] . "',
				name = '" . $this->db->escape($data['prod_name']) . "',
				model = '" . $this->db->escape($data['model']) . "',
				quantity = '". $data['quantity'] . "',
				total = '" . (float)$data['price_no_tax'] * $data['quantity'] . "',
				tax = '" . (float)$this->tax->getTax($data['price_no_tax'], $data['tax_class_id_total']) . "',
				price = '" . $this->db->escape($data['price_no_tax']) . "'");
	 
			$order_product_id = $this->db->getLastId();

			
			$this->db->query("INSERT INTO " . DB_PREFIX . "order_total SET 
				order_id = '" . (int)$order_id . "',
				code = 'sub_total',
				title = 'Сумма',
				text = '" . $this->currency->format($data['price_no_tax'] * $data['quantity']) . "',
				`value` = '" . (float)$data['price_no_tax']*$data['quantity'] . "',
				sort_order = '1'");
				
			if($data['shipping_title'] !='') {
			$this->db->query("INSERT INTO " . DB_PREFIX . "order_total SET 
				order_id = '" . (int)$order_id . "',
				code = 'shipping',
				title = '" . $data['shipping_title'] . "',
				text = '" . $data['price_shipping_text'] . "',
				`value` = '" . $data['price_shipping_value'] . "',
				sort_order = '3'");
			}
			
				
			foreach($tax_rates_f_p as $tax_rate){
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_total SET 
				order_id = '" . (int)$order_id . "',
				code = 'tax',
				title = '". $tax_rate['name'] ."',
				text = '". $this->currency->format($tax_rate['amount']*$data['quantity']) ."',
				`value` = '". (float)$tax_rate['amount']*$data['quantity'] ."',
				sort_order = '5'");
			}		
			
			
			$this->db->query("INSERT INTO " . DB_PREFIX . "order_total SET 
				order_id = '" . (int)$order_id . "',
				code = 'total',
				title = 'Итого',
				text = '" . $this->currency->format($data['total_fast']) . "',
				`value` = '" . $data['total_fast'] . "',
				sort_order = '9'");						
		
			/*FAST ADD*/
			if ($this->config->get('config_tax')) {
				$data['price_no_tax_qucik_list'] = $data['price_tax'];
			} else {
				$data['price_no_tax_qucik_list'] = $data['price_no_tax'];
			}
			$query = $this->db->query("INSERT INTO " . DB_PREFIX . "newfastorder_product SET product_name = '" . $data['prod_name']  . "',
				order_id 		= '" . (int)$order_id . "',
				product_id 		= '". $data['prod_id'] ."',
				price 			= '". $data['price_no_tax_qucik_list'] ."',
				total 			= '" . $data['total_fast'] . "',
				product_image 	= '" . $data['prod_image']  . "',
				currency_code 	= '" . $data['currency_code'] . "',
				currency_value 	= '" . $data['currency_value'] . "',
				model 			= '" . $this->db->escape($data['model']) . "',
				quantity 		= '". $data['quantity'] ."'");
				
			$newfastorder_product_id = $this->db->getLastId();
			
			foreach($product_options as $product_option){
				$this->db->query("INSERT INTO " . DB_PREFIX . "newfastorder_product_option SET 
					order_id 				= '". $order_id."',
					order_product_id 		= '". $data['prod_id']."',
					product_option_id 		= '". $product_option['product_option_id'] . "',
					product_option_value_id = '". (int)$product_option['product_option_value_id'] . "',
					type 					= '". $product_option['type'] ."',
					name 					= '". $product_option['name'] ."',
					`value` 				= '". $product_option['value'] . "'");
			}
			
			
			$query = $this->db->query("INSERT INTO " . DB_PREFIX . "newfastorder SET name = '" . $data['name_fastorder']  . "',
				newfastorder_url = '" . $data['url_site']  . "',
				comment_buyer = '" . $data['comment_buyer']  . "',
				email_buyer = '" . $data['email_buyer']  . "',
				telephone = '" . $data['phone'] . "',
				total = '" . $this->currency->format($data['total_fast']) . "',
				order_id = '" . (int)$order_id . "',
				date_added = NOW(),
				date_modified = NOW(),
				status_id = '0',
				comment = ''");
			$newfastorder_id = $this->db->getLastId();	
			/*-------------------------------------------*/
			
			foreach($product_options as $product_option){
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_option SET 
					order_id = '". $order_id ."',
					order_product_id = '".$order_product_id."',
					product_option_id = '" . $product_option['product_option_id'] . "',
					product_option_value_id = '" . (int)$product_option['product_option_value_id'] . "',
					type = '". $product_option['type'] ."',
					name = '". $product_option['name'] ."',
					`value` = '" . $product_option['value'] . "'");
				}	
			
		return $order_id;
	}
}

?>
