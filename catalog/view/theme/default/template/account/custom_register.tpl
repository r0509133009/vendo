<?php
// ***************************************************
//             Custom Registration Fields   
//       
// Author : Francesco Pisanò - francesco1279@gmail.com
//              
//                   www.leverod.com		
//               © All rights reserved	  
// ***************************************************



// Registration page template (..?route=account/custom_register)
?>


<?php
// this javascript removes the old form from the page. It has already been hidden by 
// vQmod with a <form style="display:none"... to prevent displaying it while the page 
// is loading. ?>
<script type="text/javascript"><!--
$(document).ready(function() {

	old_form = $( 'form[action="<?php echo $action; ?>"]' );
	old_form.not('#registration').remove();

});
//--></script> 



<form id="registration" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">


	<?php // Set disabled fields to empty to avoid compatibility problems with other extensions ?>
	
	<input type="hidden" name="firstname"	value="" />
	<input type="hidden" name="lastname"	value="" />
	<input type="hidden" name="telephone"	value="" />
	<input type="hidden" name="fax"			value="" />
	<input type="hidden" name="company"		value="" />
	<input type="hidden" name="address_1"	value="" />
	<input type="hidden" name="address_2"	value="" />
	<input type="hidden" name="city"		value="" />
	<input type="hidden" name="postcode"	value="" />
	<input type="hidden" name="country_id"	value="<?php echo $country_id; ?>" />
	<input type="hidden" name="zone_id"		value="" />
	
	<input type="hidden" name="newsletter"	value="0" />
	
	<input type="hidden" name="company_id" value=""  />
	<input type="hidden" name="tax_id" value=""  />


	<?php if ($custom_register_account_member_enable_firstname || $custom_register_account_member_enable_lastname || $custom_register_account_member_enable_telephone || $custom_register_account_member_enable_fax ) { ?>
	  
		<h2><?php echo $text_your_details; ?></h2>
		
		<div class="content">
			<table class="form">

				<?php if ($custom_register_account_member_enable_firstname) { ?>
					<tr>
						<td class="label">
							<?php if ($custom_register_account_member_firstname_required) { ?> <span class="required">*</span> <?php } ?> 
							<?php echo $entry_firstname; ?>
						</td>
						<td><input type="text" name="firstname" value="<?php echo $firstname; ?>" />
						<?php if ($error_firstname) { ?>
						<span class="error"><?php echo $error_firstname; ?></span>
						<?php } ?></td>
					</tr>
				<?php } ?>

				
				<?php if ($custom_register_account_member_enable_lastname) { ?>  
					<tr>
						<td class="label">
							<?php if ($custom_register_account_member_lastname_required) { ?> <span class="required">*</span> <?php } ?> 
							<?php echo $entry_lastname; ?>
						</td>
						<td><input type="text" name="lastname" value="<?php echo $lastname; ?>" />
						<?php if ($error_lastname) { ?>
						<span class="error"><?php echo $error_lastname; ?></span>
						<?php } ?></td>
					</tr>
				<?php } ?>

				<tr>
					<td class="label"><span class="required">*</span> <?php echo $entry_email; ?></td>
					<td><input type="text" name="email" value="<?php echo $email; ?>" />
					<?php if ($error_email) { ?>
					<span class="error"><?php echo $error_email; ?></span>
					<?php } ?></td>
				</tr>
					
				<?php if ($custom_register_account_member_enable_telephone) { ?>	
					<tr>
						<td class="label">
							<?php if ($custom_register_account_member_telephone_required) { ?> <span class="required">*</span> <?php } ?> 
							<?php echo $entry_telephone; ?>
						</td>
						<td><input type="text" name="telephone" value="<?php echo $telephone; ?>" />
						<?php if ($error_telephone) { ?>
						<span class="error"><?php echo $error_telephone; ?></span>
						<?php } ?></td>
					</tr>
				<?php } ?>

				<?php if ($custom_register_account_member_enable_fax) { ?>
					<tr>
						<td class="label">
							<?php if ($custom_register_account_member_fax_required) { ?> <span class="required">*</span> <?php } ?>
							<?php echo $entry_fax; ?>
						</td>
						<td><input type="text" name="fax" value="<?php echo $fax; ?>" />
						<?php if ($error_fax) { ?>
						<span class="error"><?php echo $error_fax; ?></span>
						<?php } ?></td>
					</tr>
				<?php } ?>
						
			</table>
		</div>
		<?php } 
	 else { ?>	
		<div class="content">
			<table class="form">
				<tr>
					<td class="label"><span class="required">*</span> <?php echo $entry_email; ?></td>
					<td><input type="text" name="email" value="<?php echo $email; ?>" />
					<?php if ($error_email) { ?>
					<span class="error"><?php echo $error_email; ?></span>
					<?php } ?></td>
				</tr>
			</table>
		</div>
	<?php } ?>
	
	
	<?php if ($custom_register_account_member_enable_company || $custom_register_account_member_enable_address_1 || $custom_register_account_member_enable_address_2 || $custom_register_account_member_enable_city || $custom_register_account_member_enable_country || $custom_register_account_member_enable_postcode || $custom_register_account_member_enable_zone ) { ?>
		<h2><?php echo $text_your_address; ?></h2>
		
		<div class="content">
	<?php } ?>
	
		<table class="form">
	  
	  
			<?php if ($custom_register_account_member_enable_company) { ?>
				<tr>
					<td class="label">
						<?php if ($custom_register_account_member_company_required) { ?> <span class="required">*</span> <?php } ?>
						<?php echo $entry_company; ?>
					</td>
					<td><input type="text" name="company" value="<?php echo $company; ?>" />
					<?php if ($error_company) { ?>
					<span class="error"><?php echo $error_company; ?></span>
					<?php } ?></td>
				</tr>
			<?php } ?>
		
		
<?php	// CUSTOMER GROUPS, TAX ID AND COMPANY ID HAVE BEEN ADDED WITH Oc 1.5.3
		if ( version_compare(VERSION, '1.5.3', '>=') ) { ?>
		
			<tr style="display: <?php echo (count($customer_groups) > 1 ? 'table-row' : 'none'); ?>;">
				<td><?php echo $entry_customer_group; ?></td>
				<td><?php foreach ($customer_groups as $customer_group) { ?>
				<?php if ($customer_group['customer_group_id'] == $customer_group_id) { ?>
				<input type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" id="customer_group_id<?php echo $customer_group['customer_group_id']; ?>" checked="checked" />
				<label for="customer_group_id<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></label>
				<br />
				<?php } else { ?>
				<input type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" id="customer_group_id<?php echo $customer_group['customer_group_id']; ?>" />
				<label for="customer_group_id<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></label>
				<br />
				<?php } ?>
				<?php } ?></td>
			</tr>   
		
			<?php if( count($customer_groups) > 0) { ?>
			
				<tr id="company-id-display">
					<td class="label"><span id="company-id-required" class="required">*</span> <?php echo $entry_company_id; ?></td>
					<td><input type="text" name="company_id" value="<?php echo $company_id; ?>" />
					<?php if ($error_company_id) { ?>
					<span class="error"><?php echo $error_company_id; ?></span>
					<?php } ?></td>
				</tr>

				<tr id="tax-id-display">
					<td class="label"><span id="tax-id-required" class="required">*</span> <?php echo $entry_tax_id; ?></td>
					<td><input type="text" name="tax_id" value="<?php echo $tax_id; ?>" />
					<?php if ($error_tax_id) { ?>
					<span class="error"><?php echo $error_tax_id; ?></span>
					<?php } ?></td>
				</tr>
			<?php } ?>
			
		<?php } ?>
		
			<?php if ($custom_register_account_member_enable_address_1) { ?>
				<tr>
					<td class="label">
						<?php if ($custom_register_account_member_address_1_required) { ?> <span class="required">*</span> <?php } ?>
						<?php echo $entry_address_1; ?>
					</td>
					<td><input type="text" name="address_1" value="<?php echo $address_1; ?>" />
					<?php if ($error_address_1) { ?>
					<span class="error"><?php echo $error_address_1; ?></span>
					<?php } ?></td>
				</tr>
			<?php } ?>
	
		
			<?php if ($custom_register_account_member_enable_address_2) { ?>
				<tr>	
					<td class="label">
						<?php if ($custom_register_account_member_address_2_required) { ?> <span class="required">*</span> <?php } ?>
						<?php echo $entry_address_2; ?>
					</td>
					<td><input type="text" name="address_2" value="<?php echo $address_2; ?>" />
					<?php if ($error_address_2) { ?>
					<span class="error"><?php echo $error_address_2; ?></span>
					<?php } ?></td>
				</tr>
			<?php } ?>
			
			
			<?php if ($custom_register_account_member_enable_city) { ?>
				<tr>
					<td class="label">
						<?php if ($custom_register_account_member_city_required) { ?> <span class="required">*</span> <?php } ?>  
						<?php echo $entry_city; ?>
					</td>
					<td><input type="text" name="city" value="<?php echo $city; ?>" />
					<?php if ($error_city) { ?>
					<span class="error"><?php echo $error_city; ?></span>
					<?php } ?></td>
				</tr>
			<?php } ?>
		
		
		
			<?php if ($custom_register_account_member_enable_postcode) { ?>
				<tr>
					<td class="label">
					
						<?php if ($custom_register_account_member_postcode_required) { ?> 
							<span class="required">*</span> 
						<?php } else {?>
							<span class="postcode-required required">*</span>
						<?php } ?>
					
						<?php echo $entry_postcode; ?>
					</td>
					<td><input type="text" name="postcode" value="<?php echo $postcode; ?>" />
					<?php if ($error_postcode) { ?>
					<span class="error"><?php echo $error_postcode; ?></span>
					<?php } ?></td>
				</tr>

			<?php } elseif ($custom_register_account_member_enable_country || $country_disabled_zone_enabled) { ?>
				<tr class="postcode-required">
					<td class="label"><span class="required">*</span> <?php echo $entry_postcode; ?></td>
					<td><input id="empty_if_hidden" type="text" name="postcode" value="<?php echo $postcode; ?>" />
					<?php if ($error_postcode) { ?>
					<span class="error"><?php echo $error_postcode; ?></span>
					<?php } ?></td>
				</tr>
			<?php } ?>
		
		
		
			<?php if ($custom_register_account_member_enable_country) { ?>
				<tr>
					<td class="label">
						<?php if ($custom_register_account_member_country_required) { ?> <span class="required">*</span> <?php } ?>  
						<?php echo $entry_country; ?>
					</td>
					<td>				
					<select name="country_id">
					<option value=""><?php echo $text_select; ?></option>
					<?php foreach ($countries as $country) { ?>
					<?php if ($country['country_id'] == $country_id) { ?>
					<option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
					<?php } else { ?>
					<option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
					<?php } ?>
					<?php } ?>
					</select>			
					<?php if ($error_country) { ?>
					<span class="error"><?php echo $error_country; ?></span>
					<?php } ?></td>
				</tr>
			<?php } ?>
			


			<?php if ($custom_register_account_member_enable_zone) { ?>	
				<tr>
					<td class="label">
						<?php if ($custom_register_account_member_zone_required) { ?> <span class="required">*</span> <?php } ?>
						<?php echo $entry_zone; ?>
					</td>
					<td><select name="zone_id">
					</select>
					<?php if ($error_zone) { ?>
					<span class="error"><?php echo $error_zone; ?></span>
					<?php } ?></td>
				</tr>
			<?php } ?>
		
		</table>
	  
	<?php if ($custom_register_account_member_enable_company || $custom_register_account_member_enable_address_1 || $custom_register_account_member_enable_address_2 || $custom_register_account_member_enable_city || $custom_register_account_member_enable_country || $custom_register_account_member_enable_postcode || $custom_register_account_member_enable_zone ) { ?>
	</div>
	<?php } ?>
	  

	
	
    <h2><?php echo $text_your_password; ?></h2>
	<div class="content">
	
		<table class="form">
			
			<tr>
			  <td class="label"><span class="required">*</span> <?php echo $entry_password; ?></td>
			  <td><input type="password" name="password" value="<?php echo $password; ?>" />
				<?php if ($error_password) { ?>
				<span class="error"><?php echo $error_password; ?></span>
				<?php } ?></td>
			</tr>
			
			
			<?php if ($custom_register_account_member_enable_confirm) { ?>
				<tr>
					<td class="label">
						<span class="required">*</span> 
						<?php echo $entry_confirm; ?>
					</td>
					<td><input type="password" name="confirm" value="<?php echo $confirm; ?>" />
					<?php if ($error_confirm) { ?>
					<span class="error"><?php echo $error_confirm; ?></span>
					<?php } ?></td>
				</tr>
			<?php } ?>
				
		</table>
    </div>

	<?php if ($custom_register_account_member_enable_newsletter) { ?>
		<h2><?php echo $text_newsletter; ?></h2>
		<div class="content">
			<table class="form">
				<tr>
					<td class="label"><?php echo $entry_newsletter; ?></td>
					
					<td><?php if ($newsletter) { ?>
					<input type="radio" name="newsletter" value="1" checked="checked" />
					<?php echo $text_yes; ?>
					<input type="radio" name="newsletter" value="0" />
					<?php echo $text_no; ?>
					<?php } else { ?>
					<input type="radio" name="newsletter" value="1" />
					<?php echo $text_yes; ?>
					<input type="radio" name="newsletter" value="0" checked="checked" />
					<?php echo $text_no; ?>
					<?php } ?></td>
				</tr>
			</table>	
		</div>
	<?php } ?>
	

		
    <?php if ($text_agree) { ?>
    <div class="buttons clearfix">
		<div class="right">
	  
			<?php echo $text_agree; ?> 
			<?php if ($agree) { ?>
			<input type="checkbox" name="agree" value="1" checked="checked" />
			<?php } else { ?>
			<input type="checkbox" name="agree" value="1" />
			<?php } ?>			
			
			<?php 	// On Oc <= 1.5.1.3.1 <input> buttons were <a> links
			if ( version_compare(VERSION, '1.5.1.3.1', '<=') ) { ?>		
				<a onclick="$('#registration').submit();" class="button"><span><?php echo $button_continue; ?></span></a>
			<?php } else { ?>
				<input type="submit" value="<?php echo $button_continue; ?>" class="button" />
			<?php } ?>
		
		</div>
    </div>
    <?php } else { ?>
    <div class="buttons clearfix">
		<div class="right">
		
			<?php 	// On Oc <= 1.5.1.3.1 <input> buttons were <a> links
			if ( version_compare(VERSION, '1.5.1.3.1', '<=') ) { ?>		
				<a onclick="$('#registration').submit();" class="button"><span><?php echo $button_continue; ?></span></a>
			<?php } else { ?>
				<input type="submit" value="<?php echo $button_continue; ?>" class="button" />
			<?php } ?>
			
		</div>
    </div>
    <?php } ?>
	

	<?php if ($custom_register_enable_antispam) { ?>
		<div class="kill">
			<label for="toomanysecrets"></label>
			<input type="text" size="1" id="toomanysecrets" name="phone800" value="" />
		</div>
		<style>div.kill, div.kill label, div.kill input {visibility:hidden;display:none;}</style> 
		<script type="text/javascript"><!-- 
			$(document).ready(function() { $('.kill label').append('<?php echo $entry_anti_spam_label ?>');}); 
		//--></script> 
	<?php } ?>	
	
  </form>


	<?php	
	// CUSTOMER GROUPS, TAX ID AND COMPANY ID HAVE BEEN ADDED WITH Oc 1.5.3
	if ( version_compare(VERSION, '1.5.3', '>=') ) { ?>
 
		<?php
		// ADDED - the following jQuery script prevents the browser to fill the fields tax id and company id 
		// also when they are hidden. It would causes problems when the controller checks if an address is 
		// empty. In this case it is deleted after the registration, but if the browser fills some hidden field, 
		// the address will not be deleted and a customer will see a line of dots .... instead of the payment
		// address into the checkout page.
		?>
		<script type="text/javascript"><!--
			$(document).ready(function() {
			
				if ( ! $('#company-id-display').is(':visible')) {
					$('input[name="company_id"]').val('');
				}
				if ( ! $('#tax-id-display').is(':visible')) {
					$('input[name="tax_id"]').val('');
				}
			});
		//--></script> 
		
		<?php // end ?>
 

		<script type="text/javascript"><!--
		$('input[name=\'customer_group_id\']:checked').live('change', function() {
			var customer_group = [];
			
		<?php foreach ($customer_groups as $customer_group) { ?>
			customer_group[<?php echo $customer_group['customer_group_id']; ?>] = [];
			customer_group[<?php echo $customer_group['customer_group_id']; ?>]['company_id_display'] = '<?php echo $customer_group['company_id_display']; ?>';
			customer_group[<?php echo $customer_group['customer_group_id']; ?>]['company_id_required'] = '<?php echo $customer_group['company_id_required']; ?>';
			customer_group[<?php echo $customer_group['customer_group_id']; ?>]['tax_id_display'] = '<?php echo $customer_group['tax_id_display']; ?>';
			customer_group[<?php echo $customer_group['customer_group_id']; ?>]['tax_id_required'] = '<?php echo $customer_group['tax_id_required']; ?>';
		<?php } ?>	

			if (customer_group[this.value]) {
				if (customer_group[this.value]['company_id_display'] == '1') {
					$('#company-id-display').show();
				} else {
					$('#company-id-display').hide();
					$('input[name="company_id"]').val(''); // added to prevent posting unwanted info
				}
				
				if (customer_group[this.value]['company_id_required'] == '1') {
					$('#company-id-required').show();
				} else {
					$('#company-id-required').hide();
				}
				
				if (customer_group[this.value]['tax_id_display'] == '1') {
					$('#tax-id-display').show();
				} else {
					$('#tax-id-display').hide();
					$('input[name="tax_id"]').val(''); // added to prevent posting unwanted info
				}
				
				if (customer_group[this.value]['tax_id_required'] == '1') {
					$('#tax-id-required').show();
				} else {
					$('#tax-id-required').hide();
				}	
			}
		});

		$('input[name=\'customer_group_id\']:checked').trigger('change');
		//--></script> 
		
	<?php } ?>

	
	
<?php require_once(DIR_TEMPLATE.'default/template/crf_include/crf_load_zones.tpl'); ?>


<?php 
// fancybox script hasn't added in the header on Oc <= 1.5.0.1  :(
if ( version_compare(VERSION, '1.5.0.1', '<=') ) { ?>
<script type="text/javascript" src="catalog/view/javascript/jquery/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/fancybox/jquery.fancybox-1.3.4.css" media="screen" />  
<?php } ?>

<?php 
// colorbox replaced fancybox from Oc 1.5.2
if ( version_compare(VERSION, '1.5.2', '>=') ) { ?>

	<script type="text/javascript"><!--
	$(document).ready(function() {
		if ($.isFunction($.colorbox)) { // check if the function colorbox exists
			$('.colorbox').colorbox({
				width: 640,
				height: 480
			});
		}
	});
	//--></script> 
<?php } else { // for Oc <= 1.5.1.3.1 ?>

	<script type="text/javascript"><!--
	$(document).ready(function() {
		if ($.isFunction($.fancybox)) { // check if the function fancybox exists
			$('.fancybox').fancybox({
				width: 560,
				height: 560,
				autoDimensions: false
			});
		}
	});
	//--></script>  
<?php } ?>


<?php if (!empty($custom_register_link)){ ?> <div><?php echo $custom_register_link; ?></div> <?php } ?>