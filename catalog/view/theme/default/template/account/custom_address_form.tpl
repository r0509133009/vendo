<?php
// ***************************************************
//             Custom Registration Fields   
//       
// Author : Francesco Pisanò - francesco1279@gmail.com
//              
//                   www.leverod.com		
//               © All rights reserved	  
// ***************************************************
?>



<?php $action = str_replace('&amp;', '&', $action); ?> 

<?php
// this javascript removes the old form from the page. It has already been hidden by 
// vQmod with a <form style="display:none"... to prevent displaying it while the page 
// is loading. ?>
<script type="text/javascript"><!--
$(document).ready(function() {

	old_form = $( 'form[action="<?php echo $action; ?>"]' );
	old_form.not('.update_address_form').remove();	
});
//--></script> 



<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="address" class="update_address_form">
    <h2><?php echo $text_edit_address; ?></h2>
	
	
	<?php // Set disabled fields to empty to avoid compatibility problems with other extensions ?>

	<input type="hidden" name="firstname"	value="<?php echo $firstname; ?>" />
	<input type="hidden" name="lastname"	value="<?php echo $lastname; ?>" />
	<input type="hidden" name="company"		value="<?php echo $company; ?>" />
	<input type="hidden" name="address_1"	value="<?php echo $address_1; ?>" />
	<input type="hidden" name="address_2"	value="<?php echo $address_2; ?>" />
	<input type="hidden" name="city"		value="<?php echo $city; ?>" />
	<input type="hidden" name="postcode"	value="<?php echo $postcode; ?>" />
	<input type="hidden" name="country_id"	value="<?php echo $country_id; ?>" />
	<input type="hidden" name="zone_id"		value="<?php echo $zone_id; ?>" />

	
	
	<div class="content">
      <table class="form">
	  
		<?php if ($custom_register_account_member_enable_firstname || $custom_register_member_enable_firstname) { ?>
			<tr>
				<td>
					<?php if ($custom_register_account_member_firstname_required || $custom_register_member_firstname_required) { ?> <span class="required">*</span> <?php } ?> 
					<?php echo $entry_firstname; ?>
				</td>
				<td><input type="text" name="firstname" value="<?php echo $firstname; ?>" />
				<?php if ($error_firstname) { ?>
				<span class="error"><?php echo $error_firstname; ?></span>
				<?php } ?></td>
			</tr>
		<?php } ?>
		
		
		<?php if ($custom_register_account_member_enable_lastname || $custom_register_member_enable_lastname) { ?>  
			<tr>
				<td>
					<?php if ($custom_register_account_member_lastname_required || $custom_register_member_lastname_required) { ?> <span class="required">*</span> <?php } ?> 
					<?php echo $entry_lastname; ?>
				</td>
				<td><input type="text" name="lastname" value="<?php echo $lastname; ?>" />
				<?php if ($error_lastname) { ?>
				<span class="error"><?php echo $error_lastname; ?></span>
				<?php } ?></td>
			</tr>
		<?php } ?>
	  

		<?php if ($custom_register_account_member_enable_company || $custom_register_member_enable_company) { ?>
			<tr>
				<td>
					<?php if ($custom_register_account_member_company_required || $custom_register_member_company_required ) { ?> <span class="required">*</span> <?php } ?>
					<?php echo $entry_company; ?>
				</td>
				<td><input type="text" name="company" value="<?php echo $company; ?>" />
				<?php if ($error_company) { ?>
				<span class="error"><?php echo $error_company; ?></span>
				<?php } ?></td>
			</tr>
		<?php } ?>
	
		
<?php // TAX ID AND COMPANY ID HAVE BEEN ADDED WITH Oc 1.5.3
	if ( version_compare(VERSION, '1.5.3', '>=') ) { ?>
		
        <?php if ( $company_id_display && count($customer_groups) > 0 ) {  ?>
        <tr>
          <td><?php echo $entry_company_id; ?></td>
          <td><input type="text" name="company_id" value="<?php echo $company_id; ?>" />
            <?php if ($error_company_id) { ?>
            <span class="error"><?php echo $error_company_id; ?></span>
            <?php } ?></td>
        </tr>
        <?php } else { // post an empty value - bug fix for Oc <= 1.5.3 ?>
			<input type="hidden" name="company_id" value="" class="large-field" />
		<?php } ?>
		
		
        <?php if ( $tax_id_display && count($customer_groups) > 0 ) { ?>
        <tr>
          <td><?php echo $entry_tax_id; ?></td>
          <td><input type="text" name="tax_id" value="<?php echo $tax_id; ?>" />
            <?php if ($error_tax_id) { ?>
            <span class="error"><?php echo $error_tax_id; ?></span>
            <?php } ?></td>
        </tr>  
		<?php } else { // post an empty value - bug fix for Oc <= 1.5.3 ?>
			<input type="hidden" name="tax_id" value="" class="large-field" />
		<?php } ?>

<?php } ?>		
				
		<?php if ($custom_register_account_member_enable_address_1 || $custom_register_member_enable_address_1) { ?>
			<tr>
				<td>
					<?php if ($custom_register_account_member_address_1_required || $custom_register_member_address_1_required) { ?> <span class="required">*</span> <?php } ?>
					<?php echo $entry_address_1; ?>
				</td>
				<td><input type="text" name="address_1" value="<?php echo $address_1; ?>" />
				<?php if ($error_address_1) { ?>
				<span class="error"><?php echo $error_address_1; ?></span>
				<?php } ?></td>
			</tr>
		<?php } ?>
			
		
		<?php if ($custom_register_account_member_enable_address_2 || $custom_register_member_enable_address_2) { ?>
			<tr>	
				<td>
					<?php if ($custom_register_account_member_address_2_required || $custom_register_member_address_2_required) { ?> <span class="required">*</span> <?php } ?>
					<?php echo $entry_address_2; ?>
				</td>
				<td><input type="text" name="address_2" value="<?php echo $address_2; ?>" />
				<?php if ($error_address_2) { ?>
				<span class="error"><?php echo $error_address_2; ?></span>
				<?php } ?></td>
			</tr>
		<?php } ?>
			
		
		<?php if ($custom_register_account_member_enable_city || $custom_register_member_enable_city) { ?>
			<tr>
				<td>
					<?php if ($custom_register_account_member_city_required || $custom_register_member_city_required) { ?> <span class="required">*</span> <?php } ?>  
					<?php echo $entry_city; ?>
				</td>
				<td><input type="text" name="city" value="<?php echo $city; ?>" />
				<?php if ($error_city) { ?>
				<span class="error"><?php echo $error_city; ?></span>
				<?php } ?></td>
			</tr>
		<?php } ?>
		
		
	
		
			<?php if ($custom_register_account_member_enable_postcode || $custom_register_member_enable_postcode) { ?>
				<tr>
					<td>
					
						<?php if ($custom_register_account_member_postcode_required || $custom_register_member_postcode_required) { ?> 
							<span class="required">*</span> 
						<?php } else {?>
							<span class="postcode-required required">*</span>
						<?php } ?>
					
						<?php echo $entry_postcode; ?>
					</td>
					<td><input type="text" name="postcode" value="<?php echo $postcode; ?>" />
					<?php if ($error_postcode) { ?>
					<span class="error"><?php echo $error_postcode; ?></span>
					<?php } ?></td>
				</tr>

			<?php } elseif ($custom_register_account_member_enable_country || $custom_register_member_enable_country || $country_disabled_zone_enabled) { ?>
				<tr class="postcode-required">
					<td><span class="required">*</span> <?php echo $entry_postcode; ?></td>
					<td><input id="empty_if_hidden" type="text" name="postcode" value="<?php echo $postcode; ?>" />
					<?php if ($error_postcode) { ?>
					<span class="error"><?php echo $error_postcode; ?></span>
					<?php } ?></td>
				</tr>
			<?php } ?>
		

		
			<?php if ($custom_register_account_member_enable_country || $custom_register_member_enable_country) { ?>
				<tr>
					<td>
						<?php if ($custom_register_account_member_country_required || $custom_register_member_country_required) { ?> <span class="required">*</span> <?php } ?>  
						<?php echo $entry_country; ?>
					</td>
					<td>				
					<select name="country_id">
					<option value=""><?php echo $text_select; ?></option>
					<?php foreach ($countries as $country) { ?>
					<?php if ($country['country_id'] == $country_id) { ?>
					<option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
					<?php } else { ?>
					<option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
					<?php } ?>
					<?php } ?>
					</select>			
					<?php if ($error_country) { ?>
					<span class="error"><?php echo $error_country; ?></span>
					<?php } ?></td>
				</tr>
			<?php } ?>
			
			
			<?php if ($custom_register_account_member_enable_zone || $custom_register_member_enable_zone) { ?>	
				<tr>
					<td>
						<?php if ($custom_register_account_member_zone_required || $custom_register_member_zone_required) { ?> <span class="required">*</span> <?php } ?>
						<?php echo $entry_zone; ?>
					</td>
					<td><select name="zone_id">
					</select>
					<?php if ($error_zone) { ?>
					<span class="error"><?php echo $error_zone; ?></span>
					<?php } ?></td>
				</tr>
			<?php } ?>

		
		
        <tr>
          <td><?php echo $entry_default; ?></td>
          <td><?php if ($default) { ?>
            <input type="radio" name="default" value="1" checked="checked" />
            <?php echo $text_yes; ?>
            <input type="radio" name="default" value="0" />
            <?php echo $text_no; ?>
            <?php } else { ?>
            <input type="radio" name="default" value="1" />
            <?php echo $text_yes; ?>
            <input type="radio" name="default" value="0" checked="checked" />
            <?php echo $text_no; ?>
            <?php } ?></td>
        </tr>
		
      </table>
    </div>
	
	
	
	<div class="buttons clearfix">
		<?php 	// On Oc <= 1.5.1.3.1 <input> buttons were <a> links
		if ( version_compare(VERSION, '1.5.1.3.1', '<=') ) { ?>
			<div class="left"><a href="<?php echo $back; ?>" class="button"><span><?php echo $button_back; ?></span></a></div>
			<div class="right"><a onclick="$('#address').submit();" class="button"><span><?php echo $button_continue; ?></span></a></div>
		<?php } else { ?>
			<div class="left"><a href="<?php echo $back; ?>" class="button"><?php echo $button_back; ?></a></div>
			<div class="right"><input type="submit" value="<?php echo $button_continue; ?>" class="button" /></div>
		<?php } ?>
	</div>
	
	
	
</form>

<?php require_once(DIR_TEMPLATE.'default/template/crf_include/crf_load_zones.tpl'); ?>
 