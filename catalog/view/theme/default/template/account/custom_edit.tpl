<?php
// ***************************************************
//             Custom Registration Fields   
//       
// Author : Francesco Pisanò - francesco1279@gmail.com
//              
//                   www.leverod.com		
//               © All rights reserved	  
// ***************************************************
?>


<style>
.hide {
	display: none!important;
}
.img-preview-sm {
	width: 200px;
	height: 200px;
}
/* Image cropper style */
.img-container,
.img-preview {
	overflow: hidden;
	text-align: center;
}
.ibox-content {
	margin-left: 20px;
	margin-top: 20px;
}
</style>

<?php
// this javascript removes the old form from the page. It has already been hidden by 
// vQmod with a <form style="display:none"... to prevent displaying it while the page 
// is loading. ?>
<script type="text/javascript"><!--
$(document).ready(function() {

	old_form = $( 'form[action="<?php echo $action; ?>"]' );
	old_form.not('.edit_account_form').remove();

});
//--></script> 


  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="edit" class="edit_account_form">
    
	
	
	<?php // Set disabled fields to empty to avoid compatibility problems with other extensions ?>
	
	<input type="hidden" name="firstname"	value="<?php echo $firstname; ?>" />
	<input type="hidden" name="lastname"	value="<?php echo $lastname; ?>" />
	<input type="hidden" name="telephone"	value="<?php echo $telephone; ?>" />
	<input type="hidden" name="fax"			value="<?php echo $fax; ?>" />

		
	<h2><?php echo $text_your_details; ?></h2>
    <div class="content">
	

      <table class="form">
	    
		<?php if ($custom_register_account_member_enable_firstname || $custom_register_member_enable_firstname) { ?>
			<tr>
				<td>
					<?php if ($custom_register_account_member_firstname_required || $custom_register_member_firstname_required) { ?> <span class="required">*</span> <?php } ?> 
					<?php echo $entry_firstname; ?>
				</td>
				<td><input type="text" name="firstname" value="<?php echo $firstname; ?>" />
				<?php if ($error_firstname) { ?>
				<span class="error"><?php echo $error_firstname; ?></span>
				<?php } ?></td>
			</tr>
		<?php } ?>
		
		
		<?php if ($custom_register_account_member_enable_lastname || $custom_register_member_enable_lastname) { ?>  
			<tr>
				<td>
					<?php if ($custom_register_account_member_lastname_required || $custom_register_member_lastname_required) { ?> <span class="required">*</span> <?php } ?> 
					<?php echo $entry_lastname; ?>
				</td>
				<td><input type="text" name="lastname" value="<?php echo $lastname; ?>" />
				<?php if ($error_lastname) { ?>
				<span class="error"><?php echo $error_lastname; ?></span>
				<?php } ?></td>
			</tr>
		<?php } ?>
		
        <tr>
          <td><span class="required">*</span> <?php echo $entry_email; ?></td>
          <td><input type="text" name="email" value="<?php echo $email; ?>" />
            <?php if ($error_email) { ?>
            <span class="error"><?php echo $error_email; ?></span>
            <?php } ?></td>
        </tr>
		
		
		<?php if ($custom_register_account_member_enable_telephone || $custom_register_member_enable_telephone) { ?>	
			<tr>
				<td>
					<?php if ($custom_register_account_member_telephone_required || $custom_register_member_telephone_required) { ?> <span class="required">*</span> <?php } ?> 
					<?php echo $entry_telephone; ?>
				</td>
				<td><input type="text" name="telephone" value="<?php echo $telephone; ?>" />
				<?php if ($error_telephone) { ?>
				<span class="error"><?php echo $error_telephone; ?></span>
				<?php } ?></td>
			</tr>
		<?php } ?>

		<?php if ($custom_register_account_member_enable_fax || $custom_register_member_enable_fax) { ?>
			<tr>
				<td>
					<?php if ($custom_register_account_member_fax_required || $custom_register_member_fax_required) { ?> <span class="required">*</span> <?php } ?>
					<?php echo $entry_fax; ?>
				</td>
				<td><input type="text" name="fax" value="<?php echo $fax; ?>" />
				<?php if ($error_fax) { ?>
				<span class="error"><?php echo $error_fax; ?></span>
				<?php } ?></td>
			</tr>
		<?php } ?>
		

      </table>
    </div>

	<h2><?php echo $text_your_photo; ?></h2>

	<div class="content">
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-6" style="width:25%;float:left">
                    <h4><?php echo $text_your_avatar_profile; ?></h4>
                    <div class="image-crop">
                        <img id="user_transform_image" class="img img-responsive" src="<?php echo $customer_photo; ?>">
                    </div>
                </div>
                <div class="col-md-6" style="width:25%;float:left;margin-left:30px;">
                    <h4><?php echo $text_preview_image_title; ?></h4>
                    <div id="user_preview_image" class="img-preview img-preview-sm"></div>
                    <p><?php echo $text_preview_image_description; ?></p>
                    <div class="btn-group">
                        <label title="Upload image file" for="input_user_image" class="btn btn-primary"><input type="file" accept="image/*" name="file" id="input_user_image" class="hide"><?php echo $button_upload; ?></label>
                        <label title="Save image" class="btn btn-primary save-image" form_id="#form_user_avatar" image_id="#user_transform_image"><?php echo $button_save; ?></label>
                    </div>
                </div>
            </div>
        </div>
	</div>	
		
	<div class="buttons clearfix">
		<?php 	// On Oc <= 1.5.1.3.1 <input> buttons were <a> links
		if ( version_compare(VERSION, '1.5.1.3.1', '<=') ) { ?>
			<div class="left"><a href="<?php echo $back; ?>" class="button"><span><?php echo $button_back; ?></span></a></div>
			<div class="right"><a onclick="$('#edit').submit();" class="button"><span><?php echo $button_continue; ?></span></a></div>
		<?php } else { ?>
			 <div class="left"><a href="<?php echo $back; ?>" class="button"><?php echo $button_back; ?></a></div>
			<div class="right"><input type="submit" value="<?php echo $button_continue; ?>" class="button" /></div>
		<?php } ?>
	</div>

  </form>
	
	

  <script type="text/javascript">
	$(document).ready(function(){

		$('#user_transform_image').cropper({
        aspectRatio: 1,
        preview: "#user_preview_image",
        done: function(data) {
            // Output the result data for cropping image.
        }
    });

    if (window.FileReader) {
		$("#input_user_image").change(function() {
            var fileReader = new FileReader(), files = this.files, file;
            if (!files.length) return;
            file = files[0];
            if (/^image\/\w+$/.test(file.type)) {
                fileReader.readAsDataURL(file);
                fileReader.onload = function () {
                    $("#input_user_image").val("");
                    $("#user_transform_image").cropper("reset", true).cropper("replace", this.result);
                };
            } else {
                alert("Please choose an image file.");
            }
        });
    } else {
		$("#input_user_image").addClass("hide");
        $("#input_developer_image").addClass("hide");
    }

	$(".save-image").click(function() {
        $.ajax({
			type: 'post',
			url: 'index.php?route=account/edit/saveCustomerAvatar',
			data: 'image='+$($(this).attr('image_id')).cropper("getDataURL"),
			dataType: 'json',
			beforeSend: function() {
				
			},
			complete: function() {
				   
			},
			success: function(json) {
				alert(json['success']);
				//showAlert(form_id, json);
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});  
    });

    $(".zoomIn").click(function() {
        $($(this).attr('image_id')).cropper("zoom", 0.1);
    });

    $(".zoomOut").click(function() {
        $($(this).attr('image_id')).cropper("zoom", -0.1);
    });

    $(".rotateLeft").click(function() {
         $($(this).attr('image_id')).cropper("rotate", 90);
    });

    $(".rotateRight").click(function() {
         $($(this).attr('image_id')).cropper("rotate", -90);
    });

    $(".setDrag").click(function() {
         $($(this).attr('image_id')).cropper("setDragMode", "crop");
    });

	});
  </script>
