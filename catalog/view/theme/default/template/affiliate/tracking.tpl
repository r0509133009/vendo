<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1><?php echo $heading_title; ?></h1>
  <p><?php echo $text_description; ?></p>
  <div class="content">
<script type="text/javascript">
$.fn.tabs = function() {
	var selector = this;
	
	this.each(function() {
		var obj = $(this); 
		
		$(obj.attr('href')).hide();
		
		$(obj).click(function() {
			$(selector).removeClass('selected');
			
			$(selector).each(function(i, element) {
				$($(element).attr('href')).hide();
			});
			
			$(this).addClass('selected');
			
			$($(this).attr('href')).show();
			
			return false;
		});
	});

	$(this).show();
	
	$(this).first().click();
};
</script>

  	<div id="tabs" class="htabs">
    	<a href="#tab-banners" class="selected"><?php echo $text_banners; ?></a>
        <a href="#tab-code"><?php echo $text_code_bn_rand; ?></a>
        <a href="#tab-url"><?php echo $text_url_product; ?></a>
   	</div>

	<div id="tab-url" style="display:none;">
    	<p><?php echo $text_code; ?><br />
        	<textarea cols="40" rows="5"><?php echo $code; ?></textarea>
      	</p>
      	<p><?php echo $text_generator; ?><br />
        	<input type="text" name="product" value="" />
      	</p>
      	<p><?php echo $text_link; ?><br />
        	<textarea name="link" cols="40" rows="5"></textarea>
      	</p>
    </div>
<script type="text/javascript"><!--
$('input[name=\'product\']').autocomplete({
	delay: 0,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=affiliate/tracking/autocomplete&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.name,
						value: item.link
					}
				}));
			}
		});
	},
	select: function(event, ui) {

		$('input[name=\'product\']').attr('value', ui.item.label);
		$('textarea[name=\'link\']').attr('value', ui.item.value);
        $('input[name=\'product\']').val(ui.item.label);
        $('textarea[name=\'link\']').val(ui.item.value);
					 console.log(ui);	
		return false;
	},
	focus: function(event, ui) {
      return false;
   }
});
//--></script>
<script type="text/javascript">
<!--
$('#tabs a').tabs(); 
//--></script> 
    <div id="tab-banners">
    				<table class="list">
                    	<thead>
                        	<tr>
                            	<td width="116" class="text-center"><?php echo $text_image; ?></td>
                                <td class="text-center"><?php echo $text_size; ?></td>
                                <td class="text-center"><?php echo $text_date; ?></td>
                                <td class="text-center"><?php echo $text_code; ?></td>
                                <td class="text-center"><?php echo $text_detail; ?></td>
                            </tr>
                        </thead>
                        <tbody>
<style>
.img_view {
	text-align:center;
	cursor:pointer;
}
.list > tbody > tr > td, .list > tbody > tr > th, .list > tfoot > tr > td, .list > tfoot > tr > th, .list > thead > tr > td, .list > thead > tr > th { vertical-align: middle !important; padding: 7px !important;}
.popover { width: auto !important; max-width:800px !important; background: #E4E2E2;}
.popover {
    background-clip: padding-box;
    background-color: #fff;
    border: 1px solid rgba(0, 0, 0, 0.2);
    border-radius: 6px;
    box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
    display: none;
    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
    font-size: 14px;
    font-style: normal;
    font-weight: 400;
    left: 0;
    letter-spacing: normal;
    line-height: 1.42857;
    max-width: 276px;
    padding: 1px;
    position: absolute;
    text-align: start;
    text-decoration: none;
    text-shadow: none;
    text-transform: none;
    top: 0;
    white-space: normal;
    word-break: normal;
    word-spacing: normal;
    word-wrap: normal;
    z-index: 1060;
}
.fade {
    opacity: 0;
    transition: opacity 0.15s linear 0s;
}
.popover.right {
    margin-left: 10px;
}
.fade.in {
    opacity: 1;
}
.popover.right > .arrow::after {
    border-left-width: 0;
    border-right-color: #fff;
    bottom: -10px;
    content: " ";
    left: 1px;
}
.popover > .arrow::after {
    border-width: 10px;
    content: "";
}
.popover > .arrow, .popover > .arrow::after {
    border-color: transparent;
    border-style: solid;
    display: block;
    height: 0;
    position: absolute;
    width: 0;
}
.popover.right > .arrow {
    border-left-width: 0;
    border-right-color: rgba(0, 0, 0, 0.25);
    left: -11px;
    margin-top: -11px;
    top: 50%;
}
.popover > .arrow {
    border-width: 11px;
}
.popover > .arrow, .popover > .arrow::after {
    border-color: transparent;
    border-style: solid;
    display: block;
    height: 0;
    position: absolute;
    width: 0;
}
.popover.top {
    margin-top: -10px;
}
.popover.top > .arrow::after {
    border-bottom-width: 0;
    border-top-color: #fff;
    bottom: 1px;
    content: " ";
    margin-left: -10px;
}
.popover.top > .arrow {
    border-bottom-width: 0;
    border-top-color: rgba(0, 0, 0, 0.25);
    bottom: -11px;
    left: 50%;
    margin-left: -11px;
}
.popover.bottom {
    margin-top: 10px;
}
.popover.bottom > .arrow {
    border-bottom-color: rgba(0, 0, 0, 0.25);
    border-top-width: 0;
    left: 50%;
    margin-left: -11px;
    top: -11px;
}
.popover-content {
    padding: 9px 14px;
}
.popover.bottom > .arrow::after {
    border-bottom-color: #fff;
    border-top-width: 0;
    content: " ";
    margin-left: -10px;
    top: 1px;
}
</style>
                        <?php if ($banners) { ?>
<script src="display/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
                        <?php foreach ($banners as $banner) { ?>
<?php
	if(parse_url($banner['bn_img'], PHP_URL_HOST) == $_SERVER['HTTP_HOST']){
		$change	= explode("images", $banner['bn_img']);
		//$thumb	= $change[0]."_thumbs/Images".$change[1];
	} else {
		//$thumb = $banner['bn_img'];
	}
    $check_size = explode("x", $banner['bn_size']);
	$width  = $check_size[0];
	$height = $check_size[1];
if($_SERVER['SERVER_PORT']!=443){
	$domainurl = "http://".$_SERVER['HTTP_HOST'];
} else {
	$domainurl = "https://".$_SERVER['HTTP_HOST'];
}
?>
<script> $(document).ready(function(){ $(".img_view_<?php echo $banner['bn_id']; ?>").popover({content: "<img width='<?php echo $width; ?>' height='<?php echo $height; ?>' src='<?php echo $banner['bn_img']; ?>' />", html: true, placement: "right"}); $(".get_code_<?php echo $banner['bn_id']; ?>").popover({content: "<textarea rows='4' class='form-control' style='width: 468px;'><a href='<?php echo $domainurl; ?>/display/click.php?bnid=<?php echo $banner['bn_id']; ?>&aff=<?php echo $code; ?>' target='_blank' rel='nofollow'><img src='<?php echo $domainurl; ?>/display/impression.php?bnid=<?php echo $banner['bn_id']; ?>&aff=<?php echo $code; ?>' alt='<?php echo $banner['bn_name']; ?>' width='<?php echo $width; ?>' height='<?php echo $height; ?>' /></a></textarea>", html: true, placement: "bottom"}); }); </script>
							<tr>
                            	<td width="116" height="99">
                                <div class="img_view img_view_<?php echo $banner['bn_id']; ?>">
                                	<?php if($width <= 300){ ?>
                                		<img height="88" src="<?php echo $banner['bn_img']; ?>" title="<?php echo $banner['bn_name']; ?>" />
                                    <?php } else { ?>
                                    	<img width="100" src="<?php echo $banner['bn_img']; ?>" title="<?php echo $banner['bn_name']; ?>" />
                                    <?php } ?>
                                </div>
                                </td>
                                <td class="text-center"><?php echo $banner['bn_size']; ?></td>
                                <td class="text-center">
                                <?php echo $banner['bn_create']; ?>
                                </td>
                                <td class="text-center">
                                	<strong style="color:blue; cursor:pointer;" class="get_code_<?php echo $banner['bn_id']; ?>">
                                    	<i class="fa fa-code" style="font-size:18px;"></i>
                                        GET CODE
                                    </strong>
                                </td>
                                <td>
                                <strong><?php echo $banner['bn_name']; ?></strong>,<br>
                                <?php echo $banner['bn_des']; ?>
                                </td>
                            </tr>
                        <?php } ?>
                        <?php } else { ?>
                        	<tr>
                            	<td colspan="5" class="text-center"><?php echo $text_no_data; ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
    </div>

    <div id="tab-code" style="display:none;">

<script>
$(document).ready(function() {
	$('.get_code').bind('click', function() {
		var size_code 	= $("#size_list").val();
		var sizebn = $("#size_list").val().split("x");
		var width = sizebn[0];
		var height = sizebn[1];
		if(size_code != ''){
			document.getElementById("input-size").innerHTML = "&lt;script&gt; var affdomain = '<?php echo $domainurl; ?>'; var affcode = '<?php echo $code; ?>'; var affhieght = "+sizebn[1]+"; var affwidth = "+width+";&lt;/script&gt;&lt;script src='<?php echo $domainurl; ?>/display/banner.js?size="+size_code+"&aff=<?php echo $code; ?>'&gt;&lt;/script&gt;";
		} else {
			document.getElementById("input-size").innerHTML = "";
		}
	});
});
</script>
    	<table class="list">
        	<tr>
            	<td>
                	<?php echo $text_choose_size; ?>
                </td>
            	<td>
               		<select class="form-control" id="size_list">
                      	<option value=""><?php echo $text_choose_size_bn; ?></option>
                    	<?php foreach ($sizes as $size_bn) { ?>
                   		<option value="<?php echo $size_bn['size_name']; ?>"><?php echo $size_bn['size_name']; ?></option>
                    	<?php } ?>
                	</select>
                    <button class="btn btn-info form-control get_code" type="button"><?php echo $text_get_code; ?></button>
              	</td>
       		</tr>
            <tr>
            	<td>
                	<?php echo $text_code; ?>
                </td>
                <td>
                	<textarea style="width:98%;" class="form-control" rows="3" id="input-size"></textarea>
                </td>
            </tr>
        </table>
    </div>
    
  </div>
  

</script>  
  <div class="buttons">
    <div class="right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a></div>
  </div>
  <?php echo $content_bottom; ?>
</div>
<style>
    #container i:before,#container i:after {
        font-family:  FontAwesome;
    }
    button {
        background-color: rgb(221, 221, 221);
    }
    #container button {
    color: rgb(68, 67, 73); 
    }
    #container button:hover {
    color:rgb(76, 179, 86);
    }
</style>
<?php echo $footer; ?>