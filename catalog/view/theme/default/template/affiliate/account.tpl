<?php echo $header; ?>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?></div>
<?php } ?>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  
<script src="display/jquery.min.js"></script>
<script src="catalog/view/javascript/highcharts/exporting.js" type="text/javascript"></script>
<script src="catalog/view/javascript/highcharts/highcharts.js" type="text/javascript"></script>
<style>
.highcharts-button, .highcharts-legend { display: none !important;}
#container .row { display: table; width:100%; margin-bottom: 20px;}
.col-sm-8 { width: 65%; float:left;display: table;}
.col-sm-8 .panel { display:table;}
.col-sm-4 { width: 33.333%; float:right;}
.panel { border: 1px solid #ddd; padding: 7px; display:block;}
.panel .panel-heading {background: #4090d1 none repeat scroll 0 0;padding: 10px;}
.panel .panel-heading h3 {
	color: #fff;
    display: inline-block;
    margin: 0;
}
.choose_month {
	display: inline-block;
    float: right;
    margin-top: -4px;
}	
.text-center { text-align: center; padding:7px;}
hr { border-color: #ddd;}
.event {text-align: center;}
.event .blance { font-size: 18px; color:#0AB72E; font-weight:bold;}
.list {
    border-collapse: collapse;
    border-left: 1px solid #dddddd;
    border-top: 1px solid #dddddd;
    margin-bottom: 20px;
    width: 100%;
}
.list .left {
    padding: 7px;
    text-align: left;
}
.list thead td a, .list thead td {
    color: #222222;
    font-weight: bold;
    text-decoration: none;
}
.list thead td {
    background-color: #efefef;
    padding: 0 5px;
}
.list td {
    border-bottom: 1px solid #dddddd;
    border-right: 1px solid #dddddd;
}
.list tbody td {
    background: #ffffff none repeat scroll 0 0;
    padding: 0 5px;
    vertical-align: middle;
}
.content_off :before,.content_off :after{
	font-family: FontAwesome;
}
#container a:hover{
        color:rgb(76, 179, 86);
}
</style>
<script>
$(document).ready(function() {
	/* Search */
	$('.button-month').bind('click', function() {
		var month = $("#value_month").val();
		if (month != '') {
			url = "<?php echo $breadcrumb['href']; ?>&month=" + month;
		}
		location = url;
	});
});
</script>
<div class="box">
	<div class="heading">
    	<h1>Dashboard</h1>
    </div>
    <div class="content_off">
		<div class="row">
        	<div class="col-sm-8">
<script type="text/javascript">
$(function () {
    $('#containers').highcharts({
        chart: {
            type: 'areaspline'
        },
		colors: ["#57c713", "#6fb3e0", "#d87311"],
        title: {
            text: ''
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: 150,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        xAxis: {
            categories: <?php if(isset($category)){ echo $category; } ?>

        },
        yAxis: {
            title: {
                text: 'Clicks'
            }
        },
        tooltip: {
            shared: true,
            valueSuffix: ''
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            areaspline: {
                fillOpacity: 0.5
            }
        },
        series: [{
            name: 'Click',
            data: <?php if(isset($series)){ echo $series; } ?>
        }]
    });
});
</script>
            	<div class="panel panel-info">
                    <?php if(isset($category)){ ?>
            		<div id="containers" style="height:210px; width:485px;"></div>
                    <?php } else { ?>
                    <p class="text-center" style="padding:10px; margin:0;"> <?php echo $text_no_data; ?> </p>
                    <?php } ?>
                </div>
            </div>
            <div class="col-sm-4">
            	<div class="panel panel-default">
            		<div class="event">
                    <?php echo $text_balance; ?> <span class="blance">$<?php echo number_format($totalamount,2); ?></span>
                    <hr>
                    <?php if(isset($last_sales)){ ?>
                    <?php foreach ($last_sales as $last_sale) { ?>
                    <p><?php echo $last_sale['description']; ?>: <?php echo $last_sale['date_added']; ?>, $<?php echo number_format($last_sale['amount'],2); ?></p>
                    <?php } ?>
                    <?php } else { ?>
                    <p class="text-center" style="padding:10px; margin:0;"> <?php echo $text_no_data; ?> </p>
                    <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    	
        <div class="row">
        	<div class="col-sm-12">
              	<div class="panel panel-default" style="margin-bottom:0; padding:0; border:none;">
                	<div class="panel-heading">
    					<h3 style="display:inline-block;" class="panel-title"><i class="fa fa-bar-chart-o"></i> <?php echo $text_stats_day_month; ?></h3>
                        <div class="choose_month">
                    	<select class="month" id="value_month" name="month">
                            <?php foreach($group_months as $group_month){ ?>
                            <?php if($smonth == $group_month['stats_month']){ ?>
                            	<option selected value="<?php echo $group_month['stats_month']; ?>"><?php echo $group_month['stats_month']; ?></option>
                           	<?php } else { ?>
                            	<option value="<?php echo $group_month['stats_month']; ?>"><?php echo $group_month['stats_month']; ?></option>
                           	<?php } ?>
                            
                            <?php } ?>
                        </select>
                        <button class="button-month" type="button">Apply</button>
                    	</div>
  					</div>
                    <div class="table-responsive">
                    	<table class="list">
                    		<thead>
                            	<tr>
                                	<td class="left"><?php echo $text_date; ?></th>
                                    <td class="left"><?php echo $text_impression; ?></th>
                                    <td class="left"><?php echo $text_click; ?></th>
                                    <td class="left"><?php echo $text_sale; ?></th>
                                    <td class="left"><?php echo $text_amount; ?></th>
                                    <td class="left"><?php echo $text_ctr; ?></th>
                                    <td class="left"><?php echo $text_cr; ?></th>
                                </tr>
                        	</thead>
                            <?php if(isset($stats)){ ?>
                            <tbody>
                            <?php
                            $total_views = 0;
                            $total_click = 0;
                            $total_sale = 0;
                            $total_amount = 0;
                            ?>
                            
                            <?php foreach ($stats as $stat) { ?>
                            <?php
                            	$total_views = $total_views + $stat['stats_views'];
                            	$total_click = $total_click + $stat['stats_click'];
                            	$total_sale = $total_sale + $stat['stats_sale'];
                                $total_amount = $total_amount + $stat['sale_amount'];
                            ?>
                            	<tr>
                                	<td class="left"><?php echo $stat['stats_date']; ?></td>
                                    <td class="left"><?php echo number_format($stat['stats_views'],0); ?></td>
                                    <td class="left"><?php echo number_format($stat['stats_click'],0); ?></td>
                                    <td class="left"><?php echo number_format($stat['stats_sale'],0); ?></td>
                                    <td class="left">$<?php echo number_format($stat['sale_amount'],2); ?></td>
                                    <td class="left">
                                    <?php if($stat['stats_views'] > 0){ ?>
                                    <?php echo @number_format((@$stat['stats_click']/@$stat['stats_views'])*100,2); ?>%
                                    <?php } else { ?>
                                    0.00%
                                    <?php } ?>
                                    </td>
                                    <td class="left">
                                    <?php if($stat['stats_click'] > 0){ ?>
                                    <?php echo @number_format((@$stat['stats_sale']/@$stat['stats_click'])*100,2); ?>%
                                    <?php } else { ?>
                                    0.00%
                                    <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            
                            </tbody>
                        	<thead>
                            	<tr>
                                	<td class="left"><?php echo $text_total_month; ?> (<?php echo $smonth; ?>)</th>
                                    <td class="left"><?php echo number_format($total_views,0); ?></th>
                                    <td class="left"><?php echo number_format($total_click,0); ?></th>
                                    <td class="left"><?php echo number_format($total_sale,0); ?></th>
                                    <td class="left">$<?php echo number_format($total_amount,2); ?></th>
                                    <td class="left"><?php echo number_format((@$total_click/@$total_views)*100,2); ?>%</th>
                                    <td class="left"><?php echo number_format((@$total_sale/@$total_click)*100,2); ?>%</th>
                                </tr>
                        	</thead>
                            <?php } else { ?>
                            <tbody>
                            	<tr>
                                	<td colspan="7" class="text-center left" style="text-align:center;"><?php echo $text_no_data; ?></td>
                                </tr>
                            </tbody>
                            <thead>
                            	<tr>
                                	<td class="left"><?php echo $text_total_month; ?> (<?php echo $smonth; ?>)</th>
                                    <td class="left">0</th>
                                    <td class="left">0</th>
                                    <td class="left">0</th>
                                    <td class="left">$0.00</th>
                                    <td class="left">0.00%</th>
                                    <td class="left">0.00%</th>
                                </tr>
                        	</thead>
                            <?php } ?>
                        </table>
          			</div>
            	</div>
            </div>
        </div>
<hr>
	<div class="row">   
        <div class="col-sm-4">
      	<h3><?php echo $text_my_account; ?></h3>
          <ul class="list-unstyled">
            <li><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></li>
            <li><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></li>
            <li><a href="<?php echo $payment; ?>"><?php echo $text_payment; ?></a></li>
          </ul>
      </div>
      <div class="col-sm-4">
      	<h3><?php echo $text_my_tracking; ?></h3>
      	<ul class="list-unstyled">
        	<li><a href="<?php echo $tracking; ?>"><?php echo $text_tracking; ?></a></li>
      	</ul>
      </div>
      <div class="col-sm-4">
      	<h3><?php echo $text_my_transactions; ?></h3>
      	<ul class="list-unstyled">
        	<li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
      	</ul>
      </div>
      </div>
        
    </div>
</div>

  <?php echo $content_bottom; ?>
</div>
<?php echo $footer; ?>