<div id="livehelp" class="lh-widget box">
   <!-- WIDGET HEAD -->
   <div id="livehelp-head" class="lh-widget-head box-heading">
      <div class="lh-head-left">
         <h5>
            <i class="icon-bubbles"></i> <span><?php echo $heading_title; ?></span>
         </h5>
      </div>
      <div style="clear: both;"></div>
   </div>
   <!-- WIDGET BODY -->
   <div id="livehelp-body" class="lh-widget-body collapse box-content">
      <div class="lh-login-form">
         <form action="" id="livehelp-form" role="form" class="form-horizontal form-condensed group-border-dashed">
            <p>
               <?php echo $text_offline_description; ?>
            </p>
            <b><?php echo $entry_livehelp_name; ?></b><br>
            <input type="text" class="lh-form-element" id="livehelp_name" name="livehelp_name" value="<?php echo $user_name; ?>"><br>
            <b><span class="required">*</span>  <?php echo $entry_livehelp_email; ?></b><br>
            <input type="text" class="lh-form-element" id="livehelp_email" name="livehelp_email" value="@"><br>
            <b><span class="required">*</span>  <?php echo $entry_livehelp_enquiry; ?></b><br>
            <textarea name="livehelp_enquiry" id="livehelp_enquiry" rows="3" class="lh-form-element"></textarea>
            <br>
            <div style="text-align:center;">
               <div class="lh-preloader"><img src="catalog/view/theme/default/image/loading.gif" alt=""> <?php echo $text_wait; ?></div>
               <input value="<?php echo $button_send; ?>" type="button" class="button lh-form-element lh-contact-form-submit" />
            </div>
         </form>
      </div>
   </div>
</div>
<script type="text/javascript">
   $(function() {
      $('#livehelp-head').bind("click", function(){
         var $target = $('#livehelp-body');
         if( $target.hasClass("collapse") ) {
            $target.slideDown(300, function(){
               $target.removeClass("collapse");
            });
         } else { 
            $target.slideUp(300, function(){
               $target.addClass("collapse");
            });
         }
      });
   
      $('.lh-contact-form-submit').bind("click", function(e) {
         e.preventDefault();
         var $t = $(this),
            $form = $('#livehelp-form');
         $.ajax({
            url: 'index.php?route=module/livehelp/operation',
            type: 'post',
            dataType: 'json',
            data: $form.serialize() + "&operation=contact",
            beforeSend: function() {
               $form.find(".error").remove();
               $t.prop("disabled", true);
               $('#livehelp-body .lh-preloader').fadeIn();
            },
            complete: function() {
               $t.prop("disabled", false);
               $('#livehelp-body .lh-preloader').fadeOut();
            },
            success: function(data) {
               if (data['error']) {
                  if (data['error']['email']) {
                     $('#livehelp_email').after('<span class="error">' + data['error']['email'] + '</span>');
                  }
                  if (data['error']['enquiry']) {
                     $('#livehelp_enquiry').after('<span class="error">' + data['error']['enquiry'] + '</span>');
                  }
               }
               if (data['success']) {
                  alert(data['success']);
                  $('#livehelp_enquiry').val('');
                  $('#livehelp-head').trigger('click');
               }
            }
         });
      });
   });
</script>