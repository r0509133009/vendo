<?php
$style = '';

if ($setting['background_color']) {
	$style .= ' background-color: ' . $setting['background_color'] . ';';
}

if ($setting['border_style']) {
	$style .= ' border: ' . $setting['border_style'] . ' ' . $setting['border_size'] . ' ' . $setting['border_color'] . ';';
}

if ($setting['border_radius_status']) {
	$style .= ' -webkit-border-radius: ' . $setting['border_radius_size'] . '; -moz-border-radius: ' . $setting['border_radius_size'] . '; border-radius: ' . $setting['border_radius_size'] . ';';
}

$style .= ' padding: ' . $setting['padding_top'] . ' ' . $setting['padding_right'] . ' ' . $setting['padding_bottom'] . ' ' . $setting['padding_left']. ';';
$style .= ' margin: ' . $setting['margin_top'] . ' ' . $setting['margin_right'] . ' ' . $setting['margin_bottom'] . ' ' . $setting['margin_left']. ';';
?>
<div class="SP_section SP_section_bank_transfer" style="width: <?php echo $setting['wrapper']; ?>;">
	<div class="SP_section_wrapper" style="<?php echo $style; ?>">
		<?php if ($setting['table_status']) { ?>
		<table class="table table-bordered SP_table">
			<thead>
				<tr>
					<td class="text-left"><?php echo isset($setting[$language_id]['title']) ? $setting[$language_id]['title'] : ''; ?></td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="text-left"><?php echo $instruction; ?></td>
				</tr>
			</tbody>
		</table>
		<?php } else { ?>
		<h2><?php echo isset($setting[$language_id]['title']) ? $setting[$language_id]['title'] : ''; ?></h2>
		<p><?php echo $instruction; ?></p>
		<?php } ?>
    </div>
</div>