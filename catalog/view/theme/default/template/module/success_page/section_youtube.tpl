<?php
$style = '';

if ($setting['background_color']) {
	$style .= ' background-color: ' . $setting['background_color'] . ';';
}

if ($setting['border_style']) {
	$style .= ' border: ' . $setting['border_style'] . ' ' . $setting['border_size'] . ' ' . $setting['border_color'] . ';';
}

if ($setting['border_radius_status']) {
	$style .= ' -webkit-border-radius: ' . $setting['border_radius_size'] . '; -moz-border-radius: ' . $setting['border_radius_size'] . '; border-radius: ' . $setting['border_radius_size'] . ';';
}

$style .= ' padding: ' . $setting['padding_top'] . ' ' . $setting['padding_right'] . ' ' . $setting['padding_bottom'] . ' ' . $setting['padding_left']. ';';
$style .= ' margin: ' . $setting['margin_top'] . ' ' . $setting['margin_right'] . ' ' . $setting['margin_bottom'] . ' ' . $setting['margin_left']. ';';
?>
<div class="SP_section SP_section_youtube" style="width: <?php echo $setting['wrapper']; ?>;">
    <div class="SP_section_wrapper" style="<?php echo $style; ?>">
        <?php $height = (!empty($setting['height'])) ? (int)$setting['height'] : 200; ?>
		<?php $width = (!empty($setting['width'])) ? (int)$setting['width'] : 210; ?>
        <object width="<?php echo $width; ?>" height="<?php echo $height; ?>">
          <param name="movie" value="https://www.youtube.com/v/<?php echo $setting['url']; ?>?version=3&autoplay=0"></param>
          <param name="allowScriptAccess" value="always"></param>
          <embed src="https://www.youtube.com/v/<?php echo $setting['url']; ?>?version=3&autoplay=0"
                 type="application/x-shockwave-flash"
                 allowscriptaccess="always"
                 width="<?php echo $width; ?>" height="<?php echo $height; ?>"></embed>
        </object>
    </div>
</div>