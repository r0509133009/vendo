<?php
$style = '';

if ($setting['background_color']) {
	$style .= ' background-color: ' . $setting['background_color'] . ';';
}

if ($setting['border_style']) {
	$style .= ' border: ' . $setting['border_style'] . ' ' . $setting['border_size'] . ' ' . $setting['border_color'] . ';';
}

if ($setting['border_radius_status']) {
	$style .= ' -webkit-border-radius: ' . $setting['border_radius_size'] . '; -moz-border-radius: ' . $setting['border_radius_size'] . '; border-radius: ' . $setting['border_radius_size'] . ';';
}

$style .= ' padding: ' . $setting['padding_top'] . ' ' . $setting['padding_right'] . ' ' . $setting['padding_bottom'] . ' ' . $setting['padding_left']. ';';
$style .= ' margin: ' . $setting['margin_top'] . ' ' . $setting['margin_right'] . ' ' . $setting['margin_bottom'] . ' ' . $setting['margin_left']. ';';
?>
<div class="SP_section SP_section_facebook" style="width: <?php echo $setting['wrapper']; ?>;">
    <div class="SP_section_wrapper" style="<?php echo $style; ?>">
        <div class="SP_table_responsive">
			<?php $height = (!empty($setting['height'])) ? (int)$setting['height'] : 200; ?>
			<?php $width = (!empty($setting['width'])) ? (int)$setting['width'] : 500; ?>
			<iframe src="//www.facebook.com/plugins/likebox.php?href=<?php echo htmlentities($setting['url'])?>&amp;width=<?php echo $width; ?>&amp;height=<?php echo $height; ?>&amp;colorscheme=light&amp;show_faces=true&amp;border_color=#ffffff&amp;stream=false&amp;header=false&amp;appId=" scrolling="no" frameborder="0" style="border: none; overflow: hidden; width: <?php echo $width; ?>px; height: <?php echo $height; ?>px;" allowTransparency="true"></iframe>
		</div>
	</div>
</div>