<div id="popup-quickorder">
<style>
#popup-quickorder {
  max-width: 650px;
}
*::before, *::after {
  box-sizing: border-box;
}
*:before, *:after {
  box-sizing: border-box;
}
* {
  box-sizing: border-box;
}

</style>
<div class="popup-heading"><?php echo isset($config_title_popup_quickorder[$lang_id]) ? $config_title_popup_quickorder[$lang_id]['config_title_popup_quickorder'] : ''; ?></div>
	<div class="popup-center">
		<form id="fastorder_data" enctype="multipart/form-data" method="post">
			 <div>
				<div style="margin-top:10px;">
					<div id="quick_checkout_cart">	
						<?php if ($products) { ?>
							<div class="col-qsm-12 text-center">
								<div class="well well-sm products">
									<?php foreach($products as $product) { ?>
										<div class="product">
											<div class="row">
												<div class="col-qxs-12 col-qsm-5">
													<div class="image">
														<?php if ($product['thumb']) { ?>
														<a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" /></a>
														<?php } ?>
													</div>
													<div class="pr-name quick-cell">
														<div class="quick-cell-content">
															<a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
															<?php if ($product['option']) { ?>
															<br />
																<?php foreach ($product['option'] as $option) { ?>
																	- <small><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small><br />
																<?php } ?>
															<?php } ?>													
														</div>
													</div>
												</div>
												<div class="col-qxs-12 col-qsm-7">
													<div class="col-qxs-5 quantity_quickorder quick-cell">
														<div class="quick-cell-content pquantity">
															<span class="quantity-quickorder">
																<input type="button" id="decrease" class="btn-update-popup minus_quantity" value="-" onclick="btnminus_<?php echo $product['product_id']; ?>('1');updateQuantity('<?php echo $product['product_id']; ?>');" />
																<input size="2" name="quantity[<?php echo $product['key']; ?>]" type="text" class="qty_fastorder" onchange="updateQuantity('<?php echo $product['product_id']; ?>');" id="htop_fastorder<?php echo $product['product_id']; ?>" value="<?php echo $product['quantity']; ?>" />		
																<input type="button" id="increase" class="btn-update-popup plus_quantity" value="+" onclick="btnplus_<?php echo $product['product_id']; ?>();updateQuantity('<?php echo $product['product_id']; ?>');" />
															</span>	
															<script type="text/javascript"><!--
																	function btnminus_<?php echo $product['product_id']; ?>(a){									
																	document.getElementById("htop_fastorder<?php echo $product['product_id']; ?>").value>a?document.getElementById("htop_fastorder<?php echo $product['product_id']; ?>").value--:document.getElementById("htop_fastorder<?php echo $product['product_id']; ?>").value=a;															
																	}									
																	function btnplus_<?php echo $product['product_id']; ?>(){
																	document.getElementById("htop_fastorder<?php echo $product['product_id']; ?>").value++;									
																	};							
														//--></script>
														</div>
													</div>
													<div class="col-qxs-5 quick-cell pprice">
														<div class="quick-cell-content">
															<?php echo $product['total']; ?>
														</div>
													</div>
													<div class="col-qxs-2 quick-cell text-center delete-pr">
														<div class="quick-cell-content">
															<a href="javascript:void(0);" onclick="deleteProduct('<?php echo $product['key']; ?>');return false;" title="<?php echo $button_remove; ?>" class=""><i class="fa fa-trash-o fa-lg"></i></a>
														</div>
													</div>
												</div>
											</div>
										</div>
									<?php } ?>
								</div>
							</div>
							<div class="col-qxs-12 col-qsm-12 col-qmd-12 form-group">	
								<div class="totals text-center">
									<a class="popupTotal" onclick="openTotal()" href="javascript:void(0);" title="<?php echo $text_total_qucik_ckeckout; ?>"><?php echo $text_total_qucik_ckeckout; ?>&nbsp;&nbsp;<i class="fa fa-angle-down"></i></a>
									<div id="total-quick-ckeckout">
										<table>
											<?php foreach ($totals as $total) { ?>
											<tr>
												<td class="text-right" style="width: 50%"><strong><?php echo $total['title']; ?></strong></td>
												<td class="text-left" style="width: 50%"><?php echo $total['text']; ?></td>
											</tr>
											<?php } ?>
											<input type="hidden" value="<?php echo $total_order; ?>" name="total_order" />											
										</table>
									</div>
									
								</div>
							</div>
						<?php } else { ?>
							<div class="text-center" style="padding: 30px 0"><?php echo $text_empty; ?></div>
						<?php } ?>
					</div>
				</div>	
			</div>	
			<input type="hidden" value="<?php echo $this->currency->getCode();?>" name="currency_code"  />	  
			<input type="hidden" value="<?php echo $this->currency->getValue();?>" name="currency_value"  />		
			<input type="hidden" value="<?php echo $this->currency->getId();?>" name="currency_id"  />		
			<input type="hidden" id="ip_store" value="<?php echo $ip_store; ?>" name="ip_store"/>
			<input type="hidden" id="store_url" value="<?php echo "http://$_SERVER[HTTP_HOST]"; ?>" name="store_url"/>
			<input type="hidden" id="quickorder_url" value="" name="url_site"  />
		<?php if ($products) { ?>	
			<?php if($on_off_fields_firstname == '1') { ?>
			  <div class="col-qsm-12 col-qmd-6 mar_tb">
				<div class="input-group-quick <?php echo $config_fields_firstname_requared == '1' ? 'sections_block_rquaired' : 'sections_block' ; ?>">             
					<input id="contact-name" type="text" placeholder="<?php echo isset($config_placeholder_fields_firstname[$lang_id]) ? $config_placeholder_fields_firstname[$lang_id]['config_placeholder_fields_firstname'] : ''; ?>" value="" class="contact-name input-quick" name="name_fastorder">
					<span class="input-icon"><i class="fa fa-user fa-fw"></i></span>
				</div>
			  </div>
			  <?php } ?>
			  <?php if($on_off_fields_phone == '1') { ?>
			  <div class="col-qsm-12 col-qmd-6 mar_tb">
				<div class="input-group-quick <?php echo $config_fields_phone_requared == '1' ? 'sections_block_rquaired' : 'sections_block' ; ?>">                    
					<input id="contact-phone" type="text" placeholder="<?php echo isset($config_placeholder_fields_phone[$lang_id]) ? $config_placeholder_fields_phone[$lang_id]['config_placeholder_fields_phone'] : ''; ?>" value="" class="contact-phone input-quick" name="phone">
					<span class="input-icon"><i class="icon-append_1 fa fa-phone-square fa-fw"></i></span>
				</div>
			  </div>
				<script src="catalog/view/javascript/jquery/maskedinput.js" type="text/javascript"></script>
				<script type="text/javascript">
					$(document).ready(function() {
						<?php if ($mask_phone_number != '') { ?>
						$("#contact-phone").mask("<?php echo $mask_phone_number;?>");
						<?php } ?>
					});
				</script>
				<?php } ?>
				<?php if($on_off_fields_email) { ?>
			  <div class="col-qsm-12 col-qmd-6 mar_tb">
				<div class="input-group-quick <?php echo $config_fields_email_requared == '1' ? 'sections_block_rquaired' : 'sections_block' ; ?>">                         
					<input id="contact-email" type="text" placeholder="<?php echo isset($config_placeholder_fields_email[$lang_id]) ? $config_placeholder_fields_email[$lang_id]['config_placeholder_fields_email'] : ''; ?>" value="" class="contact-email input-quick" name="email_buyer">
					<span class="input-icon"><i class="icon-append_1 fa fa-envelope fa-fw"></i></span>
				</div>
			  </div>
			  <?php } ?>
			  <?php if($on_off_fields_comment) { ?>
				<div class="col-qsm-12 col-qmd-6 mar_tb">
				<div class="input-group-quick <?php echo $config_fields_comment_requared == '1' ? 'sections_block_rquaired' : 'sections_block' ; ?>">                          
					<input id="contact-comment" name="comment_buyer" id="contact_comment_buyer" class="contact-comment-buyer input-quick" placeholder="<?php echo isset($config_placeholder_fields_comment[$lang_id]) ? $config_placeholder_fields_comment[$lang_id]['config_placeholder_fields_comment'] : ''; ?>"/>
					<span class="input-icon"><i class="icon-append_1 fa fa-comment fa-fw"></i></span>
				</div>
			  </div>
			  <?php } ?>
		<?php } ?>
		</form>
	</div>
	<div class="popup-footer">
	<style>
		button.btn-confirm-checkout {
			background-color:#<?php echo $background_button_send_fastorder;?> !important;
			border:1px solid #<?php echo $background_button_send_fastorder;?> !important;
		}
		button.btn-confirm-checkout:hover {
			background-color:#<?php echo $background_button_send_fastorder_hover;?> !important;
			border:1px solid #<?php echo $background_button_send_fastorder_hover;?> !important;
		}
	</style>
		<?php if ($products) { ?>
			<div id="quickorder_btn" class="form-group text-center">
				<div class="button-send-fastorder">
					<button type="button" onclick="quickorder_confirm_checkout();" class="btn-cart-add-order btn-confirm-checkout"><i class="<?php echo $icon_send_fastorder;?>"></i> <?php echo $button_send; ?></button>
				</div>
			</div>
		<?php } else { ?>
			<div id="quickorder_btn" class="form-group text-center">
				<div class="button-send-fastorder">
					<button type="button" onclick="$.magnificPopup.close();" class="btn-confirm-checkout"><?php echo $text_continue; ?></button>
				</div>
			</div>
		<?php } ?>
		<?php if ($config_any_text_at_the_bottom[$lang_id]['config_any_text_at_the_bottom'] != ''){?>
			<div class="col-qsm-12 mar_tb text-center"><span style="color:#<?php echo $any_text_at_the_bottom_color;?>"><?php echo $config_any_text_at_the_bottom[$lang_id]['config_any_text_at_the_bottom']; ?></span></div>
		<?php }?>
	</div>
<script type="text/javascript">
	function openTotal() {
		$('#total-quick-ckeckout').toggleClass('open');
	}
</script>
<script type="text/javascript">
function updateQuantity(prod_id){
$.ajax({
	data: $('#htop_fastorder'+prod_id).serialize(),
	url: 'index.php?route=module/newfastordercart/editCartQuick',
	type: 'post',
	beforeSend: function() {
		$('#quick_checkout_cart').addClass('maskPopupQuickorder');
		$('#fastorder_data').after('<span class="loading_quick_order"><i class="fa fa-circle-o-notch fa-spin fa-2x fa-fw"></i></span>');
	},
	success: function(data) {
		setTimeout(function () {
			$('#cart').load('index.php?route=module/cart #cart > *');
		}, 100);
		if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
			location = 'index.php?route=checkout/cart';
		} else {
			$.ajax({
				url: 'index.php?route=module/newfastordercart',
				type: 'get',
				dataType: 'html',
				success: function(data){
					$('#popup-quickorder #fastorder_data').html($(data).find('#fastorder_data').html());
					$('#popup-quickorder .popup-footer').html($(data).find('.popup-footer').html());
					$('.loading_quick_order').remove();
					$('#quick_checkout_cart').removeClass('maskPopupQuickorder');
				}
			});
		}
	},
	    error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	    }
});	
};
function deleteProduct(key){
$.ajax({
	url: 'index.php?route=module/newfastordercart/deleteProduct',
	type: 'post',
	data: 'key=' + key,
	dataType: 'json',
	beforeSend: function() {
		$('#quick_checkout_cart').addClass('maskPopupQuickorder');
		$('#fastorder_data').after('<span class="loading_quick_order"><i class="fa fa-circle-o-notch fa-spin fa-2x fa-fw"></i></span>');
	},
	success: function(json) {
		setTimeout(function () {
			$('#cart').load('index.php?route=module/cart #cart > *');
		}, 100);

		if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
			location = 'index.php?route=checkout/cart';
		} else {
			$.ajax({
				url: 'index.php?route=module/newfastordercart',
				type: 'get',
				dataType: 'html',
				success: function(data){
					$('#popup-quickorder #fastorder_data').html($(data).find('#fastorder_data').html());
					$('#popup-quickorder .popup-footer').html($(data).find('.popup-footer').html());
					$('.loading_quick_order').remove();
					$('#quick_checkout_cart').removeClass('maskPopupQuickorder');
				}
			});
		}
	},
	error: function(xhr, ajaxOptions, thrownError) {
	    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	}
});
};	
</script>	
</div>
