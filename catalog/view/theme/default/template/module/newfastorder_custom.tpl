<div id="popup-quickorder">
<style>
*::before, *::after {
  box-sizing: border-box;
   font-family: FontAwesome;
}
.mfp-preloader ::before, .mfp-preloader ::after {
   font-family: FontAwesome;
}
*:before, *:after {
  box-sizing: border-box;
}
* {
  box-sizing: border-box;
}
.price-new {
  color: red;
  font-weight: 600;
}
.price-old {
  text-decoration: line-through;
  color: #888;
}

</style>
<div class="popup-heading text-center
"><?php echo isset($config_title_popup_quickorder[$lang_id]) ? $config_title_popup_quickorder[$lang_id]['config_title_popup_quickorder'] : ''; ?></div>
<div class="popup-center">
	<form id="fastorder_data" enctype="multipart/form-data" method="post">
	<?php if($config_general_image_product_popup !='1') { ?>
		<div class="row">
		<div class="col-qsm-12 col-qmd-5">
			<?php if ($thumb) { ?><img src="<?php echo $thumb; ?>" class=" image-additional" alt="<?php echo $heading_title; ?>" /><?php } ?>
		</div>
		<div  class="col-qsm-12 col-qmd-7">
			<div class="row">
				<p class="col-qsm-12 mar_tb title-product">
					<?php echo $heading_title; ?>
				</p>
				<div class="col-qsm-12 mar_tb text-center">
					<div class="price-quantity-fastorder">
						<!-- <div class="row"> -->
						<?php if (!$special) { ?>
							<div class="price_fast col-qsm-6"><span id="formated_price_quick" price="<?php echo $price_value; ?>"><?php echo $price; ?></span></div>
							<input type="hidden" id="price_tax_plus_options" name="price_tax" value="<?php echo $price; ?>"/>
							<input type="hidden" id="price_no_tax_plus_options" name="price_no_tax" value="<?php echo $price_value; ?>"/>
						<?php } else { ?>
							<div class="special_fast">
							<div class=" col-qsm-6"><span id="formated_price_quick" class="price-old " price="<?php echo $price_value; ?>"><?php echo $price; /**/ ?></span></div>
							<div class="price-new col-qsm-6"><span id="formated_special_quick" price="<?php echo $special_value; ?>"><?php echo $special; /**/ ?></span></div>
							<input type="hidden" id="price_tax_plus_options" name="price_tax" value="<?php echo $special; ?>"/>
							<input type="hidden" id="price_no_tax_plus_options" name="price_no_tax" value="<?php echo $special_value; ?>"/>
							</div>
						<?php } ?>	
						<!-- </div>	 -->
						<div class="row">		
							<div class="quantity col-qsm-12 mar_tb ">	
								<input type="button" id="decrease" class="minus_quantity" value="-" onclick="btnminus('<?php echo $minimum; ?>');recalculateprice_quick();" />
								<input type="text" class="qty_fastorder" onchange="recalculateprice_quick(); "name="quantity" id="htop_fastorder" size="2" value="<?php echo $minimum; ?>" />
								<input type="button" id="increase" class="plus_quantity" value="+" onclick="btnplus();recalculateprice_quick();" />
								<?php if (!$special) { ?>
									<script type="text/javascript"><!--
										function btnminus(a){
											document.getElementById("htop_fastorder").value>a?document.getElementById("htop_fastorder").value--:document.getElementById("htop_fastorder").value=a;						
										}
										function btnplus(){
											document.getElementById("htop_fastorder").value++;	
										};
									//--></script>
								<?php } else {?>
									<script type="text/javascript"><!--
										function btnminus(a){									
											document.getElementById("htop_fastorder").value>a?document.getElementById("htop_fastorder").value--:document.getElementById("htop_fastorder").value=a;															
										}									
										function btnplus(){
											document.getElementById("htop_fastorder").value++;									
										};
								//--></script>
								<?php } ?>
							</div>
							</div>
							<?php if (!$special) { ?>
								<input id="total_form" type="hidden" value="<?php echo $price;?>" name="total_fast"/>												
							<?php } else { ?>						
								<input id="total_form" type="hidden" value="<?php echo $special;?>" name="total_fast"/>
							<?php } ?>
					</div>
				</div>
			</div>
		</div>
		</div>
		<?php } else { ?>
			<div class="col-qsm-12 ">
				<div class="well well-sm products" style="margin-top:10px;">
					<div class="product">
						<div class="row">
							<div class="col-qxs-12 col-qsm-5">
								<div class="image">
									<?php if ($thumb_small) { ?><img src="<?php echo $thumb_small; ?>" alt="<?php echo $heading_title; ?>" /><?php } ?>
								</div>
								<div class="pr-name quick-cell">
									<div class="quick-cell-content">
										<?php echo $heading_title; ?>
									</div>
								</div>
							</div>
							<div class="col-qxs-12 col-qsm-7">	
								<div class="col-qxs-6 quantity_quickorder quick-cell">
								<div class="quick-cell-content pquantity">
									<input type="button" id="decrease" class="btn-update-popup minus_quantity" value="-" onclick="btnminus('<?php echo $minimum; ?>');recalculateprice_quick();" />
									<input type="text" class="qty_fastorder" onchange="recalculateprice_quick(); "name="quantity" id="htop_fastorder" size="2" value="<?php echo $minimum; ?>" />
									<input type="button" id="increase" class="btn-update-popup plus_quantity" value="+" onclick="btnplus();recalculateprice_quick();" />
								</div>		
									<?php if (!$special) { ?>
										<script type="text/javascript"><!--
											function btnminus(a){
												document.getElementById("htop_fastorder").value>a?document.getElementById("htop_fastorder").value--:document.getElementById("htop_fastorder").value=a;						
											}
											function btnplus(){
												document.getElementById("htop_fastorder").value++;	
											};
										//--></script>
									<?php } else {?>
										<script type="text/javascript"><!--
											function btnminus(a){									
												document.getElementById("htop_fastorder").value>a?document.getElementById("htop_fastorder").value--:document.getElementById("htop_fastorder").value=a;															
											}									
											function btnplus(){
												document.getElementById("htop_fastorder").value++;									
											};
										//--></script>
									<?php } ?>
								</div>
								<div class="col-qxs-6 text-center quick-cell">
									<div class="quick-cell-content">
										<?php if (!$special) { ?>
											<div class="price_fast"><span id="formated_price_quick" price="<?php echo $price_value; ?>"><?php echo $price; ?></span></div>
											<input type="hidden" id="price_tax_plus_options" name="price_tax" value="<?php echo $price; ?>"/>
											<input type="hidden" id="price_no_tax_plus_options" name="price_no_tax" value="<?php echo $price_value; ?>"/>
										<?php } else { ?>
											<div class="special_fast">
											<div class="price-old"><span id="formated_price_quick" class="price-old" price="<?php echo $price_value; ?>"><?php echo $price; /**/ ?></span></div>
											<div class="price-new"><span id="formated_special_quick" price="<?php echo $special_value; ?>"><?php echo $special; /**/ ?></span></div>
											<input type="hidden" id="price_tax_plus_options" name="price_tax" value="<?php echo $special; ?>"/>
											<input type="hidden" id="price_no_tax_plus_options" name="price_no_tax" value="<?php echo $special_value; ?>"/>
											</div>
										<?php } ?>
									</div>
								</div>	
								<?php if (!$special) { ?>
									<input id="total_form" type="hidden" value="<?php echo $price;?>" name="total_fast"/>												
								<?php } else { ?>						
									<input id="total_form" type="hidden" value="<?php echo $special;?>" name="total_fast"/>
								<?php } ?>								
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
		<!-- <div class="row">
			<div class="col-qsm-12 mar_tb description">
				<p><?php echo $description_pr;?></p>
			</div>
		</div> -->


    <div class="option-fastorder">
	<?php if ($options) { ?>
    <div class="options mar_tb">
        <div class="text-option"><?php echo $text_option; ?></div>
        <br />
        <?php foreach ($options as $option) { ?>
        <div class="row">
        
        <?php if ($option['type'] == 'select') { ?>
	        <div id="option-fast-<?php echo $option['product_option_id']; ?>" class="option-quickorder col-qsm-12 select">
			
	          <?php if ($option['required']) { ?>
	          <span class="required">*</span>
	          <?php } ?>
	          
	          <select  name="option-fast[<?php echo $option['product_option_id']; ?>] ">
	            <option value=""><?php echo preg_replace_callback('/[a-zа-я]+(\s+[a-zа-я]*)*/iu',function ($matches)use ($option){return $matches[0].$option['name'];},$text_select); ?></option>
				<?php $opt_checked="checked"; ?>
	            <?php foreach ($option['option_value'] as $option_value) { ?>
	            <option value="<?php echo $option_value['product_option_value_id']; ?>" points="<?php echo (isset($option_value['points_value']) ? $option_value['points_value'] : 0); ?>" price_prefix="<?php echo $option_value['price_prefix']; ?>" price="<?php echo $option_value['price_value']; ?>"><?php echo $option_value['name']; ?>
	            <?php if ($option_value['price']) { ?>
	            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
	            <?php } ?>
	            </option>
	            <?php } ?>
	          </select>
	          <div class="select__arrow"></div>
	          <div class="error option-error-<?php echo $option['product_option_id']; ?>"></div>
	        </div>
        </div>
        <?php } ?>
		
        <?php if ($option['type'] == 'radio') { ?>
	        <div id="option-fast-<?php echo $option['product_option_id']; ?>" class="option-quickorder col-qsm-12">
			<div class="error option-error-<?php echo $option['product_option_id']; ?>"></div>
	          <?php if ($option['required']) { ?>
	          <span class="required">*</span>
	          <?php } ?>
	          <b><?php echo $option['name']; ?>:</b><br />
			  <?php $opt_checked="checked"; ?>
	          <?php foreach ($option['option_value'] as $option_value) { ?>
				<div class="radio-checbox-options">
						<label for="option-value-<?php echo $option['product_option_id']; ?>-<?php echo $option_value['product_option_value_id']; ?> "
						class="control control--radio">
							<span class="option-name"><?php echo $option_value['name']; ?></span>
		                    <?php if ($option_value['price']) { ?>
		                    <span class="option-price"><?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?></span>
		                    <?php } ?>
							 <input <?php echo (isset($opt_checked) ? $opt_checked : ''); $opt_checked=""; ?> type="radio" name="option-fast[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" points="<?php echo (isset($option_value['points_value']) ? $option_value['points_value'] : 0); ?>" price_prefix="<?php echo $option_value['price_prefix']; ?>" price="<?php echo $option_value['price_value']; ?>" id="option-value-<?php echo $option['product_option_id']; ?>-<?php echo $option_value['product_option_value_id']; ?>" />
							 <div class="control__indicator"></div>
		              </label>
	            </div>
	          <?php } ?>
	        </div>
        </div>
        <?php } ?>
		
        <?php if ($option['type'] == 'checkbox') { ?>
	        <div id="option-fast-<?php echo $option['product_option_id']; ?>" class="option-quickorder col-qsm-12">
			<div class="error option-error-<?php echo $option['product_option_id']; ?>"></div>
	          <?php if ($option['required']) { ?>
	          <span class="required">*</span>
	          <?php } ?>
	          <b><?php echo $option['name']; ?>:</b><br />
			  <?php $opt_checked="checked"; ?>
	          <?php foreach ($option['option_value'] as $option_value) { ?>
					<div class="radio-checbox-options">
	                  
						<label for="option-value-<?php echo $option['product_option_id']; ?>-<?php echo $option_value['product_option_value_id']; ?>" class="control control--checkbox">
						<input type="checkbox" name="option-fast[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" points="<?php echo (isset($option_value['points_value']) ? $option_value['points_value'] : 0); ?>" price_prefix="<?php echo $option_value['price_prefix']; ?>" price="<?php echo $option_value['price_value']; ?>" id="option-value-<?php echo $option['product_option_id']; ?>-<?php echo $option_value['product_option_value_id']; ?>" />
	                    <span class="option-name"><?php echo $option_value['name']; ?></span>
	                    <?php if ($option_value['price']) { ?>
	                    <span class="option-price"><?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?></span>
	                    <?php } ?>
	                    <div class="control__indicator"></div>
	                  </label>
	                </div>
	          <?php } ?>
	        </div>
        </div>
        <?php } ?>
		
        <?php if ($option['type'] == 'image') { ?>
	        <div id="option-fast-<?php echo $option['product_option_id']; ?>" class="option-quickorder col-qsm-12">
			<div class="error option-error-<?php echo $option['product_option_id']; ?>"></div>
	          <?php if ($option['required']) { ?>
	          <span class="required">*</span>
	          <?php } ?>
	          <b><?php echo $option['name']; ?>:</b><br />
	          <table class="option-image">
			  <?php $opt_checked="checked"; ?>
	            <?php foreach ($option['option_value'] as $option_value) { ?>
	            <div class="image-radio">
	                <label>
	                    <input <?php echo (isset($opt_checked) ? $opt_checked : ''); $opt_checked=""; ?> type="radio" name="option-fast[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" points="<?php echo (isset($option_value['points_value']) ? $option_value['points_value'] : 0); ?>" price_prefix="<?php echo $option_value['price_prefix']; ?>" price="<?php echo $option_value['price_value']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" />
	                    <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" data-toggle="tooltip" title="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" /> 
	                  </label>
	            </div>
	            <?php } ?>
	          </table>
        </div>
        </div>
        <?php } ?>
		
		<?php if ($option['type'] == 'text') { ?>
	        <div id="option-fast-<?php echo $option['product_option_id']; ?>" class="option-quickorder col-qsm-12">
			<div class="error option-error-<?php echo $option['product_option_id']; ?>"></div>
	          <?php if ($option['required']) { ?>
	          <span class="required">*</span>
	          <?php } ?>
	          <b><?php echo $option['name']; ?>:</b><br />
	          <input class="form-control-quickorder" type="text" name="option-fast[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" />
	        </div>
        </div>
        <?php } ?>
		
        <?php if ($option['type'] == 'textarea') { ?>
	        <div id="option-fast-<?php echo $option['product_option_id']; ?>" class="option-quickorder col-qsm-12">
			<div class="error option-error-<?php echo $option['product_option_id']; ?>"></div>
	          <?php if ($option['required']) { ?>
	          <span class="required">*</span>
	          <?php } ?>
	          <b><?php echo $option['name']; ?>:</b><br />
	          <textarea class="form-control-quickorder" name="option-fast[<?php echo $option['product_option_id']; ?>]" cols="40" rows="5"><?php echo $option['option_value']; ?></textarea>
	        </div>
        </div>
        <?php } ?>
		
		  <?php if ($option['type'] == 'file') { ?>
	        <div id="option-<?php echo $option['product_option_id']; ?>" class="option col-qsm-12">
	          <?php if ($option['required']) { ?>
	          <span class="required">*</span>
	          <?php } ?>
	          <b><?php echo $option['name']; ?>:</b><br />
	          <input type="button" value="<?php echo $button_upload; ?>" id="button-option-quick-<?php echo $option['product_option_id']; ?>" class="button">
	          <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" />
	        </div>
        </div>
        <br />
        <?php } ?>
		
        <?php if ($option['type'] == 'date') { ?>
	        <div id="option-fast-<?php echo $option['product_option_id']; ?>" class="option-quickorder col-qsm-12">
			<div class="error option-error-<?php echo $option['product_option_id']; ?>"></div>
	          <?php if ($option['required']) { ?>
	          <span class="required">*</span>
	          <?php } ?>
	          <b><?php echo $option['name']; ?>:</b><br />
	          <input class="form-control-quickorder date" type="text" name="option-fast[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" />
	        </div>
        </div>
        <?php } ?>
		
        <?php if ($option['type'] == 'datetime') { ?>
	        <div id="option-fast-<?php echo $option['product_option_id']; ?>" class="option-quickorder col-qsm-12">
			<div class="error option-error-<?php echo $option['product_option_id']; ?>"></div>
	          <?php if ($option['required']) { ?>
	          <span class="required">*</span>
	          <?php } ?>
	          <b><?php echo $option['name']; ?>:</b><br />
	          <input class="form-control-quickorder datetime" type="text" name="option-fast[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" />
	        </div>
        </div>
        <?php } ?>
		
        <?php if ($option['type'] == 'time') { ?>
	        <div id="option-fast-<?php echo $option['product_option_id']; ?>" class="option-quickorder col-qsm-12">
			<div class="error option-error-<?php echo $option['product_option_id']; ?>"></div>
	          <?php if ($option['required']) { ?>
	          <span class="required">*</span>
	          <?php } ?>
	          <b><?php echo $option['name']; ?>:</b><br />
	          <input class="form-control-quickorder time" type="text" name="option-fast[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" />
	        </div>
        </div>
        <?php } ?>
		
        <?php } ?>
      
      <?php } ?>
      	</div>




		<?php if($on_off_fields_firstname == '1') { ?>
			<div class="row">
				
			
			<div class="col-qsm-4">
				<label for="contact-name" class="form-label col-qsm-4 mar_tb"> <?php echo isset($config_placeholder_fields_firstname[$lang_id]) ? $config_placeholder_fields_firstname[$lang_id]['config_placeholder_fields_firstname'] : ''; ?></label>
			</div>
			<div class="col-qsm-8 mar_tb">
				<div class="input-group-quick <?php echo $config_fields_firstname_requared == '1' ? 'sections_block_rquaired' : 'sections_block' ; ?>">             
					<input id="contact-name" type="text" placeholder="<?php echo isset($config_placeholder_fields_firstname[$lang_id]) ? $config_placeholder_fields_firstname[$lang_id]['config_placeholder_fields_firstname'] : ''; ?>" value="" class="contact-name  input-quick" name="name_fastorder">
				</div>
			</div>
		</div>
		<?php } ?>
		<?php if($on_off_fields_phone == '1') { ?>
			<div class="row">
			<div class="col-qsm-4 mar_tb">
				<label for="contact-phone" class="form-label"><?php echo isset($config_placeholder_fields_phone[$lang_id]) ? $config_placeholder_fields_phone[$lang_id]['config_placeholder_fields_phone'] : ''; ?></label>
			</div>
		
			<div class="col-qsm-8 mar_tb">
				<div class="input-group-quick <?php echo $config_fields_phone_requared == '1' ? 'sections_block_rquaired' : 'sections_block' ; ?>">                    
					<input id="contact-phone" type="text" placeholder="<?php echo isset($config_placeholder_fields_phone[$lang_id]) ? $config_placeholder_fields_phone[$lang_id]['config_placeholder_fields_phone'] : ''; ?>" value="" class="contact-phone input-quick" name="phone">
				</div>
			</div>
			</div>
		<script src="catalog/view/javascript/jquery/maskedinput.js" type="text/javascript"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				<?php if ($mask_phone_number != '') { ?>
				$("#contact-phone").mask("<?php echo $mask_phone_number;?>");
				<?php } ?>
			});
		</script>
		<?php } ?>
		<?php if($on_off_fields_email) { ?>
			<div class="row">
			<div class="col-qsm-4 mar_tb" class="form-label">
				<label for="contact-email"><?php echo isset($config_placeholder_fields_email[$lang_id]) ? $config_placeholder_fields_email[$lang_id]['config_placeholder_fields_email'] : ''; ?></label>
			</div>
		
			<div class="col-qsm-8 mar_tb">
				<div class="input-group-quick <?php echo $config_fields_email_requared == '1' ? 'sections_block_rquaired' : 'sections_block' ; ?>">                         
					<input id="contact-email" type="text" placeholder="<?php echo isset($config_placeholder_fields_email[$lang_id]) ? $config_placeholder_fields_email[$lang_id]['config_placeholder_fields_email'] : ''; ?>" value="" class="contact-email input-quick" name="email_buyer">
				</div>
			</div>
			</div>
		<?php } ?>
		<?php if($on_off_fields_comment) { ?>
			<div class="row">
			<div class="col-qsm-4 mar_tb">
				<label for="contact-comment" class="form-label"><?php echo isset($config_placeholder_fields_comment[$lang_id]) ? $config_placeholder_fields_comment[$lang_id]['config_placeholder_fields_comment'] : ''; ?></label>
			</div>
		
			<div class="col-qsm-8 mar_tb">
				<div class="input-group-quick <?php echo $config_fields_comment_requared == '1' ? 'sections_block_rquaired' : 'sections_block' ; ?>">                          
					<input id="contact-comment" name="comment_buyer" id="contact_comment_buyer" class="contact-comment-buyer input-quick" placeholder="<?php echo isset($config_placeholder_fields_comment[$lang_id]) ? $config_placeholder_fields_comment[$lang_id]['config_placeholder_fields_comment'] : ''; ?>"/>
				</div>
			</div>
			</div>
		<?php } ?>
		<input type="hidden" id="product_url" value="" name="url_site"/>
		<input type="hidden" id="prod_id" value="<?php echo $product_id;?>" name="prod_id"  />	
		<!-- <div class="col-qsm-12 mar_tb text-center">		
			<?php echo $text_quick_order_enter_name_phone; ?>
		</div> -->

	</form>
	<div class="row">
		<div class="col-qsm-12 text-center" id="quickorder_btn">
		<button onclick="quickorder_confirm();" type="button" class="contact-send btn-confirm-checkout"><i class="<?php echo $icon_send_fastorder;?>"></i> <?php echo $button_send; ?></button>
	</div>
	</div>
</div>
<!-- <div class="popup-footer">
	
<style>
	button.btn-confirm-checkout {
		background-color:#<?php echo $background_button_send_fastorder;?> !important;
		border:1px solid #<?php echo $background_button_send_fastorder;?> !important;
	}
	button.btn-confirm-checkout:hover {
		background-color:#<?php echo $background_button_send_fastorder_hover;?> !important;
		border:1px solid #<?php echo $background_button_send_fastorder_hover;?> !important;
	}
</style>	
<?php if ($config_any_text_at_the_bottom[$lang_id]['config_any_text_at_the_bottom'] != ''){?>
	<div class="col-qsm-12 mar_tb text-center"><span style="color:#<?php echo $any_text_at_the_bottom_color;?>"><?php echo $config_any_text_at_the_bottom[$lang_id]['config_any_text_at_the_bottom']; ?></span></div>
<?php }?>
</div> -->
<script type="text/javascript"><!--
function price_format_quickorder(n)
{ 
    c = <?php echo (empty($currency['decimals']) ? "0" : $currency['decimals'] ); ?>;
    d = '<?php echo $currency['decimal_point']; ?>'; // decimal separator
    t = '<?php echo $currency['thousand_point']; ?>'; // thousands separator
    s_left = '<?php echo $currency['symbol_left']; ?>';
    s_right = '<?php echo $currency['symbol_right']; ?>';
      
    n = n * <?php echo $currency['value']; ?>;

    //sign = (n < 0) ? '-' : '';

    //extracting the absolute value of the integer part of the number and converting to string
    i = parseInt(n = Math.abs(n).toFixed(c)) + ''; 

    j = ((j = i.length) > 3) ? j % 3 : 0; 
    return s_left + (j ? i.substr(0, j) + t : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : '') + s_right; 
}

function calculate_tax_quickorder(price)
{
    <?php // Process Tax Rates
      if (isset($tax_rates) && $tax) {
         foreach ($tax_rates as $tax_rate) {
           if ($tax_rate['type'] == 'F') {
             echo 'price += '.$tax_rate['rate'].';';
           } elseif ($tax_rate['type'] == 'P') {
             echo 'price += (price * '.$tax_rate['rate'].') / 100.0;';
           }
         }
      }
    ?>
    return price;
}

function process_discounts_quickorder(price, quantity)
{
    <?php
      foreach ($dicounts_unf as $discount) {
        echo 'if ((quantity >= '.$discount['quantity'].') && ('.$discount['price'].' < price)) price = '.$discount['price'].';'."\n";
      }
    ?>
    return price;
}


animate_delay_quickorder = 20;

main_price_final_quickorder = calculate_tax_quickorder(Number($('#formated_price_quick').attr('price')));
main_price_start_quickorder = calculate_tax_quickorder(Number($('#formated_price_quick').attr('price')));
main_step_quickorder = 0;
main_timeout_id_quickorder = 0;

function animateMainPrice_callback_quick() {
    main_price_start_quickorder += main_step_quickorder;
    
    if ((main_step_quickorder > 0) && (main_price_start_quickorder > main_price_final_quickorder)){
        main_price_start_quickorder = main_price_final_quickorder;
    } else if ((main_step_quickorder < 0) && (main_price_start_quickorder < main_price_final_quickorder)) {
        main_price_start_quickorder = main_price_final_quickorder;
    } else if (main_step_quickorder == 0) {
        main_price_start_quickorder = main_price_final_quickorder;
    }
    
    $('#formated_price_quick').html( price_format_quickorder(main_price_start_quickorder) );
    $('#total').html( price_format_quickorder(main_price_start_quickorder) );
    $('#total_form').val(main_price_start_quickorder);
   
    
    if (main_price_start_quickorder != main_price_final_quickorder) {
        main_timeout_id_quickorder = setTimeout(animateMainPrice_callback_quick, animate_delay_quickorder);
    }
}

function animateMainPrice_quick(price) {
    main_price_start_quickorder = main_price_final_quickorder;
    main_price_final_quickorder = price;
    main_step_quickorder = (main_price_final_quickorder - main_price_start_quickorder) / 10;
    
    clearTimeout(main_timeout_id_quickorder);
    main_timeout_id_quickorder = setTimeout(animateMainPrice_callback_quick, animate_delay_quickorder);
}


<?php if ($special) { ?>
special_price_final_quickorder = calculate_tax_quickorder(Number($('#formated_special_quick').attr('price')));
special_price_start_quickorder = calculate_tax_quickorder(Number($('#formated_special_quick').attr('price')));
special_step_quickorder = 0;
special_timeout_id_quickorder = 0;

function animateSpecialPrice_callback_quick() {
    special_price_start_quickorder += special_step_quickorder;
    
    if ((special_step_quickorder > 0) && (special_price_start_quickorder > special_price_final_quickorder)){
        special_price_start_quickorder = special_price_final_quickorder;
    } else if ((special_step_quickorder < 0) && (special_price_start_quickorder < special_price_final_quickorder)) {
        special_price_start_quickorder = special_price_final_quickorder;
    } else if (special_step_quickorder == 0) {
        special_price_start_quickorder = special_price_final_quickorder;
    }
    $('#formated_special_quick').html( price_format_quickorder(special_price_start_quickorder) );
    $('#total').html( price_format_quickorder(special_price_start_quickorder) );
    $('#total_form').val(special_price_start_quickorder);
	
   
    
    if (special_price_start_quickorder != special_price_final_quickorder) {
        special_timeout_id_quickorder = setTimeout(animateSpecialPrice_callback_quick, animate_delay_quickorder);
    }
}

function animateSpecialPrice_quick(price) {
    special_price_start_quickorder = special_price_final_quickorder;
    special_price_final_quickorder = price;
    special_step_quickorder = (special_price_final_quickorder - special_price_start_quickorder) / 10;
    
    clearTimeout(special_timeout_id_quickorder);
    special_timeout_id_quickorder = setTimeout(animateSpecialPrice_callback_quick, animate_delay_quickorder);
}
<?php } ?>


function recalculateprice_quick()
{
    var main_price = Number($('#formated_price_quick').attr('price'));
    var input_quantity = Number($('input#htop_fastorder[name="quantity"]').attr('value'));
    var special = Number($('#formated_special_quick').attr('price'));
	
    var tax = 0;
	
	<?php if ($special) { ?>
        total_price = special;
    <?php } else { ?>
        total_price = main_price;
    <?php } ?>
	<?php if($min_price_fastorder !='') { ?>
    var min_price = <?php echo $min_price_fastorder;?>;
	<?php } else { ?>
	var min_price = '0';
	<?php } ?>
	if (min_price > total_price){
		$('.button-send-fastorder').addClass('dispnone');
		
	} else {
		$('.button-send-fastorder').removeClass('dispnone');
		
	}
	
	
	
    if (isNaN(input_quantity)) input_quantity = 0;
    
    // Process Discounts.
    <?php if ($special) { ?>
        special = process_discounts_quickorder(special, input_quantity);
    <?php } else { ?>
        main_price = process_discounts_quickorder(main_price, input_quantity);
    <?php } ?>
    tax = process_discounts_quickorder(tax, input_quantity);
    
    
   <?php if ($points) { ?>
     var points = Number($('#formated_points').attr('points'));
     $('.option-quickorder input:checked').each(function() {
       points += Number($(this).attr('points'));
     });
     $('.option-quickorder option:selected').each(function() {
       points += Number($(this).attr('points'));
     });
     $('#formated_points').html(points);
   <?php } ?>
    
    var option_price = 0;
    
    $('.option-quickorder input:checked,option:selected').each(function() {
      if ($(this).attr('price_prefix') == '=') {
        option_price += Number($(this).attr('price'));
        main_price = 0;
        special = 0;
      }
    });
    
    $('.option-quickorder input:checked,option:selected').each(function() {
      if ($(this).attr('price_prefix') == '+') {
        option_price += Number($(this).attr('price'));
      }
      if ($(this).attr('price_prefix') == '-') {
        option_price -= Number($(this).attr('price'));
      }
      if ($(this).attr('price_prefix') == 'u') {
        pcnt = 1.0 + (Number($(this).attr('price')) / 100.0);
        option_price *= pcnt;
        main_price *= pcnt;
        special *= pcnt;
      }
      if ($(this).attr('price_prefix') == '*') {
        option_price *= Number($(this).attr('price'));
        main_price *= Number($(this).attr('price'));
        special *= Number($(this).attr('price'));
      }
    });
    special += option_price;
    main_price += option_price;
	 
	<?php if ($special) { ?>		
		$('#price_no_tax_plus_options').val(special);
    <?php } else { ?>		
		$('#price_no_tax_plus_options').val(main_price);
    <?php } ?>
	
    <?php if ($special) { ?>
      tax = special;
    <?php } else { ?>
      tax = main_price;
    <?php } ?>
    
    // Process TAX.
    main_price = calculate_tax_quickorder(main_price);
    special = calculate_tax_quickorder(special);
	<?php if ($special) { ?>
		$('#price_tax_plus_options').val(special);
    <?php } else { ?>
		$('#price_tax_plus_options').val(main_price);
    <?php } ?>
    // Раскомментировать, если нужен вывод цены с умножением на количество
	
    // Раскомментировать, если нужен вывод цены с умножением на количество
    main_price *= input_quantity;	
    special *= input_quantity;

	<?php if ($special) { ?>
      var total_price =  special;
    <?php } else { ?>
     var total_price =  main_price;
    <?php } ?>
	<?php if($min_price_fastorder !='') { ?>
    var min_price = <?php echo $min_price_fastorder;?>;
	<?php } else { ?>
	var min_price = '0';
	<?php } ?>
	if (min_price > total_price){
		$('.button-send-fastorder').addClass('dispnone');
		$('.button-send-fastorder').hide();		
		$('.min_price_message').show();	
	} else {
		$('.button-send-fastorder').removeClass('dispnone');
		$('.min_price_message').hide();			
		$('.button-send-fastorder').show();	
	}

    animateMainPrice_quick(main_price);
      
    <?php if ($special) { ?>
      //$('#formated_special').html( price_format_quickorder(special) );
      animateSpecialPrice_quick(special);
    <?php } ?>

    <?php if ($tax) { ?>
      $('#formated_tax').html( price_format_quickorder(tax) );
    <?php } ?>
}

$(document).ready(function() {
    $('.option-quickorder input[type="checkbox"]').bind('change', function() { recalculateprice_quick(); });
    $('.option-quickorder input[type="radio"]').bind('change', function() { recalculateprice_quick(); });
    $('.option-quickorder select').bind('change', function() { recalculateprice_quick(); });
    
    $quantity_quickorder = $('input#htop_fastorder[name="quantity"]');
    $quantity_quickorder.data('val', $quantity_quickorder.val());
    (function() {
        if ($quantity_quickorder.val() != $quantity_quickorder.data('val')){
            $quantity_quickorder.data('val',$quantity_quickorder.val());
            recalculateprice_quick();
        }
        setTimeout(arguments.callee, 250);
    })();    
    
    recalculateprice_quick();
});

//--></script>
<script type="text/javascript"><!--
$(document).ready(function() {
	if ($.browser.msie && $.browser.version == 6) {
		$('.date, .datetime, .time').bgIframe();
	}

	$('.date').datepicker({dateFormat: 'yy-mm-dd'});
	$('.datetime').datetimepicker({
		dateFormat: 'yy-mm-dd',
		timeFormat: 'h:m'
	});
	$('.time').timepicker({timeFormat: 'h:m'});
});


//--></script>

<?php if ($options) { ?>
<script type="text/javascript" src="catalog/view/javascript/jquery/ajaxupload.js"></script>
<?php foreach ($options as $option) { ?>
<?php if ($option['type'] == 'file') { ?>
<script type="text/javascript">
new AjaxUpload('#button-option-quick-<?php echo $option['product_option_id']; ?>', {
	action: 'index.php?route=product/product/upload',
	name: 'file',
	autoSubmit: true,
	responseType: 'json',
	onSubmit: function(file, extension) {
		$('#button-option-quick-<?php echo $option['product_option_id']; ?>').after('<img src="catalog/view/theme/default/image/loading.gif" class="loading" style="padding-left: 5px;" />');
		$('#button-option-quick-<?php echo $option['product_option_id']; ?>').attr('disabled', true);
	},
	onComplete: function(file, json) {
		$('#button-option-quick-<?php echo $option['product_option_id']; ?>').attr('disabled', false);
		
		$('.error').remove();
		
		if (json['success']) {
			alert(json['success']);
			
			$('input[name=\'option[<?php echo $option['product_option_id']; ?>]\']').attr('value', json['file']);
		}
		
		if (json['error']) {
			$('#option-<?php echo $option['product_option_id']; ?>').after('<span class="error">' + json['error'] + '</span>');
		}
		
		$('.loading').remove();	
	}
});
</script>
<?php } ?>
<?php } ?>
<?php } ?>		
</div>
