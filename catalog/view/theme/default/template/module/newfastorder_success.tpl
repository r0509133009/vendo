<style>
	*::before, *::after {
	  box-sizing: border-box;
	   font-family: FontAwesome;
	}
	.checkmark {
   display: inline-block;
    width: 104px;
    height: 104px;
    -ms-transform: rotate(45deg);
    -webkit-transform: rotate(45deg);
    transform: rotate(45deg);
}

.checkmark_circle {
    position: absolute;
    width: 104px;
    height: 104px;
    background-color: #7ebb32;
    border-radius: 52px;
    border: 1px solid #009933;
    left: 0px;
    top: 0;
}

.checkmark_stem {
    position: absolute;
    width: 16px;
    height: 60px;
    background-color: #fff;
    left: 52px;
    top: 18px;
    box-sizing: border-box;
    border: 1px solid #009933;
    background-color: white;
}

.checkmark_kick {
    position: absolute;
    width: 20px;
    height: 16px;
    background-color: #fff;
    left: 33px;
    top: 62px;
    border-top: 1px solid #009933;
    border-bottom: 1px solid #009933;
    border-left: 1px solid #009933;
    box-sizing: border-box;
}
</style>
<div class="col-qsm-12">
	

	<div class="row text-center succes-heading mar_tb">
		<div class="col-qsm-12">
			<?php echo isset($config_title_popup_quickorder[$lang_id]) ? $config_title_popup_quickorder[$lang_id]['config_title_success'] : ''; ?>
		</div>
	</div>
	
	<div class="row mar_tb">
		<div class="col-qsm-12 check">
		<span class="checkmark">
		    <div class="checkmark_circle"></div>
		    <div class="checkmark_stem"></div>
		    <div class="checkmark_kick"></div>
		</span>
		<!-- <i class="fa fa-check-circle fa-fw"></i> -->
			<!-- <span class="fa-stack fa-lg">
				  <i class="fa fa-circle-thin fa-2x fa-stack-2x"></i>
				  <i class="fa fa-check-circle fa-2x fa-stack-1x"></i>
			</span> -->
		</div>
	</div>

	<div class="row mar_tb">
		<p class="col-qsm-12 succes-text text-center"><?php echo $text_success; ?></p>
	</div>

	<div class="row mar_tb">
		<div class="col-qsm-12 text-center" id="quickorder_btn">
		<button onclick="$.magnificPopup.close();" type="button" class="contact-send btn-confirm-checkout"><i class="<?php echo $icon_send_fastorder;?>"></i> <?php echo $button_continue; ?></button>
		</div>
	</div>
</div>	
