<style>
#popuploggin {
	width: 700px;
    max-width: 70%;
    margin: auto auto;
    position: relative;
    background-color: #fff;
    padding-top:35px;
    padding-left:45px;
    padding-right: 45px;
    padding-bottom: 5px;
    color: #434242;
}
#popuploggin .box {
    background-color: #e6e7e9;
    border-radius: 8px;
    padding: 10px;
    overflow: hidden;
    margin-bottom: 30px;
}
#popuploggin .box img {
	height: 60px;
	width: 60px;
}
#loggin-logo {
	position: absolute;
    top: -75px;
    left: 300px;
}
#loggin-logo img {
	width: 200px;
}
#popuploggin .box .pp-left {
	float: left;
	width: 20%;
    text-align: center;
}
#popuploggin .box .pp-right {
	float: right;
	width: 75%;
	font-family: "Roboto Condensed";
	font-size:14px;
}
#popuploggin .box p {
	font-size:11px;
	line-height: 11px;
}
h2.popuploggin-title {
    font-size: 28px;
    font-family: "Roboto Condensed";
	text-align: center;
	margin-top: 20px;
	margin-bottom: 40px;
	position: relative;
}
/* facebook button */
#popuploggin a.fbkRoundedBtn {
    display: inline-block;
    text-decoration: none;
    font-family: "Roboto Condensed";
    font-weight: bold;
    font-size: 14px;
    color: #fff;
    height: 40px!important;
    width: 100%;
    line-height: 40px;
    text-align: center;
    padding: 0px;
    background-color: #2e4cac;
    background-image: none;
    border: 1px solid #264076;
    position: relative;
    box-sizing: border-box;
    border-radius: 0px;
}
#popuploggin a.fbkRoundedBtn span {
    width: 30px;
    height: 30px;
    padding: 0;
    margin: 0;
    left: 0;
    top: 5px;
    position: absolute;
    background: url(catalog/view/theme/journal2/image/facebooklogin/fbkIconRoundedBtn.png) no-repeat left top;
}
#popuploggin .facebookButton {
    position: relative;
}
.popuploggin-button {
    font-size: 19px;
    font-weight: bold;
    text-align: center;
    display: block;
    height: 45px;
    width: 100%;
    line-height: 45px;
    box-sizing: border-box;
    margin: 15px 0px;
}
.popuploggin-button.loggin {
	background-color: #7ebb32;
	color: #fff;
}
.popuploggin-button.register {
	background-color: #fff;
	color: #00aeef;
	border: 2px solid #00aeef;
	line-height: 40px;

}
.popuploggin-or {
	text-align: center;
	font-weight: bold;
	margin-bottom: 15px;
}
.popuploggin-or:after {
	content: "";
	height: 2px;
	width: 38%;
	bottom: 4px;
	background-color: #3d3d3d;
	margin-left: 15px;
}
.popuploggin-or:before {
	content: "";
	height: 2px;
	width: 38%;
	bottom: 4px;
	background-color: #3d3d3d;
	margin-right: 15px;
}
.popuplogin-arrow {
	cursor: pointer;
	position: absolute;
	left: 2px;
}
.popuplogin-arrow:before {
    content: '\e610';
    font-size: 30px;
    float: left;
    top: 2px;
}
/* tabs */
#popuploggin .tab-content-login > .tab-pane-login {
	display: none;
}
#popuploggin .tab-content-login > .active {
	display: block;
}
.popuplogin-form {
	margin-bottom: 10px;
    text-align: right;
}
.popuplogin-form input {
	box-sizing: border-box;
	background-color: #fff;
	border: 2px solid #6acff5;
    -webkit-box-shadow: inset 0 0 0 50px #fff;
    -webkit-text-fill-color: #57585a;
    width: 80%;
    height: 37px;
    margin-left: 10px;
	::-webkit-input-placeholder {color:#dcdcdc;}
	::-moz-placeholder {color:#dcdcdc;}
	:-moz-placeholder {color:#dcdcdc;}
	:-ms-input-placeholder {color:#dcdcdc;}

}
.popuplogin-form input:-webkit-autofill {
	background-color: #fff!important;
}
.popuplogin-form-text {
	text-align: center;
	margin-bottom: 25px;
}
.popuplogin-form-text a {
	font-size: 14px;
	font-weight: 700;
	font-style: italic;
	text-decoration: underline;
}
.popuplogin-form-text a:hover {
	color: #000;
}
.popuplogin-alert {
	display: none;
    font-size: 11px;
    color: #434242;
    box-sizing: border-box;
    padding: 10px;
    margin-bottom: 17px;
    border: 1px solid transparent;
    border-radius: 3px;
}
.popuplogin-alert .success {
    background-color: #dff0d8;
    border-color: #d6e9c6;
}
.popuplogin-alert .warning {
	background-color: #fef1f1;
    border-color: #fde2e2;
}

</style>
<div id="popuploggin">
	<div id="popuploggin-box">
		<div id="loggin-logo">
		    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAS0AAABVCAYAAAAc5bL2AAAKN2lDQ1BzUkdCIElFQzYxOTY2LTIuMQAAeJydlndUU9kWh8+9N71QkhCKlNBraFICSA29SJEuKjEJEErAkAAiNkRUcERRkaYIMijggKNDkbEiioUBUbHrBBlE1HFwFBuWSWStGd+8ee/Nm98f935rn73P3Wfvfda6AJD8gwXCTFgJgAyhWBTh58WIjYtnYAcBDPAAA2wA4HCzs0IW+EYCmQJ82IxsmRP4F726DiD5+yrTP4zBAP+flLlZIjEAUJiM5/L42VwZF8k4PVecJbdPyZi2NE3OMErOIlmCMlaTc/IsW3z2mWUPOfMyhDwZy3PO4mXw5Nwn4405Er6MkWAZF+cI+LkyviZjg3RJhkDGb+SxGXxONgAoktwu5nNTZGwtY5IoMoIt43kA4EjJX/DSL1jMzxPLD8XOzFouEiSniBkmXFOGjZMTi+HPz03ni8XMMA43jSPiMdiZGVkc4XIAZs/8WRR5bRmyIjvYODk4MG0tbb4o1H9d/JuS93aWXoR/7hlEH/jD9ld+mQ0AsKZltdn6h21pFQBd6wFQu/2HzWAvAIqyvnUOfXEeunxeUsTiLGcrq9zcXEsBn2spL+jv+p8Of0NffM9Svt3v5WF485M4knQxQ143bmZ6pkTEyM7icPkM5p+H+B8H/nUeFhH8JL6IL5RFRMumTCBMlrVbyBOIBZlChkD4n5r4D8P+pNm5lona+BHQllgCpSEaQH4eACgqESAJe2Qr0O99C8ZHA/nNi9GZmJ37z4L+fVe4TP7IFiR/jmNHRDK4ElHO7Jr8WgI0IABFQAPqQBvoAxPABLbAEbgAD+ADAkEoiARxYDHgghSQAUQgFxSAtaAYlIKtYCeoBnWgETSDNnAYdIFj4DQ4By6By2AE3AFSMA6egCnwCsxAEISFyBAVUod0IEPIHLKFWJAb5AMFQxFQHJQIJUNCSAIVQOugUqgcqobqoWboW+godBq6AA1Dt6BRaBL6FXoHIzAJpsFasBFsBbNgTzgIjoQXwcnwMjgfLoK3wJVwA3wQ7oRPw5fgEVgKP4GnEYAQETqiizARFsJGQpF4JAkRIauQEqQCaUDakB6kH7mKSJGnyFsUBkVFMVBMlAvKHxWF4qKWoVahNqOqUQdQnag+1FXUKGoK9RFNRmuizdHO6AB0LDoZnYsuRlegm9Ad6LPoEfQ4+hUGg6FjjDGOGH9MHCYVswKzGbMb0445hRnGjGGmsVisOtYc64oNxXKwYmwxtgp7EHsSewU7jn2DI+J0cLY4X1w8TogrxFXgWnAncFdwE7gZvBLeEO+MD8Xz8MvxZfhGfA9+CD+OnyEoE4wJroRIQiphLaGS0EY4S7hLeEEkEvWITsRwooC4hlhJPEQ8TxwlviVRSGYkNimBJCFtIe0nnSLdIr0gk8lGZA9yPFlM3kJuJp8h3ye/UaAqWCoEKPAUVivUKHQqXFF4pohXNFT0VFysmK9YoXhEcUjxqRJeyUiJrcRRWqVUo3RU6YbStDJV2UY5VDlDebNyi/IF5UcULMWI4kPhUYoo+yhnKGNUhKpPZVO51HXURupZ6jgNQzOmBdBSaaW0b2iDtCkVioqdSrRKnkqNynEVKR2hG9ED6On0Mvph+nX6O1UtVU9Vvuom1TbVK6qv1eaoeajx1UrU2tVG1N6pM9R91NPUt6l3qd/TQGmYaYRr5Grs0Tir8XQObY7LHO6ckjmH59zWhDXNNCM0V2ju0xzQnNbS1vLTytKq0jqj9VSbru2hnaq9Q/uE9qQOVcdNR6CzQ+ekzmOGCsOTkc6oZPQxpnQ1df11Jbr1uoO6M3rGelF6hXrtevf0Cfos/ST9Hfq9+lMGOgYhBgUGrQa3DfGGLMMUw12G/YavjYyNYow2GHUZPTJWMw4wzjduNb5rQjZxN1lm0mByzRRjyjJNM91tetkMNrM3SzGrMRsyh80dzAXmu82HLdAWThZCiwaLG0wS05OZw2xljlrSLYMtCy27LJ9ZGVjFW22z6rf6aG1vnW7daH3HhmITaFNo02Pzq62ZLde2xvbaXPJc37mr53bPfW5nbse322N3055qH2K/wb7X/oODo4PIoc1h0tHAMdGx1vEGi8YKY21mnXdCO3k5rXY65vTW2cFZ7HzY+RcXpkuaS4vLo3nG8/jzGueNueq5clzrXaVuDLdEt71uUnddd457g/sDD30PnkeTx4SnqWeq50HPZ17WXiKvDq/XbGf2SvYpb8Tbz7vEe9CH4hPlU+1z31fPN9m31XfKz95vhd8pf7R/kP82/xsBWgHcgOaAqUDHwJWBfUGkoAVB1UEPgs2CRcE9IXBIYMj2kLvzDecL53eFgtCA0O2h98KMw5aFfR+OCQ8Lrwl/GGETURDRv4C6YMmClgWvIr0iyyLvRJlESaJ6oxWjE6Kbo1/HeMeUx0hjrWJXxl6K04gTxHXHY+Oj45vipxf6LNy5cDzBPqE44foi40V5iy4s1licvvj4EsUlnCVHEtGJMYktie85oZwGzvTSgKW1S6e4bO4u7hOeB28Hb5Lvyi/nTyS5JpUnPUp2Td6ePJninlKR8lTAFlQLnqf6p9alvk4LTduf9ik9Jr09A5eRmHFUSBGmCfsytTPzMoezzLOKs6TLnJftXDYlChI1ZUPZi7K7xTTZz9SAxESyXjKa45ZTk/MmNzr3SJ5ynjBvYLnZ8k3LJ/J9879egVrBXdFboFuwtmB0pefK+lXQqqWrelfrry5aPb7Gb82BtYS1aWt/KLQuLC98uS5mXU+RVtGaorH1futbixWKRcU3NrhsqNuI2ijYOLhp7qaqTR9LeCUXS61LK0rfb+ZuvviVzVeVX33akrRlsMyhbM9WzFbh1uvb3LcdKFcuzy8f2x6yvXMHY0fJjpc7l+y8UGFXUbeLsEuyS1oZXNldZVC1tep9dUr1SI1XTXutZu2m2te7ebuv7PHY01anVVda926vYO/Ner/6zgajhop9mH05+x42Rjf2f836urlJo6m06cN+4X7pgYgDfc2Ozc0tmi1lrXCrpHXyYMLBy994f9Pdxmyrb6e3lx4ChySHHn+b+O31w0GHe4+wjrR9Z/hdbQe1o6QT6lzeOdWV0iXtjusePhp4tLfHpafje8vv9x/TPVZzXOV42QnCiaITn07mn5w+lXXq6enk02O9S3rvnIk9c60vvG/wbNDZ8+d8z53p9+w/ed71/LELzheOXmRd7LrkcKlzwH6g4wf7HzoGHQY7hxyHui87Xe4Znjd84or7ldNXva+euxZw7dLI/JHh61HXb95IuCG9ybv56Fb6ree3c27P3FlzF3235J7SvYr7mvcbfjT9sV3qID0+6j068GDBgztj3LEnP2X/9H686CH5YcWEzkTzI9tHxyZ9Jy8/Xvh4/EnWk5mnxT8r/1z7zOTZd794/DIwFTs1/lz0/NOvm1+ov9j/0u5l73TY9P1XGa9mXpe8UX9z4C3rbf+7mHcTM7nvse8rP5h+6PkY9PHup4xPn34D94Tz+49wZioAAAAJcEhZcwAACxIAAAsSAdLdfvwAABuJSURBVHic7Z0L/GZTucfXdKhDnTo6NIqYjlsup9yLQ/0THUIoCrmM2zHkPi45MYYaBjGhhJEZ5DYY12PkOiYZipARTtJUDpqUU5ET6d/vO3u9f+/sed99fdZ+9/uf/ft81mfPf969n/Wstdd+1lrPei6LDQ4OugYNGoSHvrVVdNlL5ZMqq6q8XeU1lV+pzFS5aMSIEQ/2jME+wWK9ZqBBg+EOCat36HK6yn4q/xD7eXGV1X05QPdep+sYCa951XLZP2iEVoMGASEh9D5dblX5t4yP7KCynp77DwmuJ8Nx1r9ohFaDBoEgwcP2b4bLLrBaWEHlNj2/gQTXb+w56280QqtBg3CYqPKhgs++X+U8F628GrShEVoNGgSAVkkr6zKmJJntRWdjrbbus+BpuKARWg0ahMH+zub7QvA1QqsNjdBq0CAMtjWis6URnWGDRmg1aGAMr4Bf1YjcMqI3slHIv4lGaDVoYI9lVUYY0nu3SiO0PBqh1aCBPd5qTO8txvT6Go3QatCgQV+hEVoNGjToKzRCq0GDBn2FRmg1aNCgr9AIrQYNGvQVGqHVoEGDvkIjtBo0aNBXSBRag4ODH3CRl/naKu9UeV7lByrTR4wY8efw7C3AC7xu5yK3huVVXlF5TOU68fKTKnnx/LxHl/VVcIzFIx+DwiVdFI3ydRX65/cqv1aZq/KIypPi9W9V82oNtf0fdXmbyhsqfx4ObWrQP+gotLywIqzG59zCkRZx4DxD9xytwXpxYP5a/Gyoy1QXRXdsB/yN1+936HqM+PlxQB6W0GULla1VPqUyqgCZP4rOXS4KCofg/60dh8lQvSvp8mGVZVwkUJ9QeUQ8/DXhmffqMqCygYtiQiGgR6os0XbbG7rvRV2fUXlchXcwS3QfD9CM4FBbmHjWUqG/mIyWUkFIY+H+qsofVJ5T+bnK42rn//WI1aBQPzBOGC+tSZm/mZCJtMpkRV/w3ukL3j0Lh1+qP4LHb19IaInZXXSZ7BnsBlYZU3XvuroeFpJRIjjqcr2LBk43bK7ygO49Trycalw/HyuCeleVfy5JjtXq9r6cLdo36nqWeL63JN2OEH0sqXdXOcJ1jus0T/ec53n4vX9maf8M44CVZJo7CpPaSF82aqt7ri7TVKbUOQKn76ONXeTgzDiin7KqTfT44M90/b4KE+cMtfUPQRgNDLWDsc2EzE5mE1dsUn5RdGbrepvKTeqLX9px+CYWeDmqkHAa33bZ/aYOUXlJZbwtW0P8rKHL1S5ZYLVAWybqmWXVWYcb1M0He7yLBrOlH1kLuHrsSFFdDPojxfcPrYj77euVKp9IuI17xqmM0f3/5aLBirB6mwELo1SOVjlKtG/XdaLad7cBXRPghKzLAS5KNLFCQTKMi1V92UflL6J7i4uEWO0hXplwPq2yt8pWrvx7Z8Lb1hcmZVRJ7Mau1Lt/uSTtIQwJLVXA4P6Wy/+BHs/2zHq14Dv0EpV/yvnoYcx+4ufcgvUymE9WGe2q8/naVOV+1X2RrmPLztZeYM1SWS3jI9x/YZk6E8B4Yjv9KfH1PRetzHu28hIPOB9/xUUCa4mU2/OCj34HV/Noo+oDtnijXTSprByoGt77Jr6cqjr5Hie1VvRlMF9oiSCdzaCN66+ygA8bqbqe8TZxT5X1Cj57mvi5Wfz8Ks9DemZnFwnudxestwx4yczWm4uPL4r3HxQh4rc7V7nsAqtKsNV/VDxO0PXkJH2aNVQn/buvi3S1vXi/tYD64fMu6oMPVFgt/X2cysGqn7oRXn8pSqy10hqt8q8lmFrHRQPy1hI0huA/vC+XIIE+7lgXzaZZ6mPGRbezR4k6rbCiyl3i6WC92AsKPE8bBmxZMgXb4hNVtlAbv6A2Phe6Qr/yZJuyyAbUUx+M0uV8F616e4V3qZyisqf42bfoxNwutMoC3YCJ0BI2U1mlJI1d1TGHq2P+P+km3bOcLje5SPDWBXzY58Ob+D8h60N+NVFG2FcJtg0/Es/bBj71ZbXOQc7yoeqoO1i568L27J295sXjgyr3iC8OzU7Iu+JeTA/SkA0MGNmKbWaZZV8bRhvQoF2YSszqdoPP+MtJxyiD+kJgnO/TrIKI09w6bgu7gZyAd6uNW4c4QRVdlMsc5CSdhA9beNvGSSoH9ZqXDkAVxeHPxuJzJ73/F7M+SKNWc8V0WXGgMCfd9y1liHghaqXI5PSxo9Dy2VL4bVmjukLhGPH6UkZTjk2Cc2MP3vcMtXFztfEBK6IIQl2mO/uAfH0Bn9X6GhepbeqMAZXZ4ndLvf+fZ3kAoVXW9qgdCJtSQktAUbikAS/gXZ3+028JOYavu8Bq4WTx/JRe6vUp961UCTf24AO7WW38aNaBmwTR4TSWD3ZRFViMe5LEbpR2b03AAuL7fuL6adrNCK1EnU9ObIsSvaRbx55m3ESW3wvAK935+EcZ1hMaHExcgjGv+vbphPveURVDAYCNzw1q40fUxleKEtHzHCilGSMPW3iL/ptd/wisFvC+uIMJJ23iQmj9wrBibJywLi6kn/A6pn835KdT4zklXN+wjqrA9vsKn7zz9S73hDCCrRJrqpztItOP3PCmO9e6RdSkwds2Xu76U00AEFwz/BjvquNaTD8+q5tw6n2/UcW4qBRVqnJcb/XhcSIxu/0/1M5dXT3MGooCYYtB4IReMxIQe+s94QR/c4FnOU5f25qhPgLjYrteM1ESLFyu1hjYotupYsvkAYXloUaVIrSOzPuQt82yFCi3q9EvtdFHin/LkH6vgAfCNLXtZ71mJCC+iWN5nkgiup/tkNUY7jtgOuKiCW04YEDlJBedLi6EltCa4uxe+Eo4GWvAPZbzOdyIivqAdcJ3Yn+f6WwPHVrACwAPd1Z1bEcRlAhgtsqcXqIUtrQRYgtEW6wyGNcRGNgyHk/JcrPfFmGHtEim2vLGs4z3flcPtINT81slRxY6/Z8vtPQDrhU4625oVCGrrbxCa7RR3YDElje2/lDb2OPvbEgfIJxYuU1OchfyBp+sAnAu38nZfFjbiO7HOr3QYQQcrc/N6IfJ4c2ivC3EFmuZXjNhDL6TyRoDH44biLdHeSAcjaXQ+mrWm71t1meN6gYXx5TVXzOkDXBsJp7Y79Ju9P6Y91HUzjNctKpd04AHls8DBnTqCuJY7afy9aSbvPPvuEo4qiGYvFwUNmk4gugZRGxZYMXdLrQIY8K2I29UhU5YR525Yo54OqxArGyzEBJDEQv8KuvjRrRfU9lH7fpukYf1HG4rTAyXuUiwl8HHvZP6QyXp1BkHIuhTHPEJpbNiVQzVCX4VnyjUhwGOVTsnt58mDgkt4t3oRwTXfgYV0Zl8lGdlvN/SNuuemJL6MCO6RGvcWbSvK0ME5TJuCy5yLykruIh/9p8ladQZRCJgwpmZcM8iq3x3UdA+Cxe8OoNF1FgXBUCYj3iERraIFkILZBJa3p3G0q5kchttLN6tjoCPLyuwWuAoV7ztpn+iR1yjBKntRefAKkO89AB4SMzs9IN3hl63Um7qheFyWpgGglQSyuhP/LGA0PLbFxIwWCg1NxGtpTM4QlraZhFgbHrb32wdLDIO/UjlNAM6Q8DqW/1DxEhOHYu2H+UrH66Zz14K0BM+6KJDFvoaC3z0cxw0hLJA3zrht90D1dnCsyqPuuhgB3BKR3urjEXVERo7hIXetNd8VARO/YlUgWF4xw+alYqFPRO0t3FRQoqOCGCbdWnspGFHI7oo3d8wojUEHITVB2wTP1+CDAIjtNAiAQcO21M6RZ70BymEJsKu5j3Gda+Ap0TcLs3rcywPb1r4o4tCjnOY80SnGwajxC8IzINd5H7UC+xdYV0Pq5CQhf7g/WNiwnsmfwLxucrE4ssKvCS6Ci3cAE53NopxtohTE34fcLZK1HYFPIPpowY0H9bgnWlApxs4/CgjtCxOIpNwg8reSWFy9Rsf+lnqc8JjE2huJ2MeeI9xY1oyxVh5cbSATveQtCxJ+h3Xt5PU3m+4SJiPMeYjEX6yt+7jOJikOSX/utr7Pwm8tMIqE8I6ZESJ9VXXSvglLiS0SInkZ38L5Thxwd+e4AA72qCOFmarnjltf2OsamETVeikMCv8aosPsmjQw1GG7MRxhcpuWR3g8UAgGqn++YKLViFWQEBdGvu/zQ3p0z5i15+T5yEvrA8YjBKTYMpSVVQJ3LneF5A+43GXLCfT/mSX9m+pfiClHwuHEEbc4DMqk7rpe9giWggtIiqwfFxIga0GcipgubyfHPvbyvF6cfHKivFvbeWvXf6d9nu3ezESLSq0OobfMQDW/fvkjdjBIBY4sSVOm1Vo31U7/J/l4c2XxPd5RR/Ws5erzaglmOyrsMoPuaIhBPI2RfI56plr1Q/kuySwpvUqGDCeOgstYjercuLalDnZaoEPvtOpG8tbq4iSzHjTYv9nFclhohGdUAg1u5+icfBqkQcRdJxq6p9k3bE4COnk3mVlCH1hGYHVgmhMV5sxgvyKAU9psIyE0g4OWLYukw2KTEuDUa5SDKqtV1wb4bKVNKBY5p1pUBEuJ4t1OJYfbUC7hcs7bEFD63rqgj8Fontj+i3dge5hMMoB+BkDXhZQdntfu/ca0OVUcKwBnRbwUmAy7rQytESI0EpMUDtZJJvlAEPvCPvB+EKiLNhVrJIktFCqMnOUTeBIbCMMBO9s/cdglKI9iG2Wp8+gDrWvrht+k35LbrycpozOCHQdFkIrHtzQSiic6vVSJhCt1zT2yDR0mRXNOEQfXda/BCB9uvh/yoqYaBFehjyX1lvZtboKLfzqVCk2T7sYVMQW8c62v9GXWdlm/bhDNpcQ++m6Yk76LblhZayaOVlBChaP/W1xxE4ClikGdOJAr4VRdShTiBDmBexSJgWgi8+vtdBaKU3fwArGQmhtJwF4iFfSoqi0NAqMK+CBta1QnTE7/ZbcsJpQQimlLU7O7iqibE4Djvoa46Sk28uatkeIVGg3hugLFyn1OdCxzF2wXJrQmqlCTPKyqbNZ+WC5jTX1gLM7pmeGuLzD/y8qW0O2NjMD0LUSWhZZnjrBYnt0nwGNJNqhhFaIreFtAWi2TpJJIGMptJZOFFq+UhTyFidobBERWpbO0dO66CTK6uH6BVON8kzGUXehZZF0tKvBpAFCRpUNkcPxJwFohqK9ZJbjaNKJExsrrlfIi+19RtnPlaTTjk5bQ7AoRLAkFPEZgWhb9Z+FuUMnWExKIbZDLbyUfkthhOjTFwLQDEV78dQO0Ez+goQNx99lhQ0mCISXsJop5oi3bvocy7RodcWJSRFTS6LuKy0L/kJObCFpd8vEVAbmfrUBab+eVWqzRbRYIVmG0rgw4bdQtkt1AXn9QgZ/q7vQsvhwQzo6hwx9XDgnZAIwSwphOtOibYmXswotFHVEIS3r3Gw1iFlJxX3R2mFhY1RX4A+4V8mEuGmwElqhtocvG9Cw8PbohtUD0rYyI2kH/HaMaGFE2xIvZhpU3i2DbB8nGTNQFNOTog4IobZNvcQ8lWPV7osqqMtqexNqpWWhM/qYAY1usArv3Qm/DkBzwC0Yh86atiWezTMTYoh3ggs3EPOgmwK+heddtIwOcdJSJVAWc3zOgLoiTx7Akqj79vB5Axr4sS1PsmIDWkPwgQC2tKQZQ2LK+ILYUXwfYR0B13u+WIeDfjqz0PKZqGe4KLBfL8Fx8j1JN3hTDSzFP1KyLuKKdbIDS8NbXPHVCqE+2P6yxf1tSlKHUKj79tBiJc37OUjlywa02kGwOqskLQtB42Gexjb6p5GGZPHjJDLoxYY0AX6d1rkY5+QdVKxwei20Lsz4IRMiuazQwhvghKLRDvoYdV9pWdlBHSwB8O0cWaMSIVoonauI8oC9Y1IY6iKYIP6vs/LFJGGzLvta0GoDKqGFgwCmAK/951zYAGRJ4NQo62yAs+5BJevDZeJkF+VeW5RQd6FF5FCU8XFH6rxgRTRFH9gWRuG0CdNcRfhlxra10FpO5QL1xS5lV/eiwXthh1LWtjOOe+Etl9DyWWTQbVUxm3QCPlJZj2Zx0GYglv1wDlWbSUt2fUk6/YRaK+L9wRBO8hbKdCLcniN6Xyrzser58a5c2Ow8IHpCiDhvRJ19Tm0ZW7Qv9CwTAfHz1jLlLMJ8d6MiOgdOETES7YXVeZoCfgg+SgUz0kDJOll1EJlyK9FM1KUNJxD720CfFkqnBXDGtToBPEBlSbV5TDwFexoISueiTE1HGPGSBWQImuvChNpmVzHS90Uue0ef8OMqFyYXI2Nxfoy33IOKoP5i7g5nF0o3K+aq3J7zGeIaDRjUTdjoW9Vu7KOuNKA3BNH8oItmaOKgs6xGAU8+xKtyrCpDAGFdVmiFPGlmJX1s6l3ZgU/s2j6PZCZnap/G61wXLpJoR/iDJoTDMYGq2FVlU9VB/16ZtnX220FUMWRjsshQ3wlsDeebexSdCVnxVC20LipgUMmLxT/PwsGWvH5X+FCyY1PsxFIhOsRFwqcTZX9ch8RJzmm6h0QL43p0EGCh1woptO51UZQLi3fbAhMHocYRiFNV7sCNrf0G/UaUhc1cFF4JvVKv/Fyx18PDxPp0rgUis5DUZaJPdEMKMeK/YyNHmwn/RH/xPZDrIVSughaG7BOLCi2WaRg7VhW3qpXOKBdY3nodnGXq9NEucv4mfdT58UGdBj1HOiy2IwirJEUlTsFHqgz4rWkIS+gkWHwMwbaHRLfwcau+GID8J31x3rygteLFPccizHNpkNbLh30JvXjgMOpw19vDKHYfV7X+KDSofFhZTvGOsuIqBbeozv8t+Cz6hv2dbQZk4nWNVzlO/XC3i7YqpFsiXO08/0GxyljKRa5POIsjrJiZOyVpSALxwG8QvQECzBnxnwUWK4jQhsisBEIIrXaMdLY2UZYgakrVO55e4Jz23UaZmRCHZVYCoZan8boKQY3lNAS9QwhFKf23hS9DUH2v+9+s+mZjFxnqVZkZqO7bQ8BKw8Inti+hsX2XxtpMZ+8qUyewwzir/T8KCy2/POU0baAkU2nALuyWkjTwmWRGrmrGtLZPAUepv8+qUL9V6+0hQEHsJ6RTQ9ZTc7DbecAN3xhyJ8YNXssOKlZAAyVppGFKWZ8o0iJhe+ICZ4sODKytUXpWZS/WDystQM5CXHGWqqCu2kFj+0GNbQ7G9u81LwHwiIve7wIoK7SuVTnb2cfMaYHTwu9YENLLvUwvdwdnGzm1amzkqhNa/aDTmp+aXu8VveUpoeuqMTB92Mrl15fWGa+5KMP5QguWUkILQzwNGOJaWZ7OtYMj518Y0mM2IsHGKEOaVWK5Cuuq/fawDeg8xrhFV7fFTgI7M+wn6xCFxQLjOqQGnA+LQcUWMZTQKqyA7wRvJc9qC2vqYJ74wwT9sj3kvb5Kijr984Yq6qsj1Acz1Qe411V5WBMK7CZO6/ZjaaGlzpqjziJW+0ZlacWAHZj5IBS/j4jfnV0Uo6qqlYAVQgSA64a+EVpA7/VGvVeyou9RVZ01BB86EVn7uQ/QY+2R5EJm9dGiCLQWWpdgD2ZMcz5E9yZcclxk9dxPy+kQiVm7oZ+2hy3gSsI4XKXiemsB796zn4tyI1pHgagC5Fj9dJrPo9WgmqaChbiVSwVS1nRrGIc65rt6wQhFThRDmChY43cuPammpfFpL4IPdkOmdnkPCLb/CPdQPnC9QGYnbm/4vaP+eY3rL8FFHsrNxX9qVFoToaWKXlFH4Zx8gAU9YZZoPmVEqytUxzTxjS8VLgJ1PzI/LUMEAisHa05sLLK+WCSgAPOy3qg+elzvdCcXuZq91aj+XgIXtlwhof0BGf6AnLzvFoQrW+BNsk1WlzjL5TtbRCuhlTkETVmoo27XC2ZLgfnGmlXVmxNEHZiU4T6rbL5zjYLizTWgAR7Nc7N4/x7B7PRPInL0wyo6CQ8VySLuV1zotpj8T3T1NT5lRTiahU/WB8yElip92PvhfaIkKWaVqw1YygxWdeKdGEDEhD/QVeOalBWE1t0uo98hR95secuuMKx0Z/cb0ZmR9wH113S9021d9FGUjXDaSxQOheSV2V9TP2AxP9X1LuJwJ7BrIErFN/PGbbNWlBJPh5AhZZTb40Ip4JPg3WMO0gtGP4drSK9XXcyu2B+Nz+q6o/te8mFEyjoRW6WTwjewbFhktu+FJjG/4trERUfoo0rw0Cuw3S+t2/W7CWJ/sVrfvTRX5cHOYV/xVSjXoqnQEhP3q3Mm6J/jCpLAxGGqHUf5oTbMUhvW0T85XTzORXGFqgTbMmbX4wsa1o53kdV/0agWpKi6ueCzC0D8v6y+JKTQISXITMgbQTPGw6PiYV0XuYNUFQ65BXSDtB9dTZFv4qAybW8HNoq67KG+QAgSY259C7o5wS7qeBdZBhRONhziSHq8i+IO5dVv4Xy9a49SZi0AvxW7wNv9oMg8WOVDgatlcJIM4AzVXzjbjJ59Wnyz7D67wOP0/aHG+e8IdIhdXJHYa4yJb5RlgBWoLl/w0T7PdOEt5+lHVofjW6sJ1U2f5kl2jKPwNdaM+Ul5Q/1zexe5/5TNWJUFTL6s8ibnDWfdCeZCywudA33eQaxz046ekbhkMTmiF9vCJPgOZma6UO3h5fLxcZy8vFEV0EcPxXbsalYmFkRFh0QN8Hh0zkdP0bP/bcFDGy8vihcSJqCXyrP6e1jls0YHAi1e0HMRMYQJlegI1gH94BVhNZEVXqzur6pujIPPd8k6RxLy8i2cb8xbOy98oySfuM4LMFJ9sTq39CFmbN/qoiTPt1hOhCEjS56rDmG7x9aAfXR8gHBawO9n6t6HQvFhBfGIMpNyuI/rzoEDL3xtldVdFGk0CQwUBi3CnPbOUpmd59QkJ7/HiE+2emwF0nRKDCiyhwdxOvYuJmRdZtu7bIZH5pvPWG2NYrzwMU3yIW3YLo52UaSSMqdrhE8iKOZ5ot81kax+m6p6EcasOuIHVkxY9M/Jxv62iVBd5CP4ofjCMHdTF2XHJmEI4zrPgQ6LD04q0WljT3hbPKSMFULHOyLaKEtQPqCVXbRC4Qgau5ufVhyJ0wzi+0ldKKwQ52eucW+G4mVlyYqCF87KkZkTncZzFkvjnHyyxUU/RVgekhXEBQYKfn7nQ3kkMC/3iJc1PC97u4UnMfqG1FhMYrNC8uL54aADZ/9LxRdx1jDERJCg61nVJQsxVlRMPkSspf/uyaqj8SuwzXyOAGKs8w0i6B7u5U7D132nL4xpxu9qKqS2J3oE+RwZ261xzdhBT8Y3/oyLvmcru7xEVOZmga7FRWb6ww5+uT3P5TCCrApEbtVlrAYhUWYZgKNcJFQ5mZpTYVDBlm6JENUoYxEMpJzipBmh/njVQr2NL/riIl/4YMm+NMpFghWjY/5GKLFyoD+fKNtvep4P/ZkyNELCC7HHfKkV/g5Rspmze9GivQAAAABJRU5ErkJggg==">
		</div>
		<div class="row">
			<div class="xs-100 sm-100 md-50 lg-50 xl-50">
				<div class="defense-block box">
					<div class="block-image pp-left">
						<div><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEwAAABQCAIAAAA84QScAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAJH0lEQVR4nN2ca1CTVxrHT94kgBCEgFwSwk0g4WohcQl3uU0TQkabWQUXUBSstTir7H1nd73MdvuptlMBseuyn/xQCypaYBdmRFgWmhlURESIlDKKwyKU4RZAEgjsh7gRQ3Le814S2v19g/Oc5zz/nDfn8pzzhvGf8Qnw/w7Lpt6Xlpbu3bvX09MzPz8vFoulUqm3t7dNW7QIw0Y9ubCw0N7e3tnZqdPpNv4/NjZWLpf7+PjYolFr0C9Sq9W2tbV1dXXp9XrLTTIYRql261U6RWq12jt37qjV6pWVFfyGGQyxWCyTyby8vOgKwGpbtIjU6XRtbW1tbW3Wes8aGIYlJCTI5XJXV1fqYViDqkiDwaBWq1taWhYWFkg7cXR0zMjISE9Pd3R0pBKMNSiJ7O3tbWpqmpqaoiWU7du3y2SyhIQEDMNocWiCpMjh4eGGhobR0VF6owEAeHt7K5XKmJgYGn0SFjk+Pt7Y2DgwMEBjEJsRCARyuTwqKooWbwREjo2Ntba29vb2rq+v09I2LgEBATk5OeHh4RT9IIkcGhq6e/fu06dPKTZGjuDgYLlcLhQKSXuAiVxeXu7v7+/o6Hjx4gXpBujC29tbKpXGx8dzOByidc1FGgyGsbGxZ8+eDQ8PazQalGndnjAYjNDQ0MjISKFQyOPxGAwGUi2TyL6+vu7u7qGhoR+aMGtwOJzo6Ohdu3aFh4fD1b4W2dzc3NLSYq/waCY5OXn//v0QAwwAMDc39+NVCADo6urq7u6GGGAAAI1GY694bMXz588hpRiuxY+CiQnYRIgBAF69emWvYGzF9PQ0pBQDAKyurtorGFvBYsHyODSv97cKBwcHSCkLAIA4pdoUnq+vVBofvDP4+8nvBwYHe3oeEqru7OwMKWUBAGjfvxElKyuztOSoKQylMvcbtfrKlZrl5WVED/C1HgYAcHNzoxglFdLSUt8/Vmr2QSclJh7Y/1N0J/gid+zYQS4+6vjx+aUlRy0WyWTvenh4IPpxcXGBlG6lSDabXV5+ylpeh8Vi+SKnZ+Efx1aKPHq02N/fH2Lg4AgbMzfC5/MhpRgAwMPDw/4DbHJyUmZGBsRgeXn522+HUVwxmUxfX1+IAWY04nK5hEKkCI/He/9YKdymo+Pfi4uLKN58fHyYTCbE4PWYZs8nls1ml58+5eTkBLGZnJy89lUtokP4swpMIu2QqzdRfPhQYGAAxGB1dfViReXS0hKiQz8/P7jBa5EhISGIHimSmJCQnZ0Ft/nyy2vffTeC7hO1J4VCoR3GHh8fn+PHj8FtHjzoafrHPwm5xe3J14t3FxcXgUCAnpWLiorMVSj4fny9TtfX97j+1m3cQYLFYpWfPrVt2zaIzdTUVPXlLxBjMOLu7g5fCYCNJ83h4eGIIlWq9/LzDpj+DAgIyMhIr6isevSoD1KrqKgwODgIYmAwGC5WVCKOqCZEIhGuzZsVI4o1AOCdd3blHTDPGrm4uPz2N7/ek5ZmrVZ8/E/ksnfhnq99VYs4MW4kOjoa1+aNyKCgIJSTs9SUFIvfXiaT+eGHH6hU720u8vLyOvHBcbjbhw97GxoacVs3g81mo2TW34hkMplhYWG4FULDQiGl+XkHjpWWbPwUmExm+emfw/d709PT1Zcv4za9mbCwMPh22chbGxyUJ3Z2ZhZukJ2d9atf/oLNZhv/LCz4GXx+Wltbq6io0mrJnOEiHnu9JTImJgZ3A42Sv9y9W3LmT3/kcDi7JRKFIgduXFd3XUP2KAlR5Fv5Hzc3t8jIyP7+fkiFhsam5OQk3BWSUBh2ubrK1J/W6Ot7fOv21yiBbsbf3x9xu2/eb0lJSfAKi4uLn1z4bHZ2Dtc1rsKZmZmqS9WkTzvRj2jNRYpEItwdyejo6Nlz58fHX5IJ7X+sra1VVlXPz8+T9hAXF4doaS7SeOcEt9rk5OTZc+dITGsmbtysp3ImLxKJ0O86WRhmpFIpSv5Oq1346C8fP+jpIRYdAACA/v4nN2/Wk6hoIjU1Fd3Yghjj8INSWa/XX7jwWWvrXfT2AABzc3OVVZeoXDzw9PSMiIhAt7fcY7jDj4n19fW/1fy9ru46ov3a2lrVpeq5OfxxC0JKSgqhXLFlU5FI5Onpie7lxs36L/56xWAw4Freuv3148ewKQoXBwcHqVRKqIplkRiGyWQyQo7a2//1yYVP4TnvwcFB9D63hkQige/XNmO10yUSCY/HI+Srt/fRnz/62NqsMD+vraik9FU0QmjIMWJVJIZhubm5RN2NjIycOXv+5UvzKVSv13/++cWZmRmiDs2IiIgg+tED+NFdVFRUcHAwUY8TExO/+/0f6q7fMD66BoPhG7X6zNnzA4ODRF2ZwWAw9u7dS6Yi/EbWyMhIZWUl2agAm83GMMzshjZpEhMT8/LySFTEGYh37tyJOGdaZGVlhS6Fjo6OOTk4Gxpr4M82SqXyh3BKm52dTfp2M75IHo8nkUjIeacLLpebnp5OujrSumHfvn0kru3RiFKphF99gIMkksPh5Ofnk26DIoGBgWKxmIoH1BVgdHR0fHw8lZbIwWazDx48SNEJgWWuSqVCP9+mC5VKBT97RIGASCcnp4KCAnuOtLGxsYmJidT9ELvcEhISQmWUIwSXyyU39W+G8A0ehUJBYvVIFAzDDh8+THS3YdUb0QosFquoqAglb02FnJycoKAguryRuYvF5/NLSkrgx/RUEAqFmZmZNDokeeFMJBIVFBTQGIcJgUBw5MgRem/CkfclFotVKhWNoQAAfH19T5w4QddX0QSlDywtLS07O5uuULy8vMrKynCPjUlA9anIzc0lmlayCJfLLSsrs9FblDQ8+nl5eRTfHHNzczt58qS7uzv1YCxCg0gMw4qLi0lvx1xdXcvKyghlQIlC5zvNra2tTU1NhPJxISEhhw4dsvWFW5rfTn/y5MnVq1cRUx5ZWVkKhcIO96bpfwV/YmKipqYG/g6ws7NzYWEhlewRIWzyYwpLS0v19fX379+3WBoYGFhcXGzPe5m2+sUIAIBGo6mtrd2YUGYymXv27FEoFLZbElrEhiIBADqdrrm5ubOzc3V1NS4uTqFQbMktaduKNDI7O7uwsCAQCGzdkDXsIXLL+S89YeKzs7t1OQAAAABJRU5ErkJggg=="></div>
						<p>Защитено пазаруване</p>
					</div>

					<div class="defense-block-text pp-right">
						100% защита на твоето плащане! Пълно възстановяване на платената сума, ако не получите пратката си! Възстановяване на плащането при дефектен или несъответстващ на описанието продукт!
					</div>
				</div>
				<div class="checkout-block box">
					<div class="block-image pp-left">
						<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEwAAABMCAIAAABI9cZ8AAAACXBIWXMAAA7EAAAOxAGVKw4bAAAIgklEQVR4nN2cW0wb2RnHz8z4AjbBEWCuNrJCgLCEiwN0iWIripKiZtGKpBAiha26VSLlId0nNq9Rs2pf2r4mD20fSpIihRAeuoTQREoCxiLcDFujBIG5eZyFwAxjgyfg8a0PgwbjG3Mzsfp7QGeOz/f5+zNzzplzM/Tzykfw/w78uQM4DCSJc+3xeBwOB4qidrsdx3Gv1+v1en0+HwRBEolEKpWmpKTk5eUVFhZqtdqcnBwYTtR/HBL3cSUIYnp6mha2trYWDAZZGspksoKCAq1Wq9PpTp48KZVKRYxKNJFzc3Mmk2l6epq9sFgoFIr6+vozZ85kZGSIEptQkR6PZ3x8fGhoaHV1VZSAGCAIKi8vNxgMpaWlQl3xFun1el+8eDE0NLSzsyMwiPhkZ2c3NjZWVlby9sBT5PLycmdn59raGu8v5oper29ublYqlTxsOYv0+Xz9/f2vXr0SXve4kp6e3traWl5eztWQm0gURTs7O0Wvfpyoq6u7fPlyamoqexMOIs1mc09PTyAQ4BWbmKhUqps3b+bl5bEsz7b/NZlM3d3dyaAQAOByue7du7eyssKyPCuRJpOpp6dHQFTiQ5Ike50Hi0xChTTsdR4gMmkV0rDUGU/k1NRUMiukIUny/v37W1tbccrEFLm1tfXkyZMERCU+brc7fqgxRXZ1dX369CkBISUEq9U6MTER69PoIsfGxqanpxMWUkJ4+vSpy+WK+lEUkU6nM/mrYiTb29tdXV1RP4oi8vHjx4keWCSId+/ejYyMROaHT3/MzMzMzMxEdaFUKtVqtUSCIAiCwAgiQQAAKyurGIaJHi5vent7a2pqJJJ9usJFmkymqMaZmZl/+uMPR48ejfyIJMmOBw8HB6MbHjJut3tycrKuri40c9/jiuP4+/fvoxpXVJyMqhAAoFQqv7r4K7GiFM7Q0FBYzj6RZrM51ijR+l/r5uZmLL86nS4tLU14fKJgt9vtdntozt5Qi6Kou3fvxukbVap0tVoNAMTkFBUd+923v6XTJEkmaIwSDAKK8lgskw8ePvL5fGxMamtr29ramMu9OmmxWOL3/i7Xpsu172biOM6I5DcxwZojDQ2/nF9YGBgYZFN6cnKyqamJebj2Htfh4WGu30wQRPyXRnGRy+UsS/r9/tHRUeZyVyRFUSiK8vjixcUlHlaHwMLCApPeFelwOPhNTM3OzooTlNiE3jM4MosTs7NzIkSUADY3N5lXWaEi52y2JJn4iYQRtSsyrGNhz/b29ocPH8QJSmz2idzZ2VlfX+fta85mEycosWHuHAwAEHgrbLZ5ESJKAA6Hg07AAACBMwC2ZL2T29vbdAIGAHi9XiG+UNSRnONPv99PN4oiiAwGg/MhPW9SQUuDAQDC+4CkrZZ7dzJsHM2DpK2WtDQYACB8F8LhiPT7/VxNaGniiCQIJ47jAp3EZ35+fnR0jJMJo0sCAIg1r8EJm20+MzNTuB+SJDEMx3Ecw3EcxzEMw/GN9bU1fGODqyuVSkUnJACA3NxciUTCctAdi7k525df/oJlYYqicHwDx3EMx3B8A6Nl4RsYhnk8HiFhhKLVaumEBACAIEh+fj7v11eayGoZCAQIgogQs4Fh2OEMtQsLC+mEhLkWKHJhcdHv9yMIAgAYGxv/Z8cDgiA+7wCFuZNw2DVvKIpi1gmlUimO459XIQRBGo2GTu+KZO4sb/Jyc3Nzc+m08I5XONnZ2cyckITJkslkFEXxdvrNb9oYbQRBGI0GgVECAEg3OfXTT/yeiNBnczcsGIZLS0utViu/aFJTU2tOnWIujUaDKCIBABbL5J//8lcehmVlZUx6b0rSYOAfVlZWFm/b+FRWVkAQdHC5/aSnp1dVVTGXeyJLSkqYSsUVgiCWl5f52cbnzcAgj2nE06dP0+08zb4dWWazubu7m180mRkZt2+363Q6AABFUb3P+pxOZyAQoAd1gUAg4Kf/RL9kSumrqy5dapLL5cFgsK/v+cNH/+IaCQzDd+7cYV53QNjSXW1tbW9vL48RMARBv//uFq0QACCTyb4oK/vD3R+4+jl+/Hhz86/pBgyCoMbGr9YxrL//P5ycVFVVhSoEYatacrk8bGWPJUVFRWUnToTmnDhRWlNzKlb5WHz9dWNY93P5UhPXOhnZuIQvpxuNRh4b3tXqKA1Pbk4OZz9Z6rAclUqVkpLC3oNWqz127FhYZrgetVp94cIFrsE5HFHm+1Y/ct4uHPlqiWEYMx91IDAMX716NUp+ZFZDQ0NBQQGn4FAUHR5+G5qzsLBosUxycgIA+PePP5IkGZrT9YRDQxgrcqT9+9thWTAM63S6kZERTm33+PjEzs6OUqF0uVwDAwN/+/s/eMyPbW25JyYsKXI5DEN2O/rw0aOw/10cNBrNtWvXota1mJt6X7582dfXxzXKzwWCIO3t7bG2+cZsY86fPy/8rf3QuHjxYpyNzDFFwjDc1tamUCgSE5WYlJSUnDt3Lk6BeL1Fdnb2rVu3klxncXHx9evX43d7B3SJ+fn5yayzuLj4xo0bMpksfrGD+/2k1clSIWC50T4JdbJXCDidC1lZWeno6PjI/T1GdGpra69cucJSIeB6wsfn8z1//vz169eHf4aJJi0trbW1taKigpMVnwNpS0tLnZ2dQlbg+VFdXd3S0sJj6xfPU3cURT179mxwkNUmMOEoFIqWlha9Xs/PXNAh0aWlpTdv3lit1sRNsSqVyvr6+rNnzx45coS3ExGO+zqdTrPZ/PbtW7fbLdBVKBqNxmg06vV64Ytuop1p9vl8U1NTJpNJ4HIDgiDV1dUGg4GZTBGOyKfTAQBOp5M+nY6iKIqibLaWZGVlabVa+iy+RqNhvxuSJeKLDAPHcRRFMQwL/Z0BqVQqlUrlcjn9OwOcTnzyIOEik4H/Ac63/eOsP/vKAAAAAElFTkSuQmCC">
						<p>Умно пазаруване</p>
					</div>
					<div class="checkout-block-text pp-right">
						По-умното пазаруване винаги ще бъде онлайн. Vendo ти предлага богата гама от облекло, аксесоари, обувки и електроника с директна доставка от цял свят на ниски цени!
					</div>
				</div>
				<div class="bonus-block box">
					<div class="block-image pp-left">
						<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEwAAABMCAIAAABI9cZ8AAAACXBIWXMAAA7EAAAOxAGVKw4bAAAKe0lEQVR4nN1ce1BTVxq/Ny9CCAQIMTySYF2xPER5JaNNsq44rPjaCBZwKLvb1dqX03VnfW53dkZabV2dzvqspTvtrE5BhtlCp1XQgrJOwksRGAVbcWcCuREEhYCQQF737h83XkISwrk3iTj7++vce7/vu98v59x7v/Od7wQeGByC/t9BW2gHXgQYgTM9NTWFIAiCIDqdbmBgYHp62mq1Wq1WDMMYDAaTyWQymXw+XywWi8ViiUTC5/NptID86LB/h6vRaOzo6NBqtQiCPH36lJQum83GCaempsbHx8Mw7C+v/EZSr9er1eqOjg6bzea7NZFIpFQq09PTmUym79Z8JWmz2e7evatWq/v6+nz3xgUcDmf16tVyuTwiIsIXO9RJYhjW2tpaV1c3MTHhiwfzAobhtLS0vLy80NBQihaokRwbG6usrHzw4AG1u1IAh8MpKChIS0ujoEuaJIZh7e3t1dXV09PTFO7nI9LS0rZt28blcklpkSP57Nmzqqqqnp4ekr75E1wut7CwMDU1FVyFBEmdTldWVmYymSj55mesWbNGpVIBfmZAP746ne78+fMvCUMIgm7evFlTU4NhGIgwEEmc4YI8hF6gVqsBec4f1gWUoUAgyNuqkkqzQkJCpqam7t3r/ra6BkEQQHW1Wg1BUF5envdxO88zGVCGJW8Ub9y4wT1eVas1X5R9abfbAe0olUrvPL0N1/Hx8bKysgAxzNuq2rx5k8eIXKlU7PjDm+Cm1Gp1Y2OjF4E5SWIYVlVVFaA3jUwqLSoqfH5kn+y/fuLgZ/X9RqLv1q3L3rAhF9xgbW3t48eP57o6J8lbt27dv38f/DakkJ+/1dGyG27/8+CuQ1/f0XV8dejdA1/dMaCOK3lbVXQ6HdCg3W6vqKiYa4R7JmkwGGpqakj5DQ6RSLR48WK8bdZeuXBjMFy65pVQ8aok9qOGSw2IBb8UFhaWmroc3CyCINevX/d4yQNJDMMqKyvNZjM534GhUMiJtu3ZkwkI5i9dzAuKeu2Dk19fPF4Qz/IoCYJr1649evTI/bwHki0tLb29vaSs41iyZAnI1D42JoZoB0syX2Vh/710oWtk+OdeAzpbPS42lpQDKIqWl5e7T2hdfbLZbLW1taRME5DJpB+VHo6OFnoXY7PZM7ePUry///UMIQPCHtWePPje3yq6J1DiapCTJCAGBwc7OztdTrqS7OrqMhqNZE3jMBlNS5f+4tinn6xbl+1FzGxxfhBo4cvzDhz5ID0sOiOVb9Ve+UY9TLw9zNNUHhk8QnCG+4fYVQIcRpMRgiA2m73rrZ379+0NCwvzKDb0mAg/bMMt33z2yenvdVaYFfOr3259BYKMYyZitA0NUZnrIgjS39/vfGYWyf7+fp1OR8EuDpNx5qOamZlx4vixjPR0d7E7HcRwYoSEGh/ca7t0/gftxKC6/DstFJacKgx6frmpuZmaJy5dNSt21Wg01IziwHuSAI/HO3Bg340bjU+ePBFLxGKRKCIiksMJdn45hSRv35M/cqa6xwBBt7uj0ov2lKSEOKwZjZ2dXdQ86erqUqlURLpkhuTk5KT7I0sKHsOj7Oy13nRovJSCDz/PaTn24c3sY4dWOQ3w+obrlBN/dru9tbU1JyfHcRPiQnt7O3hM7MFbGi0lOZmibmhSwTvbEkNmncxd/+uSN4qDg4Op2WxpaSHaMz3pS06Rx+Pt2/vnhISlFPXp4Qkrw13OsdnszZs3ZWZm/uPkKQpvCoPBMD4+zuPxIOeepPzKEYlER49+TJ2hV8TERB/5uFQuf42CLjEvdZCcnJw0GAwUDAUHB+/ftzeKz6egCwgWi/X+e++uWEEic4XDlST4ZNwFb+3cIRQuoqYLDjqd/qc9fxSJ4khpEWOT5nJMCklJiS4Dqbu754uyL/914aLeU6DsCzgczjtvv01KBUEQPAPEII4p3Hjzpk3OhxpN09lzn+Ptxsb/fFR6OD5eQsHsXEhIWCqTSm/dvg0obzQaDQZDZGSkoyc9zlC8IzpamJExK6CpqfmOaJvN5rqrV8nanBcFBa+Tksd5OUhSSOQo5HKX3JFpasr5cGr2oV8gFotIzb9wXg6SVqsVUC0qKiolJXl5SopUmuVy6ZdKxexDJbg34Ej3FA/PBZwXA4IgFEVJ5P8Ucqcc1CwUFhbQ6XS1ponNZm/ZsikzMwPcG3AkJr56+coVQOEZkoDJ9nlBp9MLCwsKCwv8Ym0uRES4xkZegKIohA9XGo0GvkDvr1+EMnjhJEgyGAwIJwnDsF+W5l8MSJVLsFgsiHjxgJNc4H6EoLGxMXBhnJeDZGRkZEA8CgBGycTYOC8HSbFYDKq30M9k7wPQdCkMw7GxsRBBUiLxZ/wVUHR2geZEYmJiZj2TJHpyQaHX6/V60AiUIOUI0IVCIZPJBIl7vv/h8uUrtRiGbdywoaSkmJqvlPHvb6vBhYnh6ehJOp0eFwc0W8MwzG63oyj6Y339yMgIWS99QV9fX2trG7g80ZM091OAsFgsFRWVpFR8gclkOnXqDLg8nU6Peb7oMkNy+XIS62Q4mpqbOzp8ymICwm63nzl7bnDuZVZ3JCYm4uEO5EwyISFh0SLSiYyTp04/fPiQrBYpjI+PHzn6CdlEs9JpDjRDEoZhhULhSd4bLBbLsb+fuHOng6wiCFAUbW5uOfSXv/7008+kFAUCQUJCAnE4q/pjenr68OHD1JZf09JWvvn730VHR1PQdYdW23evu7v+x/onJCuDceTn5zv3pGuJS3V1NeWFLRqNlpSU+JstW1auXEHNAoEdO3dRrslgsVilpaXOq6CuxUpyuZwySRRFe3rui8Vid5KtrW0XLl7E285x4bmzpz1WP/iymC+TydizV29dSQqFwuTkZF/qPjwmGaxWq8HgYfbgcR5rs9kor8rQaDSlW9rFQ0XW6Ojo8ePHKf+WMAwzGAwYhnECeMNut1ssFnfhkJAQDMPwibhzw6MwCNavX5+b61oA5KG2LjIyUqVSVVVVUbsNhmHgaTHKS/ceIRKJiOU6Z3gu1li1alViYqIfb/8CQKfTi4uLPT7hnknCMFxUVMQmX3yxgMjNzY1xKp5xxpxlN+Hh4fn5+QFzyc+QSCRr1865pO2ttigrK0smkwXAJT+Dy+WWlJR4KcTzRhIftFKpNACO+Q1cLnf37t0CgcCLzDxVYjQabfv27S8tT5zhvLHk/KVwLy1PQIYQYKH9S8gTnCFEal8IiqJXr15taGhY8JUCsVhcUlICPvslvY1Jp9OVl5cPDw+T980PoNFoubm52dnZ4EXNELUNaVarta6uzntteyAQGxtbXFwMmHBzBvWthVqttqKigux2V2qAYTgnJycnJ4dI25BT92WTqMViaWtr02g0gRu9TCYzKytLqVTOFbKBwA/bfTEM6+3t1Wg0PT09fnwn8fl8hUIhk8k4HI6Ppvy5cXt0dLSpqamtrc2XCRQMw8uWLVMqlUlJSf7arO7n3ekQBKEoOjIygu+8RxBEr9fPOwMWCAQSiQTfmh4XFxcUFORdniz8T9IFKIoODQ0NDAyYzWb8fwbsdjvzOfh8vkgkolzvCYiAk3wZ8D9pp0HwJigZcAAAAABJRU5ErkJggg==">
						<p>Бонус Точки за редовни клиенти</p>
					</div>
					<div class="bonus-block-text pp-right">
						Пазарувайки от Vendo получаваш Бонус точки за всеки похарчен лев. Натрупаните в сметката ти бонус точки можеш да използваш за отстъпки и безплатни поръчки на продукти от сайта.
					</div>
				</div>
			</div>

			<div class="xs-100 sm-100 md-5 lg-5 xl-5"></div>

			<div class="xs-100 sm-100 md-45 lg-45 xl-45">
				<div class="tab-content-login">

					<div class="tab-pane-login" id="tab-register">
						<h2 class="popuploggin-title">Регистрация</h2>
						<?php echo $facebooklogin; ?>
						<a class="popuploggin-button register">Регистрация с Имейл</a>
						<div class="popuploggin-or">ИЛИ</div>
						<a class="popuploggin-button loggin">ВХОД</a>
					</div><!-- registration -->

					<div class="tab-pane-login" id="tab-login">
						<h2 class="popuploggin-title"><span title="Back" class="popuplogin-arrow"></span>Вход</h2>
						<?php echo $facebooklogin; ?>
						<div class="popuploggin-or">ИЛИ</div>
						<div class="popuplogin-alert warning"></div>
						<div class="popuplogin-form">
							Имейл
							<input type="text" id="login-email" name="email" placeholder="Въведете вашият Имейл" value="">
						</div>
						<div class="popuplogin-form">
							Парола
							<input type="password" id="login-pass" name="password" value="">
						</div>
						<div class="popuplogin-form"><a id="popuploggin-action-login" class="popuploggin-button loggin">ВХОД</a></div>
						<div class="popuplogin-form-text"><a class="popuploggin-action-forget">Забравена парола?</a></div>
					</div><!-- Log in -->

					<div class="tab-pane-login" id="tab-almost">
						<h2 class="popuploggin-title"><span title="Back" class="popuplogin-arrow"></span>Почти Готово</h2>
						<div class="popuplogin-alert warning"></div>
						<div class="popuplogin-form">
							Име
							<input type="text" id="login-name" name="name" placeholder="Въведете вашето име" value="">
						</div>
						<div class="popuplogin-form">
							Имейл
							<input type="text" id="login-email" name="email" placeholder="Въведете вашият Имейл" value="">
						</div>
						<div class="popuplogin-form">
							Парола
							<input type="password" id="login-pass" name="password" placeholder="Въведете вашата парола" value="">
						</div>
						<div class="popuplogin-form"><a id="popuploggin-action-register" class="popuploggin-button loggin">Регистрация</a></div>
						<div class="popuplogin-form-text"><a class="popuploggin-action-loggin">Вече имате регистрация?</a></div>
					</div><!-- almost -->

					<div class="tab-pane-login " id="tab-forget">
						<h2 class="popuploggin-title"><span title="Back" class="popuplogin-arrow"></span>Забравена парола?</h2>
						<div class="popuplogin-alert warning"></div>
						<div class="popuplogin-form">
							Имейл
							<input type="text" id="login-email" name="email" placeholder="Въведете вашият Имейл" value="">
						</div>
						<div class="popuplogin-form"><a id="popuploggin-action-forgotten" class="popuploggin-button loggin">Изпрати</a></div>
					</div><!-- forget password -->

				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	
	$('#tab-register').fadeIn(100);

	// action registration
	$('#tab-register a.popuploggin-button.register').on('click', function(){
		$('#tab-register').fadeOut(1, function(){
		    $('#tab-almost').fadeIn('slow');
		});
		backArrow('tab-register');
	});

	$('#tab-register a.popuploggin-button.loggin').on('click', function(){
		$('#tab-register').fadeOut(1, function(){
		    $('#tab-login').fadeIn('slow');
		});
		backArrow('tab-register');
	});

    $('.popuploggin-action-loggin').on('click', function(){
        $('#tab-almost').fadeOut(1, function(){
            $('#tab-login').fadeIn('slow');
        });
    });

	// action on forgotten password FROM tab-almost
	$('.popuploggin-action-forget').on('click', function(){
		var parent_tab = $(this).closest('.tab-pane-login').attr('id');

		$('#'+parent_tab).fadeOut(1, function(){
		    $('#tab-forget').fadeIn('slow');
		});
		backArrow(parent_tab);
	});

	// action on back arrow
	function backArrow(prev){
		$('.popuplogin-arrow').off('click');

		$('.popuplogin-arrow').on('click', function(){
			parent_tab = $(this).closest('.tab-pane-login').attr('id');

			if(parent_tab != prev){
				$('#'+parent_tab).fadeOut(1, function(){
					$('#'+prev).fadeIn('slow');
				});
			} else {
				$('#'+parent_tab).fadeOut(1, function(){
					$('#tab-register').fadeIn('slow');
				});
			}
			
			$('.popuplogin-alert').fadeOut(1);
		});
	};

	$('.tab-content-login .facebookLoginAnchor.fbkRoundedBtn').on('click', function(){
		$('#popuploggin').fadeTo('slow', 0.5);
		setTimeout(function(){
			$('#popuploggin').fadeTo('slow', 1);
		}, 3000);
	});
	
	$("#popuploggin-action-login").on('click', function(){
		var email = $("#tab-login #login-email").val();
		var pass = $("#tab-login #login-pass").val();

		$.ajax({
			type: 'post',
			url: 'index.php?route=module/popuploggin/login',
			data: 'email='+email+'&password='+pass,
			 beforeSend: function() {
                $('#tab-login').fadeTo('slow', 0.5);
            },
            complete: function() {
                $('#tab-login').fadeTo('slow', 1);
            },
			success: function(json) {

				$('#tab-login .popuplogin-alert').html('').fadeOut(1);

				if(json['warning']) {
					$('#tab-login .popuplogin-alert').html(json['warning']).fadeIn('slow').addClass('warning');
				}
				if(json['success']) {
					$('#popuploggin').fadeTo('slow', 0.5);
					window.location = 'index.php?route=account/account';
				}
			}
		})
	});

	$("#popuploggin-action-register").on('click', function(){
		var name = $('#tab-almost #login-name').val();
		var email = $("#tab-almost #login-email").val();
		var pass = $("#tab-almost #login-pass").val();

		$.ajax({
			type: 'post',
			url: 'index.php?route=module/popuploggin/register',
			data: 'firstname='+name+'&email='+email+'&password='+pass,
			 beforeSend: function() {
                $('#popuploggin').fadeTo('slow', 0.5);
            },
            complete: function() {
                $('#popuploggin').fadeTo('slow', 1);
            },
			success: function(json) {
				$('#tab-almost .popuplogin-alert').html('').fadeOut(1);

				if(json['warning']) {
					$('#tab-almost .popuplogin-alert').removeClass('success');
					$('#tab-almost .popuplogin-alert').html(json['warning']).fadeIn('slow').addClass('warning');
				}
				if(json['success']) {
					$('#popuploggin').fadeTo('slow', 0.5);
					window.location = 'index.php?route=common/home';
				}
			}
		})
	});

	$("#popuploggin-action-forgotten").on('click', function(){
		var email = $("#tab-forget #login-email").val();

		$.ajax({
			type: 'post',
			url: 'index.php?route=module/popuploggin/forgotten',
			data: 'email='+email,
			 beforeSend: function() {
                $('#tab-forget').fadeTo('slow', 0.5);
            },
            complete: function() {
                $('#tab-forget').fadeTo('slow', 1);
            },
			success: function(json) {
				$('#tab-forget .popuplogin-alert').html('').fadeOut(1);

				if(json['warning']) {
					$('#tab-forget .popuplogin-alert').removeClass('success');
					$('#tab-forget .popuplogin-alert').html(json['warning']).fadeIn('slow').addClass('warning');
				}
				if(json['success']) {
					$('#tab-forget .popuplogin-alert').removeClass('warning');
					$('#tab-forget .popuplogin-alert').html(json['success']).fadeIn('slow').addClass('success');
				}
			}
		})
	});

});
</script>