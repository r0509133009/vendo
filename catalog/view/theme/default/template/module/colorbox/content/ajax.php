<div class="">
	<!--<img src="catalog/view/theme/default/stylesheet/colorbox/content/popup_save.jpg" alt="popup img" title="Homer Defined">-->
	<div data-height="471px" data-width="800px" class="OM-box-container conversion actual">
		<form class="OM-form">
			<div class="OM-box">
				<div class="OM-conversion-box">
					<div class="OM-conversion-beforeHeading">VIP Customer Promotion</div>    
					<div class="OM-conversion-heading">10% Off Today's Order</div>    
					<div class="OM-conversion-displaySubHeading">
						<div class="OM-conversion-subHeading">Get 10% off your purchase today. Enter your name and email and we'll give you your code. We'll also sign you up to our newsletter</div>
					</div>
					<div class="OM-conversion-coupon">
						<div class="OM-conversion-couponShopName">20dollars4everything</div>
						<div class="OM-conversion-couponValue">10%</div>
						<div class="OM-conversion-couponName">Exclusive Discount</div>
					</div>
					<div class="OM-conversion-form">
						<div class="OM-conversion-formDisplayFirstname">
							<input data-validations="required" data-error="Please enter your first name!" name="visitor[firstname]" id="OM-conversion-formFirstnameInput" class="OM-conversion-formFirstnameInput OM-conversion-formFirstnameLabel OM-conversion-formFirstnameError" placeholder="First name" required>
						</div>    
						<div class="OM-conversion-formDisplayEmail">
							<input data-validations="required email" data-error="Please enter your email!" name="visitor[email]" id="OM-conversion-formEmailInput" class="OM-conversion-formEmailInput OM-conversion-formEmailLabel OM-conversion-formEmailError" placeholder="Email" type="email" required>
						</div>    
						<div class="OM-conversion-formSubmitContainer">
							<input type="submit" data-redirecturl="http://" data-action="nextPopup" value="Get my 10% discount" name="submit" class="OM-conversion-formSubmitText OM-conversion-action OM-conversion-redirectUrl OM-popup-button">
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</form>
	</div>
</div>

<script>
    $('#homer strong').css({color:'red'});
</script>
<style rel="stylesheet">

body {
    margin: 0;
}
.clearfix {
    clear: both;
    font-size: 0;
}
.OM-box-container.conversion {
    background: none repeat scroll 0 0 #fff;
    font-family: "Trebuchet MS",sans-serif;
    margin: auto;
    width: 800px;
}
.OM-box-container.conversion .OM-box {
    background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
    width: 800px;
}
.OM-conversion-box {
    background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
    border: 1px solid rgba(250, 250, 250, 0.1);
    box-shadow: 0 0 30px rgba(0, 0, 0, 0.4) inset;
    padding: 24px 40px;
    /*width: 718px;*/
}
.OM-conversion-heading {
    color: #840a0a;
    font-size: 52px;
    font-weight: bold;
    line-height: 50px;
    padding: 20px 0;
    text-align: center;
    text-shadow: none;
    text-transform: uppercase;
}
.OM-conversion-beforeHeading {
    color: #840a0a;
    font-size: 25px;
    font-weight: bold;
    line-height: 32px;
    padding: 5px 0;
    text-align: center;
    text-shadow: none;
}
.OM-conversion-subHeading {
    color: #c3c3c3;
    display: block;
    font-size: 15px;
    line-height: 27px;
    padding: 0 0 10px;
    text-align: left;
}
.OM-conversion-form {
    float: left;
    padding-top: 15px;
    width: 288px;
}
.OM-conversion-formDisplayFirstname.half, .OM-conversion-formDisplayLastname.half {
    float: left;
    width: 48%;
}
.OM-conversion-form .half:first-child {
    margin-right: 4%;
}
.OM-conversion-form input {
    background-color: #fff;
    background-image: linear-gradient(to bottom, #fff, #e2e2e2);
    border: 1px solid #b3b3b3;
    box-shadow: 2px 2px 7px 2px #bbb inset;
    box-sizing: border-box;
    color: #333;
    font: 17px/24px "Trebuchet MS",sans-serif;
    height: 55px;
    margin-bottom: 10px;
    padding: 0 0 0 10px;
    width: 100%;
}
.OM-conversion-form input[type="submit"] {
    background: none repeat scroll 0 0 #850a0a;
    border: 0 none;
    box-shadow: 0 -40px 40px rgba(0, 0, 0, 0.15) inset, 2px 2px 2px 0 rgba(0, 0, 0, 0.2);
    color: #fff;
    cursor: pointer;
    font: bold 24px/70px "Trebuchet MS",sans-serif;
    height: 70px;
    padding: 0;
    text-align: center;
    text-shadow: none;
    text-transform: uppercase;
    transition: background-color 0.4s ease-in-out 0s;
}
.OM-conversion-form input[type="submit"]:hover, .OM-conversion-form input[type="submit"]:active {
    background: none repeat scroll 0 0 #700a0a;
}
.OM-conversion-form input[type="submit"]:active {
    box-shadow: 0 -40px 40px rgba(0, 0, 0, 0.15) inset, 2px 2px 2px 0 rgba(0, 0, 0, 0.2), 0 0 7px 2px rgba(50, 50, 50, 0.3) inset;
}
.OM-conversion-coupon {
    background: url("catalog/view/theme/default/stylesheet/colorbox/content/kpn_bg.png") repeat scroll 0 0 rgba(0, 0, 0, 0);
    color: #f6f6f6;
    float: left;
    height: 216px;
    margin-right: 20px;
    padding: 35px 0 0;
    text-align: center;
    width: 400px;
}
.OM-conversion-couponShopName {
    font-size: 18px;
    line-height: 34px;
}
.OM-conversion-couponValue {
    color: #f6f6f6;
    font: italic 900 100px/84px Georgia,serif;
    margin: 0;
}
.OM-conversion-couponName {
    font-size: 20px;
    font-weight: bold;
    line-height: 33px;
    margin-top: 10px;
}

</style>