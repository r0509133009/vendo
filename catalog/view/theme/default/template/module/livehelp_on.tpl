<div id="livehelp" class="lh-widget box">
   <!-- WIDGET HEAD -->
   <div id="livehelp-head" class="lh-widget-head box-heading">
      <div class="lh-head-left">
         <h5>
            <?php echo $user_name; ?><span class="lh-badge"></span>
         </h5>
      </div>
      <div class="lh-head-right">
         <?php if($setting['sound']) { ?>
         <div class="lh-head-btn">
            <a onclick="javascript:LivehelpChat.toggleSound(event);" title="<?php echo $text_volume; ?>" data-toggle="tooltip" class="lh-js-sound-toggle"><i class="icon-volume-high"></i></a>
         </div>
         <?php } ?>
         <div class="lh-head-btn">
            <a id="lh-logout-btn" title="<?php echo $text_signout; ?>" data-toggle="tooltip"><i class="icon-exit"></i></a>
         </div>
      </div>
      <div style="clear: both;"></div>
   </div>
   <!-- WIDGET BODY -->
   <div id="livehelp-body" class="lh-widget-body collapse box-content">
      <div class="lh-messages"></div>
      <div class="lh-input">
         <div class="lh-textarea-plugin">
            <?php if ($setting['bbcode_status']) { ?><button type="button" class="btn btn-link lh-bbcode-plugin"><i class="icon-bold"></i></button><?php } ?><?php if ($setting['smiley_status']) { ?><button type="button" class="btn btn-link lh-smiley-plugin"><i class="icon-happy"></i></button>
            <?php } ?>
         </div>
         <textarea class="form-control lh-textarea-input"></textarea>
      </div>
   </div>
</div>
<?php if ($setting['smiley_status']) { ?>
<div id="lh-smile-list" style="display: none;">
   <div class="emoticon-list">
      <?php foreach ($emoticons as $key => $emoticon) { ?>
      <a onclick="javascript:LivehelpChat.insertText('<?php echo (addslashes($emoticon[0])); ?>');" class="emoticon-item emote-<?php echo $key; ?>"></a>
      <?php } ?>
      <div style="clear:both;"></div>
   </div>
</div>
<?php } ?>
<?php if ($setting['bbcode_status']) { ?>
<div id="lh-bbcode-list" style="display: none;">
   <div class="bbcode-list">
      <a onclick="javascript:LivehelpChat.insertBBCode('b');" title="<?php echo $title_bold; ?>"><i class="icon-bold"></i></a>
      <a onclick="javascript:LivehelpChat.insertBBCode('i');" title="<?php echo $title_italic; ?>"><i class="icon-italic"></i></a>
      <a onclick="javascript:LivehelpChat.insertBBCode('u');" title="<?php echo $title_underline; ?>"><i class="icon-underline"></i></a>
      <a onclick="javascript:LivehelpChat.insertBBCode('url');" title="<?php echo $title_link; ?>"><i class="icon-link"></i></a>
      <a onclick="javascript:LivehelpChat.insertBBCode('img');" title="<?php echo $title_image; ?>"><i class="icon-image"></i></a>
      <a onclick="javascript:LivehelpChat.insertBBCode('youtube');" title="<?php echo $title_youtube; ?>"><i class="icon-youtube"></i></a>
      <div style="clear:both;"></div>
   </div>
</div>
<?php } ?>
<script type="text/javascript">
   $(function() {
      // LOGOUT
      $('#lh-logout-btn').bind("click", function(e) {
         e.preventDefault();
         e.stopPropagation();
         var result = confirm("<?php echo $text_signout_confirm; ?>");
         if(result) LivehelpChat.logout();
      });

      $('#livehelp-head').bind("click", function(){
         var $target = $('#livehelp-body');
         //if($target)
         if( $target.hasClass("collapse") ) {
            $('#livehelp').trigger("chatBox.focused");
            $target.slideDown(300, function(){
               $target.removeClass("collapse");
               LivehelpChat.scrollToBottom();
            });
         } else { 
            $target.slideUp(300, function(){
               $target.addClass("collapse");
            });
         }
      });

      $('#livehelp-body').click(function(){
         $('#livehelp').trigger("chatBox.focused");
      });
      
      <?php if ($setting['smiley_status']) { ?>
      $('.lh-smiley-plugin').popover({
         placement: 'top',
         html: true,
         content: $('#lh-smile-list').html()
      }).on("show.bs.popover", function() {
         $('.lh-bbcode-plugin').popover('hide');
      });
      <?php } ?>
      <?php if ($setting['bbcode_status']) { ?>
      $('.lh-bbcode-plugin').popover({
         placement: 'top',
         html: true,
         content: $('#lh-bbcode-list').html()
      }).on("show.bs.popover", function() {
         $('.lh-smiley-plugin').popover('hide');
      });
      <?php } ?>
      LivehelpChat.init({
         timeout: <?php echo $setting['store_refresh_rate']; ?>,
         textMaxLength: <?php echo $setting['text_max_length']; ?>,
         ajaxUrl: "index.php?route=module/livehelp/operation",
         soundAlert: {
            newMessage: <?php echo ($setting['store_sound_new_message'] ? "'" . $setting['store_sound_new_message'] . "'" : "null"); ?>,
            messageSent: <?php echo ($setting['store_sound_message_sent'] ? "'" . $setting['store_sound_message_sent'] . "'" : "null"); ?>,
            operatorJoin: <?php echo ($setting['store_sound_operator_logged'] ? "'" . $setting['store_sound_operator_logged'] . "'" : "null"); ?>,
            operatorLeave: <?php echo ($setting['store_sound_operator_logout'] ? "'" . $setting['store_sound_operator_logout'] . "'" : "null"); ?>,
            special: <?php echo ($setting['store_sound_special'] ? "'" . $setting['store_sound_special'] . "'" : "null"); ?>
         },
         lastReaded: <?php echo $last_readed; ?>,
         isOnline: 1,
         languageId: <?php echo (int)$language_id; ?>,
         languageCode: '<?php echo $language_code; ?>',
         antiSpam: <?php echo (int)$setting['spam_filter_status']; ?>,
         antiSpam_scoreLimit: <?php echo (int)$setting['spam_filter_score_limit']; ?>,
         antiSpam_penalty: <?php echo (int)$setting['spam_filter_penalty']; ?>,
         antiSpam_range: <?php echo (int)$setting['spam_filter_range']; ?>,
      },
      <?php echo json_encode($js_language); ?>
      );
   });
</script>