<div id="livehelp" class="lh-widget box">
   <!-- WIDGET HEAD -->
   <div id="livehelp-head" class="lh-widget-head box-heading">
      <div class="lh-head-left">
         <h5>
            <i class="icon-bubbles"></i> <span><?php echo $heading_title; ?></span>
         </h5>
      </div>
      <div style="clear: both;"></div>
   </div>
   <!-- WIDGET BODY -->
   <div id="livehelp-body" class="lh-widget-body collapse box-content">
      <div class="lh-login-form">
         <form id="livehelp-form" role="form" class="form-horizontal group-border-dashed">
            <p>
               <?php echo $text_description; ?>
            </p>
            <b><?php echo $button_select_operator; ?> </b><br>
            <select name="livehelp_operator_id" class="lh-form-element">
               <?php foreach ($operators as $operator) { ?>
               <?php if($default_operator_id == $operator['operator_id']) { ?>
               <option value="<?php echo $operator['operator_id']; ?>" selected="selected"><?php echo $operator['name']; ?></option>
               <?php } else { ?>
               <option value="<?php echo $operator['operator_id']; ?>"><?php echo $operator['name']; ?></option>
               <?php } ?>  
               <?php } ?>
            </select>
            <br>
            <b><?php echo $entry_livehelp_name; ?></b><br>
            <input type="text" class="lh-form-element" id="livehelp_name" name="livehelp_name" value="<?php echo $user_name; ?>" placeholder="<?php echo $placeholder_livehelp_name; ?>">
            <br>
            <div style="text-align:center;">
               <div class="lh-preloader"><img src="catalog/view/theme/default/image/loading.gif" alt=""> <?php echo $text_wait; ?></div>
               <input value="<?php echo $button_sign; ?>" class="button lh-form-element lh-login-form-submit" type="button">
            </div>
         </form>
      </div>
   </div>
</div>
<script type="text/javascript">
   $(function() {
      $('#livehelp_name').on('keydown', function(e){
         if(e.keyCode == 13 || e.keyCode == 10) {
            e.preventDefault();
            $('.lh-login-form-submit').trigger('click');
         }
      });
   
      $('#livehelp-head').bind("click", function(){
         var $target = $('#livehelp-body');
         //if($target)
         if( $target.hasClass("collapse") ) {
            $target.slideDown(300, function(){
               $target.removeClass("collapse");
            });
         } else { 
            $target.slideUp(300, function(){
               $target.addClass("collapse");
            });
         }
      });
   
      $('.lh-login-form-submit').bind("click", function(e) {
         e.preventDefault();
         var $t = $(this),
            $form = $('#livehelp-form');
         $.ajax({
            url: 'index.php?route=module/livehelp/operation',
            type: 'post',
            dataType: 'json',
            data: $form.serialize() + "&operation=signin",
            beforeSend: function() {
               $form.find(".error").remove();
               $t.prop("disabled", true);
               $('#livehelp-body .lh-preloader').fadeIn();
            },
            complete: function() {
               $t.prop("disabled", false);
               $('#livehelp-body .lh-preloader').fadeOut();
            },
            success: function(data) {
               if (data['error']) {
                  if (data['error']['name']) {
                     $('#livehelp_name').after('<span class="error">' + data['error']['name'] + '</span>');
                  }
                  if (data['error']['operator_id']) {
                     $('.lh-operator-selected').after('<span class="error">' + data['error']['operator_id'] + '</span>');
                  }
               }
               if (data['success']) {
                  location.reload();
               }
            }
         });
      });
   });
</script>