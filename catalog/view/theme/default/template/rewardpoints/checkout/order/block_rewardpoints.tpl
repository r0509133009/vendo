<div class="box_rewardpoints">
    <h2>
        <span class='icon'>
            <img src="media/rewardpoints/image/icon_gift.png" alt="reward points icon" style="width: 22px; vertical-align: middle" />
        </span>
        Points to redeem <input type="text" size="5" id="input-with-keypress"/>
    </h2>
    <div style="float:left;min-width: 99%;padding: 10px 0;">
        <?php if($this->customer->isLogged()){ ?>
        <div id="range" style="margin: 0 16px;"></div>
        <div style="clear: both"></div>
        <div style="margin: 40px 10px 19px 12px" class="box_check_use_maxium">
            <input type="checkbox" class="use_maxium" <?php echo (isset($this->session->data['points_to_checkout']) && $this->session->data['points_to_checkout'] == $max_redeem_point ? 'checked="checked"' : '')?> value="<?php echo $max_redeem_point?>"/><span>Use maximum (<?php echo number_format($max_redeem_point)?> points)</span>
        </div>
        <?php } else { ?>
        Please <a href="<?php echo $this->url->link("account/account", "", 'SSL')?>">LOGIN</a> to use reward points
        <?php } ?>
    </div>
</div>