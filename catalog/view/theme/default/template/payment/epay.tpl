<!-- Epay.bg payment gateway by Extensa Web Development - www.extensadev.com -->
<form action="<?php echo $action; ?>" method="post" id="payment"> <!-- epay form id 150123 -->
  <input type="hidden" name="PAGE" value="paylogin" />
  <input type="hidden" name="LANG" value="<?php echo $lang; ?>" />
  <input type="hidden" name="ENCODED" value="<?php echo $encoded; ?>" />
  <input type="hidden" name="CHECKSUM" value="<?php echo $checksum; ?>" />
  <input type="hidden" name="CHECKID" value="<?php echo $checksum; ?>0xf1303" />
  <input type="hidden" name="URL_OK" value="<?php echo $url_ok; ?>" />
  <input type="hidden" name="URL_CANCEL" value="<?php echo $url_cancel; ?>" />
</form>
<div class="buttons">
  <div class="right"><a id="button-confirm" class="button" onclick="$('#payment').submit();"><span><?php echo $button_confirm; ?></span></a></div>
</div>
<!-- Epay.bg payment gateway by Extensa Web Development - www.extensadev.com -->