<?php if($redirect){?>
<form id="xpayment_form" action="<?php echo $redirect; ?>" method="<?php echo $redirect_type;?>">
  <?php
    foreach($form_data as $name=>$value){
  ?>
    <input type="hidden" name="<?php echo $name?>" value="<?php echo $value?>" />
  <?php } ?>
</form>
<?php } ?>

<div class="xpayment-instruction"><?php echo html_entity_decode($xpayment_instruction)?></div>
 <div class="buttons">
    <div class="right">
     <input type="button" value="<?php echo $button_confirm; ?>" id="button-confirm" class="button" />
    </div>
 </div>
 
<script type="text/javascript"><!--
<?php if($redirect){ ?>
$('#button-confirm').bind('click', function() {
	 $("#xpayment_form").submit();
});
<?php } else { ?>
  
  $('#button-confirm').bind('click', function() {
	$.ajax({ 
		type: 'get',
		before:function(){$('body').css('cursor','progress');},
		url: 'index.php?route=payment/xpayment/confirm',
		success: function() {
			location = '<?php echo $continue; ?>';
		}		
	});
  });
  
<?php
 }
?>
//--></script> 

<style style="text/css">
.xpayment-instruction {
    padding: 5px 0;
}
</style>