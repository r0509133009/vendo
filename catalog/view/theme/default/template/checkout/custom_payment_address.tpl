<?php
// ***************************************************
//             Custom Registration Fields   
//       
// Author : Francesco Pisanò - francesco1279@gmail.com
//              
//                   www.leverod.com		
//               © All rights reserved	  
// ***************************************************


// Payment address registration for members (template loaded by ajax)
?>

<?php if ($addresses) { ?>

	<?php
	// if there is only one existing address and all input fields are hidden, we can't add a new address so we just skip this step
	if ( count($addresses) == 1 &&

		!$custom_register_member_enable_firstname	&&	
		!$custom_register_member_enable_lastname	&&
		!$custom_register_member_enable_company		&&
		( !(version_compare(VERSION, '1.5.3', '>=' ) && ( count($customer_groups) > 0 && ($company_id_display || $tax_id_display) ) ) || (version_compare(VERSION, '1.5.3', '<')) ) &&
		!$custom_register_member_enable_address_1	&&
		!$custom_register_member_enable_address_2	&&
		!$custom_register_member_enable_city		&&
		!$custom_register_member_enable_postcode	&&
		!$custom_register_member_enable_country		&&
		!$custom_register_member_enable_zone		
	) { 
	$address = reset($addresses); // shifts the array pointer to the first element and returns its value
	?>
		<input type="hidden" name="payment_address" value="existing" checked="checked" />

		<select name="address_id">
			<option value="<?php echo $address['address_id']; ?>" selected="selected" >
		</select>
		 
		<script type="text/javascript"><!--
		$('#button-payment-address').click();
		$('#payment-address').remove();
		//--></script>
		 
		 
	<?php } else { ?>

		<input type="radio" name="payment_address" value="existing" id="payment-address-existing" checked="checked" />
		<label for="payment-address-existing"><?php echo $text_address_existing; ?></label>
		
		<div id="payment-existing">
			<select name="address_id" style="width: 100%; margin-bottom: 15px;" size="5" >
			
			<?php $selected = ''; ?>
			<?php foreach ($addresses as $address) { ?>
				<?php 
				if ($address['address_id'] == $address_id) { 
					$selected = 'selected="selected"';
					
				} else { 
					$selected = '';
				}
				?>
				<option value="<?php echo $address['address_id']; ?>" <?php echo $selected; ?> >						
					<?php echo $address['firstname'];	?> 	
					<?php echo $address['lastname']; 	?>	<?php if ( !empty($address['firstname']) || !empty($address['lastname']) ) { ?>, <?php } ?>				
					<?php echo $address['address_1'];	?>	<?php if ( !empty($address['address_1']) ) { ?>, <?php } ?>
					<?php echo $address['city']; 		?>	<?php if ( !empty($address['city']) ) { ?>, <?php } ?>
					<?php echo $address['zone'];		?>	<?php if ( !empty($address['zone']) ) { ?>, <?php } ?>
					<?php echo $address['country'];		?>
				</option>

			<?php } ?>
		  </select>
	</div>
	<p>
		<input type="radio" name="payment_address" value="new" id="payment-address-new" />
		<label for="payment-address-new"><?php echo $text_address_new; ?></label>
	</p>
	<?php } ?>
	
<?php } ?>


<div id="payment-new" style="display: <?php echo ($addresses ? 'none' : 'block'); ?>;">
  

	<div style="display:none"> <?php // This div prevents error messages from being appended (and displayed) under hidden input fields ?>
  
	<?php // Set disabled fields to empty to avoid compatibility problems with other extensions ?>

	<input type="hidden" name="firstname"	value="" />
	<input type="hidden" name="lastname"	value="" />
	<input type="hidden" name="company"		value="" />
	<input type="hidden" name="address_1"	value="" />
	<input type="hidden" name="address_2"	value="" />
	<input type="hidden" name="city"		value="" />
	<input type="hidden" name="postcode"	value="" />
	<input type="hidden" name="country_id"	value="<?php echo $country_id; ?>" />
	<input type="hidden" name="zone_id"		value="" />
	</div>

  <table class="form">

    <?php if ($custom_register_member_enable_firstname) { ?>	
		<tr>
			<td class="label">
				<?php if ($custom_register_member_firstname_required) { ?> <span class="required">*</span> <?php } ?> 
				<?php echo $entry_firstname; ?><br />
			</td>
			<td><input type="text" name="firstname" value="" class="large-field" /></td>
		</tr>
	<?php } ?>
	
	
  	<?php if ($custom_register_member_enable_lastname) { ?>  
		<tr>
			<td class="label">
				<?php if ($custom_register_member_lastname_required) { ?> <span class="required">*</span> <?php } ?> 
				<?php echo $entry_lastname; ?><br />
			</td>
			<td><input type="text" name="lastname" value="" class="large-field" /></td>
		</tr>
	<?php } ?>	
	
	
	<?php if ($custom_register_member_enable_company) { ?>
		<tr>
			<td class="label">
				<?php if ($custom_register_member_company_required) { ?> <span class="required">*</span> <?php } ?>
				<?php echo $entry_company; ?><br />
			</td>
			<td><input type="text" name="company" value="" class="large-field" /></td>
		</tr>
	<?php } ?>
	
	
<?php // TAX ID AND COMPANY ID HAVE BEEN ADDED WITH Oc 1.5.3
	if ( version_compare(VERSION, '1.5.3', '>=') ) { ?>
	
	<?php if ( $company_id_display && count($customer_groups) > 0 ) {  ?>
	<tr>
	  <td class="label"><?php if ($company_id_required) { ?>
		<span class="required">*</span>
		<?php } ?>
		<?php echo $entry_company_id; ?></td>
	  <td><input type="text" name="company_id" value="" class="large-field" /></td>
	</tr>
	<?php } else { // post an empty value - bug fix for Oc <= 1.5.3 ?>
		<input type="hidden" name="company_id" value="" class="large-field" />
	<?php } ?>
	
	
	<?php if ( $tax_id_display && count($customer_groups) > 0 ) { ?>
	<tr>
	  <td class="label"><?php if ($tax_id_required) { ?>
		<span class="required">*</span>
		<?php } ?>
		<?php echo $entry_tax_id; ?></td>
	  <td><input type="text" name="tax_id" value="" class="large-field" /></td>
	</tr>
	<?php } else { // post an empty value - bug fix for Oc <= 1.5.3 ?>
		<input type="hidden" name="tax_id" value="" class="large-field" />
	<?php } ?>

	
<?php } ?>
	
	
    <?php if ($custom_register_member_enable_address_1) { ?>	
		<tr>
			<td class="label">
				<?php if ($custom_register_member_address_1_required) { ?> <span class="required">*</span> <?php } ?>
				<?php echo $entry_address_1; ?><br />
			</td>
			<td><input type="text" name="address_1" value="" class="large-field" /></td>
		</tr>
	<?php } ?>
	
	
	
    <?php if ($custom_register_member_enable_address_2) { ?>	
		<tr>
			<td class="label">
				<?php if ($custom_register_member_address_2_required) { ?> <span class="required">*</span> <?php } ?>
				<?php echo $entry_address_2; ?><br />
			</td>
			<td><input type="text" name="address_2" value="" class="large-field" /></td>
		</tr>
	<?php } ?>
	
	
	
	<?php if ($custom_register_member_enable_city) { ?>	
		<tr>
			<td class="label">
				<?php if ($custom_register_member_city_required) { ?> <span class="required">*</span> <?php } ?> 
				<?php echo $entry_city; ?><br />
			</td>
			<td><input type="text" name="city" value="" class="large-field" /></td>
		</tr>
	<?php } ?>
	
	

	
	<?php if ($custom_register_member_enable_postcode) { ?>	
		<tr>
			<td class="label">
				<?php if ($custom_register_member_postcode_required) { ?> 
					<span class="required">*</span> 
				<?php } else {?>
					<span class="postcode-required required">*</span>
				<?php } ?>
				<?php echo $entry_postcode; ?><br />
			</td>
			<td><input type="text" name="postcode" value="" class="large-field" /></td>
		</tr>
	<?php } elseif ($custom_register_member_enable_country || $country_disabled_zone_enabled) { ?>
		<tr class="postcode-required">
			<td class="label"><span class="required">*</span> <?php echo $entry_postcode; ?></td>
			<td><input id="empty_if_hidden" type="text" name="postcode" value="" class="large-field" /></td>
		</tr>
	<?php } ?>
	

	
	<?php if ($custom_register_member_enable_country) { ?>	
		<tr>
			<td class="label">
				<?php if ($custom_register_member_country_required) { ?> <span class="required">*</span> <?php } ?> 
				<?php echo $entry_country; ?><br />
			</td>
			<td>
				<select name="country_id" class="large-field" >
				<option value=""><?php echo $text_select; ?></option>
				<?php foreach ($countries as $country) { ?>
				<?php if ($country['country_id'] == $country_id) { ?>
				<option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
				<?php } else { ?>
				<option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
				<?php } ?>
				<?php } ?>
				</select>
				<br />
			</td>
		</tr>
	<?php } ?>
	
	
	
	<?php if ($custom_register_member_enable_zone) { ?>	
		<tr>
			<td class="label">
				<?php if ($custom_register_member_zone_required) { ?> <span class="required">*</span> <?php } ?> 
				<?php echo $entry_zone; ?><br />
			</td>
			<td>
				<select name="zone_id" class="large-field"></select><br />
			</td>
		</tr>
	<?php } ?>
			
</table>
</div>


<br />
<div class="buttons">
	<div class="right">
		<?php 	// On Oc <= 1.5.1.3.1 <input> buttons were <a> links
		if ( version_compare(VERSION, '1.5.1.3.1', '<=') ) { ?>
		
			<a id="button-payment-address" class="button"><span><?php echo $button_continue; ?></span></a>
		<?php } else { ?>
			<input type="button" value="<?php echo $button_continue; ?>" id="button-payment-address" class="button" /> 
		<?php } ?>
	</div>
</div>

<script type="text/javascript"><!--
$('#payment-address input[name=\'payment_address\']').live('change', function() {
	if (this.value == 'new') {
		$('#payment-existing').hide();
		$('#payment-new').show();
	} else {
		$('#payment-existing').show();
		$('#payment-new').hide();
	}
});

country_select = $('#payment-address select[name="country_id"]');

//--></script> 



<?php require_once(DIR_TEMPLATE.'default/template/crf_include/crf_load_zones.tpl'); ?>
