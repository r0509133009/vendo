<?php
// ***************************************************
//             Custom Registration Fields   
//       
// Author : Francesco Pisanò - francesco1279@gmail.com
//              
//                   www.leverod.com		
//               © All rights reserved	  
// ***************************************************
?>


<div class="clearfix">
	<div class="left">
		<h2><?php echo $text_new_customer; ?></h2>
		<p><?php echo $text_checkout; ?></p>
		<label for="register">
		<?php if ($account == 'register') { ?>
		<input type="radio" name="account" value="register" id="register" checked="checked" />
		<?php } else { ?>
		<input type="radio" name="account" value="register" id="register" />
		<?php } ?>
		<b><?php echo $text_register; ?></b></label>
		<br />
		<?php if ($guest_checkout) { ?>
		<label for="guest">
		<?php if ($account == 'guest') { ?>
		<input type="radio" name="account" value="guest" id="guest" checked="checked" />
		<?php } else { ?>
		<input type="radio" name="account" value="guest" id="guest" />
		<?php } ?>
		<b><?php echo $text_guest; ?></b></label>
		<br />
		<?php } ?>
		<br />
		<p><?php echo $text_register_account; ?></p>
		
		<br />
		<div class="buttons" >
			<div class="right" >
				<input type="button" value="<?php echo $button_continue; ?>" id="button-account" class="button" />
			</div>
		</div>	
			
		<br />
	</div>
	
	<div id="login" class="left">
			<h2><?php echo $text_returning_customer; ?></h2>
			<p><?php echo $text_i_am_returning_customer; ?></p>
			<b><?php echo $entry_email; ?></b><br />
			
			<input type="text" name="email" value="" />
			<br />
			<br />
			<b><?php echo $entry_password; ?></b><br />
			<input type="password" name="password" value="" />
			<br />
			<br />
			<a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
			<br />
			<div class="buttons" >
				<div class="right" >
					<input type="button" value="<?php echo $button_login; ?>" id="button-login" class="button" />
				</div>
			</div>
			
			<br />
	</div>
</div>



<script type="text/javascript"><!--
// Login if press enter button 
$('#login input').keydown(function(e) {
	if (e.keyCode == 13) {
		$('#button-login').click();
	}
});
//--></script>   
