<?php
// ***************************************************
//             Custom Registration Fields   
//       
// Author : Francesco Pisanò - francesco1279@gmail.com
//              
//                   www.leverod.com		
//               © All rights reserved	  
// ***************************************************
?>



<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($payment_methods) { ?>
<p><?php echo $text_payment_method; ?></p>
<table class="radio">
  <?php foreach ($payment_methods as $payment_method) { ?>
  <tr class="highlight">
    <td><?php if ($payment_method['code'] == $code || !$code) { ?>
      <?php $code = $payment_method['code']; ?>
      <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" id="<?php echo $payment_method['code']; ?>" checked="checked" />
      <?php } else { ?>
      <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" id="<?php echo $payment_method['code']; ?>" />
      <?php } ?></td>
    <td><label for="<?php echo $payment_method['code']; ?>"><?php echo $payment_method['title']; ?></label></td>
  </tr>
  <?php } ?>
</table>
<br />
<?php } ?>
<div class="comment"><?php echo $text_comments; ?></div>
<textarea name="comment" rows="8" style="width: 98%;"><?php echo $comment; ?></textarea>
<br />
<br />
<?php if ($text_agree) { ?>
<div class="buttons">
  <div class="right"><?php echo $text_agree; ?>
    <?php if ($agree) { ?>
    <input type="checkbox" name="agree" value="1" checked="checked" />
    <?php } else { ?>
    <input type="checkbox" name="agree" value="1" />
    <?php } ?>
	
	<?php 	// On Oc <= 1.5.1.3.1 <input> buttons were <a> links
	if ( version_compare(VERSION, '1.5.1.3.1', '<=') ) { ?>
	
		<a id="button-payment-method" class="button"><span><?php echo $button_continue; ?></span></a>
	<?php } else { ?>
		<input type="button" value="<?php echo $button_continue; ?>" id="button-payment-method" class="button" /> 
	<?php } ?>
		
  </div>
</div>
<?php } else { ?>

<div class="buttons">
	<div class="right">
		<?php 	// On Oc <= 1.5.1.3.1 <input> buttons were <a> links
		if ( version_compare(VERSION, '1.5.1.3.1', '<=') ) { ?>
		
			<a id="button-payment-method" class="button"><span><?php echo $button_continue; ?></span></a>
		<?php } else { ?>
			<input type="button" value="<?php echo $button_continue; ?>" id="button-payment-method" class="button" /> 
		<?php } ?>
	</div>
</div>

<?php } ?>


<?php 
// fancybox script hasn't added in the header on Oc <= 1.5.0.1  :(
if ( version_compare(VERSION, '1.5.0.1', '<=') ) { ?>
<script type="text/javascript" src="catalog/view/javascript/jquery/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/fancybox/jquery.fancybox-1.3.4.css" media="screen" />  
<?php } ?>

<?php 
// colorbox replaced fancybox from Oc 1.5.2
if ( version_compare(VERSION, '1.5.2', '>=') ) { ?>

	<script type="text/javascript"><!--
	$(document).ready(function() {
		if ($.isFunction($.colorbox)) { // check if the function colorbox exists
			$('.colorbox').colorbox({
				width: 640,
				height: 480
			});
		}
	});
	//--></script> 
<?php } else { // for Oc <= 1.5.1.3.1 ?>

	<script type="text/javascript"><!--
	$(document).ready(function() {
		if ($.isFunction($.fancybox)) { // check if the function fancybox exists
			$('.fancybox').fancybox({
				width: 560,
				height: 560,
				autoDimensions: false
			});
		}
	});
	//--></script>  
<?php } ?>