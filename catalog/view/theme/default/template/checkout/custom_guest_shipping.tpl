<?php
// ***************************************************
//             Custom Registration Fields   
//       
// Author : Francesco Pisanò - francesco1279@gmail.com
//              
//                   www.leverod.com		
//               © All rights reserved	  
// ***************************************************


// Shipping address form for a guest user (template loaded by ajax)
?>

<div style="display:none"> <?php // This div prevents error messages from being appended (and displayed) under hidden input fields ?>

<?php // Set disabled fields to empty to avoid compatibility problems with other extensions ?>

<input type="hidden" name="firstname"	value="" />
<input type="hidden" name="lastname"	value="" />
<input type="hidden" name="company"		value="" />
<input type="hidden" name="address_1"	value="" />
<input type="hidden" name="address_2"	value="" />
<input type="hidden" name="city"		value="" />
<input type="hidden" name="postcode"	value="" />
<input type="hidden" name="country_id"	value="<?php echo $country_id; ?>" />
<input type="hidden" name="zone_id"		value="" />

</div>

<table class="form">

	<?php if ($custom_register_guest_enable_firstname) { ?>	
		<tr>
			<td>
				<?php if ($custom_register_guest_firstname_required) { ?> <span class="required">*</span> <?php } ?> 
				<?php echo $entry_firstname; ?><br />
			</td>
			<td><input type="text" name="firstname" value="<?php echo $firstname; ?>" class="large-field" /></td>
		</tr>
	<?php } ?>	
  
  
	<?php if ($custom_register_guest_enable_lastname) { ?>  
		<tr>
			<td>
				<?php if ($custom_register_guest_lastname_required) { ?> <span class="required">*</span> <?php } ?> 
				<?php echo $entry_lastname; ?><br />
			</td>
			<td><input type="text" name="lastname" value="<?php echo $lastname; ?>" class="large-field" /></td>
		</tr>
	<?php } ?>	
		
		
	<?php if ($custom_register_guest_enable_company) { ?>
		<tr>
			<td>
				<?php if ($custom_register_guest_company_required) { ?> <span class="required">*</span> <?php } ?>
				<?php echo $entry_company; ?><br />
			</td>
			<td><input type="text" name="company" value="<?php echo $company; ?>" class="large-field" /></td>
		</tr>
	<?php } ?>
	
	
	<?php if ($custom_register_guest_enable_address_1) { ?>	
		<tr>
			<td>
				<?php if ($custom_register_guest_address_1_required) { ?> <span class="required">*</span> <?php } ?>
				<?php echo $entry_address_1; ?><br />
			</td>
			<td><input type="text" name="address_1" value="<?php echo $address_1; ?>" class="large-field" /></td>
		</tr>
	<?php } ?>
		
		
	<?php if ($custom_register_guest_enable_address_2) { ?>	
		<tr>
			<td>
				<?php if ($custom_register_guest_address_2_required) { ?> <span class="required">*</span> <?php } ?>
				<?php echo $entry_address_2; ?><br />
			</td>
			<td><input type="text" name="address_2" value="<?php echo $address_2; ?>" class="large-field" /></td>
		</tr>
	<?php } ?>

	
	<?php if ($custom_register_guest_enable_city) { ?>	
		<tr>
			<td>
				<?php if ($custom_register_guest_city_required) { ?> <span class="required">*</span> <?php } ?> 
				<?php echo $entry_city; ?><br />
			</td>
			<td><input type="text" name="city" value="<?php echo $city; ?>" class="large-field" /></td>
		</tr>
	<?php } ?>
	
	
	<?php if ($custom_register_guest_enable_postcode) { ?>	
		<tr>
			<td>
				<?php if ($custom_register_guest_postcode_required) { ?> 
					<span class="required">*</span> 
				<?php } else {?>
					<span class="postcode-required" class="required">*</span>
				<?php } ?>
				<?php echo $entry_postcode; ?><br />
			</td>
			<td><input type="text" name="postcode" value="<?php echo $postcode; ?>" class="large-field" /></td>
		</tr>
	<?php } elseif ($custom_register_guest_enable_country || $country_disabled_zone_enabled) { ?>
		<tr class="postcode-required">
			<td><span class="required">*</span> <?php echo $entry_postcode; ?></td>
			<td><input id="empty_if_hidden" type="text" name="postcode" value="<?php echo $postcode; ?>" class="large-field" /></td>
		</tr>
	<?php } ?>
	
	
	<?php if ($custom_register_guest_enable_country) { ?>	
		<tr>
			<td>
				<?php if ($custom_register_guest_country_required) { ?> <span class="required">*</span> <?php } ?> 
				<?php echo $entry_country; ?><br />
			</td>
			<td>
			<select name="country_id" class="large-field" >
			<option value=""><?php echo $text_select; ?></option>
			<?php foreach ($countries as $country) { ?>
			<?php if ($country['country_id'] == $country_id) { ?>
			<option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
			<?php } else { ?>
			<option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
			<?php } ?>
			<?php } ?>
			</select>
			<br />
			</td>
		</tr>
	<?php } ?>
	

 	<?php if ($custom_register_guest_enable_zone) { ?>	
		<tr>
			<td>
				<?php if ($custom_register_guest_zone_required) { ?> <span class="required">*</span> <?php } ?> 
				<?php echo $entry_zone; ?><br />
			</td>
			<td>
				<select name="zone_id" class="large-field"></select><br />
			</td>
		</tr>
	<?php } ?>
  
  
</table>

<br />
<div class="buttons">
	<div class="right">
		<?php 	// On Oc <= 1.5.1.3.1 <input> buttons were <a> links
		if ( version_compare(VERSION, '1.5.1.3.1', '<=') ) { ?>
		
			<a id="button-guest-shipping" class="button"><span><?php echo $button_continue; ?></span></a>
		<?php } else { ?>
			<input type="button" value="<?php echo $button_continue; ?>" id="button-guest-shipping" class="button" />
		<?php } ?>
	</div>
</div>

<script type="text/javascript"><!--
country_select = $('#shipping-address select[name="country_id"]');
//--></script> 

<?php require_once(DIR_TEMPLATE.'default/template/crf_include/crf_load_zones.tpl'); ?>
