<?php
// ***************************************************
//             Custom Registration Fields   
//       
// Author : Francesco Pisanò - francesco1279@gmail.com
//              
//                   www.leverod.com		
//               © All rights reserved	  
// ***************************************************


// Registration page template - checkout
?>


<div style="display:none"> <?php // This div prevents error messages from being appended (and displayed) under hidden input fields ?>

<?php // Set disabled fields to empty to avoid compatibility problems with other extensions ?>

<input type="hidden" name="firstname"	value="" />
<input type="hidden" name="lastname"	value="" />
<input type="hidden" name="telephone"	value="" />
<input type="hidden" name="fax"			value="" />
<input type="hidden" name="company"		value="" />
<input type="hidden" name="address_1"	value="" />
<input type="hidden" name="address_2"	value="" />
<input type="hidden" name="city"		value="" />
<input type="hidden" name="postcode"	value="" />
<input type="hidden" name="country_id"	value="<?php echo $country_id; ?>" />
<input type="hidden" name="zone_id"		value="" />

<input type="hidden" name="newsletter"	value="0" />

<input type="hidden" name="company_id" value=""  />
<input type="hidden" name="tax_id" value=""  />


</div>

<div class="columns_container">
	<div class="left">
		<?php if ($custom_register_member_enable_firstname || $custom_register_member_enable_lastname || $custom_register_member_enable_telephone || $custom_register_member_enable_fax ) { ?>

			<h2><?php echo $text_your_details; ?></h2>

			<?php if ($custom_register_member_enable_firstname) { ?>	
				<?php if ($custom_register_member_firstname_required) { ?> <span class="required">*</span> <?php } ?>
				<?php echo $entry_firstname; ?><br />
				<input type="text" name="firstname" value="" class="large-field" />
				<br /><br />
			<?php } ?>	

			
			<?php if ($custom_register_member_enable_lastname) { ?>  
				<?php if ($custom_register_member_lastname_required) { ?> <span class="required">*</span> <?php } ?>
				<?php echo $entry_lastname; ?><br />
				<input type="text" name="lastname" value="" class="large-field" />
				<br /><br />
			<?php } ?>		

			
			<span class="required">*</span>
			<?php echo $entry_email; ?><br />
			<input type="text" name="email" value="" class="large-field" />
			<br /><br />

			
			<?php if ($custom_register_member_enable_telephone) { ?>	
				<?php if ($custom_register_member_telephone_required) { ?> <span class="required">*</span> <?php } ?>
				<?php echo $entry_telephone; ?><br />
				<input type="text" name="telephone" value="" class="large-field" />
				<br /><br />
			<?php } ?>

			
			<?php if ($custom_register_member_enable_fax) { ?>
				<?php if ($custom_register_member_fax_required) { ?> <span class="required">*</span> <?php } ?>
				<?php echo $entry_fax; ?><br />
				<input type="text" name="fax" value="" class="large-field" />
				<br /><br />
			<?php } ?> 
			
		<?php }
		 else { ?>	
		
			<span class="required">*</span>
			<?php echo $entry_email; ?><br />
			<input type="text" name="email" value="" class="large-field" />
			<br /><br />
		
		<?php } ?>
	  

		<h2><?php echo $text_your_password; ?></h2>
		<span class="required">*</span>
		<?php echo $entry_password /* .' '.$text_min_4_chars ; */?><br />
		<input type="password" name="password" value="" class="large-field" />
		<br /><br />


		<?php if ($custom_register_member_enable_confirm) { ?>
			<span class="required">*</span>
			<?php echo $entry_confirm; ?> <br />
			<input type="password" name="confirm" value="" class="large-field" />
			<br /><br /><br />
		<?php } ?>
		
		
	</div> <!-- end left -->





	<div class="left">

		<?php if ($custom_register_member_enable_company || $custom_register_member_enable_address_1 || $custom_register_member_enable_address_2 || $custom_register_member_enable_city || $custom_register_member_enable_country || $custom_register_member_enable_postcode || $custom_register_member_enable_zone ) { ?>
			<h2><?php echo $text_your_address; ?></h2>
		<?php } ?>
		
		<?php if ($custom_register_member_enable_company) { ?>
			<?php if ($custom_register_member_company_required) { ?> <span class="required">*</span> <?php } ?>
			<?php echo $entry_company; ?><br />
			<input type="text" name="company" value="" class="large-field" />
			<br /><br />
		<?php } ?>

		<?php // CUSTOMER GROUPS, TAX ID AND COMPANY ID HAVE BEEN ADDED WITH Oc 1.5.3
		if ( version_compare(VERSION, '1.5.3', '>=') ) { ?>
		
			<div style="display: <?php echo (count($customer_groups) > 1 ? 'table-row' : 'none'); ?>;">
			<?php echo $entry_customer_group; ?><br />
			<?php foreach ($customer_groups as $customer_group) { ?>
				<?php if ($customer_group['customer_group_id'] == $customer_group_id) { ?>
					<input type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" id="customer_group_id<?php echo $customer_group['customer_group_id']; ?>" checked="checked" />
					<label for="customer_group_id<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></label>
					<br />
				<?php } else { ?>
					<input type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" id="customer_group_id<?php echo $customer_group['customer_group_id']; ?>" />
					<label for="customer_group_id<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></label>
					<br />
				<?php } ?>
			<?php } ?>
			<br />
			</div>

			<?php if( count($customer_groups) > 0) { ?>
				<div id="company-id-display"><span id="company-id-required" class="required">*</span> <?php echo $entry_company_id; ?><br />
					<input type="text" name="company_id" value="" class="large-field" />
					<br /><br />
				</div>


				<div id="tax-id-display"><span id="tax-id-required" class="required">*</span> <?php echo $entry_tax_id; ?><br />
					<input type="text" name="tax_id" value="" class="large-field" />
					<br /><br />
				</div>	
			<?php } ?>
		
		<?php } ?>

		
		<?php if ($custom_register_member_enable_address_1) { ?>	
			<?php if ($custom_register_member_address_1_required) { ?> <span class="required">*</span> <?php } ?>
			<?php echo $entry_address_1; ?><br />
			<input type="text" name="address_1" value="" class="large-field" />
			<br /><br />
		<?php } ?>
		
		
		<?php if ($custom_register_member_enable_address_2) { ?>	
			<?php if ($custom_register_member_address_2_required) { ?> <span class="required">*</span> <?php } ?>
			<?php echo $entry_address_2; ?><br />
			<input type="text" name="address_2" value="" class="large-field" />
			<br /><br />
		<?php } ?>
			
			
		<?php if ($custom_register_member_enable_city) { ?>	
			<?php if ($custom_register_member_city_required) { ?> <span class="required">*</span> <?php } ?> 
			<?php echo $entry_city; ?><br />
			<input type="text" name="city" value="" class="large-field" />
			<br /><br />
		<?php } ?>
		
		
		
		
		<?php if ($custom_register_member_enable_postcode) { ?>	

			<?php if ($custom_register_member_postcode_required) { ?> 
				<span class="required">*</span> 
			<?php } else {?>
				<span class="postcode-required required">*</span>
			<?php } ?>
			<?php echo $entry_postcode; ?><br />
		
		<input type="text" name="postcode" value="<?php echo $postcode; ?>" class="large-field" />
		<br /><br />

		<?php } elseif ($custom_register_member_enable_country || $country_disabled_zone_enabled) { ?>
			<span class="postcode-required">
				<span class="required">*</span> <?php echo $entry_postcode; ?><br />
				<input id="empty_if_hidden" type="text" name="postcode" value="<?php echo $postcode; ?>" class="large-field" />		
				<br /><br />
			</span>	
		<?php } ?>
		

		
		
		<?php if ($custom_register_member_enable_country) { ?>	
			<?php if ($custom_register_member_country_required) { ?> <span class="required">*</span> <?php } ?> 
			<?php echo $entry_country; ?><br />
			<select name="country_id" class="large-field" >
				<option value=""><?php echo $text_select; ?></option>
				<?php foreach ($countries as $country) { ?>
				<?php if ($country['country_id'] == $country_id) { ?>
				<option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
				<?php } else { ?>
				<option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
				<?php } ?>
				<?php } ?>
				</select>
			<br /><br />
		<?php } ?>
		
		
		
		<?php if ($custom_register_member_enable_zone) { ?>	
			<?php if ($custom_register_member_zone_required) { ?> <span class="required">*</span> <?php } ?> 
			<?php echo $entry_zone; ?><br />
			<select name="zone_id" class="large-field">
			</select>
			<br /><br /><br />
		<?php } ?>
		
	</div>

</div> <!-- end columns container -->


<?php if ($custom_register_member_enable_newsletter || $custom_register_member_enable_shipping_address ) { ?>
	<div style="clear: both; padding-top: 15px; border-top: 1px solid #EEEEEE;">
<?php } ?>
    
	<?php if ($custom_register_member_enable_newsletter) { ?>
	
	  	<?php if ($newsletter_checked) { ?>
			<input type="checkbox" name="newsletter" value="1" id="newsletter" checked="checked" />
			<?php } else { ?>
			<input type="checkbox" name="newsletter" value="1" id="newsletter" />
		<?php } ?>	
	
		<label for="newsletter"><?php echo $entry_newsletter; ?></label>
		<br />
	<?php } ?>
	  
	  

	<?php if ($shipping_required) { ?>

		<?php if (!$custom_register_member_enable_shipping_address) { ?> 
				<?php // to avoid issues with old browsers (input fields not sent if the container is styled with a "display:none") 
					  // we hide the div with the "visibility" attribute and set the height to 0) ?>
			<div style="visibility:hidden; height:0px"> 
		<?php } ?>
		
		
		<?php if ($shipping_address) { ?>
			<input type="checkbox" name="shipping_address" value="1" id="shipping" checked="checked" />
			<?php } else { ?>
			<input type="checkbox" name="shipping_address" value="1" id="shipping" />
		<?php } ?>	

			<label for="shipping"><?php echo $entry_shipping; ?></label>
			<br />
			
		<?php if (!$custom_register_member_enable_shipping_address) { ?>
			</div> 
		<?php } ?>
	<?php } ?>
	<br />
	<br />
  
<?php if ($custom_register_member_enable_newsletter || $custom_register_member_enable_shipping_address ) { ?>    
	</div>
<?php } ?>



<?php if ($text_agree) { ?>
<div class="buttons">
  <div class="right">

			<?php echo $text_agree; ?>
			
  			<?php if ($agree) { ?>
			<input type="checkbox" name="agree" value="1" checked="checked" />
			<?php } else { ?>
			<input type="checkbox" name="agree" value="1" />
			<?php } ?>	
  
  
		<?php 	// On Oc <= 1.5.1.3.1 <input> buttons were <a> links
		if ( version_compare(VERSION, '1.5.1.3.1', '<=') ) { ?>		
			<a id="button-register" class="button"><span><?php echo $button_continue; ?></span></a>
		<?php } else { ?>
			<input type="button" value="<?php echo $button_continue; ?>" id="button-register" class="button" />
		<?php } ?>
  </div>
</div>
<?php } else { ?>
<div class="buttons">
  <div class="right">
  
		<?php 	// On Oc <= 1.5.1.3.1 <input> buttons were <a> links
		if ( version_compare(VERSION, '1.5.1.3.1', '<=') ) { ?>		
			<a id="button-register" class="button"><span><?php echo $button_continue; ?></span></a>
		<?php } else { ?>
			<input type="button" value="<?php echo $button_continue; ?>" id="button-register" class="button" />
		<?php } ?>
		
  </div>
</div>
<?php } ?>




<?php if ($custom_register_enable_antispam) { ?>
	<div class="kill">
		<label for="toomanysecrets"></label>
		<input type="text" size="1" id="toomanysecrets" name="phone800" value="" />
	</div>
	<style>div.kill, div.kill label, div.kill input {visibility:hidden;display:none;}</style> 
	<script type="text/javascript"><!-- 
		$(document).ready(function() { $('.kill label').append('<?php echo $entry_anti_spam_label ?>');}); 
	//--></script> 
<?php } ?>



<?php // CUSTOMER GROUPS, TAX ID AND COMPANY ID HAVE BEEN ADDED WITH Oc 1.5.3
if ( version_compare(VERSION, '1.5.3', '>=') ) { ?>

	<script type="text/javascript"><!--
	$('#payment-address input[name=\'customer_group_id\']:checked').live('change', function() {
		var customer_group = [];
		
	<?php foreach ($customer_groups as $customer_group) { ?>
		customer_group[<?php echo $customer_group['customer_group_id']; ?>] = [];
		customer_group[<?php echo $customer_group['customer_group_id']; ?>]['company_id_display'] = '<?php echo $customer_group['company_id_display']; ?>';
		customer_group[<?php echo $customer_group['customer_group_id']; ?>]['company_id_required'] = '<?php echo $customer_group['company_id_required']; ?>';
		customer_group[<?php echo $customer_group['customer_group_id']; ?>]['tax_id_display'] = '<?php echo $customer_group['tax_id_display']; ?>';
		customer_group[<?php echo $customer_group['customer_group_id']; ?>]['tax_id_required'] = '<?php echo $customer_group['tax_id_required']; ?>';
	<?php } ?>	

		if (customer_group[this.value]) {
			if (customer_group[this.value]['company_id_display'] == '1') {
				$('#company-id-display').show();
			} else {
				$('#company-id-display').hide();
			}
			
			if (customer_group[this.value]['company_id_required'] == '1') {
				$('#company-id-required').show();
			} else {
				$('#company-id-required').hide();
			}
			
			if (customer_group[this.value]['tax_id_display'] == '1') {
				$('#tax-id-display').show();
			} else {
				$('#tax-id-display').hide();
			}
		
			if (customer_group[this.value]['tax_id_required'] == '1') {
				$('#tax-id-required').show();
			} else {
				$('#tax-id-required').hide();
			}	
		}
	});

	$('#payment-address input[name=\'customer_group_id\']:checked').trigger('change');
	//--></script> 
<?php } ?>

<script type="text/javascript"><!--
country_select = $('#payment-address select[name="country_id"]');
//--></script> 
<?php require_once(DIR_TEMPLATE.'default/template/crf_include/crf_load_zones.tpl'); ?>


<?php 
// fancybox script has not been added in the Oc header <= 1.5.0.1  :(
if ( version_compare(VERSION, '1.5.0.1', '<=') ) { ?>
<script type="text/javascript" src="catalog/view/javascript/jquery/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/fancybox/jquery.fancybox-1.3.4.css" media="screen" />  
<?php } ?>

<?php 
// colorbox replaced fancybox from Oc 1.5.2
if ( version_compare(VERSION, '1.5.2', '>=') ) { ?>

	<script type="text/javascript"><!--
	$(document).ready(function() {
		if ($.isFunction($.colorbox)) { // check if the function colorbox exists
			$('.colorbox').colorbox({
				width: 640,
				height: 480
			});
		}
	});
	//--></script> 
<?php } else { // for Oc <= 1.5.1.3.1 ?>

	<script type="text/javascript"><!--
	$(document).ready(function() {
		if ($.isFunction($.fancybox)) { // check if the function fancybox exists
			$('.fancybox').fancybox({
				width: 560,
				height: 560,
				autoDimensions: false
			});
		}
	});
	//--></script>  
<?php } ?>