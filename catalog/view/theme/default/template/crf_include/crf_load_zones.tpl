<script type="text/javascript"><!--

if (typeof country_select === 'undefined') {
	country_select = $('select[name=\'country_id\']');
}

<?php if ( $country_disabled_zone_enabled ) { ?>

	var country_id = '<?php echo $country_id ?>';
	load_zones(country_id);	

<?php } else { ?>

	country_select.bind('change', function() {
		load_zones(this.value);	
	});
	
	load_zones(country_select.val());	
	
<?php } ?>
	
function load_zones(country_id) {

	if (country_id == '') return;
	
	$.ajax({
		url: 'index.php?route=account/register/country&country_id=' + country_id,
		dataType: 'json',
		beforeSend: function() {
			country_select.after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
		},
		complete: function() {
			$('.wait').remove();
		},			
		success: function(json) {
			
			if (json['postcode_required'] == '1') {
				$('.postcode-required').show();						
			} else {
				$('.postcode-required').hide();
				if ( typeof $('#empty_if_hidden') !== 'undefined' && $('#empty_if_hidden').length > 0 ) $('#empty_if_hidden').val('');
			//	if ( $('#empty_if_hidden').length > 0 ) $('#empty_if_hidden').val('');
			}
			html = '<option value=""><?php echo $text_select; ?></option>';
			
			if ( typeof json['zone'] !== 'undefined' && json['zone'] != '') {
		
				for (i = 0; i < json['zone'].length; i++) {
        			html += '<option value="' + json['zone'][i]['zone_id'] + '"';
	    			
					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
	      				html += ' selected="selected"';
	    			}
	
	    			html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}
			
			$('select[name=\'zone_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
}

//--></script>