<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
    <div id="content"><?php echo $content_top; ?>
		<div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
		  <h1><?php echo $heading_title; ?></h1>
		  <div class="srch">
			<div class="input-group text-right">
			  <input type="text" name="filtername" value="" class="form-control"/> <span class="input-group-btn">
				<a onclick="faqfilter();"  class="button"><?php echo $button_search; ?></a>
			  </span>
			</div>
		  </div>
		  <?php foreach($results as $keys => $result){ ?>
			<h3 class="faq_title"><?php echo $result['name']; ?></h3>
				<div class="panel-group accord" id="accordion">
				 <?php foreach($result['subfaqs'] as $key => $sub){ ?>
					<ul>
						<li class="expandd"><a href="<?php echo $sub['href'];  ?>" >
							  <?php echo $sub['name']; ?>
							</a>
						<ul class="sub">
							<li><div id="collapse<?php echo $key ?>-<?php echo $result['faq_id'];  ?>" class="panel-collapse collapse <?php if($key==0){ echo 'in'; } ?> " aria-expanded="true">
								<?php echo $sub['description']; ?>
								</div>
							</li>
						</ul>						
						</li>
					  </ul>
				  <?php } ?>
				</div>
			<?php } ?>
		<?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div></div></div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function ($) {
		$('.accord ul li:has(ul)').addClass('expand').find('ul').hide();
		$('.accord ul li.expand>a').before('<span> + </span>');

		$('.accord ul').on('click', 'li.collapse span ', function (e) {
			$(this).text(' + ').parent().addClass('expand').removeClass('collapse').find('>ul').slideUp();
			e.stopImmediatePropagation();
		});

		$('.accord ul').on('click', 'li.expand span', function (e) {
			$(this).text(' - ').parent().addClass('collapse').removeClass('expand').find('>ul').slideDown();
			e.stopImmediatePropagation();
		});

		$('.accord ul').on('click', 'li.collapse li:not(.collapse)', function (e) {
			e.stopImmediatePropagation();
		});		
	});
</script>
<script>
function faqfilter(){
  var search = $('input[name="filtername"]').val();
  url = 'index.php?route=information/faq/search';
  url += '&fsearch='+encodeURI(search);
  location =url; 
}
</script>
<script type="text/javascript"><!--
$('input[name="filtername"]').keydown(function(e) {
	if (e.keyCode == 13) {
		faqfilter();
	}
});
//--></script> 
<?php echo $footer; ?>
