<?php echo $header; ?>
<div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
</div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content">
  <?php if(isset($payment_method_content)){?>
  		<?php echo $payment_method_content; ?>
  		<div class="bottom-section">
  			<div class="text_section xs-100 sm-100 md-30 lg-30 xl-30">
  				<p><?php echo $text_bottom_thanks; ?></p>
  			</div>
  			<div class="images_section xs-100 sm-100 md-50 lg-50 xl-50">
  				<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAXoAAABWCAYAAADBuV1pAAAKN2lDQ1BzUkdCIElFQzYxOTY2LTIuMQAAeJydlndUU9kWh8+9N71QkhCKlNBraFICSA29SJEuKjEJEErAkAAiNkRUcERRkaYIMijggKNDkbEiioUBUbHrBBlE1HFwFBuWSWStGd+8ee/Nm98f935rn73P3Wfvfda6AJD8gwXCTFgJgAyhWBTh58WIjYtnYAcBDPAAA2wA4HCzs0IW+EYCmQJ82IxsmRP4F726DiD5+yrTP4zBAP+flLlZIjEAUJiM5/L42VwZF8k4PVecJbdPyZi2NE3OMErOIlmCMlaTc/IsW3z2mWUPOfMyhDwZy3PO4mXw5Nwn4405Er6MkWAZF+cI+LkyviZjg3RJhkDGb+SxGXxONgAoktwu5nNTZGwtY5IoMoIt43kA4EjJX/DSL1jMzxPLD8XOzFouEiSniBkmXFOGjZMTi+HPz03ni8XMMA43jSPiMdiZGVkc4XIAZs/8WRR5bRmyIjvYODk4MG0tbb4o1H9d/JuS93aWXoR/7hlEH/jD9ld+mQ0AsKZltdn6h21pFQBd6wFQu/2HzWAvAIqyvnUOfXEeunxeUsTiLGcrq9zcXEsBn2spL+jv+p8Of0NffM9Svt3v5WF485M4knQxQ143bmZ6pkTEyM7icPkM5p+H+B8H/nUeFhH8JL6IL5RFRMumTCBMlrVbyBOIBZlChkD4n5r4D8P+pNm5lona+BHQllgCpSEaQH4eACgqESAJe2Qr0O99C8ZHA/nNi9GZmJ37z4L+fVe4TP7IFiR/jmNHRDK4ElHO7Jr8WgI0IABFQAPqQBvoAxPABLbAEbgAD+ADAkEoiARxYDHgghSQAUQgFxSAtaAYlIKtYCeoBnWgETSDNnAYdIFj4DQ4By6By2AE3AFSMA6egCnwCsxAEISFyBAVUod0IEPIHLKFWJAb5AMFQxFQHJQIJUNCSAIVQOugUqgcqobqoWboW+godBq6AA1Dt6BRaBL6FXoHIzAJpsFasBFsBbNgTzgIjoQXwcnwMjgfLoK3wJVwA3wQ7oRPw5fgEVgKP4GnEYAQETqiizARFsJGQpF4JAkRIauQEqQCaUDakB6kH7mKSJGnyFsUBkVFMVBMlAvKHxWF4qKWoVahNqOqUQdQnag+1FXUKGoK9RFNRmuizdHO6AB0LDoZnYsuRlegm9Ad6LPoEfQ4+hUGg6FjjDGOGH9MHCYVswKzGbMb0445hRnGjGGmsVisOtYc64oNxXKwYmwxtgp7EHsSewU7jn2DI+J0cLY4X1w8TogrxFXgWnAncFdwE7gZvBLeEO+MD8Xz8MvxZfhGfA9+CD+OnyEoE4wJroRIQiphLaGS0EY4S7hLeEEkEvWITsRwooC4hlhJPEQ8TxwlviVRSGYkNimBJCFtIe0nnSLdIr0gk8lGZA9yPFlM3kJuJp8h3ye/UaAqWCoEKPAUVivUKHQqXFF4pohXNFT0VFysmK9YoXhEcUjxqRJeyUiJrcRRWqVUo3RU6YbStDJV2UY5VDlDebNyi/IF5UcULMWI4kPhUYoo+yhnKGNUhKpPZVO51HXURupZ6jgNQzOmBdBSaaW0b2iDtCkVioqdSrRKnkqNynEVKR2hG9ED6On0Mvph+nX6O1UtVU9Vvuom1TbVK6qv1eaoeajx1UrU2tVG1N6pM9R91NPUt6l3qd/TQGmYaYRr5Grs0Tir8XQObY7LHO6ckjmH59zWhDXNNCM0V2ju0xzQnNbS1vLTytKq0jqj9VSbru2hnaq9Q/uE9qQOVcdNR6CzQ+ekzmOGCsOTkc6oZPQxpnQ1df11Jbr1uoO6M3rGelF6hXrtevf0Cfos/ST9Hfq9+lMGOgYhBgUGrQa3DfGGLMMUw12G/YavjYyNYow2GHUZPTJWMw4wzjduNb5rQjZxN1lm0mByzRRjyjJNM91tetkMNrM3SzGrMRsyh80dzAXmu82HLdAWThZCiwaLG0wS05OZw2xljlrSLYMtCy27LJ9ZGVjFW22z6rf6aG1vnW7daH3HhmITaFNo02Pzq62ZLde2xvbaXPJc37mr53bPfW5nbse322N3055qH2K/wb7X/oODo4PIoc1h0tHAMdGx1vEGi8YKY21mnXdCO3k5rXY65vTW2cFZ7HzY+RcXpkuaS4vLo3nG8/jzGueNueq5clzrXaVuDLdEt71uUnddd457g/sDD30PnkeTx4SnqWeq50HPZ17WXiKvDq/XbGf2SvYpb8Tbz7vEe9CH4hPlU+1z31fPN9m31XfKz95vhd8pf7R/kP82/xsBWgHcgOaAqUDHwJWBfUGkoAVB1UEPgs2CRcE9IXBIYMj2kLvzDecL53eFgtCA0O2h98KMw5aFfR+OCQ8Lrwl/GGETURDRv4C6YMmClgWvIr0iyyLvRJlESaJ6oxWjE6Kbo1/HeMeUx0hjrWJXxl6K04gTxHXHY+Oj45vipxf6LNy5cDzBPqE44foi40V5iy4s1licvvj4EsUlnCVHEtGJMYktie85oZwGzvTSgKW1S6e4bO4u7hOeB28Hb5Lvyi/nTyS5JpUnPUp2Td6ePJninlKR8lTAFlQLnqf6p9alvk4LTduf9ik9Jr09A5eRmHFUSBGmCfsytTPzMoezzLOKs6TLnJftXDYlChI1ZUPZi7K7xTTZz9SAxESyXjKa45ZTk/MmNzr3SJ5ynjBvYLnZ8k3LJ/J9879egVrBXdFboFuwtmB0pefK+lXQqqWrelfrry5aPb7Gb82BtYS1aWt/KLQuLC98uS5mXU+RVtGaorH1futbixWKRcU3NrhsqNuI2ijYOLhp7qaqTR9LeCUXS61LK0rfb+ZuvviVzVeVX33akrRlsMyhbM9WzFbh1uvb3LcdKFcuzy8f2x6yvXMHY0fJjpc7l+y8UGFXUbeLsEuyS1oZXNldZVC1tep9dUr1SI1XTXutZu2m2te7ebuv7PHY01anVVda926vYO/Ner/6zgajhop9mH05+x42Rjf2f836urlJo6m06cN+4X7pgYgDfc2Ozc0tmi1lrXCrpHXyYMLBy994f9Pdxmyrb6e3lx4ChySHHn+b+O31w0GHe4+wjrR9Z/hdbQe1o6QT6lzeOdWV0iXtjusePhp4tLfHpafje8vv9x/TPVZzXOV42QnCiaITn07mn5w+lXXq6enk02O9S3rvnIk9c60vvG/wbNDZ8+d8z53p9+w/ed71/LELzheOXmRd7LrkcKlzwH6g4wf7HzoGHQY7hxyHui87Xe4Znjd84or7ldNXva+euxZw7dLI/JHh61HXb95IuCG9ybv56Fb6ree3c27P3FlzF3235J7SvYr7mvcbfjT9sV3qID0+6j068GDBgztj3LEnP2X/9H686CH5YcWEzkTzI9tHxyZ9Jy8/Xvh4/EnWk5mnxT8r/1z7zOTZd794/DIwFTs1/lz0/NOvm1+ov9j/0u5l73TY9P1XGa9mXpe8UX9z4C3rbf+7mHcTM7nvse8rP5h+6PkY9PHup4xPn34D94Tz+49wZioAAAAJcEhZcwAALiMAAC4jAXilP3YAACAASURBVHic7Z0HvBXF+f7HGmyJJZYoUTSW/IWo6LUnRiOixhqxgahExYZKAhJBiRLyM5aIDQtYr4oFNVEURIwFSwwqigUQO/YKdlFE+T/f3dnL3r27e3bP2XPOlTvP5zN398zOzs7u3X3mnXfe953F58+fbxwcHPKjZ8+ei2mzk9JvlW4fNWrUY3VukoNDLBavdwMcHH5oEMH/SJvDlPoprWuzByofoj9TaYxI30lQDq0GjugdHDJCRL6ENn9UGqz085giWyrdpvSUyg4W2Y+vZfscfljQO9JOmx8rLaM0R+kzvTNfVeNajugdHEpAH+Si2uyvNFRpvQynbKp0l857UNu/6ON9vJrtc2j90LuwvDbbK+2gtInS+kqrxZT7WJsXlJ5Rmqh0v96fDyq9viN6B4cU6MPranx1TOcyTkd3P0l13KLtyfpgXym0ca0Mus+ltPl/xiexDkrLmZC0qvSu0gylaXoWn9epmTWDFRB2VzpUaTelH2U4bQWlrWw6Suk71XOfttcp3aznNrectjiid3CIgT6uBuMT/I4VVrWI8UcDe6vOy7Q9XR/re5W2r7VA97SWNj2U6BAhp3YZTpun8yZre6/SjXoe06vYxJpD98b/nGdyivE7vkrAhH9Xm/6hus/WdqSe2bd5KnFE7+AQgiX4k5X2Nj5JF4UllY5T+qOuMVzbc/Wxflhg/TWDJbJdlQYYf9SS9znBO4HUOtiS/jClW/RMviuyrbWG7qWjNpcq/aYK1TMvxLtzpK5zjJ7Vf7Oe6Ijeoc3DEheENcj4klM1gSpjoNIJuu7VxpfOnqvyNQuD2gw5X6i0eYHV0rneqDRE9ffT87irwLprBrX9WON3WFlGNZXgV0oP6nqnazs0S+foiN6hTUIfyerabGcT+tM1a9yEpZX6kKxEe4cSutjH9eHOq3FbSsLq3yEx9MaLVukyGyiN07Vu5Tp6DrOrdJ1CYa2xrlA6pIaXRaVzqtLWun63UnMejugd2hT0USBRIz0ySVakaqYSNNiEVc9stfF8fbh/r3ObmqD2MLkK+f6qRpfcV2kLXXf/1u6EZjvAfxlflVUYFllkEZPRmRWHvfvVjl31rD5KKuSI3qGt4XilPerdiBSsqDRUH+4T+nDvrndj1A58A8Yq/bTGl2aEBYEdoOcwtsbXzgS1Df4cbQoi+aWXXtp069bNbLPNNmaZZZYxb7/9thk3bpx55JFHSp2KkIA5745Jkr0jeoe2hmpMklUDv1aqK9GLOII2LFOnJqDeuk3t6C4Cu7VObUgDE6OFCA0Q++DBg0379u3Nl19+acaOHWu6du1qjjrqKLP66qubm2++uVQVzJmM1rPaXc/q++hBR/QObQ2r1LsBGfGTel5chIGa5k5TP5IPAEeNUns+EYHdW+e2NIHOR5uji6qvR48eHsm/+OKL5oILLjDff/+9ueeee8zQoUPNHnvsYaZOnWqmTy9phcrIAoOC06MHHNE7tDW0Fr18KZTlGFMErBfnGKXl69WGCHA0ulnt2lRkP7PejVE71tBmRFH1LbfccubXv/61tz9lyhSzySabmO+++85stdVWTWV22WWXLEQPsFwar+f0VDjTEX0O9O/fD2uDlZU+GTbs3G/q3R6HspDL0aRSNDQ0eENwhuZPP/20GTNmjJk7NxOHf1zttqUAC5K163j9OOAxeqNI7DetwCrpXOPHqMmEJZZYwnz7bfJrt95665lFF/UNmTbddFNz1llnee/LxhtvbFZYYQUv/5e//GXWy8Hpl+g5bRNW4TiizwiR/H7anKdEbz5Hv9HPnSzC/0E7eLRBfFarC2299dbm2GOPbfq95pprmjXWWMOcf/75WU6vi/cspnradKvHtTMAEfcE4xNtXaDns7XxPZ0zo1evXub5559PnFRFog8A6Z999tnmnHPOMTfeeKOnr+/SpYtZaqmlzGKLLeZJ+hnABPqBSjcEGY7oM0Ckzj8Xk7zFbBYmVX8xPmm00IfVE506brj41GnT6y3xtGa8U4uL/OxnPzOHH354i/zNNtvMrLLKKuaDD0rGqXq3Kg1LgTU9Pa/W180JVBM3SVqtyf8xBqfkPQEzSSZVUclcffXVHnmH8fnnCwxlbr/9dk8/j1DQrl07M3u270owZ86crCQfYJB9Tp5U74g+G44xC0g+DBxeWgXRi+DpeHBJX0n7T2p7jAh/cp2b1RrxdrUvwFD9uOOOMz/6UXwMK4blGfByoY3KhiNNJPwyKgXuhZEItt0kCAdyamxs9EwAawzEX971P9X6wiJOHLp+n/e8r77yIw9vueWWZv311zcjR44006ZNM8suu6xZddVVPbNKOgOeLZ3Bueee63UGK6+8shkwYIB37owZM/JetpPxbewn8MMRfTYk6St/Jml/sXqrb0TsvbU5K5SFXe3dyl9fZP+D8C6sIZ6v9gVECJ5EFodPPvnEvPnmm6WqQKH7atHtSoPaTCyeE6P5SJWMQgIdchg77LCDkcRYi+ZF0Rv3/zrECjrYlDGZHxA9QOd+0kknmS+++KKZyiZAhw4dPKsbnuvBBx/clD9hwoRy2ounriP6HIiT5gO0BiuOnjF5Kxnftf+6GreltaOqcWWwlPjd736XePyKK64w8+aV1Ky9lDc6YQHgXVk9mglJnX766Z7kGQDiJ+HYg4qqKCDVvvPOO57NeIlnhH39QUqZJjsKxAF5T1hppZXMhhtu2CwPyT2O5MPHwyQPEB6uueaavJI9EVPb6V362hH9woEkm+taezP+EMCXQnz0pYqueLXVVovVywfAy/GZZ57JUlU9Fio5OOkAtt2kAKhuIHrIaqONNiq0EViaPP744+bll0tqrmhvzYhehNnBLFg2MhPWXXddc+KJJ2ZV1aUCG/uTTz7Z6wRxpsoIOkTmFx/wiL5//35IpViV7Gn8SaBzhg079/20GnQOjicnKdHV367yiZ5rKgsR7ay0hfF1gEjImI9NU7pL576YdK49n+4P+6KZKvuhzfuFNpsZf5UWgkK8rvSojifGeyhxDcam/CPb2yyu84Lqq9ieWXVDuMSlxr2d+lBsziiibod8YOEGfbSTjL/STyqwckAFwzD7ww/TtQSLL764p8tG1RGHl156ydxyyy1Zm1lTordqm52zln/llequn4JEmwGbYs+u/2etJgl+m6cwknz//v0LIfkAPJcDDjjAG+3cfXdmp2na/UAg0V9p/LUwA3QTOTWIiGL1uzpG6yeaBUH1D1Le5Sp/ZKQcJPxXW3eSBHWuyhG1r7/OfzbmWnifnWN8D715+n298YMrbRpT13c6fo+2f1dd/0u4XrR+yL2/8U3KVo4c/sq2bY0sdUXqxc6WtrMAAWJP9O390rZ1pNpalgLOoWw8ZEoQPWZuxx9/fJMd81NPPWVGjBjhWT/E4aCDDjJrrbVW7DEm1i6++OI8VhMlg5sUDMzxls5a+P333/cmY9PUDzUC/8NaTRJ0ylMY1QuTrYB35sEHH/R+b7vttlk7skR0797dM9d8/fXXsxT32r24yAa32T9GDjL5eJJNcWABhejKKb1V178C0tI+s9Poh1cs0RDuuovSZJ3TV+dfGhzQ7221udgsCItKx3RoSl2MFLifXXTuBdoOUH2xyj47isFd+DTjLwoRB17+3LEsVDfP85/G15MngY7rDySVZ23R3mrrS3mvVQK/6tRxQ8zleI7rGD9ONp33VOPHMLl+6rTps/JUiPmm8SM/7mP8+BrodRlRMQLEyud2m+j06eh42xlrDtK1vijgnooA7+hpSQeRzk844QSz/PILHENxZCEWCfbNH3/c3Jdpiy228Gydk3DZZZeZWbMyP2bMBqdmLVwQtipdpDkYofBM6gzaXSuiXz9rQWLToNoKcNFFF5lnn/VlWN6Dvfbaq6KGMDF+4IEHeo5VGYClkEecuyQU6C4CGijyaRYr0xJk74RzINkJKsNs71UmfRIzCmI6X6JzF9c1h9s8JlzKiX1NGzG/aq/6Doxaxdh7oH29yqg7EbRdG5aLi3acpcDw6kmdv7/aWmQgq7h20MGgPuN/9Q8RNx3SGSLhkmoklaXzZtGJX8QcRj3Hx8AIBlYLd3IIBnQIrcURh9C3dEyrxh1EMg+TfADUOKeddprn0MKkIcAmPk0vP378eG80kAP3jho1KlN82gKxQd4TUN+0AqLP3e4K0GIh7yQgtYcRtrJ64403CmlMp06dzE9/+lPz0UclNdXeOw4xJU1vQwZ0S1FbbIIyxH3oYK7IiuEULtR5SD6M81TH0yK8h02Oh5sA4loT1/vkSP7fTPEkT+fBikFxFjBZwDj4dtWzm+79vuJalgpIf4jSLiLxPUX2sYpoHePezjC+/XKWcWfcSOYPqmflpGvUEjiR9OzZk4BdR8QdD5vDRYHu9dRTT/VsnV999VXTp08fzw46DhzPEHUwijF5TygASd9zIqqtp8+I3O2uACX1VEjxkPwGGzTvf/bcc0/PXJL5G2LWFIWOHTt6KqES8NoN0f9HqV9CIdbNjBJ9r5RK0X1CdkskHP/YlsGjlLUV40QCOoirRHgdU66TB39RXTcF+n/mHkxL4i8CSK3lknwAPGxuVBs3Vntr6RnJEPg/IuLtRMRxIQIYIw6o8Bp0EHQAdSd6C9zDY4n+3Xff9YbaSRYlTLANHDjQ05Ous846sWXoLIYPH57FlDIMVFvj85xQEHIHL6MTC5x86ohaBl2L934LAVPTzTdvucIiar3tttvOm9wnFYWMpq2eWjogesYWP48pBNEPDn7YSdj9EirEwWNjpfgZKT8+xakisCb/XxtagMnVqEMSE6R51R9J4Mmii+9uf//DlD/aiIXuY3VbbxFgQpjJ54MKqi8r+N8xH9LMzE7kj0KxUpJvjUAUYjYr9n2FpPv27esNkeOA92uaaeHll1+eZVgdxR2S/OJne6uL3LOqTDCivsJjto6o5WxwRfNLSy6ZNA1YPpIsvCLw+BZ9OJYq6KvjJqc66tj6IfNHJuCSHi6S/HEJxy5RHf2jmVjGqH4WY55i/Em7MHqZ0vE+EJem2nPTbFz3tiaavJXJs2Y+XjO+CST1Ze0Q+pmW7a8EB6q9fytldloF9BSxj5BU760ur33ezlo7pdQEVn3TaBImZb/++mtv4vWII45oCiGbFcQRnzy5rOgTV5dzUgEoKzYS6ps6E30tPdJrFgwvK9JUjCF47Q7MKyF6JPc4YkOqP9vuJ1m88KIQLDlOp/6NCY0KohCZvYxppnb/HDmEWidtGHu/0iE637OjVR0s8nyTUtx4hq4P9QTjqqSx5gNKx6q+GbY+6uG+S6ljmCxOdDaxYDaO9UAx+WQIyEQoBNPCEzFS519L1JsVXJeRE5Okmxh/geekYS9u8P+1+0yedkipF+sVLGr4/9NhI/1Xa+HoamCk8Ud7seIW5pDEJSGwFHrWLJg5c6YXdbAMIGDcX86JBaAsaRWnJlQSRQJ1EH4LGZG6IHbB4P+Tr8evMjLGGZrJH4/oRW5viNj4aOMC9nhEr+MMcZNsj8cZ36EpDvS6TDISRS0uQRJx5LykSQ7RwPB2X7W7yc5N+w/pGqg7kj4WLEKSlpHDQWXXcIx5qyM/2E6ypqlRUHmkrVpEB/J71fd1KO8y1cszg4DjVGaAgERFED3mlf0lpQeWHDdJUseEdaKJJ/FddXxplUdcSLKSoa4jVeaKUN4InYe5KF5BharGqgVJ9e9Kqqe9qWoyHJ0wqTzkkENSddKoM8rQywcYGbcEXI1QlpNhJROyjJjoQFFvYXLI82Uf3T9zJBlRVrvLxAs1vFZJ0CGy6lQGeO0OEylSdRzRb2mlW0wmk6Q1zt034RgmCeV0+xBjUryPmWGSD2Gi8UcQcRMnmP8ldUYnpywkgqUJ+v2ke0+bNObD7R0heQ+MRPRcqTtJ/OtEJxM1b80JRlkDQiTvQb9fFykTkTNuxMSz62x8qT7JvvrWCMkH9d6mejEvPaaCNtcazIdgEpo6q3jvvfd6AcmIL49+Pg7EsckQfjgOSKYjyzmxIKAizOwZG+Ctt94y33zzTYsonXR0kDcJMg/vB78zqh2ytLtWeLJUAcxo8aBmQpZIldUEqsFPP/00U1H+hImeIThdaVS6huAYkh+SUNFbxne+SbKtLxeMMHKRHKRoRw5x4EOOk7wZcTyUUuc7qpNeMeogFiBNmp+u89PEHkz85pt4kmHim07yy5hjWTFG5Jukx8QrF4KJm3NZU4RNmIAk1dK/Uq75b/MDInpJ0U9Lqr/D+O94Kvi4zjzzTNOvX78Wru10BMRoKRNXqB2flHtyASgroidx01977TVv9aPnnnvOG/lA4p99VjN1dtUjkYaAtzKCZ5JFoTc5TSIuUOfOnasyAQvoXHOY7aJRWED0eJCK0BqNr7OMgmD77WPywVV2QrfIiRHeFEwg/y/heJr0lRaoLe4Y7S411k5bNjCtMyqlQ5xjr5/U5kodZxI7CXUA34vMGWnEET3tQf2S9JzTOp/W4v2aB8yfoIQvaSvIR8yCzUcffbRZe+21vSE0tswVhOtFtP1nuScXhIfLPRHSASyVB+nXGGW3Oy/0//1CAgHXSw5NaoEaCiIm4mTR4H3D0/q99zItQPaa2u3NOUYJhpg3A03LFz6J5JGer7L7SYq1QBefBUg1SNeD1Xk8r84jqVzaB5mmH6YDiUZ0pNvFhi42fK01KU3zwEt74lgttYtT3Vhgn5dE8p/rvErHt9smHSBWvWkZ2yfAh3jKqgxjw7jImNSbFEKvVU1YZQELKeujRIXWI0t5pDacpvCeheiS4t9kxHDmCiqpoAAQXDDRUzgJdHREmwSY+pXr2g95EX43HCEzAxDOakb0FvTmJYkeED+ehUN23jm3RiwRqA4x2w3CKWRAk/TRjGRQM9ggXqVMEAPco3OCyDpJ8VcfVfqtyhU50RRL9HbiNM3qA5EjLnQvnVvShByhFNJC2qbFNyewGdZEZ0QP2LYmxlsRMv83U4DH6y4i7WZhFZRHZ5i27uYzoW3c/Eof1XGN6m0WHFt5TCz/UG3uGckymZw5fDEfXoXACipTwJJqgpAL6uhQxx1bsnAI4XguxFyPxl3PA/T6hJJAHZQRE9TuWps8EqEXc+NMC4Mzyps+fbrp1q1b4kI0WUAHOGnSJPPwww97k9gZgabgmuBHnDTJJFtWor88tI/ON07fjIT3TxHbiUkTizqGJIFVz9SMUmySRF8qvv4TxjexjKKH2kAQChy6vrVt4hp4Tg4pUSfSEPMUSaOeoaqL/87wIMCatelnEnDvlHqLiGjJPTBBSsiHRuMTC2arqMSS/sdTROCBhHmXiSd62v+I6h1ifMmelworIVQgaXMWrRb6KN8Q2UG6Q2p42UG6bpxRQT1wrclJ9CyHR0C3wEMTc1QSZM0WSZ394Hc4P/Cq5VxGRRMnTsxD8qDmC+rof/W53hGcCuPU27Fggpa04447eouEh/HEE080rQkboGvXri0su2644YZyLJxuUXubToojRiIP4qaeNKwPgMrizuAHkr010YwL5oAOprOOI9k+jCpD+6hMiE9PRCikaSY5Zin/gAyxXpKk9lJmfZg0Jr3MnlSv62OeicMU6oksIgqdV6NJ9hXgGSM9n6i6ma3Dpn8bky4V0CFcG84QqTIGxLIJKQanpqyRLrneGTYlTfyGcWlov9H4o444KZdwBsNtWlhwptL+Jtv/vVIQWO3KGlwnE0QKj4nEkgShWDDp+pOf+Jq9uXPnigOGeRJsFmCpQzTQYD+6YHYJIFjdlueEAoG5MhyStNhPLDAZJaBZsO4uYAERzEnDIH4N0Snp9IIOMeezAfBHs/nNFkSPmaEICZJp4ckawTWB9BsC/zkcZ+KIeAebsIxhknLZmHKQx03Y7JeQ7JPIqhTRM+pICvdgbH5aGOQkEBKZhcJXSCmDBUuaBB/G1SGVGCRPm/CaDO77COVtIbLPa9tbiuQZnTQGP1T/+7rOMJPi8LYwQWT3jciOURwWFtV0/GKEd3gd7eaTQBiPzATKmrFBQDcsTHbbbbfMRI9un/VRA6Dfv/POO7P6IJzDAjJZ21kkWKdW7wjCTy6PcZ7LKaecUrLcoEGZBwtpuFjtnBbOSFJ1oL5BCk8ihvm2TDOInAi1izfpwJRGUGeaNIsOHfVC2uILZalurGURPV2hNsusaqV60cU3FlAd0kr0v40FUvieeX7Hm+SQE+UAseEgkXu08yb6547GX5JsoYc+kP/pQ0ayr0bguwCDox9iKwGRM/GfSJzEDwMpPgykz6wIrHUCQPAZVTeoI+rpcwBQ3zBxv0Wd2xGHmUqnRjNjiZEwACIuiDbJk/QBQhckHMObE4uOfcpoZIBSngDlSvSADop/Uq6lwVLgvd16HtfomTE7dXwFdWGa2E11RVepiBsmhvMq1fMywtpHJN9iQt1a32B6iHNVQ4XX+aEAiY25iWpYEKHePK8K9VYMOymLWgLnoJLrSWNZgr0465nivHPbbdm1KdjdP/bYY55jESR//fXXZyX641nsOvOFqgBdf56e04HGD21SywiapYCQ1j1ukjrtn8lEaxLRt5DmA1ipGU9SdL2H5WmlBR1M4Nsbu5ShSbbVxs4tSQ8927bvexb4ML5pVuZVYxLwUWRRk77Gt7k/sYy6cKncW/XFed3wBR0dyft3aB8zqu3LuCYgoFxPEXrimFvHPhLZ0zEy14BjXLlqjbJiA9Qa9kNGGEBnncvksARQxx3UClU2TVDbntW9DzHJPixNgNxRNbDcIvr6HEsleqTOykuNjY2eDX5Uwk/A5WpfPcI4t4Da8Zp9RxgFJTpR1Rh91K5JcQfSiB7XK3SzUTLEpC7NM9LYRa8PF6Fi1ocqp0OGRvKWEHirX8g6h4nhuOV7YqPt67w5uiYfZ3RIRXvuCpX7QOXoxEabbARJe7DKiYa0bYxcn3IDVDe+AAzvkuYCooDI+6TEoKfjwM+cENH01mcRbiA4qP0rRcRI+HQ0SBh4emLVc4g9J9oOen6G6HTmo1O8Z5tgY98cbePkoKZCyo/OSaD+oZNO8v9uLbHoS0IfzJv6kJlTwbMwUzzYEkA42Uf15lq6sU5g4p4RTdcshaNLK+ZBjgBmmBv3LftCVQCdjp3TaTTZFuSpJk5Tey5POphI9HZSFkuPRrNAzYEUfIgl8pJQuVtUBz0eHwyWNQyFw2vIUg/EAAlfFw3Lq99jdT5L150QykZH9/eUy/IyMOkaeHwiRR4bnty0dUP26J57GT+eTZxTFOciOWN2x8Rno/EtXwIb1dhJStV9p+omzn8Pm7jvaPwd1qLDCohF1Z9IuR9IFgI9zKSMkFQGaTtqG8+Een9r307Mf6xnCAQ1w9aZG1a908va4q9nFpiV0klhCYQDXRzRz9S5mYJztBYgHelD7mX8RUoqmZylY+2GY1YhDasybAhnRr0TjR/ttN5AyNq9TrH6U6E2XatnFTiO1kOyR7hkzid1PYxSk5cztdlepIVtNAG23s/bCtsp3GwTNuQQPZOJ6NlmxVjuRM/vywpRxlcjIRHeqrzE0AI6NknliUuD8wsPflxSXHfrxMVqVli04B0LQQXxXZiDuC9yz/upLAG4v1R+qreM9Ya9ytYPybMUEffO8yC+6LsVBizLDBEslkZvliyYASJ4RgyBY9Fdqvtem4/FFKOYJMezcUVcv9bQBzRaHzIWYkhL5UhtCAuHqJ57im1ZdaH2fqr7Jpw2ps61MDdNAt/Kzoyw6tiGVKhto/Ss4CY0EnHLaFYLjLKP1eWvKVWw5IQLQPqtuEkL6kJXnqR7TzqHcL7/y1Gel+OiHOUh3OdMupdruO5csJExaxmAqSqwYRMmmgWB7+Yrj5EOnTWRQZMkGjrUEVVvYJWgD+lKfcj4ffBO5ZHsg8mxVFVna4Xa/Z7uGwELp7h6WF3xbkHyr5csWWeojRP0rBj9QPbFBumPB5qQA7Nab2UiegcHC8wOw9FNkXCTQj+HcbUk/0zBs1sr9EFdqg8ZgQePzCxhElBcH6Dz/lPdllUXav9s3TfxXfCnyOU5WyFQmR5e56ieuaC2vqVntb3xl0HlWynldFoOmNRAdX2erpeqDQnDEb1DHnQu4xwm7xOj0/2QgGSuD5kRHRJ6UghnwOhtb5Wv9VKQVYE1Z+yje0f9hCd0ViODcsBo/yRdM9GyrzUDE1VtrtKzQlWNpRyGC2nvSlYgOKAavRCnrbwnO6J3yAOIq0OO8kzO7ixpvtWtt1ku7AQtHR7hC3aPHOYjxyppgMoVsrJGa4LuaYzunRAhOJPhCV7k4tyoNxuVTtV1ClMV1wuENdbmHD0vPGgJC0PMYuJBrZh6YnNQx0Tjq4PGVDIZ7YjeIQ/w4SZOT5aF0An720ck31qCdhUGS0R76CNmXV9iyWNrj2rqzzp2b10bV2UQ2EubQbp37psFZjDhrcQfBU9w/EAI1/xOAU1sVcAnw/hzHGP1zJjfIa4zgsIvjG+uTWdJKHRIHIGIZ4CAhDnpk3nUM2lwRO+QGSLtyZ06bsiLigkrljfRITykjnXNRSr7WK3bV2voI7xOHy++Huiwx+p3kYvvtGqgu9fmdJKeAYHQMMXe3vjhS9JiPtFRQGL4J9ApPtyaHciKhL3PKTbVFI7oHXJBBE64PYi+r0gf3SOBtpmUxQz19SzOVwsTrIQ7pt7tqCf0DPADIXnetCJ+4lV1ML4ZdVRafdvqsR1qCEf0DmVDpM6Hu9ANtx0qg4gcp7yP6t0OhwVwRO/g4OCwkMMRvYODg8NCDkf0Dg4ODgs5HNE7tGk0NDQwoczkMvbNoydPnrxQm0c6tE04ondo67jV+MvC4cF7nYh/P5H9QuHR6lAc9F5cYpqHvrhI78mTVbgOEWij5qnn6VrPVlKvI3qHNgt9VARhQ4q/2f5+TZuVtCWSKSulEYeekAesGobN+Bn21EE6Z6DK4UtAuGcimk5T3uPKG2p8O2nC8YRmpAAADtxJREFU/LKgKg4zhNYmdABrMxDJdKLKXqiy5P1Z+/O0f5a9BsHDcETCZBUyqWqsHF2XqJxn6jqv2N/4SRyq3wtF2IoC8Xvje7gGyB3cMCPoUOBlIt+yJCAOZRVH7nRE79BmITLD6/ACkRsL1RCqmrAFrPDFQi8Ddfw1HWOBl18Z35sxiM4ZxPzBh4CgW3g5vqeyRHokoNWbOre7Jf3HtT9W+6yMRAfxtPYZObAuK9cNQh/jaIT9OZ1JsP7Dgyr3hM6pZmAvQjlAKIfa3xD8xXkrUTvxlp6jthbiR2Hr+0r1fR/KW0a/U9dRUBmk7nn2f1skvlOdMyLXIqIpnTWB/gg/zhrOOM99rLLjrSCAN+wBSt15Nsojbvxlxl8rAodD3rmjdMwLva7ty7Zu7vMV+w52UaI87wqLJXFvH+rYBOWz1kewNsSdSh2N76nc3l6D/8cYR/QObRr6UBa1kjikSxhi9PUTlYYqj++DDye6aEwc+OhPMr4aKA4bcVx1sk9Uw7Vt/ijlQWZ0Jix+81Tw0SufkQEfbdzykoVA15qk65yoRBRSnJpWU/pAvyGgvRhRaL+X8UcjrPNwnfJ+rzxI51Hjd0qEMOBZrar8IeFRiH4/pt9basuaFoQB31y/37DH6DiRVglBvoRNexh/oZf5tj7ICoJl1PWpnVNhvdbbqNfWM0n7W2k7zLZxGe2PUl7iiktl4MeqE29eiJsYNHTIdPiv6TpHQcbGX9XtSbMglDX/Z57Te0o7qAzPiw6eoHd0Bscrr5vx12J+IO6i9h1EncPiRTieMYeEGmdy6BoQPeEUUPkQU2cvpftt/gClDRzRO7RZ6CNiAvY+bbdGUtSWjwcCZti8k/LeUR5SUpYY9BASoWnXTDiO5/BxqnOW6kQFgKoElVBPpE/lQY6s0rVO6BxGCtVSEYTBYuhI9TONv3A55ASZIJHSLtQWQcTEJUPn0QHuY3yyoxNj0Q2IKBzlNChPaN3ZpuXiLW/o3D/oXBa0YfSEyut55f1VeUi8jLQIMzHeqruI9b52tB1W5bSe7YTgtSna3lBqBJADn6muHdix16JN99j2AYia+D8Q/QkqA9nyv0R6bzT+/3ple4+oBidykuostVYBHRvP6DN7bZYAJQDc0dpHNbiXbQugoxuusvOtQIGUz/v4qCN6hzYLfRCzrY4aFQm6UHTtSFi47Z+lPCQoCGgVe8pldruRjhG0DWkbAqSDeE/13aX86CLuAYj4OEbHkU4ZVu8XUwbp9lGVucP+flB1Vp3odY1puiahiFE9Ebiug/EXMm9vJWjWuQ2WD93IqrMCwobMnrb1zLIk2wzK28r4MW5eyNAcyAmyDpYwRBqmwxtvr/GQrXNF2w6wRqQdzHnQkTI6eSXLM8gJng0qIt4Z3oFJtt1v2eMXqg2M1I7U/iLaf0r76xp/9anexh+R/MLex/barKwytyRcCw/j9nbk+b29Hp3mtca/5/D62SyJ+neVDdbHphxrbg9zRO/QpqGP5xJ9GEjwy2k/kFqPUR7D4C9Cut7YeDYqx2Ru09rB2h8R2j81tH+3yrIs348hRJu9Zej4TnZ3sNVPzy9QGs0C1ET/CUmDgMB0SIkjlbayec9aCRyiR3qdaXwJ1VipfF5M3XRyqBSujeQzUooGNINEx/HsVB9zFt2NrxZCrXWP8liwnM5nNu2w151i23Gw/Y3KAhJ8L98jSEWgugHcJyoROp8b7XwLqq8/GT+wWxzolHZQm99XeZb+u1VbRj50ED2TLqryX1lhhHkeL16Q8p6xh9Hfc5+L2d8IJowq/8/+vktl3+X/6Yjeoc1DHwPS7NeRvKzhlS/IcR06jVkZyn2Rtc5KIRIg8BjqByyM9ogcZpUnRihJ6wADzFP3RydufDXBKZHjdAg3o3oIdSABWAs6ajbISOnfqF1sfRDXw0r/Uh4T1hAj5N83fJLqR1WDqg1VCKF/RxTZUaqupNWidtI1OTbbTkRPCZ1zWagcz6HR5jO53sWeNys84Rw6d6fQ/sVWGFk8mL8JHYubh7grUqaHI3oHhwqgj+jRerehElgC7qXdLwPCwdLD+JYiIAhFHejdu9gyTJZuavP2tvMdn0etXfR7k9D+3pHL7x+Ut+S3g83f1er7PwvV1yVCqE3zAPrd2W6PU5mfaHeu9stepCMvQiPBWKhNmM2ywHozoaDUeZGyFd2PI3oHhzaOqJRYZh2zyzgn0QQypN4K55UkRpX5NG87qg21KTrKqTkc0Ts4ODgs5HBE79DmYPW4gRUJ0uxRSn8z1gbb+JNZ2HpjF40JIWZzz9jfTBBi2oeOGNv7zYyvS0anP1BpmCS43vY61IOVCI40zAEgbTKJh8UFNvfoxVk2DgetS3TeMfY8JkDxzMVL9gTjT3BiyRG2sHBwyAxH9A5tEWuLND39skj1KeNbaGBhs59+YwnzZyUsIwhLgD4Ywsa+nNAFo1XmCONbSmDPvJT1gsVOmonLZa1pIHpuLFUg5+mT/ZAJQ4xvm45zCwSObT126tiFN1ngGN+BBl0zHQsmj+jOH8K6Q/V8U62HkgZrZcPE7eeBLt96oc4NvGGttctyYU9ee97SeSdGrdfpkqUmpm0bvsWkMtcNFQi1AQEBhzLazLuCCe2Fxu/IsXU/Qu1705Zl7gELHNYdxkEMiyRUUnjYIlTQ8aPq4X1bzZZ5xPhmojxnzG15zwbYc6gf4eQoO7cSC0f0Dg6+/TPelNgy82HNty7srIV6hRIjABb/xolnX+M7viCpQ7oTbB0TlQYZ34KFToCPfbQ9tqfOQ4rH2QfzQD5aSJzvj3zP89ba5gOIA3NCTDwDqwo+clztZxZ7681hHX0gq5ft9fBIxUmHUQ0k00llcKDClJFOiNhAWMHgQUz4hA/1u4PxJ1Y5F6IOTDbpOPvp2fbUb+6ZZ4fT0e7GHxHxPJiwZWTESGkJlcM/4dDAw9a2MfCExWMUKxz+dxdncD6qFui4uXdGe3T4CAlXqj33ql2M/Lin4bYs94hQsW+DH77gMON3ENcqb5zyuPceStjNd8OZz/hWT4w8v7XHsI1nNDnKvqcQP45rw5Ia6IjewcH/eJCK8MrcRukvlswgc1zPcdHnI56iD+sE+2G1t+cGnqwdjG8DjmfoEOOTNfXyQd5hJXrqRK0D2W+jvI+Uh8Tved4yMmBr87ABfzuUxzVrsWwjbblF1x1qCRyrGxzGeinvVeVB+DyjGfr9D/1G5UUYBDoiiAv1FB0cxA+BnaVyT6oc/gVIsxtYPwGeAaMZOOifKnOb8vFVgLgxOdzdSvXTbbuinrCMyNZSuT1sp0GYiLoQvfWR4D0hIBmCAW3+m/L6GF8qjzpDdbbqQzozhAMcq06zdZ1ppf4/aouVDp39Z/Y8yjGq/FzHCKMw1I4uV1Uam9ZGR/QODr7tM0TKcBlVAB8attiEP4BwkTpxHjrOqmX4sPj4ILVe1iUeybwHag3rGLUOFiBWkkWiRz0DYTLE5+M9W3mQ45JmgedtGEjPhGfgA0a/P1P1zY0pVwvgnTm8wY/JQ1unWBUWDkJ0XHRgdAjYxTPK4VkETjyBnfw046uosM3HXh8JFE9ORgZ0gsyT0Dmg4lhKv5H2ecbL2fN/HvKE5fnhIcro4k6bhyNSO+sTUVPYUd4448+pTFTyQiSoLXfoGEQeDaExxUrrePReb/wOHM/Zx22HjmDB+8b9I2wEZqeM7na17xvPawIetfrNaKJdWhsd0Tu0OQT6+fC+lciWieiXkTCXDMUZwRNy0UBvrN/oSVHBQEqfRBxfrrL1I7VuGGnCeOt5+3lItxxuUxC98lQr/dbUiSoGBPBCB/yC2nOp9pdEktTv87UlrAFkvafSplbaxGEnIDdGNs/aLc5Vdysh3S8x2Y/MSJkzrUTPBDWkhbMQpEknGIQVeDPGE/Y5qwKBxwbVg+QtEA7ocOj4CV/ByK6v9eKNC8OARM+z4B1A2mcEd6ny0M+jFiTmUUfdz25WdRMA3wXeOTyViTraR8d/GzonEY7oHRxMk033J5G8Zh6zuKNHTmNC7eOwDbk+PKJX4jx0f4nrZfK8rTPBB6CDI5wzowyIC/39hpbIkOQJAQHZXd3gh9dlf1V77jDlca88xwdsiAWkz7A6A4me8M6MpFBFEOoXr1ImGomkGTfiQeL9WMcI6IXUf3tMmZpA93SdJe4mz1WrmjHRSWhCIBh/7qMZGvyYNyuGfAXG2PJI9/+LFO9iz6HMIlkmuh3ROziUCX1g18Tk/akebSkKSNbGtwphP5gcNlaSnxuWmu2oZI7Nu68hEh9IvzEXZSLx20jHRvnR9hrE7B8aboMdXUH6s6wHLmjhCSv0tp6w39RRmg/aNCfyO5eVkbVcyuwpa8+JCh6JcETv4OBQEoH6KpL3cdpv44clnhfki5RR5zxh/MnpRE9aS+4fZWxXq/OEbY1wRO/g4FAViIT3j/xmDmOzOjWnTcMRvYODg8NCDkf0Dg4ODhWgoaEB238mj1E3MdFM3Hqsrlh7F6spJvkJg4EpKqanTC6jusJ2Hkcq4uv/JuRxjInlVP1+RPs436G7xzkNG31MLZe31xhifOsm5gMOTTO/dUTv4ODgUBmwDmI5xcMa/KUOMQMljAELqNygPDxZ8XjFKe8F5WEWyQT0TjbsBvMW4SUWsVh63+5j5cRELT4IWHPtbm3w6Tj+pP2Z2scWnwnqx5Ia6IjewcHBoXiw2Pt5dh9TW/wMiEXzkM2DlLcLlccBjfAbLKCO7wVer5A84SFG2jJYPp1u91m+8iwbX4hrLZHWGEf0Dg4ODsUDRy9UOni94sX7ps1HdfO0PTYzVP4ATCyJ46N9Yvig5iHMxn9DZZDiz1MZOg5URTtP9pcKxBw2dQF7R/QODg4OxQNv4qtsTBqIHocwoqL2U95uxtfLdwuVH229oJ+3pqekF21MnwA4Y6H3x0GNMBt4JmNeiv9C0lKHHhzROzg4OFSAyf5i3YfZfVQznnpGJMzEKY5fH01esOg6sW+mTQ6toKX9zVPqPsfuBs5rM4L9Bn8x9i8np6zUFcARvYODg0MVYK1owt6uTLB+NTlmmcQy6/+kdCkf/x9/Tkcq+c6N6wAAAABJRU5ErkJggg==">
  			</div>
  		</div>
  		<style type="text/css">
  			#payment-details{
  				
  			}
  			#payment-details,.bottom-section{
  				max-width: 618px;
  				max-height: 480px;
  				overflow: hidden;
    			margin:0 auto;
  			}
  			#payment-details p,.bottom-section p{
  				font-size: 16px;
  				line-height: 19px;
  				font-weight: 400;
  				text-align: left;
  			}
  			#payment-details div,#payment-details p{
  				overflow: hidden;
  				margin-bottom: 5px;
  			}
  			#payment-content{
			    padding: 5px;
			    border: 1px dashed black;
			    width: 90%;
  			}
  			#payment-content-head{
  				text-align: left;
  			}
        #payment-content-head > img{
          margin-top: 25px;
        }
  			#payment-content-head>div{
  				padding-left: 10px;
  			}
  			#payment-content-head h1{
          margin-top: 15px;
		    	font-size: 24px;
		    	line-height: 27px;
				font-weight: 700;
  			}
  			#payment-details .offline-total{
  				padding-left: 5px;
  				font-size: 26px;
  				line-height: 29px;
    			font-weight: 600;
  			}
  			#payment-details #payment-content span{
  				float: left;
  				float: none;
  				display: inline-block;
  			}
  			#payment-details #payment-content span.payment-offline-receiver{
  				font-size: 19px;
    			line-height: 22px;
			    font-weight: 600;
			    padding-left: 5px;
  			}
  			#payment-content > p:nth-child(3) > span:nth-child(1){
  				max-width:60%;
  			}
  			#payment-details #payment-content p.offline-order-id span{
  				/*vertical-align: bottom;*/
  			}
  			
  			#payment-details #payment-content p.offline-order-to span{
  				vertical-align: top;
  			}
  			#payment-details #payment-content p.offline-order-id span.offline-order-id{
  				padding-left: 5px;
  				font-size: 42px;
			    line-height: 45px;
			    font-weight: 600;
			    color: #d64848;
  			}
        #payment-details #payment-content p.easypay-order-id span.easypay-order-id{
            padding-left: 5px;
            font-size: 32px;
            line-height: 34px;
            font-weight: 600;
            color: #d64848;
        }
  			
  			#payment-details .payment-content-bottom p img{
  				width:50px;
  			}
        #payment-details .payment-content-bottom p img.easypay,#payment-details .payment-content-bottom p img.cod{
            width:100px;
        }
  			#payment-details .payment-content-bottom p:nth-child(1) a{
  				font-size: 16px;
    			line-height: 19px;
			    vertical-align: super;
			    color: #5f5fe8;
			    text-decoration: underline;
  			}
  			#payment-details .payment-content-bottom p.account a{
  				color: #5f5fe8;
  				text-decoration: underline;
  			}
  			#payment-details .payment-content-bottom p{
  				font-size: 14px;
  				line-height: 17px;
  			}

  			.bottom-section .images_section img{    
  				width: 100%;
  				display: block;
  				margin: 0 auto;
  			}
  			div.bottom-section  div.text_section p{
  				font-weight: 600;
  			}
  			
  			#content,#container{
  				height: 100%;
  				margin-bottom: 20px;
  			}
  			@media (max-width: 767px){
  				#payment-details,.bottom-section{
  					max-width: 100%;
  					max-height: 100%;
  				}
  				#payment-details p,.bottom-section p{
  					font-size: 16px;
  					line-height: 19px;
  					
  				}
  				#payment-content{
				    padding: 10px;
				}
				#payment-content-head>div{
	  				padding-left: 10px;
	  			}
	  			#payment-content-head h1{
			    	font-size: 22px;
			    	line-height: 25px;
	  			}
  				#payment-details .offline-total{
	  				font-size: 20px;
	  				line-height: 23px;
	  			}
	  			#payment-details #payment-content span.payment-offline-receiver{
	  				font-size: 18px;
	  				line-height: 21px;
	  			}
	  			#payment-content > p:nth-child(3) > span:nth-child(1){
	  				max-width:100%;
	  			}
	  			#payment-details #payment-content p.offline-order-id span.offline-order-id{
	  				font-size: 35px;
				    line-height: 38px;
	  			}
          #payment-details #payment-content p.easypay-order-id span.easypay-order-id{
            font-size: 25px;
            line-height: 28px;
          }
        
	  			#payment-details .payment-content-bottom p img{
	  				width:30px;
	  			}
          #payment-details .payment-content-bottom p img.easypay,#payment-details .payment-content-bottom p img.cod{
            width:50px;
          }
	  			#payment-details .payment-content-bottom p:nth-child(1) a{
	  				font-size: 15px;
	  				line-height: 18px;
	  			}
	  			#content,#container{
	  				
	  				margin-bottom: 40px;
	  			}
  			}
  		</style>
  <?php }else{?>
  	<div id="content"><h1 class="heading-title"><?php echo $heading_title; ?></h1><?php echo $content_top; ?>
	  <div class="text-empty"><?php echo $text_message; ?></div>
	  <div class="buttons">
	    <div class="right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a></div>
	  </div>
  <?php }?>
  <?php echo $content_bottom; ?></div>
<?php echo $footer; ?>