<?php if($data['CustomerPhotos']['Enabled'] != 'no' && $CustomerPhotosProductName!=''): ?>
    <link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $currenttemplate?>/stylesheet/customerphotos.css" />
    <?php if($data['CustomerPhotos']['showInTab'] == 'no' || $RegardTabs==false) { ?>
        <style type="text/css">
        .CustomerPhotosImg {
            border: none;
            height: <?php echo $data['CustomerPhotos']['PhotoHeight']; ?>px;
            width: auto;
            padding: 2px;
            display: none;
        }
        </style>
        <?php if ($data['CustomerPhotos']['WrapInWidget'] != 'no') { ?>
            <div class="box box-relatedproductspro">
                 <div class="box-heading"><?php echo $h2_CustomerPhotos; ?></div>
            <div class="box-content">
        <?php } else { ?>
            <h2><?php echo $h2_CustomerPhotos; ?></h2>
        <?php } ?>
        
        <?php $tagVar = $CustomerPhotosProductName; ?>
        
        <div id="CustomerPhotosImages">
        <?php
            $patterns = array();
            $patterns[0] = '/%s/';
            $patterns[1] = '/Instagram/';
            $patterns[2] = '/Twitter/';
            $replacements = array();
            $replacements[0] = "<strong>#".$tagVar."</strong>";
            $replacements[1] = '<strong>Instagram</strong>';
            $replacements[2] = '<strong>Twitter</strong>';
            echo preg_replace($patterns, $replacements, $data['CustomerPhotos']['MainMessage']); ?>
        <br /><br />
        <script>
            var customerIgnoredPhotos = '<?php echo $data['CustomerPhotos']['IgnoredPhotos']; ?>';
        </script>
        <?php if ($data['CustomerPhotos']['InstagramEnable'] == 'yes') { ?>
            <script>
                var instagramClientId = '<?php echo $data['CustomerPhotos']['InstagramAPIKey']; ?>';
                var instagramTag = '<?php echo $tagVar; ?>';
            $.ajax({
                url: 'https://api.instagram.com/v1/tags/'+instagramTag+'/media/recent?client_id='+instagramClientId+'&count=<?php echo $data['CustomerPhotos']['InstagramPhotos']; ?>',
                dataType: 'jsonp',
                crossDomain: true,
                success: function(data) {
                    if (data.data) {
                        $(data.data).each(function(index, element) {
                            if (customerIgnoredPhotos.indexOf(element.images.low_resolution.url) == -1) {
                                $('#CustomerPhotosImages').append('<a href="'+element.link+'" title="Click to see the picture in full size" target="_blank"><img src="'+element.images.low_resolution.url+'" class="CustomerPhotosImg" /></a>');
                            }
                            $('.CustomerPhotosImg').fadeIn('slow');
                        });	
                    }
                }
            });
            </script>
        <?php } ?>
        <?php if ($data['CustomerPhotos']['TwitterEnable'] == 'yes') {  ?>
            <script>
                var twitterTag = '<?php echo $tagVar; ?>';
                $.ajax({
                    url: 'index.php?route=module/customerphotos/twitterTags&hashtag='+twitterTag,
                    dataType: 'json',
                    success: function(data) { 
                        if (data) {
                            $(data).each(function(index, element) {	
                                if (customerIgnoredPhotos.indexOf(element.image) == -1) {	
                                    $('#CustomerPhotosImages').append('<a href="'+element.link+'" title="Click to see the picture in full size" target="_blank"><img src="'+element.image+'" class="CustomerPhotosImg" /></a>');
                                }
                                $('.CustomerPhotosImg').fadeIn('slow');
                            });	
                        }
                    }
                });
            </script>
        <?php } ?>
        </div>
        <?php if ($data['CustomerPhotos']['WrapInWidget'] != 'no') { ?>
                </div>
            </div> 
        <?php } ?>
    <?php } ?>
<?php endif; ?>