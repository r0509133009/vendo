<?php if ($isLogged){ ?>
<div class="content">
	<div class="profile-box">
		<div class="customer-img">
			<img src="<?php echo $customer_photo; ?>" alt="<?php echo $avatar_name; ?>" title="<?php echo $avatar_name; ?>" class="avatar" />
		</div>
		<div class="customer-info">
			<div class="customer-email"><?php echo $customer_email; ?></div>
			<div class="my-profile">Моят профил
				<div class="profile-wrapper">
					<?php foreach ($items as $item): ?>
					<?php if ($item['href']): ?>
					<a href="<?php echo $item['href']; ?>" <?php echo $item['class']; ?><?php echo $item['target']; ?>><?php echo $item['icon_left']; ?><span class="top-menu-link"><?php echo $item['name']; ?></span><?php echo $item['icon_right']; ?></a>
					<?php else: ?>
					<span class="no-link" <?php echo $item['class']; ?><?php echo $item['target']; ?>><?php echo $item['icon_left']; ?><?php echo $item['name']; ?><?php echo $item['icon_right']; ?></span>
					<?php endif; ?>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } else {  ?>
	<div class="loggin"><span id="popupLogin"><?php echo $text_login; ?></span></div>
<?php } ?>
<script>
	$(document).ready(function(){
		$('#popupLogin').on('click', function(){
			$.magnificPopup.open({
		        tLoading: '<span><i style="font-size:50px;font-family: FontAwesome;" class="fa fa-spinner fa-pulse"></i></span>',
		        items: {
					src: 'index.php?route=module/popuploggin',
					type: 'ajax'
		        },
		        closeOnContentClick: false,
		   		closeOnBgClick: false
	    	});
		});
	});
</script>