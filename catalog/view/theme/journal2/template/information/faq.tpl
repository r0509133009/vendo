<?php echo $header; ?>
<div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
	</div>
	
<?php echo $column_left; ?><?php echo $column_right; ?>
    <div id="content"><h1><?php echo $heading_title; ?></h1><br/><?php echo $content_top; ?>
		
		  <div class="main-products">
		  
		  <div class="srch">
			<div class="input-group text-right">
			  <input type="text" name="filtername" value="" class="form-control"/> <span class="input-group-btn">
				<a onclick="faqfilter();"  class="button"><?php echo $button_search; ?></a>
			  </span>
			</div>
		  </div>
		  <?php foreach($results as $keys => $result){ ?>
			<h2 class="faq_title"><?php echo $result['name']; ?></h2>
				<div id="accordion" data-collapse>
				 <?php foreach($result['subfaqs'] as $key => $sub){ ?>
					 <h3><?php echo $sub['name']; ?></h3>
						<ul class="sub">
							<li><div id="collapse<?php echo $key ?>-<?php echo $sub['faq_id'];  ?>" class="panel-collapse collapse <?php if($key==0){ echo 'in'; } ?> " aria-expanded="true">
								<?php echo $sub['description']; ?>
								</div>
							</li>
						</ul>						
				  <?php } ?>
				</div>
			<?php } ?>
			</div>
		<?php echo $content_bottom; ?></div>	
	
    
<script src="catalog/view/javascript/jquery/jquery.faq.js"></script>
<script>
function faqfilter(){
  var search = $('input[name="filtername"]').val();
  url = 'index.php?route=information/faq/search';
  url += '&fsearch='+encodeURI(search);
  location =url; 
}
</script>
<script type="text/javascript"><!--
$('input[name="filtername"]').keydown(function(e) {
	if (e.keyCode == 13) {
		faqfilter();
	}
});
//--></script> 
<?php echo $footer; ?>