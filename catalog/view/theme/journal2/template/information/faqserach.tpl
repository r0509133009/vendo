<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
	<div class="wrap">
	<div class="inner-wrap">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
		  <h1 class="faq"><?php echo $heading_title; ?></h1>
		  <div class="srch">
			<div class="input-group pull-right">
			  <input class="form-control" type="text" name="filtername" value="<?php echo $search; ?>" class="form-control"/> <span class="input-group-btn">
			  <a onclick="faqfilter();"  class="button"><?php echo $button_search; ?></a></span>
			</div>
		  </div>
		  <div class="col-sm-12">
		  <h3 class="faq_title"><?php echo $text_search; ?> <?php echo $search; ?></h3>
		  <?php foreach($results as $key => $result){ ?>
				<div class="panel-group accord" id="accordion">
					<ul>
						<li class="expandd">
							<a href="#collapse<?php echo $key ?>-<?php echo $result['faq_id'];  ?>">
							  <?php echo $result['name']; ?>
							</a>
							<ul class="sub">
							<li>
								<div id="collapse<?php echo $key ?>-<?php echo $result['faq_id'];  ?>" class="panel-collapse collapse <?php if($key==0){ echo 'in'; } ?> " aria-expanded="true">
						  
							<?php echo $result['description']; ?>
						  
						</div>
							</li>
						</ul>
						</li>  
						
					  </ul>
				</div>
			<?php } ?>
		   </div>	
		<?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div></div></div>
</div>
<script type="text/javascript"><!--
function faqfilter(){
  var search = $('input[name="filtername"]').val();
  url = 'index.php?route=information/faq/search';
  url += '&fsearch='+encodeURI(search);
  location =url; 
}
</script>
<script type="text/javascript"><!--
$('input[name="filtername"]').keydown(function(e) {
	if (e.keyCode == 13) {
		faqfilter();
	}
});
//--></script> 
<script type="text/javascript">
	jQuery(document).ready(function ($) {
		$('.accord ul li:has(ul)').addClass('expand').find('ul').hide();
		$('.accord ul li.expand>a').before('<span> + </span>');

		$('.accord ul').on('click', 'li.collapse span ', function (e) {
			$(this).text(' + ').parent().addClass('expand').removeClass('collapse').find('>ul').slideUp();
			e.stopImmediatePropagation();
		});

		$('.accord ul').on('click', 'li.expand span', function (e) {
			$(this).text(' - ').parent().addClass('collapse').removeClass('expand').find('>ul').slideDown();
			e.stopImmediatePropagation();
		});

		$('.accord ul').on('click', 'li.collapse li:not(.collapse)', function (e) {
			e.stopImmediatePropagation();
		});		
	});
</script>
<?php echo $footer; ?>