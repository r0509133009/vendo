<?php
// Text
$_['button_remove']						= 'Удалить';
$_['text_continue']						= 'Продолжить покупки';
$_['text_empty']						= 'В корзине пусто!';
$_['text_total_qucik_ckeckout']  		= 'В корзине <big><b>%s</b></big> товаров, на <big><b>%s</b></big>';
$_['sendthis']				='Быстрый Заказ';
$_['callback']				='Перезвонить:';
$_['namew']					= 'Ваше имя:';
$_['phonew']				= 'Ваш телефон:';
$_['comment_buyer']			= 'Ваш Комментарий:';
$_['email_buyer']			= 'Ваш E-mail:';
$_['theme']					= 'Перезвонить:';
$_['messagew']				= 'Сообщение:';
$_['cancel']				= 'Отмена';
$_['button_send']			= 'Оформить Заказ';
$_['time']					= 'Мы вам перезвоним в течении часа или в другое, указанное Вами время.';
$_['ok']					= 'Ваше сообщение отправлено';
$_['erno']					= 'Извините, непредвиденная ошибка.';
$_['die']					= 'Извините, настройки сервера почты не дают возможноти отправить Ваше сообщение.';
$_['error_phone']			= 'Ваш Номер ?';
$_['mister']				= 'Ваше Имя ?';
$_['subject']				= 'Быстрый заказ';
$_['text_1']				= 'Вы получили заказ на ';
$_['name']					= 'Имя: ';
$_['phone']					= 'Телефон: ';
$_['comment_buyer']			= 'Комментарий: ';
$_['url_callback']			= 'С этой страницы оформили быстрый заказ: ';
$_['error_required']        = 'Поле %s должно быть заполнено!';

$_['comment_buyer_error']	= 'Нужно заполнить';
$_['email_buyer_error']		= 'Ваш e-mail';

$_['text_column_name']		= 'Я покупаю';
$_['text_column_price']		= 'По цене:';
$_['text_column_quantity']	= 'В количестве';

$_['text_column_photo_product']		= 'Фото товара';
$_['text_column_name_product']		= 'Название';
$_['text_column_quantity_product']		= 'Количество';
$_['text_column_price_product']		= 'Цена';
$_['text_column_total_product']		= 'Итого';

$_['text_photo']			= 'фото';
$_['text_new_product']		= 'Товар';
$_['text_new_model']		= 'Модель';
$_['text_new_quantity']		= 'Количество';
$_['text_new_price']		= 'Цена';
$_['text_new_total']		= 'Итого';
$_['continue_shopping']		= 'Продолжить покупки';
$_['text_quick_order_enter_name_phone']		= 'Пожалуйста, укажите ваше имя и телефон, чтобы мы могли связаться с вами';
?>
