<?php
// ***************************************************
//             Custom Registration Fields   
//       
// Author : Francesco Pisanò - francesco1279@gmail.com
//              
//                   www.leverod.com		
//               © All rights reserved	  
// ***************************************************




// Frontend language file

// Here you can customize the strings for the module.
// For reference, see the files:
// /catalog/language/english/account/register.php
// /catalog/language/english/checkout/checkout.php 

// Heading 
// $_['heading_title']			= '';

// Text
// $_['text_account']			= '';
// $_['text_register']			= '';
// $_['text_account_already']	= '';
// $_['text_your_details']		= '';
// $_['text_your_address']		= '';
// $_['text_newsletter']		= '';
// $_['text_your_password']		= '';
// $_['text_min_4_chars']		= '(Deve essere tra 4 e 20 caratteri)'; /* this entry is disabled in this version */
// $_['text_agree']				= '';

// Entry
// $_['entry_firstname']		= '';
// $_['entry_lastname']			= '';
// $_['entry_email']			= '';
// $_['entry_telephone']		= '';
// $_['entry_fax']				= '';
// $_['entry_company']			= '';

$_['entry_customer_group']		= 'Tipologia Cliente:';
// $_['entry_company_id']		= '';
// $_['entry_tax_id']			= '';

// $_['entry_address_1']		= '';
// $_['entry_address_2']		= '';
// $_['entry_city']				= '';
// $_['entry_country']			= '';
// $_['entry_postcode']			= '';
// $_['entry_zone']				= '';
// $_['entry_newsletter']		= '';
// $_['entry_password']			= '';
// $_['entry_confirm']			= '';
// $_['entry_shipping']			= '';
$_['entry_anti_spam_label']		= 'Lascia questo campo vuoto:';


// Error
// $_['error_exists']			= '';
// $_['error_firstname']		= '';
// $_['error_lastname']			= '';
// $_['error_email']			= '';
// $_['error_telephone']		= '';
$_['error_fax']					= 'Il numero di fax deve essere compreso tra 3 e 32 caratteri';
// $_['error_password']			= '';
// $_['error_confirm']			= '';
// $_['error_confirm_1']		= 'La password di conferma non corrisponde con la password'; /* this entry is disabled in this version */
$_['error_company']				= 'Il nome dell&#039;azienda deve essere compreso tra 3 e 32 caratteri';

// $_['error_company_id']		= '';
// $_['error_tax_id']			= '';
// $_['error_vat']				= '';

// $_['error_address_1']		= '';
$_['error_address_2']			= 'L&#039;indirizzo 2 deve essere compreso tra 3 e 128 caratteri';
// $_['error_city']				= '';
// $_['error_postcode']			= '';
// $_['error_country']			= '';
// $_['error_zone']				= '';
// $_['error_agree']			= '';
?>