<?php //!
// Heading 
$_['heading_title']      = 'mo';

// Текст
$_['text_account']       = 'Профил';
$_['text_my_account']    = 'Моят профил';
$_['text_my_orders']     = 'Моите поръчки';
$_['text_my_newsletter'] = 'Известия';
$_['text_edit']          = 'Редакция данните в профила';
$_['text_password']      = 'Смяна на парола';
$_['text_address']       = 'Управление на адреси';
$_['text_wishlist']      = 'Преглед на &quot;Желани&quot;';
$_['text_order']         = 'История на поръчките';
$_['text_download']      = 'Е-продукти';
$_['text_reward']        = 'Налични бонус точки'; 
$_['text_return']        = 'История на рекламациите'; 
$_['text_transaction']   = 'История на плащанията'; 
$_['text_newsletter']    = 'Записване / отписване за известяване';
$_['text_recurring']     = 'Периодични плащания';
$_['text_transactions']  = 'Трансакции';
?>