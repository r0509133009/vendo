<?php //!
// Heading 
$_['heading_title']      = 'Вашите бонус точки';

// Column
$_['column_date_added']  = 'Дата на добавяне';
$_['column_description'] = 'Описание';
$_['column_points']      = 'Точки';

// Текст
$_['text_account']       = 'Профил';
$_['text_reward']        = 'Бонус точки';
$_['text_total']         = 'Общо, вашите бонус точки са:';
$_['text_empty']         = 'Все още нямате бонус точки!';
?>