<?php //!
// Heading 
$_['heading_title']         = 'История на поръчките';

// Текст
$_['text_account']          = 'Профил';
$_['text_order']            = 'Информация';
$_['text_order_detail']     = 'Детайли';
$_['text_invoice_no']       = 'Фактура №:';
$_['text_order_id']         = 'Поръчка №:';
$_['text_status']           = 'Статус:';
$_['text_date_added']       = 'Дата на създаване:';
$_['text_customer']         = 'Клиент:';
$_['text_shipping_address'] = 'Адрес за доставка';
$_['text_shipping_method']  = 'Метод на доставка:';
$_['text_payment_address']  = 'Платежен адрес';
$_['text_payment_method']   = 'Начин на плащане:';
$_['text_products']         = 'Артикули:';
$_['text_total']            = 'Общо:';
$_['text_comment']          = 'Пояснения към поръчката';
$_['text_history']          = 'История на поръчките';
$_['text_success']          = 'Успешно добавихте продуктите от Поръчка № #%s в кошницата си!';
$_['text_empty']            = 'Все още нямате направени поръчки!';
$_['text_error']            = 'Заявената от вас поръчка не бе открита!';

// Column
$_['column_name']           = 'Име на продукта';
$_['column_model']          = 'Модел';
$_['column_quantity']       = 'Количество';
$_['column_price']          = 'Цена';
$_['column_total']          = 'Всичко';
$_['column_action']         = 'Действие';
$_['column_date_added']     = 'Дата на добавяне';
$_['column_status']         = 'Статус';
$_['column_comment']        = 'Отзив';
?>
