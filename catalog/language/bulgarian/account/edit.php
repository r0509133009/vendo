<?php //!
// Heading 
$_['heading_title']     = 'Информация за моя профил';

// Текст
$_['text_account']      = 'Профил';
$_['text_edit']         = 'Редакция';
$_['text_your_details'] = 'Лични данни';
$_['text_success']      = 'Профилът ви бе обновен успешно.';

// Entry
$_['entry_firstname']  = 'Име:';
$_['entry_lastname']   = 'Фамилия:';
$_['entry_email']      = 'Е-поща:';
$_['entry_telephone']  = 'Телефон:';
$_['entry_fax']        = 'Факс:';

// Avatar
$_['text_your_avatar']     				= 'Вашите снимки';
$_['text_your_avatar_profile']  		= 'Изображението на потребителския профил';
$_['text_preview_image_title']  		= 'Преглед на изображение';
$_['text_preview_image_description']	= 'Моля, качете изображението на потребителския профил. Можете да го изрежете след това.';
$_['button_upload']  					= 'качване';
$_['button_save']  						= 'запазите промените';
$_['text_transform_image_title']  		= 'запазите промените';
$_['text_your_photo']					= 'Вашите снимки';
$_['text_edit_avatar_success']			= 'Вашата снимка е успешно записан';

// Error
$_['error_exists']     = 'Внимание: Вече има регистриран профил с тази е-поща!';
$_['error_firstname']  = 'Внимание: Името трябва да е между 1 и 32 символа!';
$_['error_lastname']   = 'Внимание: Името трябва да е между 1 и 32 символа!';
$_['error_email']      = 'Внимание: Посоченият адрес на е-поща не е валиден!';
$_['error_telephone']  = 'Внимание: Телефонът трябва да е между 3 и 32 символа!';
?>