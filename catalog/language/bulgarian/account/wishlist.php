<?php //!
// Heading 
$_['heading_title'] = 'Желани продукти';

// Текст
$_['text_account']  = 'Профил';
$_['text_instock']  = 'В наличност';
$_['text_wishlist'] = 'Желани продукти (%s)';
$_['text_login']    = 'За да разгледате списъка с желани продукти <a href="%s">трябва да влезете в сайта </a> или да се регистрирате <a href="%s">от тук</a>';
$_['text_success']  = 'Готово, добавихте <a href="%s">%s</a> към списъка с <a href="%s">желани продукти</a>!';
$_['text_remove']   = 'Готово!';
$_['text_empty']    = 'Нямате Желани продукти.';

// Column
$_['column_image']  = 'Снимка';
$_['column_name']   = 'Продукт';
$_['column_model']  = 'Модел';
$_['column_stock']  = 'Наличност';
$_['column_price']  = 'Цена';
$_['column_action'] = 'Купи/Изтрий';
?>