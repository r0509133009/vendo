<?php //!
$_['heading_title'] = 'Абонаментни плащания';
$_['button_continue'] = 'Продължи';
$_['button_view'] = 'Виж';
$_['button_cancel_profile'] = 'Отмени плащанията';
$_['text_empty'] = 'Няма открит абонаментен профил';
$_['text_product'] = 'Артикул: ';
$_['text_order'] = 'Поръчка: ';
$_['text_quantity'] = 'Брой: ';
$_['text_account'] = 'Профил';
$_['text_action'] = 'Действие';
$_['text_recurring'] = 'Абонаментно плащане';
$_['text_transactions'] = 'Трансакции';
$_['button_return'] = 'Върни';
$_['text_confirm_cancel'] = 'Сигурен ли сте, че искате да закриете профила?';
$_['text_empty_transactions'] = 'Няма трансакции за този профил.';

$_['column_created'] = 'Дата';
$_['column_type'] = 'Тип';
$_['column_amount'] = 'Сума';
$_['column_status'] = 'Статус';
$_['column_product'] = 'Артикул';
$_['column_action'] = 'Действие';
$_['column_profile_id'] = 'Номер на профил (ID)';

$_['text_recurring_detail'] = 'Детайли за абонаментно плащане';
$_['text_recurring_id'] = 'Номер на профил (ID): ';
$_['text_payment_method'] = 'Платежен метод: ';
$_['text_date_added'] = 'Дата: ';
$_['text_recurring_description'] = 'Описание: ';
$_['text_status'] = 'Статус: ';
$_['text_ref'] = 'Връзка: ';

$_['text_status_active'] = 'Включен';
$_['text_status_inactive'] = 'Изключен';
$_['text_status_cancelled'] = 'Закрит';
$_['text_status_suspended'] = 'Спрян';
$_['text_status_expired'] = 'Изтекъл';
$_['text_status_pending'] = 'Изчакващ';

$_['text_transaction_created'] = 'Дата';
$_['text_transaction_payment'] = 'Плащане';
$_['text_transaction_outstanding_payment'] = 'За плащане';
$_['text_transaction_skipped'] = 'Прескочена трансакция';
$_['text_transaction_failed'] = 'Неуспешна трансакция';
$_['text_transaction_cancelled'] = 'Отменена трансакция';
$_['text_transaction_suspended'] = 'Спряна трансакция';
$_['text_transaction_suspended_failed'] = 'Спряно след неуспешна трансакция';
$_['text_transaction_outstanding_failed'] = 'Неуспешно плащане';
$_['text_transaction_expired'] = 'Изтекла трансакция';

$_['error_not_cancelled'] = 'Грешка: %s';
$_['error_not_found'] = 'Внимание: Профилът не може да бъде закрит.';
$_['success_cancelled'] = 'Внимание: Абонаментното плащане бе премахнато.';
?>