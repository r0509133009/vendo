<?php //!
// Heading 
$_['heading_title']      = 'Връщане на продукти';

// Текст
$_['text_account']       = 'Профил';
$_['text_return']        = 'Връщане на продукти';
$_['text_return_detail'] = 'Информация';
$_['text_description']   = '<p>Моля, попълнете формуляра по-долу, за издаване на входящ номер.</p>';
$_['text_order']         = 'Информация';
$_['text_product']       = 'Информация за продукта и причини за връщането му';
$_['text_message']       = '<p>Вашата молба за връщане на продукт е приета и подадена към съответния отдел за обработка.</p><p>Ще ви информираме за движението й на посочената от вас е-поща.</p>';
$_['text_return_id']     = 'Входящ №:';
$_['text_order_id']      = 'Поръчка №:';
$_['text_date_ordered']  = 'Дата на поръчката:';
$_['text_status']        = 'Статус:';
$_['text_date_added']    = 'Дата на създаване:';
$_['text_customer']      = 'Клиент:';
$_['text_comment']       = 'Пояснения към връщането';
$_['text_history']       = 'История на връщанията';
$_['text_empty']         = 'Нямате направени връщания!';
$_['text_error']         = 'Заявеното връщане не бе намерено!';

// Column
$_['column_product']     = 'Име на продукта';
$_['column_model']       = 'Модел';
$_['column_quantity']    = 'Количество';
$_['column_price']       = 'Цена';
$_['column_opened']      = 'Отварян';
$_['column_comment']     = 'Отзив';
$_['column_reason']      = 'Причина';
$_['column_action']      = 'Действие';
$_['column_date_added']  = 'Дата на добавяне';
$_['column_status']      = 'Статус';

// Entry
$_['entry_order_id']     = 'Поръчка №:';
$_['entry_date_ordered'] = 'Дата на поръчката:';
$_['entry_firstname']    = 'Име:';
$_['entry_lastname']     = 'Фамилия:';
$_['entry_email']        = 'е-поща:';
$_['entry_telephone']    = 'Телефон:';
$_['entry_product']      = 'Име на продукта:';
$_['entry_model']        = 'Код на продукта:';
$_['entry_quantity']     = 'Количество:';
$_['entry_reason']       = 'Причина за връщането';
$_['entry_opened']       = 'Продуктът е отварян:';
$_['entry_fault_detail'] = 'Описание на проблема (неизправност или други подробности):';
$_['entry_captcha']      = 'Въведете в полето кода от картинката по-долу:';

// Error
$_['text_error']         = 'Поисканото от вас не бе намерено!';
$_['error_order_id']     = 'Изисква се номер на поръчката!';
$_['error_firstname']    = 'Името трябва да е между 1 и 32 символа!';
$_['error_lastname']     = 'Името трябва да е между 1 и 32 символа!';
$_['error_email']        = 'Невалиден адрес на е-поща!';
$_['error_telephone']    = 'Телефонът трябва да е между 3 и 32 символа!';
$_['error_product']      = 'Името на продукта трябва да е между 3 и 255 символа!';
$_['error_model']        = 'Името на модела на продукта трябва да е между 3 и 64 символа!';
$_['error_reason']       = 'Трябва да посочите причина за връщането!';
$_['error_captcha']      = 'Въведеният код не съвпада с кода от картинката!';
$_['error_agree']        = 'Внимание: трябва да се съгласите с %s!';
?>