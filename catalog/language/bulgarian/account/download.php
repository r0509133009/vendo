<?php //!
// Heading 
$_['heading_title']   = 'Електронни продукти';

// Текст
$_['text_account']    = 'Профил';
$_['text_downloads']  = 'Е-продукти';
$_['text_order']      = 'Номер на поръчка:';
$_['text_date_added'] = 'Дата на добавяне:';
$_['text_name']       = 'Име:';
$_['text_remaining']  = 'Оставащи:';
$_['text_size']       = 'Размер:';
$_['text_empty']      = 'Досега нямате поръчки, свързани с електронен продукт (за изтегляне)!';
?>