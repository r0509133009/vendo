<?php //!
// Текст
$_['text_home']     	  = 'Магазин';
$_['text_wishlist']		  = 'Желани продукти (%s)';
$_['text_shopping_cart']  = 'Кошница';
$_['text_search']         = 'Търсене';
$_['text_welcome']        = 'Добре дошли! <a href="%s">Влезте</a> или създайте <a href="%s">нов профил</a>!';
$_['text_logged']         = 'Идентифицирахте се като <a href="%s">%s</a>. (<a href="%s">Изход</a>)';
$_['text_account']        = 'Моят профил';
$_['text_checkout']       = 'Плащане';
?>