<?php //!
// Текст
$_['text_information']  = 'Информация';
$_['text_service']      = 'Обслужване на клиенти';
$_['text_extra']        = 'Екстри';
$_['text_contact']      = 'Контакт с нас';
$_['text_return']       = 'Връщане на продукти';
$_['text_sitemap']      = 'Съдържание';
$_['text_manufacturer'] = 'Производители';
$_['text_voucher']      = 'Подарете ваучер';
$_['text_affiliate']    = 'Станете партньор';
$_['text_special']      = 'Намаления';
$_['text_account']      = 'За регистрирани';
$_['text_order']        = 'История на поръчките';
$_['text_wishlist']     = 'Желани продукти';
$_['text_newsletter']   = 'Известия';
$_['text_powered']      = '<a href="http://www.opencart.bg">Онлайн магазин</a><br /> %s &copy; %s';
?>