<?php //!
// Heading
$_['heading_title'] = 'Благодарим ви за направената поръчка!';

// Текст
$_['text_customer'] = '<p>Можете да преглеждате историята на вашите поръчки в профила си, като изберете <a href="%s">История на поръчките</a>.</p>Можете да прегледате вашите бонус точки в профила си като изберете <a href="http://antistar.me/index.php?route=account/reward">Бонус Точки.</a> Благодарим ви, че пазарувате от Antistar.me!';
$_['text_guest']    = '<p>Поръчката ви бе приета успешно!</p><p>При въпроси, моля <a href="%s">пишете ни</a>.</p><p> Благодарим ви, че пазарувате от Antistar.me!</p>';
$_['text_basket']   = 'Кошница';
$_['text_checkout'] = 'Плащане';
$_['text_success']  = 'Готово!';

$_['text_head_greeting_h'] = 'Поръчката ви е приета успешно!';
$_['text_head_payment_type'] = 'Начин на плащане: %s';
$_['text_history'] = '<a href="%s" target="_blank">&#8222;История на поръчките&#8221;</a>';
$_['text_reward'] = '<a href="%s" target="_blank">&#8222;Бонус Точки&#8221;</a>';
$_['text_content_bottom_account_reward'] = 'Изпратихме ви потвърждаващ e-mail с данните от поръчката.  Можете да преглеждате  историята на поръчките 
в профила си от линка %s. Следете баланса на вашите бонус точки, в профила си като изберете линка %s';

$_['text_content_bottom_text'] = 'До 48 часа след получаване на вашето плащане, статусът на поръчката ви ще бъде променен от  &#34;Изчаква Плащане&#34; на &#34;Изпратена&#34; това ознчава, че поръчката ви е била изпратена и предстои да я получите в указаният срок за получаване.При възникнали въпроси можете да се свържете с нас по телефон или чат на живо от менюто в дъното на сайта. ';
$_['text_bottom_thanks'] = 'Благодарим ви, че избрахте услугите на ';

///by post
//$_['text_head_payment_offline_type_name'] = 'Български пощи - пощенски запис';
$_['text_head_payment_offline_description'] = 'ОТИДЕТЕ В КЛОН НА БЪЛГАРСКИ ПОЩИ И ПОПЪЛНЕТЕ СТАНДАРТНА БЛАНКА ЗА &#8222;ПОЩЕНСКИ ЗАПИС&#8221;';
//$_['text_content_payment_offline_total_details'] = 'Моля, изпратете пощенски запис на стойност: %s';
$_['text_content_payment_offline_total_details'] = '<span>Моля, изпратете пощенски запис на стойност:</span> <span class="offline-total">%s</span>';
$_['text_content_payment_offline_to'] = '<span>до получател:</span> <span class="payment-offline-receiver">Пламена Бончева Вълчева,<br>Пловдив 4000, До поискване</span>';
$_['text_content_payment_offline_order_id'] = '<span>В полето за допълнителни бележки напишете номера на Вашата поръчка:</span> <span class="offline-order-id">%s</span>';
$_['text_content_payment_offline_link'] = '<a href="bgpost.bg/bg/16?" target="_blank">ВИЖТЕ АДРЕСИТЕ НА ВСИЧКИ ПОЩЕНСКИ КЛОНОВЕ ТУК</a>';

$_['text_content_bottom_offline_additional_info'] = 'Ако желаете плащането ви да бъде отразено по-бързо, моля да ни изпратите е-мейл с прикачена снимка на оригиналната бланка „пощенски запис“ с която сте изпратили записа с печат от пощата на е-мейл: <span class="offline-email">365vendo@gmail.com</span> ';
///bank transfer  
$_['text_head_payment_bank_transfer_description'] = 'ЗА ДА ПЛАТИТЕ ВАШАТА ПОРЪЧКА НАПРАВЕТЕ БАНКОВ ПРЕВОД СЪС СЛЕДНИТЕ ПАРАМЕТРИ';
$_['text_content_payment_bank_transfer_to'] = '<span>до титуляр:</span> <span class="payment-offline-receiver">ВЕНДО МАРКЕТ ООД<br>  IBAN: BG85BGUS91601005856000<br>BIC: BGUSBGSF - Българо Американска Кредитна Банка</span>';
$_['text_content_payment_bank_transfer_order_id'] = '<span>Като основание за плащането в полето<br> „Детайли за плащане/Мотив, напишете<br>
задължително номера на Вашата поръчка:</span> <span class="offline-order-id">%s</span>';
$_['text_content_payment_bank_transfer_total_details'] = '<span>Моля, изпратете пощенски запис на стойност:</span> <span class="offline-total">%s</span>';

//econt
$_['text_head_payment_cod_description'] = 'ЩЕ ВИ СЕ ОБАДИМ, ЗА ДА УТОЧНИМ УДОБЕН ЗА ВАС ЧАС, ЗА ПОСЕЩЕНИЕ ОТ КУРИЕР НА ЕКОНТ, НА КОЙТО ДА ПРЕДПЛАТИТЕ В БРОЙ ВАЩАТА ПОРЪЧКА. ПОЩЕНСКИ ПАРИЧЕН ПРЕВОД МОЖЕТЕ ДА ИЗПРАТИТЕ  СЪЩО И ОТ ВСЕКИ ОФИС НА ЕКОНТ.';
$_['text_content_payment_cod_to'] = '<span>получател:</span> <span class="payment-offline-receiver">Вендо Маркет ООД<br>тел: 0897 610 747</span>';
$_['text_content_payment_cod_total_details'] = '<span>Общо стойност : </span> <span class="offline-total">%s</span>';
$_['text_content_payment_cod_order_id'] = '<span>номер поръчка:</span> <span class="offline-order-id">%s</span>';
$_['text_content_payment_cod_link'] = '<a href="http://www.econt.com/offices/" target="_blank">ВИЖТЕ НАЙ-БЛИЗКАТА ОФИС ДО ВАС ОТ ТУК</a>';

//easy pay
$_['text_head_payment_easypay_description'] = 'ЗА ДА НАПРАВИТЕ ПЛАЩАНЕ ЧРЕЗ EASY PAY,МОЛЯ СЛЕДВАЙТЕ ИНСТРУКЦИИТЕ ПО-ДОЛУ';
$_['text_content_payment_easypay_order_id'] = '<span>Номер на поръчка:</span> <span class="easypay-order-id">%s</span>';
$_['text_content_payment_easypay_total_details'] = '<span>Направете плащане на каса на Easy Pay на стойност:</span> <span class="offline-total">%s</span>';
$_['text_content_payment_easypay_link'] = '<a href="http://www.easypay.bg/?p=offices" target="_blank">ВИЖТЕ НАЙ-БЛИЗКАТА КАСА ДО ВАС ОТ ТУК</a>';

//paypal

$_['text_content_payment_pp_standard_order_id'] = '<span>Номер на вашата поръчка:</span> <span class="offline-order-id">%s</span>';
$_['text_content_payment_pp_standard_total_details'] = '<span>Успешно направихте плащане на стойност:</span> <span class="offline-total">%s</span>';

//paypal

$_['text_content_payment_borica_order_id'] = '<span>Номер на вашата поръчка:</span> <span class="offline-order-id">%s</span>';
$_['text_content_payment_borica_total_details'] = '<span>Успешно направихте плащане на стойност:</span> <span class="offline-total">%s</span>';

?>