<?php //!
// Текст
$_['text_subject']  = '%s - Партньорска програма';
$_['text_welcome']  = 'Благодарим ви за регистрирането ви в Партньорската програма на %s!';
$_['text_approval'] = 'Вашата заявка очаква разглеждане от администратор. Веднага след като бъде одобрена, ще можете да влезете в системата с посочената от вас при регистрацията е-поща и парола на следния адрес:';
$_['text_services'] = 'Когато вече сте в системата, ще можете да създавате и използвате проследяващи кодове и връзки, да следите комисионите и плащанията, според резултатите от партньорската ви активност, да управлявате информацията за профила си и др.';
$_['text_thanks']   = 'Благодарим ви и ви пожелаваме успех!';
?>