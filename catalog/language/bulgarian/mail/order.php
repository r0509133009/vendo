<?php //!
// Текст
$_['text_new_subject']          = '%s - Поръчка %s';
$_['text_new_greeting']         = 'Благодарим ви, че пазарувате от %s!<br />Вашата поръчка е приета за обработка!';
$_['text_new_received']         = 'Имате нова поръчка.';
$_['text_new_link']             = 'Можете да видите поръчката си на адрес:';
$_['text_new_order_detail']     = 'Данни за поръчката';
$_['text_new_instruction']      = 'Инструкции';
$_['text_new_order_id']         = 'Поръчка №:';
$_['text_new_date_added']       = 'Дата на създаване:';
$_['text_new_order_status']     = 'Статус на поръчката:';
$_['text_new_payment_method']   = 'Начин на плащане:';
$_['text_new_shipping_method']  = 'Метод на доставка:';
$_['text_new_email']  			= 'е-поща:';
$_['text_new_telephone']  		= 'Телефон:';
$_['text_new_ip']  				= 'IP Адрес:';
$_['text_new_payment_address']  = 'Платежен адрес';
$_['text_new_shipping_address'] = 'Адрес за доставка';
$_['text_new_products']         = 'Артикули';
$_['text_new_product']          = 'Продукт';
$_['text_new_model']            = 'Модел';
$_['text_new_quantity']         = 'Количество';
$_['text_new_price']            = 'Цена';
$_['text_new_order_total']      = 'Сума на поръчката';	
$_['text_new_total']            = 'Всичко';	
$_['text_new_download']         = 'След като плащането ви бъде потвърдено, ще можете да достигнете до продуктите си за изтегляне на адреса по-долу:';
$_['text_new_comment']          = 'Поясненията към поръчката ви са:';
$_['text_new_footer']           = 'В случай, че имате въпроси, натиснете "Отговор" на това е-писмо.';
$_['text_new_powered']          = 'Продукт на <a href="http://www.vendo.bg"> VENDO </a>.';
$_['text_update_subject']       = '%s - Обновяване на поръчка %s';
$_['text_update_order']         = 'Поръчка №:';
$_['text_update_date_added']    = 'Дата на поръчката:';
$_['text_update_order_status']  = 'Вашата поръчка бе обновена и вече има следния статус:';
$_['text_update_comment']       = 'Поясненията към поръчката ви са:';
$_['text_update_link']          = 'Можете да видите поръчката си на адрес:';
$_['text_update_footer']        = 'В случай, че имате въпроси, натиснете "Отговор" на това е-писмо.';
$_['text_new_image'] = 'Снимка:';
?>