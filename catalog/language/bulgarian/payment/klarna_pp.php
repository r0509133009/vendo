<?php //!
// Текст
$_['text_title']       = 'Klarna Плащане';
$_['text_information'] = 'Klarna Информация';
$_['text_additional']  = 'Klarna се нуждае от допълнителна информация, за да могат да обработят поръчката ви.';
$_['text_wait']        = 'Моля изчакайте!';
$_['text_male']        = 'Мъж';
$_['text_female']      = 'Жена';

// Entry
$_['entry_gender']     = 'Пол:';
$_['entry_pno']        = 'ЕГН / Дата на раждане:<span class="help">(07071960)</span>';
$_['entry_house_no']   = 'Домашен телефонен номер:';
$_['entry_house_ext']  = 'Код:';
$_['entry_cellno']     = 'Мобилен телефонен номер:';

// Error
$_['error_gender']     = 'Внимание: Посочете пол!';
$_['error_dob']        = 'Внимание: Въведете ЕГН / Дата на раждане!';
$_['error_house_no']   = 'Внимание: Посочете домашен телефонен номер!';
$_['error_house_ext']  = 'Внимание: Посочете код!';
?>