<?php

/**
 * Easypay.bg payment gateway for Opencart by Extensa Web Development
 *
 * Copyright © 2012-2015 Extensa Web Development Ltd. All Rights Reserved.
 * This file may not be redistributed in whole or significant part.
 * This copyright notice MUST APPEAR in all copies of the script!
 *
 * @author 		Extensa Web Development Ltd. (www.extensadev.com)
 * @copyright	Copyright (c) 2012-2015, Extensa Web Development Ltd.
 * @package 	Easypay.bg payment gateway
 * @link		http://www.opencart.com/index.php?route=extension/extension/info&extension_id=5303
 */

// Text
$_['text_title']   = 'В брой на каса на EasyPay';
$_['text_payment'] = 'За да направите плащане чрез EasyPay, моля запишете вашия платежен код и посетете каса на EasyPay. <br />
Можете да видите най-близките офиси от тук: <a href="https://www.easypay.bg/site/?p=offices" target="_blank">Карта на офисите на EasyPay</a> .';
$_['text_code']    = 'Вашият платежен код в EasyPay е:';

$_['text_failed']  = 'Възникна грешка, моля опитайте отново.';
?>