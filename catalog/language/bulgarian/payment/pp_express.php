<?php //!
// Текст
$_['text_title']                    = 'Плащане с PayPal Express';
$_['button_continue']               = 'Продължи';
$_['text_cart']                     = 'Количка';
$_['text_shipping_updated']         = 'Данните за доставка са обновени';
$_['text_trial']                    = '%s всеки %s %s за %s плащания, то ';
$_['text_recurring']                = '%s всеки %s %s';
$_['text_length']                   = ' за %s плащания';

// Standard checkout error page
$_['error_heading_title']           = 'Възникна грешка';
$_['error_too_many_failures']       = 'Твърде много грешни опити за удостоверяване.';

// Express confirm page
$_['express_text_title']            = 'Потвърждаване на поръчката';
$_['express_button_coupon']         = 'Добави';
$_['express_button_confirm']        = 'Потвърди';
$_['express_button_login']          = 'Към PayPal';
$_['express_button_shipping']       = 'Обнови доставката';
$_['express_entry_coupon']          = 'Въведете своя купон (код) тук:';
?>