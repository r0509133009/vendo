<?php //!
// Entry
$_['entry_postcode'] = 'Пощ. код:';
$_['entry_country']  = 'Страна:';
$_['entry_zone']     = 'Регион / Област:';
$_['text_title']     = 'Банкова карта (чрез Google Checkout)';

// Error
$_['error_shipping'] = 'Внимание: Посочването на метод на доставка е задължително!';
?>