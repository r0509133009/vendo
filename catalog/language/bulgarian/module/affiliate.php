<?php //!
// Heading 
$_['heading_title']    = 'Партньор-комисионер';

// Текст
$_['text_register']    = 'Регистрация';
$_['text_login']       = 'Вход';
$_['text_logout']      = 'Изход';
$_['text_forgotten']   = 'Забравена парола';
$_['text_account']     = 'Моят профил';
$_['text_edit']        = 'Редакция на профила';
$_['text_password']    = 'Парола';
$_['text_payment']     = 'Плащане';
$_['text_tracking']    = 'Активност';
$_['text_transaction'] = 'Трансакции';
?>
