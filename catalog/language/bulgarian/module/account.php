<?php //!
// Heading 
$_['heading_title']    = 'Профил';

// Текст
$_['text_register']    = 'Регистрация';
$_['text_login']       = 'Вход';
$_['text_logout']      = 'Изход';
$_['text_forgotten']   = 'Забравена парола';
$_['text_account']     = 'Моят профил';
$_['text_edit']        = 'Редакция на профила';
$_['text_password']    = 'Парола';
$_['text_address']     = 'Адресни книги';
$_['text_wishlist']    = 'Желани продукти';
$_['text_order']       = 'История на поръчките';
$_['text_download']    = 'Е-продукти';
$_['text_return']      = 'Връщане на продукти';
$_['text_transaction'] = 'Трансакции';
$_['text_newsletter']  = 'Бюлетин';
$_['text_recurring']   = 'Периодични плащания';
?>
