<?php 

$_['option_register_payment_address_title']  		= 'Адрес за плащане';
$_['option_register_payment_address_description']  	= '';
$_['option_register_shipping_address_title']  		= 'Адрес за доставка';
$_['option_register_shipping_address_description']  = '';
$_['option_register_shipping_method_title']  		= 'Начин на доставка';
$_['option_register_payment_method_title'] 			= 'Начин на плащане';

$_['option_guest_payment_address_title']  			= 'Адрес за плащане';
$_['option_guest_payment_address_description']  	= '';
$_['option_guest_shipping_address_title']  			= 'Адрес за доставка';
$_['option_guest_shipping_address_description'] 	= '';
$_['option_guest_shipping_method_title']  			= 'Начин на доставка';
$_['option_guest_payment_method_title'] 			= 'Начин на плащане';

$_['option_logged_payment_address_title']  			= 'Адрес за плащане';
$_['option_logged_payment_address_description']  	= '';
$_['option_logged_shipping_address_title']  		= 'Адрес за доставка';
$_['option_logged_shipping_address_description'] 	= '';
$_['option_logged_shipping_method_title']  			= 'Начин на доставка';
$_['option_logged_payment_method_title'] 			= 'Начин на плащане';





$_['step_option_guest_desciption'] = 'При поръчка като гост не получавате БОНУС ТОЧКИ';

$_['error_step_payment_address_fields_company'] = 'Company name required more the 3 and less the 128 characters';
$_['error_step_shipping_address_fields_company'] = 'Company name required more the 3 and less the 128 characters';

$_['error_step_confirm_fields_comment'] = 'Please fill in the comment to the order';


?>