<?php
// Text
$_['button_remove']					= 'Изтрий';
$_['text_continue']					= 'Продължи пазаруването';
$_['text_empty']					= 'Нямате добавени продукти!';
$_['text_total_qucik_ckeckout']  	= 'Продуктите са на стойност<big><b>%s</b></big> products on <big><b>%s</b></big>';
$_['sendthis']				='Поръчка по телефон';
$_['callback']				='Позвъняване по телефон';
$_['namew']					= 'Име:';
$_['phonew']				= 'Телефон:';
$_['comment_buyer']			= 'Бележки към поръчката:';
$_['email_buyer']			= 'Твоят E-mail:';
$_['theme']					= 'Позвъняване по телефона:';
$_['messagew']				= 'Съобщение:';
$_['cancel']				= 'Отмяна';
$_['button_send']			= 'ПОРЪЧАЙ';
$_['button_continue']			= 'ПРОДЪЛЖИ ПАЗАРУВАНЕТО';
$_['time']					= 'Ще се свържем с вас в рамките на работния ден';
$_['ok']					= 'Съобщението ти беше изпратено, поръчката ти в въведена под № ';
$_['erno']					= 'Неочаквана грешка.';
$_['die']					= 'Съжаляваме съобщението ти не беше изпратено успешно.';
$_['error_phone']			= 'Моля въвдеи твоят телфон';
$_['mister']				= 'Име ?';
$_['subject']				= 'ПОРЪЧКА';
$_['text_1']				= 'Имате нова поръчка';
$_['name']					= 'Име: ';
$_['phone']					= 'Телефон: ';
$_['comment_buyer']			= 'Съобщение: ';
$_['url_callback']			= 'Допълнително за поръчката: ';
$_['error_required']        = ' %s моля попълнете формата!';

$_['comment_buyer_error']	= 'Моля попълнете задължителните полета';
$_['email_buyer_error']		= 'Моля попълни твоят е-мейл адрес';

$_['text_column_name']		= 'По име';
$_['text_column_price']		= 'По цена:';
$_['text_column_quantity']	= 'Количество';

$_['text_column_photo_product']		= 'снимка на продукта';
$_['text_column_name_product']		= 'име на продукта';
$_['text_column_quantity_product']	= 'количество';
$_['text_column_price_product']		= 'цена';
$_['text_column_total_product']		= 'крайна цена';

$_['text_photo']			= 'снимка';
$_['text_new_product']		= 'продукт';
$_['text_new_model']		= 'модел';
$_['text_new_quantity']		= 'количество';
$_['text_new_price']		= 'цена';
$_['text_new_total']		= 'крайна цена';
$_['continue_shopping']		= 'продължи пазаруването';
$_['text_quick_order_enter_name_phone']         = 'Моля попълния твоят телефонен номер!';
?>
