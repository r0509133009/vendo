<?php //!
// Heading 
$_['heading_title'] = 'Кошница';

// Text
$_['text_items']    = '%s продукт(а) - %s';
$_['text_empty']    = 'Кошницата е празна!';
$_['text_cart']     = 'Виж кошницата';
$_['text_checkout'] = 'Плащане';

$_['text_payment_profile'] = 'Платежен профил';
$_['text_quantity'] = 'бр.';
?>