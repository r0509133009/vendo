<?php
$_['heading_title'] = 'Чат с оператор';

$_['text_operator'] = 'Оператор';
$_['text_wait']     = 'Моля изчакайте';
$_['text_volume']   = 'звуци';

$_['entry_livehelp_name']       = 'Вашето име:';
$_['placeholder_livehelp_name'] = 'Име';
$_['button_select_operator']    = 'Оператори на линия';
$_['button_sign']               = 'Вход';
$_['error_operator_id']         = 'Необходимо е да изберете оператор!';
$_['error_name']                = 'Името трябва да е с дължина от 3 до 64 символа';
$_['text_signout_confirm']      = 'Изход и прекратяване на чата с оператор';
$_['text_signout']              = 'Изход';
$_['text_description']          = 'Изберете оператор на линия с който желаете да се свържете.';
$_['error_chat_closed']         = 'Чата прекъсна';
$_['text_muted_for_time']       = 'Не можете да пишете за %s секунди!';
$_['text_offline_description']  = 'В момента няма оператор на линия, но можете да ни напишете съобщение';
$_['entry_livehelp_email']      = 'Вашият имейл адрес:';
$_['entry_livehelp_enquiry']    = 'Съобщение:';
$_['error_user_enquiry']        = 'Съобщението трябва да бъде от 10 до 250 символа!';
$_['error_user_email']          = 'Въведете валиден имейл адрес!';
$_['livehelp_email_subject']    = 'Ново съобщение от %s';
$_['success_email_sent']        = 'Благодарим ви че ни писахте, очаквайте да се свържем с вас!';
$_['button_send']               = 'Изпрати';

// BBCodes
$_['title_bold']      = 'Удебелен шрифт';
$_['title_italic']    = 'Наклонен';
$_['title_underline'] = 'Подчертан';
$_['title_link']      = 'Добави линк';
$_['title_image']     = 'Добави снимка';
$_['title_youtube']   = 'Добави видео от YouTube';

// JS localisation
$_['text_js_operator_signed']       = 'Операторът се присъедини към чата';
$_['text_js_operator_online_again'] = 'Операторът е на линия отново';
$_['text_js_operator_away']         = 'Операторът не е на линия в момента';
$_['error_js_kicked']               = 'Чатът беше прекратен!';
$_['text_js_muted_for_time']        = 'Не можете да пишете още %s секунди!';
$_['text_js_entry_url']            = 'въведете Линк';
$_['text_js_entry_youtube']         = 'въведете линк от YouTube';
?>