<?php
// Heading 
$_['heading_title_1']       = 'Вход';
$_['heading_title_2']       = 'Моят акаунт';

// Text
$_['text_register']         = 'Регистрация';
$_['text_login']            = 'Вход';
$_['text_logout']           = 'Изход';
$_['text_forgotten']        = 'Забравена парола';
$_['text_account']          = 'ПРОФИЛ';
$_['text_edit']             = 'Профил';
$_['text_password']         = 'Парола';
$_['text_address']          = 'Моите адреси';
$_['text_wishlist']         = 'Желани';
$_['text_order']            = 'Моите поръчки';
$_['text_download']         = 'Даунлоуди';
$_['text_return']           = 'Връщане';
$_['text_transaction']      = 'Транзакции';
$_['text_recurring']      = 'Абонаментни плащания';
$_['text_newsletter']       = 'Новини';
$_['entry_email_address']	= 'E-mail адрес:';
$_['entry_password']    	= 'Парола:';
$_['text_welcome']			= 'Здравейте';
$_['text_logged']   	    = 'Добре дошли, <strong>%s</strong>';
$_['text_guest']	        = ', <strong>visitor</strong>.';
$_['text_my_newsletter']    = 'Новини';
$_['text_my_account']       = 'Моят акаунт';
$_['text_my_orders']        = 'Моите поръчки';
$_['text_reward']           = 'Бонус точки';
$_['text_id_number']       	= 'ID номер :';


$_['text_my_account_a']      = 'Моят афилиейт акаунт';
$_['text_my_tracking']     = 'Моята тракинг информация';
$_['text_my_transactions'] = 'Моите транзакции';

//Text Improved
$_['text_edit_desc']       	 	= 'Редакция на данни.';
$_['text_password_desc']   	 	= 'Промяна на парола.';
$_['text_address_desc']     	= 'Промяна на адресите.';
$_['text_wishlist_desc']    	= 'Промяна на листа с желания';
$_['text_order_desc']       	= 'Преглед на поръчки';
$_['text_download_desc']    	= 'Вашите даунлоуди';
$_['text_reward_desc']      	= 'Бонус точки';
$_['text_return_desc']      	= 'Преглед на заявки за връщане';
$_['text_transaction_desc'] 	= 'Преглед на транзакциите.';
$_['text_newsletter_desc']  	= 'Записване / Отписване за новини.';
$_['text_logout_desc']      	= 'Изход от вашия акаунт.';
$_['text_total_balance_a'] 		= 'В момента разполагате с:';
$_['text_total_reward_a']   	= 'Вашите бонус точки са общо';
$_['text_total_reward_a_desc']	= '100 точки = 5,00 лв';
$_['text_total_register_a'] 	= 'Регистриран в Antistar.me на:';
$_['text_greeting_a']      		= 'Здравейте отново, %s';

//Text Improved
$_['text_edit_desc_a']        = 'Редакция на данни.';
$_['text_password_desc_a']    = 'Промяна на парола.';
$_['text_payment_desc_a']     = 'Промяна на метод за плащане.';
$_['text_wishlist_desc_a']    = 'Промяна на листа с желания';
$_['text_tracking_desc_a']    = 'Уникален афилиейт тракинг код.';
$_['text_reward_desc_a']      = 'Бонус точки';
$_['text_return_desc_a']      = 'Преглед на заявки за връщане';
$_['text_transaction_desc_a'] = 'Преглед на транзакциите.';
$_['text_newsletter_desc_a']  = 'Записване / Отписване за новини.';
$_['text_logout_desc_a']      = 'Изход от вашия акаунт.';
$_['text_payment_a']      = 'Плащане';
$_['text_tracking_a']      = 'Тракинг код';
$_['text_greeting_af']      = 'Здравейте отново, %s';
$_['text_total_balance_af']  = 'В момента разполагате с:';
$_['text_total_register_af'] = 'Регистриран в Antistar.me на:';
$_['text_total_commi_af'] = 'Комисионна:';
$_['text_total_order_af'] = 'Общо поръчки: ';

?>