<?php
// Heading 
$_['heading_title']        = 'Вашият акаунт';

// Text
$_['text_account']         = 'Акаунт';
$_['text_my_account']      = 'Афилиейт акаунт';
$_['text_my_tracking']     = 'Тракинг код';
$_['text_my_transactions'] = 'Моите трансакции';
$_['text_edit']            = 'Редакция на данни';
$_['text_password']        = 'Промяна на парола';
$_['text_payment']         = 'Промяна на метод на изплащане на комисионни';
$_['text_tracking']        = 'Къстъм тракинг код';
$_['text_transaction']     = 'Преглед на историята';

$_['text_clicks_last10'] 	= 'Кликове: Последните 10 дни';
$_['text_no_data'] 			= 'Няма данни!';
$_['text_event']			= 'Събитие';
$_['text_balance'] 			= 'Баланс:';
$_['text_stats_day_month'] 	= 'Статистика по дни:';
$_['text_date'] = 'Дата';
$_['text_impression'] = 'Импресии';
$_['text_click'] = 'Кликове';
$_['text_sale'] = 'Продажби';
$_['text_amount'] = 'сума';
$_['text_ctr'] = 'CTR';
$_['text_cr'] = 'CR';
$_['text_total_month'] = 'Общо за месеца:';