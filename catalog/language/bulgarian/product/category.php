<?php //!
// Текст
$_['text_refine']       = 'Филтрирай по категория';
$_['text_product']      = 'Артикули';
$_['text_error']        = 'Категорията не бе намерена!';
$_['text_empty']        = 'Няма добавени продукти в тази категория.';
$_['text_quantity']     = 'Брой:';
$_['text_manufacturer'] = 'Производител:';
$_['text_model']        = 'Код на продукта:'; 
$_['text_points']       = 'Наградни точки:'; 
$_['text_price']        = 'Цена:'; 
$_['text_tax']          = 'Без данък:'; 
$_['text_reviews']      = 'На база %s отзива.'; 
$_['text_compare']      = 'Продукти, добавени за сравняване: %s'; 
$_['text_display']      = 'Изглед:';
$_['text_list']         = 'Списъчен';
$_['text_grid']         = 'Табличен';
$_['text_sort']         = 'Ред:';
$_['text_default']      = 'Основен';
$_['text_name_asc']     = 'Име (възх.)';
$_['text_name_desc']    = 'Име (низх.)';
$_['text_price_asc']    = 'Цена (възх.)';
$_['text_price_desc']   = 'Цена (низх.)';
$_['text_rating_asc']   = 'Рейтинг (възх.)';
$_['text_rating_desc']  = 'Рейтинг (низх.)';
$_['text_model_asc']    = 'Модел (възх.)';
$_['text_model_desc']   = 'Модел (низх.)';
$_['text_limit']        = 'Покажи:';
?>