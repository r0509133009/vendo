<?php //!
// Heading
$_['heading_title']     = 'Открийте любимата ви марка';

// Текст
$_['text_brand']        = 'Производител';
$_['text_index']        = 'Азбучен указател на производителите:';
$_['text_error']        = 'Производителят не бе намерен!';
$_['text_empty']        = 'Няма налични продукти.';
$_['text_quantity']     = 'Брой:';
$_['text_manufacturer'] = 'Производител:';
$_['text_model']        = 'Код на продукта:'; 
$_['text_points']       = 'Бонус точки:'; 
$_['text_price']        = 'Цена:'; 
$_['text_tax']          = 'Без данък:'; 
$_['text_reviews']      = 'На база %s отзива.'; 
$_['text_compare']      = 'Продукти, добавени за сравняване: %s'; 
$_['text_display']      = 'Изглед:';
$_['text_list']         = 'Списъчен';
$_['text_grid']         = 'Табличен';
$_['text_sort']         = 'Ред:';
$_['text_default']      = 'Основен';
$_['text_name_asc']     = 'Име (възх.)';
$_['text_name_desc']    = 'Име (низх.)';
$_['text_price_asc']    = 'Цена (възх.)';
$_['text_price_desc']   = 'Цена (низх.)';
$_['text_rating_asc']   = 'Рейтинг (възх.)';
$_['text_rating_desc']  = 'Рейтинг (низх.)';
$_['text_model_asc']    = 'Модел (възх.)';
$_['text_model_desc']   = 'Модел (низх.)';
$_['text_limit']        = 'Покажи:';
?>