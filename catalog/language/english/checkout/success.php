<?php
// Heading
$_['heading_title'] = 'Thanks for your order!';

// Text
$_['text_customer'] = '<p>You can view your order history by going to the <a href="%s">MY ACCOUNT</a> page and by clicking on <a href="%s">ORDER HISTORY</a>.</p><p>If you need any information drop us a message <a href="%s">HERE</a>.</p>';
$_['text_guest']    = '<p>Your order has been successfully processed!</p><p>Please direct any questions you have to the <a href="%s">store owner</a>.</p><p>Thanks for your order!</p>';
$_['text_basket']   = 'Shopping Cart';
$_['text_checkout'] = 'Checkout';
$_['text_success']  = 'Success';
?>