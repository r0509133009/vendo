<?php

/**
 * eBORICA payment gateway for Opencart by Extensa Web Development
 *
 * Copyright © 2011-2015 Extensa Web Development Ltd. All Rights Reserved.
 * This file may not be redistributed in whole or significant part.
 * This copyright notice MUST APPEAR in all copies of the script!
 *
 * @author 		Extensa Web Development Ltd. (www.extensadev.com)
 * @copyright	Copyright (c) 2011-2015, Extensa Web Development Ltd.
 * @package 	eBORICA payment gateway
 * @link		http://www.opencart.com/index.php?route=extension/extension/info&extension_id=3004
 */

// Text
$_['text_title']          = 'Direct payment with credit/debit card';
$_['text_failed']         = 'eBORICA Transaction Failed';
$_['text_failed_message'] = '<p>Unfortunately there was an error processing your eBORICA transaction.<br />%s</p>';
$_['text_basket']         = 'Basket';
$_['text_checkout']       = 'Checkout';
$_['text_success']        = 'Payment Status';

$_['text_resp_code_00']   = 'Successful payment';
$_['text_resp_code_85']   = 'Транзакция от тип reversal със същите характеристики вече е регистриран в системата.';
$_['text_resp_code_86']   = 'Транзакция със същите характеристики е вече регистрирана в системата.';
$_['text_resp_code_87']   = 'Грешна версия на протокола.';
$_['text_resp_code_88']   = 'За управляващи транзакции. Не е подаден параметър BOReq.';
$_['text_resp_code_89']   = 'За управляващи транзакции. Не е намерена първоначалната транзакция. (Пример: при reversal – не е намерена първоначалната транзакция, върху която ще се извърши reversal.)';
$_['text_resp_code_90']   = 'Картата не е регистрирана в Directory сървера';
$_['text_resp_code_91']   = 'Timeout от авторизационната система';
$_['text_resp_code_92']   = 'При операция „<i>Проверка за статуса на транзакция</i>”. Изпратеният параметър eBorica е с невалиден формат.';
$_['text_resp_code_93']   = 'Неуспешна 3D автентикация от ACS.';
$_['text_resp_code_94']   = 'Анулирана (канцелирана) транзакция';
$_['text_resp_code_95']   = 'Невалиден подпис на търговеца';
$_['text_resp_code_96']   = 'Техническа грешка при обработка на транзакцията';
$_['text_resp_code_97']   = 'Отхвърлен Reversal';
$_['text_resp_code_98']   = 'При операция „<i>Проверка за статуса на транзакция</i>”. За изпратения <b>BOReq</b> няма регистриран <b>BOResp</b> в сайта на БОРИКА.';
$_['text_resp_code_99']   = 'Авторизацията е отхвърлена от TPSS';

// Error
$_['error_sign']          = 'Certificate not match!';
$_['error_terminal']      = 'Terminal ID not match!';
$_['error_amount']        = 'Paid Amount not match!';
$_['error_order']         = 'The Order not exists!';
$_['error_code']          = 'Code:';
?>