<?php

/**
 * Epay.bg payment gateway for Opencart by Extensa Web Development
 *
 * Copyright � 2010-2015 Extensa Web Development Ltd. All Rights Reserved.
 * This file may not be redistributed in whole or significant part.
 * This copyright notice MUST APPEAR in all copies of the script!
 *
 * @author 		Extensa Web Development Ltd. (www.extensadev.com)
 * @copyright	Copyright (c) 2010-2015, Extensa Web Development Ltd.
 * @package 	Epay.bg payment gateway
 * @link		http://www.opencart.com/index.php?route=extension/extension/info&extension_id=2999
 */

// Text
$_['text_title'] = 'ePay.bg';
?>