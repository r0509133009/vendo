<?php

/**
 * Easypay.bg payment gateway for Opencart by Extensa Web Development
 *
 * Copyright � 2012-2015 Extensa Web Development Ltd. All Rights Reserved.
 * This file may not be redistributed in whole or significant part.
 * This copyright notice MUST APPEAR in all copies of the script!
 *
 * @author 		Extensa Web Development Ltd. (www.extensadev.com)
 * @copyright	Copyright (c) 2012-2015, Extensa Web Development Ltd.
 * @package 	Easypay.bg payment gateway
 * @link		http://www.opencart.com/index.php?route=extension/extension/info&extension_id=5303
 */

// Text
$_['text_title']   = 'Easypay.bg';
$_['text_code']    = 'Easypay payment code:';
$_['text_payment'] = 'Your order will not ship until we receive payment.';
$_['text_failed']  = 'There was an error, please try again.';
?>