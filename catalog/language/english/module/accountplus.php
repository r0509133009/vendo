<?php
// Heading 
$_['heading_title_1']       = 'User Login';
$_['heading_title_2']       = 'My account';

// Text
$_['text_register']         = 'Register';
$_['text_login']            = 'Login';
$_['text_logout']           = 'Logout';
$_['text_forgotten']        = 'Forgotten Password';
$_['text_account']          = 'My Account';
$_['text_edit']             = 'Edit Account';
$_['text_password']         = 'Password';
$_['text_address']          = 'Address book';
$_['text_wishlist']         = 'Wish List';
$_['text_order']            = 'Order History';
$_['text_download']         = 'Downloads';
$_['text_return']           = 'Returns';
$_['text_transaction']      = 'Transactions';
$_['text_recurring']      = 'Recurring';
$_['text_newsletter']       = 'Newsletter';
$_['entry_email_address']	= 'E-mail Address:';
$_['entry_password']    	= 'Password:';
$_['text_welcome']			= 'Welcome';
$_['text_logged']   	    = 'Welcome back, <strong>%s</strong>';
$_['text_guest']	        = ', <strong>visitor</strong>.';
$_['text_my_newsletter']    = 'Newsletter';
$_['text_my_account']       = 'My Account';
$_['text_my_orders']        = 'My Orders';
$_['text_reward']           = 'Reward Points';
$_['text_id_number']       	= 'ID number :';


$_['text_my_account_a']      = 'My Affiliate Account';
$_['text_my_tracking']     = 'My Tracking Information';
$_['text_my_transactions'] = 'My Transactions';

//Text Improved
$_['text_edit_desc']        = 'Edit your account details.';
$_['text_password_desc']    = 'Change your password.';
$_['text_address_desc']     = 'Modify your address book entries.';
$_['text_wishlist_desc']    = 'Modify your wish list';
$_['text_order_desc']       = 'View your orders history';
$_['text_download_desc']    = 'Your downloads';
$_['text_reward_desc']      = 'Reward points';
$_['text_return_desc']      = 'View your return requests';
$_['text_transaction_desc'] = 'View your your Transactions.';
$_['text_newsletter_desc']  = 'Subscribe / unsubscribe to newsletter.';
$_['text_logout_desc']      = 'Logout from your account.';
$_['text_total_balance_a']  = 'Current balance is:';
$_['text_total_reward_a']   = 'Number of reward points is:';
$_['text_total_register_a'] = 'Register date:';
$_['text_greeting_a']      = 'Welcome back, %s';

//Text Improved
$_['text_edit_desc_a']        = 'Edit your account details.';
$_['text_password_desc_a']    = 'Change your password.';
$_['text_payment_desc_a']     = 'Change your payment preferences.';
$_['text_wishlist_desc_a']    = 'Modify your wish list';
$_['text_tracking_desc_a']      = 'Custom Affiliate Tracking Code.';
$_['text_reward_desc_a']      = 'Reward points';
$_['text_return_desc_a']      = 'View your return requests';
$_['text_transaction_desc_a'] = 'View your your Transactions.';
$_['text_newsletter_desc_a']  = 'Subscribe / unsubscribe to newsletter.';
$_['text_logout_desc_a']      = 'Logout from your account.';
$_['text_payment_a']      = 'Payment';
$_['text_tracking_a']      = 'Tracking code';
$_['text_greeting_af']      = 'Welcome back, %s';
$_['text_total_balance_af']  = 'Current balance is:';
$_['text_total_register_af'] = 'Register date:';
$_['text_total_commi_af'] = 'Commision:';
$_['text_total_order_af'] = 'Total orders: ';

?>