<?php
//Front end 

//Strings
$_['text_flexi_registration']      = 'Registration bonus.';
$_['text_flexi_newsletter_sign_up']	= 'Newsletter subscription';
$_['text_flexi_productreview']	= ' Product review ';
$_['text_flexi_firstorder']	= ' First order ';
$_['text_order_id']                           = 'Order ID:';

//Error
$_['error_flexi_minca_points']      = 'You must have MINIMUM %s reward points in your account.';
$_['error_redeem_max']      = 'You can redeem MAX %s reward points for current order.';
$_['error_flexi_redeem_minimum_order_amount']      = 'Your order must be MINIMUM %s before using reward points.';



