<?php
// Text
$_['button_remove']					= 'Delete';
$_['text_continue']					= 'Continue shopping';
$_['text_empty']					= 'The shopping cart is empty!';
$_['text_total_qucik_ckeckout']  	= 'The shopping cart <big><b>%s</b></big> products on <big><b>%s</b></big>';
$_['sendthis']				='Quick order';
$_['callback']				='Call back:';
$_['namew']					= 'Your name:';
$_['phonew']				= 'Your phone number:';
$_['comment_buyer']			= 'Your comment:';
$_['email_buyer']			= 'Your E-mail:';
$_['theme']					= 'Call back:';
$_['messagew']				= 'Message:';
$_['cancel']				= 'Cancel';
$_['button_send']			= 'Checkout';
$_['button_continue']			= 'ПРОДЪЛЖИ ПАЗАРУВАНЕТО';
$_['time']					= 'We will call you back in an hour or another specified time.';
$_['ok']					= 'Your message has been sent. your order № ';
$_['erno']					= 'Sorry, an unexpected error.';
$_['die']					= 'Sorry, the mail server settings do not allow to send your message.';
$_['error_phone']			= 'Your phone number?';
$_['mister']				= 'Your name ?';
$_['subject']				= 'Quick order';
$_['text_1']				= 'You have received an order for';
$_['name']					= 'Name: ';
$_['phone']					= 'Phone: ';
$_['comment_buyer']			= 'Comment: ';
$_['url_callback']			= 'On this page Quick order issued: ';
$_['error_required']        = 'Golf %s It must be filled!';

$_['comment_buyer_error']	= 'It is necessary to fill';
$_['email_buyer_error']		= 'Your E-mail';

$_['text_column_name']		= 'I buying';
$_['text_column_price']		= 'By price:';
$_['text_column_quantity']	= 'The amount';

$_['text_column_photo_product']		= 'product Photos';
$_['text_column_name_product']		= 'Name';
$_['text_column_quantity_product']	= 'Quantity';
$_['text_column_price_product']		= 'Price';
$_['text_column_total_product']		= 'Total';

$_['text_photo']			= 'Photo';
$_['text_new_product']		= 'Product';
$_['text_new_model']		= 'Model';
$_['text_new_quantity']		= 'Quantity';
$_['text_new_price']		= 'Price';
$_['text_new_total']		= 'Total';
$_['continue_shopping']		= 'Continue shopping';
$_['text_quick_order_enter_name_phone']         = 'Please include your name and phone number so we can contact you';
?>
