<?php
// Heading 
$_['heading_title']        = 'My Affiliate Account';

// Text
$_['text_account']         = 'Account';
$_['text_my_account']      = 'My Affiliate Account';
$_['text_my_tracking']     = 'My Tracking Information';
$_['text_my_transactions'] = 'My Transactions';
$_['text_edit']            = 'Edit your account information';
$_['text_password']        = 'Change your password';
$_['text_payment']         = 'Change your payment preferences';
$_['text_tracking']        = 'Custom Affiliate Tracking Code';
$_['text_transaction']     = 'View your transaction history';

$_['text_clicks_last10'] 	= 'Clicks: Last 10 Days';
$_['text_no_data'] 			= 'Nodata!';
$_['text_event']			= 'Event';
$_['text_balance'] 			= 'Balance:';
$_['text_stats_day_month'] 	= 'Stats the day of month:';
$_['text_date'] = 'Date';
$_['text_impression'] = 'Impression';
$_['text_click'] = 'Click';
$_['text_sale'] = 'Sale';
$_['text_amount'] = 'Amount';
$_['text_ctr'] = 'CTR';
$_['text_cr'] = 'CR';
$_['text_total_month'] = 'Total month:';