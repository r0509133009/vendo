<?php
// Heading 
$_['heading_title']    = 'Affiliate Tracking';

// Text
$_['text_account']     = 'Account';
$_['text_description'] = 'To make sure you get paid for referrals you send to us we need to track the referral by placing a tracking code in the URL\'s linking to us. You can use the tools below to generate links to the %s web site.';
$_['text_code']        = '<b>Your Tracking Code:</b>';
$_['text_generator']   = '<b>Tracking Link Generator</b><br />Type in the name of a product you would like to link to:';
$_['text_link']        = '<b>Tracking Link:</b>';

$_['text_banners'] = 'Banners';
$_['text_code_bn_rand'] = 'Code Random Banner';
$_['text_url_product'] = 'Url Product';
$_['text_image'] = 'Image';
$_['text_size'] = 'Size';
$_['text_date'] = 'Date';
$_['text_code'] = 'Code';
$_['text_detail'] = 'Detail';
$_['text_no_data'] = 'No data!';
$_['text_choose_size'] = 'Choose Size';
$_['text_choose_size_bn'] = '--Choose Size Banner--';
$_['text_get_code'] = 'Get code!';
$_['text_bn_rand'] = 'Random Banners';
?>