<?php
class ControllerInformationFaq extends Controller {
	public function index() {
		$this->load->language('information/faq');
		$this->load->model('catalog/faq');
		
	

		$this->document->setTitle($this->language->get('heading_title'));

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('information/faq'),
			'separator' => $this->language->get('text_separator')
		);
		
		$this->data['text_readmore'] = $this->language->get('text_readmore');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['heading_payment'] = $this->language->get('heading_payment');
		$this->data['button_search'] = $this->language->get('button_search');
		$this->data['results']=array();
		
		$categoies = $this->model_catalog_faq->getfcategoies();
		foreach($categoies as $category){
			$subfaqs=array();
			$filterdata=array(
			 'filter_fcategory_id' => $category['fcategory_id'],
			);
			$faqs = $this->model_catalog_faq->getfaqs($filterdata);
			foreach($faqs as $faq){
			  $subfaqs[]=array(
				'faq_id'	  => $faq['faq_id'],
				'name'		  => $faq['name'],
				'description' => html_entity_decode($faq['description'], ENT_QUOTES, 'UTF-8'),
				'href'		  => $this->url->link('information/faq/fullview','&faq_id='.$faq['faq_id']),
			  );
			}
			$this->data['results'][]=array(
			  'fcategory_id' => $category['fcategory_id'],
			  'name'		 => $category['name'],
			  'subfaqs'		 => $subfaqs,
			);
		}
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/faq.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/information/faq.tpl';
			} else {
				$this->template = 'default/template/information/faq.tpl';
			}

			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'
			);

			$this->response->setOutput($this->render());
	}
	
	public function search(){
		$this->load->language('information/faq');
		$this->load->model('catalog/faq');
		$this->document->addStyle('catalog/view/theme/default/stylesheet/faq.css');
		$this->document->setTitle($this->language->get('heading_title'));

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home'),
			'separator' => false
		);
		$this->data['button_search'] = $this->language->get('button_search');
		$this->data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('information/faq'),
			'separator' => $this->language->get('text_separator')
		);
		
		if(isset($this->request->get['fsearch'])){
			$search = $this->request->get['fsearch'];
		}else{
			$search = '';
		}
		
		$this->data['search'] = $search;

		$this->data['text_readmore'] = $this->language->get('text_readmore');
		$this->data['text_search'] = $this->language->get('text_search');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['heading_payment'] = $this->language->get('heading_payment');
		$this->data['results']=array();
		$filterdata=array(
		 'filter_name' => trim($search),
		);
		$faqs = $this->model_catalog_faq->getfaqs($filterdata);
		foreach($faqs as $faq){
			$category_info = $this->model_catalog_faq->getfCategory($faq['fcategory_id']);
			if($category_info){
				$categoiesname = $category_info['name'];
			}else{
				$categoiesname = '';
			}
			
			$this->data['results'][]=array(
			  'faq_id'	  	  => $faq['faq_id'],
			  'name'	 	  => $faq['name'],
			  'categoiesname' => $categoiesname,
			  'description'   => html_entity_decode($faq['description'], ENT_QUOTES, 'UTF-8'),
			  'href'		  => $this->url->link('information/faq/fullview','&faq_id='.$faq['faq_id']),
			);
		}
		
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/faqserach.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/information/faqserach.tpl';
			} else {
				$this->template = 'default/template/information/faqserach.tpl';
			}

			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'
			);

			$this->response->setOutput($this->render());
	}
	
	public function fullview(){
		$this->load->language('information/faq');
		$this->load->model('catalog/faq');
		$this->document->addStyle('catalog/view/theme/default/stylesheet/faq.css');
		if($this->request->get['faq_id']){
		  $faq_id = $this->request->get['faq_id'];
		}else{
		  $faq_id = 0;
		}
		
		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home'),
			'separator' => false
		);
		
		$faq_info = $this->model_catalog_faq->getfaq($faq_id);
		if($faq_info){
		    $category_info = $this->model_catalog_faq->getfCategory($faq_info['fcategory_id']);
			if($category_info){
				$this->document->setTitle($category_info['meta_title']);
				$this->document->setDescription($category_info['meta_description']);
				$this->document->setKeywords($category_info['meta_keyword']);
			}
			
			$this->data['breadcrumbs'][] = array(
				'text' => $faq_info['name'],
				'href' => $this->url->link('information/faq/fullview', 'faq_id=' .  $faq_id),
				'separator' => $this->language->get('text_separator')
			);

			$this->data['heading_title1'] = $faq_info['name'];
			$this->data['heading_title'] = $this->language->get('heading_title');
			$this->data['button_continue'] = $this->language->get('button_continue');
			
			$this->data['description'] = html_entity_decode($faq_info['description'], ENT_QUOTES, 'UTF-8');

			
			$this->data['continue'] = $this->url->link('information/faq/search','','SSL');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/faqview.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/information/faqview.tpl';
			} else {
				$this->template = 'default/template/information/faqview.tpl';
			}

			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'
			);

			$this->response->setOutput($this->render());
		}else{
			$this->data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('information/information', 'information_id=' . $information_id),
				'separator' => $this->language->get('text_separator')
			);

			$this->document->setTitle($this->language->get('text_error'));

			$this->data['heading_title'] = $this->language->get('text_error');

			$this->data['text_error'] = $this->language->get('text_error');

			$this->data['button_continue'] = $this->language->get('button_continue');

			$this->data['continue'] = $this->url->link('information/faq/search','','SSL');
			
			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/error/not_found.tpl';
			} else {
				$this->template = 'default/template/error/not_found.tpl';
			}

			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'
			);

			$this->response->setOutput($this->render());
		}
	}
}