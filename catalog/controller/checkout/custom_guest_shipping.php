<?php
// ***************************************************
//             Custom Registration Fields   
//       
// Author : Francesco Pisanò - francesco1279@gmail.com
//              
//                   www.leverod.com		
//               © All rights reserved	  
// ***************************************************




class ControllerCheckoutGuestShipping extends Controller {
  	public function index() {	
		$this->language->load('checkout/checkout');
		
		
		// check if a language file for this module is available for 
		// the current opencart installation:
		$this->load->model('localisation/language');
		$current_language = $this->model_localisation_language->getLanguage($this->config->get('config_language_id'));
		
		if (file_exists(DIR_LANGUAGE.'/'.$current_language['directory'].'/account/custom_register.php')){
			$this->language->load('account/custom_register');
		} 
		// else we load a default language file (english)
		else {
			// by adding "../english" to the path we can go up a level in the directory tree and go down
			// again to load the subdirectory "english" (to know how Opencart builds the path, see the file
			// "system/library/language.php"
			$this->language->load('../english/account/custom_register');
		}
		
		
		$this->data['text_select'] = $this->language->get('text_select');
		$this->data['text_none'] = $this->language->get('text_none');

		$this->data['entry_firstname'] = $this->language->get('entry_firstname');
		$this->data['entry_lastname'] = $this->language->get('entry_lastname');
		$this->data['entry_company'] = $this->language->get('entry_company');
		$this->data['entry_address_1'] = $this->language->get('entry_address_1');
		$this->data['entry_address_2'] = $this->language->get('entry_address_2');
		$this->data['entry_postcode'] = $this->language->get('entry_postcode');
		$this->data['entry_city'] = $this->language->get('entry_city');
		$this->data['entry_country'] = $this->language->get('entry_country');
		$this->data['entry_zone'] = $this->language->get('entry_zone');
	
		$this->data['button_continue'] = $this->language->get('button_continue');
		
		
		// my settings 
		$reg_entries = array(	
			'firstname',
			'lastname',
			'company',
			'address_1',
			'address_2',
			'city',
			'country',
			'postcode',
			'zone'
		);

		foreach ($reg_entries as $reg_entry) {
			$this->data['custom_register_guest_enable_'.$reg_entry] = $this->config->get('custom_register_guest_enable_'.$reg_entry);
			$this->data['custom_register_guest_'.$reg_entry.'_required'] = $this->config->get('custom_register_guest_'.$reg_entry.'_required');
		}
		
		
		// This flag tells whether the field country is disabled and the field zones is not
		$this->data['country_disabled_zone_enabled'] = !$this->data['custom_register_guest_enable_country'] && $this->data['custom_register_guest_enable_zone'];
		
		// end my settings
		
					
		if (isset($this->session->data['guest']['shipping']['firstname'])) {
			$this->data['firstname'] = $this->session->data['guest']['shipping']['firstname'];
		} else {
			$this->data['firstname'] = '';
		}

		if (isset($this->session->data['guest']['shipping']['lastname'])) {
			$this->data['lastname'] = $this->session->data['guest']['shipping']['lastname'];
		} else {
			$this->data['lastname'] = '';
		}
		
		if (isset($this->session->data['guest']['shipping']['company'])) {
			$this->data['company'] = $this->session->data['guest']['shipping']['company'];			
		} else {
			$this->data['company'] = '';
		}
		
		if (isset($this->session->data['guest']['shipping']['address_1'])) {
			$this->data['address_1'] = $this->session->data['guest']['shipping']['address_1'];			
		} else {
			$this->data['address_1'] = '';
		}

		if (isset($this->session->data['guest']['shipping']['address_2'])) {
			$this->data['address_2'] = $this->session->data['guest']['shipping']['address_2'];			
		} else {
			$this->data['address_2'] = '';
		}

		if (isset($this->session->data['guest']['shipping']['postcode'])) {
			$this->data['postcode'] = $this->session->data['guest']['shipping']['postcode'];	
		} elseif (isset($this->session->data['shipping_postcode'])) {
			$this->data['postcode'] = $this->session->data['shipping_postcode'];								
		} else {
			$this->data['postcode'] = '';
		}
		
		if (isset($this->session->data['guest']['shipping']['city'])) {
			$this->data['city'] = $this->session->data['guest']['shipping']['city'];			
		} else {
			$this->data['city'] = '';
		}

		
		$this->data['custom_register_default_country'] = $this->config->get('custom_register_default_country');
		
		if (!$this->data['custom_register_guest_enable_country'] && !$this->data['custom_register_guest_enable_zone']) {
			$this->data['country_id'] = '';
		} elseif (isset($this->session->data['guest']['shipping']['country_id']) && $this->session->data['guest']['shipping']['country_id'] > 0) {
			$this->data['country_id'] = $this->session->data['guest']['shipping']['country_id'];			  	
		} elseif (isset($this->session->data['shipping_country_id']) && $this->session->data['shipping_country_id'] > 0) {
			$this->data['country_id'] = $this->session->data['shipping_country_id'];	
		} elseif (!empty($this->data['custom_register_default_country'])) {
			$this->data['country_id'] = $this->data['custom_register_default_country'];				
		} else {
			$this->data['country_id'] = $this->config->get('config_country_id');
		}
		
		

		if ( isset($this->session->data['guest']['shipping']['zone_id']) && $this->session->data['guest']['shipping']['zone_id'] > 0 ) {
			$this->data['zone_id'] = $this->session->data['guest']['shipping']['zone_id'];	
		} elseif (isset($this->session->data['shipping_zone_id'])) {
			$this->data['zone_id'] = $this->session->data['shipping_zone_id'];						
		} else {
			$this->data['zone_id'] = '';
		}
					
		$this->load->model('localisation/country');
		
		$this->data['countries'] = $this->model_localisation_country->getCountries();
		
		
		

		
		
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/custom_guest_shipping.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/checkout/custom_guest_shipping.tpl';
		} else {
			$this->template = 'default/template/checkout/custom_guest_shipping.tpl';
		}		
		
		$this->response->setOutput($this->render());
	}
	
	public function validate() {
	
		$this->language->load('checkout/checkout');
		
		// check if a language file for this module is available for 
		// the current opencart installation:
		$this->load->model('localisation/language');
		$current_language = $this->model_localisation_language->getLanguage($this->config->get('config_language_id'));
		
		if (file_exists(DIR_LANGUAGE.'/'.$current_language['directory'].'/account/custom_register.php')){
			$this->language->load('account/custom_register');
		} 
		// else we load a default language file (english)
		else {
			// by adding "../english" to the path we can go up a level in the directory tree and go down
			// again to load the subdirectory "english" (to know how Opencart builds the path, see the file
			// "system/library/language.php"
			$this->language->load('../english/account/custom_register');
		}
		
		$json = array();
		
		// Validate if customer is logged in.
		if ($this->customer->isLogged()) {
			$json['redirect'] = $this->url->link('checkout/checkout', '', 'SSL');
		} 			
		
		// Validate cart has products and has stock.
		if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
			$json['redirect'] = $this->url->link('checkout/cart');		
		}
		
		// Check if guest checkout is avaliable.	
		if (!$this->config->get('config_guest_checkout') || $this->config->get('config_customer_price') || $this->cart->hasDownload()) {
			$json['redirect'] = $this->url->link('checkout/checkout', '', 'SSL');
		} 
		
		if (!$json) {		
		
		
			// if firstname enabled
			if ( $this->config->get('custom_register_guest_enable_firstname') ){
				
				// if required 
				if ( $this->config->get('custom_register_guest_firstname_required') ) {
		
					// if empty or out of range
					if ( (strlen(utf8_decode($this->request->post['firstname'])) < 1) || (strlen(utf8_decode($this->request->post['firstname'])) > 32) ) {

						$json['error']['firstname'] = $this->language->get('error_firstname');
					}
				}
				// if not required, not empty but out of range
				elseif ( strlen(utf8_decode($this->request->post['firstname'])) > 32 ) {
					$json['error']['firstname'] = $this->language->get('error_firstname');
				}
			}
			// if the field wasn't enabled we initialize the corresponding post var with an empty value to 
			// avoid "Notice: Undefined index firstname..." errors (you can see them in the json answer)
			else $this->request->post['firstname'] = '';
			
	
			// if lastname enabled
			if ( $this->config->get('custom_register_guest_enable_lastname') ){
				
				// if required 
				if ( $this->config->get('custom_register_guest_lastname_required') ) {
		
					// if empty or out of range
					if ( (strlen(utf8_decode($this->request->post['lastname'])) < 1) || (strlen(utf8_decode($this->request->post['lastname'])) > 32) ) {

						$json['error']['lastname'] = $this->language->get('error_lastname');
					}
				}
				// if not required, not empty but out of range
				elseif ( strlen(utf8_decode($this->request->post['lastname'])) > 32 ) {
					$json['error']['lastname'] = $this->language->get('error_lastname');
				}
			}
			else $this->request->post['lastname'] = '';
			
			
			// if address 1 enabled
			if ( $this->config->get('custom_register_guest_enable_address_1') ){
				
				// if required 
				if ( $this->config->get('custom_register_guest_address_1_required') ) {

					// if empty or out of range
					if ( (strlen(utf8_decode($this->request->post['address_1'])) < 3) || (strlen(utf8_decode($this->request->post['address_1'])) > 128) ) {
						$json['error']['address_1'] = $this->language->get('error_address_1');
					}	
				}
				// if not required, not empty but out of range
				elseif ( (strlen(utf8_decode($this->request->post['address_1'])) > 0 ) && (strlen(utf8_decode($this->request->post['address_1'])) < 3) || (strlen(utf8_decode($this->request->post['address_1'])) > 128) ) {
					$json['error']['address_1'] = $this->language->get('error_address_1');
				}	
			}
			else $this->request->post['address_1'] = '';
			
			
			
		// Added checking for address 2:
			// if address 2 enabled
			if ( $this->config->get('custom_register_guest_enable_address_2') ){
				
				// if required 
				if ( $this->config->get('custom_register_guest_address_2_required') ) {

					// if empty or out of range
					if ( (strlen(utf8_decode($this->request->post['address_2'])) < 3) || (strlen(utf8_decode($this->request->post['address_2'])) > 128) ) {
						$json['error']['address_2'] = $this->language->get('error_address_2');
					}	
				}
				// if not required, not empty but out of range
				elseif ( (strlen(utf8_decode($this->request->post['address_2'])) > 0 ) && (strlen(utf8_decode($this->request->post['address_2'])) < 3) || (strlen(utf8_decode($this->request->post['address_2'])) > 128) ) {
					$json['error']['address_2'] = $this->language->get('error_address_2');
				}	
			}
			else $this->request->post['address_2'] = '';
			
			
			
		// Added checking for company:
			// if company enabled
			if ( $this->config->get('custom_register_guest_enable_company') ){
				
				// if required 
				if ( $this->config->get('custom_register_guest_company_required') ) {

					// if empty or out of range
					if ( (strlen(utf8_decode($this->request->post['company'])) < 3) || (strlen(utf8_decode($this->request->post['company'])) > 32) ) {
						$json['error']['company'] = $this->language->get('error_company');
					}	
				}
				// if not required, not empty but out of range
				elseif ( (strlen(utf8_decode($this->request->post['company'])) > 0 ) && (strlen(utf8_decode($this->request->post['company'])) < 3) || (strlen(utf8_decode($this->request->post['company'])) > 32) ) {
					$json['error']['company'] = $this->language->get('error_company');
				}	
			}
			else $this->request->post['company'] = '';
			
			
			
			// if city enabled
			if ( $this->config->get('custom_register_guest_enable_city') ){
				
				// if required 
				if ( $this->config->get('custom_register_guest_city_required') ) {

					// if empty or out of range
					if ( (strlen(utf8_decode($this->request->post['city'])) < 2) || (strlen(utf8_decode($this->request->post['city'])) > 128) ) {
						$json['error']['city'] = $this->language->get('error_city');
					}	
				}
				// if not required, not empty but out of range
				elseif ( (strlen(utf8_decode($this->request->post['city'])) > 0 ) && (strlen(utf8_decode($this->request->post['city'])) < 2) || (strlen(utf8_decode($this->request->post['city'])) > 128) ) {
					$json['error']['city'] = $this->language->get('error_city');
				}	
			}
			else $this->request->post['city'] = '';
			
			
			
			// Added checking for country  
			if ( $this->config->get('custom_register_guest_enable_country') ){
				
				if ( $this->config->get('custom_register_guest_country_required') ) {
				
					if ( $this->request->post['country_id'] == '' ) {
						$json['error']['country'] = $this->language->get('error_country');
					}	
				}
			}


			$this->load->model('localisation/country');
			$country_info = $this->model_localisation_country->getCountry($this->request->post['country_id']);
	
	
		// Added checking for postal code 
			
			// if the current country requires the postcode even if it is disabled from admin control panel	
			if ( ($country_info && $country_info['postcode_required'] ) && ( strlen(utf8_decode($this->request->post['postcode'])) < 2 || strlen(utf8_decode($this->request->post['postcode'])) > 10)  ) {
				$json['error']['postcode'] = $this->language->get('error_postcode');
			} 
			
			if ( $this->config->get('custom_register_guest_enable_postcode') ){
				
				if ( $this->config->get('custom_register_guest_postcode_required') ) {
				
					// if empty or out of range:
					if ( strlen(utf8_decode($this->request->post['postcode'])) < 2 || strlen(utf8_decode($this->request->post['postcode'])) > 10) {
						$json['error']['postcode'] = $this->language->get('error_postcode');
					}
				}
				// if not required, not empty but out of range
				elseif ( strlen(utf8_decode($this->request->post['postcode'])) > 0  && ( strlen(utf8_decode($this->request->post['postcode'])) < 2 || strlen(utf8_decode($this->request->post['postcode'])) > 10)  ) {
					$json['error']['postcode'] = $this->language->get('error_postcode');
				}		
			}
			else {
				if ( !isset($this->request->post['postcode']) ) $this->request->post['postcode'] = '';
			}


			
						
			// if zone enabled
			if ( $this->config->get('custom_register_guest_enable_zone') ){
				
				// if required 
				if ( $this->config->get('custom_register_guest_zone_required') ) {
				
					if ($this->request->post['zone_id'] == '') {
						$json['error']['zone'] = $this->language->get('error_zone');
					}
				}
			}
					
			
		}
		
		if (!$json) {
			$this->session->data['guest']['shipping']['firstname'] = trim($this->request->post['firstname']);
			$this->session->data['guest']['shipping']['lastname'] = trim($this->request->post['lastname']);
			$this->session->data['guest']['shipping']['company'] = trim($this->request->post['company']);
			$this->session->data['guest']['shipping']['address_1'] = $this->request->post['address_1'];
			$this->session->data['guest']['shipping']['address_2'] = $this->request->post['address_2'];
			$this->session->data['guest']['shipping']['postcode'] = $this->request->post['postcode'];
			$this->session->data['guest']['shipping']['city'] = $this->request->post['city'];
			$this->session->data['guest']['shipping']['country_id'] = $this->request->post['country_id'];
			$this->session->data['guest']['shipping']['zone_id'] = $this->request->post['zone_id'];
			
			$this->load->model('localisation/country');
			
			$country_info = $this->model_localisation_country->getCountry($this->request->post['country_id']);
			
			if ($country_info) {
				$this->session->data['guest']['shipping']['country'] = $country_info['name'];	
				$this->session->data['guest']['shipping']['iso_code_2'] = $country_info['iso_code_2'];
				$this->session->data['guest']['shipping']['iso_code_3'] = $country_info['iso_code_3'];
				$this->session->data['guest']['shipping']['address_format'] = $country_info['address_format'];
			} else {
				$this->session->data['guest']['shipping']['country'] = '';	
				$this->session->data['guest']['shipping']['iso_code_2'] = '';
				$this->session->data['guest']['shipping']['iso_code_3'] = '';
				$this->session->data['guest']['shipping']['address_format'] = '';
			}
			
			$this->load->model('localisation/zone');
							
			$zone_info = $this->model_localisation_zone->getZone($this->request->post['zone_id']);
		
			if ($zone_info) {
				$this->session->data['guest']['shipping']['zone'] = $zone_info['name'];
				$this->session->data['guest']['shipping']['zone_code'] = $zone_info['code'];
			} else {
				$this->session->data['guest']['shipping']['zone'] = '';
				$this->session->data['guest']['shipping']['zone_code'] = '';
			}
			
			$this->session->data['shipping_country_id'] = $this->request->post['country_id'];
			$this->session->data['shipping_zone_id'] = $this->request->post['zone_id'];
			$this->session->data['shipping_postcode'] = $this->request->post['postcode'];	
			
			unset($this->session->data['shipping_method']);	
			unset($this->session->data['shipping_methods']);
		}
		
		$this->response->setOutput(json_encode($json));		
	}
	
}
?>