<?php
class ControllerCheckoutSuccess extends Controller { 
	public function index() {

		if (isset($this->session->data['order_id'])) {
			

			if(isset($this->session->data['payment_method'])){
				$this->language->load('checkout/custom_success');
				$this->load->model('checkout/order');
				$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
				switch ($this->session->data['payment_method']['code']) {
					case 'offlinepay':
						$this->data['payment_method_content'] = $this->getChild('checkout/success_offline',$order_info);
						break;
					case 'bank_transfer':
						$this->data['payment_method_content'] = $this->getChild('checkout/success_bank_transfer',$order_info);
						break;
					case 'cod':
						$this->data['payment_method_content'] = $this->getChild('checkout/success_cod',$order_info);
						break;
					case 'easypay':
					$this->data['payment_method_content'] = $this->getChild('checkout/success_easypay',$order_info);
					break;
					case 'pp_standard':
					$this->data['payment_method_content'] = $this->getChild('checkout/success_pp_standard',$order_info);
					break;
					case 'borica':
					$this->data['payment_method_content'] = $this->getChild('checkout/success_borica',$order_info);
					break;
					default:
						# code...
						break;
				}

				$this->data['text_bottom_thanks'] = $this->language->get('text_bottom_thanks');
			}

			$this->cart->clear();

		    unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['guest']);
			unset($this->session->data['comment']);
			unset($this->session->data['order_id']);	
			unset($this->session->data['coupon']);
			unset($this->session->data['reward']);
			unset($this->session->data['voucher']);
			unset($this->session->data['vouchers']);
			unset($this->session->data['totals']);
		}	

		$this->language->load('checkout/success');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->data['breadcrumbs'] = array(); 

		$this->data['breadcrumbs'][] = array(
			'href'      => $this->url->link('common/home'),
			'text'      => $this->language->get('text_home'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'href'      => $this->url->link('checkout/cart'),
			'text'      => $this->language->get('text_basket'),
			'separator' => $this->language->get('text_separator')
		);

		$this->data['breadcrumbs'][] = array(
			'href'      => $this->url->link('checkout/checkout', '', 'SSL'),
			'text'      => $this->language->get('text_checkout'),
			'separator' => $this->language->get('text_separator')
		);	

		$this->data['breadcrumbs'][] = array(
			'href'      => $this->url->link('checkout/success'),
			'text'      => $this->language->get('text_success'),
			'separator' => $this->language->get('text_separator')
		);

		$this->data['heading_title'] = $this->language->get('heading_title');

		if ($this->customer->isLogged()) {
			$this->data['text_message'] = sprintf($this->language->get('text_customer'), $this->url->link('account/account', '', 'SSL'), $this->url->link('account/order', '', 'SSL'), $this->url->link('account/download', '', 'SSL'), $this->url->link('information/contact'));
		} else {
			$this->data['text_message'] = sprintf($this->language->get('text_guest'), $this->url->link('information/contact'));
		}

		$this->data['button_continue'] = $this->language->get('button_continue');

		$this->data['continue'] = $this->url->link('common/home');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/custom_success.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/common/custom_success.tpl';
		} else {
			$this->template = 'default/template/common/custom_success.tpl';
		}

		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'			
		);

		$this->response->setOutput($this->render());
	}

}
?>