<?php
// ***************************************************
//             Custom Registration Fields   
//       
// Author : Francesco Pisanò - francesco1279@gmail.com
//              
//                   www.leverod.com		
//               © All rights reserved	  
// ***************************************************


 

class ControllerCheckoutGuest extends Controller {
  	public function index() {
	
    	$this->language->load('checkout/checkout');
		
		// check if a language file for this module is available for 
		// the current opencart installation:
		$this->load->model('localisation/language');
		$current_language = $this->model_localisation_language->getLanguage($this->config->get('config_language_id'));
		
		if (file_exists(DIR_LANGUAGE.'/'.$current_language['directory'].'/account/custom_register.php')){
			$this->language->load('account/custom_register');
		} 
		// else we load a default language file (english)
		else {
			// by adding "../english" to the path we can go up a level in the directory tree and go down
			// again to load the subdirectory "english" (to know how Opencart builds the path, see the file
			// "system/library/language.php"
			$this->language->load('../english/account/custom_register');
		}
		
		$this->data['text_select'] = $this->language->get('text_select');
		$this->data['text_none'] = $this->language->get('text_none');
		$this->data['text_your_details'] = $this->language->get('text_your_details');
		$this->data['text_your_account'] = $this->language->get('text_your_account');
		$this->data['text_your_address'] = $this->language->get('text_your_address');
		
		$this->data['entry_firstname'] = $this->language->get('entry_firstname');
		$this->data['entry_lastname'] = $this->language->get('entry_lastname');
		$this->data['entry_email'] = $this->language->get('entry_email');
		$this->data['entry_telephone'] = $this->language->get('entry_telephone');
		$this->data['entry_fax'] = $this->language->get('entry_fax');
		$this->data['entry_company'] = $this->language->get('entry_company');
		$this->data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$this->data['entry_company_id'] = $this->language->get('entry_company_id');
		$this->data['entry_tax_id'] = $this->language->get('entry_tax_id');			
		$this->data['entry_address_1'] = $this->language->get('entry_address_1');
		$this->data['entry_address_2'] = $this->language->get('entry_address_2');
		$this->data['entry_postcode'] = $this->language->get('entry_postcode');
		$this->data['entry_city'] = $this->language->get('entry_city');
		$this->data['entry_country'] = $this->language->get('entry_country');
		$this->data['entry_zone'] = $this->language->get('entry_zone');
		$this->data['entry_shipping'] = $this->language->get('entry_shipping');
		
		$this->data['button_continue'] = $this->language->get('button_continue');
		$this->data['entry_anti_spam_label'] = $this->language->get('entry_anti_spam_label');
		
		
		// my settings 
		$reg_entries = array(	
			'firstname',
			'lastname',
			'email',
			'telephone',
			'fax',
			'company',
			'address_1',
			'address_2',
			'city',
			'country',
			'postcode',
			'zone',	
			'shipping_address',
			'agree'
		);
		
		
		foreach ($reg_entries as $reg_entry) {
			$this->data['custom_register_guest_enable_'.$reg_entry] = $this->config->get('custom_register_guest_enable_'.$reg_entry);
			$this->data['custom_register_guest_'.$reg_entry.'_required'] = $this->config->get('custom_register_guest_'.$reg_entry.'_required');
		}
		
		$this->data['custom_register_enable_antispam'] = $this->config->get('custom_register_enable_antispam');
		
		// This flag tells whether the field country is disabled and the field zones is not
		$this->data['country_disabled_zone_enabled'] = !$this->data['custom_register_guest_enable_country'] && $this->data['custom_register_guest_enable_zone'];
		
		// end my settings
		
		
		
		if (isset($this->session->data['guest']['firstname'])) {
			$this->data['firstname'] = $this->session->data['guest']['firstname'];
		} else {
			$this->data['firstname'] = '';
		}

		if (isset($this->session->data['guest']['lastname'])) {
			$this->data['lastname'] = $this->session->data['guest']['lastname'];
		} else {
			$this->data['lastname'] = '';
		}
		
		if (isset($this->session->data['guest']['email'])) {
			$this->data['email'] = $this->session->data['guest']['email'];
		} else {
			$this->data['email'] = '';
		}
		
		if (isset($this->session->data['guest']['telephone'])) {
			$this->data['telephone'] = $this->session->data['guest']['telephone'];		
		} else {
			$this->data['telephone'] = '';
		}

		if (isset($this->session->data['guest']['fax'])) {
			$this->data['fax'] = $this->session->data['guest']['fax'];				
		} else {
			$this->data['fax'] = '';
		}

		if (isset($this->session->data['guest']['payment']['company'])) {
			$this->data['company'] = $this->session->data['guest']['payment']['company'];			
		} else {
			$this->data['company'] = '';
		}

		// CUSTOMER GROUPS, TAX ID AND COMPANY ID HAVE BEEN ADDED WITH Oc 1.5.3
		if ( version_compare(VERSION, '1.5.3', '>=') ) {
		
			$this->load->model('account/customer_group');

			$this->data['customer_groups'] = array();
			
			if (is_array($this->config->get('config_customer_group_display'))) {
				$customer_groups = $this->model_account_customer_group->getCustomerGroups();
				
				foreach ($customer_groups as $customer_group) {
					if (in_array($customer_group['customer_group_id'], $this->config->get('config_customer_group_display'))) {
						$this->data['customer_groups'][] = $customer_group;
					}
				}
			}
			
			if (isset($this->session->data['guest']['customer_group_id'])) {
				$this->data['customer_group_id'] = $this->session->data['guest']['customer_group_id'];
			} else {
				$this->data['customer_group_id'] = $this->config->get('config_customer_group_id');
			}
			
			// Company ID
			if (isset($this->session->data['guest']['payment']['company_id'])) {
				$this->data['company_id'] = $this->session->data['guest']['payment']['company_id'];			
			} else {
				$this->data['company_id'] = '';
			}
			
			// Tax ID
			if (isset($this->session->data['guest']['payment']['tax_id'])) {
				$this->data['tax_id'] = $this->session->data['guest']['payment']['tax_id'];			
			} else {
				$this->data['tax_id'] = '';
			}
		}
								
		if (isset($this->session->data['guest']['payment']['address_1'])) {
			$this->data['address_1'] = $this->session->data['guest']['payment']['address_1'];			
		} else {
			$this->data['address_1'] = '';
		}

		if (isset($this->session->data['guest']['payment']['address_2'])) {
			$this->data['address_2'] = $this->session->data['guest']['payment']['address_2'];			
		} else {
			$this->data['address_2'] = '';
		}

		if (isset($this->session->data['guest']['payment']['postcode'])) {
			$this->data['postcode'] = $this->session->data['guest']['payment']['postcode'];							
		} elseif (isset($this->session->data['shipping_postcode'])) {
			$this->data['postcode'] = $this->session->data['shipping_postcode'];			
		} else {
			$this->data['postcode'] = '';
		}
		
		if (isset($this->session->data['guest']['payment']['city'])) {
			$this->data['city'] = $this->session->data['guest']['payment']['city'];			
		} else {
			$this->data['city'] = '';
		}

		
		$this->data['custom_register_default_country'] = $this->config->get('custom_register_default_country');
		
		if (!$this->data['custom_register_guest_enable_country'] && !$this->data['custom_register_guest_enable_zone']) {
			$this->data['country_id'] = '';
		} elseif (isset($this->session->data['guest']['payment']['country_id']) && $this->session->data['guest']['payment']['country_id'] > 0) {
			$this->data['country_id'] = $this->session->data['guest']['payment']['country_id'];			  	
		} elseif (isset($this->session->data['shipping_country_id']) && $this->session->data['shipping_country_id'] > 0) {
			$this->data['country_id'] = $this->session->data['shipping_country_id'];	
		} elseif (!empty($this->data['custom_register_default_country'])) {
			$this->data['country_id'] = $this->data['custom_register_default_country'];						
		} else {
			$this->data['country_id'] = $this->config->get('config_country_id');
		}



		if (isset($this->session->data['guest']['payment']['zone_id'])) {
			$this->data['zone_id'] = $this->session->data['guest']['payment']['zone_id'];	
		} elseif (isset($this->session->data['shipping_zone_id'])) {
			$this->data['zone_id'] = $this->session->data['shipping_zone_id'];						
		} else {
			$this->data['zone_id'] = '';
		}
					
		$this->load->model('localisation/country');
		
		$this->data['countries'] = $this->model_localisation_country->getCountries();


		
// copied from custom_register.php to add the checkbox "agree with privacy policy"		
		if ($this->config->get('config_account_id')) {
			$this->load->model('catalog/information');
			
			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));
			
			if ($information_info) {
				$this->data['text_agree'] = sprintf($this->language->get('text_agree'), $this->url->link('information/information/info', 'information_id=' . $this->config->get('config_account_id'), 'SSL'), $information_info['title'], $information_info['title']);
			} else {
				$this->data['text_agree'] = '';
			}
		} else {
			$this->data['text_agree'] = '';
		}
// end copy


		if ($this->config->get('custom_register_account_agree_checked') ) {
			$this->data['agree'] = true;
		} else {
			$this->data['agree'] = false;
		}
	
	
		$this->data['shipping_required'] = $this->cart->hasShipping();
		

		if (isset($this->session->data['guest']['shipping_address'])) {
			$this->data['shipping_address'] = $this->session->data['guest']['shipping_address'];
		} else if ( !isset($this->session->data['guest']['shipping_address']) && $this->config->get('custom_register_shipping_address_checked') ) {
			$this->data['shipping_address'] = true;
		} else {
			$this->data['shipping_address'] = '';
		}
		
			

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/custom_guest.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/checkout/custom_guest.tpl';
		} else {
			$this->template = 'default/template/checkout/custom_guest.tpl';
		}
		
		$this->response->setOutput($this->render());		
  	}
	
	
	
	
	public function validate() {
	
    	$this->language->load('checkout/checkout');

		// check if a language file for this module is available for 
		// the current opencart installation:
		$this->load->model('localisation/language');
		$current_language = $this->model_localisation_language->getLanguage($this->config->get('config_language_id'));
		
		if (file_exists(DIR_LANGUAGE.'/'.$current_language['directory'].'/account/custom_register.php')){
			$this->language->load('account/custom_register');
		} 
		// else we load a default language file (english)
		else {
			// by adding "../english" to the path we can go up a level in the directory tree and go down
			// again to load the subdirectory "english" (to know how Opencart builds the path, see the file
			// "system/library/language.php"
			$this->language->load('../english/account/custom_register');
		}
		
		$json = array();
		
		// Validate if customer is logged in.
		if ($this->customer->isLogged()) {
			$json['redirect'] = $this->url->link('checkout/checkout', '', 'SSL');
		} 			
		
		// Validate cart has products and has stock.
		if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
			$json['redirect'] = $this->url->link('checkout/cart');		
		}
		
		// Check if guest checkout is avaliable.			
		if (!$this->config->get('config_guest_checkout') || $this->config->get('config_customer_price') || $this->cart->hasDownload()) {
			$json['redirect'] = $this->url->link('checkout/checkout', '', 'SSL');
		} 
					
		if (!$json) {
		
		
			// AntiSpam
			if ( $this->config->get('custom_register_enable_antispam') ){
				if ($this->request->post['phone800'] !== ''){
					// skip the registration
					$json['error']['spam'] = true;
				}
			}
			// end AntiSpam

		
		
			// if firstname enabled
			if ( $this->config->get('custom_register_guest_enable_firstname') ){
				
				// if required 
				if ( $this->config->get('custom_register_guest_firstname_required') ) {
		
					// if empty or out of range
					if ( (strlen(utf8_decode($this->request->post['firstname'])) < 1) || (strlen(utf8_decode($this->request->post['firstname'])) > 32) ) {

						$json['error']['firstname'] = $this->language->get('error_firstname');
					}
				}
				// if not required, not empty but out of range
				elseif ( strlen(utf8_decode($this->request->post['firstname'])) > 32 ) {
					$json['error']['firstname'] = $this->language->get('error_firstname');
				}
			}
			// if the field wasn't enabled we initialize the corresponding post var with an empty value to 
			// avoid "Notice: Undefined index firstname..." errors (you can see them in the json answer)
			else $this->request->post['firstname'] = '';
			

			// if lastname enabled
			if ( $this->config->get('custom_register_guest_enable_lastname') ){
				
				// if required 
				if ( $this->config->get('custom_register_guest_lastname_required') ) {

					// if empty or out of range
					if ( (strlen(utf8_decode($this->request->post['lastname'])) < 1) || (strlen(utf8_decode($this->request->post['lastname'])) > 32) ) {
						$json['error']['lastname'] = $this->language->get('error_lastname');
					}	
				}
				// if not required, not empty but out of range
				elseif ( strlen(utf8_decode($this->request->post['lastname'])) > 32 ) {
					$json['error']['lastname'] = $this->language->get('error_lastname');
				}	
			}
			else $this->request->post['lastname'] = '';
			
			
			// if email enabled
			if ( $this->config->get('custom_register_guest_enable_email') ){
				
				// if required 
				if ( $this->config->get('custom_register_guest_email_required') ) {

					// if empty or out of range or in a wrong format
					if ( ( strlen(utf8_decode($this->request->post['email'])) < 1 || strlen(utf8_decode($this->request->post['email'])) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['email']) ) {
						$json['error']['email'] = $this->language->get('error_email');
					}	
				}
				// if not required, not empty and (( out of range) or (in a wrong format))
				elseif ( strlen(utf8_decode($this->request->post['email'])) > 0 && ( strlen(utf8_decode($this->request->post['email'])) > 96 || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['email'] ) ) ) {
					$json['error']['email'] = $this->language->get('error_email');
				}	
			}
			else $this->request->post['email'] = '';
	

			// if telephone enabled
			if ( $this->config->get('custom_register_guest_enable_telephone') ){
				
				// if required 
				if ( $this->config->get('custom_register_guest_telephone_required') ) {

					// if empty or out of range
					if ( (strlen(utf8_decode($this->request->post['telephone'])) < 3) || (strlen(utf8_decode($this->request->post['telephone'])) > 32) ) {
						$json['error']['telephone'] = $this->language->get('error_telephone');
					}	
				}
				// if not required, not empty and out of range
				elseif ( (strlen(utf8_decode($this->request->post['telephone'])) > 0 ) && (strlen(utf8_decode($this->request->post['telephone'])) < 3) || (strlen(utf8_decode($this->request->post['telephone'])) > 32) ) {
					$json['error']['telephone'] = $this->language->get('error_telephone');
				}	
			}
			else $this->request->post['telephone'] = '';

		
		
		// Added checking for company:
			// if company enabled
			if ( $this->config->get('custom_register_guest_enable_company') ){
				
				// if required 
				if ( $this->config->get('custom_register_guest_company_required') ) {

					// if empty or out of range
					if ( (strlen(utf8_decode($this->request->post['company'])) < 3) || (strlen(utf8_decode($this->request->post['company'])) > 32) ) {
						$json['error']['company'] = $this->language->get('error_company');
					}	
				}
				// if not required, not empty but out of range
				elseif ( (strlen(utf8_decode($this->request->post['company'])) > 0 ) && (strlen(utf8_decode($this->request->post['company'])) < 3) || (strlen(utf8_decode($this->request->post['company'])) > 32) ) {
					$json['error']['company'] = $this->language->get('error_company');
				}	
			}
			else $this->request->post['company'] = '';		
			
		
			// CUSTOMER GROUPS, TAX ID AND COMPANY ID HAVE BEEN ADDED WITH Oc 1.5.3
			if ( version_compare(VERSION, '1.5.3', '>=') ) {
			
				// Customer Group
				$this->load->model('account/customer_group');
				
				
				if (isset($this->request->post['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($this->request->post['customer_group_id'], $this->config->get('config_customer_group_display'))) {
					
					$customer_group_id = $this->request->post['customer_group_id'];
				
				} else if ( is_array($this->config->get('config_customer_group_display')) && in_array($this->request->post['customer_group_id'], $this->config->get('config_customer_group_display')) ) {
					
					$customer_group_id = $this->config->get('config_customer_group_id');
				} else {
				
					$customer_group_id = 0;
				}
				
				$customer_group = $this->model_account_customer_group->getCustomerGroup($customer_group_id);
					
				if ($customer_group) {	
					// Company ID
					if ($customer_group['company_id_display'] && $customer_group['company_id_required'] && empty($this->request->post['company_id'])) {
						$json['error']['company_id'] = $this->language->get('error_company_id');
					}
					
					// Tax ID
					if ($customer_group['tax_id_display'] && $customer_group['tax_id_required'] && empty($this->request->post['tax_id'])) {
						$json['error']['tax_id'] = $this->language->get('error_tax_id');
					}						
				}
			}
			
			
	// Added checking for fax: 
			// if fax enabled
			if ( $this->config->get('custom_register_guest_enable_fax') ){
				
				// if required 
				if ( $this->config->get('custom_register_guest_fax_required') ) {

					// if empty or out of range
					if ( (strlen(utf8_decode($this->request->post['fax'])) < 3) || (strlen(utf8_decode($this->request->post['fax'])) > 32) ) {
						$json['error']['fax'] = $this->language->get('error_fax');
					}	
				}
				// if not required, not empty but out of range
				elseif ( (strlen(utf8_decode($this->request->post['fax'])) > 0 ) && (strlen(utf8_decode($this->request->post['fax'])) < 3) || (strlen(utf8_decode($this->request->post['fax'])) > 32) ) {

					$json['error']['fax'] = $this->language->get('error_fax');
				}	
			}
			else $this->request->post['fax'] = '';
			


			
			// if address 1 enabled
			if ( $this->config->get('custom_register_guest_enable_address_1') ){
				
				// if required 
				if ( $this->config->get('custom_register_guest_address_1_required') ) {

					// if empty or out of range
					if ( (strlen(utf8_decode($this->request->post['address_1'])) < 3) || (strlen(utf8_decode($this->request->post['address_1'])) > 128) ) {
						$json['error']['address_1'] = $this->language->get('error_address_1');
					}	
				}
				// if not required, not empty but out of range
				elseif ( (strlen(utf8_decode($this->request->post['address_1'])) > 0 ) && (strlen(utf8_decode($this->request->post['address_1'])) < 3) || (strlen(utf8_decode($this->request->post['address_1'])) > 128) ) {
					$json['error']['address_1'] = $this->language->get('error_address_1');
				}	
			}
			else $this->request->post['address_1'] = '';
		
	
	// Added checking for address 2:
			// if address 2 enabled
			if ( $this->config->get('custom_register_guest_enable_address_2') ){
				
				// if required 
				if ( $this->config->get('custom_register_guest_address_2_required') ) {

					// if empty or out of range
					if ( (strlen(utf8_decode($this->request->post['address_2'])) < 3) || (strlen(utf8_decode($this->request->post['address_2'])) > 128) ) {
						$json['error']['address_2'] = $this->language->get('error_address_2');
					}	
				}
				// if not required, not empty but out of range
				elseif ( (strlen(utf8_decode($this->request->post['address_2'])) > 0 ) && (strlen(utf8_decode($this->request->post['address_2'])) < 3) || (strlen(utf8_decode($this->request->post['address_2'])) > 128) ) {
					$json['error']['address_2'] = $this->language->get('error_address_2');
				}	
			}
			else $this->request->post['address_2'] = '';
	
	
			// if city enabled
			if ( $this->config->get('custom_register_guest_enable_city') ){
				
				// if required 
				if ( $this->config->get('custom_register_guest_city_required') ) {

					// if empty or out of range
					if ( (strlen(utf8_decode($this->request->post['city'])) < 2) || (strlen(utf8_decode($this->request->post['city'])) > 128) ) {
						$json['error']['city'] = $this->language->get('error_city');
					}	
				}
				// if not required, not empty but out of range
				elseif ( (strlen(utf8_decode($this->request->post['city'])) > 0 ) && (strlen(utf8_decode($this->request->post['city'])) < 2) || (strlen(utf8_decode($this->request->post['city'])) > 128) ) {
					$json['error']['city'] = $this->language->get('error_city');
				}	
			}
			else $this->request->post['city'] = '';
		
		
			
		// Added checking for country  
			if ( $this->config->get('custom_register_guest_enable_country') ){
				
				if ( $this->config->get('custom_register_guest_country_required') ) {
				
					if ( $this->request->post['country_id'] == '' ) {
						$json['error']['country'] = $this->language->get('error_country');
					}	
				}
			}
			
			
			$this->load->model('localisation/country');
			$country_info = $this->model_localisation_country->getCountry($this->request->post['country_id']);
			
					
		// Added checking for postal code 
		
		
			// if the current country requires the postcode even if it is disabled from admin control panel	
			if ( ($country_info && $country_info['postcode_required'] ) && ( strlen(utf8_decode($this->request->post['postcode'])) < 2 || strlen(utf8_decode($this->request->post['postcode'])) > 10)  ) {
				$json['error']['postcode'] = $this->language->get('error_postcode');
			}
		
			if ( $this->config->get('custom_register_guest_enable_postcode') ){
				
				if ( $this->config->get('custom_register_guest_postcode_required') ) {
				
					// if empty or out of range:
					if ( strlen(utf8_decode($this->request->post['postcode'])) < 2 || strlen(utf8_decode($this->request->post['postcode'])) > 10) {
						$json['error']['postcode'] = $this->language->get('error_postcode');
					}
				}
				// if not required, not empty but out of range
				elseif ( strlen(utf8_decode($this->request->post['postcode'])) > 0  && ( strlen(utf8_decode($this->request->post['postcode'])) < 2 || strlen(utf8_decode($this->request->post['postcode'])) > 10)  ) {
					$json['error']['postcode'] = $this->language->get('error_postcode');
				}		
			}
			else {
				if ( !isset($this->request->post['postcode']) ) $this->request->post['postcode'] = '';
			}
			
			
			
			// VAT VALIDATION IS AVAILABLE FROM Oc 1.5.3
			if ( version_compare(VERSION, '1.5.3', '>=') ) {	
			
			// VAT Validation
				if ($country_info) {
					
					// VAT Validation
					$this->load->helper('vat');
					
					if ($this->config->get('config_vat') && $this->request->post['tax_id'] && (vat_validation($country_info['iso_code_2'], $this->request->post['tax_id']) == 'invalid')) {
						$json['error']['tax_id'] = $this->language->get('error_vat');
					}					
				}
			}
	
	
			
			// if zone enabled
			if ( $this->config->get('custom_register_guest_enable_zone') ){
				
				// if required 
				if ( $this->config->get('custom_register_guest_zone_required') ) {
				
					if ($this->request->post['zone_id'] == '') {
						$json['error']['zone'] = $this->language->get('error_zone');
					}
				}
			}
			
			
	
			if ($this->config->get('config_account_id')) {
				$this->load->model('catalog/information');
				
				$information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));
				
				if ($information_info && !isset($this->request->post['agree'])) {
					$json['error']['warning'] = sprintf($this->language->get('error_agree'), $information_info['title']);
				}
			}
			
			
			
		}
		
		if (!$json) {
		
			// CUSTOMER GROUPS, TAX ID AND COMPANY ID HAVE BEEN ADDED WITH Oc 1.5.3
			if ( version_compare(VERSION, '1.5.3', '>=') ) {
				$this->session->data['guest']['customer_group_id'] = $customer_group_id;
				$this->session->data['guest']['payment']['company_id'] = $this->request->post['company_id'];
				$this->session->data['guest']['payment']['tax_id'] = $this->request->post['tax_id'];
			}
			
			$this->session->data['guest']['firstname'] = $this->request->post['firstname'];
			$this->session->data['guest']['lastname'] = $this->request->post['lastname'];
			$this->session->data['guest']['email'] = $this->request->post['email'];
			$this->session->data['guest']['telephone'] = $this->request->post['telephone'];
			$this->session->data['guest']['fax'] = $this->request->post['fax'];
			
			$this->session->data['guest']['payment']['firstname'] = $this->request->post['firstname'];
			$this->session->data['guest']['payment']['lastname'] = $this->request->post['lastname'];				
			$this->session->data['guest']['payment']['company'] = $this->request->post['company'];
			
			$this->session->data['guest']['payment']['address_1'] = $this->request->post['address_1'];
			$this->session->data['guest']['payment']['address_2'] = $this->request->post['address_2'];
			$this->session->data['guest']['payment']['postcode'] = $this->request->post['postcode'];
			$this->session->data['guest']['payment']['city'] = $this->request->post['city'];
			$this->session->data['guest']['payment']['country_id'] = $this->request->post['country_id'];
			$this->session->data['guest']['payment']['zone_id'] = $this->request->post['zone_id'];
							
			$this->load->model('localisation/country');
			
			$country_info = $this->model_localisation_country->getCountry($this->request->post['country_id']);
			
			if ($country_info) {
				$this->session->data['guest']['payment']['country'] = $country_info['name'];	
				$this->session->data['guest']['payment']['iso_code_2'] = $country_info['iso_code_2'];
				$this->session->data['guest']['payment']['iso_code_3'] = $country_info['iso_code_3'];
				$this->session->data['guest']['payment']['address_format'] = $country_info['address_format'];
			} else {
				$this->session->data['guest']['payment']['country'] = '';	
				$this->session->data['guest']['payment']['iso_code_2'] = '';
				$this->session->data['guest']['payment']['iso_code_3'] = '';
				$this->session->data['guest']['payment']['address_format'] = '';
			}
						
			$this->load->model('localisation/zone');

			$zone_info = $this->model_localisation_zone->getZone($this->request->post['zone_id']);
			
			if ($zone_info) {
				$this->session->data['guest']['payment']['zone'] = $zone_info['name'];
				$this->session->data['guest']['payment']['zone_code'] = $zone_info['code'];
			} else {
				$this->session->data['guest']['payment']['zone'] = '';
				$this->session->data['guest']['payment']['zone_code'] = '';
			}
			
			if (!empty($this->request->post['shipping_address'])) {
				$this->session->data['guest']['shipping_address'] = true;
			} else {
				$this->session->data['guest']['shipping_address'] = false;
			}
			
			// Default Payment Address
			$this->session->data['payment_country_id'] = $this->request->post['country_id'];
			$this->session->data['payment_zone_id'] = $this->request->post['zone_id'];
			
			if ($this->session->data['guest']['shipping_address']) {
				$this->session->data['guest']['shipping']['firstname'] = $this->request->post['firstname'];
				$this->session->data['guest']['shipping']['lastname'] = $this->request->post['lastname'];
				$this->session->data['guest']['shipping']['company'] = $this->request->post['company'];
				$this->session->data['guest']['shipping']['address_1'] = $this->request->post['address_1'];
				$this->session->data['guest']['shipping']['address_2'] = $this->request->post['address_2'];
				$this->session->data['guest']['shipping']['postcode'] = $this->request->post['postcode'];
				$this->session->data['guest']['shipping']['city'] = $this->request->post['city'];
				$this->session->data['guest']['shipping']['country_id'] = $this->request->post['country_id'];
				$this->session->data['guest']['shipping']['zone_id'] = $this->request->post['zone_id'];
				
				if ($country_info) {
					$this->session->data['guest']['shipping']['country'] = $country_info['name'];	
					$this->session->data['guest']['shipping']['iso_code_2'] = $country_info['iso_code_2'];
					$this->session->data['guest']['shipping']['iso_code_3'] = $country_info['iso_code_3'];
					$this->session->data['guest']['shipping']['address_format'] = $country_info['address_format'];
				} else {
					$this->session->data['guest']['shipping']['country'] = '';	
					$this->session->data['guest']['shipping']['iso_code_2'] = '';
					$this->session->data['guest']['shipping']['iso_code_3'] = '';
					$this->session->data['guest']['shipping']['address_format'] = '';
				}
	
				if ($zone_info) {
					$this->session->data['guest']['shipping']['zone'] = $zone_info['name'];
					$this->session->data['guest']['shipping']['zone_code'] = $zone_info['code'];
				} else {
					$this->session->data['guest']['shipping']['zone'] = '';
					$this->session->data['guest']['shipping']['zone_code'] = '';
				}
				
				// Default Shipping Address
				$this->session->data['shipping_country_id'] = $this->request->post['country_id'];
				$this->session->data['shipping_zone_id'] = $this->request->post['zone_id'];
				$this->session->data['shipping_postcode'] = $this->request->post['postcode'];
			}
			
			$this->session->data['account'] = 'guest';
			
			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
		}
					
		$this->response->setOutput(json_encode($json));	
	}
	
  	public function zone() {
		$output = '<option value="">' . $this->language->get('text_select') . '</option>';
		
		$this->load->model('localisation/zone');

    	$results = $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']);
        
      	foreach ($results as $result) {
        	$output .= '<option value="' . $result['zone_id'] . '"';
	
	    	if (isset($this->request->get['zone_id']) && ($this->request->get['zone_id'] == $result['zone_id'])) {
	      		$output .= ' selected="selected"';
	    	}
	
	    	$output .= '>' . $result['name'] . '</option>';
    	} 
		
		if (!$results) {
		  	$output .= '<option value="0">' . $this->language->get('text_none') . '</option>';
		}
	
		$this->response->setOutput($output);
  	}
}
?>