<?php
// ***************************************************
//             Custom Registration Fields   
//       
// Author : Francesco Pisanò - francesco1279@gmail.com
//              
//                   www.leverod.com		
//               © All rights reserved	  
// ***************************************************




class ControllerCheckoutShippingAddress extends Controller {
	public function index() {
	
		$this->language->load('checkout/checkout');
	
		// check if a language file for this module is available for 
		// the current opencart installation:
		$this->load->model('localisation/language');
		$current_language = $this->model_localisation_language->getLanguage($this->config->get('config_language_id'));
		
		if (file_exists(DIR_LANGUAGE.'/'.$current_language['directory'].'/account/custom_register.php')){
			$this->language->load('account/custom_register');
		} 
		// else we load a default language file (english)
		else {
			// by adding "../english" to the path we can go up a level in the directory tree and go down
			// again to load the subdirectory "english" (to know how Opencart builds the path, see the file
			// "system/library/language.php"
			$this->language->load('../english/account/custom_register');
		}
		
		$this->data['text_address_existing'] = $this->language->get('text_address_existing');
		$this->data['text_address_new'] = $this->language->get('text_address_new');
		$this->data['text_select'] = $this->language->get('text_select');
		$this->data['text_none'] = $this->language->get('text_none');

		$this->data['entry_firstname'] = $this->language->get('entry_firstname');
		$this->data['entry_lastname'] = $this->language->get('entry_lastname');
		$this->data['entry_company'] = $this->language->get('entry_company');
		$this->data['entry_address_1'] = $this->language->get('entry_address_1');
		$this->data['entry_address_2'] = $this->language->get('entry_address_2');
		$this->data['entry_postcode'] = $this->language->get('entry_postcode');
		$this->data['entry_city'] = $this->language->get('entry_city');
		$this->data['entry_country'] = $this->language->get('entry_country');
		$this->data['entry_zone'] = $this->language->get('entry_zone');
	
		$this->data['button_continue'] = $this->language->get('button_continue');
		
		// my settings 
		$reg_entries = array(	
			'firstname',
			'lastname',
			'company',
			'address_1',
			'address_2',
			'city',
			'country',
			'postcode',
			'zone'
		);
		
		foreach ($reg_entries as $reg_entry) {
			$this->data['custom_register_member_enable_'.$reg_entry] = $this->config->get('custom_register_member_enable_'.$reg_entry);
			$this->data['custom_register_member_'.$reg_entry.'_required'] = $this->config->get('custom_register_member_'.$reg_entry.'_required');
		}
		
		
		// This flag tells whether the field country is disabled and the field zones is not
		$this->data['country_disabled_zone_enabled'] = !$this->data['custom_register_member_enable_country'] && $this->data['custom_register_member_enable_zone'];
		
		// end my settings
		
			
		if (isset($this->session->data['shipping_address_id'])) {
			$this->data['address_id'] = $this->session->data['shipping_address_id'];
		} else {
			$this->data['address_id'] = $this->customer->getAddressId();
		}

		$this->load->model('account/address');

		$this->data['addresses'] = $this->model_account_address->getAddresses();


		if (isset($this->session->data['shipping_postcode'])) {
			$this->data['postcode'] = $this->session->data['shipping_postcode'];		
		} else {
			$this->data['postcode'] = '';
		}
			
		
		$this->data['custom_register_default_country'] = $this->config->get('custom_register_default_country');	


		
		if (!$this->data['custom_register_member_enable_country'] && !$this->data['custom_register_member_enable_zone']) {
			$this->data['country_id'] = '';
		} elseif (isset($this->session->data['shipping_country_id']) && $this->session->data['shipping_country_id'] > 0) {
			$this->data['country_id'] = $this->session->data['shipping_country_id'];	
		} elseif (!empty($this->data['custom_register_default_country'])) {
			$this->data['country_id'] = $this->data['custom_register_default_country'];		
		} else {
			$this->data['country_id'] = $this->config->get('config_country_id');
		}
	
				
		if (isset($this->session->data['shipping_zone_id']) && $this->session->data['shipping_zone_id'] > 0 ) {
			$this->data['zone_id'] = $this->session->data['shipping_zone_id'];		
		} else {
			$this->data['zone_id'] = '';
		}
						
		$this->load->model('localisation/country');
		
		$this->data['countries'] = $this->model_localisation_country->getCountries();

		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/custom_shipping_address.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/checkout/custom_shipping_address.tpl';
		} else {
			$this->template = 'default/template/checkout/custom_shipping_address.tpl';
		}
				
		$this->response->setOutput($this->render());
  	}	
	
	public function validate() {
	
		$this->language->load('checkout/checkout');
		
		// check if a language file for this module is available for 
		// the current opencart installation:
		$this->load->model('localisation/language');
		$current_language = $this->model_localisation_language->getLanguage($this->config->get('config_language_id'));
		
		if (file_exists(DIR_LANGUAGE.'/'.$current_language['directory'].'/account/custom_register.php')){
			$this->language->load('account/custom_register');
		} 
		// else we load a default language file (english)
		else {
			// by adding "../english" to the path we can go up a level in the directory tree and go down
			// again to load the subdirectory "english" (to know how Opencart builds the path, see the file
			// "system/library/language.php"
			$this->language->load('../english/account/custom_register');
		}
		
		$json = array();
		
		// Validate if customer is logged in.
		if (!$this->customer->isLogged()) {
			$json['redirect'] = $this->url->link('checkout/checkout', '', 'SSL');
		}
		
		// Validate if shipping is required. If not the customer should not have reached this page.
		if (!$this->cart->hasShipping()) {
			$json['redirect'] = $this->url->link('checkout/checkout', '', 'SSL');
		}
		
		// Validate cart has products and has stock.		
		if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
			$json['redirect'] = $this->url->link('checkout/cart');
		}	

		// Validate minimum quantity requirments.			
		$products = $this->cart->getProducts();
				
		foreach ($products as $product) {
			$product_total = 0;
				
			foreach ($products as $product_2) {
				if ($product_2['product_id'] == $product['product_id']) {
					$product_total += $product_2['quantity'];
				}
			}		
			
			if ($product['minimum'] > $product_total) {
				$json['redirect'] = $this->url->link('checkout/cart');
				
				break;
			}				
		}
								
		if (!$json) {
			if (isset($this->request->post['shipping_address']) && $this->request->post['shipping_address'] == 'existing') {

				// the function getAddresses() has been uptated from Oc 1.5.3	
				if ( version_compare(VERSION, '1.5.3', '>=') ) {
					
					$this->load->model('account/address');
					
					if (empty($this->request->post['address_id'])) {
						$json['error']['warning'] = $this->language->get('error_address');
					} elseif (!in_array($this->request->post['address_id'], array_keys($this->model_account_address->getAddresses()))) {
						$json['error']['warning'] = $this->language->get('error_address');
					}
				}
				else { // for Oc < 1.5.3
				
					if (empty($this->request->post['address_id'])) {
						$json['error']['warning'] = $this->language->get('error_address');
					}
				}
						
				if (!$json) {			
					$this->session->data['shipping_address_id'] = $this->request->post['address_id'];
					
					// Default Shipping Address
					$this->load->model('account/address');

					$address_info = $this->model_account_address->getAddress($this->request->post['address_id']);
					
					if ($address_info) {
						$this->session->data['shipping_country_id'] = $address_info['country_id'];
						$this->session->data['shipping_zone_id'] = $address_info['zone_id'];
						$this->session->data['shipping_postcode'] = $address_info['postcode'];						
					} else {
						unset($this->session->data['shipping_country_id']);	
						unset($this->session->data['shipping_zone_id']);	
						unset($this->session->data['shipping_postcode']);
					}
					
					unset($this->session->data['shipping_method']);							
					unset($this->session->data['shipping_methods']);
				}
			} 
			
			if ($this->request->post['shipping_address'] == 'new') {
			
			
				// if firstname enabled
				if ( $this->config->get('custom_register_member_enable_firstname') ){
					
					// if required 
					if ( $this->config->get('custom_register_member_firstname_required') ) {

						// if empty or out of range
						if ( (strlen(utf8_decode($this->request->post['firstname'])) < 1) || (strlen(utf8_decode($this->request->post['firstname'])) > 32) ) {
							$json['error']['firstname'] = $this->language->get('error_firstname');
						}	
					}
					// if not required, not empty but out of range
					elseif ( strlen(utf8_decode($this->request->post['firstname'])) > 32 ) {
						$json['error']['firstname'] = $this->language->get('error_firstname');
					}	
				}

				// if the field wasn't enabled we initialize the corresponding post var with an empty value to 
				// avoid "Notice: Undefined index firstname..." errors (you can see them in the json answer)
				else $this->request->post['firstname'] = '';
			
			
				// if lastname enabled
				if ( $this->config->get('custom_register_member_enable_lastname') ){
					
					// if required 
					if ( $this->config->get('custom_register_member_lastname_required') ) {

						// if empty or out of range
						if ( (strlen(utf8_decode($this->request->post['lastname'])) < 1) || (strlen(utf8_decode($this->request->post['lastname'])) > 32) ) {
							$json['error']['lastname'] = $this->language->get('error_lastname');
						}	
					}
					// if not required, not empty but out of range
					elseif ( strlen(utf8_decode($this->request->post['lastname'])) > 32 ) {
						$json['error']['lastname'] = $this->language->get('error_lastname');
					}	
				}


				else $this->request->post['lastname'] = '';
				
				
			// Added checking for company:
				// if company enabled
				if ( $this->config->get('custom_register_member_enable_company') ){
					
					// if required 
					if ( $this->config->get('custom_register_member_company_required') ) {

						// if empty or out of range
						if ( (strlen(utf8_decode($this->request->post['company'])) < 3) || (strlen(utf8_decode($this->request->post['company'])) > 32) ) {
							$json['error']['company'] = $this->language->get('error_company');
						}	
					}
					// if not required, not empty but out of range
					elseif ( (strlen(utf8_decode($this->request->post['company'])) > 0 ) && (strlen(utf8_decode($this->request->post['company'])) < 3) || (strlen(utf8_decode($this->request->post['company'])) > 32) ) {
						$json['error']['company'] = $this->language->get('error_company');
					}	
				}
				else $this->request->post['company'] = '';	
				
				
				// if address 1 enabled
				if ( $this->config->get('custom_register_member_enable_address_1') ){
					
					// if required 
					if ( $this->config->get('custom_register_member_address_1_required') ) {

						// if empty or out of range
						if ( (strlen(utf8_decode($this->request->post['address_1'])) < 3) || (strlen(utf8_decode($this->request->post['address_1'])) > 128) ) {
							$json['error']['address_1'] = $this->language->get('error_address_1');
						}	
					}
					// if not required, not empty but out of range
					elseif ( (strlen(utf8_decode($this->request->post['address_1'])) > 0 ) && (strlen(utf8_decode($this->request->post['address_1'])) < 3) || (strlen(utf8_decode($this->request->post['address_1'])) > 128) ) {
						$json['error']['address_1'] = $this->language->get('error_address_1');
					}	
				}
				else $this->request->post['address_1'] = '';
				

		// Added checking for address 2:
				// if address 2 enabled
				if ( $this->config->get('custom_register_member_enable_address_2') ){
					
					// if required 
					if ( $this->config->get('custom_register_member_address_2_required') ) {

						// if empty or out of range
						if ( (strlen(utf8_decode($this->request->post['address_2'])) < 3) || (strlen(utf8_decode($this->request->post['address_2'])) > 128) ) {
							$json['error']['address_2'] = $this->language->get('error_address_2');
						}	
					}
					// if not required, not empty but out of range
					elseif ( (strlen(utf8_decode($this->request->post['address_2'])) > 0 ) && (strlen(utf8_decode($this->request->post['address_2'])) < 3) || (strlen(utf8_decode($this->request->post['address_2'])) > 128) ) {
						$json['error']['address_2'] = $this->language->get('error_address_2');
					}	
				}
				else $this->request->post['address_2'] = '';
		
			
				// if city enabled
				if ( $this->config->get('custom_register_member_enable_city') ){
					
					// if required 
					if ( $this->config->get('custom_register_member_city_required') ) {

						// if empty or out of range
						if ( (strlen(utf8_decode($this->request->post['city'])) < 2) || (strlen(utf8_decode($this->request->post['city'])) > 128) ) {
							$json['error']['city'] = $this->language->get('error_city');
						}	
					}
					// if not required, not empty but out of range
					elseif ( (strlen(utf8_decode($this->request->post['city'])) > 0 ) && (strlen(utf8_decode($this->request->post['city'])) < 2) || (strlen(utf8_decode($this->request->post['city'])) > 128) ) {
						$json['error']['city'] = $this->language->get('error_city');
					}	
				}
				else $this->request->post['city'] = '';
				

				
				
				// Added checking for country  
				if ( $this->config->get('custom_register_member_enable_country') ){
					
					if ( $this->config->get('custom_register_member_country_required') ) {
					
						if ( $this->request->post['country_id'] == '' ) {
							$json['error']['country'] = $this->language->get('error_country');
						}	
					}
				}

				
				$this->load->model('localisation/country');
				$country_info = $this->model_localisation_country->getCountry($this->request->post['country_id']);
						
				
		// Added checking for postal code 
		
				// if the current country requires the postcode even if it is disabled from admin control panel	
				if ( ($country_info && $country_info['postcode_required'] ) && ( strlen(utf8_decode($this->request->post['postcode'])) < 2 || strlen(utf8_decode($this->request->post['postcode'])) > 10)  ) {
					$json['error']['postcode'] = $this->language->get('error_postcode');
				}

		
				if ( $this->config->get('custom_register_member_enable_postcode') ){
					
					if ( $this->config->get('custom_register_member_postcode_required') ) {
					
						// if empty or out of range:
						if ( strlen(utf8_decode($this->request->post['postcode'])) < 2 || strlen(utf8_decode($this->request->post['postcode'])) > 10) {
							$json['error']['postcode'] = $this->language->get('error_postcode');
						}
					}
					// if not required, not empty but out of range
					elseif ( strlen(utf8_decode($this->request->post['postcode'])) > 0  && ( strlen(utf8_decode($this->request->post['postcode'])) < 2 || strlen(utf8_decode($this->request->post['postcode'])) > 10)  ) {
						$json['error']['postcode'] = $this->language->get('error_postcode');
					}		
				}
				else {
					if ( !isset($this->request->post['postcode']) ) $this->request->post['postcode'] = '';
				}
				
								
				
				// if zone enabled
				if ( $this->config->get('custom_register_member_enable_zone') ){
					
					// if required 
					if ( $this->config->get('custom_register_member_zone_required') ) {
					
						if ($this->request->post['zone_id'] == '') {
							$json['error']['zone'] = $this->language->get('error_zone');
						}
					}
				}
				
				
				
				if (!$json) {						
					// Default Shipping Address
					$this->load->model('account/address');		
					
					$this->session->data['shipping_address_id'] = $this->model_account_address->addAddress($this->request->post);

					$this->session->data['shipping_country_id'] = $this->request->post['country_id'];
					$this->session->data['shipping_zone_id'] = $this->request->post['zone_id'];
					$this->session->data['shipping_postcode'] = $this->request->post['postcode'];
									
					unset($this->session->data['shipping_method']);						
					unset($this->session->data['shipping_methods']);
				}
			}
		}
		
		$this->response->setOutput(json_encode($json));
	}

}
?>