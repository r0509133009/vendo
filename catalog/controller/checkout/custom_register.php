<?php
// ***************************************************
//             Custom Registration Fields   
//       
// Author : Francesco Pisanò - francesco1279@gmail.com
//              
//                   www.leverod.com		
//               © All rights reserved	  
// ***************************************************




class ControllerCheckoutRegister extends Controller {
  	public function index() {
	
		$this->language->load('checkout/checkout');
			
		// check if a language file for this module is available for 
		// the current opencart installation:
		$this->load->model('localisation/language');
		$current_language = $this->model_localisation_language->getLanguage($this->config->get('config_language_id'));
		

		if (file_exists(DIR_LANGUAGE.'/'.$current_language['directory'].'/account/custom_register.php')){
			$this->language->load('account/custom_register');
		} 
		// else we load a default language file (english)
		else {
			// by adding "../english" to the path we can go up a level in the directory tree and go down
			// again to load the subdirectory "english" (to know how Opencart builds the path, see the file
			// "system/library/language.php"
			$this->language->load('../english/account/custom_register');
		}
		
// added customizable javascript (for Oc and MijoShop)
		$filename = 'catalog/view/javascript/custom_register.js';
		if ( (file_exists($filename) && filesize($filename) != 0) || (file_exists('components/com_mijoshop/opencart/'.$filename) && filesize('components/com_mijoshop/opencart/'.$filename) != 0) ) {
			$this->document->addScript($filename);
		}
// added customizable style	(for Oc and MijoShop)
		$filename = 'catalog/view/theme/default/stylesheet/custom_register.css';
		if ( (file_exists($filename) && filesize($filename) != 0) || (file_exists('components/com_mijoshop/opencart/'.$filename) && filesize('components/com_mijoshop/opencart/'.$filename) != 0) ) {
			$this->document->addStyle($filename);
		}
		
		
		
		$this->data['text_your_details'] = $this->language->get('text_your_details');
		$this->data['text_your_address'] = $this->language->get('text_your_address');
		$this->data['text_your_password'] = $this->language->get('text_your_password');

		$this->data['text_select'] = $this->language->get('text_select');
		$this->data['text_none'] = $this->language->get('text_none');
						
		$this->data['entry_firstname'] = $this->language->get('entry_firstname');
		$this->data['entry_lastname'] = $this->language->get('entry_lastname');
		$this->data['entry_email'] = $this->language->get('entry_email');
		$this->data['entry_telephone'] = $this->language->get('entry_telephone');
		$this->data['entry_fax'] = $this->language->get('entry_fax');
		$this->data['entry_company'] = $this->language->get('entry_company');
		$this->data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$this->data['entry_company_id'] = $this->language->get('entry_company_id');
		$this->data['entry_tax_id'] = $this->language->get('entry_tax_id');		
		$this->data['entry_address_1'] = $this->language->get('entry_address_1');
		$this->data['entry_address_2'] = $this->language->get('entry_address_2');
		$this->data['entry_postcode'] = $this->language->get('entry_postcode');
		$this->data['entry_city'] = $this->language->get('entry_city');
		$this->data['entry_country'] = $this->language->get('entry_country');
		$this->data['entry_zone'] = $this->language->get('entry_zone');
		$this->data['entry_newsletter'] = sprintf($this->language->get('entry_newsletter'), $this->config->get('config_name'));
		$this->data['entry_password'] = $this->language->get('entry_password');
		$this->data['entry_confirm'] = $this->language->get('entry_confirm');
		$this->data['entry_shipping'] = $this->language->get('entry_shipping');

		$this->data['button_continue'] = $this->language->get('button_continue');
		$this->data['entry_anti_spam_label'] = $this->language->get('entry_anti_spam_label');


		// my settings 
		$reg_entries = array(	
			'firstname',
			'lastname',
			'email',
			'telephone',
			'fax',
			'password',
			'confirm',
			'company',
			'address_1',
			'address_2',
			'city',
			'country',
			'postcode',
			'zone',
			'newsletter',		
			'shipping_address',
			'agree'
		);
		
		foreach ($reg_entries as $reg_entry) {
			$this->data['custom_register_member_enable_'.$reg_entry] = $this->config->get('custom_register_member_enable_'.$reg_entry);
			$this->data['custom_register_member_'.$reg_entry.'_required'] = $this->config->get('custom_register_member_'.$reg_entry.'_required');
		}
		
		$this->data['custom_register_enable_antispam'] = $this->config->get('custom_register_enable_antispam');
		
		// end my settings	
		
		
		// This flag tells whether the field country is disabled and the field zones is not
		$this->data['country_disabled_zone_enabled'] = !$this->data['custom_register_member_enable_country'] && $this->data['custom_register_member_enable_zone'];
		
					
			
		
		
		// CUSTOMER GROUPS HAVE BEEN ADDED WITH Oc 1.5.3
		if ( version_compare(VERSION, '1.5.3', '>=') ) {
		
			$this->data['customer_groups'] = array();
			
			if (is_array($this->config->get('config_customer_group_display'))) {
				$this->load->model('account/customer_group');
				
				$customer_groups = $this->model_account_customer_group->getCustomerGroups();
				
				foreach ($customer_groups  as $customer_group) {
					if (in_array($customer_group['customer_group_id'], $this->config->get('config_customer_group_display'))) {
						$this->data['customer_groups'][] = $customer_group;
					}
				}
			}
			
			$this->data['customer_group_id'] = $this->config->get('config_customer_group_id');
		}
		
	
	
		if (isset($this->session->data['shipping_postcode'])) {
			$this->data['postcode'] = $this->session->data['shipping_postcode'];		
		} else {
			$this->data['postcode'] = '';
		}
		
		
		
		
		$this->data['custom_register_default_country'] = $this->config->get('custom_register_default_country');	
		
		
		
		if (!$this->data['custom_register_member_enable_country'] && !$this->data['custom_register_member_enable_zone']) {
			$this->data['country_id'] = '';
		}
    	elseif (isset($this->session->data['shipping_country_id']) && $this->session->data['shipping_country_id'] > 0) {
			$this->data['country_id'] = $this->session->data['shipping_country_id'];	
		} elseif (!empty($this->data['custom_register_default_country'])) {
			$this->data['country_id'] = $this->data['custom_register_default_country'];			
		} else {	
      		$this->data['country_id'] = $this->config->get('config_country_id');
    	}

			
		
    	if (isset($this->session->data['shipping_zone_id'])) {
			$this->data['zone_id'] = $this->session->data['shipping_zone_id'];			
		} else {
      		$this->data['zone_id'] = '';
    	}
			
			
		$this->load->model('localisation/country');
		
		$this->data['countries'] = $this->model_localisation_country->getCountries();

		if ($this->config->get('config_account_id')) {
			$this->load->model('catalog/information');
			
			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));
			
			if ($information_info) {
				$this->data['text_agree'] = sprintf($this->language->get('text_agree'), $this->url->link('information/information/info', 'information_id=' . $this->config->get('config_account_id'), 'SSL'), $information_info['title'], $information_info['title']);
			} else {
				$this->data['text_agree'] = '';
			}
		} else {
			$this->data['text_agree'] = '';
		}
		
		
		
		if ($this->config->get('custom_register_account_agree_checked') ) {
			$this->data['agree'] = true;
		} else {
			$this->data['agree'] = false;
		}

		
		$this->data['shipping_required'] = $this->cart->hasShipping();
		
		
		if ($this->config->get('custom_register_newsletter_checked') ) {
			$this->data['newsletter_checked'] = true;
		} else {
			$this->data['newsletter_checked'] = false;
		}
		
		
		if ($this->config->get('custom_register_shipping_address_checked') ) {
			$this->data['shipping_address'] = true;
		} else {
			$this->data['shipping_address'] = false;
		}
			

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/custom_register.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/checkout/custom_register.tpl';
		} else {
			$this->template = 'default/template/checkout/custom_register.tpl';
		}
		
		$this->response->setOutput($this->render());		
  	}
	
	public function validate() {
	
		$this->language->load('checkout/checkout');
		
		// check if a language file for this module is available for 
		// the current opencart installation:
		$this->load->model('localisation/language');
		$current_language = $this->model_localisation_language->getLanguage($this->config->get('config_language_id'));
		
		if (file_exists(DIR_LANGUAGE.'/'.$current_language['directory'].'/account/custom_register.php')){
			$this->language->load('account/custom_register');
		} 
		// else we load a default language file (english)
		else {
			// by adding "../english" to the path we can go up a level in the directory tree and go down
			// again to load the subdirectory "english" (to know how Opencart builds the path, see the file
			// "system/library/language.php"
			$this->language->load('../english/account/custom_register');
		}
		
		
		$this->load->model('account/customer');
		
		$json = array();
		
		// Validate if customer is already logged out.
		if ($this->customer->isLogged()) {
			$json['redirect'] = $this->url->link('checkout/checkout', '', 'SSL');			
		}
		
		// Validate cart has products and has stock.
		if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
			$json['redirect'] = $this->url->link('checkout/cart');
		}
		
		// Validate minimum quantity requirments.			
		$products = $this->cart->getProducts();
				
		foreach ($products as $product) {
			$product_total = 0;
				
			foreach ($products as $product_2) {
				if ($product_2['product_id'] == $product['product_id']) {
					$product_total += $product_2['quantity'];
				}
			}		
			
			if ($product['minimum'] > $product_total) {
				$json['redirect'] = $this->url->link('checkout/cart');

				break;
			}				
		}
						
		if (!$json) {
		
		
		
			// AntiSpam
			if ( $this->config->get('custom_register_enable_antispam') ){
				if ($this->request->post['phone800'] !== ''){
					// skip the registration
					$json['error']['spam'] = true;
				}
			}
			// end AntiSpam

			
		
			// if firstname enabled
			if ( $this->config->get('custom_register_member_enable_firstname') ){
				
				// if required 
				if ( $this->config->get('custom_register_member_firstname_required') ) {

					// if empty or out of range
					if ( (strlen(utf8_decode($this->request->post['firstname'])) < 1) || (strlen(utf8_decode($this->request->post['firstname'])) > 32) ) {
						$json['error']['firstname'] = $this->language->get('error_firstname');
					}	
				}
				// if not required, not empty but out of range
				elseif ( strlen(utf8_decode($this->request->post['firstname'])) > 32 ) {
					$json['error']['firstname'] = $this->language->get('error_firstname');
				}	
			}

			// if the field wasn't enabled we initialize the corresponding post var with an empty value to 
			// avoid "Notice: Undefined index firstname..." errors (you can see them in the json answer)
			else $this->request->post['firstname'] = '';
			
			
			// if lastname enabled
			if ( $this->config->get('custom_register_member_enable_lastname') ){
				
				// if required 
				if ( $this->config->get('custom_register_member_lastname_required') ) {

					// if empty or out of range
					if ( (strlen(utf8_decode($this->request->post['lastname'])) < 1) || (strlen(utf8_decode($this->request->post['lastname'])) > 32) ) {
						$json['error']['lastname'] = $this->language->get('error_lastname');
					}	
				}
				// if not required, not empty but out of range
				elseif ( strlen(utf8_decode($this->request->post['lastname'])) > 32 ) {
					$json['error']['lastname'] = $this->language->get('error_lastname');
				}	
			}
			else $this->request->post['lastname'] = '';


			// email is mandatory for members
			if ((strlen(utf8_decode($this->request->post['email'])) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['email'])) {
				$json['error']['email'] = $this->language->get('error_email');
			}

			
			// if email already exists:
			if ($this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
				$json['error']['warning'] = $this->language->get('error_exists');
			}
	

			
			// if telephone enabled
			if ( $this->config->get('custom_register_member_enable_telephone') ){
				
				// if required 
				if ( $this->config->get('custom_register_member_telephone_required') ) {

					// if empty or out of range
					if ( (strlen(utf8_decode($this->request->post['telephone'])) < 3) || (strlen(utf8_decode($this->request->post['telephone'])) > 32) ) {
						$json['error']['telephone'] = $this->language->get('error_telephone');
					}	
				}
				// if not required, not empty but out of range
				elseif ( (strlen(utf8_decode($this->request->post['telephone'])) > 0 ) && (strlen(utf8_decode($this->request->post['telephone'])) < 3) || (strlen(utf8_decode($this->request->post['telephone'])) > 32) ) {
					$json['error']['telephone'] = $this->language->get('error_telephone');
				}	
			}
			else $this->request->post['telephone'] = '';
		
		
	// Added checking for fax: 
			// if fax enabled
			if ( $this->config->get('custom_register_member_enable_fax') ){
				
				// if required 
				if ( $this->config->get('custom_register_member_fax_required') ) {

					// if empty or out of range
					if ( (strlen(utf8_decode($this->request->post['fax'])) < 3) || (strlen(utf8_decode($this->request->post['fax'])) > 32) ) {
						$json['error']['fax'] = $this->language->get('error_fax');
					}	
				}
				// if not required, not empty but out of range
				elseif ( (strlen(utf8_decode($this->request->post['fax'])) > 0 ) && (strlen(utf8_decode($this->request->post['fax'])) < 3) || (strlen(utf8_decode($this->request->post['fax'])) > 32) ) {

					$json['error']['fax'] = $this->language->get('error_fax');
				}	
			}
			else $this->request->post['fax'] = '';
	

	
	
	// Added checking for company:
			// if company enabled
			if ( $this->config->get('custom_register_member_enable_company') ){
				
				// if required 
				if ( $this->config->get('custom_register_member_company_required') ) {

					// if empty or out of range
					if ( (strlen(utf8_decode($this->request->post['company'])) < 3) || (strlen(utf8_decode($this->request->post['company'])) > 32) ) {
						$json['error']['company'] = $this->language->get('error_company');
					}	
				}
				// if not required, not empty but out of range
				elseif ( (strlen(utf8_decode($this->request->post['company'])) > 0 ) && (strlen(utf8_decode($this->request->post['company'])) < 3) || (strlen(utf8_decode($this->request->post['company'])) > 32) ) {
					$json['error']['company'] = $this->language->get('error_company');
				}	
			}
			else $this->request->post['company'] = '';
	
	
			// CUSTOMER GROUPS, TAX ID AND COMPANY ID HAVE BEEN ADDED WITH Oc 1.5.3
			if ( version_compare(VERSION, '1.5.3', '>=') ) {
			
				// Customer Group
				$this->load->model('account/customer_group');
				
				if (isset($this->request->post['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($this->request->post['customer_group_id'], $this->config->get('config_customer_group_display'))) {
					$customer_group_id = $this->request->post['customer_group_id'];
				} else {
					$customer_group_id = $this->config->get('config_customer_group_id');
				}
				
				$customer_group = $this->model_account_customer_group->getCustomerGroup($customer_group_id);
					
				if ($customer_group) {	
					// Company ID
					if ($customer_group['company_id_display'] && $customer_group['company_id_required'] && empty($this->request->post['company_id'])) {
						$json['error']['company_id'] = $this->language->get('error_company_id');
					}
					
					// Tax ID
					if ($customer_group['tax_id_display'] && $customer_group['tax_id_required'] && empty($this->request->post['tax_id'])) {
						$json['error']['tax_id'] = $this->language->get('error_tax_id');
					}						
				}
			}
			
				
		
	// if address 1 enabled
			if ( $this->config->get('custom_register_member_enable_address_1') ){
				
				// if required 
				if ( $this->config->get('custom_register_member_address_1_required') ) {

					// if empty or out of range
					if ( (strlen(utf8_decode($this->request->post['address_1'])) < 3) || (strlen(utf8_decode($this->request->post['address_1'])) > 128) ) {
						$json['error']['address_1'] = $this->language->get('error_address_1');
					}	
				}
				// if not required, not empty but out of range
				elseif ( (strlen(utf8_decode($this->request->post['address_1'])) > 0 ) && (strlen(utf8_decode($this->request->post['address_1'])) < 3) || (strlen(utf8_decode($this->request->post['address_1'])) > 128) ) {
					$json['error']['address_1'] = $this->language->get('error_address_1');
				}	
			}
			else $this->request->post['address_1'] = '';

			
	// Added checking for address 2:
			// if address 2 enabled
			if ( $this->config->get('custom_register_member_enable_address_2') ){
				
				// if required 
				if ( $this->config->get('custom_register_member_address_2_required') ) {

					// if empty or out of range
					if ( (strlen(utf8_decode($this->request->post['address_2'])) < 3) || (strlen(utf8_decode($this->request->post['address_2'])) > 128) ) {
						$json['error']['address_2'] = $this->language->get('error_address_2');
					}	
				}
				// if not required, not empty but out of range
				elseif ( (strlen(utf8_decode($this->request->post['address_2'])) > 0 ) && (strlen(utf8_decode($this->request->post['address_2'])) < 3) || (strlen(utf8_decode($this->request->post['address_2'])) > 128) ) {
					$json['error']['address_2'] = $this->language->get('error_address_2');
				}	
			}
			else $this->request->post['address_2'] = '';
			
			
			// if city enabled
			if ( $this->config->get('custom_register_member_enable_city') ){
				
				// if required 
				if ( $this->config->get('custom_register_member_city_required') ) {

					// if empty or out of range
					if ( (strlen(utf8_decode($this->request->post['city'])) < 2) || (strlen(utf8_decode($this->request->post['city'])) > 128) ) {
						$json['error']['city'] = $this->language->get('error_city');
					}	
				}
				// if not required, not empty but out of range
				elseif ( (strlen(utf8_decode($this->request->post['city'])) > 0 ) && (strlen(utf8_decode($this->request->post['city'])) < 2) || (strlen(utf8_decode($this->request->post['city'])) > 128) ) {
					$json['error']['city'] = $this->language->get('error_city');
				}	
			}
			else $this->request->post['city'] = '';
			
			
			
			// Added checking for country  
			if ( $this->config->get('custom_register_member_enable_country') ){
				
				if ( $this->config->get('custom_register_member_country_required') ) {
				
					if ( $this->request->post['country_id'] == '' ) {
						$json['error']['country'] = $this->language->get('error_country');
					}	
				}
			}


	
			$this->load->model('localisation/country');

			$country_info = $this->model_localisation_country->getCountry($this->request->post['country_id']);
			
	
	// Added checking for postal code 
			
		
			// if the current country requires the postcode even if it is disabled from admin control panel	
			if ( ($country_info && $country_info['postcode_required'] ) && ( strlen(utf8_decode($this->request->post['postcode'])) < 2 || strlen(utf8_decode($this->request->post['postcode'])) > 10)  ) {
				$json['error']['postcode'] = $this->language->get('error_postcode');
			}

						
			if ( $this->config->get('custom_register_member_enable_postcode') ){
				
				if ( $this->config->get('custom_register_member_postcode_required') ) {
				
					// if empty or out of range:
					if ( strlen(utf8_decode($this->request->post['postcode'])) < 2 || strlen(utf8_decode($this->request->post['postcode'])) > 10) {
						$json['error']['postcode'] = $this->language->get('error_postcode');
					}
				}
				// if not required, not empty but out of range
				elseif ( strlen(utf8_decode($this->request->post['postcode'])) > 0  && ( strlen(utf8_decode($this->request->post['postcode'])) < 2 || strlen(utf8_decode($this->request->post['postcode'])) > 10)  ) {
					$json['error']['postcode'] = $this->language->get('error_postcode');
				}		
			}

			else {
				if ( !isset($this->request->post['postcode']) ) $this->request->post['postcode'] = '';
			}
			
			
		// VAT VALIDATION IS AVAILABLE FROM Oc 1.5.3
		if ( version_compare(VERSION, '1.5.3', '>=') ) {	
		
		// VAT Validation
			if ($country_info) {
				
				// VAT Validation
				$this->load->helper('vat');
				
				if ($this->config->get('config_vat') && $this->request->post['tax_id'] && (vat_validation($country_info['iso_code_2'], $this->request->post['tax_id']) == 'invalid')) {
					$json['error']['tax_id'] = $this->language->get('error_vat');
				}					
			}
		}
	

			// if zone enabled
			if ( $this->config->get('custom_register_member_enable_zone') ){
				
				// if required 
				if ( $this->config->get('custom_register_member_zone_required') ) {
				
					if ($this->request->post['zone_id'] == '') {
						$json['error']['zone'] = $this->language->get('error_zone');
					}
				}
			}


	
			if ((strlen(utf8_decode($this->request->post['password'])) < 4) || (strlen(utf8_decode($this->request->post['password'])) > 20)) {
				$json['error']['password'] = $this->language->get('error_password');
			}
	

			// if password confirm enabled
			if ( $this->config->get('custom_register_member_enable_confirm') ){
		
				// if wrong password or confirm pwd is empty:
				if (isset($json['error']['password']) || $this->request->post['confirm'] == '') {
					$json['error']['confirm'] = $this->language->get('error_confirm');
				}
				// pwd is right but doesn't match confirm pwd:
				elseif ($this->request->post['confirm'] != $this->request->post['password']) {
				//	$json['error']['confirm'] = $this->language->get('error_confirm_1');
					$json['error']['confirm'] = $this->language->get('error_confirm');
				}
			}
		
			

			if ($this->config->get('config_account_id')) {
				$this->load->model('catalog/information');

				
				$information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));

				
				if ($information_info && !isset($this->request->post['agree'])) {
					$json['error']['warning'] = sprintf($this->language->get('error_agree'), $information_info['title']);
				}
			}
			
		}
		
		if (!$json) {
			$this->model_account_customer->addCustomer($this->request->post);
			
			$this->session->data['account'] = 'register';
			

			// CUSTOMER GROUPS, TAX ID AND COMPANY ID HAVE BEEN ADDED WITH Oc 1.5.3
			if ( version_compare(VERSION, '1.5.3', '>=') ) {
			
				if ($customer_group && !$customer_group['approval']) {
					$this->customer->login($this->request->post['email'], $this->request->post['password']);
					
					$this->session->data['payment_address_id'] = $this->customer->getAddressId();

					$this->session->data['payment_country_id'] = $this->request->post['country_id'];
					$this->session->data['payment_zone_id'] = $this->request->post['zone_id'];
										
					if (!empty($this->request->post['shipping_address'])) {
						$this->session->data['shipping_address_id'] = $this->customer->getAddressId();
						$this->session->data['shipping_country_id'] = $this->request->post['country_id'];
						$this->session->data['shipping_zone_id'] = $this->request->post['zone_id'];
						$this->session->data['shipping_postcode'] = $this->request->post['postcode'];					
					}
				} else {
					$json['redirect'] = $this->url->link('account/success');
				}
			} // END if for Oc >= 1.5.3
			
			// for Oc < 1.5.3
			else {
			
				if (!$this->config->get('config_customer_approval')) {
					$this->customer->login($this->request->post['email'], $this->request->post['password']);
					

	
					$this->session->data['payment_address_id'] = $this->customer->getAddressId();

					if (!empty($this->request->post['shipping_address'])) {
						$this->session->data['shipping_address_id'] = $this->customer->getAddressId();

					}
				} else {
					$json['redirect'] = $this->url->link('account/success');
				}
			
			} // END else for Oc < 1.5.3
			
			unset($this->session->data['guest']);
			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);	
			unset($this->session->data['payment_methods']);
		}	
		
		$this->response->setOutput(json_encode($json));	

	} 
		
}
?>