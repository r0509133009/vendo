<?php
class ControllerCheckoutSuccessBankTransfer extends Controller { 


	public function index($order_info = array())
	{

		if(!$order_info) return false;

			$this->data['text_head_greeting_h'] = $this->language->get('text_head_greeting_h');
			$this->data['text_head_payment_type'] = sprintf($this->language->get('text_head_payment_type'),$order_info['payment_method']);
			$this->data['text_head_payment_bank_transfer_description'] = $this->language->get('text_head_payment_bank_transfer_description');
			$this->data['text_content_payment_bank_transfer_total_details'] = sprintf($this->language->get('text_content_payment_bank_transfer_total_details'),$this->currency->format($order_info['total'],$order_info['currency_code'],$order_info['currency_value']));

			$this->data['text_content_payment_bank_transfer_to'] = $this->language->get('text_content_payment_bank_transfer_to');

			$this->data['text_content_payment_bank_transfer_order_id'] = sprintf($this->language->get('text_content_payment_bank_transfer_order_id'),$order_info['order_id']);
			
			if ($this->customer->isLogged()) {
				$this->data['text_content_bottom_account_reward'] = sprintf($this->language->get('text_content_bottom_account_reward'),
					sprintf($this->language->get('text_history'),$this->url->link('account/order', '', 'SSL')),
					sprintf($this->language->get('text_reward'),$this->url->link('account/reward', '', 'SSL'))
					);
			}
			
			$this->data['text_content_bottom_text'] = $this->language->get('text_content_bottom_text');
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/custom_success_bank_transfer.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/common/custom_success_bank_transfer.tpl';
			} else {
				$this->template = 'default/template/common/custom_success_bank_transfer.tpl';
			}

			return $this->render();
	}

}