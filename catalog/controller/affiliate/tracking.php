<?php 
class ControllerAffiliateTracking extends Controller { 
	public function index() {
		if (!$this->affiliate->isLogged()) {
	  		$this->session->data['redirect'] = $this->url->link('affiliate/tracking', '', 'SSL');
	  
	  		$this->redirect($this->url->link('affiliate/login', '', 'SSL'));
    	} 
	
		$this->language->load('affiliate/tracking');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->data['text_banners'] = $this->language->get('text_banners');
		$this->data['text_code_bn_rand'] = $this->language->get('text_code_bn_rand');
		$this->data['text_image'] = $this->language->get('text_image');
		$this->data['text_size'] = $this->language->get('text_size');
		$this->data['text_date'] = $this->language->get('text_date');
		$this->data['text_code'] = $this->language->get('text_code');
		$this->data['text_detail'] = $this->language->get('text_detail');
		$this->data['text_no_data'] = $this->language->get('text_no_data');
		$this->data['text_choose_size'] = $this->language->get('text_choose_size');
		$this->data['text_choose_size_bn'] = $this->language->get('text_choose_size_bn');
		$this->data['text_get_code'] = $this->language->get('text_get_code');
		$this->data['text_bn_rand'] = $this->language->get('text_bn_rand');
		$this->data['text_url_product'] = $this->language->get('text_url_product');

      	$this->data['breadcrumbs'] = array();

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),
        	'separator' => false
      	); 

      	$this->data['breadcrumbs'][] = array(       	
        	'text'      => $this->language->get('text_account'),
			'href'      => $this->url->link('affiliate/account', '', 'SSL'),
        	'separator' => $this->language->get('text_separator')
      	);

      	$this->data['breadcrumbs'][] = array(       	
        	'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('affiliate/tracking', '', 'SSL'),
        	'separator' => $this->language->get('text_separator')
      	);
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['limit'])) {
			$limit = $this->request->get['limit'];
		} else {
			$limit = 10;
		}
		
		
		//
		if (isset($this->request->get['bn_size'])) {
			$bn_size = $this->request->get['bn_size'];
		} else {
			$bn_size = null;
		}
		if (isset($this->request->get['bn_name'])) {
			$bn_name = $this->request->get['bn_name'];
		} else {
			$bn_name = null;
		}
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'bn_size';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';
		//sort
		if (isset($this->request->get['bn_size'])) {
			$url .= '&bn_size=' . $this->request->get['bn_size'];
		}
		if (isset($this->request->get['bn_name'])) {
			$url .= '&bn_name=' . $this->request->get['bn_name'];
		}
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$this->data['banners'] = array();
		$filter_data = array(
			'bn_size'   		=> $bn_size,
			'bn_name'      		=> $bn_name,
			'sort'              => $sort,
			'order'             => $order,
			'start'              => ($page - 1) * $limit,
			'limit'              => $limit
		);
		
		
		//Size banner
		$this->load->model('affiliate/affiliate');
		$this->data['sizes'] = $this->model_affiliate_affiliate->getSizes();
		
		$this->load->model('affiliate/affiliate');
		$banner_total = $this->model_affiliate_affiliate->getTotalBanners($filter_data);
		$results = $this->model_affiliate_affiliate->getBanners($filter_data);
		
		foreach ($results as $result) {
			$this->data['banners'][] = array(
				'bn_id' 		=> $result['bn_id'],
				'bn_name'		=> $result['bn_name'],
				'bn_size'		=> $result['bn_size'],
				'bn_img'		=> $result['bn_img'],
				'bn_des'		=> $result['bn_des'],
				'bn_camid'		=> $result['bn_camid'],
				'bn_create'   	=> date($this->language->get('date_format_short'), strtotime($result['bn_create'])),
			);
		}
		
		$pagination = new Pagination();
		$pagination->total = $banner_total;
		$pagination->page = $page;
		$pagination->limit = $limit;
		$pagination->url = $this->url->link('affiliate/tracking&page={page}');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($banner_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($banner_total - $limit)) ? $banner_total : ((($page - 1) * $limit) + $limit), $banner_total, ceil($banner_total / $limit));
		
		$data['bn_name'] 	= $bn_name;
		$data['bn_size'] 	= $bn_size;

		$data['sort'] = $sort;
		$data['order'] = $order;
		$data['limit'] = $limit;
		
    	$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_description'] = sprintf($this->language->get('text_description'), $this->config->get('config_name'));
		$this->data['text_code'] = $this->language->get('text_code');
		$this->data['text_generator'] = $this->language->get('text_generator');
		$this->data['text_link'] = $this->language->get('text_link');
		
		$this->data['button_continue'] = $this->language->get('button_continue');

    	$this->data['code'] = $this->affiliate->getCode();
		
		$this->data['continue'] = $this->url->link('affiliate/account', '', 'SSL');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/affiliate/tracking.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/affiliate/tracking.tpl';
		} else {
			$this->template = 'default/template/affiliate/tracking.tpl';
		}
		// echo json_encode($this->data['banners']);
		// exit();
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'	
		);
				
		$this->response->setOutput($this->render());		
  	}
	
	public function autocomplete() {
		$json = array();
		
		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/product');
			 
			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);
			
			$results = $this->model_catalog_product->getProducts($data);
			
			foreach ($results as $result) {
				$json[] = array(
					'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
					'link' => str_replace('&amp;', '&', $this->url->link('product/product', 'product_id=' . $result['product_id'] . '&tracking=' . $this->affiliate->getCode()))			
				);	
			}
		}

		$this->response->setOutput(json_encode($json));
	}
}
?>