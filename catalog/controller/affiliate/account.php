<?php 
class ControllerAffiliateAccount extends Controller { 
	public function index() {
		if (!$this->affiliate->isLogged()) {
	  		$this->session->data['redirect'] = $this->url->link('affiliate/account', '', 'SSL');
	  
	  		$this->redirect($this->url->link('affiliate/login', '', 'SSL'));
    	} 
	
		$this->language->load('affiliate/account');

      	$this->data['breadcrumbs'] = array();

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),
        	'separator' => false
      	); 

      	$this->data['breadcrumbs'][] = array(       	
        	'text'      => $this->language->get('text_account'),
			'href'      => $this->url->link('affiliate/account', '', 'SSL'),
        	'separator' => $this->language->get('text_separator')
      	);
		
		
		$this->document->setTitle($this->language->get('heading_title'));

    	$this->data['heading_title'] = $this->language->get('heading_title');

    	$this->data['text_my_account'] = $this->language->get('text_my_account');
    	$this->data['text_my_tracking'] = $this->language->get('text_my_tracking');
		$this->data['text_my_transactions'] = $this->language->get('text_my_transactions');
		$this->data['text_edit'] = $this->language->get('text_edit');
		$this->data['text_password'] = $this->language->get('text_password');
		$this->data['text_payment'] = $this->language->get('text_payment');
		$this->data['text_tracking'] = $this->language->get('text_tracking');
		$this->data['text_transaction'] = $this->language->get('text_transaction');
		
		$this->data['text_clicks_last10'] = $this->language->get('text_clicks_last10');
		$this->data['text_no_data'] = $this->language->get('text_no_data');
		$this->data['text_event'] = $this->language->get('text_event');
		$this->data['text_balance'] = $this->language->get('text_balance');
		$this->data['text_stats_day_month'] = $this->language->get('text_stats_day_month');
		$this->data['text_date'] = $this->language->get('text_date');
		$this->data['text_impression'] = $this->language->get('text_impression');
		$this->data['text_click'] = $this->language->get('text_click');
		$this->data['text_sale'] = $this->language->get('text_sale');
		$this->data['text_amount'] = $this->language->get('text_amount');
		$this->data['text_ctr'] = $this->language->get('text_ctr');
		$this->data['text_cr'] = $this->language->get('text_cr');
		$this->data['text_total_month'] = $this->language->get('text_total_month');
		
		if (isset($this->session->data['success'])) {
    		$this->data['success'] = $this->session->data['success'];
			
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		$this->data['code'] = $this->affiliate->getCode();
		$this->load->model('affiliate/affiliate');
		$referid = $this->model_affiliate_affiliate->getAffiliateByCode($this->data['code']);
		$totaltrans = $this->model_affiliate_affiliate->getTotalTransaction($referid['affiliate_id']);
		if(isset($_GET['month'])){
			$this->data['smonth'] = $_GET['month'];
		} else {
			$this->data['smonth'] = date('m/Y');
		}
		
		
		$stats_resut = $this->model_affiliate_affiliate->getStats($referid['code'], $this->data['smonth']);
		foreach ($stats_resut as $stats) {
			$this->data['stats'][] = array(
				'stats_id' 		=> $stats['stats_id'],
				'stats_views'	=> $stats['stats_views'],
				'stats_click'	=> $stats['stats_click'],
				'stats_date'   	=> date($this->language->get('date_format_short'), strtotime($stats['stats_date'])),
				'stats_sale'	=> $this->model_affiliate_affiliate->getTotalSales($referid['affiliate_id'], date('Y-m-d', strtotime($stats['stats_date']))),
				'sale_amount'	=> $this->model_affiliate_affiliate->getTotalSalesAmount($referid['affiliate_id'], date('Y-m-d', strtotime($stats['stats_date']))),
			);
		}
		
		$ChartStats = $this->model_affiliate_affiliate->getChartStats($referid['code']);
		
		$category = array();
		$category['name'] = 'Date';

		$seriesStats1 = array();
		$seriesStats1['name'] = 'Click';
		
		foreach ($ChartStats as $ChartStat) {
			$aa[] = str_replace(date('/Y'), '', $ChartStat['stats_date']);
			$bb[] = $ChartStat['stats_click'];
		}
		if(isset($aa) && isset($bb)){
		$category['data'] = @array_reverse(@$aa);
		$seriesStats1['data'] = @array_reverse(@$bb);
		$this->data['category'] = json_encode($category['data'], JSON_NUMERIC_CHECK);
		$this->data['series'] = json_encode($seriesStats1['data'], JSON_NUMERIC_CHECK);
		}
		
		$sales = $this->model_affiliate_affiliate->getLastSales($referid['affiliate_id']);
		foreach ($sales as $sale) {
			$this->data['last_sales'][] = array(
				'affiliate_transaction_id' 		=> $sale['affiliate_transaction_id'],
				'order_id'		=> $sale['order_id'],
				'description'	=> $sale['description'],
				'amount'		=> $sale['amount'],
				'date_added'   	=> date($this->language->get('date_format_short'), strtotime($sale['date_added'])),
			);
		}
		
		$this->data['totalamount'] = $this->model_affiliate_affiliate->getTransactionTotal($referid['affiliate_id']);
		$transactions = $this->model_affiliate_affiliate->getTransactions($referid['affiliate_id']);
		foreach ($transactions as $transaction) {
			$this->data['transactions'][] = array(
				'affiliate_transaction_id' 		=> $transaction['affiliate_transaction_id'],
				'order_id'		=> $transaction['order_id'],
				'description'	=> $transaction['description'],
				'amount'		=> $transaction['amount'],
				'date_added'   	=> date($this->language->get('date_format_short'), strtotime($transaction['date_added'])),
			);
		}
		
		$group_monthss = $this->model_affiliate_affiliate->getStatsMonth($this->data['code']);
		foreach ($group_monthss as $group_month) {
			$this->data['group_months'][] = array(
				'stats_month' => $group_month['stats_month'],
			);
		}

    	$this->data['edit'] = $this->url->link('affiliate/edit', '', 'SSL');
		$this->data['password'] = $this->url->link('affiliate/password', '', 'SSL');
		$this->data['payment'] = $this->url->link('affiliate/payment', '', 'SSL');
		$this->data['tracking'] = $this->url->link('affiliate/tracking', '', 'SSL');
    	$this->data['transaction'] = $this->url->link('affiliate/transaction', '', 'SSL');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/affiliate/account.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/affiliate/account.tpl';
		} else {
			$this->template = 'default/template/affiliate/account.tpl';
		}
		
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'	
		);
				
		$this->response->setOutput($this->render());		
  	}
}
?>