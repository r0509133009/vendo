<?php
/* This module is copywrite to ozxmod
 * Author: ozxmod(ozxmod@gmail.com)
* It is illegal to remove this comment without prior notice to ozxmod(ozxmod@gmail.com)
*/
class ControllerAccountAjaxLoginRegister extends Controller {
	private $error;
	public function validateAjaxLogin(){
	
		$this->language->load('account/ajax_login_register');
		$json = array();
		
		if(!preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['ajax_email'])){
			$json["error"] = $this->language->get('error_email');
		}else if (!$this->customer->login($this->request->post['ajax_email'], $this->request->post['ajax_password'])) {
			$json['error'] = $this->language->get('error_login');
		}else{
			$json['success'] = $this->language->get('text_success');
		}
		if ($this->customer->isLogged()) {
			$json['redirect']=$this->url->link('account/account', '', 'SSL');
		}
		
		return	$this->response->setOutput(json_encode($json));
	}
	
	public function ajaxregister(){
		
		$this->language->load('account/ajax_login_register');
		
		$this->load->model('account/customer');
		
		$json = array();
		$data = array();
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			$email = $this->request->post["ajax_register_email"];
			$password = $this->request->post["ajax_register_password"];
			$re_password = $this->request->post["re_ajax_register_password"];
			if(!empty($email) && !empty($password) && !empty($re_password)){
				if(!preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $email)){
					$json["error"] = $this->language->get('error_email');
				} else if($password != $re_password){
					$json["error"] = $this->language->get("error_password_match");
				} else if ($this->model_account_customer->getTotalCustomersByEmail($this->request->post['ajax_register_email'])) {
					$json["error"] = $this->language->get('error_exists');
				}else {
					
					$name_arr = explode('@',$email);
					
					$config_customer_approval = $this->config->get('config_customer_approval');
					$this->config->set('config_customer_approval',0);
					
					$this->request->post['email'] = $email;
						
					$add_data=array();
					$add_data['email'] = $email;
					$add_data['password'] = $password;
					$add_data['firstname'] = $name_arr[0];
					$add_data['lastname'] = '';
					$add_data['fax'] = '';
					$add_data['telephone'] = '';
					$add_data['company'] = '';
					$add_data['company_id'] = '';
					$add_data['tax_id'] = '';
					$add_data['address_1'] = '';
					$add_data['address_2'] = '';
					$add_data['city'] = '';
					$add_data['city_id'] = '';
					$add_data['postcode'] = '';
					$add_data['country_id'] = 0;
					$add_data['zone_id'] = 0;
					
					$this->model_account_customer->addCustomer($add_data);
					$this->config->set('config_customer_approval',$config_customer_approval);
					
					if($this->customer->login($email, $password)){
						// Delete address
						$this->deleteAddress();
							
						unset($this->session->data['guest']);
						$json['success'] = "Success";
						$json['redirect'] = $this->url->link('account/success');
					}
				}
			}else{
				$json["error"] = $this->language->get('error_all_fields');
			}
		} else{
			$json["error"] = $this->language->get('error_hack');
		}
		$this->response->setOutput(json_encode($json));
	}
	
	public function sendForgotPassword(){
		$json = array();
		
		$this->language->load('account/ajax_login_register');
		
		if($this->validate()) {
		$this->language->load('mail/forgotten');
		
		$password = substr(sha1(uniqid(mt_rand(), true)), 0, 10);
		
		$this->model_account_customer->editPassword($this->request->post['ajax_forgot_email'], $password);
		
		$subject = sprintf($this->language->get('text_subject'), $this->config->get('config_name'));
		
		$message  = sprintf($this->language->get('text_greeting'), $this->config->get('config_name')) . "\n\n";
		$message .= $this->language->get('text_password') . "\n\n";
		$message .= $password;
		
		$mail = new Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->hostname = $this->config->get('config_smtp_host');
		$mail->username = $this->config->get('config_smtp_username');
		$mail->password = $this->config->get('config_smtp_password');
		$mail->port = $this->config->get('config_smtp_port');
		$mail->timeout = $this->config->get('config_smtp_timeout');
		$mail->setTo($this->request->post['ajax_forgot_email']);
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender($this->config->get('config_name'));
		$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
		$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
		$mail->send();
		
		$json['success'] = sprintf($this->language->get('text_email_sent'), $this->request->post['ajax_forgot_email']);
		
		} else {
			$json["error"] = $this->language->get('error_no_accounts');
		}
		
		$this->response->setOutput(json_encode($json));
	}
	
	protected function validate() {
		$this->load->model("account/customer");
		
		if (!isset($this->request->post['ajax_forgot_email']) || empty($this->request->post['ajax_forgot_email'])) {
			$this->error['warning'] = $this->language->get('error_ajax_forgot_email');
		} elseif (!$this->model_account_customer->getTotalCustomersByEmail($this->request->post['ajax_forgot_email'])) {
			$this->error['warning'] = $this->language->get('error_ajax_forgot_email');
		}
	
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	
	public function fblogin() {
		$a = "";
		$loc = $this->url->link("account/account", "", 'SSL');
		
		if(isset($this->session->data['ajaxfblogin_from'])) {
			$a= $this->session->data['ajaxfblogin_from'];
			unset($this->session->data['ajaxfblogin_from']);
		}
		
		if(isset($this->session->data['ajaxfblogin_loc'])) {
			$loc = urldecode($this->session->data['ajaxfblogin_loc']);
			unset($this->session->data['ajaxfblogin_loc']);
			
			$loc_arr = explode("route=", $loc);
			
			if(isset($loc_arr[1]) && $loc_arr[1] == "account/logout")
				$loc = $this->url->link("account/account", '', "SSL");
		}
		
		if ($this->customer->isLogged())	
			$this->redirect($loc);
		 
		if(!isset($this->myfbconnect)){
						
			require_once(DIR_SYSTEM . 'social-login/facebook-sdk/facebook.php');
	
			$this->myfbconnect = new Facebook(array(
					'appId'  => $this->config->get('ajaxfbgoogle_apikey'),
					'secret' => $this->config->get('ajaxfbgoogle_apisecret'),
			));
		}
	
		$_SERVER_CLEANED = $_SERVER;
		$_SERVER = $this->clean_decode($_SERVER);
	
		$fbuser = $this->myfbconnect->getUser();
		$fbuser_profile = null;
		if ($fbuser){
			try {
				$fbuser_profile = $this->myfbconnect->api("/me?fields=id,first_name,last_name,email");
			} catch (FacebookApiException $e) {
				error_log($e);
				$fbuser = null;
			}
		}
	
		$_SERVER = $_SERVER_CLEANED;
	
		if($fbuser_profile['id'] && $fbuser_profile['email']){
			$this->load->model('account/customer');
	
			$email = $fbuser_profile['email'];
			$password = $this->get_password($fbuser_profile['id']);
			if($this->customer->login($email, $password, true)){
				if($a=='checkout') {
					$this->redirect($this->url->link('checkout/checkout', '', 'SSL'));
				} else {
					$this->redirect($loc);
				}
			}
	
			$email_query = $this->db->query("SELECT `email` FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(strtolower($email)) . "'");
			if($email_query->num_rows){
				//$this->model_account_customer->editPassword($email, $password);
				if($this->customer->login($email, $password, true)){
					if($a=='checkout') {
						$this->redirect($this->url->link('checkout/checkout', '', 'SSL'));
					} else{
						$this->redirect($loc);
					}
				}
			}
			else{
	
				$config_customer_approval = $this->config->get('config_customer_approval');
				$this->config->set('config_customer_approval',0);
	
				$this->request->post['email'] = $email;
					
				$add_data=array();
				$add_data['email'] = $fbuser_profile['email'];
				$add_data['password'] = $password;
				$add_data['firstname'] = isset($fbuser_profile['first_name']) ? $fbuser_profile['first_name'] : '';
				$add_data['lastname'] = isset($fbuser_profile['last_name']) ? $fbuser_profile['last_name'] : '';
				$add_data['fax'] = '';
				$add_data['telephone'] = '';
				$add_data['company'] = '';
				$add_data['company_id'] = '';
				$add_data['tax_id'] = '';
				$add_data['address_1'] = '';
				$add_data['address_2'] = '';
				$add_data['city'] = '';
				$add_data['city_id'] = '';
				$add_data['postcode'] = '';
				$add_data['country_id'] = 0;
				$add_data['zone_id'] = 0;
$add_data['newsletter'] = '0';
	
				$this->model_account_customer->addCustomer($add_data);
				$this->config->set('config_customer_approval',$config_customer_approval);
	
				if($this->customer->login($email, $password, true)){
					
					// Delete address
					$this->deleteAddress();
					
					unset($this->session->data['guest']);
					if($a=='checkout')
					{
						$this->redirect($this->url->link('checkout/checkout'));
					}
					else{
						$this->redirect($loc);
					}
				}
			}
	
		}
		$this->redirect($loc);
		
	}
	
	// Google Login Code
	
	public function glogin() {
		
		require_once DIR_SYSTEM.'social-login/google/src/apiClient.php';
		require_once DIR_SYSTEM.'social-login/google/src/contrib/apiOauth2Service.php';
		
		$client = new apiClient();
		$client->setApplicationName("Google+ PHP Starter Application");
		// Visit https://code.google.com/apis/console to generate your
		// oauth2_client_id, oauth2_client_secret, and to register your oauth2_redirect_uri.
		$client->setClientId($this->config->get('ajaxfbgoogle_googleapikey'));
		$client->setClientSecret($this->config->get('ajaxfbgoogle_googleapisecret'));
		$client->setRedirectUri($this->url->link('account/ajax_login_register/glogin', '', 'SSL'));
		$client->setScopes(array('https://www.googleapis.com/auth/userinfo.email','https://www.googleapis.com/auth/userinfo.profile'));
		$client->setDeveloperKey('');
		$plus = new apiOauth2Service($client);
		
		if (isset($_REQUEST['logout'])) {
			unset($_SESSION['access_token']);
		}
		
		if (isset($_GET['code'])) {
			$client->authenticate($_GET['code']);
			$access_token = $client->getAccessToken();
			$client->setAccessToken($access_token);
			//header('Location: http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
		}
		
		if ($client->getAccessToken()) {
			$userinfo = $plus->userinfo;
			$data = $userinfo->get();
		 
			$a = "";
			$loc = $this->url->link("account/account", "", 'SSL');
			
			
			if(isset($this->session->data['ajaxfblogin_from'])) {
				$a= $this->session->data['ajaxfblogin_from'];
				unset($this->session->data['ajaxfblogin_from']);
			}
			
			if(isset($this->session->data['ajaxfblogin_loc'])) {
				$loc = urldecode($this->session->data['ajaxfblogin_loc']);
				unset($this->session->data['ajaxfblogin_loc']);
				
				$loc_arr = explode("route=", $loc);
					
				if(isset($loc_arr[1]) && $loc_arr[1] == "account/logout")
					$loc = $this->url->link("account/account", '', "SSL");
				
			}
			
			$this->load->model('account/customer');

			// Checking email id if already registered
			$email = $data["email"];
			$password = $this->get_password($email);

			if($this->customer->login($email, $password, true)){
				if($a=='checkout') {
					$this->redirect($this->url->link('checkout/checkout', '', 'SSL'));
				} else {
					$this->redirect($loc);
				}
			}

			$email_query = $this->db->query("SELECT `email` FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(strtolower($email)) . "'");

			if($email_query->num_rows){
				//$this->model_account_customer->editPassword($email, $password);
				if($this->customer->login($email, $password, true)){
					if($a=='checkout'){
						$this->redirect($this->url->link('checkout/checkout', '', 'SSL'));
					} else {
						$this->redirect($loc);
					}
				}
			} else {
				$name = $data['name'];
				$name_split = explode(" ", $name);
				
				$f_name = $name_split[0];
				$l_name = '';
				if(isset($name_split[1]))
					$l_name = $name_split[1];
				
				if(isset($name_split[2]))
					$l_name .= $name_split[2];
					
				$config_customer_approval = $this->config->get('config_customer_approval');
				$this->config->set('config_customer_approval',0);
					
				$this->request->post['email'] = $email;

				$add_data=array();
				$add_data['email'] = $email;
				$add_data['password'] = $password;
				$add_data['firstname'] = $f_name;
				$add_data['lastname'] = $l_name;
				$add_data['fax'] = '';
				$add_data['telephone'] = '';
				$add_data['company'] = '';
				$add_data['company_id'] = '';
				$add_data['tax_id'] = '';
				$add_data['address_1'] = '';
				$add_data['address_2'] = '';
				$add_data['city'] = '';
				$add_data['city_id'] = '';
				$add_data['postcode'] = '';
				$add_data['country_id'] = 0;
				$add_data['zone_id'] = 0;
					
				$this->model_account_customer->addCustomer($add_data);
				$this->config->set('config_customer_approval',$config_customer_approval);
				
					
				if($this->customer->login($email, $password, true)){
					
					// Delete address
					$this->deleteAddress();
					
					
					unset($this->session->data['guest']);
					if($a=='checkout')
					{
						$this->redirect($this->url->link('checkout/checkout'));
					}
					else{
						$this->redirect($loc);
					}
				}
			}
		}else{
			$this->redirect($this->url->link('common/home'));		
		}
	}
	
	// End Google Login Code
	
	
	// Twitter Login Code
	
	public function twit() {
		require_once DIR_SYSTEM.'social-login/twitter/twitteroauth.php';
		
		$twitteroauth = new TwitterOAuth($this->config->get('ajaxfbgoogle_twitterapikey'), $this->config->get('ajaxfbgoogle_twitterapisecret'));
		
		// Requesting authentication tokens, the parameter is the URL we will be redirected to
		$request_token = $twitteroauth->getRequestToken($this->url->link('account/ajax_login_register/twitter', '', 'SSL'));
	
		// Saving them into the session
	
		$_SESSION['oauth_token'] = $request_token['oauth_token'];
		$_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];
	
		// If everything goes well..
		if ($twitteroauth->http_code == 200) {
			// Let's generate the URL and redirect
				
			$url = $twitteroauth->getAuthorizeURL($request_token['oauth_token']);
				
			header('Location: ' . $url);
		} else {
			// It's a bad idea to kill the script, but we've got to know when there's an error.
			die('Something wrong happened.');
		}
	}
	
	public function twitter() {
		require_once DIR_SYSTEM.'social-login/twitter/twitteroauth.php';
	
		$this->language->load('account/ajax_login_register');
	
		if (!empty($_GET['oauth_verifier']) && !empty($_SESSION['oauth_token']) && !empty($_SESSION['oauth_token_secret'])) {
			
			// We've got everything we need
			//$twitteroauth = new TwitterOAuth(YOUR_CONSUMER_KEY, YOUR_CONSUMER_SECRET, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
				
			$twitteroauth = new TwitterOAuth($this->config->get('ajaxfbgoogle_twitterapikey'), $this->config->get('ajaxfbgoogle_twitterapisecret'), $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
				
			// Let's request the access token
	
			$access_token = $twitteroauth->getAccessToken($_GET['oauth_verifier']);
			// Save it in a session var
			$_SESSION['access_token'] = $access_token;
			// Let's get the user's info
			$user_info = $twitteroauth->get('account/verify_credentials');
			// Print user's info
				
			if (isset($user_info->error)) {
				// Something's wrong, go back to square 1
				//header('Location: login-twitter.php');
			} else {
				$twit_id = $user_info->id;
				$name = $user_info->name;
	
				$name_arr = explode(" ", $name);
				$f_name = array_shift($name_arr);
	
				$l_name = implode(" ", $name_arr);
	
				$a = "";
				$loc = $this->url->link("account/account", "", 'SSL');
					
					
				if(isset($this->session->data['ajaxfblogin_from'])) {
					$a= $this->session->data['ajaxfblogin_from'];
					unset($this->session->data['ajaxfblogin_from']);
				}
					
				if(isset($this->session->data['ajaxfblogin_loc'])) {
					$loc = urldecode($this->session->data['ajaxfblogin_loc']);
					unset($this->session->data['ajaxfblogin_loc']);
	
					$loc_arr = explode("route=", $loc);
	
					if(isset($loc_arr[1]) && $loc_arr[1] == "account/logout")
						$loc = $this->url->link("account/account", '', "SSL");
	
				}
					
				$this->load->model('account/customer');
	
				// Checking email id if already registered
				$email = $this->session->data['ozxmod_twit_email'];
				unset($this->session->data['ozxmod_twit_email']);
	
				$password = $this->get_twitpassword($twit_id);
	
				if($this->customer->login($email, $password)){
					if($a=='checkout') {
						$this->response->redirect($this->url->link('checkout/checkout', '', 'SSL'));
					} else {
						$this->response->redirect($loc);
					}
				}
	
				$email_query = $this->db->query("SELECT `email` FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(strtolower($email)) . "'");
				//print_r($email_query->rows); die("hey");
				if($email_query->num_rows){
					//$this->model_account_customer->editPassword($email, $password);
						
					if($this->customer->login($email, $password)){
						if($a=='checkout'){
							$this->response->redirect($this->url->link('checkout/checkout', '', 'SSL'));
						} else {
							$this->response->redirect($loc);
						}
					} else {
							
						$this->session->data['ozxmod_twit_error'] = $this->language->get('error_twitter');
	
						$this->response->redirect($loc);
					}
						
				} else {
						
					//die("out");
	
					$config_customer_approval = $this->config->get('config_customer_approval');
					$this->config->set('config_customer_approval',0);
	
					$this->request->post['email'] = $email;
	
					$add_data=array();
					$add_data['email'] = $email;
					$add_data['password'] = $password;
					$add_data['firstname'] = $f_name;
					$add_data['lastname'] = $l_name;
					$add_data['fax'] = '';
					$add_data['telephone'] = '';
					$add_data['company'] = '';
					$add_data['company_id'] = '';
					$add_data['tax_id'] = '';
					$add_data['address_1'] = '';
					$add_data['address_2'] = '';
					$add_data['city'] = '';
					$add_data['city_id'] = '';
					$add_data['postcode'] = '';
					$add_data['country_id'] = 0;
					$add_data['zone_id'] = 0;
	
					$this->model_account_customer->addCustomer($add_data);
					$this->config->set('config_customer_approval',$config_customer_approval);
	
	
					if($this->customer->login($email, $password)){
							
						// Delete address
						$this->deleteAddress();
							
							
						unset($this->session->data['guest']);
						if($a=='checkout')
						{
							$this->response->redirect($this->url->link('checkout/checkout', '', 'SSL'));
						}
						else{
							$this->response->redirect($loc);
						}
					}
				}
	
			}
		} else {
			// Something's missing, go back to square 1
			$this->response->redirect($this->url->link('common/home', '', 'SSL'));
			//header('Location: login-twitter.php');
		}
	}
	
	// End Twitter Login Code
	
	// LinkedIn Login
	
	public function linkedinlogin() {
	
		require_once DIR_SYSTEM.'social-login/linkedin/http.php';
		require_once DIR_SYSTEM.'social-login/linkedin/oauth_client.php';
	
		$client = new oauth_client_class;
		$client->debug = false;
		$client->debug_http = true;
		$client->server = 'LinkedIn';
		$client->redirect_uri = $this->url->link('account/ajax_login_register/linkedinlogin', '', 'SSL');
	
		/*
		 * Uncomment the next line if you want to use
		* the pin based authorization flow
		*/
		// $client->redirect_uri = 'oob';
	
		/*
		 * Was this script included defining the pin the
		* user entered to authorize the API access?
		*/
		if(defined('OAUTH_PIN'))
			$client->pin = OAUTH_PIN;
	
		$client->client_id = $this->config->get('ajaxfbgoogle_linkedinapikey'); $application_line = __LINE__;
		$client->client_secret = $this->config->get('ajaxfbgoogle_linkedinapisecret');
	
		/*  API permission scopes
		 *  Separate scopes with a space, not with +
		*/
		$client->scope = 'r_fullprofile r_emailaddress';
	
		if(($success = $client->Initialize()))
		{
			
			if(($success = $client->Process()))
			{
	
				if(strlen($client->access_token))
				{
						
					$success = $client->CallAPI(
							'https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address)',
							'GET', array(
									'format'=>'json'
							), array('FailOnAccessError'=>true), $user);
	
				}
			}
					$success = $client->Finalize($success);

		}
			if($client->exit)
				exit;
				
			if(strlen($client->authorization_error))
			{
				$client->error = $client->authorization_error;
				$success = false;
			}
				
			$a = "";
			$loc = $this->url->link("account/account", "", 'SSL');


			if(isset($this->session->data['ajaxfblogin_from'])) {
				$a= $this->session->data['ajaxfblogin_from'];
				unset($this->session->data['ajaxfblogin_from']);
			}

			if(isset($this->session->data['ajaxfblogin_loc'])) {
				$loc = urldecode($this->session->data['ajaxfblogin_loc']);
				unset($this->session->data['ajaxfblogin_loc']);

				$loc_arr = explode("route=", $loc);
					
				if(isset($loc_arr[1]) && $loc_arr[1] == "account/logout")
					$loc = $this->url->link("account/account", '', "SSL");

			}

			$this->load->model('account/customer');

			// Checking email id if already registered
			$email = $user->emailAddress;
			$password = $this->get_password($email);

			if($this->customer->login($email, $password, true)){
				if($a=='checkout') {
					$this->response->redirect($this->url->link('checkout/checkout', '', 'SSL'));
				} else {
					$this->response->redirect($loc);
				}
			}

			$email_query = $this->db->query("SELECT `email` FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(strtolower($email)) . "'");

			if($email_query->num_rows){
				//$this->model_account_customer->editPassword($email, $password);
				if($this->customer->login($email, $password, true)){
					if($a=='checkout'){
						$this->response->redirect($this->url->link('checkout/checkout', '', 'SSL'));
					} else {
						$this->response->redirect($loc);
					}
				}
			} else {

				$f_name = $user->firstName;
				$l_name = $user->lastName;
					
				$config_customer_approval = $this->config->get('config_customer_approval');
				$this->config->set('config_customer_approval',0);
					
				$this->request->post['email'] = $email;

				$add_data=array();
				$add_data['email'] = $email;
				$add_data['password'] = $password;
				$add_data['firstname'] = $f_name;
				$add_data['lastname'] = $l_name;
				$add_data['fax'] = '';
				$add_data['telephone'] = '';
				$add_data['company'] = '';
				$add_data['company_id'] = '';
				$add_data['tax_id'] = '';
				$add_data['address_1'] = '';
				$add_data['address_2'] = '';
				$add_data['city'] = '';
				$add_data['city_id'] = '';
				$add_data['postcode'] = '';
				$add_data['country_id'] = 0;
				$add_data['zone_id'] = 0;
					
				$this->model_account_customer->addCustomer($add_data);
				$this->config->set('config_customer_approval',$config_customer_approval);

					
				if($this->customer->login($email, $password, true)){

					// Delete address
					$this->deleteAddress();


					unset($this->session->data['guest']);
					if($a=='checkout')
					{
						$this->response->redirect($this->url->link('checkout/checkout', '', 'SSL'));
					}
					else{
						$this->response->redirect($loc);
					}
				}
			}
	
	}
	
	// End LinkedIn Login
	
	public function validateTwitLogin(){
	
		$this->language->load('account/ajax_login_register');
		$json = array();
	
		if(!preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['ajax_twit_email'])){
			$json["error"] = $this->language->get('error_email');
		}else{
				
			$json['success'] = $this->language->get('text_success');
				
			$this->session->data['ozxmod_twit_email'] = $this->request->post['ajax_twit_email'];
			$json['redirect']= $this->url->link('account/ajax_login_register/twit', '', 'SSL');
				
		}
	
		return	$this->response->setOutput(json_encode($json));
	}
	
	private function get_password($str) {
		$password = 'newpassword';
		$password.=substr('74993889fa88dc03c2e6b83bab88e845',0,3).substr($str,0,3).substr('74993889fa88dc03c2e6b83bab88e845',-3).substr($str,-3);
		return strtolower($password);
	}
	
	private function get_twitpassword($twit_id) {
		$password = 'newpassword';
		$password.=substr('74993889fa88dc03c2e6b83bab88e845',0,3).substr($twit_id,0,3).substr('74993889fa88dc03c2e6b83bab88e845',-3).substr($twit_id,-3);
		return strtolower($password);
	}
	
	private function clean_decode($data) {
		if (is_array($data)) {
			foreach ($data as $key => $value) {
				unset($data[$key]);
				$data[$this->clean_decode($key)] = $this->clean_decode($value);
			}
		} else {
			$data = htmlspecialchars_decode($data, ENT_COMPAT);
		}
	
		return $data;
	}
	
	private function deleteAddress(){
		$customer_id = $this->session->data['customer_id'];
		$this->db->query("DELETE FROM ".DB_PREFIX."address WHERE customer_id = '".(int)$customer_id."' AND country_id=0 AND zone_id = 0 ");	
	}
	
	
}

/* This module is copywrite to ozxmod
 * Author: ozxmod(ozxmod@gmail.com)
* It is illegal to remove this comment without prior notice to ozxmod(ozxmod@gmail.com)
*/
?>
