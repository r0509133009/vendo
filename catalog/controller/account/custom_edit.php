<?php
// ***************************************************
//             Custom Registration Fields   
//       
// Author : Francesco Pisanò - francesco1279@gmail.com
//              
//                   www.leverod.com		
//               © All rights reserved	  
// ***************************************************




class ControllerAccountEdit extends Controller {
	private $error = array();

	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/edit', '', 'SSL');

			$this->redirect($this->url->link('account/login', '', 'SSL'));
		}

		$this->language->load('account/edit');
		
				// add library cropper
		$this->document->addScript('catalog/view/javascript/cropper/cropper.min.js');
		$this->document->addStyle('catalog/view/javascript/cropper/cropper.min.css');
		
		// check if a language file for this module is available for 
		// the current opencart installation:
		$this->load->model('localisation/language');
		$current_language = $this->model_localisation_language->getLanguage($this->config->get('config_language_id'));
		
		if (file_exists(DIR_LANGUAGE.'/'.$current_language['directory'].'/account/custom_register.php')){
			$this->language->load('account/custom_register');
		} 
		// else we load a default language file (english)
		else {
			// by adding "../english" to the path we can go up a level in the directory tree and go down
			// again to load the subdirectory "english" (to know how Opencart builds the path, see the file
			// "system/library/language.php"
			$this->language->load('../english/account/custom_register');
		}


// added customizable javascript (for Oc and MijoShop)
		$filename = 'catalog/view/javascript/custom_register.js';
		if ( (file_exists($filename) && filesize($filename) != 0) || (file_exists('components/com_mijoshop/opencart/'.$filename) && filesize('components/com_mijoshop/opencart/'.$filename) != 0) ) {
			$this->document->addScript($filename);
		}
// added customizable style	(for Oc and MijoShop)
		$filename = 'catalog/view/theme/default/stylesheet/custom_register.css';
		if ( (file_exists($filename) && filesize($filename) != 0) || (file_exists('components/com_mijoshop/opencart/'.$filename) && filesize('components/com_mijoshop/opencart/'.$filename) != 0) ) {
			$this->document->addStyle($filename);
		}	
		
		
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('account/customer');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_account_customer->editCustomer($this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect($this->url->link('account/account', '', 'SSL'));
		}

      	$this->data['breadcrumbs'] = array();

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),     	
        	'separator' => false
      	); 

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_account'),
			'href'      => $this->url->link('account/account', '', 'SSL'),        	
        	'separator' => $this->language->get('text_separator')
      	);

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_edit'),
			'href'      => $this->url->link('account/edit', '', 'SSL'),       	
        	'separator' => $this->language->get('text_separator')
      	);
		
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_your_details'] = $this->language->get('text_your_details');

		$this->data['entry_firstname'] = $this->language->get('entry_firstname');
		$this->data['entry_lastname'] = $this->language->get('entry_lastname');
		$this->data['entry_email'] = $this->language->get('entry_email');
		$this->data['entry_telephone'] = $this->language->get('entry_telephone');
		$this->data['entry_fax'] = $this->language->get('entry_fax');

		$this->data['button_continue'] = $this->language->get('button_continue');
		$this->data['button_back'] = $this->language->get('button_back');
		
		
		
		$reg_entries = array(	
			'firstname',
			'lastname',
			'telephone',
			'fax'
		);
		
		foreach ($reg_entries as $reg_entry) {
			$this->data['custom_register_account_member_enable_'.$reg_entry] = $this->config->get('custom_register_account_member_enable_'.$reg_entry);
			$this->data['custom_register_account_member_'.$reg_entry.'_required'] = $this->config->get('custom_register_account_member_'.$reg_entry.'_required');
			
			$this->data['custom_register_member_enable_'.$reg_entry] = $this->config->get('custom_register_member_enable_'.$reg_entry);
			$this->data['custom_register_member_'.$reg_entry.'_required'] = $this->config->get('custom_register_member_'.$reg_entry.'_required');
		}

		
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['firstname'])) {
			$this->data['error_firstname'] = $this->error['firstname'];
		} else {
			$this->data['error_firstname'] = '';
		}

		if (isset($this->error['lastname'])) {
			$this->data['error_lastname'] = $this->error['lastname'];
		} else {
			$this->data['error_lastname'] = '';
		}
		
		if (isset($this->error['email'])) {
			$this->data['error_email'] = $this->error['email'];
		} else {
			$this->data['error_email'] = '';
		}	
		
		if (isset($this->error['telephone'])) {
			$this->data['error_telephone'] = $this->error['telephone'];
		} else {
			$this->data['error_telephone'] = '';
		}	
		
		if (isset($this->error['fax'])) {
			$this->data['error_fax'] = $this->error['fax'];
		} else {
			$this->data['error_fax'] = '';
		}

		$this->data['action'] = $this->url->link('account/edit', '', 'SSL');

		if ($this->request->server['REQUEST_METHOD'] != 'POST') {
			$customer_info = $this->model_account_customer->getCustomer($this->customer->getId());
		}

		if (isset($this->request->post['firstname'])) {
			$this->data['firstname'] = $this->request->post['firstname'];
		} elseif (isset($customer_info)) {
			$this->data['firstname'] = $customer_info['firstname'];
		} else {
			$this->data['firstname'] = '';
		}

		if (isset($this->request->post['lastname'])) {
			$this->data['lastname'] = $this->request->post['lastname'];
		} elseif (isset($customer_info)) {
			$this->data['lastname'] = $customer_info['lastname'];
		} else {
			$this->data['lastname'] = '';
		}

		if (isset($this->request->post['email'])) {
			$this->data['email'] = $this->request->post['email'];
		} elseif (isset($customer_info)) {
			$this->data['email'] = $customer_info['email'];
		} else {
			$this->data['email'] = '';
		}

		if (isset($this->request->post['telephone'])) {
			$this->data['telephone'] = $this->request->post['telephone'];
		} elseif (isset($customer_info)) {
			$this->data['telephone'] = $customer_info['telephone'];
		} else {
			$this->data['telephone'] = '';
		}

		if (isset($this->request->post['fax'])) {
			$this->data['fax'] = $this->request->post['fax'];
		} elseif (isset($customer_info)) {
			$this->data['fax'] = $customer_info['fax'];
		} else {
			$this->data['fax'] = '';
		}


		// customer avatar
		if (isset($customer_info['image']) && file_exists(DIR_IMAGE . $customer_info['image']) && !empty($customer_info['image'])) {
			$this->data['customer_photo'] = HTTP_SERVER . 'image/' . $customer_info['image'];
		} else {
			// if(!empty($this->session->data['facebook_profile_picture'])){
   //          	$this->data['customer_photo'] = $this->session->data['facebook_profile_picture'];
   //        	}else{
   //          	$this->data['customer_photo'] = HTTP_SERVER . 'catalog/view/javascript/cropper/no_user.png';
   //        	}
			$this->data['customer_photo'] = HTTP_SERVER . 'catalog/view/javascript/cropper/edit-profile-placehold.png';
		}

		$data['action_user_avatar'] = $this->url->link('account/edit/saveUserAvatar', 'SSL');	

		
		$this->data['text_your_photo'] = $this->language->get('text_your_photo');
		$this->data['text_your_avatar_profile'] = $this->language->get('text_your_avatar_profile');
		$this->data['text_preview_image_title'] = $this->language->get('text_preview_image_title');
		$this->data['text_preview_image_description'] = $this->language->get('text_preview_image_description');
		$this->data['button_upload'] = $this->language->get('button_upload');
		$this->data['button_save'] = $this->language->get('button_save');

		$this->data['back'] = $this->url->link('account/account', '', 'SSL');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/edit.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/account/edit.tpl';
		} else {
			$this->template = 'default/template/account/edit.tpl';
		}
		
	
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'	
		);
						
		$this->response->setOutput($this->render());	
	}

	public function saveCustomerAvatar() {
		
		$this->load->language('account/edit');
		$this->load->model('account/custom_edit');
		
		$data['success'] = '';
				
		$image = $this->request->post['image'];
		$image = str_replace('data:image/png;base64,', '', $image);
		$image = str_replace(' ', '+', $image);
		$image = base64_decode($image);
		$filename = 'customers/'.$this->customer->getId().'/avatar.png';
		$file = DIR_IMAGE . $filename;
		
		$dirname = dirname(DIR_IMAGE.$filename);
		if (!is_dir($dirname))
		{
		    mkdir($dirname, 0755, true);
		}

		file_put_contents($file, $image);
		
		$this->model_account_custom_edit->editCustomerImage($filename);
			
		$data['success'] = $this->language->get('text_edit_avatar_success');			
		
		$data['error'] = $this->error;
						
		$this->response->setOutput(json_encode($data));
	}

	protected function validate() {
	
	
		// if firstname enabled
		if ( $this->config->get('custom_register_account_member_enable_firstname') || $this->config->get('custom_register_member_enable_firstname') ){
			
			// if required 
			if ( $this->config->get('custom_register_account_member_firstname_required') || $this->config->get('custom_register_member_firstname_required') ) {

				// if empty or out of range
				if ( (strlen(utf8_decode($this->request->post['firstname'])) < 1) || (strlen(utf8_decode($this->request->post['firstname'])) > 32) ) {
					$this->error['firstname'] = $this->language->get('error_firstname');
				}	
			}
			// if not required, not empty but out of range
			elseif ( strlen(utf8_decode($this->request->post['firstname'])) > 32 ) {
				$this->error['firstname'] = $this->language->get('error_firstname');
			}	
		}

		
		// if lastname enabled
		if ( $this->config->get('custom_register_account_member_enable_lastname') || $this->config->get('custom_register_member_enable_lastname') ){
			
			// if required 
			if ( $this->config->get('custom_register_account_member_lastname_required') || $this->config->get('custom_register_member_lastname_required') ) {

				// if empty or out of range
				if ( (strlen(utf8_decode($this->request->post['lastname'])) < 1) || (strlen(utf8_decode($this->request->post['lastname'])) > 32) ) {
					$this->error['lastname'] = $this->language->get('error_lastname');
				}	
			}
			// if not required, not empty but out of range
			elseif ( strlen(utf8_decode($this->request->post['lastname'])) > 32 ) {
				$this->error['lastname'] = $this->language->get('error_lastname');
			}	
		}

		
		if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['email'])) {
			$this->error['email'] = $this->language->get('error_email');
		}
		
		
		if (($this->customer->getEmail() != $this->request->post['email']) && $this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_exists');
		}

		
		// if telephone enabled
		if ( $this->config->get('custom_register_account_member_enable_telephone') ||$this->config->get('custom_register_member_enable_telephone') ){
			
			// if required 
			if ( $this->config->get('custom_register_account_member_telephone_required') || $this->config->get('custom_register_member_telephone_required') ) {

				// if empty or out of range
				if ( (strlen(utf8_decode($this->request->post['telephone'])) < 3) || (strlen(utf8_decode($this->request->post['telephone'])) > 32) ) {
					$this->error['telephone'] = $this->language->get('error_telephone');
				}	
			}
			// if not required, not empty but out of range
			elseif ( (strlen(utf8_decode($this->request->post['telephone'])) > 0 ) && (strlen(utf8_decode($this->request->post['telephone'])) < 3) || (strlen(utf8_decode($this->request->post['telephone'])) > 32) ) {
				$this->error['telephone'] = $this->language->get('error_telephone');
			}	
		}
		
		
	// Added checking for fax: 
		// if fax enabled
		if ( $this->config->get('custom_register_account_member_enable_fax') || $this->config->get('custom_register_member_enable_fax') ){
			
			// if required 
			if ( $this->config->get('custom_register_account_member_fax_required') || $this->config->get('custom_register_member_fax_required') ) {

				// if empty or out of range
				if ( (strlen(utf8_decode($this->request->post['fax'])) < 3) || (strlen(utf8_decode($this->request->post['fax'])) > 32) ) {
					$this->error['fax'] = $this->language->get('error_fax');
				}	
			}
			// if not required, not empty but out of range
			elseif ( (strlen(utf8_decode($this->request->post['fax'])) > 0 ) && (strlen(utf8_decode($this->request->post['fax'])) < 3) || (strlen(utf8_decode($this->request->post['fax'])) > 32) ) {

				$this->error['fax'] = $this->language->get('error_fax');
			}	
		}
		

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
}
?>