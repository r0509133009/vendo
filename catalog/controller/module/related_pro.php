<?php
require_once(substr_replace(DIR_SYSTEM, '', -7) . 'vendor/equotix/related_pro/equotix.php');
class ControllerModuleRelatedPro extends Equotix {
	protected $code = 'related_pro';
	protected $extension_id = '65';
	
	protected function index($setting) {
		if (isset($this->request->get['product_id'])) {
			$product_id = (int)$this->request->get['product_id'];
		} else {
			$product_id = 0;
		}
		
		$this->language->load('module/related_pro');
		
		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['button_cart'] = $this->language->get('button_cart');
				
		$this->load->model('catalog/product');
		
		$this->load->model('tool/image');
		
		$this->data['products'] = array();

		$results = $this->model_catalog_product->getRelatedProProducts($product_id);
		
		if ($this->config->get('related_pro_random')) {
			shuffle($results);
		}
		
		$results = array_slice($results, 0, $setting['limit']);

		foreach ($results as $product_id) {
			$result = $this->model_catalog_product->getProduct($product_id);
			
			if ($result['image']) {
				$image = $this->model_tool_image->resize($result['image'], $setting['image_width'], $setting['image_height']);
			} else {
				$image = false;
			}
						
			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$price = false;
			}
					
			if ((float)$result['special']) {
				$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$special = false;
			}
			
			if ($this->config->get('config_review_status')) {
				$rating = $result['rating'];
			} else {
				$rating = false;
			}
			
			$this->data['products'][] = array(
				'product_id' => $result['product_id'],
				'thumb'   	 => $image,
				'name'    	 => $result['name'],
				'price'   	 => $price,
				'special' 	 => $special,
				'rating'     => $rating,
				'reviews'    => sprintf($this->language->get('text_reviews'), (int)$result['reviews']),
				'href'    	 => $this->url->link('product/product', 'product_id=' . $result['product_id']),
			);
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/related_pro.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/related_pro.tpl';
		} else {
			$this->template = 'default/template/module/related_pro.tpl';
		}

		if ($this->validated() && isset($this->request->get['route']) && $this->request->get['route'] == 'product/product') {
			$this->render();
		}
	}
}
?>