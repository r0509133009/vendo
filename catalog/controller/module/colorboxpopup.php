<?php
class ControllerModuleColorboxpopup extends Controller {
	public function index() {
		$this->language->load('module/colorboxpopup');
         
		$data['heading_title'] = $this->language->get('heading_title');

		$this->data['action'] = $this->url->link('module/colorboxpopup', '', 'SSL');
		
		$this->load->model('module/coupon');
		$coupom_info = $this->model_module_coupon->getCoupon($this->config->get('colorboxpopup'));
		$this->data['coupon'][] = array(
			'coupon_id'  => $coupom_info['coupon_id'],
			'coupon_name'=> $coupom_info['name'],
			'coupon_code'=> $coupom_info['code'],
			
		);
		
		$this->data['colorboxpopup_dicount_title_second'] = $this->config->get('colorboxpopup_dicount_title_second');
		$this->data['colorboxpopup_dicount_desc_second'] = $this->config->get('colorboxpopup_dicount_desc_second');
			
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			

			$this->load->model('module/coupon');
			
			$chk_db = $this->model_module_coupon->check_db();
			
			$add_info = $this->model_module_coupon->addpopData($this->request->post);
			
			
		}
			
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/colorbox/content/inline.php')) {
			$this->template = $this->config->get('config_template') . '/template/module/colorbox/content/inline.php';
		} else {
			$this->template = 'default/template/module/colorbox/content/inline.php';
		}

		$this->response->setOutput($this->render());			
	}
}