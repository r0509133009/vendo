<?php  
class ControllerModuleCustomerphotos extends Controller {
	protected function index($setting) {
		$this->language->load('module/customerphotos');
		$this->load->model('module/customerphotos');

      	$this->data['heading_title']			= $this->language->get('heading_title');
		$this->data['h2_CustomerPhotos']		= $this->language->get('h2_CustomerPhotos');
		$this->data['currenttemplate']		  = $this->config->get('config_template');
        $this->data['data']					 = $this->model_module_customerphotos->getSetting('CustomerPhotos', $this->config->get('config_store_id'));


		if  ((isset($this->request->get['route'])) && ($this->request->get['route']=="product/product")) {
			$this->data['customerPhotosCurrentURL'] = $this->url->link("product/product","product_id=".$this->request->get['product_id'],"");
		} else {
			if (strpos(HTTP_SERVER,'www.') && strpos(HTTPS_SERVER,'www.')) {
				$siteName = $_SERVER["SERVER_NAME"];
			} else {
				$siteName = str_replace("www.", "", $_SERVER['SERVER_NAME']);
			}
			$this->data['customerPhotosCurrentURL'] = "http://".$siteName.$_SERVER["REQUEST_URI"];  
		}
		
		if(!isset($this->data['data']['CustomerPhotos']['MainMessage'][$this->config->get('config_language')])){
			$this->data['data']['CustomerPhotos']['MainMessage'] = '';
		} else {
			$this->data['data']['CustomerPhotos']['MainMessage'] = $this->data['data']['CustomerPhotos']['MainMessage'][$this->config->get('config_language')];
		}
		
		if (isset($this->request->get['product_id'])) {
			$product_id = (int)$this->request->get['product_id'];
			$product_info = $this->model_catalog_product->getProduct($product_id);
			$this->data['data']['CustomerPhotos']['CustomTags'] = $this->model_module_customerphotos->getSetting('customerphotos_customtags', $this->config->get('config_store_id'));
			$flag=0;
			if (!empty($this->data['data']['CustomerPhotos']['CustomTags'])) {
				foreach ($this->data['data']['CustomerPhotos']['CustomTags'] as $CustomTags) {
					 if ($CustomTags['pid']==$product_id) {
						 $flag=1;
							if ($this->data['data']['CustomerPhotos']['UseSiteShortName'] == "yes") {
								$this->data['CustomerPhotosProductName'] = $this->data['data']['CustomerPhotos']['ShortTag'].$CustomTags['tag'];	
							} else {
								$this->data['CustomerPhotosProductName'] = $CustomTags['tag'];
							}
					 }
				}
			}
			if ($flag==0) {
				$replace = array(' ', '"', '&quot;', '.', ',' ,'\'', '!', '-', '?', '*', '+', '&', '@', '$', '%', ':', '(', ')');
				$replaced = array('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
				if (isset($this->data['data']['CustomerPhotos']['TagType']) && ($this->data['data']['CustomerPhotos']['TagType']=='product_model')) {
					$product_strip_unused_chars = str_replace($replace, $replaced, $product_info['model']);
				} else {
					$product_strip_unused_chars = str_replace($replace, $replaced, $product_info['name']);
				}
				if (extension_loaded('mbstring')) { 
					$final_tag_var = mb_substr($product_strip_unused_chars,0,$this->data['data']['CustomerPhotos']['TagCharacterLimit'], 'utf-8');
					$this->data['CustomerPhotosProductName'] = $this->data['data']['CustomerPhotos']['ShortTag'].mb_strtolower($final_tag_var,'utf-8'); 
				} else {
					$final_tag_var = substr($product_strip_unused_chars,0,$this->data['data']['CustomerPhotos']['TagCharacterLimit']);
					$this->data['CustomerPhotosProductName'] = $this->data['data']['CustomerPhotos']['ShortTag'].strtolower($final_tag_var); 
				}
			}
			$this->data['CustomerPhotosProductNameBig'] = $product_info['name'];
			$this->data['RegardTabs'] = true;
		} else {
			$this->data['RegardTabs'] = false;
			$this->data['CustomerPhotosProductName'] = (isset($setting['tag']) && !empty($setting['tag'])) ? $setting['tag'] : '';
		}
		
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/customerphotos.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/customerphotos.tpl';
		} else {
			$this->template = 'default/template/module/customerphotos.tpl';
		}
		
		$this->render();
	}

	public function twitterTags() {
		if(isset($_GET) && !empty($_GET['hashtag'])) {
			require_once(DIR_SYSTEM . '../vendors/customerphotos/twitter/RestApi.php');

			$this->data['data']['CustomerPhotos'] = $this->config->get('CustomerPhotos');

			$consumerKey = $this->data['data']['CustomerPhotos']['TwitterConsumerKey'];
			$consumerSecret = $this->data['data']['CustomerPhotos']['TwitterConsumerKeySecret'];
			$accessToken = $this->data['data']['CustomerPhotos']['TwitterAccessToken'];
			$accessTokenSecret = $this->data['data']['CustomerPhotos']['TwitterAccessTokenSecret'];
			
			$twitter = new \TwitterPhp\RestApi($consumerKey,$consumerSecret,$accessToken,$accessTokenSecret);
			$connection = $twitter->connectAsApplication();
			$tweets = $connection->get('search/tweets', array('count'=>$this->data['data']['CustomerPhotos']['TwitterPhotos'],'q'=>'#'.$_GET['hashtag'].' filter:images','include_entities'=>1));	
			$n=1;
			$twitterTagsInfo = array();
			foreach($tweets['statuses'] as $twitterPost){
				if (empty($twitterPost['entities']['media'][0]['media_url'])) continue;
				if ($n>$this->data['data']['CustomerPhotos']['TwitterPhotos']) break;
					$twitterTagsInfo[] = array('image' => $twitterPost['entities']['media'][0]['media_url'], 'link' => $twitterPost['entities']['media'][0]['url'] );
					$n++;
			}    
			header('content-type: text/json');
			echo json_encode($twitterTagsInfo);
			exit;
		}
	}

}

?>