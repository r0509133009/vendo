<?php
class ControllerModuleColorboxpopupMain extends Controller {
	public function index() {
		$this->language->load('module/colorboxpopup');
         
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['action'] = $this->url->link('module/colorboxpopup', '', 'SSL');
		
		$this->load->model('module/coupon');
		$coupom_info = $this->model_module_coupon->getCoupon($this->config->get('colorboxpopup'));
		$this->data['coupon'][] = array(
			'coupon_id'  => $coupom_info['coupon_id'],
			'coupon_name'=> $coupom_info['name'],
			'coupon_code'=> $coupom_info['code'],
			'coupon_type'=> $coupom_info['type'],
			'coupon_discount'=> $coupom_info['discount']
		);
		
		$this->data['currency_symbol_right'] = $this->currency->getSymbolRight($this->session->data['currency']);
		$this->data['currency_symbol_left'] = $this->currency->getSymbolLeft($this->session->data['currency']);
		$this->data['colorboxpopup_dicount_title'] = $this->config->get('colorboxpopup_dicount_title');
		$this->data['colorboxpopup_dicount_desc'] = $this->config->get('colorboxpopup_dicount_desc');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			
			$this->load->model('module/coupon');
			
			$chk_db = $this->model_module_coupon->check_db();
			
			$add_info = $this->model_module_coupon->addpopData($this->request->post);
			
		}
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/colorbox/content/colorboxpopup.php')) {
			$this->template = $this->config->get('config_template') . '/template/module/colorbox/content/colorboxpopup.php';
		} else {
			$this->template = 'default/template/module/colorbox/content/colorboxpopup.php';
		}
			
		$this->response->setOutput($this->render());
		
		
	}
}