<?php
class ControllerModuleLivehelp extends Controller {
   private $livehelp;
   protected $operators;
   protected $error;
   
   protected function index($setting) {
      if (!$this->config->get('livehelp_status'))
         return;
      
      $this->initClass();
      
      if ($this->livehelp->isBanned())
         return;
      
      $this->language->load('module/livehelp');
      
      $this->document->addStyle('catalog/view/theme/default/stylesheet/livehelp.min.css');
      
      if ($this->livehelp->isLogged()) {
         $this->data['is_logged'] = true;
         $this->loggedIndex($setting);
      } else {
         $this->data['is_logged'] = false;
         $this->operators         = $this->livehelp->getOperators(array(
            'online' => 1
         ));
         
         if (count($this->operators)) {
            $this->notLoggedIndex($setting);
         } else {
            if ($this->livehelp->getSetting('offline_form'))
               $this->offlineForm($setting);
         }
      }
   }
   
   // When user is logged
   protected function loggedIndex($setting) {
      $this->document->addScript('catalog/view/javascript/livehelp/livehelp_chat.min.js');
      
      if ($this->livehelp->getSetting('smiley_status') || $this->livehelp->getSetting('bbcode_status')) {
         $this->document->addStyle('catalog/view/javascript/livehelp/livehelp_bs_popover.css');
         $this->document->addScript('catalog/view/javascript/livehelp/livehelp_bs_popover.js');
      }
      
      // Language
      $this->data['text_operator']        = $this->language->get('text_operator');
      $this->data['text_signout_confirm'] = $this->language->get('text_signout_confirm');
      $this->data['text_signout']         = $this->language->get('text_signout');
      $this->data['text_volume']          = $this->language->get('text_volume');
      
      // BBCode title
      $this->data['title_bold']      = $this->language->get('title_bold');
      $this->data['title_italic']    = $this->language->get('title_italic');
      $this->data['title_underline'] = $this->language->get('title_underline');
      $this->data['title_link']      = $this->language->get('title_link');
      $this->data['title_image']     = $this->language->get('title_image');
      $this->data['title_youtube']   = $this->language->get('title_youtube');
      
      $js_language                                        = array();
      $js_language[$this->config->get('config_language')] = array(
         'text_operator_signed' => $this->language->get('text_js_operator_signed'),
         'text_operator_online_again' => $this->language->get('text_js_operator_online_again'),
         'text_operator_away' => $this->language->get('text_js_operator_away'),
         'error_kicked' => $this->language->get('error_js_kicked'),
         'text_muted_for_time' => $this->language->get('text_js_muted_for_time'),
         'text_entry_url' => $this->language->get('text_js_entry_url'),
         'text_entry_youtube' => $this->language->get('text_js_entry_youtube')
      );

      $setting = array();
      
      $setting['store_refresh_rate'] = (int) $this->livehelp->getSetting('store_refresh_rate');
      $setting['text_max_length']    = (int) $this->livehelp->getSetting('text_max_length');
      $setting['smiley_status']      = $this->livehelp->getSetting('smiley_status');
      $setting['bbcode_status']      = $this->livehelp->getSetting('bbcode_status');
      
      $setting['store_sound_new_message']     = $this->livehelp->getSetting('store_sound_new_message');
      $setting['store_sound_message_sent']    = $this->livehelp->getSetting('store_sound_message_sent');
      $setting['store_sound_operator_logged'] = $this->livehelp->getSetting('store_sound_operator_logged');
      $setting['store_sound_operator_logout'] = $this->livehelp->getSetting('store_sound_operator_logout');
      $setting['store_sound_special']         = $this->livehelp->getSetting('store_sound_special');
      
      $setting['sound'] = ($setting['store_sound_new_message'] || $setting['store_sound_operator_logged'] || $setting['store_sound_operator_logout'] || $setting['store_sound_special']);
      
      // SPAM filter
      $setting['spam_filter_status']      = $this->livehelp->getSetting('spam_filter_status');
      $setting['spam_filter_score_limit'] = $this->livehelp->getSetting('spam_filter_score_limit');
      $setting['spam_filter_penalty']     = $this->livehelp->getSetting('spam_filter_penalty');
      $setting['spam_filter_range']       = $this->livehelp->getSetting('spam_filter_range');
      
      // Load smiles
      $this->data['emoticons']  = $this->livehelp->getEmoticons();
      $setting['smiley_status'] = ($setting['smiley_status'] && !empty($this->data['emoticons'])); // merge to one
      
      $this->data['user_name']   = $this->livehelp->getUserName();
      $this->data['last_readed'] = $this->livehelp->getLastReaded();
      $this->data['setting']     = $setting;
      $this->data['js_language'] = $js_language;
      
      $this->data['language_id']   = $this->config->get('config_language_id');
      $this->data['language_code'] = $this->config->get('config_language');
      
      
      if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/livehelp_on.tpl')) {
         $this->template = $this->config->get('config_template') . '/template/module/livehelp_on.tpl';
      } else {
         $this->template = 'default/template/module/livehelp_on.tpl';
      }
      
      $this->render();
   }
   
   // When user is not logged
   protected function notLoggedIndex($setting) {
      $title = $this->livehelp->getSetting('store_heading_title');
      
      if (!empty($setting['title'][$this->config->get('config_language_id')])) {
         $this->data['heading_title'] = html_entity_decode($setting['title'][$this->config->get('config_language_id')]);
      } else {
         $this->data['heading_title'] = (!empty($title[$this->config->get('config_language_id')]) ? html_entity_decode($title[$this->config->get('config_language_id')]) : $this->language->get('heading_title'));
      }
      
      // Language
      $this->data['text_operator']             = $this->language->get('text_operator');
      $this->data['text_description']          = $this->language->get('text_description');
      $this->data['text_wait']                 = $this->language->get('text_wait');
      $this->data['entry_livehelp_name']       = $this->language->get('entry_livehelp_name');
      $this->data['placeholder_livehelp_name'] = $this->language->get('placeholder_livehelp_name');
      $this->data['button_sign']               = $this->language->get('button_sign');
      $this->data['button_select_operator']    = $this->language->get('button_select_operator');
      
      $this->data['operators'] = array();
      
      foreach ($this->operators as $operator) {
         $this->data['operators'][] = array(
            'operator_id' => $operator['operator_id'],
            'name' => ($operator['name'] ? $operator['name'] : $operator['username']),
            'description' => html_entity_decode($operator['description']),
            'status_id' => $operator['status_id']
         );
      }
      
      $this->data['default_operator_id'] = $this->data['operators'][0]['operator_id'];
      
      $this->data['user_name'] = $this->livehelp->getUserName();
      
      if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/livehelp_off.tpl')) {
         $this->template = $this->config->get('config_template') . '/template/module/livehelp_off.tpl';
      } else {
         $this->template = 'default/template/module/livehelp_off.tpl';
      }
      
      $this->render();
   }
   
   protected function offlineForm($setting) {
      $title = $this->livehelp->getSetting('store_heading_title');
      
      if (!empty($setting['title'][$this->config->get('config_language_id')])) {
         $this->data['heading_title'] = html_entity_decode($setting['title'][$this->config->get('config_language_id')]);
      } else {
         $this->data['heading_title'] = (!empty($title[$this->config->get('config_language_id')]) ? html_entity_decode($title[$this->config->get('config_language_id')]) : $this->language->get('heading_title'));
      }
      
      $this->data['text_offline_description'] = $this->language->get('text_offline_description');
      $this->data['text_wait']                = $this->language->get('text_wait');
      
      $this->data['entry_livehelp_email']   = $this->language->get('entry_livehelp_email');
      $this->data['entry_livehelp_enquiry'] = $this->language->get('entry_livehelp_enquiry');
      $this->data['entry_livehelp_name']    = $this->language->get('entry_livehelp_name');
      
      $this->data['button_send'] = $this->language->get('button_send');
      
      $this->data['user_name'] = $this->livehelp->getUserName();
      
      if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/livehelp_form.tpl')) {
         $this->template = $this->config->get('config_template') . '/template/module/livehelp_form.tpl';
      } else {
         $this->template = 'default/template/module/livehelp_form.tpl';
      }
      
      $this->render();
   }
   
   protected function formValidate() {
      if ((utf8_strlen($this->request->post['livehelp_email']) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['livehelp_email'])) {
         $this->error['email'] = $this->language->get('error_user_email');
      }
      
      if ((utf8_strlen($this->request->post['livehelp_enquiry']) < 10) || (utf8_strlen($this->request->post['livehelp_enquiry']) > 250)) {
         $this->error['enquiry'] = $this->language->get('error_user_enquiry');
      }
      
      return !$this->error;
   }
   
   private function initClass() {
      $this->load->library('livehelp/livehelp_user');
      
      $this->livehelp = new Livehelp($this->registry);
   }
   
   public function operation() {
      $this->language->load('module/livehelp');
      $json = array();
      
      if ($this->request->server['REQUEST_METHOD'] == 'POST') {
         if (isset($this->request->post['operation'])) {
            $operation = strtolower($this->request->post['operation']);
         } else {
            $operation = null;
         }
         
         $this->initClass();
         
         try {
            switch ($operation) {
               case 'contact':
                  if ($this->formValidate()) {
                     $from   = $this->request->post['livehelp_email'];
                   $sender = $this->request->post['livehelp_name'] ? $this->request->post['livehelp_name'] : $this->request->post['livehelp_email'];
                     $mail            = new Mail();
                     $mail->protocol  = $this->config->get('config_mail_protocol');
                     $mail->parameter = $this->config->get('config_mail_parameter');
                     $mail->hostname  = $this->config->get('config_smtp_host');
                     $mail->username  = $this->config->get('config_smtp_username');
                     $mail->password  = $this->config->get('config_smtp_password');
                     $mail->port      = $this->config->get('config_smtp_port');
                     $mail->timeout   = $this->config->get('config_smtp_timeout');
                     $mail->setTo($this->config->get('config_email'));
                     $mail->setFrom($from);
                     $mail->setSender($sender);
                     $mail->setSubject(html_entity_decode(sprintf($this->language->get('livehelp_email_subject'), $sender), ENT_QUOTES, 'UTF-8'));
                     $mail->setText(strip_tags(html_entity_decode($this->request->post['livehelp_enquiry'], ENT_QUOTES, 'UTF-8')));
                     $mail->send();
                     
                     // Send to additional alert emails
                     $emails = explode(',', $this->livehelp->getSetting('offline_form_emails'));
                     
                     foreach ($emails as $email) {
                        if ($email && preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $email)) {
                           $mail->setTo($email);
                           $mail->send();
                        }
                     }
                     
                     $json['success'] = $this->language->get('success_email_sent');
                  } else {
                     $json['error'] = $this->error;
                  }
                  break;
               case 'signin':
                  if ((utf8_strlen($this->request->post['livehelp_name']) < 3) || (utf8_strlen($this->request->post['livehelp_name']) > 64)) {
                     $json['error']['name'] = $this->language->get('error_name');
                  }
                  
                  if (!(int) $this->request->post['livehelp_operator_id'] || !$this->livehelp->getOperators(array(
                     'operator_id' => $this->request->post['livehelp_operator_id']
                  ))) {
                     $json['error']['operator_id'] = $this->language->get('error_operator_id');
                  }
                  
                  if (!isset($json['error'])) {
                     $json['success'] = $this->livehelp->signIn($this->request->post['livehelp_name'], $this->request->post['livehelp_operator_id']);
                  }
                  break;
               case 'signout':
                  $this->livehelp->signOut();
                  break;
               case 'new_msg':
                  if (!isset($this->request->post))
                     throw new Exception("Error requested data", 1);
                  $json = $this->livehelp->sendMsg($this->request->post);
                  if (!$json)
                     $json = array(
                        'error' => $this->language->get('error_chat_closed')
                     );
                  break;
               default: // autoscript refresh
                  if (!isset($this->request->post['lastId']))
                     throw new Exception("Error requested data", 1);
                  $json = $this->livehelp->autoUpdate($this->request->post);
                  break;
            }
            
         }
         catch (Exception $e) {
            $json['error'] = $e;
         }
      }
      
      $this->response->setOutput(json_encode($json));
   }
}
