<?php
class ControllerModulePopuploggin extends Controller {

	private $json = array();

	public function index() {

		$this->load->language('module/popuploggin');

		$this->data['text_loggin'] = $this->language->get('text_loggin');
		if($this->request->server['REQUEST_METHOD'] == 'POST') {

		}

		$this->data['facebooklogin'] =  $this->getChild('module/facebooklogin');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/popuploggin/popuploggin.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/popuploggin/popuploggin.tpl';
		} else {
			$this->template = 'default/template/module/popuploggin/popuploggin.tpl';
		}



		$this->response->setOutput($this->render());
	}

	public function login() {
		$this->load->model('account/customer');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateLogin()) {
			unset($this->session->data['guest']);

			// Default Shipping Address
			$this->load->model('account/address');

			$address_info = $this->model_account_address->getAddress($this->customer->getAddressId());

			if ($address_info) {
				if ($this->config->get('config_tax_customer') == 'shipping') {
					$this->session->data['shipping_country_id'] = $address_info['country_id'];
					$this->session->data['shipping_zone_id'] = $address_info['zone_id'];
					$this->session->data['shipping_postcode'] = $address_info['postcode'];	
				}

				if ($this->config->get('config_tax_customer') == 'payment') {
					$this->session->data['payment_country_id'] = $address_info['country_id'];
					$this->session->data['payment_zone_id'] = $address_info['zone_id'];
				}
			} else {
				unset($this->session->data['shipping_country_id']);	
				unset($this->session->data['shipping_zone_id']);	
				unset($this->session->data['shipping_postcode']);
				unset($this->session->data['payment_country_id']);	
				unset($this->session->data['payment_zone_id']);	
			}
			
			$this->json['success'] = 'success';
		}


		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($this->json));
	}

	public function register() {
		$this->language->load('account/register');
		$this->load->model('account/customer');
		$this->load->model('account/popuploggin_register');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateRegister()) {
			$this->model_account_popuploggin_register->addCustomer($this->request->post);

			$this->customer->login($this->request->post['email'], $this->request->post['password']);

			unset($this->session->data['guest']);

			$this->json['success'] = 'success';
		}

		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($this->json));
	}

	public function forgotten() {
		$this->load->model('account/customer');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForgotten()) {
			$this->language->load('mail/forgotten');
			$this->language->load('account/forgotten');

			$password = substr(sha1(uniqid(mt_rand(), true)), 0, 10);

			$this->model_account_customer->editPassword($this->request->post['email'], $password);

			$subject = sprintf($this->language->get('text_subject'), $this->config->get('config_name'));

			$message  = sprintf($this->language->get('text_greeting'), $this->config->get('config_name')) . "\n\n";
			$message .= $this->language->get('text_password') . "\n\n";
			$message .= $password;

			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->hostname = $this->config->get('config_smtp_host');
			$mail->username = $this->config->get('config_smtp_username');
			$mail->password = $this->config->get('config_smtp_password');
			$mail->port = $this->config->get('config_smtp_port');
			$mail->timeout = $this->config->get('config_smtp_timeout');				
			$mail->setTo($this->request->post['email']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender($this->config->get('config_name'));
			$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
			$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
			$mail->send();

			$this->json['success'] = $this->language->get('text_success');
		}

		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($this->json));
	}

	protected function validateLogin() {
		$json = array();

		if (!$this->customer->login($this->request->post['email'], $this->request->post['password'])) {
			$this->json['warning'] = $this->language->get('error_login');
		}

		$customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);

		if ($customer_info && !$customer_info['approved']) {
			$this->json['warning'] = $this->language->get('error_approved');
		}

		if (!$this->json) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateRegister() {
		if ((utf8_strlen($this->request->post['firstname']) < 1) || (utf8_strlen($this->request->post['firstname']) > 32)) {
			$this->json['warning'] = $this->language->get('error_firstname');
		}

		if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['email'])) {
			$this->json['warning'] = $this->language->get('error_email');
		}

		if ($this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->json['warning'] = $this->language->get('error_exists');
		}

		$this->load->model('localisation/country');

		if ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
			$this->json['warning'] = $this->language->get('error_password');
		}

		if ($this->config->get('config_account_id')) {
			$this->load->model('catalog/information');

			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));
		}

		if (!$this->json) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateForgotten() {
		$json = array();

		if (!isset($this->request->post['email']) || empty($this->request->post['email'])) {
			$this->json['warning'] = $this->language->get('error_email');
		} elseif (!$this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->json['warning'] = $this->language->get('error_email');
		}

		if (!$this->json) {
			return true;
		} else {
			return false;
		}
	}

}