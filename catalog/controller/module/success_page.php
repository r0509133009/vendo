<?php
class ControllerModuleSuccessPage extends Controller {
	public function index() {
		$this->load->model('checkout/order');
			
		$order_info = $this->model_checkout_order->getOrder($this->session->data['last_order_id']);

		if ($order_info) {
			if ($this->config->get('success_page_invoice_status') && !$order_info['invoice_no']) {
				$this->createInvoiceNo($order_info);
			}

			$data['order_total'] = $order_info['total'];
			$data['order_currency_code'] = $order_info['currency_code'];

			$query_tax = $this->db->query("SELECT value FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_info['order_id'] . "' AND code = 'tax' LIMIT 1");

			if ($query_tax->num_rows) {
				$order_tax = $query_tax->row['value'];
			} else {
				$order_tax = '';
			}

			$data['order_total_without_tax'] = round(($order_info['total'] - $order_tax), 2);

			$data['sections'] = array();

			$sections = ($this->config->get('success_page_section') && is_array($this->config->get('success_page_section'))) ? $this->config->get('success_page_section') : array();

			foreach ($sections as $module) {
				$section_key = strtolower(key($module));

				if (method_exists($this, 'section_' . $section_key) && $module[$section_key]['status'] == 1) {
					if (version_compare(VERSION, '2.0') < 0) {
						$section = $this->getChild('module/success_page/section_' . $section_key, array('setting' => $module[$section_key], 'language_id' => $this->config->get('config_language_id'), 'order_info' => $order_info));
					} else {
						$section = $this->load->controller('module/success_page/section_' . $section_key, array('setting' => $module[$section_key], 'language_id' => $this->config->get('config_language_id'), 'order_info' => $order_info));
					}

					if ($section) {
						$data['sections'][] = $section;
					}
				}
			}

			return $this->toOutput('default/template/module/success_page.tpl', $data);
		}
	}

	public function section_text($setting) {
		$data['setting'] = $setting['setting'];

		$coupon = '';

		if ($this->config->get('success_page_coupon_id')) {
			$coupon_query = $this->db->query("SELECT code FROM `" . DB_PREFIX . "coupon` WHERE coupon_id = '" . (int)$this->config->get('success_page_coupon_id') . "' AND status = '1'");

			if ($coupon_query->num_rows) {
				$coupon = $coupon_query->row['code'];
			}
		}

		$find = array(
			'{order_id}',
			'{invoice_id}',
			'{total}',
			'{client}',
			'{email}',
			'{customer_id}',
			'{coupon}'
		);

		$replace = array(
			'order_id'    => $setting['order_info']['order_id'],
			'invoice_id'  => $setting['order_info']['invoice_prefix'] . $setting['order_info']['invoice_no'],
			'total'       => $setting['order_info']['total'],
			'client'      => $setting['order_info']['firstname'] . ' ' . $setting['order_info']['lastname'],
			'email'       => $setting['order_info']['email'],
			'customer_id' => $setting['order_info']['customer_id'],
			'coupon'      => $coupon
		);

		$description = isset($setting['setting'][$setting['language_id']]['description']) ? $setting['setting'][$setting['language_id']]['description'] : '';
		$data['description'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $description))));

		if ($setting['setting']['logged_status'] && !$this->customer->isLogged()) {
			return '';
		}

		return $this->toOutput('default/template/module/success_page/section_text.tpl', $data);
	}

	public function section_social($setting) {
		$data['setting'] = $setting['setting'];
		$data['language_id'] = $setting['language_id'];

		return $this->toOutput('default/template/module/success_page/section_social.tpl', $data);
	}

	public function section_address($setting) {
		$data['setting'] = $setting['setting'];
		$data['language_id'] = $setting['language_id'];

		if ($setting['order_info']['payment_address_format']) {
			$format = $setting['order_info']['payment_address_format'];
		} else {
			$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
		}

		$find = array(
			'{firstname}',
			'{lastname}',
			'{company}',
			'{address_1}',
			'{address_2}',
			'{city}',
			'{postcode}',
			'{zone}',
			'{zone_code}',
			'{country}'
		);

		$replace = array(
			'firstname' => $setting['order_info']['payment_firstname'],
			'lastname'  => $setting['order_info']['payment_lastname'],
			'company'   => $setting['order_info']['payment_company'],
			'address_1' => $setting['order_info']['payment_address_1'],
			'address_2' => $setting['order_info']['payment_address_2'],
			'city'      => $setting['order_info']['payment_city'],
			'postcode'  => $setting['order_info']['payment_postcode'],
			'zone'      => $setting['order_info']['payment_zone'],
			'zone_code' => $setting['order_info']['payment_zone_code'],
			'country'   => $setting['order_info']['payment_country']
		);

		$data['payment'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

		if ($setting['order_info']['shipping_address_format']) {
			$format = $setting['order_info']['shipping_address_format'];
		} else {
			$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
		}

		$find = array(
			'{firstname}',
			'{lastname}',
			'{company}',
			'{address_1}',
			'{address_2}',
			'{city}',
			'{postcode}',
			'{zone}',
			'{zone_code}',
			'{country}'
		);

		$replace = array(
			'firstname' => $setting['order_info']['shipping_firstname'],
			'lastname'  => $setting['order_info']['shipping_lastname'],
			'company'   => $setting['order_info']['shipping_company'],
			'address_1' => $setting['order_info']['shipping_address_1'],
			'address_2' => $setting['order_info']['shipping_address_2'],
			'city'      => $setting['order_info']['shipping_city'],
			'postcode'  => $setting['order_info']['shipping_postcode'],
			'zone'      => $setting['order_info']['shipping_zone'],
			'zone_code' => $setting['order_info']['shipping_zone_code'],
			'country'   => $setting['order_info']['shipping_country']  
		);

		$data['shipping'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

		return $this->toOutput('default/template/module/success_page/section_address.tpl', $data);
	}

	public function section_comment($setting) {
		$data['setting'] = $setting['setting'];
		$data['language_id'] = $setting['language_id'];
		$data['order_info'] = $setting['order_info'];

		if (!$setting['order_info']['comment']) {
			return '';
		}

		return $this->toOutput('default/template/module/success_page/section_comment.tpl', $data);
	}

	public function section_bank_transfer($setting) {
		$data['setting'] = $setting['setting'];
		$data['language_id'] = $setting['language_id'];

		if ($setting['setting']['instruction'] && (!isset($setting['order_info']['payment_code']) || $setting['order_info']['payment_code'] != 'bank_transfer')) {
			return '';
		}

		$data['instruction'] = nl2br($this->config->get('bank_transfer_bank' . $setting['language_id']));

		return $this->toOutput('default/template/module/success_page/section_bank_transfer.tpl', $data);
	}

	public function section_payment($setting) {
		$data['setting'] = $setting['setting'];
		$data['language_id'] = $setting['language_id'];

		if (!isset($setting['order_info']['payment_code']) || !isset($setting['setting'][$setting['order_info']['payment_code']]) || !isset($setting['setting'][$setting['order_info']['payment_code']][$setting['language_id']]['description'])) {
			return '';
		}

		if (!$setting['setting'][$setting['order_info']['payment_code']][$setting['language_id']]['description']) {
			return '';
		}

		$description = $setting['setting'][$setting['order_info']['payment_code']][$setting['language_id']]['description'];

		$find = array(
			'{order_id}',
			'{invoice_id}',
			'{total}',
			'{client}',
			'{email}',
			'{customer_id}'
		);

		$replace = array(
			'order_id'    => $setting['order_info']['order_id'],
			'invoice_id'  => $setting['order_info']['invoice_prefix'] . $setting['order_info']['invoice_no'],
			'total'       => $setting['order_info']['total'],
			'client'      => $setting['order_info']['firstname'] . ' ' . $setting['order_info']['lastname'],
			'email'       => $setting['order_info']['email'],
			'customer_id' => $setting['order_info']['customer_id']
		);

		$data['description'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $description))));

		return $this->toOutput('default/template/module/success_page/section_payment.tpl', $data);
	}

	public function section_cart($setting) {
		$data['setting'] = $setting['setting'];
		$data['language_id'] = $setting['language_id'];
		$data['order_info'] = $setting['order_info'];

		$this->load->model('account/order');
		$this->load->model('catalog/product');
		$this->load->model('tool/image');

		if (version_compare(VERSION, '2.0') > 0) {
			$this->load->model('tool/upload');
		}

		$data['products'] = array();

		$products = $this->model_account_order->getOrderProducts($setting['order_info']['order_id']);

		foreach ($products as $product) {
			$option_data = array();

			$options = $this->model_account_order->getOrderOptions($setting['order_info']['order_id'], $product['order_product_id']);

			foreach ($options as $option) {
				if ($option['type'] != 'file') {
					$value = $option['value'];
				} else {
					if (version_compare(VERSION, '2.0') < 0) {
						$value = $option['value'];
					} else {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

						if ($upload_info) {
							$value = $upload_info['name'];
						} else {
							$value = '';
						}
					}
				}

				$option_data[] = array(
					'name'  => $option['name'],
					'value' => $value
				);
			}

			$product_info = $this->model_catalog_product->getProduct($product['product_id']);

			$image = '';
			$sku = '';

			if ($product_info) {
				if ($product_info['image']) {
					$image = $this->model_tool_image->resize($product_info['image'], (int)$setting['setting']['width'], (int)$setting['setting']['height']);
				}

				$sku = $product_info['sku'];
			}

			$data['products'][] = array(
				'thumb'    => $image,
				'name'     => $product['name'],
				'model'    => $product['model'],
				'option'   => $option_data,
				'quantity' => $product['quantity'],
				'sku'      => $sku,
				'price'    => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $setting['order_info']['currency_code'], $setting['order_info']['currency_value']),
				'total'    => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $setting['order_info']['currency_code'], $setting['order_info']['currency_value'])
			);
		}

		$data['vouchers'] = array();

		$vouchers = $this->model_account_order->getOrderVouchers($setting['order_info']['order_id']);

		foreach ($vouchers as $voucher) {
			$data['vouchers'][] = array(
				'description' => $voucher['description'],
				'amount'      => $this->currency->format($voucher['amount'], $setting['order_info']['currency_code'], $setting['order_info']['currency_value'])
			);
		}

		$data['totals'] = array();

		$totals = $this->model_account_order->getOrderTotals($setting['order_info']['order_id']);

		foreach ($totals as $total) {
			$data['totals'][] = array(
				'title' => $total['title'],
				'text'  => $this->currency->format($total['value'], $setting['order_info']['currency_code'], $setting['order_info']['currency_value']),
			);
		}

		return $this->toOutput('default/template/module/success_page/section_cart.tpl', $data);
	}

	public function section_order($setting) {
		$data['setting'] = $setting['setting'];
		$data['language_id'] = $setting['language_id'];
		$data['order_info'] = $setting['order_info'];

		return $this->toOutput('default/template/module/success_page/section_order.tpl', $data);
	}

	public function section_product($setting) {
		$data['setting'] = $setting['setting'];
		$data['language_id'] = $setting['language_id'];

		$data['products'] = array();

		if ($setting['setting']['product']) {
			$this->load->model('catalog/product');
			$this->load->model('tool/image');

			foreach ($setting['setting']['product'] as $product_id) {
				$product_info = $this->model_catalog_product->getProduct($product_id);

				if ($product_info) {
					if ($product_info['image']) {
						$image = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
					} else {
						if (version_compare(VERSION, '2.0') < 0) {
							$image = $this->model_tool_image->resize('no_image.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
						} else {
							$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
						}
					}

					if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$price = false;
					}

					if ((float)$product_info['special']) {
						$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$special = false;
					}

					if ($this->config->get('config_tax')) {
						$tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
					} else {
						$tax = false;
					}

					if ($this->config->get('config_review_status')) {
						$rating = $product_info['rating'];
					} else {
						$rating = false;
					}
					
					$data['products'][] = array(
						'product_id'  => $product_info['product_id'],
						'thumb'       => $image,
						'name'        => $product_info['name'],
						'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, 100) . '..',
						'price'       => $price,
						'special'     => $special,
						'tax'         => $tax,
						'rating'      => $rating,
						'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
					);
				}
			}
		}

		return $this->toOutput('default/template/module/success_page/section_product.tpl', $data);
	}

	public function section_facebook($setting) {
		$data['setting'] = $setting['setting'];
		$data['language_id'] = $setting['language_id'];

		return $this->toOutput('default/template/module/success_page/section_facebook.tpl', $data);
	}

	public function section_youtube($setting) {
		$data['setting'] = $setting['setting'];
		$data['language_id'] = $setting['language_id'];

		return $this->toOutput('default/template/module/success_page/section_youtube.tpl', $data);
	}

	private function createInvoiceNo($order_info) {
		$query = $this->db->query("SELECT MAX(invoice_no) AS invoice_no FROM `" . DB_PREFIX . "order` WHERE invoice_prefix = '" . $this->db->escape($order_info['invoice_prefix']) . "'");

		if ($query->row['invoice_no']) {
			$invoice_no = $query->row['invoice_no'] + 1;
		} else {
			$invoice_no = 1;
		}

		$this->db->query("UPDATE `" . DB_PREFIX . "order` SET invoice_no = '" . (int)$invoice_no . "', invoice_prefix = '" . $this->db->escape($order_info['invoice_prefix']) . "' WHERE order_id = '" . (int)$order_info['order_id'] . "'");
	}

	private function toOutput($file, $data) {
		if (version_compare(VERSION, '2.0') < 0) {
			$this->data = $data;

			$this->template = $file;

			return $this->render();
		} else {
			return $this->load->view($file, $data);
		}
	}
}
?>