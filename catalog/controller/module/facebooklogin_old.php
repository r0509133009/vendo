<?php  
class ControllerModuleFacebooklogin extends Controller {
	protected function index($config) {
		$this->language->load('module/facebooklogin');

      	$this->data['heading_title'] = $this->language->get('heading_title');
		
      	$login_ssl = isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'));
      	
		if(!$this->customer->isLogged()){
			if ($login_ssl) {
				$configuration = str_replace('http', 'https', $this->config->get('FacebookLogin'));
			} else {
				$configuration = $this->config->get('FacebookLogin');
			}
			
			$this->data['data']['FacebookLogin'] = $configuration[$this->config->get('config_store_id')];
			$this->data['data']['FacebookLoginConfig'] = $config;

			if (!empty($this->data['data']['FacebookLogin']['Enabled']) && $this->data['data']['FacebookLogin']['Enabled'] == 'Yes') {

				$this->data['url_login'] = $this->url->link('module/facebooklogin/display', '', $login_ssl ? 'SSL' : 'NONSSL');
				
				if (file_exists('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/facebooklogin.css')) {
					$this->document->addStyle('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/facebooklogin.css');
				} else {
					$this->document->addStyle('catalog/view/theme/default/stylesheet/facebooklogin.css');
				}
				
				if(!isset($this->data['data']['FacebookLogin']['ButtonName_'.$this->config->get('config_language')])){
					$this->data['data']['FacebookLogin']['ButtonLabel'] = 'Login with Facebook';
				} else {
					$this->data['data']['FacebookLogin']['ButtonLabel'] = $this->data['data']['FacebookLogin']['ButtonName_'.$this->config->get('config_language')];
				}
				
				if(!isset($this->data['data']['FacebookLogin']['WrapperTitle_'.$this->config->get('config_language')])){
					$this->data['data']['FacebookLogin']['WrapperTitle'] = 'Login';
				} else {
					$this->data['data']['FacebookLogin']['WrapperTitle'] = $this->data['data']['FacebookLogin']['WrapperTitle_'.$this->config->get('config_language')];
				}
	
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/facebooklogin.tpl')) {
					$this->template = $this->config->get('config_template') . '/template/module/facebooklogin.tpl';
				} else {
					$this->template = 'default/template/module/facebooklogin.tpl';
				}
	
				$this->render();
			}
		}
	}
	
	public function display() {		
      	$login_ssl = isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'));

		if ($login_ssl) {
			$configuration = str_replace('http', 'https', $this->config->get('FacebookLogin'));
		} else {
			$configuration = $this->config->get('FacebookLogin');
		}
		
		$this->data['data']['FacebookLogin'] = $configuration[$this->config->get('config_store_id')];
		
		if (!class_exists('Facebook\Facebook')) {	
			require_once(DIR_SYSTEM . '../vendors/facebook-php-sdk-v4/autoload.php');
		}

		$fb = new Facebook\Facebook(array(
			'app_id' => $this->data['data']['FacebookLogin']['APIKey'],
			'app_secret' => $this->data['data']['FacebookLogin']['APISecret'],
			'default_graph_version' => 'v2.2',
		));

		$helper = $fb->getRedirectLoginHelper();

		$permissions = array(
			'email',
			'user_birthday'
		);
		
		if (!empty($this->session->data['facebooklogin_redirect'])) {
			$redirect_url = $this->session->data['facebooklogin_redirect'];
		} else if (!empty($this->request->server['HTTP_REFERER'])) {
			$redirect_url = $this->request->server['HTTP_REFERER'];
		} else {
			$redirect_url = '';
		}

		$redirect_url = htmlspecialchars_decode($redirect_url);

		unset($this->session->data['facebooklogin_redirect']);

		$loginUrl = $helper->getLoginUrl(str_replace('&amp;', '&', $this->url->link('account/facebooklogin', !empty($redirect_url) ? 'redirect=' . base64_encode($redirect_url) : '', $login_ssl ? 'SSL' : 'NONSSL')), $permissions);
		
		echo $loginUrl;
		exit;
	}
}
?>