<?php

/**
 * eBORICA payment gateway for Opencart by Extensa Web Development
 *
 * Copyright © 2011-2015 Extensa Web Development Ltd. All Rights Reserved.
 * This file may not be redistributed in whole or significant part.
 * This copyright notice MUST APPEAR in all copies of the script!
 *
 * @author 		Extensa Web Development Ltd. (www.extensadev.com)
 * @copyright	Copyright (c) 2011-2015, Extensa Web Development Ltd.
 * @package 	eBORICA payment gateway
 * @link		http://www.opencart.com/index.php?route=extension/extension/info&extension_id=3004
 */

class ControllerPaymentBorica extends Controller {
	protected function index() {
		$this->load->model('checkout/order');

		$this->language->load('payment/borica');

		$this->data['button_confirm'] = $this->language->get('button_confirm');

		//$this->data['return_url'] = HTTPS_SERVER . 'catalog/controller/payment/borica_callback.php';

		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);

		if ($order_info) {
			$message = '10'; //direct authorization
			$message .= date('YmdHis'); //current timestamp
			//$amount = round($this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], false) * 100); //eBORICA allows only BGN in version 1.0 and in stotinki; eBORICA allows only one currency in version 1.1 and in stotinki
			$amount = round($this->currency->format($order_info['total'], $this->config->get('borica_currency'), '', false) * 100); //eBORICA allows only BGN in version 1.0 and in stotinki; eBORICA allows only one currency in version 1.1 and in stotinki
			$message .= str_pad($amount, 12, '0', STR_PAD_LEFT);
			$message .= $this->config->get('borica_terminal'); //terminal id
			$message .= str_pad($this->session->data['order_id'], 15); //order id
			$message .= str_pad(mb_convert_encoding($this->config->get('borica_description'), 'Windows-1251', 'UTF-8'), 125); //short order description
			$message .= (strtolower($this->config->get('config_language')) == 'bg') ? 'BG' : 'EN'; //language - BG or EN
			$message .= '1.1'; //protocol version
			$message .= $this->config->get('borica_currency'); //only in protocol version 1.1
			if (!$this->config->get('borica_test')) {
				$privateKey = $this->config->get('borica_real_private_key');
			} else {
				$privateKey = $this->config->get('borica_test_private_key');
			}
			$pkeyid = openssl_get_privatekey($privateKey);
			openssl_sign($message, $signature, $pkeyid);
			openssl_free_key($pkeyid);
			$message .= $signature;

			$eBorica = urlencode(base64_encode($message));

			if (!$this->config->get('borica_test')) {
				$this->data['action'] = 'https://gate.borica.bg/boreps/registerTransaction?eBorica=' . $eBorica;
			} else {
				$this->data['action'] = 'https://gatet.borica.bg/boreps/registerTransaction?eBorica=' .  $eBorica;
			}

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/borica.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/payment/borica.tpl';
			} else {
				$this->template = 'default/template/payment/borica.tpl';
			}

			$this->render();
		}
	}

	public function callback() {
		$this->load->language('payment/borica');

		$success = false;
		$text_error = '';

		if (isset($this->request->get['eBorica'])) {
			$message = base64_decode($this->request->get['eBorica']);
			$response['trans_code'] = substr($message, 0, 2);
			$response['trans_time'] = substr($message, 2, 14);
			$response['amount'] = substr($message, 16, 12);
			$response['termid'] = substr($message, 28, 8);
			$response['orderid'] = substr($message, 36, 15);
			$response['resp_code'] = substr($message, 51, 2);
			$response['prot_ver'] = substr($message, 53, 3);
			$response['sign'] = substr($message, 56, 128);

			if (strpos($this->language->get('text_resp_code_' . $response['resp_code']), 'text_resp_code_') === false) {
				$text_error = $this->language->get('text_resp_code_' . $response['resp_code']);
			} else {
				$text_error = $this->language->get('error_code') . ' ' . $response['resp_code'];
			}

			if (!$this->config->get('borica_test')) {
				$certificate = $this->config->get('borica_real_certificate');
			} else {
				$certificate = $this->config->get('borica_test_certificate');
			}
			$pubkeyid = openssl_get_publickey($certificate);
			$response['signok'] = openssl_verify(substr($message, 0, strlen($message)-128), $response['sign'], $pubkeyid);
			openssl_free_key($pubkeyid);

			$this->load->model('checkout/order');

			$order_info = $this->model_checkout_order->getOrder($response['orderid']);

			if ($order_info) {
				$amount = round($this->currency->format($order_info['total'], $this->config->get('borica_currency'), '', false) * 100); //eBORICA allows only BGN in version 1.0 and in stotinki; eBORICA allows only one currency in version 1.1 and in stotinki

				if ($response['signok'] && ($response['termid'] == $this->config->get('borica_terminal')) && ($response['amount'] == str_pad($amount, 12, '0', STR_PAD_LEFT))) {
					if ($response['resp_code'] == '00') {
						$order_status_id = $this->config->get('borica_order_status_id');

						$success = true;
					} elseif ($response['resp_code'] == '94') {
						$order_status_id = $this->config->get('borica_order_status_canceled_id');
					} elseif ($response['resp_code'] == '85') {
						$order_status_id = $this->config->get('borica_order_status_canceled_reversal_id');
					} elseif ($response['resp_code'] == '86' || $response['resp_code'] == '90' || $response['resp_code'] == '93' || $response['resp_code'] == '95') {
						$order_status_id = $this->config->get('borica_order_status_denied_id');
					} else {
						$order_status_id = $this->config->get('borica_order_status_failed_id');
					}
				} else {
					$order_status_id = $this->config->get('config_order_status_id');

					if (!$response['signok']) {
						$text_error = $this->language->get('error_sign');
					} elseif ($response['termid'] != $this->config->get('borica_terminal')) {
						$text_error = $this->language->get('error_terminal');
					} else {
						$text_error = $this->language->get('error_amount');
					}
				}

				if (!$order_info['order_status_id']) {
					$this->model_checkout_order->confirm($response['orderid'], $order_status_id);
				} else {
					$this->model_checkout_order->update($response['orderid'], $order_status_id);
				}
			} else {
				$text_error = $this->language->get('error_order');
			}
		}

		if ($success) {
			$this->redirect($this->url->link('checkout/success'));
		} else {
			//$this->redirect($this->url->link('checkout/checkout', '', 'SSL'));

			$this->document->setTitle($this->language->get('text_title'));

			$this->data['heading_title'] = $this->language->get('text_failed');
			$this->data['text_message'] = sprintf($this->language->get('text_failed_message'), $text_error);
			$this->data['button_continue'] = $this->language->get('button_continue');

			$this->data['continue'] = $this->url->link('checkout/checkout', '', 'SSL');

			$this->data['breadcrumbs'] = array();

			$this->data['breadcrumbs'][] = array(
				'href'      => $this->url->link('common/home'),
				'text'      => $this->language->get('text_home'),
				'separator' => false
			);

			$this->data['breadcrumbs'][] = array(
				'href'      => $this->url->link('checkout/cart'),
				'text'      => $this->language->get('text_basket'),
				'separator' => $this->language->get('text_separator')
			);

			$this->data['breadcrumbs'][] = array(
				'href'      => $this->url->link('checkout/checkout', '', 'SSL'),
				'text'      => $this->language->get('text_checkout'),
				'separator' => $this->language->get('text_separator')
			);

			$this->data['breadcrumbs'][] = array(
				'href'      => $this->url->link('payment/borica/callback'),
				'text'      => $this->language->get('text_success'),
				'separator' => $this->language->get('text_separator')
			);

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/success.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/common/success.tpl';
			} else {
				$this->template = 'default/template/common/success.tpl';
			}

			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'
			);

			$this->response->setOutput($this->render());
		}
	}
}
?>