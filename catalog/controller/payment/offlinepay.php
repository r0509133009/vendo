<?php
class ControllerPaymentOfflinepay extends Controller {
	protected function index() {
	    $this->language->load('payment/offlinepay');
    	$this->data['button_confirm'] = $this->language->get('button_confirm');
		$this->data['continue'] = $this->url->link('checkout/success');
		
		$this->data['offlinepay_logo'] = $this->config->get('offlinepay_logo');
		$this->data['text_title'] = $this->language->get('text_title');
		$this->data['offlinepay_message'] = $this->config->get('offlinepay_message');
		$this->data['offlinepay_condition'] = $this->config->get('offlinepay_condition');
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/offlinepay.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/payment/offlinepay.tpl';
		} else {
			$this->template = 'default/template/payment/offlinepay.tpl';
		}	
		
		$this->render();
	}
	
	public function confirm() {
		$this->language->load('payment/offlinepay');
		
		$this->load->model('checkout/order');
				
		$this->model_checkout_order->confirm($this->session->data['order_id'], $this->config->get('offlinepay_order_status_id'), $comment, true);
	}

	public function getDue() {
	
		$orderResponse = array();
		if (isset($this->request->get['order_id']) && is_numeric($this->request->get['order_id'])) {
			$this->load->model('checkout/order');
			$order_id = $this->request->get['order_id'];
			$order = $this->model_checkout_order->getOrder($order_id);
			if ($this->validate($order)) {
				$orderResponse['total'] = $this->currency->convert($order['total'], $order['currency_code'], 'BGN' );
				$orderResponse['customer'] = $order['firstname'] . ' ' . $order['lastname'];
				$orderResponse['telephone'] = $order['telephone'];
			}
		}

		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($orderResponse));
	}


	public function pay() {
		$response = array('status'=> 1);
		if (!empty($this->request->post['amount'])
			&& !empty($this->request->post['order_id']) 
			&& !empty($this->request->post['pay_date']) 
			&& !empty($this->request->post['transaction_id'])
		) {
			$paidAmount = $this->request->post['amount'];
			$orderId = $this->request->post['order_id'];
			$payDate = $this->request->post['pay_date'];
			$transactionId = $this->request->post['transaction_id'];
			$this->load->model('checkout/order');
			$order = $this->model_checkout_order->getOrder($orderId);
			if ($this->validate($order)) {
				$this->load->model('payment/offlinepay');
				if(!$this->model_payment_offlinepay->isExists($transactionId)) {
					try {
						$result = $this->model_payment_offlinepay->pay($transactionId, $orderId, $payDate, $paidAmount);
						if ($result) {
							$this->model_checkout_order->update($orderId, $this->config->get('offlinepay_order_paid_status_id'), "", false);
							$response['status'] = 0;
						}
					}catch(Exception $e) {
						$response['error'] = "There has been an erorr processing your request";
					}			
				}else{
					$response['status'] = 0;
				}
			}
		}else{
			$response['error'] = "Method is not a POST or some params are missed";
		}
		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($response));
	}

	protected function validate($order)
	{
		return $order && $order['order_status_id'] && $order['payment_code'] == 'offlinepay';
	}
}
?>