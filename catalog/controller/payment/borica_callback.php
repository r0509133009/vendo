<?php

/**
 * eBORICA payment gateway for Opencart by Extensa Web Development
 *
 * Copyright © 2011-2015 Extensa Web Development Ltd. All Rights Reserved.
 * This file may not be redistributed in whole or significant part.
 * This copyright notice MUST APPEAR in all copies of the script!
 *
 * @author 		Extensa Web Development Ltd. (www.extensadev.com)
 * @copyright	Copyright (c) 2011-2015, Extensa Web Development Ltd.
 * @package 	eBORICA payment gateway
 * @link		http://www.opencart.com/index.php?route=extension/extension/info&extension_id=3004
 */

header('Location: ' . str_ireplace('catalog/controller/payment/borica_callback.php?', 'index.php?route=payment/borica/callback&', $_SERVER['REQUEST_URI']));
//header('Location: ' . substr($_SERVER['REQUEST_URI'], 0, strrpos($_SERVER['REQUEST_URI'], 'catalog/controller/payment/borica_callback.php')) . 'index.php?route=payment/borica/callback&eBorica=' . $_GET['eBorica']);
exit;

?>