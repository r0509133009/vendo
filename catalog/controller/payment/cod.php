<?php
class ControllerPaymentCod extends Controller {
	protected function index() {
		$this->data['button_confirm'] = $this->language->get('button_confirm');

		$this->data['continue'] = $this->url->link('checkout/success');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/cod.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/payment/cod.tpl';
		} else {
			$this->template = 'default/template/payment/cod.tpl';
		}

		$this->render();
	}

	public function confirm() {
		$this->load->model('checkout/order');
		$this->language->load('payment/cod');
		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
		$order_total = $this->currency->format($order_info['total'],$order_info['currency_code'],$order_info['currency_value']);

		$find = array(
				'{{order_total}}',
				'{{order_id}}'
		);

		$replace = array(
			$order_total,
			$this->session->data['order_id']			
		);
		
		$instruction = $this->config->get('cod_instruction_' . $this->config->get('config_language_id'));
		if (!empty($instruction)) {
			$instruction = str_replace($find, $replace, $instruction). "\n\n";
			$instruction .= $this->language->get('text_offices'). "\n\n";
		}else{
			$instruction = '';
		}

		$this->model_checkout_order->confirm($this->session->data['order_id'], $this->config->get('cod_order_status_id'),$instruction, true);
	}
}
?>