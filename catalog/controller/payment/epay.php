<?php

/**
 * Epay.bg payment gateway for Opencart by Extensa Web Development
 *
 * Copyright � 2010-2015 Extensa Web Development Ltd. All Rights Reserved.
 * This file may not be redistributed in whole or significant part.
 * This copyright notice MUST APPEAR in all copies of the script!
 *
 * @author 		Extensa Web Development Ltd. (www.extensadev.com)
 * @copyright	Copyright (c) 2010-2015, Extensa Web Development Ltd.
 * @package 	Epay.bg payment gateway
 * @link		http://www.opencart.com/index.php?route=extension/extension/info&extension_id=2999
 */

class ControllerPaymentEpay extends Controller {
	protected function index() {
		$this->load->model('checkout/order');

		$this->language->load('payment/epay');

		$this->data['button_confirm'] = $this->language->get('button_confirm');

		if (!$this->config->get('epay_test')) {
			$this->data['action'] = 'https://www.epay.bg/';
		} else {
			$this->data['action'] = 'https://demo.epay.bg/';
		}

		if (strtolower($this->config->get('config_language')) == 'bg') {
			$this->data['lang'] = 'bg';
		} else {
			$this->data['action'] .= 'en/';
			$this->data['lang'] = 'en';
		}

		$this->data['url_cancel'] = $this->url->link('checkout/checkout', '', 'SSL');
		//$this->data['url_return'] = $this->url->link('payment/epay/callback');
		$this->data['url_ok'] = $this->url->link('checkout/success');

		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);

		if ($order_info) {
			if (strlen(utf8_decode($this->config->get('epay_client_number'))) > 0) {
				$min_email = 'MIN=' . $this->config->get('epay_client_number');
			} elseif (strlen(utf8_decode($this->config->get('epay_email'))) > 0) {
				$min_email = 'EMAIL=' . $this->config->get('epay_email');
			}
			$secret = $this->config->get('epay_secret_key');
			$this->data['invoice'] = $this->session->data['order_id'];
			//$this->data['sum'] = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], false);
			$this->data['sum'] = $this->currency->format($order_info['total'], 'BGN', '', false); //ePay.bg allows only BGN
			$this->data['exp_date'] = date('d.m.Y', mktime(0, 0, 0, date('m'), date('d') + $this->config->get('epay_expired_days'), date('Y')));
			$this->data['description'] = $this->config->get('epay_description');
			$data = <<<DATA
{$min_email}
INVOICE={$this->data['invoice']}
AMOUNT={$this->data['sum']}
EXP_TIME={$this->data['exp_date']}
DESCR={$this->data['description']}
ENCODING=utf-8
DATA;

			$this->data['encoded'] = base64_encode($data);
			$this->data['checksum'] = $this->hmac('sha1', $this->data['encoded'], $secret);

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/epay.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/payment/epay.tpl';
			} else {
				$this->template = 'default/template/payment/epay.tpl';
			}

			$this->render();
		}
	}

	public function callback() {
		if (isset($this->request->post['encoded'])) {
			$encoded  = $this->request->post['encoded'];
		} else {
			$encoded  = '';
		}

		if (isset($this->request->post['checksum'])) {
			$checksum = $this->request->post['checksum'];
		} else {
			$checksum = '';
		}

		$secret = $this->config->get('epay_secret_key');
		$hmac = $this->hmac('sha1', $encoded, $secret);

		if ($hmac == $checksum) {
			$data = base64_decode($encoded);
			$lines_arr = explode("\n", $data);
			$info_data = '';

			foreach ($lines_arr as $line) {
				if (preg_match("/^INVOICE=(\d+):STATUS=(PAID|DENIED|EXPIRED)(:PAY_TIME=(\d+):STAN=(\d+):BCODE=([0-9a-zA-Z]+))?$/", $line, $regs)) {
					$invoice  = $regs[1];
					$status   = $regs[2];
					//$pay_date = $regs[4]; # XXX if PAID
					//$stan     = $regs[5]; # XXX if PAID
					//$bcode    = $regs[6]; # XXX if PAID

					if ($status == 'PAID' || $status == 'DENIED' || $status == 'EXPIRED') {
						$this->load->model('checkout/order');

						$order_id = $invoice;
						$order_info = $this->model_checkout_order->getOrder($order_id);

						if ($order_info) {
							if ($order_info['order_status_id'] > 0) {
								$easypay = true;
								$epay = false;
							} else {
								$easypay = false;
								$epay = true;
							}

							if ($status == 'PAID') {
								if ($easypay && $this->config->get('easypay_order_status_id')) {
									$order_status = $this->config->get('easypay_order_status_id');
								} else {
									$order_status = $this->config->get('epay_order_status_id');
								}
							} elseif ($status == 'DENIED') {
								if ($easypay && $this->config->get('easypay_order_status_denied_id')) {
									$order_status = $this->config->get('easypay_order_status_denied_id');
								} else {
									$order_status = $this->config->get('epay_order_status_denied_id');
								}
							} elseif ($status == 'EXPIRED') {
								if ($easypay && $this->config->get('easypay_order_status_expired_id')) {
									$order_status = $this->config->get('easypay_order_status_expired_id');
								} else {
									$order_status = $this->config->get('epay_order_status_expired_id');
								}
							} else {
								$order_status = 0;
							}

							if ($order_status && $order_info['order_status_id'] != $order_status) {
								if ($easypay) {
									$this->model_checkout_order->update($order_id, $order_status);
								} else {
									$this->model_checkout_order->confirm($order_id, $order_status);
								}
							}

							$info_data .= "INVOICE=$invoice:STATUS=OK\n";
						} else {
							$info_data .= "INVOICE=$invoice:STATUS=NO\n";
						}
					} else {
						$info_data .= "INVOICE=$invoice:STATUS=ERR\n";
					}
				}
			}

			echo $info_data, "\n";
		} else {
			echo "ERR=Not valid CHECKSUM\n";
		}
		exit;
	}

	private function hmac($algo, $data, $passwd){
		/* md5 and sha1 only */
		$algo=strtolower($algo);
		$p=array('md5'=>'H32','sha1'=>'H40');
		if(strlen($passwd)>64) $passwd=pack($p[$algo],$algo($passwd));
		if(strlen($passwd)<64) $passwd=str_pad($passwd,64,chr(0));

		$ipad=substr($passwd,0,64) ^ str_repeat(chr(0x36),64);
		$opad=substr($passwd,0,64) ^ str_repeat(chr(0x5C),64);

		return($algo($opad.pack($p[$algo],$algo($ipad.$data))));
	}
}
?>