<?php
$config['statuses'] = array();
// OPENCART
$config['statuses']['opencart'] = array();
$config['statuses']['opencart']['status_5'] = '<span style="color:red;">Out Of Stock</span>';
$config['statuses']['opencart']['status_6'] = '<span style="color:red;">2-3 days Out Of Stock</span>';
$config['statuses']['opencart']['status_7'] = '<span style="color:green;">In Stock</span>';
$config['statuses']['opencart']['status_8'] = "Pre-Order";
$config['statuses']['opencart']['status_9'] = '<span style="color:green;">In Stock</span>';

// X-Cart
$config['statuses']['xcart'] = array();
$config['statuses']['xcart']['status_0'] = '<span style="color:red;">Disabled</span>';
$config['statuses']['xcart']['status_1'] = '<span style="color:green;">Enabled</span>';


// PrestaShop ( формируется в cms_createStatusById($prod_id) )
$config['statuses']['xcart'] = array();


