-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 14, 2015 at 11:39 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `opencart`
--

-- --------------------------------------------------------

--
-- Table structure for table `oc_affiliate_size`
--

CREATE TABLE IF NOT EXISTS `oc_affiliate_size` (
  `size_id` int(11) NOT NULL AUTO_INCREMENT,
  `size_name` varchar(20) NOT NULL,
  `size_des` varchar(155) NOT NULL,
  `size_status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`size_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `oc_affiliate_size`
--

INSERT INTO `oc_affiliate_size` (`size_id`, `size_name`, `size_des`, `size_status`) VALUES
(1, '300x250', '', 1),
(2, '300x600', '', 1),
(3, '336x280', '', 1),
(4, '120x600', '', 1),
(5, '160x600', '', 1),
(6, '250x250', '', 1),
(7, '125x125', '', 1),
(8, '468x60', '', 1),
(9, '728x90', '', 1),
(10, '200x200', '', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
