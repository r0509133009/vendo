-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 14, 2015 at 11:39 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `opencart`
--

-- --------------------------------------------------------

--
-- Table structure for table `oc_affiliate_bn`
--

CREATE TABLE IF NOT EXISTS `oc_affiliate_bn` (
  `bn_id` int(11) NOT NULL AUTO_INCREMENT,
  `bn_proid` int(11) NOT NULL,
  `bn_camid` int(11) NOT NULL,
  `bn_name` varchar(255) NOT NULL,
  `bn_img` text NOT NULL,
  `bn_size` varchar(10) NOT NULL,
  `bn_des` varchar(255) NOT NULL,
  `bn_key` varchar(255) NOT NULL,
  `bn_device` varchar(255) NOT NULL,
  `bn_views` int(11) NOT NULL,
  `bn_click` int(11) NOT NULL DEFAULT '0',
  `bn_order` int(11) NOT NULL DEFAULT '0',
  `bn_create` varchar(20) NOT NULL,
  `bn_status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`bn_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
