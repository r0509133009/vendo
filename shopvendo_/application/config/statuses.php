<?php
$config['statuses'] = array();
// OPENCART
$config['statuses']['opencart'] = array();
$config['statuses']['opencart']['status_5'] = "Out Of Stock";
$config['statuses']['opencart']['status_6'] = "2-3 days Out Of Stock";
$config['statuses']['opencart']['status_7'] = "In Stock";
$config['statuses']['opencart']['status_8'] = "Pre-Order";
$config['statuses']['opencart']['status_9'] = "In Stock";