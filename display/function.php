<?php
error_reporting (E_ALL ^ E_NOTICE);
//OS home
$referer = $_SERVER["HTTP_REFERER"];
if($_SERVER['HTTP_USER_AGENT'] == 'Wada.vn Vietnamese Search'){
	$uag = "Search Wada.vn/2.0 Vietnamese";
} else {
	$uag = @$_SERVER['HTTP_USER_AGENT'];
}
function getOS() { 
    global $uag;
    $os_platform    =   "Desktop";
    $os_array       =   array(
							'/windows nt 6.4/i'     =>  'Desktop',
                            '/windows nt 6.3/i'     =>  'Desktop',
                            '/windows nt 6.2/i'     =>  'Desktop',
                            '/windows nt 6.1/i'     =>  'Desktop',
                            '/windows nt 6.0/i'     =>  'Desktop',
                            '/windows nt 5.2/i'     =>  'Desktop',
                            '/windows nt 5.1/i'     =>  'Desktop',
                            '/windows xp/i'         =>  'Desktop',
                            '/windows nt 5.0/i'     =>  'Desktop',
                            '/windows me/i'         =>  'Desktop',
                            '/win98/i'              =>  'Desktop',
                            '/win95/i'              =>  'Desktop',
                            '/win16/i'              =>  'Desktop',
                            '/macintosh|mac os x/i' =>  'Desktop',
                            '/mac_powerpc/i'        =>  'Desktop',
                            '/linux/i'              =>  'Desktop',
                            '/ubuntu/i'             =>  'Desktop',
                            '/iphone/i'             =>  'iPhone',
                            '/ipod/i'               =>  'iPhone',
                            '/ipad/i'               =>  'iPad',
                            '/android/i'            =>  'Android',
                            '/blackberry/i'         =>  'Android',
                            '/webos/i'              =>  'Android',
							'/windows phone/i'      =>  'Window Phone',
							'/Googlebot/i'			=>	'Bot',
							'/bingbot/i'			=>  'Bot',
							'/yahoobot/i'			=>  'Bot',
							'/Baiduspider/i'	    =>  'Bot',
							'/YandexBot/i'			=>  'Bot',
							'/facebookexternalhit/i' => 'Bot',
							'/XoviBot/i'			=> 'Bot',
							'/coccoc/i'				=> 'Bot',
							'/MJ12bot/i'			=> 'Bot',
							'/PycURL/i'				=> 'Bot',
							'/libcurl/i'			=> 'Bot',
							'/libidn/i'				=> 'Bot',
							'/GnuTLS/i'				=> 'Bot',
							'/zlib/i'				=> 'Bot',
							'/Wada.vn/i'			=> 'Bot',
							'NerdyBot'				=> 'Bot',
							'/Nokia203/i'			=> 'Android',
							'Google'				=> 'Bot',
							'/Google/i'				=> 'Bot',
							'ia_archiver'			=> 'Bot',
							'/ia_archiver/i'		=> 'Bot',
							'/Bot/i'				=> 'Bot',
							'Bot'					=> 'Bot',
							'/Apache-HttpClient/i'	=> 'Bot'
                        );
    foreach ($os_array as $regex => $value) { 
        if (@preg_match($regex, $uag)) {
            $os_platform    =   $value;
        }
    }
    return $os_platform;
}
$os = getOS();

//String random 32
function RandomString($length) {
	$keys = array_merge(range(0,9), range('A', 'Z'), range('a', 'z'));
		for($i=0; $i < $length; $i++) {
		$key .= @$keys[array_rand($keys)];
	}
	return @$key;
}
//Check page
function getNoneSslPage($URL){
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      curl_setopt($ch, CURLOPT_URL, $URL);
      $data = curl_exec($ch);
      curl_close($ch);
      return $data;
}
function getSslPage($URL) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_URL, $URL);
    curl_setopt($ch, CURLOPT_REFERER, $URL);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}
?>