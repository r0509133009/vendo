<?php exit(0); ?> { 
"settings":
{
	"data_settings" : 
	{
		"save_database" : 
		{
			"database" : "",
			"is_present" : false,
			"password" : "",
			"port" : 3306,
			"server" : "",
			"tablename" : "",
			"username" : ""
		},
		"save_file" : 
		{
			"filename" : "form-results.csv",
			"is_present" : false
		},
		"save_sqlite" : 
		{
			"database" : "WHOLESALE.dat",
			"is_present" : false,
			"tablename" : "WHOLESALE"
		}
	},
	"email_settings" : 
	{
		"auto_response_message" : 
		{
			"custom" : 
			{
				"body" : "",
				"is_present" : true,
				"subject" : "Thank you for your submission"
			},
			"from" : "antistarorders@gmail.com",
			"is_present" : false,
			"to" : ""
		},
		"notification_message" : 
		{
			"bcc" : "",
			"cc" : "",
			"custom" : 
			{
				"body" : "имена на лицето за контакти:::[Name]\nтелефон:::[Tel]\nE-mail:::[Email]\nиме на фирма:::[Company_Name]\nиме на магазин:::[Shop_name]\nадрес на уеб сайта:::[web_site]\nгодина на създаване:::[year_of_foundation]\nще продава ли на сайта си:::[selling_on_website]\nадрес на магазина:::[Adres]\nадрес 2 на магазина:::[Adres_2]\nпощенски код:::[post_code]\nград:::[City]\nвидове магазин:::[vidove_magazin]\nкратко описание на магазина:::[Kratko_opisanie_magazin]\n5 други марки които продава:::[pet_drugi_marki]\nдруги видове продукти:::[drugi_vidove_produkti]\nописание на клиентите:::[opisanie_klienti]\nIP:::[_fromaddress_]",
				"is_present" : true,
				"subject" : "Заявка за търговец на едро!"
			},
			"from" : "",
			"is_present" : true,
			"replyto" : "",
			"to" : "antistarorders@gmail.com"
		}
	},
	"general_settings" : 
	{
		"colorboxautoenabled" : false,
		"colorboxautotime" : 3,
		"colorboxenabled" : true,
		"colorboxname" : "Default",
		"formname" : "Форма за регистрация на търговец на едро",
		"is_appstore" : "0",
		"timezone" : "UTC"
	},
	"mailchimp" : 
	{
		"apiKey" : "",
		"lists" : []
	},
	"payment_settings" : 
	{
		"confirmpayment" : "<center>\n<style type=\"text/css\">\n#docContainer table {width:80%; margin-top: 5px; margin-bottom: 5px;}\n#docContainer td {text-align:right; min-width:25%; font-size: 12px !important; line-height: 20px;margin: 0px;border-bottom: 1px solid #e9e9e9; padding-right:5px;}\n#docContainer td:first-child {text-align:left; font-size: 13px !important; font-weight:bold; vertical-align:text-top; min-width:50%;}\n#docContainer th {font-size: 13px !important; font-weight:bold; vertical-align:text-top; text-align:right; padding-right:5px;}\n#docContainer th:first-child {text-align:left;}\n#docContainer tr:first-child {border-bottom-width:2px; border-bottom-style:solid;}\n#docContainer center {margin-bottom:15px;}\n#docContainer form input { margin:5px; }\n#docContainer #fb_confirm_inline { margin:5px; text-align:center;}\n#docContainer #fb_confirm_inline>center h2 { }\n#docContainer #fb_confirm_inline>center p { margin:5px; }\n#docContainer #fb_confirm_inline>center a { }\n#docContainer #fb_confirm_inline input { border:none; color:transparent; font-size:0px; background-color: transparent; background-repat: no-repeat; }\n#docContainer #fb_paypalwps { background: url('https://coffeecupimages.s3.amazonaws.com/paypal.gif');background-repeat:no-repeat; width:145px; height:42px; }\n#docContainer #fb_googlepay { background: url('https://coffeecupimages.s3.amazonaws.com/googlecheckout.gif'); background-repeat:no-repeat; width:168px; height:44px; }\n#docContainer #fb_authnet { background: url('https://coffeecupimages.s3.amazonaws.com/authnet.gif'); background-repeat:no-repeat; width:135px; height:38px; }\n#docContainer #fb_2checkout { background: url('https://coffeecupimages.s3.amazonaws.com/2co.png'); background-repeat:no-repeat; width:210px; height:44px; }\n#docContainer #fb_invoice { background: url('https://coffeecupimages.s3.amazonaws.com/btn_email.png'); background-repeat:no-repeat; width:102px; height:31px; }\n#docContainer #fb_invoice:hover { background: url('https://coffeecupimages.s3.amazonaws.com/btn_email_hov.png'); }\n#docContainer #fb_goback { color: inherit; }\n</style>\n[_cart_summary_]\n<h2>Almost done! </h2>\n<p>Your order will not be processed until you click the payment button below.</p>\n<a id=\"fb_goback\"href=\"?action=back\">Back to form</a></center>",
		"currencysymbol" : "$",
		"decimals" : 2,
		"fixedprice" : "000",
		"invoicelabel" : "",
		"is_present" : false,
		"paymenttype" : "redirect",
		"shopcurrency" : "USD",
		"usecustomsymbol" : false
	},
	"redirect_settings" : 
	{
		"confirmpage" : "<!DOCTYPE html>\n<html dir=\"ltr\" lang=\"en\">\n<head>\n<title>Success!</title>\n<meta charset=\"utf-8\">\n<style type=\"text/css\">\nbody {background: #f9f9f9;padding-left: 11%;padding-top: 7%; padding-right: 2%;max-width:700px;font-family: Helvetica, Arial;}\ntable{width:80%;}\np{font-size: 16px;font-weight: bold;color: #666;}\nh1{font-size: 60px !important;color: #ccc !important;margin:0px;}\nh2{font-size: 28px !important;color: #666 !important;margin: 0px; border-bottom: 1px dotted #00A2FF; padding-bottom:3px;}\nh3{font-size: 16px; color: #a1a1a1; border-top: 1px dotted #00A2FF; padding-top:1.7%; font-weight: bold;}\nh3 span{color: #ccc;}\ntd {font-size: 12px !important; line-height: 30px;  color: #666 !important; margin: 0px;border-bottom: 1px solid #e9e9e9;}\ntd:first-child {font-size: 13px !important; font-weight:bold; color: #333 !important; vertical-align:text-top; min-width:50%; padding-right:5px;}\na:link {color:#666; text-decoration:none;} a:visited {color:#666; text-decoration:none;} a:hover {color:#00A2FF;}\n</style>\n</head>\n<body>\n<h1>Thanks! </h1>\n<h2>The form is on its way.</h2>\n<p>Here&rsquo;s what was sent:</p>\n<div>[_form_results_]</div>\n<!-- link back to your Home Page -->\n<h3>Let&rsquo;s go <span> <a target=\"_blank\" href=\"http://www.coffeecup.com\">Back Home</a></span></h3>\n</body>\n</html>\n",
		"gotopage" : "",
		"inline" : "<center>\n<style type=\"text/css\">\n#docContainer table {margin-top: 30px; margin-bottom: 30px; width:80%;}\n#docContainer td {font-size: 12px !important; line-height: 30px;color: #666666 !important; margin: 0px;border-bottom: 1px solid #e9e9e9;}\n#docContainer td:first-child {font-size: 13px !important; font-weight:bold; color: #333 !important; vertical-align:text-top; min-width:50%; padding-right:5px;}\n</style>\n\n<h2>Благодарим ви за изявеното желание да продавате продуктите ANTISTAR!</h2><br/>\n<p>Ще се свържем с вас в най-кратък срок. Ето и копие от информацията която сте ни изпратили!</p>\n<p>[_form_results_] </p>\n<p><a href=\"http://antistar.me/\">ОБРАТНО КЪМ ANTISTAR.ME</a></p>\n</center>",
		"type" : "inline"
	},
	"uid" : "a5d09f2d01f8b4263f725f25adb8ab15",
	"validation_report" : "in_line"
},
"rules":{"name":{"label":"&#1048;&#1084;&#1077;","fieldtype":"text","required":true,"messages":"&#1052;&#1086;&#1083;&#1103; &#1087;&#1086;&#1087;&#1098;&#1083;&#1085;&#1077;&#1090;&#1077; &#1074;&#1072;&#1096;&#1077;&#1090;&#1086; &#1080;&#1084;&#1077;!"},"tel":{"label":"&#1058;&#1077;&#1083;&#1077;&#1092;&#1086;&#1085;","fieldtype":"text","messages":"&#1052;&#1086;&#1083;&#1103; &#1087;&#1086;&#1087;&#1098;&#1083;&#1085;&#1077;&#1090;&#1077; &#1074;&#1072;&#1096;&#1080;&#1103;&#1090; &#1090;&#1077;&#1083;&#1077;&#1092;&#1086;&#1085;&#1077;&#1085; &#1085;&#1086;&#1084;&#1077;&#1088;!","required":true},"email":{"label":"E-mail","fieldtype":"text","messages":"&#1052;&#1086;&#1083;&#1103; &#1087;&#1086;&#1087;&#1098;&#1083;&#1085;&#1077;&#1090;&#1077; &#1074;&#1072;&#1096;&#1080;&#1103;&#1090; E-mail &#1072;&#1076;&#1088;&#1077;&#1089;!","required":true},"company_name":{"label":"&#1048;&#1084;&#1077; &#1085;&#1072; &#1092;&#1080;&#1088;&#1084;&#1072;&#1090;&#1072;","fieldtype":"text","required":true,"messages":"&#1052;&#1086;&#1083;&#1103; &#1087;&#1086;&#1087;&#1098;&#1083;&#1085;&#1077;&#1090;&#1077; &#1080;&#1084;&#1077;&#1090;&#1086; &#1085;&#1072; &#1074;&#1072;&#1096;&#1072;&#1090;&#1072; &#1092;&#1080;&#1088;&#1084;&#1072; &#1087;&#1086; &#1088;&#1077;&#1075;&#1080;&#1089;&#1090;&#1088;&#1072;&#1094;&#1080;&#1103;!"},"shop_name":{"label":"&#1048;&#1084;&#1077; &#1085;&#1072; &#1084;&#1072;&#1075;&#1072;&#1079;&#1080;&#1085;&#1072;","fieldtype":"text","required":true,"messages":"&#1052;&#1086;&#1083;&#1103; &#1087;&#1086;&#1087;&#1098;&#1083;&#1085;&#1077;&#1090;&#1077; &#1080;&#1084;&#1077;&#1090;&#1086; &#1085;&#1072; &#1074;&#1072;&#1096;&#1080;&#1103; &#1084;&#1072;&#1075;&#1072;&#1079;&#1080;&#1085;!"},"web_site":{"label":"&#1072;&#1076;&#1088;&#1077;&#1089; &#1085;&#1072; &#1091;&#1077;&#1073; &#1089;&#1072;&#1081;&#1090;&#1072;","fieldtype":"text","required":true,"messages":"&#1052;&#1086;&#1083;&#1103; &#1087;&#1086;&#1087;&#1098;&#1083;&#1085;&#1077;&#1090;&#1077; &#1072;&#1076;&#1088;&#1077;&#1089;&#1072; &#1085;&#1072; &#1074;&#1072;&#1096;&#1080;&#1103;&#1090; &#1091;&#1077;&#1073; &#1089;&#1072;&#1081;&#1090;!"},"selling_on_website":{"label":"&#1065;&#1077; &#1087;&#1088;&#1086;&#1076;&#1072;&#1074;&#1072;&#1090;&#1077; &#1083;&#1080; &#1085;&#1072; &#1089;&#1072;&#1081;&#1090;&#1072; &#1089;&#1080;?","fieldtype":"dropdown","required":true,"messages":"&#1052;&#1086;&#1083;&#1103; &#1080;&#1079;&#1073;&#1077;&#1088;&#1077;&#1090;&#1077; &#1077;&#1076;&#1080;&#1085; &#1086;&#1090; &#1076;&#1074;&#1072;&#1090;&#1072; &#1086;&#1090;&#1075;&#1086;&#1074;&#1086;&#1088;&#1072;!","values":["","ДА","НЕ"]},"year_of_foundation":{"label":"&#1043;&#1086;&#1076;&#1080;&#1085;&#1072; &#1085;&#1072; &#1089;&#1098;&#1079;&#1076;&#1072;&#1074;&#1072;&#1085;&#1077;","fieldtype":"dropdown","messages":"&#1052;&#1086;&#1083;&#1103; &#1080;&#1079;&#1073;&#1077;&#1088;&#1077;&#1090;&#1077; &#1075;&#1086;&#1076;&#1080;&#1085;&#1072; &#1085;&#1072; &#1089;&#1098;&#1079;&#1076;&#1072;&#1074;&#1072;&#1085;&#1077;!","required":true,"values":["моля изберете","1990","1991","1992","1993","1994","1995","1996","1997","1998","1999","2000","2001","2002","2003","2004","2005","2006","2007","2008","2009","2010","2011","2012","2013","2014","2015"]},"adres":{"label":"&#1040;&#1076;&#1088;&#1077;&#1089;","fieldtype":"text","messages":"&#1052;&#1086;&#1083;&#1103; &#1074;&#1098;&#1074;&#1077;&#1076;&#1077;&#1090;&#1077; &#1090;&#1086;&#1095;&#1085;&#1080;&#1103;&#1090; &#1072;&#1076;&#1088;&#1077;&#1089; &#1085;&#1072; &#1074;&#1072;&#1096;&#1080;&#1103;&#1090; &#1084;&#1072;&#1075;&#1072;&#1079;&#1080;&#1085;!","required":true},"adres_2":{"label":"&#1040;&#1076;&#1088;&#1077;&#1089; 2","fieldtype":"text"},"post_code":{"label":"&#1055;&#1086;&#1097;&#1077;&#1085;&#1089;&#1082;&#1080; &#1082;&#1086;&#1076;","fieldtype":"text","required":false,"messages":"&#1052;&#1086;&#1083;&#1103; &#1074;&#1098;&#1074;&#1077;&#1076;&#1077;&#1090;&#1077; &#1087;&#1086;&#1097;&#1077;&#1085;&#1089;&#1082;&#1080;&#1103; &#1082;&#1086;&#1076; &#1085;&#1072; &#1085;&#1072;&#1089;&#1077;&#1083;&#1077;&#1085;&#1086;&#1090;&#1086; &#1084;&#1103;&#1089;&#1090;&#1086;!"},"city":{"label":"&#1043;&#1088;&#1072;&#1076;","fieldtype":"text","messages":"&#1052;&#1086;&#1083;&#1103; &#1074;&#1098;&#1074;&#1077;&#1076;&#1077;&#1090;&#1077; &#1080;&#1084;&#1077;&#1090;&#1086; &#1085;&#1072; &#1075;&#1088;&#1072;&#1076;&#1072;!","required":true},"vidove_magazin":{"checkbox":true,"number_required":0,"label":"&#1054;&#1090;&#1073;&#1077;&#1083;&#1077;&#1078;&#1077;&#1090;&#1077; &#1086;&#1090;&#1084;&#1077;&#1090;&#1082;&#1080;&#1090;&#1077;, &#1082;&#1086;&#1080;&#1090;&#1086; &#1089;&#1077; &#1086;&#1090;&#1085;&#1072;&#1089;&#1103;&#1090; &#1079;&#1072; &#1074;&#1072;&#1089;!","fieldtype":"checkbox","values":["Магазин","Оналйн Магазин","Директен пощенски каталог"]},"kratko_opisanie_magazin":{"label":"&#1050;&#1088;&#1072;&#1090;&#1082;&#1086; &#1086;&#1087;&#1080;&#1089;&#1072;&#1085;&#1080;&#1077; &#1085;&#1072; &#1074;&#1072;&#1096;&#1080;&#1103; &#1084;&#1072;&#1075;&#1072;&#1079;&#1080;&#1085; (&#1086;&#1089;&#1085;&#1086;&#1074;&#1077;&#1085; &#1073;&#1080;&#1079;&#1085;&#1077;&#1089;, &#1087;&#1088;&#1086;&#1076;&#1091;&#1082;&#1090;&#1086;&#1074; &#1084;&#1080;&#1082;&#1089;, &#1080; &#1076;&#1088;.)","fieldtype":"textarea","maxlength":"10000","required":true,"messages":"&#1052;&#1086;&#1083;&#1103; &#1086;&#1087;&#1080;&#1096;&#1077;&#1090;&#1077; &#1074;&#1072;&#1096;&#1080;&#1103;&#1090; &#1084;&#1072;&#1075;&#1072;&#1079;&#1080;&#1085;!"},"pet_drugi_marki":{"label":"&#1048;&#1079;&#1073;&#1088;&#1086;&#1080;&#1090;&#1077; &#1087;&#1077;&#1090; &#1076;&#1088;&#1091;&#1075;&#1080; &#1084;&#1072;&#1088;&#1082;&#1080;, &#1082;&#1086;&#1080;&#1090;&#1086; &#1087;&#1088;&#1086;&#1076;&#1072;&#1074;&#1072;&#1090;&#1077;","fieldtype":"textarea","maxlength":"10000"},"drugi_vidove_produkti":{"label":"&#1048;&#1079;&#1073;&#1088;&#1086;&#1081;&#1090;&#1077; &#1076;&#1088;&#1091;&#1075;&#1080; &#1074;&#1080;&#1076;&#1086;&#1074;&#1077; &#1087;&#1088;&#1086;&#1076;&#1091;&#1082;&#1090;&#1080;, &#1082;&#1086;&#1081;&#1090;&#1086; &#1087;&#1088;&#1086;&#1076;&#1072;&#1074;&#1072;&#1090;&#1077;","fieldtype":"textarea","maxlength":"10000"},"opisanie_klienti":{"label":"&#1082;&#1088;&#1072;&#1090;&#1082;&#1086; &#1086;&#1087;&#1080;&#1089;&#1072;&#1085;&#1080;&#1077; &#1085;&#1072; &#1074;&#1072;&#1096;&#1080;&#1090;&#1077; &#1082;&#1083;&#1080;&#1077;&#1085;&#1090;&#1080;","fieldtype":"textarea","maxlength":"10000"}},
"payment_rules":{"selling_on_website":{},"year_of_foundation":{},"vidove_magazin":{}},
"conditional_rules":{},
"application_version":"Web Form Builder (Windows), build 2.3.5152"
}